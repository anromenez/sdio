﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Domain.MainContext.SalesForce.SalesForceAgg
{
    public partial class SalesForceOrigen
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SalesForceOrigen()
        {

        }

        public int Id { get; set; }
        public string IdOportunidad { get; set; }
        public string PropietarioOportunidad { get; set; }
        public string TipologiaOportunidad { get; set; }
        public string NombreCliente { get; set; }
        public string NombreOportunidad { get; set; }
        public Nullable<System.DateTime> FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaCierreEstimada { get; set; }
        public Nullable<System.DateTime> FechaCierreReal { get; set; }
        public string ProbabilidadExito { get; set; }
        public string Etapa { get; set; }
        public string TipoOportunidad { get; set; }
        public Nullable<int> PlazoEstimadoProvision { get; set; }
        public Nullable<System.DateTime> FechaEstimadaInstalacionServicio { get; set; }
        public Nullable<int> DuracionContrato { get; set; }
        public string IngresoPorUnicaVezDivisa { get; set; }
        public string IngresoPorUnicaVez { get; set; }
        public string FullContractValueNetoDivisa { get; set; }
        public string FullContractValueNeto { get; set; }
        public string RecurrenteBrutoMensualDivisa { get; set; }
        public string RecurrenteBrutoMensual { get; set; }
        public string NumeroDelCaso { get; set; }
        public string Estado { get; set; }
        public string Departamento { get; set; }
        public string TipoSolicitud { get; set; }
        public string Asunto { get; set; }
        public Nullable<System.DateTime> FechaCarga { get; set; }
        public string LoginCarga { get; set; }

        public int IdCarga { get; set; }

        public void Insertar(string Login,DateTime _fechaCarga,int _idCarga)
        {
            if (string.IsNullOrWhiteSpace(Login))
                throw new ArgumentNullException("LoginCarga");

            LoginCarga = Login;
            FechaCarga = _fechaCarga;
            IdCarga = _idCarga;
        }
       
    }
}
