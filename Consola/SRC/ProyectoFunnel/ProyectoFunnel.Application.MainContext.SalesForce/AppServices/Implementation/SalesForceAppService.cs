﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using ProyectoFunnel.Domain.MainContext.SalesForce.SalesForceAgg;
using ProyectoFunnel.Infra.Data.MainContext.SalesForce.Repositories;
using System.Data.SqlClient;
using System.Transactions;
using ProyectoFunnel.Infra.SalesForce.Util;

namespace ProyectoFunnel.Application.MainContext.SalesForce.AppServices.Implementation
{
    public class SalesForceAppService
    {
        private readonly SalesForceOrigenRepositorio _salesForceOrigenRepositorio;
        private readonly SalesForceConsolidadoRepositorio _salesForceConsolidadoRepositorio;
        private readonly SalesForcePoblarDashboardRepositorio _salesForcePoblarDashboardRepositorio;
        public SalesForceAppService()
        {
            _salesForceOrigenRepositorio = new SalesForceOrigenRepositorio();
            _salesForceConsolidadoRepositorio = new SalesForceConsolidadoRepositorio();
            _salesForcePoblarDashboardRepositorio = new SalesForcePoblarDashboardRepositorio();
        }

        public async Task ImportarReporteSalesForceSAsync(string rutaFichero, string login)
        {
            var fechaHoraCarga = DateTime.Now;
            using (var cnFunnel = new SqlConnection(Configurations.ConnectionFunnel))
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 15, 0), TransactionScopeAsyncFlowOption.Enabled))
                {
                    await cnFunnel.OpenAsync();
                    //1.Generar cabecera importacion
                    var id = await GenerarCabeceraImportacion(new SalesForceOrigen { FechaCarga = fechaHoraCarga }, cnFunnel);

                    //2.Proceso de carga
                    if (string.IsNullOrWhiteSpace(login))
                    {
                        throw new ArgumentNullException("Login no especificado");
                    }
                    await CargarReporteSalesForce(rutaFichero, new SalesForceOrigen { LoginCarga = login, IdCarga = id, FechaCarga = fechaHoraCarga },cnFunnel);

                    //3. Proceso de consolidacion   
                    await ConsolidacionSalesForce(new SalesForceOrigen { LoginCarga = login, IdCarga = id }, cnFunnel);


                    scope.Complete();
                }
            }
        }

        private async Task<int> GenerarCabeceraImportacion(SalesForceOrigen request,SqlConnection cnFunnel)
        {
            var id = await _salesForceOrigenRepositorio.CargarSalesForceBeginOrigen(new SalesForceOrigen { FechaCarga = request.FechaCarga }, cnFunnel);
            return id;
        }

        #region Carga Reporte Sales

        private async Task CargarReporteSalesForce(string rutaFichero, SalesForceOrigen request, SqlConnection cnFunnel)
        {
            var fileExt = Path.GetExtension(rutaFichero);

            //1. Validaciones

            if (string.IsNullOrWhiteSpace(fileExt))
            {
                throw new ArgumentNullException("El fichero no tiene extensión");
            }
            else
            {
                if (fileExt.ToLower() != ".xls")
                {
                    throw new Exception("La extensión del fichero es inválida");
                }
            }

            //2.Lectura de ReporteSales
            var salesForceOrigen = LecturaReporteSales(rutaFichero);

            //3. Carga de la info en DB
            foreach (var sales in salesForceOrigen)
            {
                sales.Insertar(request.LoginCarga, request.FechaCarga.Value, request.IdCarga);
                await _salesForceOrigenRepositorio.CargarSalesForceOrigen(sales, cnFunnel);
            }
        }

        private List<SalesForceOrigen> LecturaReporteSales(string rutaFichero)
        {
            HSSFWorkbook hssfwb;
            ISheet sheet = null;
            var hojaValida = true;
            using (var file = new FileStream(rutaFichero, FileMode.Open, FileAccess.Read))
            {
                hssfwb = new HSSFWorkbook(file);
            }

            if (hssfwb.GetSheet("Hoja1") == null)
            {
                hojaValida = false;
            }
            else
            {
                //Assign the sheet
                sheet = hssfwb.GetSheet("Hoja1");
            }

            var listaImportacion = new List<SalesForceOrigen>();

            if (hojaValida)
            {
                if (ValidarCabeceras(sheet.GetRow(0)))
                {
                    var numFilas = sheet.PhysicalNumberOfRows;
                    if (numFilas.Equals(Convert.ToInt32(1)))
                    {
                        throw new Exception("No se incluyen elementos a cargar");
                    }
                    else
                    {
                        for (var row = Convert.ToInt32(1); row <= sheet.LastRowNum; row++)
                        {
                            if(sheet.GetRow(row) != null)
                            {
                                if(sheet.GetRow(row).GetCell(0).ToString().Substring(0,3) == "PER")
                                {
                                    var salesForceDetalle = new SalesForceOrigen
                                    {
                                        IdOportunidad = sheet.GetRow(row).GetCell(0).ToString().Trim(),
                                        PropietarioOportunidad = sheet.GetRow(row).GetCell(1).ToString().Trim(),
                                        TipologiaOportunidad = sheet.GetRow(row).GetCell(2).ToString().Trim(),
                                        NombreCliente = sheet.GetRow(row).GetCell(3).ToString().Trim(),
                                        NombreOportunidad = sheet.GetRow(row).GetCell(4).ToString().Trim(),
                                        FechaCreacion = sheet.GetRow(row).GetCell(5).DateCellValue,
                                        FechaCierreEstimada = sheet.GetRow(row).GetCell(6).DateCellValue,
                                        FechaCierreReal = sheet.GetRow(row).GetCell(7).ToString() == "" ? (DateTime?)null : 
                                        sheet.GetRow(row).GetCell(7).DateCellValue,
                                        ProbabilidadExito = sheet.GetRow(row).GetCell(8).ToString().Trim(),
                                        Etapa = sheet.GetRow(row).GetCell(9).ToString().Trim(),
                                        TipoOportunidad = sheet.GetRow(row).GetCell(10).ToString().Trim(),
                                        PlazoEstimadoProvision = sheet.GetRow(row).GetCell(11).ToString() == "" ? (int?)null :
                                                                    Convert.ToInt32(sheet.GetRow(row).GetCell(11).ToString()),
                                        FechaEstimadaInstalacionServicio = sheet.GetRow(row).GetCell(12).DateCellValue,
                                        DuracionContrato = sheet.GetRow(row).GetCell(13).ToString() == "" ? (int?)null :
                                                                    Convert.ToInt32(sheet.GetRow(row).GetCell(13).ToString()),
                                        IngresoPorUnicaVezDivisa = sheet.GetRow(row).GetCell(14).ToString().Trim(),
                                        IngresoPorUnicaVez = sheet.GetRow(row).GetCell(15).ToString().Trim().Replace(",", "."),
                                        FullContractValueNetoDivisa = sheet.GetRow(row).GetCell(16).ToString().Trim(),
                                        FullContractValueNeto = sheet.GetRow(row).GetCell(17).ToString().Trim().Replace(",", "."),
                                        RecurrenteBrutoMensualDivisa = sheet.GetRow(row).GetCell(18).ToString().Trim(),
                                        RecurrenteBrutoMensual = sheet.GetRow(row).GetCell(19).ToString().Trim().Replace(",", "."),
                                        NumeroDelCaso = sheet.GetRow(row).GetCell(20).ToString().Trim(),
                                        Estado = sheet.GetRow(row).GetCell(21).ToString().Trim(),
                                        Departamento = sheet.GetRow(row).GetCell(22).ToString().Trim(),
                                        TipoSolicitud = sheet.GetRow(row).GetCell(23).ToString().Trim(),
                                        Asunto = sheet.GetRow(row).GetCell(24).ToString().Trim()
                                    };

                                    listaImportacion.Add(salesForceDetalle);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                throw new Exception("No se encuentra la 'Hoja1' para el Reporte Sales Force ");
            }

            return listaImportacion;
        }

        #endregion Carga Reporte Sales


        private bool ValidarCabeceras(IRow cabecera)
        {
            if (cabecera == null)
            {
                return false;
            }

            var idOportunidad = cabecera.GetCell(0).ToString().Trim();
            var propietarioOportunidad = cabecera.GetCell(1).ToString().Trim();
            var tipologiaOportunidad = cabecera.GetCell(2).ToString().Trim();
            var nombreCliente = cabecera.GetCell(3).ToString().Trim();
            var nombreOportunidad = cabecera.GetCell(4).ToString().Trim();
            var fechaCreacion = cabecera.GetCell(5).ToString().Trim();
            var fechaCierreEstimada = cabecera.GetCell(6).ToString().Trim();
            var fechaCierreReal = cabecera.GetCell(7).ToString().Trim();
            var probabilidadExito = cabecera.GetCell(8).ToString().Trim();
            var etapa = cabecera.GetCell(9).ToString().Trim();
            var tipoOportunidad = cabecera.GetCell(10).ToString().Trim();
            var plazoEstimadoProvision = cabecera.GetCell(11).ToString().Trim();
            var fechaEstimadaInstalacionServicio = cabecera.GetCell(12).ToString().Trim();
            var duracionContrato = cabecera.GetCell(13).ToString().Trim();
            var ingresoPorUnicaVezDivisa = cabecera.GetCell(14).ToString().Trim();
            var ingresoPorUnicaVez = cabecera.GetCell(15).ToString().Trim();
            var fullContractValueNetoDivisa = cabecera.GetCell(16).ToString().Trim();
            var fullContractValueNeto = cabecera.GetCell(17).ToString().Trim();
            var recurrenteBrutoMensualDivisa = cabecera.GetCell(18).ToString().Trim();
            var recurrenteBrutoMensual = cabecera.GetCell(19).ToString().Trim();
            var numeroDelCaso = cabecera.GetCell(20).ToString().Trim();
            var estado = cabecera.GetCell(21).ToString().Trim();
            var departamento = cabecera.GetCell(22).ToString().Trim();
            var tipoSolicitud = cabecera.GetCell(23).ToString().Trim();
            var asunto = cabecera.GetCell(24).ToString().Trim();


            return
                !string.IsNullOrEmpty(idOportunidad) && idOportunidad.Equals("Id de la oportunidad (interno)") &&
                !string.IsNullOrWhiteSpace(propietarioOportunidad) && propietarioOportunidad.Equals("Propietario de oportunidad: Nombre completo") &&
                !string.IsNullOrWhiteSpace(tipologiaOportunidad) && tipologiaOportunidad.Equals("Tipología de la Oportunidad") &&
                !string.IsNullOrWhiteSpace(nombreCliente) && nombreCliente.Equals("Nombre del cliente: Nombre del cliente") &&
                !string.IsNullOrWhiteSpace(nombreOportunidad) && nombreOportunidad.Equals("Nombre de la oportunidad") &&
                !string.IsNullOrWhiteSpace(fechaCreacion) && fechaCreacion.Equals("Fecha de creación") &&
                !string.IsNullOrWhiteSpace(fechaCierreEstimada) && fechaCierreEstimada.Equals("Fecha de cierre estimada") &&
                !string.IsNullOrWhiteSpace(fechaCierreReal) && fechaCierreReal.Equals("Fecha de cierre real") &&
                !string.IsNullOrWhiteSpace(probabilidadExito) && probabilidadExito.Equals("Probabilidad de éxito") &&
                !string.IsNullOrWhiteSpace(etapa) && etapa.Equals("Etapa") &&
                !string.IsNullOrWhiteSpace(tipoOportunidad) && tipoOportunidad.Equals("Tipo de oportunidad") &&
                !string.IsNullOrWhiteSpace(plazoEstimadoProvision) && plazoEstimadoProvision.Equals("Plazo estimado de provisión (días)") &&
                !string.IsNullOrWhiteSpace(fechaEstimadaInstalacionServicio) && fechaEstimadaInstalacionServicio.Equals("Fecha estimada instalación del servicio") &&
                !string.IsNullOrWhiteSpace(duracionContrato) && duracionContrato.Equals("Duración del contrato (meses)") &&
                !string.IsNullOrWhiteSpace(ingresoPorUnicaVezDivisa) && ingresoPorUnicaVezDivisa.Equals("Ingreso por única vez (convertido) Divisa") &&
                !string.IsNullOrWhiteSpace(ingresoPorUnicaVez) && ingresoPorUnicaVez.Equals("Ingreso por única vez (convertido)") &&
                !string.IsNullOrWhiteSpace(fullContractValueNetoDivisa) && fullContractValueNetoDivisa.Equals("Full contract value neto (FCV) Divisa") &&
                !string.IsNullOrWhiteSpace(fullContractValueNeto) && fullContractValueNeto.Equals("Full contract value neto (FCV)") &&
                !string.IsNullOrWhiteSpace(recurrenteBrutoMensualDivisa) && recurrenteBrutoMensualDivisa.Equals("Recurrente bruto mensual Divisa") &&
                !string.IsNullOrWhiteSpace(recurrenteBrutoMensual) && recurrenteBrutoMensual.Equals("Recurrente bruto mensual") &&
                !string.IsNullOrWhiteSpace(numeroDelCaso) && numeroDelCaso.Equals("Número del caso") &&
                !string.IsNullOrWhiteSpace(estado) && estado.Equals("Estado") &&
                !string.IsNullOrWhiteSpace(departamento) && departamento.Equals("Departamento") &&
                !string.IsNullOrWhiteSpace(tipoSolicitud) && tipoSolicitud.Equals("Tipo de solicitud") &&
                !string.IsNullOrWhiteSpace(asunto) && asunto.Equals("Asunto");
        }

        private async Task ConsolidacionSalesForce(SalesForceOrigen request,SqlConnection cnFunnel)
        {
            await _salesForceConsolidadoRepositorio.ConsolidarSalesForce(request, cnFunnel);
        }


    }
}

