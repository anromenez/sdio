﻿using ProyectoFunnel.Service.MainContext.SalesForce.Implementations;

namespace ProyectoFunnel.Console.SalesForce
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            System.Console.WriteLine("*********** Carga Diaria de Reporte SalesForce ************");
            System.Console.WriteLine("* Ingrese Login del Usuario a Cargar: *");
            var login = System.Console.ReadLine();
            System.Console.WriteLine("* Ingrese la ruta completa del fichero a cargar: *");
            var rutaFichero = System.Console.ReadLine();
            var servicio = new SalesForceService();

            try
            {
               servicio.ImportarReporteSalesForceSAsync(rutaFichero, login).Wait();
               System.Console.WriteLine("Se realizó la carga de Sales Force con éxito");
            }
            catch (System.Exception ex)
            {
                if (ex.InnerException != null) System.Console.Error.WriteLine(ex.InnerException.Message);
            }
            watch.Stop();

            System.Console.WriteLine("*********** Fin de Carga Diaria de Reporte SalesForce.Tiempo de ejecución: " + (watch.ElapsedMilliseconds / 1000 / 60) 
                + " minutos * ***********");
            System.Console.ReadLine();
        }
    }
}
