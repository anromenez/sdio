﻿using System;
using ProyectoFunnel.Service.MainContext.Oportunidades.Implementations;

namespace ProyectoFunnel.Console.Oportunidades
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            System.Console.WriteLine("*********** Carga Diaria de Plantillas de Oportunidades ************");
            System.Console.WriteLine("* Ingrese Login del Usuario a Cargar: *");
            var login = System.Console.ReadLine();
            System.Console.WriteLine("* Ingrese la ruta completa de la carpeta a cargar: *");
            var rutaFolder = System.Console.ReadLine();
            var servicio = new OportunidadesService();

            try
            {
                servicio.ImportarOportunidadesAsync(rutaFolder, login).Wait();
                System.Console.WriteLine("Se realizó la carga de Oportunidad con éxito");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) System.Console.Error.WriteLine(ex.InnerException.Message);
            }

            watch.Stop();

            System.Console.WriteLine("*********** Fin de Carga Diaria de Plantillas de Oportunidades. Tiempo de ejecución: " + (watch.ElapsedMilliseconds / 1000 / 60) 
                + " minutos ************");
            System.Console.ReadLine();

        }
    }
}
