﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Transactions;
using ProyectoFunnel.Infra.Oportunidades.Util.Resources;
using ProyectoFunnel.Application.MainContext.Oportunidades.AppServices.Dto;
using NPOI.SS.UserModel;
using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;
using ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories;
using System.Data.SqlClient;
using System.Globalization;
using ProyectoFunnel.Infra.Oportunidades.Util;
using static ProyectoFunnel.Infra.Oportunidades.Util.Grupos;

namespace ProyectoFunnel.Application.MainContext.Oportunidades.AppServices.Implementation
{
    public class OportunidadesAppService
    {
        private readonly OportunidadFinancieraOrigenRepositorio _oportunidadFinancieraOrigenRepositorio;
        private readonly OportunidadContableOrigenRepositorio _oportunidadContableOrigenRepositorio;
        private readonly OportunidadOrigenRepositorio _oportunidadOrigenRepositorio;
        private readonly SalesForceConsolidadoCabeceraRepositorio _salesForceConsolidadoCabeceraRepositorio;
        private readonly OportunidadPmoLotusLineaInsertRepositorio _oportunidadPmoLotusLineaInsertRepositorio;
        public OportunidadesAppService()
        {
            _oportunidadFinancieraOrigenRepositorio = new OportunidadFinancieraOrigenRepositorio();
            _oportunidadContableOrigenRepositorio = new OportunidadContableOrigenRepositorio();
            _oportunidadOrigenRepositorio = new OportunidadOrigenRepositorio();
            _salesForceConsolidadoCabeceraRepositorio = new SalesForceConsolidadoCabeceraRepositorio();
            _oportunidadPmoLotusLineaInsertRepositorio = new OportunidadPmoLotusLineaInsertRepositorio();
        }

        public async Task ImportarOportunidadesAsync(string rutaFolder, string login)
        {
            var fechaHoraCarga = DateTime.Now;
            using (var cnFunnel = new SqlConnection(Configurations.ConnectionFunnel))
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 15, 0), TransactionScopeAsyncFlowOption.Enabled))
                {
                    await cnFunnel.OpenAsync();
                    //1.Generar cabecera importacion
                    var idCarga =
                        await GenerarCabeceraImportacion(new OportunidadFinancieraOrigen { FechaCarga = fechaHoraCarga }, cnFunnel);

                    //2. Elegir plantillas
                    var listado = SelecteTemplatesToRead(rutaFolder, cnFunnel);

                    //3. Cargar info de plantillas
                    await CargarInfoPlantillas(listado, login, idCarga, cnFunnel);

                    scope.Complete();
                }                            
            }
        }

        #region Elegir plantillas

        private async Task<int> GenerarCabeceraImportacion(OportunidadFinancieraOrigen request, SqlConnection cnFunnel)
        {
            var id = await _oportunidadOrigenRepositorio.OportunidadBeginOrigen(new OportunidadFinancieraOrigen { FechaCarga = request.FechaCarga }, cnFunnel);
            return id;
        }

        private List<OportunidadOrigenDto> SelecteTemplatesToRead(string rutaFolder,SqlConnection cnFunnel)
        {
            var centrales = string.Concat(rutaFolder, PlantillasOportunidades.CENTRALES);
            var dataCenter = string.Concat(rutaFolder, PlantillasOportunidades.DATA_CENTER);
            var datos = string.Concat(rutaFolder, PlantillasOportunidades.DATOS);
            var elearning = string.Concat(rutaFolder, PlantillasOportunidades.E_LEARNING);
            var infra = string.Concat(rutaFolder, PlantillasOportunidades.INFRAESTRUCTURA);
            var oi = string.Concat(rutaFolder, PlantillasOportunidades.OI);
            var rt = string.Concat(rutaFolder, PlantillasOportunidades.RT);
            var seguridad = string.Concat(rutaFolder, PlantillasOportunidades.SEGURIDAD);
            var tis = string.Concat(rutaFolder, PlantillasOportunidades.TIS);
            var tiServers = string.Concat(rutaFolder, PlantillasOportunidades.TI_SERVIDORES);
            var tx = string.Concat(rutaFolder, PlantillasOportunidades.TX);
            var voz = string.Concat(rutaFolder, PlantillasOportunidades.VOZ);

            var grupo1 = new List<string> {centrales, dataCenter, datos, elearning};
            var grupo2 = new List<string> {infra, oi, rt, seguridad};
            var grupo3 = new List<string> {tis, tiServers, tx, voz};

            var grupoTask1 = new Task<List<OportunidadOrigenDto>>(() => ImportarGrupo1Plantillas(grupo1, cnFunnel));
            var grupoTask2 = new Task<List<OportunidadOrigenDto>>(() => ImportarGrupo2Plantillas(grupo2, cnFunnel));
            var grupoTask3 = new Task<List<OportunidadOrigenDto>>(() => ImportarGrupo3Plantillas(grupo3, cnFunnel));
            grupoTask1.Start();
            grupoTask2.Start();
            grupoTask3.Start();

            Task.WaitAll(grupoTask1, grupoTask2, grupoTask3);

            var resultGrupo1 = grupoTask1.Result;
            var resultGrupo2 = grupoTask2.Result;
            var resultGrupo3 = grupoTask3.Result;

            var grupoPlantillas = new List<OportunidadOrigenDto>();
            grupoPlantillas.AddRange(resultGrupo1);
            grupoPlantillas.AddRange(resultGrupo2);
            grupoPlantillas.AddRange(resultGrupo3);
            return grupoPlantillas;
        }

        private IEnumerable<PlantillaOportunidadDto> SeleccionarFicheros(List<string> grupos)
        {
            var rutasFicheros = new List<PlantillaOportunidadDto>();

            foreach (var itemGrupo in grupos)
            {
                var subFolders = Directory.GetDirectories(itemGrupo);
                foreach (var folder in subFolders)
                {
                    var files = Directory.GetFiles(folder);
                    if (!files.Any()) continue;

                    if (files.Count().Equals(1))
                    {
                        rutasFicheros.Add(new PlantillaOportunidadDto
                        {
                            ruta = files[0],
                            Nombre = Path.GetFileNameWithoutExtension(files[0])
                        });
                    }
                    else
                    {
                        var fileNames = new List<PlantillaOportunidadDto>();
                        foreach (var file in files)
                        {
                            if (FicheroAptoLeer(file))
                            {
                                var archivo = new PlantillaOportunidadDto
                                {
                                    ruta = file,
                                    Nombre = Path.GetFileNameWithoutExtension(file)
                                };
                                fileNames.Add(archivo);
                            }
                        }

                        var fechaElegida = fileNames.OrderByDescending(x => x.Fecha).ThenByDescending(x => x.NumeroCaso).ToList();
                        rutasFicheros.Add(fechaElegida.FirstOrDefault());
                    }
                }
            }

            return rutasFicheros;
        }

        private List<OportunidadOrigenDto> ImportarGrupos(List<string> grupos, TipoGrupo tipoGrupo,SqlConnection cnFunnel)
        {
            var listaFicheros = SeleccionarFicheros(grupos);
            var resultado = new List<OportunidadOrigenDto>();

            foreach (var fichero in listaFicheros)
            {
                var cabecerasProyecto = GetPerNumeroCaso(fichero.Nombre);
                var existe = _salesForceConsolidadoCabeceraRepositorio.SalesForceVigente(cnFunnel, cabecerasProyecto.IdOportunidad.Trim());
                //existe = true;

                if (!existe) continue;
                OportunidadOrigenDto libro;
                switch (tipoGrupo)
                {
                    case TipoGrupo.Grupo1:
                        libro = LecturaOportunidadesGrupo1(fichero.ruta);
                        break;
                    case TipoGrupo.Grupo2:
                        libro = LecturaOportunidadesGrupo2(fichero.ruta);
                        break;
                    case TipoGrupo.Grupo3:
                        libro = LecturaOportunidadesGrupo3(fichero.ruta);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(tipoGrupo), tipoGrupo, null);
                }

                if (libro == null)
                    continue;

                if (libro.oportunidadFinanciera != null)
                {
                    libro.oportunidadFinanciera.IdOportunidad = string.IsNullOrWhiteSpace(cabecerasProyecto.IdOportunidad) ?
                        null : cabecerasProyecto.IdOportunidad.Trim();
                    libro.oportunidadFinanciera.NumeroCaso = string.IsNullOrWhiteSpace(cabecerasProyecto.NumeroCaso) ? null :
                        cabecerasProyecto.NumeroCaso.Trim();
                }
                if (libro.oportunidadContable != null && cabecerasProyecto.IdOportunidad != null)
                {
                    libro.oportunidadContable.ForEach(x =>
                    {
                        x.IdOportunidad = cabecerasProyecto.IdOportunidad.Trim();
                        x.NumeroCaso = cabecerasProyecto.NumeroCaso == null ? null : cabecerasProyecto.NumeroCaso.Trim();
                        x.RutaCarga = fichero.ruta;
                    });
                }
                if (libro.OportunidadLotusLineas != null)
                {
                    libro.OportunidadLotusLineas.ForEach(x =>
                    {
                        x.IdOportunidad = cabecerasProyecto.IdOportunidad.Trim();
                        x.NumeroCaso = cabecerasProyecto.NumeroCaso == null ? null : cabecerasProyecto.NumeroCaso.Trim();
                    });
                }

                resultado.Add(libro);
            }
               
            return resultado;
        }

        private List<OportunidadOrigenDto> ImportarGrupo1Plantillas(List<string> grupos, SqlConnection cnFunnel)
        {
            return ImportarGrupos(grupos, TipoGrupo.Grupo1, cnFunnel);
        }

        private List<OportunidadOrigenDto> ImportarGrupo2Plantillas(List<string> grupos, SqlConnection cnFunnel)
        {
            return ImportarGrupos(grupos, TipoGrupo.Grupo2, cnFunnel);
        }

        private List<OportunidadOrigenDto> ImportarGrupo3Plantillas(List<string> grupos, SqlConnection cnFunnel)
        {
            return ImportarGrupos(grupos, TipoGrupo.Grupo3, cnFunnel);
        }

        private static bool FicheroAptoLeer(string rutaFichero)
        {
            var file = new FileStream(rutaFichero, FileMode.Open, FileAccess.Read);
            // WorkbookFactory crea un libro en funcion de la version de excel 2003 / 20007
            var hssfwb = WorkbookFactory.Create(file);
            file.Close();

            return hssfwb.GetSheet("FC_FINANCIERO") != null && hssfwb.GetSheet("FC_CONTABLE") != null;
        }

        private List<OportunidadContableOrigen> GetOportunidadesContables(ISheet sheetFC_CONTABLE,int posiIniRow,int posiFinRow,int posiFinCol,string ruta = null)
        {
            var rowInicial = 0;
            var colInicial = 0;
            var colTotalContable = 0;
            var rowInicialTipoCambio = 0;
            var colInicialTipoCambio = 0;

            for (var row = posiIniRow; row <= posiFinRow; row++)
            {
                for (var col = 0; col <= posiFinCol; col++)
                {
                    try
                    {
                        var celdaRaiz = sheetFC_CONTABLE.GetRow(row).GetCell(col);
                        if (celdaRaiz == null || !celdaRaiz.ToString().Equals("INGRESO ANUAL")) continue;
                        rowInicial = row;
                        colInicial = col;
                        break;
                    }
                    catch (Exception)
                    {

                    }
                }

                if (rowInicial != 0 && colInicial != 0)
                {
                    break;
                }
            }

            if (rowInicial == 0 && colInicial == 0)
            {
                return new List<OportunidadContableOrigen>();
            }

            rowInicial--;
            colInicial++;

            for (var col = 0; col <= 20; col++)
            {
                var celdaRaiz = sheetFC_CONTABLE.GetRow(rowInicial).GetCell(col);
                if (celdaRaiz != null && celdaRaiz.ToString().Equals("TOTAL"))
                {
                    colTotalContable = col;
                    break;
                }
            }

            // Evaluando tipo de cambio

            for (var row = posiIniRow; row <= posiFinRow; row++)
            {
                for (var col = 0; col <= posiFinCol; col++)
                {
                    try
                    {
                        var celdaRaiz = sheetFC_CONTABLE.GetRow(row).GetCell(col);
                        if (celdaRaiz == null || !celdaRaiz.ToString().Equals("Tipo de Cambio :")) continue;
                        rowInicialTipoCambio = row;
                        colInicialTipoCambio = col;
                        break;
                    }
                    catch (Exception)
                    {

                    }
                }

                if (rowInicialTipoCambio != 0 && colInicialTipoCambio != 0)
                {
                    break;
                }
            }

            double tipoCambioIngresoAnio1;
            try
            {
                tipoCambioIngresoAnio1 = sheetFC_CONTABLE.GetRow(rowInicialTipoCambio + 2).GetCell(colInicialTipoCambio + 1).NumericCellValue;
            }
            catch (Exception)
            {
                tipoCambioIngresoAnio1 = 0;
            }

            double tipoCambioIngresoAnio2;
            try
            {
                tipoCambioIngresoAnio2 = sheetFC_CONTABLE.GetRow(rowInicialTipoCambio + 2).GetCell(colInicialTipoCambio + 2).NumericCellValue;
            }
            catch (Exception)
            {
                tipoCambioIngresoAnio2 = 0;
            }

            double tipoCambioIngresoAnio3;
            try
            {
                tipoCambioIngresoAnio3 = sheetFC_CONTABLE.GetRow(rowInicialTipoCambio + 2).GetCell(colInicialTipoCambio + 3).NumericCellValue;
            }
            catch (Exception)
            {
                tipoCambioIngresoAnio3 = 0;
            }

            double tipoCambioCostos1;
            try
            {
                tipoCambioCostos1 = sheetFC_CONTABLE.GetRow(rowInicialTipoCambio + 3).GetCell(colInicialTipoCambio + 1).NumericCellValue;
            }
            catch (Exception)
            {
                tipoCambioCostos1 = 0;
            }

            double tipoCambioCapex1;
            try
            {
                tipoCambioCapex1 = sheetFC_CONTABLE.GetRow(rowInicialTipoCambio + 4).GetCell(colInicialTipoCambio + 1).NumericCellValue;
            }
            catch (Exception)
            {
                tipoCambioCapex1 = 0;
            }

            var contadorInicial = 1;

            // Fin de tipo de cambio

            var listaContable = new List<OportunidadContableOrigen>();

            for (var col = colInicial; col <= colTotalContable - 1; col++)
            {
                var anioContable = sheetFC_CONTABLE.GetRow(rowInicial).GetCell(col).NumericCellValue;
                var ingresoAnualContable = sheetFC_CONTABLE.GetRow(rowInicial + 1).GetCell(col).NumericCellValue;
                var oIBDAANUALContable = sheetFC_CONTABLE.GetRow(rowInicial + 2).GetCell(col).NumericCellValue;
                var capexContable = sheetFC_CONTABLE.GetRow(rowInicial + 3).GetCell(col).NumericCellValue;

                var oibaPorcentajeContable = sheetFC_CONTABLE.GetRow(rowInicial + 4).GetCell(col) == null ? (double?)null :
                    sheetFC_CONTABLE.GetRow(rowInicial + 4).GetCell(col).NumericCellValue;

                var vanProyectoContable = sheetFC_CONTABLE.GetRow(rowInicial + 5).GetCell(col) == null ? (double?)null :
                sheetFC_CONTABLE.GetRow(rowInicial + 5).GetCell(col).NumericCellValue;

                var vanPorVaiContable = sheetFC_CONTABLE.GetRow(rowInicial + 6).GetCell(col) == null ? (double?)null :
                    sheetFC_CONTABLE.GetRow(rowInicial + 6).GetCell(col).NumericCellValue;

                string paybackContable;
                if (sheetFC_CONTABLE.GetRow(rowInicial + 7).GetCell(col) == null)
                {
                    paybackContable = null;
                }
                else
                {
                    try
                    {
                        paybackContable = sheetFC_CONTABLE.GetRow(rowInicial + 7).GetCell(col).StringCellValue;
                    }
                    catch (Exception)
                    {
                        paybackContable = sheetFC_CONTABLE.GetRow(rowInicial + 7).GetCell(col).NumericCellValue.ToString();
                    }
                }

                var rescateContable = sheetFC_CONTABLE.GetRow(rowInicial + 8).GetCell(col) == null ? null :
                    sheetFC_CONTABLE.GetRow(rowInicial + 8).GetCell(col).NumericCellValue.ToString();

                var plantillaCentralContable = new OportunidadContableOrigen()
                {
                    IngresoAnual = Convert.ToDecimal(ingresoAnualContable),
                    OibdaAnual = Convert.ToDecimal(oIBDAANUALContable),
                    Capex = Convert.ToDecimal(capexContable),
                    OibdaPorcentaje = Convert.ToDecimal(oibaPorcentajeContable) * 100,
                    VanProyecto = Convert.ToDecimal(vanProyectoContable),
                    VanPorVai = Convert.ToDecimal(vanPorVaiContable) * 100,
                    PayBack = paybackContable,
                    ValorRescate = Convert.ToDecimal(rescateContable),
                    Anio = Convert.ToInt32(anioContable),
                    TipoCambioIngresos = contadorInicial == 1 ? Convert.ToDecimal(tipoCambioIngresoAnio1) : (
                    contadorInicial == 2 ? Convert.ToDecimal(tipoCambioIngresoAnio2) : Convert.ToDecimal(tipoCambioIngresoAnio3)),
                    TipoCambioCostos = contadorInicial == 1 ? Convert.ToDecimal(tipoCambioCostos1) : (decimal?)null,
                    TipoCambioCapex = contadorInicial == 1 ? Convert.ToDecimal(tipoCambioCapex1) : (decimal?)null
                };

                //var oportunidadesMensuales = ObtenerContabilidadMensual(Convert.ToInt32(anioContable),
                //    sheetFC_CONTABLE.GetRow(rowInicial + 1).GetCell(col).CellFormula, sheetFC_CONTABLE,ruta);

                //plantillaCentralContable.OportunidadContableOrigenMensual = oportunidadesMensuales;

                listaContable.Add(plantillaCentralContable);

                contadorInicial++;
            }
            return listaContable;
        }

        private IEnumerable<OportunidadPmoLotusLinea> CargaPmoComun(ISheet hoja,string rutaFichero,TipoHoja tipoHoja)
        {
            var listaItems = new List<OportunidadPmoLotusLinea>();

            for (var colInicial = 2; colInicial <= 180; colInicial++)
            {
                try
                {
                    var celdaFinal = hoja.GetRow(3).GetCell(colInicial);
                    if (celdaFinal == null || string.IsNullOrWhiteSpace(celdaFinal.ToString()))
                        break;
                }
                catch (Exception)
                {
                    break;
                }

                for (var rowInicial = 4; rowInicial <= hoja.LastRowNum; rowInicial++)
                {
                    if (hoja.GetRow(rowInicial) == null)
                    {
                        break;
                    }

                    string detalleProceso;
                    try
                    {
                        detalleProceso = hoja.GetRow(rowInicial).GetCell(colInicial).StringCellValue.
                            ToString(CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            detalleProceso = hoja.GetRow(3).GetCell(colInicial).ToString() == "%" ||
                                             hoja.GetRow(3).GetCell(colInicial).ToString() == "MARGEN OIBDA" ||
                                             hoja.GetRow(3).GetCell(colInicial).ToString() == "VAN/VAI"
                                ? Math.Round((hoja.GetRow(rowInicial).GetCell(colInicial).NumericCellValue * 100), 2, MidpointRounding.AwayFromZero).ToString("0.00") + "%"
                                : Math.Round(hoja.GetRow(rowInicial).GetCell(colInicial).NumericCellValue, 2, MidpointRounding.AwayFromZero)
                                    .ToString("0.00");
                        }
                        catch (Exception)
                        {
                            detalleProceso = null;
                        }
                    }

                    var item = new OportunidadPmoLotusLinea
                    {
                        Item = Convert.ToInt32(hoja.GetRow(rowInicial).GetCell(0).NumericCellValue),
                        DatosProceso = hoja.GetRow(3).GetCell(colInicial).ToString(),
                        DetalleProceso = detalleProceso,
                        TipoHoja = tipoHoja
                    };

                    listaItems.Add(item);
                }
            }

            return listaItems;
        }

        private List<OportunidadPmoLotusLinea> CargaPmoLotus(IWorkbook hssfwb,string rutaFichero)
        {
            var listaItems = new List<OportunidadPmoLotusLinea>();

            //var sheetFC_CmiAlquiler = hssfwb.GetSheet("CARGA PMO-LOTUS -CMI ALQUILER");
            var sheetFC_CmiVentas = hssfwb.GetSheet("CARGA PMO-LOTUS-CMI-VENTA");
            var sheetFC_CmiLinea = hssfwb.GetSheet("Carga PMO-LOTUS_LINEAS");
            var sheetFC_Cmi = hssfwb.GetSheet("CARGA PMO-LOTUS-CMI");

            //if (sheetFC_CmiAlquiler != null)
            //{
            //    listaItems.AddRange(CargaPmoComun(sheetFC_CmiAlquiler, rutaFichero,TipoHoja.CmiAlquiler));
            //}
            
            if(sheetFC_Cmi != null)
            {
                listaItems.AddRange(CargaPmoComun(sheetFC_CmiLinea, rutaFichero, TipoHoja.Cmi));
            }
            else
            {
                if (sheetFC_CmiLinea != null)
                {
                    listaItems.AddRange(CargaPmoComun(sheetFC_CmiLinea, rutaFichero, TipoHoja.CmiLinea));
                }
                else
                {
                    if (sheetFC_CmiVentas != null)
                    {
                        listaItems.AddRange(CargaPmoComun(sheetFC_CmiVentas, rutaFichero, TipoHoja.CmiVenta));
                    }
                }
            }

            return listaItems;
        }

        private List<OportunidadContableOrigenMensual> ObtenerContabilidadMensual(int anio,string formulaIngresoAnual, ISheet sheetFC_CONTABLE,string ruta = null)
        {
            var listaOportunidades = new List<OportunidadContableOrigenMensual>();

            var formula = formulaIngresoAnual.Replace("SUMIFS(", "").Replace(")", "");
            var rangos = formula.Split(',');
            var rowIniIngreso = Convert.ToInt32(rangos[0].Split(':')[0].Split('$')[2]);
            var colIni = rangos[0].Split(':')[0].Split('$')[1];
            var colIniPosiIngreso = 0;

            rowIniIngreso--;
            switch (colIni)
            {
                case "A":
                    colIniPosiIngreso = 0;
                    break;
                case "B":
                    colIniPosiIngreso = 1;
                    break;
                case "C":
                    colIniPosiIngreso = 2;
                    break;
                case "D":
                    colIniPosiIngreso = 3;
                    break;
                case "E":
                    colIniPosiIngreso = 4;
                    break;
                case "F":
                    colIniPosiIngreso = 5;
                    break;
                case "G":
                    colIniPosiIngreso = 6;
                    break;
                case "H":
                    colIniPosiIngreso = 7;
                    break;
                case "I":
                    colIniPosiIngreso = 8;
                    break;
                case "J":
                    colIniPosiIngreso = 9;
                    break;
                case "K":
                    colIniPosiIngreso = 10;
                    break;
                case "L":
                    colIniPosiIngreso = 11;
                    break;
                case "M":
                    colIniPosiIngreso = 12;
                    break;
                case "N":
                    colIniPosiIngreso = 13;
                    break;
                case "O":
                    colIniPosiIngreso = 14;
                    break;
                case "P":
                    colIniPosiIngreso = 15;
                    break;
            }

            var rowIniMes = rowIniIngreso - 2;

            var mesInicial = sheetFC_CONTABLE.GetRow(rowIniMes).GetCell(colIniPosiIngreso).DateCellValue.Month;

            var mes = mesInicial;
            var celdaInicial = 0;
            while (mes <= 13)
            {
                if (mes == 13)
                {
                    if (mes == 13)
                        mes = 1;
                        anio++;
                }

                try
                {
                    var oportunidadMensual = new OportunidadContableOrigenMensual
                    {
                        Mes = mes,
                        Anio = anio,
                        IngresoAnual =
                            Convert.ToDecimal(sheetFC_CONTABLE.GetRow(rowIniIngreso).GetCell(colIniPosiIngreso + celdaInicial).NumericCellValue)
                    };

                    listaOportunidades.Add(oportunidadMensual);
                }
                catch (Exception)
                {
                    mes = 13;
                }

                mes++;
                celdaInicial++;
            }

            return listaOportunidades;

        }

        private OportunidadOrigenDto LecturaOportunidadesGrupo1(string rutaFichero)
        {
            //WorkbookFactory crea un libro en funcion de la version de excel 2003/20007
            using (var file = new FileStream(rutaFichero, FileMode.Open, FileAccess.Read))
            {
                var hssfwb = WorkbookFactory.Create(file); 

                //Assign the sheet
                var sheetFC_FINANCIERO = hssfwb.GetSheet("FC_FINANCIERO");
                var sheetFC_CONTABLE = hssfwb.GetSheet("FC_CONTABLE");

                // 1.Plantilla centrales
                var rowInicial = 0;
                var colInicial = 0;

                var rowTasaDsctoInicial = 0;
                var colTasaDsctoInicial = 0;

                double vanTasa;
                double diFlujoNeto;
                var arregloTasa = new List<double>();
                double[] rangoVan;

                var listaLineasLotus = CargaPmoLotus(hssfwb, rutaFichero);

                if (rutaFichero.Contains(PlantillasOportunidades.CENTRALES))
                {
                    //Seccion financiera
                    rowInicial = 0;
                    colInicial = 0;
                    for (var row = 161; row <= 200; row++)
                    {
                        for (var col = 0; col <= 8; col++)
                        {
                            ICell celdaRaiz;
                            try
                            {
                                celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                {
                                    rowInicial = row;
                                    colInicial = col;
                                    break;
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }

                        if (rowInicial != 0 && colInicial != 0)
                        {
                            break;
                        }
                    }


                    OportunidadFinancieraOrigen plantillaCentralFinanciera = null;

                    if (rowInicial != 0 && colInicial != 0)
                    {

                        // NumericCellValue obtiene el valor en function resultado formula

                        var periodoMesesCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                        var ingresoTotalCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                        var pagoUnicoDolaresCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                        var recurrenteDolaresCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                        var vanCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                        var vanNetoPorIngresosCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                        var costosDirectosCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                        var oIDBACENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                        var oIDBAPercentCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                        var caPexCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                        string paybackCENTRALES;
                        try
                        {
                            paybackCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                        }
                        catch (Exception)
                        {
                            paybackCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                        }

                        var depreciacionCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                        var tirCENTRALES = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;



                        plantillaCentralFinanciera = new OportunidadFinancieraOrigen
                        {
                            PeriodoMeses = Convert.ToInt32(periodoMesesCENTRALES),
                            IngresoTotal = Convert.ToDecimal(ingresoTotalCENTRALES),
                            PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresCENTRALES),
                            RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresCENTRALES),
                            VanFlujoNeto = Convert.ToDecimal(vanCENTRALES),
                            VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosCENTRALES) * 100,
                            CostosDirectos = Convert.ToDecimal(costosDirectosCENTRALES),
                            Oidba = Convert.ToDecimal(oIDBACENTRALES),
                            OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentCENTRALES) * 100,
                            Capex = Convert.ToDecimal(caPexCENTRALES),
                            Payback = paybackCENTRALES.ToString(),
                            Depreciacion = Convert.ToDecimal(depreciacionCENTRALES),
                            Tir = Convert.ToDecimal(tirCENTRALES) * 100,
                            Tipo = PlantillasOportunidades.CENTRALES
                        };
                    }

                    //Seccion contable

                    var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 150, 180, 8, rutaFichero);


                    return new OportunidadOrigenDto
                        {
                          oportunidadFinanciera = plantillaCentralFinanciera,
                          oportunidadContable = listaContable,
                          OportunidadLotusLineas = listaLineasLotus
                        };
                }
                else
                {
                    if (rutaFichero.Contains(PlantillasOportunidades.DATA_CENTER))
                    {
                        //Seccion financiera
                        for (var row = 135; row <= 200; row++)
                        {
                            for (var col = 0; col <= 9; col++)
                            {
                                ICell celdaRaiz;
                                try
                                {
                                    celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                    if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                    {
                                        rowInicial = row;
                                        colInicial = col;
                                        break;
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }

                            if (rowInicial != 0 && colInicial != 0)
                            {
                                break;
                            }
                        }

                        OportunidadFinancieraOrigen plantillaDataCenterFinanciera = null;

                        if (rowInicial != 0 && colInicial != 0)
                        {
                            rowInicial = rowInicial--;

                            //CALCULO DEL FLUJO NETO
                            rowTasaDsctoInicial = 0;
                            colTasaDsctoInicial = 0;
                            for (var row = 130; row <= 150; row++)
                            {
                                for (var col = 0; col <= 12; col++)
                                {
                                    ICell celdaRaiz;
                                    try
                                    {
                                        celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                        if (celdaRaiz != null && celdaRaiz.ToString().Equals("Tasa de Descuento"))
                                        {
                                            rowTasaDsctoInicial = row;
                                            colTasaDsctoInicial = col;
                                            break;
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                if (rowTasaDsctoInicial != 0 && colTasaDsctoInicial != 0)
                                {
                                    break;
                                }
                            }

                            vanTasa = Math.Round(sheetFC_FINANCIERO.GetRow(rowTasaDsctoInicial).GetCell(colTasaDsctoInicial + 2).NumericCellValue, 10, MidpointRounding.AwayFromZero);
                            diFlujoNeto = sheetFC_FINANCIERO.GetRow(rowTasaDsctoInicial - 4).GetCell(colTasaDsctoInicial).NumericCellValue;

                            for (var x = colTasaDsctoInicial + 1; x <= sheetFC_FINANCIERO.GetRow(rowTasaDsctoInicial - 4).LastCellNum; x++)
                            {
                                try
                                {
                                    arregloTasa.Add(Math.Round(sheetFC_FINANCIERO.GetRow(rowTasaDsctoInicial - 4).GetCell(x).NumericCellValue, 10, MidpointRounding.AwayFromZero));
                                }
                                catch (Exception)
                                {
                                    break;
                                }
                            }

                            rangoVan = arregloTasa.ToArray();

                            var vanNeto = Microsoft.VisualBasic.Financial.NPV(vanTasa, ref rangoVan) - diFlujoNeto;
                            var npv = rangoVan.Select((c, n) => c / Math.Pow(1 + vanTasa, n)).Sum();


                            //FIN CALCULO DEL FLUJO NETO

                            // NumericCellValue obtiene el valor en function resultado formula

                            var periodoMesesDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                            var ingresoTotalDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                            var pagoUnicoDolaresDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                            var recurrenteDolaresDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;

                            var vanDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                            var vanNetoPorIngresosDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                            var costosDirectosDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                            var oIDBADATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                            var oIDBAPercentDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                            var caPexDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                            string paybackDATA_CENTER;
                            try
                            {
                                paybackDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                            }
                            catch
                            {
                                paybackDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                            }


                            var depreciacionDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                            var tirDATA_CENTER = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                            plantillaDataCenterFinanciera = new OportunidadFinancieraOrigen
                            {
                                PeriodoMeses = Convert.ToInt32(periodoMesesDATA_CENTER),
                                IngresoTotal = Convert.ToDecimal(ingresoTotalDATA_CENTER),
                                PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresDATA_CENTER),
                                RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresDATA_CENTER),
                                VanFlujoNeto = Convert.ToDecimal(vanDATA_CENTER),
                                VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosDATA_CENTER) * 100,
                                CostosDirectos = Convert.ToDecimal(costosDirectosDATA_CENTER),
                                Oidba = Convert.ToDecimal(oIDBADATA_CENTER),
                                OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentDATA_CENTER) * 100,
                                Capex = Convert.ToDecimal(caPexDATA_CENTER),
                                Payback = paybackDATA_CENTER.ToString(),
                                Depreciacion = Convert.ToDecimal(depreciacionDATA_CENTER),
                                Tir = Convert.ToDecimal(tirDATA_CENTER) * 100,
                                Tipo = PlantillasOportunidades.CENTRALES
                            };
                        }

                        //Seccion contable
                        var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 120, 160, 10,rutaFichero);


                        return new OportunidadOrigenDto
                        {
                            oportunidadFinanciera = plantillaDataCenterFinanciera,
                            oportunidadContable = listaContable,
                            OportunidadLotusLineas = listaLineasLotus
                        };

                    }
                    else
                    {
                        if (rutaFichero.Contains(PlantillasOportunidades.DATOS))
                        {
                            //Seccion financiera
                            for (var row = 200; row <= 240; row++)
                            {
                                for (var col = 0; col <= 10; col++)
                                {
                                    ICell celdaRaiz;
                                    try
                                    {
                                        celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                        if (celdaRaiz != null && celdaRaiz.ToString().Equals("Ingreso Total"))
                                        {
                                            rowInicial = row;
                                            colInicial = col;
                                            break;
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                if (rowInicial != 0 && colInicial != 0)
                                {
                                    break;
                                }
                            }

                            OportunidadFinancieraOrigen plantillaDatosFinanciera = null;

                            if (rowInicial != 0 && colInicial != 0)
                            {
                                rowInicial = rowInicial--;

                                int? periodoMesesDATOS = null;
                                var ingresoTotalDATOS = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                                decimal? pagoUnicoDolaresDATOS = null;
                                decimal? recurrenteDolaresDATOS = null;
                                decimal? vanDATOS = null;
                                decimal? vanNetoPorIngresosDATOS = null;
                                var costosDirectosDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                                var utilidadOperativaDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                                var margenOperativaDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                                decimal? oIDBADATOS = null;
                                var oIDBAPercentDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                                var caPexDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                                var cantPtosDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                                decimal? paybackDATOS = null;
                                decimal? depreciacionDATOS = null;
                                var tipoCambioDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                                var tirDATOS = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;

                                plantillaDatosFinanciera = new OportunidadFinancieraOrigen
                                {
                                    PeriodoMeses = periodoMesesDATOS,
                                    IngresoTotal = Convert.ToDecimal(ingresoTotalDATOS),
                                    PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresDATOS),
                                    RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresDATOS),
                                    VanFlujoNeto = Convert.ToDecimal(vanDATOS),
                                    VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosDATOS) * 100,
                                    CostosDirectos = Convert.ToDecimal(costosDirectosDATOS),
                                    UtilidadOperativa = Convert.ToDecimal(utilidadOperativaDATOS),
                                    MargenOperativa = Convert.ToDecimal(margenOperativaDATOS) * 100,
                                    Oidba = Convert.ToDecimal(oIDBADATOS),
                                    OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentDATOS) * 100,
                                    Capex = Convert.ToDecimal(caPexDATOS),
                                    CantidadPtos = Convert.ToInt32(cantPtosDATOS),
                                    Payback = paybackDATOS.ToString(),
                                    Depreciacion = Convert.ToDecimal(depreciacionDATOS),
                                    TipoCambio = Convert.ToDecimal(tipoCambioDATOS),
                                    Tir = Convert.ToDecimal(tirDATOS) * 100,
                                    Tipo = PlantillasOportunidades.DATOS
                                };
                            }

                            return new OportunidadOrigenDto
                            {
                                oportunidadFinanciera = plantillaDatosFinanciera,
                                OportunidadLotusLineas = listaLineasLotus
                            };

                        }
                        else
                        {
                            if (rutaFichero.Contains(PlantillasOportunidades.E_LEARNING))
                            {
                                //Seccion financiera
                                for (var row = 165; row <= 200; row++)
                                {
                                    for (var col = 0; col <= 8; col++)
                                    {
                                        ICell celdaRaiz;
                                        try
                                        {
                                            celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                            if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                            {
                                                rowInicial = row;
                                                colInicial = col;
                                                break;
                                            }
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }

                                    if (rowInicial != 0 && colInicial != 0)
                                    {
                                        break;
                                    }
                                }

                                OportunidadFinancieraOrigen plantillaElearningFinanciera = null;

                                if (rowInicial != 0 && colInicial != 0)
                                {

                                    var periodoMesesE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                                    var ingresoTotalE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                                    var pagoUnicoDolaresE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                                    var recurrenteDolaresE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                                    var vanE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                                    var vanNetoPorIngresosE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                                    var costosDirectosE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                                    var oIDBAE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                                    var oIDBAPercentE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                                    var caPexE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                                    string paybackE_LEARNING;
                                    try
                                    {
                                        paybackE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                                    }
                                    catch (Exception)
                                    {
                                        paybackE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                                    }


                                    var depreciacionE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                                    var tirE_LEARNING = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                                    plantillaElearningFinanciera = new OportunidadFinancieraOrigen
                                    {
                                        PeriodoMeses = Convert.ToInt32(periodoMesesE_LEARNING),
                                        IngresoTotal = Convert.ToDecimal(ingresoTotalE_LEARNING),
                                        PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresE_LEARNING),
                                        RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresE_LEARNING),
                                        VanFlujoNeto = Convert.ToDecimal(vanE_LEARNING),
                                        VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosE_LEARNING) * 100,
                                        CostosDirectos = Convert.ToDecimal(costosDirectosE_LEARNING),
                                        Oidba = Convert.ToDecimal(oIDBAE_LEARNING),
                                        OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentE_LEARNING) * 100,
                                        Capex = Convert.ToDecimal(caPexE_LEARNING),
                                        Payback = paybackE_LEARNING,
                                        Depreciacion = Convert.ToDecimal(depreciacionE_LEARNING),
                                        Tir = Convert.ToDecimal(tirE_LEARNING) * 100,
                                        Tipo = PlantillasOportunidades.E_LEARNING
                                    };
                                }

                                //Seccion contable

                                var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 140, 190, 9, rutaFichero);


                                return new OportunidadOrigenDto
                                {
                                    oportunidadFinanciera = plantillaElearningFinanciera,
                                    oportunidadContable = listaContable,
                                    OportunidadLotusLineas = listaLineasLotus
                                };
                            }
                        }

                    }
                }
            }

            return null;
        }

        private OportunidadOrigenDto LecturaOportunidadesGrupo2(string rutaFichero)
        {
            //WorkbookFactory crea un libro en funcion de la version de excel 2003/20007

            using (var file = new FileStream(rutaFichero, FileMode.Open, FileAccess.Read))
            {
                var hssfwb = WorkbookFactory.Create(file);

                //Assign the sheet
                var sheetFC_FINANCIERO = hssfwb.GetSheet("FC_FINANCIERO");
                var sheetFC_CONTABLE = hssfwb.GetSheet("FC_CONTABLE");

                // 1.Plantilla centrales
                var rowInicial = 0;
                var colInicial = 0;
                var colTotalContable = 0;

                var listaLineasLotus = CargaPmoLotus(hssfwb, rutaFichero);

                if (rutaFichero.Contains(PlantillasOportunidades.INFRAESTRUCTURA))
                {
                    //Seccion financiera
                    for (var row = 165; row <= 200; row++)
                    {
                        for (var col = 0; col <= 8; col++)
                        {
                            ICell celdaRaiz;
                            try
                            {
                                celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                {
                                    rowInicial = row;
                                    colInicial = col;
                                    break;
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }

                        if (rowInicial != 0 && colInicial != 0)
                        {
                            break;
                        }
                    }

                    OportunidadFinancieraOrigen plantillaInfraestructuraFinanciera = null;

                    if (rowInicial != 0 && colInicial != 0)
                    {
                        var periodoMesesE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                        var ingresoTotalE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                        var pagoUnicoDolaresE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                        var recurrenteDolaresE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                        var vanE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                        var vanNetoPorIngresosE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                        var costosDirectosE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                        var oIDBAE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                        var oIDBAPercentE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                        var caPexE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                        string paybackE_INFRAESTRUCTURA;
                        try
                        {
                            paybackE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                        }
                        catch (Exception)
                        {
                            paybackE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                        }

                        var depreciacionE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                        var tirE_INFRAESTRUCTURA = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                        plantillaInfraestructuraFinanciera = new OportunidadFinancieraOrigen
                        {
                            PeriodoMeses = Convert.ToInt32(periodoMesesE_INFRAESTRUCTURA),
                            IngresoTotal = Convert.ToDecimal(ingresoTotalE_INFRAESTRUCTURA),
                            PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresE_INFRAESTRUCTURA),
                            RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresE_INFRAESTRUCTURA),
                            VanFlujoNeto = Convert.ToDecimal(vanE_INFRAESTRUCTURA),
                            VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosE_INFRAESTRUCTURA) * 100,
                            CostosDirectos = Convert.ToDecimal(costosDirectosE_INFRAESTRUCTURA),
                            Oidba = Convert.ToDecimal(oIDBAE_INFRAESTRUCTURA),
                            OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentE_INFRAESTRUCTURA),
                            Capex = Convert.ToDecimal(caPexE_INFRAESTRUCTURA),
                            Payback = paybackE_INFRAESTRUCTURA,
                            Depreciacion = Convert.ToDecimal(depreciacionE_INFRAESTRUCTURA),
                            Tir = Convert.ToDecimal(tirE_INFRAESTRUCTURA) * 100,
                            Tipo = PlantillasOportunidades.INFRAESTRUCTURA
                        };
                    }

                    //Seccion contable
                    var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 140, 190, 9, rutaFichero);

                    return new OportunidadOrigenDto
                    {
                        oportunidadFinanciera = plantillaInfraestructuraFinanciera,
                        oportunidadContable = listaContable,
                        OportunidadLotusLineas = listaLineasLotus
                    };
                }
                else
                {
                    if (rutaFichero.Contains(PlantillasOportunidades.OI))
                    {
                        return new OportunidadOrigenDto
                        {
                            OportunidadLotusLineas = listaLineasLotus
                        };
                    }
                    else
                    {
                        if (rutaFichero.Contains(PlantillasOportunidades.RT))
                        {
                            //Seccion financiera
                            for (var row = 165; row <= 200; row++)
                            {
                                for (var col = 0; col <= 8; col++)
                                {
                                    ICell celdaRaiz;
                                    try
                                    {
                                        celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                        if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                        {
                                            rowInicial = row;
                                            colInicial = col;
                                            break;
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                if (rowInicial != 0 && colInicial != 0)
                                {
                                    break;
                                }
                            }

                            OportunidadFinancieraOrigen plantillaRtFinanciera = null;

                            if (rowInicial != 0 && colInicial != 0)
                            {
                                var periodoMesesRT = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                                var ingresoTotalRT = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                                var pagoUnicoDolaresRT = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                                var recurrenteDolaresRT = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                                var vanRT = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                                var vanNetoPorIngresosRT = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                                var costosDirectosRT = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                                var oIDBART = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                                var oIDBAPercentRT = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                                var caPexRT = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                                string paybackRT;
                                try
                                {
                                    paybackRT = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                                }
                                catch (Exception)
                                {
                                    paybackRT = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                                }


                                var depreciacionRT = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                                var tirRT = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                                plantillaRtFinanciera = new OportunidadFinancieraOrigen
                                {
                                    PeriodoMeses = Convert.ToInt32(periodoMesesRT),
                                    IngresoTotal = Convert.ToDecimal(ingresoTotalRT),
                                    PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresRT),
                                    RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresRT),
                                    VanFlujoNeto = Convert.ToDecimal(vanRT),
                                    VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosRT) * 100,
                                    CostosDirectos = Convert.ToDecimal(costosDirectosRT),
                                    Oidba = Convert.ToDecimal(oIDBART),
                                    OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentRT) * 100,
                                    Capex = Convert.ToDecimal(caPexRT),
                                    Payback = paybackRT,
                                    Depreciacion = Convert.ToDecimal(depreciacionRT),
                                    Tir = Convert.ToDecimal(tirRT) * 100,
                                    Tipo = PlantillasOportunidades.RT
                                };
                            }

                            //Seccion contable
                            var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 135, 180, 9, rutaFichero);

                            return new OportunidadOrigenDto
                            {
                                oportunidadFinanciera = plantillaRtFinanciera,
                                oportunidadContable = listaContable,
                                OportunidadLotusLineas = listaLineasLotus
                            };

                        }
                        else
                        {
                            if (rutaFichero.Contains(PlantillasOportunidades.SEGURIDAD))
                            {
                                //Seccion financiera
                                for (var row = 165; row <= 200; row++)
                                {
                                    for (var col = 0; col <= 8; col++)
                                    {
                                        ICell celdaRaiz;
                                        try
                                        {
                                            celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                            if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                            {
                                                rowInicial = row;
                                                colInicial = col;
                                                break;
                                            }
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }

                                    if (rowInicial != 0 && colInicial != 0)
                                    {
                                        break;
                                    }
                                }

                                OportunidadFinancieraOrigen plantillaSeguridadFinanciera = null;

                                if (rowInicial != 0 && colInicial != 0)
                                {
                                    var periodoMeses_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                                    var ingresoTotal_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                                    var pagoUnicoDolares_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                                    var recurrenteDolares_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                                    var van_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                                    var vanNetoPorIngresos_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                                    var costosDirectos_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                                    var oIDBA_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                                    var oIDBAPercent_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                                    var caPex_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;
                                    var depreciacion_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                                    var tir_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                                    string payback_SEGURIDAD;
                                    try
                                    {
                                        payback_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                                    }
                                    catch (Exception)
                                    {
                                        payback_SEGURIDAD = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                                    }

                                    plantillaSeguridadFinanciera = new OportunidadFinancieraOrigen
                                    {
                                        PeriodoMeses = Convert.ToInt32(periodoMeses_SEGURIDAD),
                                        IngresoTotal = Convert.ToDecimal(ingresoTotal_SEGURIDAD),
                                        PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolares_SEGURIDAD),
                                        RecurrenteDolares = Convert.ToDecimal(recurrenteDolares_SEGURIDAD),
                                        VanFlujoNeto = Convert.ToDecimal(van_SEGURIDAD),
                                        VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresos_SEGURIDAD) * 100,
                                        CostosDirectos = Convert.ToDecimal(costosDirectos_SEGURIDAD),
                                        Oidba = Convert.ToDecimal(oIDBA_SEGURIDAD),
                                        OidbaPorcentaje = Convert.ToDecimal(oIDBAPercent_SEGURIDAD),
                                        Capex = Convert.ToDecimal(caPex_SEGURIDAD),
                                        Payback = payback_SEGURIDAD,
                                        Depreciacion = Convert.ToDecimal(depreciacion_SEGURIDAD),
                                        Tir = Convert.ToDecimal(tir_SEGURIDAD) * 100,
                                        Tipo = PlantillasOportunidades.SEGURIDAD
                                    };
                                }

                                //Seccion contable
                                var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 140, 180, 9, rutaFichero);

                                return new OportunidadOrigenDto
                                {
                                    oportunidadFinanciera = plantillaSeguridadFinanciera,
                                    oportunidadContable = listaContable,
                                    OportunidadLotusLineas = listaLineasLotus
                                };

                            }
                        }
                    }
                }
            }
           
            return null;
        }

        private OportunidadOrigenDto LecturaOportunidadesGrupo3(string rutaFichero)
        {
            using (var file = new FileStream(rutaFichero, FileMode.Open, FileAccess.Read))
            {
                //WorkbookFactory crea un libro en funcion de la version de excel 2003/20007
                var hssfwb = WorkbookFactory.Create(file);

                //Assign the sheet
                var sheetFC_FINANCIERO = hssfwb.GetSheet("FC_FINANCIERO");
                var sheetFC_CONTABLE = hssfwb.GetSheet("FC_CONTABLE");

                // 1.Plantilla centrales
                var rowInicial = 0;
                var colInicial = 0;
                var colTotalContable = 0;

                var listaLineasLotus = CargaPmoLotus(hssfwb, rutaFichero);

                if (rutaFichero.Contains(PlantillasOportunidades.TIS))
                {
                    //Seccion financiera
                    for (var row = 210; row <= 240; row++)
                    {
                        for (var col = 6; col <= 8; col++)
                        {
                            ICell celdaRaiz;
                            try
                            {
                                celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                {
                                    rowInicial = row;
                                    colInicial = col;
                                    break;
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }

                        if (rowInicial != 0 && colInicial != 0)
                        {
                            break;
                        }
                    }

                    OportunidadFinancieraOrigen plantillaTisFinanciera = null;

                    if (rowInicial != 0 && colInicial != 0)
                    {
                        var periodoMeses_TIS = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                        var ingresoTotal_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                        var pagoUnicoDolares_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                        var recurrenteDolares_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                        var van_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                        var vanNetoPorIngresos_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                        var costosDirectos_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                        var oIDBA_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                        var oIDBAPercent_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                        var caPex_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                        string payback_TIS;
                        try
                        {
                            payback_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                        }
                        catch (Exception)
                        {
                            payback_TIS = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                        }


                        decimal? depreciacion_TIS = null;
                        decimal? tir_TIS = null;

                        plantillaTisFinanciera = new OportunidadFinancieraOrigen
                        {
                            PeriodoMeses = Convert.ToInt32(periodoMeses_TIS),
                            IngresoTotal = Convert.ToDecimal(ingresoTotal_TIS),
                            PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolares_TIS),
                            RecurrenteDolares = Convert.ToDecimal(recurrenteDolares_TIS),
                            VanFlujoNeto = Convert.ToDecimal(van_TIS),
                            VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresos_TIS),
                            CostosDirectos = Convert.ToDecimal(costosDirectos_TIS),
                            Oidba = Convert.ToDecimal(oIDBA_TIS),
                            OidbaPorcentaje = Convert.ToDecimal(oIDBAPercent_TIS) * 100,
                            Capex = Convert.ToDecimal(caPex_TIS),
                            Payback = payback_TIS,
                            Depreciacion = depreciacion_TIS,
                            Tir = tir_TIS,
                            Tipo = PlantillasOportunidades.TIS
                        };
                    }

                    return new OportunidadOrigenDto
                    {
                        oportunidadFinanciera = plantillaTisFinanciera,
                        OportunidadLotusLineas = listaLineasLotus
                    };

                }
                else
                {
                    if (rutaFichero.Contains(PlantillasOportunidades.TI_SERVIDORES))
                    {
                        //Seccion financiera
                        for (var row = 165; row <= 210; row++)
                        {
                            for (var col = 0; col <= 8; col++)
                            {
                                ICell celdaRaiz;
                                try
                                {
                                    celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                    if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                    {
                                        rowInicial = row;
                                        colInicial = col;
                                        break;
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }

                            if (rowInicial != 0 && colInicial != 0)
                            {
                                break;
                            }
                        }

                        OportunidadFinancieraOrigen plantillaTI_SERVIDORESFinanciera = null;

                        if (rowInicial != 0 && colInicial != 0)
                        {
                            var periodoMesesTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                            var ingresoTotalTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                            var pagoUnicoDolaresTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                            var recurrenteDolaresTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                            var vanTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                            var vanNetoPorIngresosTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                            var costosDirectosTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                            var oIDBATI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                            var oIDBAPercentTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                            var caPexTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                            string paybackTI_SERVIDORES;
                            try
                            {
                                paybackTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                            }
                            catch (Exception)
                            {
                                paybackTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                            }


                            var depreciacionTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                            var tirTI_SERVIDORES = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                            plantillaTI_SERVIDORESFinanciera = new OportunidadFinancieraOrigen
                            {
                                PeriodoMeses = Convert.ToInt32(periodoMesesTI_SERVIDORES),
                                IngresoTotal = Convert.ToDecimal(ingresoTotalTI_SERVIDORES),
                                PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolaresTI_SERVIDORES),
                                RecurrenteDolares = Convert.ToDecimal(recurrenteDolaresTI_SERVIDORES),
                                VanFlujoNeto = Convert.ToDecimal(vanTI_SERVIDORES),
                                VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresosTI_SERVIDORES) * 100,
                                CostosDirectos = Convert.ToDecimal(costosDirectosTI_SERVIDORES),
                                Oidba = Convert.ToDecimal(oIDBATI_SERVIDORES),
                                OidbaPorcentaje = Convert.ToDecimal(oIDBAPercentTI_SERVIDORES),
                                Capex = Convert.ToDecimal(caPexTI_SERVIDORES),
                                Payback = paybackTI_SERVIDORES,
                                Depreciacion = Convert.ToDecimal(depreciacionTI_SERVIDORES),
                                Tir = Convert.ToDecimal(tirTI_SERVIDORES) * 100,
                                Tipo = PlantillasOportunidades.TI_SERVIDORES
                            };

                        }


                        //Seccion contable
                        var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 140, 180, 9, rutaFichero);

                        return new OportunidadOrigenDto
                        {
                            oportunidadFinanciera = plantillaTI_SERVIDORESFinanciera,
                            oportunidadContable = listaContable,
                            OportunidadLotusLineas = listaLineasLotus
                        };

                    }
                    else
                    {
                        if (rutaFichero.Contains(PlantillasOportunidades.TX))
                        {
                            //Seccion financiera
                            for (var row = 162; row <= 200; row++)
                            {
                                for (var col = 0; col <= 8; col++)
                                {
                                    ICell celdaRaiz;
                                    try
                                    {
                                        celdaRaiz = sheetFC_FINANCIERO.GetRow(row).GetCell(col);
                                        if (celdaRaiz != null && celdaRaiz.ToString().Equals("Periodo - Meses"))
                                        {
                                            rowInicial = row;
                                            colInicial = col;
                                            break;
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                if (rowInicial != 0 && colInicial != 0)
                                {
                                    break;
                                }
                            }

                            OportunidadFinancieraOrigen plantillaTxFinanciera = null;

                            if (rowInicial != 0 && colInicial != 0)
                            {
                                var periodoMeses_TX = sheetFC_FINANCIERO.GetRow(rowInicial).GetCell(colInicial + 1).NumericCellValue;
                                var ingresoTotal_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 1).GetCell(colInicial + 1).NumericCellValue;
                                var pagoUnicoDolares_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 2).GetCell(colInicial + 1).NumericCellValue;
                                var recurrenteDolares_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 3).GetCell(colInicial + 1).NumericCellValue;
                                var van_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 4).GetCell(colInicial + 1).NumericCellValue;
                                var vanNetoPorIngresos_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 5).GetCell(colInicial + 1).NumericCellValue;
                                var costosDirectos_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 6).GetCell(colInicial + 1).NumericCellValue;
                                var oIDBA_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 7).GetCell(colInicial + 1).NumericCellValue;
                                var oIDBAPercent_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 8).GetCell(colInicial + 1).NumericCellValue;
                                var caPex_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 9).GetCell(colInicial + 1).NumericCellValue;

                                string payback_TX;
                                try
                                {
                                    payback_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).StringCellValue;
                                }
                                catch (Exception)
                                {
                                    payback_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 10).GetCell(colInicial + 1).NumericCellValue.ToString();
                                }

                                var depreciacion_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 11).GetCell(colInicial + 1).NumericCellValue;
                                var tir_TX = sheetFC_FINANCIERO.GetRow(rowInicial + 12).GetCell(colInicial + 1).NumericCellValue;

                                plantillaTxFinanciera = new OportunidadFinancieraOrigen
                                {
                                    PeriodoMeses = Convert.ToInt32(periodoMeses_TX),
                                    IngresoTotal = Convert.ToDecimal(ingresoTotal_TX),
                                    PagoUnicoDolares = Convert.ToDecimal(pagoUnicoDolares_TX),
                                    RecurrenteDolares = Convert.ToDecimal(recurrenteDolares_TX),
                                    VanFlujoNeto = Convert.ToDecimal(van_TX),
                                    VanFlujoNetoPorVanIngresos = Convert.ToDecimal(vanNetoPorIngresos_TX) * 100,
                                    CostosDirectos = Convert.ToDecimal(costosDirectos_TX),
                                    Oidba = Convert.ToDecimal(oIDBA_TX),
                                    OidbaPorcentaje = Convert.ToDecimal(oIDBAPercent_TX),
                                    Capex = Convert.ToDecimal(caPex_TX),
                                    Payback = payback_TX,
                                    Depreciacion = Convert.ToDecimal(depreciacion_TX),
                                    Tir = Convert.ToDecimal(tir_TX) * 100,
                                    Tipo = PlantillasOportunidades.TX
                                };
                            }

                            //Seccion contable

                            var listaContable = GetOportunidadesContables(sheetFC_CONTABLE, 140, 180, 9, rutaFichero);

                            return new OportunidadOrigenDto
                            {
                                oportunidadFinanciera = plantillaTxFinanciera,
                                oportunidadContable = listaContable,
                                OportunidadLotusLineas = listaLineasLotus
                            };
                        }
                        else
                        {
                            if (rutaFichero.Contains(PlantillasOportunidades.VOZ))
                            {

                            }
                        }
                    }
                }
            } 

            return null;
        }

        private ParametrosArchivosOportunidadDto GetPerNumeroCaso(string fichero)
        {
            var structuraFichero = fichero.Replace("_PER","-PER").Split('-');
            var positionPer = Array.FindIndex(structuraFichero, row => row.Trim() == "PER");
            if (positionPer.Equals(-1))
                return new ParametrosArchivosOportunidadDto { IdOportunidad = null, NumeroCaso = null};

            var idOportunidad = string.Concat(structuraFichero[positionPer],"-",structuraFichero[positionPer + 1]);
            int numCasoExep;
            var numeroCaso = int.TryParse(structuraFichero[positionPer + 2], out numCasoExep) ? 
                                structuraFichero[positionPer + 2].Trim() : null;

            return new ParametrosArchivosOportunidadDto { IdOportunidad = idOportunidad, NumeroCaso = numeroCaso };
        }

        #endregion Elegir plantillas

        #region Cargar info de plantillas

        private async Task CargarInfoPlantillas(List<OportunidadOrigenDto> plantillas,string login,int idCarga,SqlConnection cnOportunidad)
        {
            foreach (var item in plantillas)
            {
                if (item == null)
                    continue;

                if (item.oportunidadFinanciera != null)
                {
                    item.oportunidadFinanciera.Insertar(login, DateTime.Now, idCarga);
                    await _oportunidadFinancieraOrigenRepositorio.OportunidadFinancieraOrigenInsert(item.oportunidadFinanciera, cnOportunidad);
                }

                if (item.oportunidadContable != null)
                {
                    foreach (var contable in item.oportunidadContable)
                    {
                        contable.Insertar(login, DateTime.Now, idCarga);
                        await _oportunidadContableOrigenRepositorio.OportunidadContableOrigenInsert(contable, cnOportunidad);
                    }
                }

                if (item.OportunidadLotusLineas == null) continue;

                foreach (var line in item.OportunidadLotusLineas)
                {
                    line.Insertar(login,DateTime.Now, idCarga);
                    await _oportunidadPmoLotusLineaInsertRepositorio.OportunidadPmoLotusLineaInsert(line, cnOportunidad);
                }
            }                       
        }
       
        #endregion Cargar info de plantillas

    }
}

