﻿using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Application.MainContext.Oportunidades.AppServices.Dto
{
    public class OportunidadOrigenDto
    {
        public OportunidadFinancieraOrigen oportunidadFinanciera { get; set; }

        public List<OportunidadContableOrigen> oportunidadContable { get; set; }

        public List<OportunidadPmoLotusLinea> OportunidadLotusLineas { get; set; }

    }
}

