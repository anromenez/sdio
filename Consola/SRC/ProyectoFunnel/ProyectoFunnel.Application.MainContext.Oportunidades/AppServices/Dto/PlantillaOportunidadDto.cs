﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Application.MainContext.Oportunidades.AppServices.Dto
{
    public class PlantillaOportunidadDto
    {
        public string ruta { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get { return new DateTime(
                                                       Convert.ToInt32("20" + Nombre.Substring(7, 2)),
                                                       Convert.ToInt32(Nombre.Substring(4, 2)),
                                                       Convert.ToInt32(Nombre.Substring(1, 2))
                                                       );
                            } set { } }
        public int? NumeroCaso { get { return Convert.ToInt32(Nombre.Substring(Nombre.IndexOf("PER")).Split('-')[2].ToString().Trim()) ;} set { } }
    }
}
