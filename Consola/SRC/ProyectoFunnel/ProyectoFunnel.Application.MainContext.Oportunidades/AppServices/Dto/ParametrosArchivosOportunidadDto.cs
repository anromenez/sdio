﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Application.MainContext.Oportunidades.AppServices.Dto
{
    public class ParametrosArchivosOportunidadDto
    {
        public string IdOportunidad { get; set; }

        public string NumeroCaso { get; set; }
    }
}
