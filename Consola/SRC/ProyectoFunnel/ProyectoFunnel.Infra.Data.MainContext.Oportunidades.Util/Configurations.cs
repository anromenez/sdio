﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Infra.Oportunidades.Util
{
    public static class Configurations
    {
        public static string ConnectionFunnel { get { return ConfigurationManager.ConnectionStrings["dbFunnel"].ConnectionString.ToString(); } }

    }
}
