﻿using ProyectoFunnel.Application.MainContext.Oportunidades.AppServices.Implementation;
using System.Threading.Tasks;

namespace ProyectoFunnel.Service.MainContext.Oportunidades.Implementations
{
    public partial class OportunidadesService
    {
        private readonly OportunidadesAppService _oportunidadesAppService;
        public OportunidadesService()
        {
            _oportunidadesAppService = new OportunidadesAppService();
        }

        public async Task ImportarOportunidadesAsync(string rutaFolder, string login)
        {
            await _oportunidadesAppService.ImportarOportunidadesAsync(rutaFolder, login);
        }
    }
}

