﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using ProyectoFunnel.Domain.MainContext.SalesForce.SalesForceAgg;
using System.Configuration;
using ProyectoFunnel.Infra.Data.MainContext.SalesForce;

namespace ProyectoFunnel.Infra.Data.MainContext.SalesForce.Repositories
{
    public class SalesForceOrigenRepositorio
    {
        public async Task<int> CargarSalesForceBeginOrigen(SalesForceOrigen request,SqlConnection cnFunnel)
        {
            using (var cmd = new SqlCommand("Comun.usp_SalesForceOrigenBeginInsert", cnFunnel))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FechaCarga", request.FechaCarga);
                cmd.Parameters.Add("@Id",System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;
                await cmd.ExecuteNonQueryAsync();

                return Convert.ToInt32(cmd.Parameters["@Id"].Value); 
            }
        }
        public async Task CargarSalesForceOrigen(SalesForceOrigen request, SqlConnection cnFunnel)
        {
            using (var cmd = new SqlCommand("Comun.usp_SalesForceOrigenInsert", cnFunnel))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdOportunidad", request.IdOportunidad);
                cmd.Parameters.AddWithValue("@PropietarioOportunidad", request.PropietarioOportunidad);
                cmd.Parameters.AddWithValue("@TipologiaOportunidad", request.TipologiaOportunidad);
                cmd.Parameters.AddWithValue("@NombreCliente", request.NombreCliente);
                cmd.Parameters.AddWithValue("@NombreOportunidad", request.NombreOportunidad);
                cmd.Parameters.AddWithValue("@FechaCreacion", request.FechaCreacion);
                cmd.Parameters.AddWithValue("@FechaCierreEstimada", request.FechaCierreEstimada);
                cmd.Parameters.AddWithValue("@FechaCierreReal", request.FechaCierreReal);
                cmd.Parameters.AddWithValue("@ProbabilidadExito", request.ProbabilidadExito);
                cmd.Parameters.AddWithValue("@Etapa", request.Etapa);
                cmd.Parameters.AddWithValue("@TipoOportunidad", request.TipoOportunidad);
                cmd.Parameters.AddWithValue("@PlazoEstimadoProvision", request.PlazoEstimadoProvision);
                cmd.Parameters.AddWithValue("@FechaEstimadaInstalacionServicio", request.FechaEstimadaInstalacionServicio);
                cmd.Parameters.AddWithValue("@DuracionContrato", request.DuracionContrato);
                cmd.Parameters.AddWithValue("@IngresoPorUnicaVezDivisa", request.IngresoPorUnicaVezDivisa);
                cmd.Parameters.AddWithValue("@IngresoPorUnicaVez", request.IngresoPorUnicaVez);
                cmd.Parameters.AddWithValue("@FullContractValueNetoDivisa", request.FullContractValueNetoDivisa);
                cmd.Parameters.AddWithValue("@FullContractValueNeto", request.FullContractValueNeto);
                cmd.Parameters.AddWithValue("@RecurrenteBrutoMensualDivisa", request.RecurrenteBrutoMensualDivisa);
                cmd.Parameters.AddWithValue("@RecurrenteBrutoMensual", request.RecurrenteBrutoMensual);
                cmd.Parameters.AddWithValue("@NumeroDelCaso", request.NumeroDelCaso);
                cmd.Parameters.AddWithValue("@Estado", request.Estado);
                cmd.Parameters.AddWithValue("@Departamento", request.Departamento);
                cmd.Parameters.AddWithValue("@TipoSolicitud", request.TipoSolicitud);
                cmd.Parameters.AddWithValue("@Asunto", request.Asunto);
                cmd.Parameters.AddWithValue("@FechaCarga", request.FechaCarga);
                cmd.Parameters.AddWithValue("@LoginCarga", request.LoginCarga);
                cmd.Parameters.AddWithValue("@IdCarga", request.IdCarga);

                await cmd.ExecuteNonQueryAsync();
            }
        }

    }
}
