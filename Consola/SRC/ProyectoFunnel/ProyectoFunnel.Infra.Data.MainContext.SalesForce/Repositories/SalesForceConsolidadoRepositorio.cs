﻿using ProyectoFunnel.Domain.MainContext.SalesForce.SalesForceAgg;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoFunnel.Infra.SalesForce.Util;

namespace ProyectoFunnel.Infra.Data.MainContext.SalesForce.Repositories
{
    public class SalesForceConsolidadoRepositorio
    {
        public async Task ConsolidarSalesForce(SalesForceOrigen request,SqlConnection cnFunnel)
        {
            using (var cmd = new SqlCommand("Comun.usp_SalesForceConsolidar", cnFunnel))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 60;
                    cmd.Parameters.AddWithValue("@IdCarga", request.IdCarga);
                    cmd.Parameters.AddWithValue("@LoginCarga", request.LoginCarga);

                    await cmd.ExecuteNonQueryAsync();

                }
        }
    }
}
