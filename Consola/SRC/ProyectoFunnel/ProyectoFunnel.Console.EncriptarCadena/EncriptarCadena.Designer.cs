﻿namespace ProyectoFunnel.Console.EncriptarCadena
{
    partial class EncriptarCadena
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btmEncriptar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(44, 43);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(463, 20);
            this.textBox1.TabIndex = 0;
            // 
            // btmEncriptar
            // 
            this.btmEncriptar.Location = new System.Drawing.Point(194, 85);
            this.btmEncriptar.Name = "btmEncriptar";
            this.btmEncriptar.Size = new System.Drawing.Size(140, 23);
            this.btmEncriptar.TabIndex = 1;
            this.btmEncriptar.Text = "Encriptar/Desencriptar";
            this.btmEncriptar.UseVisualStyleBackColor = true;
            this.btmEncriptar.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(409, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ingrese ruta del ejecutable de la aplicación. Ejemplo: \r\nC:\\Users\\nabellanedar\\De" +
    "sktop\\Debug\\ProyectoFunnel.Console.Oportunidades.exe\r\n";
            // 
            // EncriptarCadena
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 131);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btmEncriptar);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.Name = "EncriptarCadena";
            this.Text = "Encriptar conexión a DB";
            this.Load += new System.EventHandler(this.EncriptarCadena_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btmEncriptar;
        private System.Windows.Forms.Label label1;
    }
}

