﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoFunnel.Console.EncriptarCadena.Recursos;

namespace ProyectoFunnel.Console.EncriptarCadena
{
    public partial class EncriptarCadena : Form
    {
        public EncriptarCadena()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ToggleConfigEncryption(textBox1.Text);
        }

        static void ToggleConfigEncryption(string exeConfigName)
        {
            // Takes the executable file name without the
            // .config extension.
            try
            {
                // Open the configuration file and retrieve 
                // the connectionStrings section.
                var config = ConfigurationManager.
                    OpenExeConfiguration(exeConfigName);

                var section =
                    config.GetSection("connectionStrings")
                        as ConnectionStringsSection;

                if (section.SectionInformation.IsProtected)
                {
                    // Remove encryption.
                    section.SectionInformation.UnprotectSection();
                }
                else
                {
                    // Encrypt the section.
                    section.SectionInformation.ProtectSection(
                        "DataProtectionConfigurationProvider");
                }
                // Save the current configuration.
                config.Save();

                MessageBox.Show(Mensajes.EncriptarExito + exeConfigName,Titulos.EncriptarTituloMensaje,
                                MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show(Mensajes.EncriptarFallo + exeConfigName, Titulos.EncriptarTituloMensaje,
                                MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void EncriptarCadena_Load(object sender, EventArgs e)
        {

        }
    }
}
