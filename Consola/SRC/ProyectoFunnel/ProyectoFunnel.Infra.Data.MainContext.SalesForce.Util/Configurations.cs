﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ProyectoFunnel.Infra.SalesForce.Util
{
    public static class Configurations
    {
        public static string ConnectionFunnel { get { return ConfigurationManager.ConnectionStrings["dbFunnel"].ConnectionString.ToString(); } }
    }
}
