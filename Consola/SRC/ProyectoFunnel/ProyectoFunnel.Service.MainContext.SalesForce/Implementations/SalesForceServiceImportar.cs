﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoFunnel.Application.MainContext.SalesForce.AppServices.Implementation;

namespace ProyectoFunnel.Service.MainContext.SalesForce.Implementations
{
    public partial class SalesForceService
    {

        private readonly SalesForceAppService _salesForceAppService;

        public SalesForceService()
        {
            _salesForceAppService = new SalesForceAppService();
        }
        public async Task ImportarReporteSalesForceSAsync(string rutaFichero, string login)
        {
            await _salesForceAppService.ImportarReporteSalesForceSAsync(rutaFichero, login);
        }

    }
}
