﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories
{
    public class SalesForceConsolidadoCabeceraRepositorio
    {
        public bool SalesForceVigente(SqlConnection cnFunnel, string idOportunidad)
        {
            using (var cmd = new SqlCommand("Comun.usp_SalesForceVigente", cnFunnel))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdOportunidad", idOportunidad);
                cmd.Parameters.Add("@existe", System.Data.SqlDbType.Bit).Direction = System.Data.ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                return Convert.ToBoolean(cmd.Parameters["@existe"].Value);
            }
        }
    }
}

