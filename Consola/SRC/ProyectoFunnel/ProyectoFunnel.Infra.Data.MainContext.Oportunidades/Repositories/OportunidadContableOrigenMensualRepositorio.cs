﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;

namespace ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories
{
    public class OportunidadContableOrigenMensualRepositorio
    {
        public async Task OportunidadContableOrigenMensualInsert(OportunidadContableOrigenMensual request, SqlConnection cnOportunidad)
        {
            using (var cmd = new SqlCommand("Oportunidad.usp_OportunidadContableOrigenMensualInsert", cnOportunidad))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdOportunidad", request.IdOportunidad);
                cmd.Parameters.AddWithValue("@NumeroCaso", request.NumeroCaso);
                cmd.Parameters.AddWithValue("@Anio", request.Anio);
                cmd.Parameters.AddWithValue("@Mes", request.Anio);
                cmd.Parameters.AddWithValue("@IngresoAnual", request.IngresoAnual);
                cmd.Parameters.AddWithValue("@OibdaAnual", request.OibdaAnual);
                cmd.Parameters.AddWithValue("@Capex", request.Capex);

                await cmd.ExecuteNonQueryAsync();
            }
        }

    }
}
