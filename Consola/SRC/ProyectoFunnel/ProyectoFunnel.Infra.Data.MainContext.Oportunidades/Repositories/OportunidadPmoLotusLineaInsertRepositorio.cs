﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;

namespace ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories
{
    public class OportunidadPmoLotusLineaInsertRepositorio
    {
        public async Task OportunidadPmoLotusLineaInsert(OportunidadPmoLotusLinea request, SqlConnection cnOportunidad)
        {
            using (var cmd = new SqlCommand("FUNNEL.usp_OportunidadPmoLotusLineaInsert", cnOportunidad))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdOportunidad", request.IdOportunidad);
                cmd.Parameters.AddWithValue("@NumeroCaso", request.NumeroCaso);
                cmd.Parameters.AddWithValue("@Proceso", request.Proceso);
                cmd.Parameters.AddWithValue("@DatosProceso", request.DatosProceso);
                cmd.Parameters.AddWithValue("@DetalleProceso", request.DetalleProceso);
                cmd.Parameters.AddWithValue("@IdEstado", request.IdEstado);
                cmd.Parameters.AddWithValue("@FechaCarga", request.FechaCarga);
                cmd.Parameters.AddWithValue("@IdCarga", request.IdCarga);
                cmd.Parameters.AddWithValue("@LoginCarga", request.LoginCarga);
                cmd.Parameters.AddWithValue("@Tipo", Convert.ToInt32(request.TipoHoja));
                cmd.Parameters.AddWithValue("@Item", request.Item);

                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}

