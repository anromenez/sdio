﻿using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;
using ProyectoFunnel.Infra.Oportunidades.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories
{
    public class OportunidadFinancieraOrigenRepositorio
    {
        public async Task OportunidadFinancieraOrigenInsert(OportunidadFinancieraOrigen request,SqlConnection cnOportunidad)
        {
            using (var cmd = new SqlCommand("FUNNEL.usp_OportunidadFinancieraOrigenInsert", cnOportunidad))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdOportunidad", request.IdOportunidad);
                cmd.Parameters.AddWithValue("@NumeroCaso", request.NumeroCaso);
                cmd.Parameters.AddWithValue("@PeriodoMeses", request.PeriodoMeses);
                cmd.Parameters.AddWithValue("@IngresoTotal", request.IngresoTotal);
                cmd.Parameters.AddWithValue("@PagoUnicoDolares", request.PagoUnicoDolares);
                cmd.Parameters.AddWithValue("@RecurrenteDolares", request.RecurrenteDolares);
                cmd.Parameters.AddWithValue("@VanFlujoNeto", request.VanFlujoNeto);
                cmd.Parameters.AddWithValue("@VanFlujoNetoPorVanIngresos", request.VanFlujoNetoPorVanIngresos);
                cmd.Parameters.AddWithValue("@UtilidadOperativa", request.UtilidadOperativa);
                cmd.Parameters.AddWithValue("@CostosDirectos", request.CostosDirectos);
                cmd.Parameters.AddWithValue("@MargenOperativa", request.MargenOperativa);
                cmd.Parameters.AddWithValue("@CantidadPtos", request.CantidadPtos);
                cmd.Parameters.AddWithValue("@Oidba", request.Oidba);
                cmd.Parameters.AddWithValue("@OidbaPorcentaje", request.OidbaPorcentaje);
                cmd.Parameters.AddWithValue("@Capex", request.Capex);
                cmd.Parameters.AddWithValue("@Payback", request.Payback);
                cmd.Parameters.AddWithValue("@Depreciacion", request.Depreciacion);
                cmd.Parameters.AddWithValue("@TipoCambio", request.TipoCambio);
                cmd.Parameters.AddWithValue("@Tir", request.Tir);
                cmd.Parameters.AddWithValue("@FechaCarga", request.FechaCarga);
                cmd.Parameters.AddWithValue("@LoginCarga", request.LoginCarga);
                cmd.Parameters.AddWithValue("@IdCarga", request.IdCarga);

                await cmd.ExecuteNonQueryAsync();

            }
        }


    }
}

