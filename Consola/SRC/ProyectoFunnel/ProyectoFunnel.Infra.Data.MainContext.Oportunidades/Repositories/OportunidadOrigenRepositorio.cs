﻿using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories
{
    public class OportunidadOrigenRepositorio
    {
        public async Task<int> OportunidadBeginOrigen(OportunidadFinancieraOrigen request, SqlConnection cnFunnel)
        {
            using (var cmd = new SqlCommand("FUNNEL.usp_OportunidadOrigenBeginInsert", cnFunnel))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FechaCarga", request.FechaCarga);
                cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;
                
                await cmd.ExecuteNonQueryAsync();
               
                return Convert.ToInt32(cmd.Parameters["@Id"].Value);

            }
        }

    }
}
