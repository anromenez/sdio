﻿using ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades;
using ProyectoFunnel.Infra.Oportunidades.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Infra.Data.MainContext.Oportunidades.Repositories
{
    public class OportunidadContableOrigenRepositorio
    {
        public async Task OportunidadContableOrigenInsert(OportunidadContableOrigen request,SqlConnection cnOportunidad)
        {
            using (var cmd = new SqlCommand("FUNNEL.usp_OportunidadContableOrigenInsert", cnOportunidad))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdOportunidad", request.IdOportunidad);
                cmd.Parameters.AddWithValue("@NumeroCaso", request.NumeroCaso);
                cmd.Parameters.AddWithValue("@IngresoAnual", request.IngresoAnual);
                cmd.Parameters.AddWithValue("@OibdaAnual", request.OibdaAnual);
                cmd.Parameters.AddWithValue("@Capex", request.Capex);
                cmd.Parameters.AddWithValue("@OibdaPorcentaje", request.OibdaPorcentaje);
                cmd.Parameters.AddWithValue("@VanProyecto", request.VanProyecto);
                cmd.Parameters.AddWithValue("@VanPorVai", request.VanPorVai);
                cmd.Parameters.AddWithValue("@PayBack", request.PayBack);
                cmd.Parameters.AddWithValue("@ValorRescate", request.ValorRescate);
                cmd.Parameters.AddWithValue("@IngresoTotal", request.IngresoTotal);
                cmd.Parameters.AddWithValue("@CostosDirectos", request.CostosDirectos);
                cmd.Parameters.AddWithValue("@UtilidadOperativa", request.UtilidadOperativa);
                cmd.Parameters.AddWithValue("@MargenOperativo", request.MargenOperativo);
                cmd.Parameters.AddWithValue("@NroCotizacion", request.NroCotizacion);
                cmd.Parameters.AddWithValue("@CantPtos", request.CantPtos);
                cmd.Parameters.AddWithValue("@TipoCambio", request.TipoCambio);
                cmd.Parameters.AddWithValue("@Anio", request.Anio);
                cmd.Parameters.AddWithValue("@FechaCarga", request.FechaCarga);
                cmd.Parameters.AddWithValue("@LoginCarga", request.LoginCarga);
                cmd.Parameters.AddWithValue("@IdCarga", request.IdCarga);
                cmd.Parameters.AddWithValue("@TipoCambioIngresos", request.TipoCambioIngresos);
                cmd.Parameters.AddWithValue("@TipoCambioCostos", request.TipoCambioCostos);
                cmd.Parameters.AddWithValue("@TipoCambioCapex", request.TipoCambioCapex);
                cmd.Parameters.AddWithValue("@RutaCarga", request.RutaCarga);

                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}

