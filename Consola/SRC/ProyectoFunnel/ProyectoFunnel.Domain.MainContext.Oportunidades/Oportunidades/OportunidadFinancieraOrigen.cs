﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades
{
    public class OportunidadFinancieraOrigen
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OportunidadFinancieraOrigen()
        {
                
        }

        public int Id { get; set; }
        public string IdOportunidad { get; set; }
        public string NumeroCaso { get; set; }
        public Nullable<int> PeriodoMeses { get; set; }
        public Nullable<decimal> IngresoTotal { get; set; }
        public Nullable<decimal> PagoUnicoDolares { get; set; }
        public Nullable<decimal> RecurrenteDolares { get; set; }
        public Nullable<decimal> VanFlujoNeto { get; set; }
        public Nullable<decimal> VanFlujoNetoPorVanIngresos { get; set; }
        public Nullable<decimal> UtilidadOperativa { get; set; }
        public Nullable<decimal> CostosDirectos { get; set; }
        public Nullable<decimal> MargenOperativa { get; set; }
        public Nullable<int> CantidadPtos { get; set; }
        public Nullable<decimal> Oidba { get; set; }
        public Nullable<decimal> OidbaPorcentaje { get; set; }
        public Nullable<decimal> Capex { get; set; }
        public string Payback { get; set; }
        public Nullable<decimal> Depreciacion { get; set; }
        public Nullable<decimal> TipoCambio { get; set; }
        public Nullable<decimal> Tir { get; set; }
        public Nullable<System.DateTime> FechaCarga { get; set; }
        public string LoginCarga { get; set; }
        public Nullable<int> IdCarga { get; set; }
        public string Tipo { get; set; }

        public void Insertar(string Login, DateTime _fechaCarga, int _idCarga)
        {
            if (string.IsNullOrWhiteSpace(Login))
                throw new ArgumentNullException("LoginCarga");

            LoginCarga = Login;
            FechaCarga = _fechaCarga;
            IdCarga = _idCarga;
        }
    }
}
