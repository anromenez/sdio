﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades
{
    public class OportunidadContableOrigen
    {
        public int Id { get; set; }

        public string IdOportunidad { get; set; }
        public string NumeroCaso { get; set; }
        public decimal? IngresoAnual { get; set; }
        public decimal? OibdaAnual { get; set; }
        public decimal? Capex { get; set; }
        public decimal? OibdaPorcentaje { get; set; }
        public decimal? VanProyecto { get; set; }
        public decimal? VanPorVai { get; set; }
        public string PayBack { get; set; }
        public decimal? ValorRescate { get; set; }
        public decimal? IngresoTotal { get; set; }
        public decimal? CostosDirectos { get; set; }
        public decimal? UtilidadOperativa { get; set; }
        public decimal? MargenOperativo { get; set; }
        public int? NroCotizacion { get; set; }
        public int? CantPtos { get; set; }
        public decimal? TipoCambio { get; set; }
        public int? Anio { get; set; }
        public DateTime? FechaCarga { get; set; }
        public string LoginCarga { get; set; }
        public int? IdCarga { get; set; }
        public decimal? TipoCambioIngresos { get; set; }
        public decimal? TipoCambioCostos { get; set; }
        public decimal? TipoCambioCapex { get; set; }
        public string RutaCarga { get; set; }
        public void Insertar(string Login, DateTime _fechaCarga, int _idCarga)
        {
            if (string.IsNullOrWhiteSpace(Login))
                throw new ArgumentNullException("LoginCarga");

            LoginCarga = Login;
            FechaCarga = _fechaCarga;
            IdCarga = _idCarga;
        }
    }
}

