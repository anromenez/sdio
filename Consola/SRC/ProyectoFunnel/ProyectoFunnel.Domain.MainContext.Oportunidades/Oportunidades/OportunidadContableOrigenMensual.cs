﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades
{
    public class OportunidadContableOrigenMensual
    {
        public int Id { get; set; }

        public string IdOportunidad { get; set; }
        public string NumeroCaso { get; set; }
        public decimal? IngresoAnual { get; set; }
        public decimal? OibdaAnual { get; set; }
        public decimal? Capex { get; set; }
        public int? Anio { get; set; }
        public int? Mes { get; set; }

    }
}
