﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFunnel.Domain.MainContext.Oportunidades.Oportunidades
{
    public class OportunidadPmoLotusLinea
    {
        public int Id { get; set; }
        public string IdOportunidad { get; set; }
        public string NumeroCaso { get; set; }
        public string Proceso { get; set; }
        public string DatosProceso { get; set; }
        public string DetalleProceso { get; set; }
        public int IdEstado { get; set; } 
        public TipoHoja TipoHoja { get; set; }

        public DateTime? FechaCarga { get; set; }
        public int? IdCarga { get; set; }
        public string LoginCarga { get; set; }
        public int? Item { get; set; }
        public void Insertar(string login, DateTime? fechaCargaDominio, int? idCargaDominio)
        {
            if (string.IsNullOrWhiteSpace(login))
                throw new ArgumentNullException("LoginCarga");
            if (fechaCargaDominio == null)
                throw new ArgumentNullException("fechaCarga");
            if (idCargaDominio == null)
                throw new ArgumentNullException("idCarga");
            IdEstado = 1;
            FechaCarga = fechaCargaDominio;
            IdCarga = idCargaDominio;
            LoginCarga = login;

        }
    }

    public enum TipoHoja
    {
        CmiVenta = 1,
        CmiAlquiler = 2,
        CmiLinea = 3,
        Cmi = 4
    }
}
