--IF OBJECT_ID('FUNNEL.usp_OportunidadPrincipalPieMigrar') IS NOT NULL
--BEGIN 
--    DROP PROC FUNNEL.usp_OportunidadPrincipalPieMigrar 
--END 
--GO


--CREATE PROC FUNNEL.usp_OportunidadPrincipalPieMigrar
--AS
--BEGIN

--	SET NOCOUNT ON 

--	TRUNCATE TABLE FUNNEL.[ReporteOportunidadPrincipalPie]

--	INSERT INTO FUNNEL.[ReporteOportunidadPrincipalPie]
--	(
--	ANIO
--	,MES
--	,SUMIngresoAnual
--    ,[SUMCapex]
--    ,[SUMOibdaAnual]
--    ,[OportTrabajadas]
--    ,[OportMaduras]
--    ,[OportGanadas]
--	)
--	SELECT 
--	TOTALES.ANIO,TOTALES.MES,TOTALES.IngresoTotal,TOTALES.Capex,TOTALES.Oidba,
--	ISNULL(TRABAJADAS.OPORT_TRABAJADAS,0) AS OPORT_TRABAJADAS,
--	0,
--	ISNULL(GANADAS.OPORT_GANADAS,0) AS OPORT_GANADAS FROM(
--	SELECT C.MES,C.ANIO,SUM(C.IngresoTotal) AS IngresoTotal,SUM(C.Capex) AS Capex,SUM(C.Oidba) AS Oidba FROM(
--	SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
--	O.IngresoTotal, O.Capex,O.Oidba
--	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
--	(	
--		SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
--		SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
--		FROM FUNNEL.OportunidadContableOrigen
--		GROUP BY IdOportunidad
--	) O ON S.IdOportunidad = O.IdOportunidad 
--	) AS C
--	GROUP BY C.MES,C.ANIO
--	) AS TOTALES
--	LEFT JOIN 
--	(
--	SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
--	COUNT(S.IdOportunidad) AS OPORT_TRABAJADAS
--	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
--	FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
--	WHERE NOT EXISTS
--		(
--			SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
--			AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
--		)
--	GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
--	) AS TRABAJADAS
--	ON TOTALES.ANIO = TRABAJADAS.ANIO AND TOTALES.MES = TRABAJADAS.MES
--	LEFT JOIN 
--	(
--	SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
--	COUNT(S.IdOportunidad) AS OPORT_GANADAS
--	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
--	FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
--	WHERE Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
--	GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
--	) AS GANADAS
--	ON TOTALES.ANIO = GANADAS.ANIO AND TOTALES.MES = GANADAS.MES
	
    
--END
--GO






IF OBJECT_ID('FUNNEL.usp_OportunidadPrincipalPieConsultar') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.usp_OportunidadPrincipalPieConsultar 
END 
GO


/*
Procedimiento : usp_OportunidadPrincipalPieConsultar
Descripcion   : Reporte de Indicador Principal - Pie
Parametros Ingreso : 
@Anio: A�O
@Mes: MES
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC FUNNEL.usp_OportunidadPrincipalPieConsultar
(
@Anio int,
@Mes int = NULL
)
AS
BEGIN

	SET NOCOUNT ON 


	DECLARE @INDICADORES_PIE TABLE (
	[Anio] [int],
	[Mes] [int],
	[SUMIngresoAnual] [numeric](10, 2),
	[SUMCapex] [numeric](10, 2),
	[SUMOibdaAnual] [numeric](10, 2),
	[OportTrabajadas] [int],
	[OportMaduras] [int],
	[OportGanadas] [int])

	INSERT INTO @INDICADORES_PIE
		SELECT 
		TOTALES.ANIO,TOTALES.MES,TOTALES.IngresoTotal,TOTALES.Capex,TOTALES.Oidba,
		ISNULL(TRABAJADAS.OPORT_TRABAJADAS,0) AS OPORT_TRABAJADAS,
		0,
		ISNULL(GANADAS.OPORT_GANADAS,0) AS OPORT_GANADAS FROM(
		SELECT C.MES,C.ANIO,SUM(C.IngresoTotal) AS IngresoTotal,SUM(C.Capex) AS Capex,SUM(C.Oidba) AS Oidba FROM(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		O.IngresoTotal, O.Capex,O.Oidba
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		(	
			SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
			SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
			FROM FUNNEL.OportunidadContableOrigen
			WHERE IdEstado = 1
			GROUP BY IdOportunidad
		) O ON S.IdOportunidad = O.IdOportunidad 
		WHERE (@Mes IS NULL OR MONTH(S.[FechaCierreEstimada]) = @Mes) AND YEAR(S.[FechaCierreEstimada]) = @Anio
		AND S.IdEstado = 1
		) AS C
		GROUP BY C.MES,C.ANIO
		) AS TOTALES
		LEFT JOIN 
		(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		COUNT(S.IdOportunidad) AS OPORT_TRABAJADAS
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
		WHERE NOT EXISTS
			(
				SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
				AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
				AND IdEstado = 1
			)
		AND (@Mes IS NULL OR MONTH(S.[FechaCierreEstimada]) = @Mes) AND YEAR(S.[FechaCierreEstimada]) = @Anio
		AND S.IdEstado = 1
		GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
		) AS TRABAJADAS
		ON TOTALES.ANIO = TRABAJADAS.ANIO AND TOTALES.MES = TRABAJADAS.MES
		LEFT JOIN 
		(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		COUNT(S.IdOportunidad) AS OPORT_GANADAS
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
		WHERE Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
		AND (@Mes IS NULL OR MONTH(S.[FechaCierreEstimada]) = @Mes) AND YEAR(S.[FechaCierreEstimada]) = @Anio
		AND S.IdEstado = 1 AND O.IdEstado = 1
		GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
		) AS GANADAS
		ON TOTALES.ANIO = GANADAS.ANIO AND TOTALES.MES = GANADAS.MES

	 
	 DECLARE @CANTI_FILAS INT
	 SELECT @CANTI_FILAS = COUNT(Anio)
	  FROM @INDICADORES_PIE where Anio = @Anio and (@Mes IS NULL OR Mes = @Mes)
	
	IF (@CANTI_FILAS = 1)
	BEGIN

	  SELECT 
		   Anio
		  ,Mes
		  ,SUMIngresoAnual
		  ,SUMCapex 
		  ,SUMOibdaAnual 
		  ,OportTrabajadas 
		  ,OportMaduras
		  ,OportGanadas
	   FROM @INDICADORES_PIE where Anio = @Anio and (@Mes IS NULL OR Mes = @Mes)

	END
	 ELSE
	BEGIN
		
		IF (@CANTI_FILAS > 1)
		BEGIN
			
			SELECT 
			   @Anio AS Anio
			  ,0 AS Mes
			  ,SUM(SUMIngresoAnual) AS SUMIngresoAnual
			  ,SUM(SUMCapex) AS SUMCapex 
			  ,SUM(SUMOibdaAnual) AS SUMOibdaAnual  
			  ,SUM(OportTrabajadas) AS OportTrabajadas  
			  ,SUM(OportMaduras) AS OportMaduras 
			  ,SUM(OportGanadas) AS OportGanadas 
		   FROM @INDICADORES_PIE where Anio = @Anio and (@Mes IS NULL OR Mes = @Mes)
			
		END
		 ELSE
		BEGIN

		 SELECT 0 AS SUMIngresoAnual
		  ,0 AS SUMCapex
		  ,0 AS SUMOibdaAnual
		  ,0 AS OportTrabajadas
		  ,0 AS OportMaduras
		  ,0 AS OportGanadas

		END

	END


END
GO







EXEC FUNNEL.usp_OportunidadPrincipalPieConsultar 2018,6
GO




--IF OBJECT_ID('FUNNEL.usp_OportunidadPrincipalCabeceraMigrar') IS NOT NULL
--BEGIN 
--    DROP PROC FUNNEL.usp_OportunidadPrincipalCabeceraMigrar 
--END 
--GO


--CREATE PROC FUNNEL.usp_OportunidadPrincipalCabeceraMigrar 
--AS
--BEGIN

--	SET NOCOUNT ON 

--	TRUNCATE TABLE FUNNEL.[ReportOportunidadPrincipalCabecera]
	
--	INSERT INTO FUNNEL.[ReportOportunidadPrincipalCabecera]([MesOrden],[Mes],[Anio],[IdOportunidad],
--	[Capex],[ProbabilidadExito],[FechaCierreEstimada],Asunto,[NombreCliente])
--	SELECT MONTH(S.[FechaCierreEstimada]),CASE MONTH(S.[FechaCierreEstimada]) 
--		   WHEN 1 THEN 'Ene'
--		   WHEN 2 THEN 'Feb'
--		   WHEN 3 THEN 'Mar'
--		   WHEN 4 THEN 'Abr'
--		   WHEN 5 THEN 'May'
--		   WHEN 6 THEN 'Jun'
--		   WHEN 7 THEN 'Jul'
--		   WHEN 8 THEN 'Ago'
--		   WHEN 9 THEN 'Set'
--		   WHEN 10 THEN 'Oct'
--		   WHEN 11 THEN 'Nov'
--		   WHEN 12 THEN 'Dic'
--		   END
--	AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
--	O.IdOportunidad,O.Capex, S.ProbabilidadExito,S.FechaCierreEstimada,S.Asunto,S.NombreCliente
--	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
--	(
	
--	SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
--    SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
--    FROM FUNNEL.OportunidadContableOrigen
--    GROUP BY IdOportunidad

--	) O ON S.IdOportunidad = O.IdOportunidad 


--END
--GO



IF OBJECT_ID('FUNNEL.usp_OportunidadPrincipalCabeceraConsultar') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.usp_OportunidadPrincipalCabeceraConsultar 
END 
GO


/*
Procedimiento : usp_OportunidadPrincipalCabeceraConsultar
Descripcion   : Reporte de Indicador Principal - Cabecera
Parametros Ingreso : 
@Anio: A�O
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC FUNNEL.usp_OportunidadPrincipalCabeceraConsultar
(
@Anio int,
@Mes int = null
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH cte AS
	(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MesOrden,CASE MONTH(S.[FechaCierreEstimada]) 
			   WHEN 1 THEN 'Ene'
			   WHEN 2 THEN 'Feb'
			   WHEN 3 THEN 'Mar'
			   WHEN 4 THEN 'Abr'
			   WHEN 5 THEN 'May'
			   WHEN 6 THEN 'Jun'
			   WHEN 7 THEN 'Jul'
			   WHEN 8 THEN 'Ago'
			   WHEN 9 THEN 'Set'
			   WHEN 10 THEN 'Oct'
			   WHEN 11 THEN 'Nov'
			   WHEN 12 THEN 'Dic'
			   END
		AS MES,MONTH(S.[FechaCierreEstimada]) AS MESPOSICION,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		O.IdOportunidad,O.Capex, S.ProbabilidadExito,S.FechaCierreEstimada,S.Asunto,S.NombreCliente
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		(	
			SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
			SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
			FROM FUNNEL.OportunidadContableOrigen
			WHERE IdEstado = 1
			GROUP BY IdOportunidad
		) O ON S.IdOportunidad = O.IdOportunidad 
		WHERE S.IdEstado = 1
	)
	SELECT ROW_NUMBER() OVER(PARTITION BY MesOrden ORDER BY convert(int,ProbabilidadExito)) * 0.3  + MesOrden AS MesOrden
	  ,[Mes]
      ,[Anio]
      ,[IdOportunidad]
      ,[Capex]
	  ,convert(int,ProbabilidadExito) as ProbabilidadExito
	  ,FechaCierreEstimada
	  ,Asunto
	  ,NombreCliente
	FROM cte
	WHERE ANIO = @Anio and (@Mes is null or MESPOSICION = @Mes)
	ORDER BY MesOrden

END
GO




EXEC FUNNEL.usp_OportunidadPrincipalCabeceraConsultar 2018,6
go




--IF OBJECT_ID('FUNNEL.usp_OportunidadMigracion') IS NOT NULL
--BEGIN 
--    DROP PROC FUNNEL.usp_OportunidadMigracion 
--END 
--GO


--CREATE PROC FUNNEL.usp_OportunidadMigracion
--AS
--BEGIN

--	SET NOCOUNT ON;

--	EXEC FUNNEL.usp_OportunidadPrincipalPieMigrar
--	EXEC FUNNEL.usp_OportunidadPrincipalCabeceraMigrar

--END
--GO




IF OBJECT_ID('FUNNEL.usp_OportunidadPrincipalPopupConsultar') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.usp_OportunidadPrincipalPopupConsultar 
END 
GO



CREATE PROC FUNNEL.usp_OportunidadPrincipalPopupConsultar
(
@IdOportunidad VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NombreCliente NVARCHAR(255)
	SELECT @NombreCliente = NombreCliente FROM COMUN.SalesForceConsolidadoCabecera 
	WHERE IdOportunidad = @IdOportunidad

	SELECT @NombreCliente AS NombreCliente,
	MAX(CONVERT(INT,NumeroCaso)) AS NumeroCaso,
	SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
	SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX,
	SUM(VanProyecto) AS VanProyecto
	FROM FUNNEL.OportunidadContableOrigen
	WHERE IdEstado = 1 AND IdOportunidad = @IdOportunidad
	GROUP BY IdOportunidad


END
GO


EXEC FUNNEL.usp_OportunidadPrincipalPopupConsultar 'PER-001723583'
GO







SELECT * FROM COMUN.SalesForceConsolidadoCabecera

LINEA DE NEGOCIO
CLIEN6E
COD DE OPORTUNIDAD
MADUREZ
DESCRIPCION COMERCIAL.
CASO
INDDIVADOR
INGRESOS, CAPEX, OPEX, VAN


SELECT * FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = 'PER-001723583'


SELECT * FROM FUNNEL.OportunidadContableOrigen WHERE IdOportunidad = 'PER-001723583'

