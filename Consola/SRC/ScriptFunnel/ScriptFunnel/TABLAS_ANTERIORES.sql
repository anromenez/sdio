USE [DIO]
GO

/****** Object:  Table [FUNNEL].[ReporteOportunidadPrincipalPie]    Script Date: 03/07/2018 02:57:51 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [FUNNEL].[ReporteOportunidadPrincipalPie](
	[Anio] [int] NULL,
	[Mes] [int] NULL,
	[SUMIngresoAnual] [numeric](10, 2) NULL,
	[SUMCapex] [numeric](10, 2) NULL,
	[SUMOibdaAnual] [numeric](10, 2) NULL,
	[OportTrabajadas] [int] NULL,
	[OportMaduras] [int] NULL,
	[OportGanadas] [int] NULL
) ON [PRIMARY]

GO



USE [DIO]
GO

/****** Object:  Table [FUNNEL].[ReportOportunidadPrincipalCabecera]    Script Date: 03/07/2018 02:58:02 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [FUNNEL].[ReportOportunidadPrincipalCabecera](
	[MesOrden] [int] NULL,
	[Mes] [varchar](50) NULL,
	[Anio] [int] NULL,
	[IdOportunidad] [varchar](255) NULL,
	[Capex] [numeric](10, 2) NULL,
	[ProbabilidadExito] [varchar](255) NULL,
	[FechaCierreEstimada] [datetime] NULL,
	[Asunto] [varchar](255) NULL,
	[NombreCliente] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


