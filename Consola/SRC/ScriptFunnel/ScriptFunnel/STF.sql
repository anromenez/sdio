
USE STF

--SELECT * FROM [dbo].[Cliente]


IF OBJECT_ID('[dbo].[usp_ClienteInsert]') IS NOT NULL
BEGIN 
    DROP PROC [dbo].[usp_ClienteInsert] 
END 
GO
CREATE PROC [dbo].[usp_ClienteInsert] 
    @Descripcion varchar(100) = NULL
AS 
	SET NOCOUNT ON 

	IF NOT EXISTS(SELECT [IdCliente] FROM [dbo].[Cliente] WHERE [Descripcion] = @Descripcion AND IdEstado = 1)
	BEGIN

	DECLARE @IdCliente INT
	SELECT @IdCliente = MAX([IdCliente]) FROM DBO.[Cliente]
	IF(@IdCliente IS NULL)
	BEGIN
		SET @IdCliente = 1
	END
	 ELSE
	BEGIN
		SET @IdCliente = @IdCliente + 1
	END


	DECLARE @CodigoCliente varchar(20)
	SET @CodigoCliente = 'C0000'

		INSERT INTO [dbo].[Cliente] ([IdCliente], [CodigoCliente], 
		[Descripcion], [IdSector], [GerenteComercial], [IdDireccionComercial], 
		[IdEstado], [IdUsuarioCreacion], [FechaCreacion])
		SELECT @IdCliente, @CodigoCliente, @Descripcion, 1, 
		1, 1, 1, 
		9999, GETDATE()

	END	

GO



SELECT * FROM [dbo].Proyecto


SELECT 1,c.IdCliente,s.NombreOportunidad,s.IdOportunidad,s.NumeroDelCaso FROM [dbSalesForce].dbo.SalesForceConsolidado s 
left join [dbOportunidades].dbo.OportunidadFinancieraOrigen f on
s.IdOportunidad = f.IdOportunidad and s.NumeroDelCaso = f.NumeroCaso
left join STF.dbo.Cliente c on s.NombreCliente = c.Descripcion
