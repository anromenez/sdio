

IF OBJECT_ID('FUNNEL.[usp_OportunidadOrigenBeginInsert]') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.usp_OportunidadOrigenBeginInsert 
END 
GO


/*
Procedimiento : usp_OportunidadOrigenBeginInsert
Descripcion   : Historial de registro de Oportunidades en el sistema
Parametros Ingreso : 
@FechaCarga: Fecha de carga en la DB
@Id: Id de carga generado
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC FUNNEL.usp_OportunidadOrigenBeginInsert 
    @FechaCarga datetime,
	@Id int output
AS 
	SET NOCOUNT ON 
	

	INSERT INTO FUNNEL.[OportunidadesHistoriaCarga]
			   ([FechaCarga],IdEstado,FechaCreacion)
		 VALUES
			   (@FechaCarga,1,GETDATE())

	set @Id = SCOPE_IDENTITY()
go





IF OBJECT_ID('FUNNEL.[usp_OportunidadFinancieraOrigenInsert]') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.[usp_OportunidadFinancieraOrigenInsert] 
END 
GO


/*
Procedimiento : usp_OportunidadFinancieraOrigenInsert
Descripcion   : Inserta una oportunidad financiera
Parametros Ingreso : 
@IdOportunidad: Id de Oportunidad
@NumeroCaso: Numero de Caso
@PeriodoMeses: Periodo
@IngresoTotal: Ingreso
@PagoUnicoDolares: Pago en Dolares
@RecurrenteDolares: Pago Recurrente en Dolares
@VanFlujoNeto: Van de Flujo Neto
@VanFlujoNetoPorVanIngresos: Van de Flujo Neto por Van de Ingresos
@UtilidadOperativa: Utilidad
@CostosDirectos: Costos Directos
@MargenOperativa: Margen Operativa
@CantidadPtos: Nro de Ptos
@Oidba:Oidba
@OidbaPorcentaje: Oidba en Porcentaje
@Capex: Capex
@Payback: Valor de retorno
@Depreciacion: Depreciacion
@TipoCambio: TipoCambio
@Tir: Tasa interna de retorno
@FechaCarga: Fecha de Carga
@LoginCarga: Login de Carga
@IdCarga: Identificador de carga
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC FUNNEL.[usp_OportunidadFinancieraOrigenInsert] 
    @IdOportunidad varchar(255) = NULL,
    @NumeroCaso varchar(255) = NULL,
    @PeriodoMeses int = NULL,
    @IngresoTotal numeric(10, 2) = NULL,
    @PagoUnicoDolares numeric(10, 2) = NULL,
    @RecurrenteDolares numeric(10, 2) = NULL,
    @VanFlujoNeto numeric(10, 2) = NULL,
    @VanFlujoNetoPorVanIngresos numeric(10, 2) = NULL,
    @UtilidadOperativa numeric(10, 2) = NULL,
    @CostosDirectos numeric(10, 2) = NULL,
    @MargenOperativa numeric(10, 2) = NULL,
    @CantidadPtos int = NULL,
    @Oidba numeric(10, 2) = NULL,
    @OidbaPorcentaje numeric(10, 2) = NULL,
    @Capex numeric(10, 2) = NULL,
    @Payback varchar(50) = NULL,
    @Depreciacion numeric(10, 2) = NULL,
    @TipoCambio numeric(10, 2) = NULL,
    @Tir numeric(10, 2) = NULL,
    @FechaCarga datetime = NULL,
    @LoginCarga varchar(100) = NULL,
    @IdCarga int = NULL
AS 
	SET NOCOUNT ON;
	
	IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NOT NULL)
	BEGIN
		DELETE FROM FUNNEL.[OportunidadFinancieraOrigen] WHERE IdOportunidad = @IdOportunidad
		AND NumeroCaso = @NumeroCaso
	END
	 ELSE
	BEGIN
		IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NULL)
		BEGIN
			DELETE FROM FUNNEL.[OportunidadFinancieraOrigen] WHERE IdOportunidad = @IdOportunidad
		END
	END

	INSERT INTO FUNNEL.[OportunidadFinancieraOrigen] ([IdOportunidad], [NumeroCaso], 
	[PeriodoMeses], [IngresoTotal], [PagoUnicoDolares], [RecurrenteDolares], 
	[VanFlujoNeto], [VanFlujoNetoPorVanIngresos], [UtilidadOperativa], 
	[CostosDirectos], [MargenOperativa], [CantidadPtos], [Oidba], [OidbaPorcentaje], 
	[Capex], [Payback], [Depreciacion], [TipoCambio], [Tir], [FechaCarga], [LoginCarga], [IdCarga],
	IdEstado,FechaCreacion)
	SELECT @IdOportunidad, @NumeroCaso, @PeriodoMeses, @IngresoTotal, @PagoUnicoDolares,
	 @RecurrenteDolares, @VanFlujoNeto, @VanFlujoNetoPorVanIngresos, @UtilidadOperativa, 
	 @CostosDirectos, @MargenOperativa, @CantidadPtos, @Oidba, @OidbaPorcentaje, 
	 @Capex, @Payback, @Depreciacion, @TipoCambio, @Tir, @FechaCarga, @LoginCarga, @IdCarga,
	 1,GETDATE()
	
	
GO








IF OBJECT_ID('FUNNEL.[usp_OportunidadContableOrigenInsert]') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.[usp_OportunidadContableOrigenInsert] 
END 
GO


/*
Procedimiento : usp_OportunidadContableOrigenInsert
Descripcion   : Inserta una oportunidad contable
Parametros Ingreso : 
@IdOportunidad: Id de Oportunidad
@NumeroCaso: Numero de Caso
@IngresoAnual: Ingreso por A�o
@OibdaAnual: OIDBA
@Capex: Monto de Capex
@OibdaPorcentaje: OIDBA en porcentaje
@VanProyecto: Valor Actual
@VanPorVai: Valor Actual Por Vai
@PayBack: Valor de retorno
@ValorRescate: Valor de rescate
@IngresoTotal: Ingreso Total
@CostosDirectos: Costos Directos
@UtilidadOperativa: Utilidad Operativa
@MargenOperativo: Margen Operativo
@NroCotizacion: Numero de Cotizacion
@CantPtos: Nro. de Ptos.
@TipoCambio: Tipo de Cambio
@Anio: Anio de la operacion
@FechaCarga: Fecha de Carga
@LoginCarga: Login de Carga
@IdCarga: Identificador de carga
@TipoCambioIngresos: Tipo de Cambio por Ingresos
@TipoCambioCostos: Tipo de Cambio por Costos
@TipoCambioCapex: Tipo de Cambio por Capex
@RutaCarga: Ruta de excel de carga
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC FUNNEL.[usp_OportunidadContableOrigenInsert] 
    @IdOportunidad varchar(255) = NULL,
    @NumeroCaso varchar(255) = NULL,
    @IngresoAnual numeric(10, 2) = NULL,
    @OibdaAnual numeric(10, 2) = NULL,
    @Capex numeric(10, 2) = NULL,
    @OibdaPorcentaje numeric(10, 2) = NULL,
    @VanProyecto numeric(10, 2) = NULL,
    @VanPorVai numeric(10, 2) = NULL,
    @PayBack varchar(50) = NULL,
    @ValorRescate numeric(10, 2) = NULL,
    @IngresoTotal numeric(10, 2) = NULL,
    @CostosDirectos numeric(10, 2) = NULL,
    @UtilidadOperativa numeric(10, 2) = NULL,
    @MargenOperativo numeric(10, 2) = NULL,
    @NroCotizacion int = NULL,
    @CantPtos int = NULL,
    @TipoCambio numeric(10, 2) = NULL,
    @Anio int = NULL,
    @FechaCarga datetime = NULL,
    @LoginCarga varchar(100) = NULL,
    @IdCarga int = NULL,
	@TipoCambioIngresos numeric(10, 2) = NULL,
	@TipoCambioCostos numeric(10, 2) = NULL,
	@TipoCambioCapex numeric(10, 2) = NULL,
    @RutaCarga varchar(255) = NULL
AS 
	SET NOCOUNT ON 
	
	IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NOT NULL AND @Anio IS NOT NULL)
	BEGIN
		DELETE FROM FUNNEL.[OportunidadContableOrigen] WHERE IdOportunidad = @IdOportunidad
		AND NumeroCaso = @NumeroCaso AND Anio = @Anio
	END
	 ELSE
	BEGIN
		IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NULL AND @Anio IS NOT NULL)
		BEGIN
			DELETE FROM FUNNEL.[OportunidadContableOrigen] WHERE IdOportunidad = @IdOportunidad
			AND Anio = @Anio
		END
	END


	INSERT INTO FUNNEL.[OportunidadContableOrigen] 
	([IdOportunidad], [NumeroCaso], [IngresoAnual], [OibdaAnual], [Capex], 
	[OibdaPorcentaje], [VanProyecto], [VanPorVai], [PayBack], [ValorRescate], 
	[IngresoTotal], [CostosDirectos], [UtilidadOperativa], [MargenOperativo], 
	[NroCotizacion], [CantPtos], [TipoCambio], [Anio], [FechaCarga], [LoginCarga], [IdCarga],
	TipoCambioIngresos,TipoCambioCostos,TipoCambioCapex,RutaCarga,
	IdEstado,FechaCreacion)
	SELECT @IdOportunidad, @NumeroCaso, @IngresoAnual, @OibdaAnual, @Capex, @OibdaPorcentaje, 
	@VanProyecto, @VanPorVai, @PayBack, @ValorRescate, @IngresoTotal, @CostosDirectos,
	@UtilidadOperativa, @MargenOperativo, @NroCotizacion, @CantPtos, @TipoCambio, @Anio, 
	@FechaCarga, @LoginCarga, @IdCarga,@TipoCambioIngresos,@TipoCambioCostos,@TipoCambioCapex,@RutaCarga,
	1,GETDATE()
	

GO






IF OBJECT_ID('FUNNEL.[usp_OportunidadPmoLotusLineaInsert]') IS NOT NULL
BEGIN 
    DROP PROC FUNNEL.[usp_OportunidadPmoLotusLineaInsert] 
END 
GO


CREATE PROC [FUNNEL].[usp_OportunidadPmoLotusLineaInsert] 
    @IdOportunidad varchar(100) = NULL,
    @NumeroCaso varchar(100) = NULL,
    @Proceso varchar(100) = NULL,
    @DatosProceso varchar(100) = NULL,
    @DetalleProceso varchar(100) = NULL,
	@Tipo int = NULL,
	@Item int = NULL,
    @IdEstado int = NULL,
    @IdUsuarioCreacion int = NULL,
    @IdUsuarioEdicion int = NULL,
    @FechaEdicion datetime = NULL,
	@FechaCarga datetime = NULL,
	@IdCarga INT = NULL,
	@LoginCarga varchar(100) = NULL
AS 
	SET NOCOUNT ON 
	
	INSERT INTO [FUNNEL].[OportunidadPmoLotusLinea] ([IdOportunidad], 
	[NumeroCaso], [Proceso], [DatosProceso], [DetalleProceso], Tipo,[IdEstado], 
	[IdUsuarioCreacion], [FechaCreacion], [IdUsuarioEdicion], [FechaEdicion],
	FechaCarga,IdCarga,LoginCarga,Item)
	SELECT @IdOportunidad, @NumeroCaso, @Proceso, @DatosProceso, @DetalleProceso, @Tipo,
	@IdEstado, @IdUsuarioCreacion, GETDATE(), @IdUsuarioEdicion, @FechaEdicion,
	@FechaCarga,@IdCarga,@LoginCarga,@Item
	
GO

