


IF OBJECT_ID('COMUN.[usp_SalesForceOrigenBeginInsert]') IS NOT NULL
BEGIN 
    DROP PROC COMUN.usp_SalesForceOrigenBeginInsert 
END 
GO



/*
Procedimiento : usp_SalesForceOrigenBeginInsert
Descripcion   : Historial de registro de SalesForce en el sistema
Parametros Ingreso : 
@FechaCarga: Fecha de carga en la DB
Parametros Retorno : 
@Id: Id de carga generado
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC COMUN.usp_SalesForceOrigenBeginInsert 
    @FechaCarga datetime,
	@Id int output
AS 
	SET NOCOUNT ON 
	

	INSERT INTO COMUN.[SalesForceHistoriaCarga]
			   ([FechaCarga],IdEstado,FechaCreacion)
		 VALUES
			   (@FechaCarga,1,GETDATE())

	set @Id = SCOPE_IDENTITY()
go






IF OBJECT_ID('COMUN.[usp_SalesForceOrigenInsert]') IS NOT NULL
BEGIN 
    DROP PROC COMUN.[usp_SalesForceOrigenInsert] 
END 
GO


/*
Procedimiento : usp_SalesForceOrigenInsert
Descripcion   : Registro de Sales Force en el Sistema
Parametros Ingreso : 
@IdOportunidad: Id de Oportunidad
@PropietarioOportunidad: Propietario de Oportunidad
@TipologiaOportunidad: Tipologia
@NombreCliente: Nombre del Cliente
@NombreOportunidad: Nombre de la Oportunidad
@FechaCreacion: Fecha de Creacion de la Oportunidad
@FechaCierreEstimada: Fecha de Cierre Estimada de la Oportunidad
@FechaCierreReal: Fecha de Cierre Real de la Oportunidad
@ProbabilidadExito: Probabilidad de Exito
@Etapa: Etapa de la Oportunidad
@TipoOportunidad: Tipo de Oportunidad
@PlazoEstimadoProvision: Plazo Estimado de Provision
@FechaEstimadaInstalacionServicio: Fecha Estimada de Instalacion del Servicio
@DuracionContrato: Duracion del Contrato
@IngresoPorUnicaVezDivisa: Monto de Ingreso Por UnicaVez(MONEDA)
@IngresoPorUnicaVez: Monto de Ingreso Por UnicaVez
@FullContractValueNetoDivisa: Full Contract Valor Neto(MONEDA)
@FullContractValueNeto: Full Contract Valor Neto
@RecurrenteBrutoMensualDivisa: Monto Recurrente Bruto Mensual(MONEDA)
@RecurrenteBrutoMensual: Monto Recurrente Bruto Mensual
@NumeroDelCaso: Numero Del Caso
@Estado: Estado de la Oportunidad
@Departamento: Departamento 
@TipoSolicitud: Tipo de la Solicitud
@Asunto: Asunto 
@FechaCarga: Fecha de Carga en la DB
@LoginCarga: Login de Carga en la DB
@IdCarga: Id de Carga en la DB
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC COMUN.[usp_SalesForceOrigenInsert] 
    @IdOportunidad nvarchar(255) = NULL,
    @PropietarioOportunidad nvarchar(255) = NULL,
    @TipologiaOportunidad nvarchar(255) = NULL,
    @NombreCliente nvarchar(255) = NULL,
    @NombreOportunidad nvarchar(255) = NULL,
    @FechaCreacion datetime = NULL,
    @FechaCierreEstimada datetime = NULL,
    @FechaCierreReal datetime = NULL,
    @ProbabilidadExito nvarchar(255) = NULL,
    @Etapa nvarchar(255) = NULL,
    @TipoOportunidad nvarchar(255) = NULL,
    @PlazoEstimadoProvision int = NULL,
    @FechaEstimadaInstalacionServicio datetime = NULL,
    @DuracionContrato int = NULL,
    @IngresoPorUnicaVezDivisa nvarchar(255) = NULL,
    @IngresoPorUnicaVez nvarchar(255) = NULL,
    @FullContractValueNetoDivisa nvarchar(255) = NULL,
    @FullContractValueNeto nvarchar(255) = NULL,
    @RecurrenteBrutoMensualDivisa nvarchar(255) = NULL,
    @RecurrenteBrutoMensual nvarchar(255) = NULL,
    @NumeroDelCaso nvarchar(255) = NULL,
    @Estado nvarchar(255) = NULL,
    @Departamento nvarchar(255) = NULL,
    @TipoSolicitud nvarchar(255) = NULL,
    @Asunto nvarchar(255) = NULL,
	@FechaCarga datetime,
    @LoginCarga varchar(100) = NULL,
	@IdCarga int
AS 
	SET NOCOUNT ON 
	
	
	INSERT INTO COMUN.[SalesForceOrigen] ([IdOportunidad], [PropietarioOportunidad], [TipologiaOportunidad], [NombreCliente], [NombreOportunidad], [FechaCreacion], [FechaCierreEstimada], [FechaCierreReal], [ProbabilidadExito], [Etapa], [TipoOportunidad], [PlazoEstimadoProvision], [FechaEstimadaInstalacionServicio], [DuracionContrato], [IngresoPorUnicaVezDivisa], [IngresoPorUnicaVez], [FullContractValueNetoDivisa], [FullContractValueNeto], [RecurrenteBrutoMensualDivisa], [RecurrenteBrutoMensual], [NumeroDelCaso], [Estado], [Departamento], [TipoSolicitud], [Asunto], [FechaCarga], [LoginCarga],IdCarga,
	IdEstado,FechaCreacionDB)
	SELECT @IdOportunidad, @PropietarioOportunidad, @TipologiaOportunidad, @NombreCliente, @NombreOportunidad, @FechaCreacion, @FechaCierreEstimada, @FechaCierreReal, @ProbabilidadExito, @Etapa, @TipoOportunidad, @PlazoEstimadoProvision, @FechaEstimadaInstalacionServicio, @DuracionContrato, @IngresoPorUnicaVezDivisa, @IngresoPorUnicaVez, @FullContractValueNetoDivisa, @FullContractValueNeto, @RecurrenteBrutoMensualDivisa, @RecurrenteBrutoMensual, @NumeroDelCaso, @Estado, @Departamento, @TipoSolicitud, @Asunto, @FechaCarga, @LoginCarga,@IdCarga,
	1,GETDATE()
	

GO







IF OBJECT_ID('COMUN.[usp_SalesForceConsolidar]') IS NOT NULL
BEGIN 
    DROP PROC COMUN.[usp_SalesForceConsolidar] 
END 
GO


/*
Procedimiento : usp_SalesForceConsolidar
Descripcion   : Actualiza el detalle y cabeceras de las tablas SalesForce
Parametros Ingreso : 
@IdCarga: Id de Carga en la DB
@LoginCarga: Login de Carga en la DB
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC COMUN.[usp_SalesForceConsolidar]
(
@IdCarga int,
@LoginCarga varchar(100)
)
AS
BEGIN

	SET NOCOUNT ON 
	

	MERGE COMUN.[SalesForceConsolidadoDetalle] AS TARGET
	USING (select * from COMUN.[SalesForceOrigen] 
	where IdCarga = @IdCarga) AS SOURCE
	ON (TARGET.IdOportunidad = SOURCE.IdOportunidad
			and TARGET.NumeroDelCaso = SOURCE.NumeroDelCaso)
    WHEN MATCHED THEN
	UPDATE SET 
			TARGET.[PropietarioOportunidad] = SOURCE.PropietarioOportunidad, 
			TARGET.[TipologiaOportunidad] = SOURCE.TipologiaOportunidad, 
			TARGET.[NombreCliente] = SOURCE.NombreCliente, 
			TARGET.[NombreOportunidad] = SOURCE.NombreOportunidad, 
			TARGET.[FechaCreacion] = SOURCE.FechaCreacion, 
			TARGET.[FechaCierreEstimada] = SOURCE.FechaCierreEstimada, 
			TARGET.[FechaCierreReal] = SOURCE.FechaCierreReal, 
			TARGET.[ProbabilidadExito] = SOURCE.ProbabilidadExito, 
			TARGET.[Etapa] = SOURCE.Etapa, 
			TARGET.[TipoOportunidad] = SOURCE.TipoOportunidad, 
			TARGET.[PlazoEstimadoProvision] = SOURCE.PlazoEstimadoProvision, 
			TARGET.[FechaEstimadaInstalacionServicio] = SOURCE.FechaEstimadaInstalacionServicio, 
			TARGET.[DuracionContrato] = SOURCE.DuracionContrato, 
			TARGET.[IngresoPorUnicaVezDivisa] = SOURCE.IngresoPorUnicaVezDivisa, 
			TARGET.[IngresoPorUnicaVez] = SOURCE.IngresoPorUnicaVez, 
			TARGET.[FullContractValueNetoDivisa] = SOURCE.FullContractValueNetoDivisa, 
			TARGET.[FullContractValueNeto] = SOURCE.FullContractValueNeto, 
			TARGET.[RecurrenteBrutoMensualDivisa] = SOURCE.RecurrenteBrutoMensualDivisa, 
			TARGET.[RecurrenteBrutoMensual] = SOURCE.RecurrenteBrutoMensual, 
			TARGET.[Estado] = SOURCE.Estado, 
			TARGET.[Departamento] = SOURCE.Departamento, 
			TARGET.[TipoSolicitud] = SOURCE.TipoSolicitud, 
			TARGET.[Asunto] = SOURCE.Asunto, 
			TARGET.FechaEdicion = GETDATE(), 
			TARGET.[LoginUltimaModificacion] = @LoginCarga,
			TARGET.IdEstado = 1
	WHEN NOT MATCHED THEN

			INSERT
			([IdOportunidad], [PropietarioOportunidad], [TipologiaOportunidad], 
			[NombreCliente], [NombreOportunidad], [FechaCreacion], [FechaCierreEstimada], 
			[FechaCierreReal], [ProbabilidadExito], [Etapa], [TipoOportunidad], 
			[PlazoEstimadoProvision], [FechaEstimadaInstalacionServicio], 
			[DuracionContrato], [IngresoPorUnicaVezDivisa], [IngresoPorUnicaVez], 
			[FullContractValueNetoDivisa], [FullContractValueNeto], [RecurrenteBrutoMensualDivisa], 
			[RecurrenteBrutoMensual], [NumeroDelCaso], [Estado], [Departamento], [TipoSolicitud], 
			[Asunto], FechaCreacionDB, FechaEdicion, [LoginRegistro], [LoginUltimaModificacion],
			IdEstado)
			VALUES(
			  SOURCE.[IdOportunidad]
			  ,SOURCE.[PropietarioOportunidad]
			  ,SOURCE.[TipologiaOportunidad]
			  ,SOURCE.[NombreCliente]
			  ,SOURCE.[NombreOportunidad]
			  ,SOURCE.[FechaCreacion]
			  ,SOURCE.[FechaCierreEstimada]
			  ,SOURCE.[FechaCierreReal]
			  ,SOURCE.[ProbabilidadExito]
			  ,SOURCE.[Etapa]
			  ,SOURCE.[TipoOportunidad]
			  ,SOURCE.[PlazoEstimadoProvision]
			  ,SOURCE.[FechaEstimadaInstalacionServicio]
			  ,SOURCE.[DuracionContrato]
			  ,SOURCE.[IngresoPorUnicaVezDivisa]
			  ,SOURCE.[IngresoPorUnicaVez]
			  ,SOURCE.[FullContractValueNetoDivisa]
			  ,SOURCE.[FullContractValueNeto]
			  ,SOURCE.[RecurrenteBrutoMensualDivisa]
			  ,SOURCE.[RecurrenteBrutoMensual]
			  ,SOURCE.[NumeroDelCaso]
			  ,SOURCE.[Estado]
			  ,SOURCE.[Departamento]
			  ,SOURCE.[TipoSolicitud]
			  ,SOURCE.[Asunto]
			  ,getdate()
			  ,null
			  ,@LoginCarga
			  ,null,
			  1);


	MERGE COMUN.[SalesForceConsolidadoCabecera] AS TARGET
	USING (
	SELECT C.* FROM COMUN.[SalesForceConsolidadoDetalle] AS C INNER JOIN
	(
	SELECT IdOportunidad,MAX(CONVERT(INT,NumeroDelCaso)) AS NumeroDelCaso FROM COMUN.SalesForceConsolidadoDetalle
	GROUP BY IdOportunidad
	) AS T
	ON C.IdOportunidad = T.IdOportunidad AND C.NumeroDelCaso = T.NumeroDelCaso
	) AS SOURCE
	ON (TARGET.IdOportunidad = SOURCE.IdOportunidad)
	WHEN MATCHED AND SOURCE.NumeroDelCaso >= TARGET.NumeroDelCaso  THEN
	UPDATE SET 
			TARGET.NumeroDelCaso = SOURCE.NumeroDelCaso, 
			TARGET.PropietarioOportunidad = SOURCE.PropietarioOportunidad,
			TARGET.TipologiaOportunidad = SOURCE.TipologiaOportunidad,
			TARGET.NombreCliente = SOURCE.NombreCliente,
			TARGET.NombreOportunidad = SOURCE.NombreOportunidad,
			TARGET.FechaCreacion = SOURCE.FechaCreacion,
			TARGET.ProbabilidadExito = SOURCE.ProbabilidadExito,
			TARGET.Etapa = SOURCE.Etapa,
			TARGET.FechaCierreEstimada = SOURCE.FechaCierreEstimada,
			TARGET.Asunto = SOURCE.Asunto,
			TARGET.IdEstado = 1,
			TARGET.IdUsuarioEdicion = null,
			TARGET.LoginUltimaModificacion = @LoginCarga,
			TARGET.FechaEdicion = GETDATE()
	WHEN NOT MATCHED THEN
	INSERT ([IdOportunidad],[NumeroDelCaso],PropietarioOportunidad,TipologiaOportunidad,
			NombreCliente,NombreOportunidad,FechaCreacion,ProbabilidadExito,Etapa,FechaCierreEstimada,Asunto,
			FechaCreacionDB,LoginRegistro,IdEstado)
			 VALUES(
			 SOURCE.[IdOportunidad],
			 SOURCE.[NumeroDelCaso],
			 SOURCE.PropietarioOportunidad,
			 SOURCE.TipologiaOportunidad,
			 SOURCE.NombreCliente,
			 SOURCE.NombreOportunidad,
			 SOURCE.FechaCreacion,
			 SOURCE.ProbabilidadExito,
			 SOURCE.Etapa,
			 SOURCE.FechaCierreEstimada,
			 SOURCE.Asunto,
			 getdate(),
			 @LoginCarga,
			 1
			 );

END

GO





IF OBJECT_ID('COMUN.[usp_SalesForceVigente]') IS NOT NULL
BEGIN 
    DROP PROC COMUN.usp_SalesForceVigente 
END 
GO



/*
Procedimiento : usp_SalesForceVigente
Descripcion   : Identifica si la oportunidad esta disponible para analizar
Parametros Ingreso : 
@IdOportunidad: Id de la Oportunidad
Parametros Retorno : 
@existe: Esta registrada en SalesForce
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC COMUN.usp_SalesForceVigente 
(
@IdOportunidad varchar(255),
@existe BIT output
)
AS
BEGIN

SET NOCOUNT ON;

	  IF EXISTS(SELECT[IdOportunidad]
	  FROM COMUN.[SalesForceConsolidadoCabecera] p where not exists
		(
			SELECT [IdOportunidad]
		  FROM COMUN.[SalesForceConsolidadoCabecera] where [IdOportunidad] = p.[IdOportunidad] and Etapa
		  in
		  ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
		)
	  AND p.IdOportunidad = @IdOportunidad)
	  BEGIN
		SET @existe = 1
	  END
	   ELSE
	  BEGIN
	   SET @existe = 0
	  END

END
GO

