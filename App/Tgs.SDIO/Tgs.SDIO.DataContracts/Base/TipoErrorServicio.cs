﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Base
{
    public class TipoErrorServicioEnum
    {
        [DataContract]
        public enum TipoErrorServicio
        {
            [EnumMember]
            ErrorNoManejado,
            [EnumMember]
            ErrorGeneral,
            [EnumMember]
            ErrorValidacion,
            [EnumMember]
            ErrorNegocio
        }
    }
}
