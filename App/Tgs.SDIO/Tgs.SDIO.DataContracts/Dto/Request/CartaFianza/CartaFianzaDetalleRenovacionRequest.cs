﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{

    [DataContract]
    public class CartaFianzaDetalleRenovacionRequest
    {
        [DataMember]
        public int IdCartaFianzaDetalleSeguimiento { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int IdColaboradorACargo { set; get; }
        [DataMember]
        public int IdRenovarTm { set; get; }
        [DataMember]
        public int PeriodoMesRenovacion { set; get; }
        [DataMember]
        public DateTime FechaVencimientoRenovacion { set; get; }
        [DataMember]
        public string SustentoRenovacion { set; get; }
        [DataMember]
        public string Observacion { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public int IdUsuarioCreacion { set; get; }
        [DataMember]
        public DateTime FechaCreacion { set; get; }
        [DataMember]
        public int IdUsuarioEdicion { set; get; }
        [DataMember]
        public DateTime FechaEdicion { set; get; }

}
}
