﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.CartaFianza
{

    [DataContract]
    public class CartaFianzaDetalleRecuperoRequest
    {
        [DataMember]
        public int IdCartaFianzaDetalleSeguimiento {set;get;}
        [DataMember]
        public int IdCartaFianza {set;get;}
        [DataMember]
        public int IdColaborador {set;get;}
        [DataMember]
        public int IdColaboradorACargo {set;get;}
        [DataMember]
        public int IdEstadoRecuperacionCartaFianzaTm {set;get;}
        [DataMember]
        public string Comentario { set; get; }
        [DataMember]
        public int IdEstado {set;get;}
        [DataMember]
        public int IdUsuarioCreacion {set;get;}
        [DataMember]
        public DateTime FechaCreacion {set;get;}
        [DataMember]
        public int IdUsuarioEdicion {set;get;}
        [DataMember]
        public DateTime FechaEdicion {set;get;}


    }
}
