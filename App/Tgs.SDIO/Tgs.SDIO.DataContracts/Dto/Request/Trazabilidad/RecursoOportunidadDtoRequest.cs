using System;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;

namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class RecursoOportunidadDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRol { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeDedicacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FInicioAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FFinAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListOportunidadSelect { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListRecursoSelect { get; set; }

    }
}
