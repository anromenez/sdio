﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
   public class IdMasivoDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public string id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool Estado { get; set; }
    }
}
