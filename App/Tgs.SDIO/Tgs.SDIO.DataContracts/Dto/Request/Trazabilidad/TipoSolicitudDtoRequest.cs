﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public class TipoSolicitudDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdTipoSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int OrdenVisual { get; set; }
    }
}
