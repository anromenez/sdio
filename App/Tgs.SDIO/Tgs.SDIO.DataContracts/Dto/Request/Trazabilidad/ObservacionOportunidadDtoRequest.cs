using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class ObservacionOportunidadDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdObservacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdConcepto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}
