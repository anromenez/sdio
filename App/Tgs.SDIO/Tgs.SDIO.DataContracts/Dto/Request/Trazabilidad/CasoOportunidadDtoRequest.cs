using System;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class CasoOportunidadDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCasoPadre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Asunto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Complejidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? Prioridad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string SolucionCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }
}
