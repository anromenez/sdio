using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class RecursoDtoRequest
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? TipoRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string UserNameSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Nombre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DNI { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario{ get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }


    }
}
