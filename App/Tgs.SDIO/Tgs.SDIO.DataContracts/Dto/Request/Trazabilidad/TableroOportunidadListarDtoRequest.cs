using System.Collections.Generic;
using System.Runtime.Serialization;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
 

//EAAR: PF12
namespace Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad
{
    [DataContract]
    public  class TableroOportunidadListarDtoRequest 
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int EstadoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FechaInicio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FechaFin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Indice { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Tamanio { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}
