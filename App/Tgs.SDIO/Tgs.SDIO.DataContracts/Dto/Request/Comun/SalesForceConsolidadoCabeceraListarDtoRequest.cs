﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class SalesForceConsolidadoCabeceraListarDtoRequest
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string IdOportunidad { get; set; }
        [DataMember]
        public string NumeroDelCaso { get; set; }
        [DataMember]
        public string PropietarioOportunidad { get; set; }
        [DataMember]
        public string TipologiaOportunidad { get; set; }
        [DataMember]
        public string NombreCliente { get; set; }
        [DataMember]
        public string NombreOportunidad { get; set; }
        [DataMember]
        public DateTime? FechaCreacion { get; set; }
        [DataMember]
        public string ProbabilidadExito { get; set; }
        [DataMember]
        public string Etapa { get; set; }
        [DataMember]
        public DateTime? FechaRegistro { get; set; }
        [DataMember]
        public DateTime? FechaUltimaModificacion { get; set; }
        [DataMember]
        public string LoginRegistro { get; set; }
        [DataMember]
        public string LoginUltimaModificacion { get; set; }

        [DataMember]
        public DateTime? FechaCierreEstimada { get; set; }

        [DataMember]
        public string Asunto { get; set; }
    }
}
