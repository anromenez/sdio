﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class DepreciacionDtoRequest
    {


        [DataMember]
        public int IdDepreciacion { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Detalle { get; set; }
        [DataMember]
        public decimal? PorcentajeAnual { get; set; }
        [DataMember]
        public int? Meses { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }



    }
}
