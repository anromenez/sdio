﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Request.Comun
{
    [DataContract]
    public class ArchivoDtoRequest
    {

        [DataMember]
        public int IdArchivo { get; set; }
        [DataMember]
        public int? IdTipoDocumento { get; set; }
        [DataMember]
        public int? IdRelacion1 { get; set; }
        [DataMember]
        public int? IdRelacion2 { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string NombreArchivo { get; set; }
        [DataMember]
        public string RutaArchivo { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }


    }
}
