﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class ProyectoServicioConceptoDtoRequest
    {
        [DataMember]
        public int IdProyecto { get; set; }

        [DataMember]
        public int IdSubServicio { get; set; }

        [DataMember]
        public int IdEstado { get; set; }

        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int IdPestana { get; set; }

        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public int IdServicioConcepto { get; set; }
    }
}
