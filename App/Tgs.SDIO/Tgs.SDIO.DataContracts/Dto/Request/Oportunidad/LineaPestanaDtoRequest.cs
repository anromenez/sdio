﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class LineaPestanaDtoRequest
    {
        [DataMember]
        public int IdPestana { get; set; }

        [DataMember]
        public int IdEstado { get; set; }
    }
}
