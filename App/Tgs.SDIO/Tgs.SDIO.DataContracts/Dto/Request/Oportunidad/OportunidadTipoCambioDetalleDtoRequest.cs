﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadTipoCambioDetalleDtoRequest
    {

        [DataMember]
        public int IdTipoCambioDetalle { get; set; }
        [DataMember]
        public int IdTipoCambioOportunidad { get; set; }
        [DataMember]
        public int IdMoneda { get; set; }
        [DataMember]
        public int IdTipificacion { get; set; }
        [DataMember]
        public int Anio { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

    }
}
