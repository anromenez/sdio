﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Oportunidad
{
    [DataContract]
    public class OportunidadServicioCMIDtoRequest
    {
        //Campos BD
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public decimal Porcentaje { get; set; }
        [DataMember]
        public int IdAnalista { get; set; }

        //Adicionales


        //Auditoria
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public System.DateTime FechaCreacion { get; set; }
        [DataMember]
        public int IdUsuarioEdicion { get; set; }
        [DataMember]
        public System.DateTime FechaEdicion { get; set; }        
    }
}
