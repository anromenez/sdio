﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    public class UsuarioSistemaEmpresaDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaFin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdTurno { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRegistra { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaInicio { get; set; }

    }
}
