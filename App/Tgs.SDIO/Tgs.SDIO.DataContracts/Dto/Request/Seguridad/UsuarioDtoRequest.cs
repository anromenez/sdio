﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    public class UsuarioDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEmpresa { get; set; } 

        [DataMember(EmitDefaultValue = false)]
        public string Apellidos { get; set; }
         
        [DataMember(EmitDefaultValue = false)]
        public string Login { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; } 

        [DataMember(EmitDefaultValue = false)]
        public int? IntentosFallidos { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NroIntentos { get; set; }
         
        [DataMember(EmitDefaultValue = false)]
        public string Nombres { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorreoElectronico { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TipoUsuario { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoCip { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCaducidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UsuarioEnvia { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoSistema { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NuevoPassword { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PerfilDtoRequest PerfilDtoRequest { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public UsuarioSistemaEmpresaDtoRequest UsuarioSistemaEmpresaDtoRequest { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }
        

    }
}
