﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Request.Seguridad
{
    public class PerfilDtoRequest
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdPerfil { get; set; } 
    
        [DataMember(EmitDefaultValue = false)]
        public int IdEstadoRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaRegistro { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombrePerfil { get; set; }
         
    }
}
