﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class CasoNegocioDtoResponse
    {
        [DataMember]
        public int? IdLineaNegocio { get; set; }

        [DataMember]
        public int IdCasoNegocio { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string LineaNegocio { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public string FlagDefecto { get; set; }

        [DataMember]
        public int IdCasoNegocioServicio { get; set; }
    }
}
