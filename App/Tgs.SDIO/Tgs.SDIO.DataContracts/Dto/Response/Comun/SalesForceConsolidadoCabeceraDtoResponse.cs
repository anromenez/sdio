﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class SalesForceConsolidadoCabeceraDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public string Madurez { get; set; }
        [DataMember]
        public decimal? IngresoTotal { get; set; }
        [DataMember]
        public decimal? Capex { get; set; }

        [DataMember]
        public decimal? Opex { get; set; }
        [DataMember]
        public decimal? Oibda { get; set; }
        [DataMember]
        public int? Oportunidades_Trabajadas { get; set; }
    }
}

