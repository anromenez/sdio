using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{

    [DataContract]
    public class ProcesoResponse
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int TipoRespuesta { get; set; }
        [DataMember]
        public string Mensaje { get; set; }

    }
}