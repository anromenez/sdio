﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class CasoNegocioServicioDtoResponse
    {
        [DataMember]
        public int IdCasoNegocioServicio { get; set; }

        [DataMember]
        public int IdCasoNegocio { get; set; }

        [DataMember]
        public int IdServicio { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }

        [DataMember]
        public string Servicio { get; set; }

    }
}
