using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class FentoceldaDtoResponse
    {

        [DataMember]
        
        public int IdFentocelda { get; set; }
        [DataMember]

        public int IdTipificacion { get; set; }
        [DataMember]

        public string Descripcion { get; set; }
        [DataMember]

        public decimal? Monto { get; set; }

        [DataMember]
        public decimal? VelocidadSubidaKBPS { get; set; }

        [DataMember]
        public decimal? PorcentajeGarantizado { get; set; }

        [DataMember]
        public decimal? PorcentajeSobresuscripcion { get; set; }

        [DataMember]
        public decimal? CostoSegmentoSatelital { get; set; }

        [DataMember]
        public decimal? InvAntenaHubUSD { get; set; }

        [DataMember]
        public decimal? AntenaCasaClienteUSD { get; set; }

        [DataMember]
        public decimal? Instalacion { get; set; }

        [DataMember]
        public int? IdUnidadConsumo { get; set; }
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        
    }
}
