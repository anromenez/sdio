﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Comun
{
    [DataContract]
    public class ClienteDtoResponse
    {


        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public string CodigoCliente { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int IdSector { get; set; }
        [DataMember]
        public string GerenteComercial { get; set; }
        [DataMember]
        public int? IdDireccionComercial { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

        [DataMember]
        public string NumeroIdentificadorFiscal { get; set; }
        [DataMember]
        public string IdTipoIdentificadorFiscalTm { get; set; }

    }
}
