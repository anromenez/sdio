using System;
using System.Runtime.Serialization;
using Tgs.SDIO.Util.Paginacion;
using System.Collections.Generic;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public  class RecursoOportunidadDtoResponse
    {
        
       [DataMember(EmitDefaultValue = false)]
        public string Estado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionRolRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdRecurso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdRol { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? PorcentajeDedicacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Observaciones { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FInicioAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FFinAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFInicioAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFFinAsignacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<ComboDtoResponse> ListSegmentoNegocio { get; set; }
    }

    [DataContract]
    public class RecursoOportunidadPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<RecursoOportunidadDtoResponse> ListRecursoOportunidadDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
