using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public  class DocumentoAdjuntoDtoResponse
    {

        [DataMember(EmitDefaultValue = false)]
        public int IdDocAdjunto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdTipoDocumento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Descripcion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdSeguimiento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdObservacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdEstado { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCreacion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaEdicion { get; set; }
    }
}
