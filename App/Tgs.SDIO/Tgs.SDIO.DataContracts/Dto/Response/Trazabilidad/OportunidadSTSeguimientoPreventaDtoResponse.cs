﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class OportunidadSTSeguimientoPreventaDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Asunto { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEstadoCaso { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEtapa { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoSolicitud { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionSegmento { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionComplejidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string DescripcionTipoEntidadCliente { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdCasoSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdOportunidad { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string IdOportunidadSF { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int? ProbalidadExito { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public decimal? ImporteCapex { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public DateTime? FechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaApertura { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string strFechaCierre { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdDatosPreventaMovil { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int IdCaso { get; set; }
    }

    [DataContract]
    public class OportunidadSTSeguimientoPreventaPaginadoDtoResponse : IPagedList
    {
        [DataMember(EmitDefaultValue = false)]
        public List<OportunidadSTSeguimientoPreventaDtoResponse> ListOportunidadSTSeguimientoPreventaDto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageSize { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalItemCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasPreviousPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool HasNextPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsFirstPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public bool IsLastPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int FirstItemOnPage { get; set; }
        [DataMember(EmitDefaultValue = false)]

        public int LastItemOnPage { get; set; }


    }
}
