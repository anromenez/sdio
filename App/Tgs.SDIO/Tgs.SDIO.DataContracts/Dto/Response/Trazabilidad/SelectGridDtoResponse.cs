﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad
{
    [DataContract]
    public class SelectGridDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool Estado { get; set; }
    }
}
