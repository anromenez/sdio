﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class ProyectadoOibdaResponse
    {
        [DataMember]
        public string label { get; set; }
        [DataMember]
        public decimal? value { get; set; }

        [DataMember]
        public string Linea { get; set; }
        [DataMember]
        public string Color { get; set; }

    }
}
