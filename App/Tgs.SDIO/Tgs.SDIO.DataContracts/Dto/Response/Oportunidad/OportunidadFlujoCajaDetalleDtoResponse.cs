﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadFlujoCajaDetalleDtoResponse
    {
        [DataMember]
        public int IdFlujoCajaConfiguracion { get; set; }
        [DataMember]
        public int IdFlujoCajaDetalle { get; set; }
        [DataMember]
        public int Anio { get; set; }
        [DataMember]
        public int Mes { get; set; }
        [DataMember]
        public decimal Monto { get; set; }
    }
}
