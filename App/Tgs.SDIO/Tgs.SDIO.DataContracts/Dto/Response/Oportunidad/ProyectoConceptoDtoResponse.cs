﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class ProyectoConceptoDtoResponse
    {
        [DataMember]
        public int IdProyecto { get; set; }
        [DataMember]
        public int IdServicioCMI { get; set; }
        [DataMember]
        public int IdConcepto { get; set; }
        [DataMember]
        public int IdTipoCosto { get; set; }
        [DataMember]
        public int IdServicioConcepto { get; set; }
        [DataMember]
        public int Concepto { get; set; }
        [DataMember]
        public string DescripcionCMI { get; set; }
        [DataMember]
        public decimal? Porcentaje { get; set; }
        [DataMember]
        public string Producto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public decimal? CostoPreOperativo { get; set; }
        [DataMember]
        public string Proveedor { get; set; }
        [DataMember]
        public string Periodo { get; set; }
        [DataMember]
        public string Inicia { get; set; }
        [DataMember]
        public decimal? Costo { get; set; }
        [DataMember]
        public int IdProyectoConcepto { get; set; }
        [DataMember]
        public int Meses { get; set; }
        [DataMember]
        public int IdTipoConcepto { get; set; }

        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int? IdEstado { get; set; }
    }
}
