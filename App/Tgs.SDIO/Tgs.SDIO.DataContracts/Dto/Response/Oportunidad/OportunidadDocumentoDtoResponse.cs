﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class OportunidadDocumentoDtoResponse
    {

        [DataMember]
        public int IdDocumento { get; set; }
        [DataMember]
        public int IdOportunidadLineaNegocio { get; set; }
        public int IdFlujoCaja { get; set; }
        [DataMember]
        public int TipoDocumento { get; set; }
        [DataMember]
        public string RutaDocumento { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int IdTipoDocumento { get; set; }
        [DataMember]
        public int? IdEstado { get; set; }
        [DataMember]
        public int IdUsuarioCreacion { get; set; }
        [DataMember]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        public int? IdUsuarioEdicion { get; set; }
        [DataMember]
        public DateTime? FechaEdicion { get; set; }

    }
}
