﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Oportunidad
{
    [DataContract]
    public class LineaConceptoCMIDtoResponse
    {
        [DataMember]
        public int IdSubServicio { get; set; }

        [DataMember]
        public string Concepto { get; set; }

        [DataMember]
        public int IdPestana { get; set; }

        [DataMember]
        public string Pestana { get; set; }

        [DataMember]
        public int IdGrupo { get; set; }

        [DataMember]
        public string Grupo { get; set; }


        [DataMember]
        public int IdLineaProducto { get; set; }

        [DataMember]
        public int IdServicioCMI { get; set; }

        [DataMember]
        public decimal Costo { get; set; }
    }
}
