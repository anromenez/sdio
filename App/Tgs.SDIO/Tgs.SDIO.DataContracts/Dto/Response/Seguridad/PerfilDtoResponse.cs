﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    public class PerfilDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string CodigoPerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombrePerfil { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int IdPerfil { get; set; }
    }
}
