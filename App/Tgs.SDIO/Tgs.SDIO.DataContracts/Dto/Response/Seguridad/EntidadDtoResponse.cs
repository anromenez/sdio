﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Seguridad
{
    public class EntidadDtoResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public int IdEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CodigoEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NombreEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DescripcionEntidad { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TipoEntidad { get; set; }
    }
}
