﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezDtoResponse
    {
        [DataMember]
        public IEnumerable<OportunidadFinancieraOrigenDtoResponse> ListaMadurez { get; set; }

        [DataMember]
        public IEnumerable<SalesForceConsolidadoCabeceraDtoResponse> ListaMadurezCostos { get; set; }
    }
}
