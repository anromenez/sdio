﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezCostoIngresosDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public decimal? IngresosN1 { get; set; }

        [DataMember]
        public decimal? IngresosN2 { get; set; }

        [DataMember]
        public decimal? IngresosN3 { get; set; }

        [DataMember]
        public decimal? IngresosN4 { get; set; }

        [DataMember]
        public decimal? IngresosN5 { get; set; }
       
    }
}
