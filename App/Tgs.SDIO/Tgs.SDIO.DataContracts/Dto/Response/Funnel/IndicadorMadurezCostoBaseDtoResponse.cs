﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezCostoBaseDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public int N1 { get; set; }

        [DataMember]
        public int N2 { get; set; }

        [DataMember]
        public int N3 { get; set; }

        [DataMember]
        public int N4 { get; set; }

        [DataMember]
        public int N5 { get; set; }
    }
}
