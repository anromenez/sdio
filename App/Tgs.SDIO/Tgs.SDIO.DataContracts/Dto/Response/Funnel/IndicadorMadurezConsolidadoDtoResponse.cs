﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezConsolidadoDtoResponse
    {
         [DataMember]
         public IEnumerable<OportunidadFinancieraOrigenDtoResponse> ListaMadurez { get; set; }

         [DataMember]
         public IEnumerable<IndicadorMadurezCostoOportunidadesTrabajadasDtoResponse> ListaOportunidadesTrabajadas { get; set; }

         [DataMember]
         public IEnumerable<IndicadorMadurezCostoIngresosDtoResponse> ListaIngresos { get; set; }

         [DataMember]
         public IEnumerable<IndicadorMadurezCapexDtoResponse> ListaCapex { get; set; }

         [DataMember]
         public IEnumerable<IndicadorMadurezOpexDtoResponse> ListaOpex { get; set; }

         [DataMember]
         public IEnumerable<IndicadorMadurezOidbaDtoResponse> ListaOidba { get; set; }

    }
}
