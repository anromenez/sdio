﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    public class DashboardTotalesDtoResponse
    {
        public decimal? SumIngresoAnual { get; set; }

        public decimal? SumCapex { get; set; }

        public decimal? SumOibdaAnual { get; set; }

        public int? OportTrabajadas { get; set; }

    }
}
