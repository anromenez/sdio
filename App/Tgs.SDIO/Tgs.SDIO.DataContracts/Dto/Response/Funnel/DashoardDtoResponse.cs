﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class DashoardDtoResponse
    {
        [DataMember]
        public List<DashoardMadurezDtoResponse> ListaMadurez { get; set; }

        [DataMember]
        public decimal? OportunidadesTrabajadas { get; set; }

        [DataMember]
        public decimal? IngresoTotal { get; set; }

        [DataMember]
        public decimal? CapexTotal { get; set; }

        [DataMember]
        public decimal? OpexTotal { get; set; }

        [DataMember]
        public decimal? OibdaTotal { get; set; }

        [DataMember]
        public List<DashoardAtencionDtoResponse>  EstadosAtencion { get; set; }

        [DataMember]
        public List<DashoardOfertasLineaNegocioDtoResponse> OfertasLineaNegocio { get; set; }

        [DataMember]
        public List<DashoardOfertasSectorDtoResponse> OfertasSector { get; set; }

        [DataMember]
        public List<DashoardIngresosLineaNegocioDtoResponse> IngresosLineaNegocio { get; set; }

        [DataMember]
        public List<DashoardIngresosSectorDtoResponse> IngresosSector { get; set; }

        [DataMember]
        public List<DashoardOfertasEstadoDtoResponse> OfertasEstado { get; set; }
    }
}
