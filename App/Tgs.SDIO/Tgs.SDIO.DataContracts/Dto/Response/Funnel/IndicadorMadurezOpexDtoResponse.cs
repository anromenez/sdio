﻿using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.Funnel
{
    [DataContract]
    public class IndicadorMadurezOpexDtoResponse
    {
        [DataMember]
        public string LineaNegocio { get; set; }
        [DataMember]
        public decimal? Opex1 { get; set; }

        [DataMember]
        public decimal? Opex2 { get; set; }

        [DataMember]
        public decimal? Opex3 { get; set; }

        [DataMember]
        public decimal? Opex4 { get; set; }

        [DataMember]
        public decimal? Opex5 { get; set; }
    }
}
