﻿using System;
using System.Runtime.Serialization;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class CartaFianzaDetalleRecuperoResponse
    {
        [DataMember]
        public int IdCartaFianzaDetalleSeguimiento { set; get; }
        [DataMember]
        public int IdCartaFianza { set; get; }
        [DataMember]
        public int IdColaborador { set; get; }
        [DataMember]
        public int IdColaboradorACargo { set; get; }
        [DataMember]
        public int IdEstadoRecuperacionCartaFianzaTm { set; get; }
        [DataMember]
        public string Comentario { set; get; }
        [DataMember]
        public int IdEstado { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioCreacion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaCreacion { set; get; }
        [DataMember]
        public Nullable<int> IdUsuarioEdicion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaEdicion { set; get; }

    }
}
