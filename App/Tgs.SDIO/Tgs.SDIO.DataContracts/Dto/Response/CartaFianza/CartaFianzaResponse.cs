﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.DataContracts.Dto.Response.CartaFianza
{
    [DataContract]
    public class CartaFianzaResponse
    {

       
            [DataMember]
            public int IdCartaFianza { set; get; }

            [DataMember]
            public int IdCliente { set; get; }
            [DataMember]
            public string NumeroIdentificadorFiscal { set; get; }
            [DataMember]
            public string NombreCliente { set; get; }

            [DataMember]
            public string NumeroOportunidad { set; get; }
            [DataMember]
            public int IdTipoContratoTm { set; get; }
            [DataMember]
            public string NumeroContrato { set; get; }
            [DataMember]
            public string Processo { set; get; }
            [DataMember]
            public string DescripcionServicioCartaFianza { set; get; }
            [DataMember]
            public Nullable<DateTime> FechaFirmaServicioContrato { set; get; }
            [DataMember]
            public Nullable<DateTime> FechaFinServicioContrato { set; get; }
            [DataMember]
            public int IdEmpresaAdjudicadaTm { set; get; }
            [DataMember]
            public int IdTipoGarantiaTm { set; get; }
            [DataMember]
            public string NumeroGarantia { set; get; }

            [DataMember]
            public int IdTipoMonedaTm { set; get; }
            [DataMember]
            public decimal ImporteCartaFianzaSoles { set; get; }
            [DataMember]
            public decimal ImporteCartaFianzaDolares { set; get; }
            [DataMember]
            public int IdBanco { set; get; }

        /***/


            [DataMember]
            public int ClienteEspecial { set; get; }
            [DataMember]
            public int SeguimientoImportante { set; get; }
            [DataMember]
            public string Observacion { set; get; }
            [DataMember]
            public string Incidencia { set; get; }
            [DataMember]
            public int IdEstado { set; get; }
            [DataMember]
        public Nullable<int> IdUsuarioCreacion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaCreacion { set; get; }
            [DataMember]
        public Nullable<int> IdUsuarioEdicion { set; get; }
        [DataMember(EmitDefaultValue = true)]
        public Nullable<DateTime> FechaEdicion { set; get; }

    }
}
