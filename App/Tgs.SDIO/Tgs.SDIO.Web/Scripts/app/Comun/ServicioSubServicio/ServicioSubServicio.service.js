﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ServicioSubServicioService', ServicioSubServicioService);

    ServicioSubServicioService.$inject = ['$http', 'URLS'];

    function ServicioSubServicioService($http, $urls) {

        var service = {
            //Servicios
            ListarServicioSubServicio: ListarServicioSubServicio,
            ObtenerServicioSubServicio: ObtenerServicioSubServicio,
            RegistrarServicioSubServicio: RegistrarServicioSubServicio,
            ActualizarServicioSubServicio: ActualizarServicioSubServicio,
            ListaServicioSubServicioPaginado: ListaServicioSubServicioPaginado,
            InhabilitarServicioSubServicio:InhabilitarServicioSubServicio,
            EliminarServicioSubServicio: EliminarServicioSubServicio,
            ListarServicioSubServicioGrupos: ListarServicioSubServicioGrupos
        };

        return service;
        //ServicioSubServicios
        
        function EliminarServicioSubServicio(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/EliminarServicioSubServicio",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
         function InhabilitarServicioSubServicio(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/InhabilitarServicioSubServicio",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListarServicioSubServicio(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/ListarServicioSubServicio",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ObtenerServicioSubServicio(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/ObtenerServicioSubServicio",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function RegistrarServicioSubServicio(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/RegistrarServicioSubServicio",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ActualizarServicioSubServicio(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/ActualizarServicioSubServicio",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function ListaServicioSubServicioPaginado(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/ListaServicioSubServicioPaginado",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        
        function ListarServicioSubServicioGrupos(ServicioSubServicio) {

            return $http({
                url: $urls.ApiComun + "ServicioSubServicio/ListarServicioSubServicioGrupos",
                method: "POST",
                data: JSON.stringify(ServicioSubServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }

})();