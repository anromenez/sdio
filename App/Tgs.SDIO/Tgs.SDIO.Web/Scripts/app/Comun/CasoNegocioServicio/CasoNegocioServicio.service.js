﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('CasoNegocioServicioService', CasoNegocioServicioService);

    CasoNegocioServicioService.$inject = ['$http', 'URLS'];

    function CasoNegocioServicioService($http, $urls) {

        var service = {
            ListarCasoNegocioServicio : ListarCasoNegocioServicio
        };

        return service;

        function ListarCasoNegocioServicio(casoServicio) {

            return $http({
                url: $urls.ApiComun + "CasoNegocioServicio/ListarCasoNegocioServicio",
                method: "POST",
                data: JSON.stringify(casoServicio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };



    }

})();