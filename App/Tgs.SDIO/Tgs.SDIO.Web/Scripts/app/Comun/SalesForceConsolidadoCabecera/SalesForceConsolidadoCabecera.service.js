﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('SalesForceConsolidadoCabeceraService', SalesForceConsolidadoCabeceraService);

    SalesForceConsolidadoCabeceraService.$inject = ['$http', 'URLS'];

    function SalesForceConsolidadoCabeceraService($http, $urls) {

        var service = {
            //Servicios
            ListarProbabilidades: ListarProbabilidades
        };

        return service;


      //Probabilidades
        function ListarProbabilidades() {

            return $http({
                url: $urls.ApiComun + "Comun/ListarProbabilidades",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                for (index = 0; index <= resultado.data.length -1 ; ++index) {
                    if (resultado.data[index].ProbabilidadExito == null)
                    {
                        resultado.data[index].ProbabilidadExito = -1;
                        break;
                    }
                }

                return resultado;
            }
        };

    }

})();


