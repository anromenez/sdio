﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('LineaNegocioService', LineaNegocioService);

    LineaNegocioService.$inject = ['$http', 'URLS'];

    function LineaNegocioService($http, $urls) {

        var service = {
            ListarLineaNegocios: ListarLineaNegocios
        };

        return service;

        function ListarLineaNegocios(linea) {

            return $http({
                url: $urls.ApiComun + "LineaNegocio/ListarLineaNegocios",
                method: "POST",
                data: JSON.stringify(linea)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();