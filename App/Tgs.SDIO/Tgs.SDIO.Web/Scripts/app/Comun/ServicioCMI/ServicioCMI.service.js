﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ServicioCMIService', ServicioCMIService);

    ServicioCMIService.$inject = ['$http', 'URLS'];

    function ServicioCMIService($http, $urls) {

        var service = {
            ListarServicioCMIPorLineaNegocio: ListarServicioCMIPorLineaNegocio
        };

        return service;

        function ListarServicioCMIPorLineaNegocio(servicioCMI) {
        debugger
            return $http({
                url: $urls.ApiComun + "ServicioCMI/ListarServicioCMIPorLineaNegocio",
                method: "POST",
                data: JSON.stringify(servicioCMI)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();