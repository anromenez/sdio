﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('MedioService', MedioService);

    MedioService.$inject = ['$http', 'URLS'];

    function MedioService($http, $urls) {

        var service = {
            ListarMedio: ListarMedio
        };

        return service;

        //Medio
        function ListarMedio(medio) {

            return $http({
                url: $urls.ApiComun + "Medio/ListarMedio",
                method: "POST",
                data: JSON.stringify(medio)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
       
      
       

    }

})();