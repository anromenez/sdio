﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('DepreciacionService', DepreciacionService);

    DepreciacionService.$inject = ['$http', 'URLS'];

    function DepreciacionService($http, $urls) {

        var service = {
            //Depreciacion
            ListarDepreciacion: ListarDepreciacion
        };

        return service;

        function ListarDepreciacion(Depreciacion) {

            return $http({
                url: $urls.ApiComun + "Depreciacion/ListarDepreciacion",
                method: "POST",
                data: JSON.stringify(Depreciacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();