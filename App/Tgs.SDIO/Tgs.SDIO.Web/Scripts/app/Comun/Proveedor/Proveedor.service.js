﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ProveedorService', ProveedorService);

    ProveedorService.$inject = ['$http', 'URLS'];

    function ProveedorService($http, $urls) {

        var service = {

            //Proveedor
            ListarProveedor: ListarProveedor
        };

        return service;

        //Proveedor
        function ListarProveedor(proveedor) {

            return $http({
                url: $urls.ApiComun + "Proveedor/ListarProveedor",
                method: "POST",
                data: JSON.stringify(proveedor)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();