﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('ClienteService', ClienteService);

    ClienteService.$inject = ['$http', 'URLS'];

    function ClienteService($http, $urls) {

        var service = {      

            ObtenerCliente: ObtenerCliente,
            ListarClientePorDescripcion: ListarClientePorDescripcion ,
            ListarCliente:ListarCliente
        };

        return service;

        //Cliente  
        function ObtenerCliente(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ObtenerCliente",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarClientePorDescripcion(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarClientePorDescripcion",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };
     

        function ListarCliente(cliente) {

            return $http({
                url: $urls.ApiComun + "Cliente/ListarCliente",
                method: "POST",
                data: JSON.stringify(cliente)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();