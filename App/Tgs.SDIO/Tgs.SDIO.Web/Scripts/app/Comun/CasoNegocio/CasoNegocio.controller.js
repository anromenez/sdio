﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('CasoNegocioController', CasoNegocioController);

    CasoNegocioController.$inject = ['CasoNegocioService', 'LineaNegocioService', 'RegistrarCasoNegocioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function CasoNegocioController(CasoNegocioService, LineaNegocioService, RegistrarCasoNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.BuscarCasoNegocio = BuscarCasoNegocio;
        vm.CargaModalCasoNegocio = CargaModalCasoNegocio;
        vm.EliminarCasoNegocio = EliminarCasoNegocio;
        vm.ListLineaNegocio = [];
        vm.IdLineaNegocio = '-1';
        vm.Descripcion = '';
        vm.IdCasoNegocio = 0;
        /******************************************* Tabla - Caso Negocio *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceCasoNegocio = {};
        vm.dtColumnsCasoNegocio = [

         DTColumnBuilder.newColumn('IdLineaNegocio').notVisible(),
         DTColumnBuilder.newColumn('IdCasoNegocio').notVisible(),
         DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
         DTColumnBuilder.newColumn('FlagDefecto').withTitle('Predeterminado').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCasoNegocio)

        ];

        function AccionesCasoNegocio(data, type, full, meta) {

            var respuesta = "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm' id='tab-button' data-toggle='modal' ng-click='vm.CargaModalCasoNegocio(\"" + data.IdLineaNegocio + "\",\"" + data.IdCasoNegocio + "\");'>" + "<span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-click='vm.EliminarCasoNegocio(\"" + data.IdLineaNegocio + "\",\"" + data.IdCasoNegocio + "\");' >" + "<span class='glyphicon dripicons-trash'></span></a></div>";
            return respuesta;
        };

        function BuscarCasoNegocio() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCasoNegocio = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarCasoNegocioPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCasoNegocioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var casonegocio = {
                IdLineaNegocio: vm.IdLineaNegocio,
                IdEstado: 1,
                Descripcion: vm.Descripcion,
            };

            var promise = CasoNegocioService.ListarCasoNegocioBandeja(casonegocio);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
                $("[id$='frmBandejaCasoNegocio']")[0].style.height = "";
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCasoNegocio = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
            $("[id$='frmBandejaCasoNegocio']")[0].style.height = "";
        };


        function CargaModalCasoNegocio(IdLineaNegocio, IdCasoNegocio) {

            vm.IdCasoNegocio = IdCasoNegocio;
            vm.IdLineaNegocio = IdLineaNegocio;

            var dto = {
                IdLineaNegocio: IdLineaNegocio,
                IdCasoNegocio: IdCasoNegocio
            };

            var promise = RegistrarCasoNegocioService.ModalCasoNegocio(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCasoNegocio").html(content);
                });

                $('#ModalCasoNegocio').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        /******************************************* Metodos *******************************************/

        function ListarLineaNegocio() {

            var linea = {
                IdEstado: 1
            };
            var promise = LineaNegocioService.ListarLineaNegocios(linea);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });
        }

        function EliminarCasoNegocio(IdLineaNegocio, IdCasoNegocio) {

            var casoNegocio = {
                IdCasoNegocio: IdCasoNegocio,
                IdLineaNegocio: IdLineaNegocio
            };

            if (confirm('¿Estas seguro de eliminar el Caso de Negocio?')) {
                var promise = CasoNegocioService.EliminarCasoNegocioBandeja(casoNegocio);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#alertCasoNegocio', 'danger', Respuesta.Mensaje, 20);
                    } else {

                        UtilsFactory.Alerta('#alertCasoNegocio', 'success', Respuesta.Mensaje, 10);
                        BuscarCasoNegocio();
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#alertCasoNegocio', 'danger', MensajesUI.DatosError, 5);
                });
            }
            else {
                blockUI.stop();
            }
        }

        /******************************************* Load *******************************************/

        ListarLineaNegocio();
        BuscarCasoNegocio();

    }

})();