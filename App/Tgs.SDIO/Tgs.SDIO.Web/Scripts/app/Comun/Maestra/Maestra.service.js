﻿(function () {
    'use strict',
     angular
    .module('app.Comun')
    .service('MaestraService', MaestraService);

    MaestraService.$inject = ['$http', 'URLS'];

    function MaestraService($http, $urls) {

        var service = {
          
            ListarMaestraPorIdRelacion: ListarMaestraPorIdRelacion
        };

        return service;

        //Maestra 
        function ListarMaestraPorIdRelacion(maestra) {

            return $http({
                url: $urls.ApiComun + "Maestra/ListarMaestraPorIdRelacion",
                method: "POST",
                data: JSON.stringify(maestra)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }

})();