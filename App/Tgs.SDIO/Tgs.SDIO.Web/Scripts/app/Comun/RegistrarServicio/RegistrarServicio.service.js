﻿(function () {
	'use strict',
     angular
    .module('app.Comun')
    .service('RegistrarServicioService', RegistrarServicioService);

	RegistrarServicioService.$inject = ['$http', 'URLS'];

	function RegistrarServicioService($http, $urls) {

		var service = {

			ModalServicio: ModalServicio
		};

		return service;

		function ModalServicio() {

			return $http({
				url: $urls.ApiComun + "RegistrarServicio/Index",
				method: "GET"
			}).then(DatosCompletados);

			function DatosCompletados(resultado) {
				return resultado;
			}
		};

	}

})();