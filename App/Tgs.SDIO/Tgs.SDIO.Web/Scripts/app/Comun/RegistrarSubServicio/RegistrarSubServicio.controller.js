﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('RegistrarSubServicio', RegistrarSubServicio);

    RegistrarSubServicio.$inject = ['RegistrarSubServicioService', 'MaestraService', 'SubServicioService', 'DepreciacionService',
    'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarSubServicio(RegistrarSubServicioService, MaestraService, SubServicioService, DepreciacionService,
        blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;


        vm.CargaModalSubServicio = CargaModalSubServicio;
        vm.ListarMaestraPorIdRelacion = ListarMaestraPorIdRelacion;
        vm.ListarDepreciacion = ListarDepreciacion;
        vm.RegistrarSubServicio = RegistrarSubServicio;
        vm.ActualizarSubServicio = ActualizarSubServicio;
        vm.ObtenerSubServicio = ObtenerSubServicio;

        vm.ListTipoServicio = [];
        vm.ListDepreciacion = [];


        vm.DescripcionNuevo = "";
        vm.CostoInstalacion = "";
        vm.IdtipoServicio = "-1";
        vm.IdDepreciacion = "-1";
        vm.IdtipoSubServicio = "-1";
        vm.IdSubServicio = 0;

        vm.Descripcion = '';

        vm.MensajeDeAlerta = "";



        CargaModalSubServicio();




        function CargaModalSubServicio() {
            vm.IdSubServicio = $scope.$parent.vm.IdSubServicio;
            (vm.IdSubServicio > 0) ? ObtenerSubServicio() : CargaModal();


        }
        function CargaModal() {
            vm.DescripcionNuevo = "";
            vm.CostoInstalacion = "";
            vm.IdtipoServicio = "-1";
            vm.IdDepreciacion = "-1";
            ListarMaestraPorIdRelacion();
            ListarDepreciacion();
        }



        function ListarMaestraPorIdRelacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 32

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();
            });
        }

        function RegistrarSubServicio() {


            blockUI.start();

            if (vm.IdDepreciacion == "-1") {

                vm.IdDepreciacion = null;
            }
            if (vm.CostoInstalacion == "") {

                vm.CostoInstalacion = 0;
            }
            var subServicio = {
                IdSubServicio: vm.IdSubServicio,
                CostoInstalacion: vm.CostoInstalacion,
                Descripcion: vm.DescripcionNuevo,
                IdTipoSubServicio: vm.IdtipoServicio,
                IdDepreciacion: vm.IdDepreciacion

            };
            var promise = (vm.IdSubServicio > 0) ? SubServicioService.ActualizarSubServicio(subServicio) : SubServicioService.RegistrarSubServicio(subServicio);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                debugger
                vm.DescripcionNuevo = "";
                vm.CostoInstalacion = "";
                vm.IdtipoServicio = "-1";
                vm.IdDepreciacion = "-1";
                UtilsFactory.Alerta('#divAlert_Registrar', 'success', Respuesta.Mensaje, 5);


            }, function (response) {
                blockUI.stop();

            });
        }
        function ObtenerSubServicio() {
            blockUI.start();

            var subServicio = {
                IdSubServicio: vm.IdSubServicio
            }

            var promise = SubServicioService.ObtenerSubServicio(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                CargaModal();
                vm.CostoInstalacion = Respuesta.CostoInstalacion;
                vm.DescripcionNuevo = Respuesta.Descripcion;
                if (Respuesta.IdDepreciacion == null || Respuesta.IdDepreciacion == 0) {
                    vm.IdDepreciacion = "-1";
                }
                else {
                    vm.IdDepreciacion = Respuesta.IdDepreciacion;
                }
                if (Respuesta.IdTipoSubServicio == null || Respuesta.IdTipoSubServicio == 0) {
                    vm.IdtipoServicio = "-1";
                }
                else {
                    vm.IdtipoServicio = Respuesta.IdTipoSubServicio;
                }
                vm.IdSubServicio = Respuesta.IdSubServicio;



            }, function (response) {
                blockUI.stop();

            });
        }



        function ActualizarSubServicio() {

            var promise = SubServicioService.ActualizarSubServicio(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                UtilsFactory.Alerta('#divAlert', 'success', "Se actualizo con exito registro.", 5);


            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarDepreciacion() {

            var depreciacion = {
                IdEstado: 1

            };
            var promise = DepreciacionService.ListarDepreciacion(depreciacion);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListDepreciacion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function LimpiarModal() {

        }






    }
})();








