﻿(function () {
    'use strict'

    angular
    .module('app.Comun')
    .controller('BandejaServicio', BandejaServicio);

    BandejaServicio.$inject = ['BandejaServicioService', 'MedioService', 'LineaNegocioService', 'SubServicioService', 'ServicioSubServicioService', 'ProveedorService', 'MaestraService', 'ServicioService', 'DepreciacionService'
        , 'ServicioCMIService'
        , 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];

    function BandejaServicio(BandejaServicioService, MedioService, LineaNegocioService, SubServicioService, ServicioSubServicioService, ProveedorService, MaestraService, ServicioService, DepreciacionService,
        ServicioCMIService
       , blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {

        var vm = this;

        vm.BuscarSubServicio = BuscarSubServicio;
        vm.BuscarServicio = BuscarServicio;
        vm.EliminarServicio = EliminarServicio;
        vm.EliminarSubServicio = EliminarSubServicio;
        vm.AceptarEliminarServicio = AceptarEliminarServicio;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.CargaModalServicio = CargaModalServicio;
        vm.ListarMaestraPorIdRelacion = ListarMaestraPorIdRelacion;
        vm.ListarDepreciacion = ListarDepreciacion;
        vm.RegistrarServicio = RegistrarServicio;
        vm.RegistrarSubServicioPorServicio = RegistrarSubServicioPorServicio;
        vm.ActualizarServicio = ActualizarServicio;
        vm.ObtenerServicio = ObtenerServicio;
        vm.ObtenerSubServicio = ObtenerSubServicio;


        vm.ListarPestanaGrupoServicio = ListarPestanaGrupoServicio;
        vm.ListGrupoServicio = [];
        vm.IdGrupoServicio = "-1";

        vm.ListarPestanaGrupoSubServicio = ListarPestanaGrupoSubServicio;
        vm.ListGrupoSubServicio = [];
        vm.IdGrupoSubServicio = "-1";

        vm.ListarLineaNegocios = ListarLineaNegocios;
        vm.ListLineaNegocio = [];
        vm.IdLineaNegocio = "-1";


        vm.ListarEnlace = ListarEnlace;
        vm.ListEnlace = [];
        vm.IdEnlace = "-1";

        vm.ListarLocalidad = ListarLocalidad;
        vm.ListLocalidad = [];
        vm.IdLocalidad = "-1";

        vm.ListarMoneda = ListarMoneda;
        vm.ListMoneda = [];
        vm.IdMoneda = "-1";


        vm.ListarEstado = ListarEstado;
        vm.ListEstado = [];
        vm.IdEstado = "-1";

        vm.ListarProveedor = ListarProveedor;
        vm.ListProveedor = [];
        vm.IdProveedor = "-1";

        vm.ListarMedio = ListarMedio;
        vm.ListMedio = [];
        vm.IdMedio = "-1";


        vm.ListarTipoCosto = ListarTipoCosto;
        vm.ListTipoCosto = [];
        vm.IdTipoCosto = "-1";

        vm.ListarPeriodo = ListarPeriodo;
        vm.ListPeriodo = [];
        vm.IdPeriodo = "-1";

        vm.ListarServicioCMI = ListarServicioCMI;
        vm.ListServicioCMI = [];
        vm.IdServicioCMI = "-1";


        vm.ListSubServicio = [];

        vm.IdSisego = "-1";
        vm.IdServicioSubServicio = 0;
        vm.IdServicioSubServicioPorservicio = 0;
        vm.ListTipoServicio = [];
        vm.ListDepreciacion = [];
        vm.costo = costo;
        vm.convertToPounds = convertToPounds;
        vm.DescripcionNuevo = "";
        vm.CostoInstalacion = "";
        vm.DescripcionContratoMarco = "";


        vm.Ponderado = 100;
        vm.Inicio = 1;
        vm.ContratoMarco = "";


        vm.IdtipoServicio = "-1";
        vm.IdDepreciacion = "-1";
        vm.IdtipoServicio = "-1";

        vm.IdServicio = 0;
        vm.IdSubServicio = "-1";


        vm.Descripcion = '';

        vm.MensajeDeAlerta = "";

        LimpiarGrilla();
        LimpiarGrillaSub();
        vm.dtInstance = {};

        vm.dtColumns = [

           DTColumnBuilder.newColumn('IdServicio').withTitle('Codigo').notSortable(),
          DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),


         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaServicio)

        ];

        function AccionesBusquedaServicio(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal' data-target='#exampleModalCenter_Nuevo'   onclick='ObtenerServicio(" + data.IdServicio + ");'>" + "<span class='fa fa-edit'></span></a></div> " +
        "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarServicio(" + data.IdServicio + ");' >" + "<span class='glyphicon glyphicon-trash'></span></a></div>";

        };

        function BuscarServicio() {

            blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarServicioPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarServicioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var Servicio = {
                Descripcion: vm.Descripcion,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ServicioService.ListaServicioPaginado(Servicio);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListServicioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


        vm.dtInstanceServicio = {};

        vm.dtColumnsServicio = [




        DTColumnBuilder.newColumn('DescripcionSubServicio').withTitle('Sub Servicio').notSortable(),
        DTColumnBuilder.newColumn('DescripcionTipoCosto').withTitle('Tipo Costo').notSortable(),
        //DTColumnBuilder.newColumn('DescripcionMoneda').withTitle('DescripcionMoneda').notSortable(),
        //DTColumnBuilder.newColumn('DescripcionPeriodo').withTitle('DescripcionPeriodo').notSortable(),
        DTColumnBuilder.newColumn('DescripcionGrupo').withTitle('Grupo').notSortable(),
        DTColumnBuilder.newColumn('DescripcionProveedor').withTitle('Proveedor').notSortable(),
        DTColumnBuilder.newColumn('DescripcionFlagSISEGO').withTitle('SISEGO').notSortable(),
        DTColumnBuilder.newColumn('ContratoMarco').withTitle('Contrato Marco').notSortable(),
        //DTColumnBuilder.newColumn('Inicio').withTitle('Inicio').notSortable(),
        //DTColumnBuilder.newColumn('Ponderacion').withTitle('Ponderacion').notSortable(),



         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSubServicio)

        ];

        function AccionesBusquedaSubServicio(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn '  onclick='ObtenerSubServicio(" + data.IdServicioSubServicio + ");'>" + "<span class='fa fa-edit'></span></a></div> " +
       "<div class='col-xs-4 col-sm-1'><a title='Eliminar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarSubServicio(" + data.IdServicioSubServicio + ");' >" + "<span class='glyphicon glyphicon-trash'></span></a></div>";

        };


        function ObtenerSubServicio(IdServicioSubServicio) {

            blockUI.start();
            var subServicio = {
                IdServicioSubServicio: IdServicioSubServicio,
                IdEstado: 1
            };
            var promise = ServicioSubServicioService.ObtenerServicioSubServicio(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;
                CargaModalServicio();

                if (Respuesta.IdGrupo == null) {
                    Respuesta.IdGrupo = "-1";
                }
                if (Respuesta.IdSubServicio == null) {
                    Respuesta.IdSubServicio = "-1";
                }
                if (Respuesta.FlagSISEGO == null) {
                    Respuesta.FlagSISEGO = "-1";
                }
                if (Respuesta.IdTipoCosto == null) {
                    Respuesta.IdTipoCosto = "-1";
                }

                vm.IdServicioSubServicio = IdServicioSubServicio;

                vm.IdGrupoSubServicio = Respuesta.IdGrupo;
                vm.IdSubServicio = Respuesta.IdSubServicio;
                //   Respuesta.IdLineaNegocio
                vm.IdSisego = Respuesta.FlagSISEGO;
                vm.IdTipoCosto = Respuesta.IdTipoCosto;

                vm.IdProveedor = Respuesta.IdProveedor;
                vm.ContratoMarco = Respuesta.ContratoMarco;

                vm.IdPeriodo = Respuesta.IdPeriodos;
                vm.Inicio = Respuesta.Inicio;

                vm.Ponderado = Respuesta.Ponderacion;
                vm.IdMoneda = Respuesta.IdMoneda;

                BuscarSubServicio();

            }, function (response) {
                blockUI.stop();

            });
        };

        function BuscarSubServicio() {

            blockUI.start();
            LimpiarGrillaSub()
            $timeout(function () {
                vm.dtOptionsServicio = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarSubServicioPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        };

        function BuscarSubServicioPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var Servicio = {
                IdServicio: vm.IdServicio,
                Indice: pageNumber,
                Tamanio: length
            };

            var promise = ServicioSubServicioService.ListaServicioSubServicioPaginado(Servicio);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListServicioSubServicioDtoResponse
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrillaSub()
            });
        };

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);

        };
        function LimpiarGrillaSub() {


            vm.dtOptionsServicio = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);
        };
        //



        function costo() {

        };
        function convertToPounds(str) {
            var n = Number.parseFloat(str);
            if (!str || isNaN(n) || n < 0) return 0;
            return n.toFixed(2);
        }

        function LimpiarModalServicioSubServicio() {

            vm.ContratoMarco = "";
            vm.IdSubServicio = "-1";
            vm.IdGrupoSubServicio = "-1";
            vm.IdSisego = "-1";
            vm.IdMoneda = "-1";
            vm.IdProveedor = "-1";
            vm.IdTipoCosto = "-1";
            vm.IdPeriodo = "-1";
            vm.Inicio = 1;
            vm.Ponderado = 100;
            vm.IdServicioSubServicio = 0;


        }

        function CargaModalServicio() {

            vm.Servicio="";
            vm.IdLineaNegocio= "-1";

            vm.DescripcionNuevo = "";
            vm.IdSubServicio = "-1";
            vm.ListGrupoServicio = [];
            vm.IdGrupoServicio = "-1";

            vm.ListGrupoSubServicio = [];
            vm.IdGrupoSubServicio = "-1";

            vm.ListEnlace = [];
            vm.IdEnlace = "-1";

            vm.ListLocalidad = [];
            vm.IdLocalidad = "-1";

            vm.ListMoneda = [];
            vm.IdMoneda = "-1";

            vm.ListEstado = [];
            vm.IdEstado = "-1";

            vm.ListProveedor = [];
            vm.IdProveedor = "-1";


            vm.ListMedio = [];
            vm.IdMedio = "-1";

            vm.ListTipoCosto = [];
            vm.IdTipoCosto = "-1";

            vm.ListPeriodo = [];
            vm.IdPeriodo = "-1";

            vm.ListSubServicio = [];
            vm.IdServicio = 0;
            vm.IdServicioSubServicio = 0;
            vm.IdServicioSubServicioPorservicio = 0;
            ListarSubServicios();
            ListarEnlace();
            ListarMedio();
            ListarProveedor();
            ListarLocalidad();
            ListarMoneda();
            ListarEstado();
            ListarPeriodo();
            ListarTipoCosto();
            ListarPestanaGrupoServicio();
            ListarPestanaGrupoSubServicio();
            ListarServicioCMI();
            BuscarSubServicio();
            ListarLineaNegocios();

        }

        function EliminarServicio(IdServicio) {




            var servicio = {
                IdServicio: IdServicio
            }

            debugger
            var promise = ServicioService.InhabilitarServicio(servicio);

            promise.then(function (resultado) {
                blockUI.stop();

                UtilsFactory.Alerta('#divAlert_Eliminar', 'success', "Se Inhabilito  el registro.", 5);
                BuscarServicio();
            }, function (response) {

                blockUI.stop();
            });


            //vm.btnEliminar = true;
            //$("#idServicio").val("");
            //var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            //modalpopupConfirm.modal('show');
            //$("#idServicio").val(IdServicio);
        }

        function AceptarEliminarServicio() {
            var ddlIdServicio = angular.element(document.getElementById("idServicio"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var Servicio = {
                IdServicio: ddlIdServicio.val()
            }
            blockUI.start();
            var promise = BandejaServicioService.EliminarServicio(Servicio);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarServicio()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });

        }
        function EliminarSubServicio(IdServicioSubServicio) {

            var subServicio = {
                IdServicioSubServicio: IdServicioSubServicio
            }

            debugger
            var promise = ServicioSubServicioService.EliminarServicioSubServicio(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();

                UtilsFactory.Alerta('#divAlert_2', 'success', "Se eliminar el registro.", 5);
                BuscarSubServicio();
            }, function (response) {

                blockUI.stop();
            });

        }
        function ListarLineaNegocios() {

            var lineaNegocio = {
                IdEstado: 1

            };
            var promise = LineaNegocioService.ListarLineaNegocios(lineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarSubServicios() {

            var subServicio = {
                IdEstado: 1

            };
            var promise = SubServicioService.ListarSubServicios(subServicio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListSubServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarProveedor() {

            var proveedor = {
                IdEstado: 1

            };
            var promise = ProveedorService.ListarProveedor(proveedor);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListProveedor = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarMedio() {
            debugger
            var medio = {
                IdEstado: 1

            };
            var promise = MedioService.ListarMedio(medio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarPestanaGrupoServicio() {

            var pestanaGrupo = {
                IdEstado: 1,
                FlagServicio: 1
            };
            debugger
            var promise = BandejaServicioService.ListarPestanaGrupo(pestanaGrupo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListGrupoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarPestanaGrupoSubServicio() {

            var pestanaGrupo = {
                IdEstado: 1,
                FlagServicio: 0
            };
            debugger
            var promise = BandejaServicioService.ListarPestanaGrupo(pestanaGrupo);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListGrupoSubServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarServicioCMI() {

            var servicioCMI = {
                IdEstado: 1,
                IdLineaNegocio: 5

            };
            var promise = ServicioCMIService.ListarServicioCMIPorLineaNegocio(servicioCMI);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListServicioCMI = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }


        function ListarMaestraPorIdRelacion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 14

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarEnlace() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 89

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListEnlace = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarLocalidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 95
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLocalidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarMoneda() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 98
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListMoneda = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }

        function ListarEstado() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 92
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListEstado = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarTipoCosto() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 153
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListTipoCosto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        function ListarPeriodo() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 28
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListPeriodo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }


        function RegistrarServicio() {
            blockUI.start();
            var estado=vm.IdSevicio;
            var servicio = {
                IdServicio: vm.IdServicio,
                IdLineaNegocio: vm.IdLineaNegocio,
                Descripcion: vm.Servicio,
                IdGrupo: vm.IdGrupoServicio,
                IdMedio: vm.IdMedio,
                IdTipoEnlace: vm.IdEnlace,
                IdPeriodo: vm.IdPeriodo,
                IdServicioCMI: vm.IdServicioCMI


            };

            var promise = (vm.IdServicio == 0) ? ServicioService.RegistrarServicio(servicio) : ServicioService.ActualizarServicio(servicio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                debugger
                vm.IdServicio = Respuesta.Id;


                RegistrarServicioDetalle();

            }, function (response) {
                blockUI.stop();

            });
        }
           function RegistrarServicioDetalle() {
            blockUI.start();
       
            var serviciosubservicio = {

                IdServicioSubServicio: vm.IdServicioSubServicioPorservicio,
                IdServicio: vm.IdServicio,
                IdGrupo: vm.IdGrupoServicio

            };

            var promise = (vm.IdServicioSubServicioPorservicio > 0) ? ServicioSubServicioService.ActualizarServicioSubServicio(serviciosubservicio) : ServicioSubServicioService.RegistrarServicioSubServicio(serviciosubservicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;

                UtilsFactory.Alerta('#divAlert', 'success', "Se registro con exito.", 5);


                BuscarServicio();
            }, function (response) {
                blockUI.stop();

            });
        }





        function RegistrarSubServicioPorServicio() {
            blockUI.start();


            var serviciosubservicio = {

                IdServicioSubServicio: vm.IdServicioSubServicio,
                IdServicio: vm.IdServicio,
                IdSubServicio: vm.IdSubServicio,
                IdTipoCosto: vm.IdTipoCosto,
                IdProveedor: vm.IdProveedor,
                ContratoMarco: vm.ContratoMarco,
                IdPeriodos: vm.IdPeriodo,
                Inicio: vm.Inicio,
                Ponderacion: vm.Ponderado,
                IdGrupo: vm.IdGrupoSubServicio,
                IdMoneda: vm.IdMoneda,
                FlagSISEGO: vm.IdSisego



            };

            var promise = (vm.IdServicioSubServicio > 0) ? ServicioSubServicioService.ActualizarServicioSubServicio(serviciosubservicio) : ServicioSubServicioService.RegistrarServicioSubServicio(serviciosubservicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;

                //   vm.IdServicio = Respuesta.IdServicio;
                LimpiarModalServicioSubServicio();

                UtilsFactory.Alerta('#divAlert_2', 'success', Respuesta.Mensaje, 5);


                BuscarSubServicio();
            }, function (response) {
                blockUI.stop();

            });
        }

        function ObtenerServicio(IdServicio) {
            blockUI.start();
            var Servicio = {
                IdServicio: IdServicio

            };
            var promise = ServicioService.ObtenerServicio(Servicio);

            promise.then(function (resultado) {
                blockUI.stop();
                debugger
                var Respuesta = resultado.data;
                CargaModalServicio();

                if (Respuesta.IdGrupo == null) {
                    Respuesta.IdGrupo = "-1";
                }
                if (Respuesta.IdMedio == null) {
                    Respuesta.IdMedio = "-1";
                }
                if (Respuesta.IdTipoEnlace == null) {
                    Respuesta.IdTipoEnlace = "-1";
                }
                if (Respuesta.IdServicioCMI == null) {
                    Respuesta.IdServicioCMI = "-1";
                }
                if (Respuesta.IdLineaNegocio == null) {
                    Respuesta.IdLineaNegocio = "-1";
                }
                vm.IdServicioSubServicioPorservicio = Respuesta.IdServicioSubServicio;
                vm.IdServicio = Respuesta.IdServicio;
                vm.Servicio = Respuesta.Descripcion;
                vm.IdGrupoServicio = Respuesta.IdGrupo;
                vm.IdLineaNegocio = Respuesta.IdLineaNegocio;
                //   Respuesta.IdLineaNegocio
                vm.IdMedio = Respuesta.IdMedio;
                vm.IdEnlace = Respuesta.IdTipoEnlace;
                vm.IdServicioCMI = Respuesta.IdServicioCMI;

                BuscarSubServicio();

            }, function (response) {
                blockUI.stop();

            });
        }

        function ActualizarServicio() {

            var Servicio = {
                IdServicio: vm.IdServicio,
                CostoInstalacion: vm.CostoInstalacion,
                Descripcion: vm.DescripcionNuevo,
                IdTipoServicio: vm.IdtipoServicio,
                IdDepreciacion: vm.IdDepreciacion

            };
            var promise = ServicioService.ActualizarServicio(Servicio);

            promise.then(function (resultado) {
                blockUI.stop();


                var Respuesta = resultado.data;
                vm.IdServicio = Respuesta.Id,
                UtilsFactory.Alerta('#divAlert', 'success', "Se actualizo el registro con exito.", 5);
                debugger
                BuscarServicio();
                BuscarSubServicio();

            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarDepreciacion() {

            var depreciacion = {
                IdEstado: 1

            };
            var promise = DepreciacionService.ListarDepreciacion(depreciacion);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListDepreciacion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function LimpiarModal() {
            var ddlEstado = angular.element(document.getElementById("ddlEstado"));
            vm.txtDescripcion = '';
            vm.txtOrdenVisual = '';
            vm.btnActualizar = false;
            vm.btnAgregar = true;
        }


        function LimpiarFiltros() {
            vm.Descripcion = '';
            vm.IdtipoServicio = "-1";
            LimpiarGrilla();
        }

    }
})();



