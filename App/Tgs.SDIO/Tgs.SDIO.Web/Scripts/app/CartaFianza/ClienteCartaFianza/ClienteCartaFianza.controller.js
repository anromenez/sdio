﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('ClienteCartaFianzaController', ClienteCartaFianzaController);

    ClienteCartaFianzaController.$inject = ['ClienteCartaFianzaService','ClienteService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function ClienteCartaFianzaController(ClienteCartaFianzaService,ClienteService, blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.BuscarClienteCartaFianza = BuscarClienteCartaFianza;
        vm.SelecionarClienteId = SelecionarClienteId;

        vm.NumeroIdentificadorFiscal = '';
        vm.NombreCliente = '';
        vm.IdCliente = 0;
        vm.IdEstado = 1;

        vm.dataMasiva = [];
        vm.dtBuscarClienteInstanceCartaFianza = {};



        vm.dtBuscarClienteColumnsCartaFianza = [
        DTColumnBuilder.newColumn('IdCliente').notVisible(), 
        DTColumnBuilder.newColumn('NumeroIdentificadorFiscal').withTitle('RUC/NIF').notSortable().withOption('width', '20%'),
        DTColumnBuilder.newColumn('Descripcion').withTitle('Cliente').withOption('width', '70%'),

       // DTColumnBuilder.newColumn('NumeroOportunidad').withTitle('Numero Opportunidad').notSortable(),

       DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza).withOption('width', '10%')
        ];

        function AccionesCartaFianza(data, type, full, meta) {

            var respuesta = "<div class='col-xs-6 col-sm-6'><a title='Selecionar' class='btn btn-primary'   id='tab-button' class='btn ' ng-click='vm.SelecionarClienteId(\"" + data.IdCliente + "\",\"" + data.NumeroIdentificadorFiscal + "\",\"" + data.Descripcion + "\");'>" + "<span class='glyphicon glyphicon-edit'></span></a></div> ";
            return respuesta;
        };



        function BuscarClienteCartaFianza() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCartaFianza = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarCartaFianzaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var clienteCartaFianza = {
                NumeroIdentificadorFiscal: vm.bClienteNumeroIdentificador,
                Descripcion: vm.bClienteNombreCliente,
                IdEstado: vm.IdEstado
            };

            var promise = ClienteService.ListarCliente(clienteCartaFianza);
            promise.then(function (response) {
                //  debugger;
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
               // $("[id$='frmCartaFianzaBCliente']")[0].style.height = "";
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianza = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
           // $("[id$='frmCartaFianzaBCliente']")[0].style.height = "";
        };



        BuscarClienteCartaFianza();

        function SelecionarClienteId(idCliente, numeroIdentificadorFiscal, descripcion)
        {
            //debugger;
            vm.NumeroIdentificadorFiscal = numeroIdentificadorFiscal;
            vm.NombreCliente = descripcion;
            vm.IdCliente = idCliente;

            $scope.$parent.vm.NombreCliente = vm.NombreCliente;
            $scope.$parent.vm.NumeroIdentificador = vm.NumeroIdentificadorFiscal,
              $scope.$parent.vm.IdCliente = vm.IdCliente;
           

         

        }


    }

})();