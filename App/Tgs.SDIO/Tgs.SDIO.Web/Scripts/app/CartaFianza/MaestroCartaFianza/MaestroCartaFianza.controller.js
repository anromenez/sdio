﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('MaestroCartaFianzaController', MaestroCartaFianzaController);

    MaestroCartaFianzaController.$inject = ['MaestroCartaFianzaService','ClienteCartaFianzaService', 'RegistroCartaFianzaService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function MaestroCartaFianzaController(MaestroCartaFianzaService,ClienteCartaFianzaService, RegistroCartaFianzaService,  blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector)
    {

        var vm = this;

        vm.BuscarCartaFianza = BuscarCartaFianza;
        vm.CargaModalCartaFianzaRegistro = CargaModalCartaFianzaRegistro;
        vm.CargaModalCartaFianzaId = CargaModalCartaFianzaId;
        vm.CargaModalCartaBuscarCliente = CargaModalCartaBuscarCliente;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.ListaCartaFianza = [];
        vm.IdCartaFianza = 0;
        vm.Descripcion = '';
        vm.IdCliente = 0;
        vm.Id = 0;
        var RegCartaFianza = [];
        /******************************************* Tabla - Registro de Carta Fianza *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceCartaFianza = {};

        vm.NumeroIdentificadorFiscal = '';
        vm.NumeroContrato = '';
        vm.NombreCliente = '';

        vm.dtColumnsCartaFianza = [

         DTColumnBuilder.newColumn('IdCartaFianza').notVisible(),
         DTColumnBuilder.newColumn('IdCliente').notVisible(),
         DTColumnBuilder.newColumn(null).withTitle('RUC/NIF'),
         DTColumnBuilder.newColumn('NombreCliente').withTitle('Cliente'),

         DTColumnBuilder.newColumn('NumeroOportunidad').withTitle('Numero Opportunidad'),
         DTColumnBuilder.newColumn('NumeroContrato').withTitle('Numero Contrato'),
         
         DTColumnBuilder.newColumn(null).withTitle('Tipo Contrato'),
       
         DTColumnBuilder.newColumn('Processo').withTitle('Processo').notSortable(),
        // DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
         DTColumnBuilder.newColumn('FechaFirmaServicioContratostr').withTitle('Fecha Firma Servicio').notSortable(),
         DTColumnBuilder.newColumn('FechaFinServicioContratostr').withTitle('Fecha Fin Servicio').notSortable(),
        
        DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza)
        ];
     
        function AccionesCartaFianza(data, type, full, meta) {

            var respuesta = "<div class='col-xs-6 col-sm-6'> <a title='Editar' class='btn btn-primary'   id='tab-button' class='btn '  ng-click='vm.CargaModalCartaFianzaId(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");'>" + "<span class='glyphicon glyphicon-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-6 col-sm-6'> <a title='Accion' class='btn btn-primary'   id='tab-button' class='btn '   ng-click='vm.EliminarCartaFianza(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");' >" + "<span class='glyphicon glyphicon-trash'></span></a></div>";
            respuesta = respuesta + "<d<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '   ng-click='vm.DetalleCartaFianza(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");' >" + "<span class='glyphicon glyphicon-trash'></span></a></div>";
            return respuesta;
        };

        function BuscarCartaFianza() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCartaFianza = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarCartaFianzaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var cartaFianza = {
                IdCartaFianza: vm.IdCartaFianza,
                NumeroContrato: vm.NumeroContrato,
                NombreCliente: vm.NombreCliente
            };

            var promise = MaestroCartaFianzaService.ListarCartaFianza(cartaFianza);

            promise.then(function (response) {
              //  debugger;
                var records = {                   
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
                $("[id$='frmCartaFianza']")[0].style.height = "";
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianza = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
            $("[id$='frmCartaFianza']")[0].style.height = "";
        };

      
        function CargaModalCartaFianzaRegistro() {
            vm.IdCartaFianza = 0;
            vm.IdCliente = 0;
              var dto = {
                IdCartaFianza: 0,
                IdCliente: 0
            };

            var promise = RegistroCartaFianzaService.ModalCartaFianzaRegistrar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianza").html(content);
                });

                $('#ModalCartaFianza').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        }; 

        /******************************************* Metodos *******************************************/

        function CargaModalCartaFianzaId(IdCliente, IdCartaFianza) {
            vm.IdCartaFianza = IdCartaFianza;
            vm.IdCliente = IdCliente;
             RegCartaFianza = {
                IdCartaFianza   :   IdCartaFianza,
                IdCliente   :   IdCliente
            };
            var promise = RegistroCartaFianzaService.ModalCartaFianzaRegistrar(RegCartaFianza);
                       
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianza").html(content);
                });

                $('#ModalCartaFianza').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

         }



       function CargaModalCartaBuscarCliente() {
        
                 var   RegCartaFianza = {
                        IdCartaFianza: 0,
                        IdCliente: 0
                    };

            var promise = ClienteCartaFianzaService.ModalCartaFianzaBuscarCliente(RegCartaFianza);

            promise.then(function (response) {

                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianzaCliente").html(content);
                });

                $('#ModalCartaFianzaCliente').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
             /* */
        }

        function LimpiarFiltros() {
            vm.NumeroIdentificadorFiscal = '';
            vm.NumeroContrato = '';
            vm.NombreCliente = '';
        }

      

        /******************************************* Load *******************************************/
      //  CargaModalCartaFianzaRegistro();
      
        BuscarCartaFianza();
        //CargaModalCartaFianzaId();

    }

})();