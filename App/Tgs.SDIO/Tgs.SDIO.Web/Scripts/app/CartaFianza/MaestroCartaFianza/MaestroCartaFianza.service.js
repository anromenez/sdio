﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('MaestroCartaFianzaService', MaestroCartaFianzaService);

    MaestroCartaFianzaService.$inject = ['$http', 'URLS'];

    function MaestroCartaFianzaService($http, $urls) {

        var service = {
            ListarCartaFianza: ListarCartaFianza
            //RegistrarCartaFianza: RegistrarCartaFianza,
            //ActualizarCartaFianza: ActualizarCartaFianza,
            //DeleteCartaFianza: DeleteCartaFianza
        };

        return service;

        function ListarCartaFianza(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "MaestroCartaFianza/ListarCartaFianza",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };



    }

})();