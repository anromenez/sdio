﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('RegistrarCartaFianzaController', RegistrarCartaFianzaController);

    RegistrarCartaFianzaController.$inject = ['CartaFianzaService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistrarCartaFianzaController(CartaFianzaService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;

        vm.BuscarCartaFianza = BuscarCartaFianza;
        vm.CargaModalCartaFianza = CargaModalCartaFianza;
        //vm.EliminarCartaFianza = EliminarCartaFianza;
        vm.ListaCartaFianza = [];
        vm.IdCartaFianza = '-1';
        vm.Descripcion = '';
        vm.IdCartaFianza = 0;
        /******************************************* Tabla - Registro de Carta Fianza *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceCartaFianza = {};
        vm.dtColumnsCartaFianza = [

         DTColumnBuilder.newColumn('IdCartaFianza').notVisible(),
         DTColumnBuilder.newColumn('IdCliente').notVisible(),
         DTColumnBuilder.newColumn('RUC/NIF').withTitle('RUC/NIF').notSortable(),
         DTColumnBuilder.newColumn('Cliente').withTitle('Cliente').notSortable(),

         DTColumnBuilder.newColumn('Numero Opportunidad').withTitle('Numero Opportunidad').notSortable(),
         DTColumnBuilder.newColumn('Numero Contrato').withTitle('Numero Contrato').notSortable(),
         
         DTColumnBuilder.newColumn('Tipo Contrato').withTitle('Tipo Contrato').notSortable(),
         
         DTColumnBuilder.newColumn('Processo').withTitle('Processo').notSortable(),
        // DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion').notSortable(),
         DTColumnBuilder.newColumn('Fecha Firma Servicio').withTitle('Fecha Firma Servicio').notSortable(),
         DTColumnBuilder.newColumn('Fecha Fin Servicio').withTitle('Fecha Fin Servicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesCartaFianza)

        ];

        function AccionesCartaFianza(data, type, full, meta) {

            var respuesta = "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm' id='tab-button' data-toggle='modal' ng-click='vm.CargaModalCartaFianza(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");'>" + "<span class='glyphicon dripicons-document-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-click='vm.EliminarCartaFianza(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");' >" + "<span class='glyphicon dripicons-trash'></span></a></div>";
            respuesta = respuesta + "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm'  id='tab-button' ng-click='vm.DetalleCartaFianza(\"" + data.IdCliente + "\",\"" + data.IdCartaFianza + "\");' >" + "<span class='glyphicon dripicons-trash'></span></a></div>";
            return respuesta;
        };

        function BuscarCartaFianza() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCartaFianza = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarCartaFianzaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCartaFianzaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var casonegocio = {
                IdLineaNegocio: vm.IdLineaNegocio,
                IdEstado: 1,
                Descripcion: vm.Descripcion,
            };

            var promise = CartaFianzaService.ListarCartaFianza(casonegocio);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
                $("[id$='frmCartaFianza']")[0].style.height = "";
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsCartaFianza = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', true);
            $("[id$='frmCartaFianza']")[0].style.height = "";
        };


        function CargaModalCartaFianza(IdCliente, IdCartaFianza) {
            /*
            vm.IdCartaFianza = IdCartaFianza;
            vm.IdCliente = IdCliente;

            var dto = {
                IdLineaNegocio: IdLineaNegocio,
                IdCartaFianza: IdCartaFianza
            };

            var promise = RegistrarCartaFianzaService.ModalCartaFianza(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoCartaFianza").html(content);
                });

                $('#ModalCartaFianza').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });
            */

        };

        /******************************************* Metodos *******************************************/

        function ListarLineaNegocio() {
        /*
            var linea = {
                IdEstado: 1
            };
            var promise = LineaNegocioService.ListarLineaNegocios(linea);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });

            */
        }

      

        /******************************************* Load *******************************************/

        ListarLineaNegocio();
        BuscarCartaFianza();

    }

})();