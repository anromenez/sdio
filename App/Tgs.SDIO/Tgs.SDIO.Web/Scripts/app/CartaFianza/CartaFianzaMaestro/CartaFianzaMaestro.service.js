﻿(function () {
    'use strict',
     angular
    .module('app.CartaFianza')
    .service('CartaFianzaService', CartaFianzaService);

    CartaFianzaService.$inject = ['$http', 'URLS'];

    function CartaFianzaService($http, $urls) {

        var service = {
            ListarCartaFianza: ListarCartaFianza
            //RegistrarCartaFianza: RegistrarCartaFianza,
            //ActualizarCartaFianza: ActualizarCartaFianza,
            //DeleteCartaFianza: DeleteCartaFianza
        };

        return service;

        function ListarCartaFianza(cartaFianza) {

            return $http({
                url: $urls.ApiCartaFianza + "CartaFianza/ListarCartaFianza",
                method: "POST",
                data: JSON.stringify(cartaFianza)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };



    }

})();