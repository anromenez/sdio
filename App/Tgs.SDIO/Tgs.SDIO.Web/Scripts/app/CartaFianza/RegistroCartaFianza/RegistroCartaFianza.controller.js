﻿(function () {
    'use strict'

    angular
    .module('app.CartaFianza')
    .controller('RegistroCartaFianzaController', RegistroCartaFianzaController);

    RegistroCartaFianzaController.$inject = ['RegistroCartaFianzaService',  'MaestraService', 'blockUI', 'UtilsFactory', 'DTColumnDefBuilder', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RegistroCartaFianzaController(RegistroCartaFianzaService,  MaestraService, blockUI, UtilsFactory, DTColumnDefBuilder, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {

        var vm = this;
        vm.IdCliente = 0;
        vm.IdCartaFianza = 0;
        vm.Id = 0;

        vm.ListTipoProyecto = [];
        vm.CmbTipoContratoTm = '-1';
       
        vm.ListTipoAccion = [];
        vm.CmbTipoAccion = '-1';

        vm.ListEmpresaAdjudicada = [];
        vm.CmbEmpresaAdjudicadaTm = '-1';

        vm.ListTipoGarantia = [];
        vm.CmbTipoGarantiaTm = '-1';

        vm.ListEntidadBancaria = [];
        vm.CmbBanco = '-1';

        vm.ListTipoMoneda = [];
        vm.CmbTipoMoneda = '-1';

        vm.regCartaFianza = [];
        
        vm.GrabarCartaFianza = GrabarCartaFianza;
        vm.BuscarCartaFianzaId = BuscarCartaFianzaId;
       // vm.CargaModalCartaBuscarCliente = CargaModalCartaBuscarCliente;

        /******************************************* Load *******************************************/

     //   ListarLineaNegocio();
        //  BuscarCartaFianza();
        
        function GrabarCartaFianza() {
          //  debugger;
            var cartafianza = {
                IdCartaFianza   : vm.IdCartaFianza,
                IdCliente : 1,//vm.IdCliente,
                NumeroOportunidad: vm.NumeroOportunidad,
                IdTipoContratoTm: vm.CmbTipoContratoTm,
                NumeroContrato: vm.NumeroContrato,
                Processo: vm.Processo,
                DescripcionServicioCartaFianza: vm.DescripcionServicioCartaFianza,
                FechaFirmaServicioContrato: vm.FechaFirmaServicioContrato,
                FechaFinServicioContrato: vm.FechaFinServicioContrato,
                IdEmpresaAdjudicadaTm: vm.CmbEmpresaAdjudicadaTm,
                IdTipoGarantiaTm: vm.CmbTipoGarantiaTm,
                NumeroGarantia: vm.NumeroGarantia,
                IdTipoMonedaTm: vm.CmbTipoMoneda,
                ImporteCartaFianzaSoles: vm.ImporteCartaFianzaSoles,
                ImporteCartaFianzaDolares: vm.ImporteCartaFianzaDolares,
                IdBanco: vm.CmbBanco,
                IdTipoAccionTm  :   vm.CmbTipoAccion,
                //IdEstadoVencimientoTm   :   vm.IdEstadoVencimientoTm,
                //IdEstadoCartaFianzaTm   :   vm.IdEstadoCartaFianzaTm
                //IdRenovarTm :   vm.IdRenovarTm,
                //IdEstadoRecuperacionCartaFianzaTm
                ClienteEspecial: vm.ClienteEspecial,
                SeguimientoImportante: vm.SeguimientoImportante,
                Observacion: vm.Observacion,
                Incidencia: vm.Incidencia,
                IdEstado: vm.IdEstado,
                IdUsuarioCreacion: vm.IdUsuarioCreacion

            };
            
            var promise = (vm.IdCartaFianza == 0) ? RegistroCartaFianzaService.InsertarRegistroCartaFianza(cartafianza) : RegistroCartaFianzaService.ActualizarCartaFianza(cartafianza);
            promise.then(function (response) {
                 debugger;
                blockUI.stop();

                var Respuesta = response.data;

                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                } else {
                    vm.Id = Respuesta.Id;
                    if (vm.Id != 0) {
                       // InactivarCampos();
                    }
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 5);
                   // $scope.$parent.vm.BuscarCartaFianza();
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });

        }
        
        /************Carga Combos**********/
        function CargarTipoContrato() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 186
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoContrato = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoAccion() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 200
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoAccion = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
      
        function CargarEmpresaAdjudicada() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 189
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEmpresaAdjudicada = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoGarantia() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 194

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoGarantia = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarEntidadBancaria() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 239

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListEntidadBancaria = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarTipoMoneda() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 98

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoMoneda = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        /************end Carga Combos**********/


        function BuscarCartaFianzaId() {
            if ($scope.$parent.vm.IdCliente != 0) {

                vm.Nombrecliente = $scope.$parent.vm.NombreCliente;
                vm.IdentificadorFiscal = $scope.$parent.vm.NumeroIdentificador;
                vm.IdCliente = $scope.$parent.vm.IdCliente;
            } else {
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', 'Selecionar Un Cliente Primero', 20);
            }
        

            if ($scope.$parent.vm.IdCartaFianza != 0) {


                //debugger;
                var RegCartaFianza = {
                    IdCartaFianza: $scope.$parent.vm.IdCartaFianza,
                    IdCliente: $scope.$parent.vm.IdCliente
                };
               // console.log(RegCartaFianza);
      

                var promise = RegistroCartaFianzaService.ListarCartaFianzaId(RegCartaFianza);

                promise.then(function (resultado) {
                    blockUI.stop();
                    //debugger;
                    var Respuesta = resultado.data;
                    // vm.ListTipoMoneda = UtilsFactory.AgregarItemSelect(Respuesta);
                
                        vm.IdCartaFianza = Respuesta[0].IdCartaFianza;
                        vm.CmbTipoAccion = Respuesta[0].IdTipoAccionTm;
                        vm.NumeroOportunidad = Respuesta[0].NumeroContrato;
                        vm.CmbTipoContratoTm = Respuesta[0].IdTipoContratoTm;
                        vm.NumeroContrato = Respuesta[0].NumeroContrato;
                        vm.Processo = Respuesta[0].Processo;
                        vm.DescripcionServicioCartaFianza = Respuesta[0].DescripcionServicioCartaFianza;
                        vm.FechaFirmaServicioContrato = Respuesta[0].FechaFirmaServicioContratostr;
                        vm.FechaFinServicioContrato = Respuesta[0].FechaFinServicioContratostr;
                        vm.CmbEmpresaAdjudicadaTm = Respuesta[0].IdEmpresaAdjudicadaTm;
                        vm.NumeroGarantia = Respuesta[0].NumeroGarantia;
                        vm.CmbBanco = Respuesta[0].IdBanco;
                        vm.CmbTipoMoneda = Respuesta[0].IdTipoMonedaTm;
                        vm.ImporteCartaFianzaSoles = Respuesta[0].ImporteCartaFianzaSoles;
                        vm.ImporteCartaFianzaDolares = Respuesta[0].ImporteCartaFianzaDolares;
                        vm.Incidencia = Respuesta[0].Incidencia;
                        vm.Observacion = Respuesta[0].Observacion;
                        vm.ClienteEspecial = Respuesta[0].ClienteEspecial;
                        vm.SeguimientoImportante = Respuesta[0].SeguimientoImportante;
                
             

                   // vm.DescripcionServicioCartaFianza = "aaaaaaaaaaaaa";

                }, function (response) {
                    blockUI.stop();

                });
            }
        }

      
        

        /************Inicializa Carga Combos**********/
        CargarTipoGarantia();
        CargarEmpresaAdjudicada();
        CargarTipoAccion();
        CargarTipoContrato();
        CargarEntidadBancaria();
        CargarTipoMoneda();
        /************Inicializa Carga Combos**********/

        BuscarCartaFianzaId();

    }



})();