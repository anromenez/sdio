﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('RouterSondaController', RouterSondaController);

    RouterSondaController.$inject = ['RouterSondaService', 'SubServicioDatosService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function RouterSondaController(RouterSondaService, SubServicioDatosService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.idRSonda = 0;
        vm.cantidad = 0;
        vm.instalacion = 0;
        vm.desinstalacion = 0;
        vm.precioUnitario = 0;
        vm.alquiler = 0;

        vm.GrabarRouterSonda = GrabarRouterSonda;
        
        vm.ListTipo = [];
        vm.IdTipo = "-1";

        /******************************************* RouterSonda *******************************************/

        function GrabarRouterSonda() {
            //CargarCombos();
            //alert("vm.IdMedio: " + vm.IdMedio);
            //alert("vm.ListTipoEnlace[0]: " + vm.ListTipoEnlace[0]);
        }

        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {

        };

        function CargarRouterSonda() {
            var dto = {
                IdSubServicioDatosCaratula: vm.idOPEX,
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdEstado: 1,
                IdGrupo: 3
            };

            var promise = SubServicioDatosService.ObtenerSubServicioDatos(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.cantidad = respuesta.Cantidad;
                vm.instalacion = respuesta.Instalacion;
                vm.desinstalacion = respuesta.Desinstalacion;
                vm.precioUnitario = respuesta.MontoUnitarioMensual;
                vm.alquiler = respuesta.Alquiler;
                vm.cantidad = respuesta.Cantidad;
                vm.IdTipo = respuesta.IdTipoCosto;//validar
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarVista() {
            vm.idRSonda = $scope.$parent.vm.idGenerico;
            vm.idOportunidadLineaNegocio = $scope.$parent.vm.idOportunidadLineaNegocio;

            if (vm.idRSonda > 0) {
                CargarRouterSonda();
            }
        };

        function CargarCombos() {
            //Tipo
            var filtroTipo = {
                IdEstado: 1,
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarVista();
    }
})();



