﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('FlujoCajaService', FlujoCajaService);

    FlujoCajaService.$inject = ['$http', 'URLS'];

    function FlujoCajaService($http, $urls) {

        var service = {
            ModalFlujoCaja: ModalFlujoCaja,
            ListarAnoMesProyecto: ListarAnoMesProyecto,
            RegistrarOportunidadFlujoCaja: RegistrarOportunidadFlujoCaja,
            ActualizarOportunidadFlujoCaja: ActualizarOportunidadFlujoCaja,
            ObtenerOportunidadFlujoCaja: ObtenerOportunidadFlujoCaja,
            RegistrarOportunidadFlujoCajaConfiguracion: RegistrarOportunidadFlujoCajaConfiguracion,
            ActualizarOportunidadFlujoCajaConfiguracion: ActualizarOportunidadFlujoCajaConfiguracion,
            ObtenerOportunidadFlujoCajaConfiguracion: ObtenerOportunidadFlujoCajaConfiguracion,
            GeneraCasoNegocio: GeneraCasoNegocio,
            GeneraServicio: GeneraServicio,
            EliminarCasoNegocio: EliminarCasoNegocio,
            ListarDetalleOportunidadCaratula: ListarDetalleOportunidadCaratula,
            ListarDetalleOportunidadEcapex: ListarDetalleOportunidadEcapex,
            ObtenerDetalleOportunidadEcapex: ObtenerDetalleOportunidadEcapex,
            ListaOportunidadFlujoCajaConfiguracion: ListaOportunidadFlujoCajaConfiguracion,
            ActualizarPeriodoOportunidadFlujoCaja: ActualizarPeriodoOportunidadFlujoCaja
        };

        return service;

        function ModalFlujoCaja() {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/Index",
                method: "POST"
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ListarAnoMesProyecto(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarAnoMesProyecto",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function RegistrarOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };


        function RegistrarOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/RegistrarOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GeneraCasoNegocio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/GeneraCasoNegocio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function GeneraServicio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/GeneraServicio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
        function EliminarCasoNegocio(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/EliminarCasoNegocio",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleOportunidadCaratula(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarDetalleOportunidadCaratula",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleOportunidadEcapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListarDetalleOportunidadEcapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerDetalleOportunidadEcapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ObtenerDetalleOportunidadEcapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadFlujoCajaConfiguracion(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ListaOportunidadFlujoCajaConfiguracion",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarPeriodoOportunidadFlujoCaja(dto) {
            return $http({
                url: $urls.ApiOportunidad + "FlujoCaja/ActualizarPeriodoOportunidadFlujoCaja",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();