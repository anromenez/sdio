﻿
(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('FlujoCajaController', FlujoCajaController);

    FlujoCajaController.$inject = ['FlujoCajaService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$injector'];

    function FlujoCajaController(FlujoCajaService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $injector) {

        var vm = this;
        vm.flagOtros = false;
        vm.flagDuracion = false;
        vm.meses = [];
        vm.fechaInicio;
        vm.CostoPreOperativo = 0;
        //vm.CostoPreOperativoOtros = 2;

        vm.Test = Test;
        vm.CargaOtros = CargaOtros;
        //vm.EliminarMes = EliminarMes;
        vm.ListPeriodos = [];
        vm.IdPeriodo = '-1';
        vm.CargarPeriodos = CargarPeriodos;
        vm.ListTiempoProyecto = [];
        vm.IdTiempo = '-1';
        vm.IdInicioProyecto = '-1';
        vm.IdOportunidadLineaNegocio = $scope.$parent.$parent.vm.IdOportunidadLineaNegocio;
        vm.GrabarFlujoCajaConfiguracion = GrabarFlujoCajaConfiguracion;
        vm.Meses = 1;
        vm.ObtenerFlujoCajaConfiguracion = ObtenerFlujoCajaConfiguracion;

        vm.dtColumnsServicios = null;
        /******************************************* Meses *******************************************/
        vm.dataMasiva = [];
        vm.v = {};
        vm.dtColumnsServicios = [
         DTColumnBuilder.newColumn('IdFlujoCajaConfiguracion').notVisible(),
         DTColumnBuilder.newColumn('IdFlujoCaja').notVisible(),
         DTColumnBuilder.newColumn('Inicio').withTitle('Inicio').notSortable(),
         DTColumnBuilder.newColumn('Meses').withTitle('Meses').notSortable(),
         DTColumnBuilder.newColumn('Ponderacion').withTitle('Ponderacion').notSortable(),
         DTColumnBuilder.newColumn('CostoPreOperativo').withTitle('Costo PreOperativo').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesServicios)

        ];

        function AccionesServicios(data, type, full, meta) {

            var respuesta = "<div class='col-xs-4 col-sm-1'><a title='Cancelar' class='btn btn-sm' id='btnEliminarServicio' ng-click='vm.EliminarServicios(\"" + data.IdCasoNegocioServicio + "\",\"" + data.IdServicio + "\");' >" + "<span class='glyphicon dripicons-trash'></span></a></div>";
            return respuesta;
        };

        function BuscarConfiguraciones() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsServicios = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarConfiguracionesPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarConfiguracionesPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var flujocaja = {
                IdFlujoCaja: $scope.$parent.vm.IdFlujoCaja,
                IdEstado: 1
            };

            var promise = FlujoCajaService.ListaOportunidadFlujoCajaConfiguracion(flujocaja);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };
                blockUI.stop();
                fnCallback(records);
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        function LimpiarGrilla() {
            vm.dtOptionsServicios = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        };

        function EliminarMes(anho, mes) {
            var index = 0;
            var indexEncontrado = 0;
            var encontrado = false;
            vm.meses.forEach(function (element) {
                if (anho == element.Anho && mes == element.Mes) {
                    encontrado = true;
                    indexEncontrado = index;
                    return;
                }
                index++;
            });

            if (encontrado) {
                vm.meses.splice(indexEncontrado, 1);
            }

            vm.dtInstanceMeses.rerender();
        };


        /******************************************* Metodos *******************************************/

        function Test() {
            alert("idperiodo: " + vm.IdPeriodo);
            alert("calc: " + !(vm.IdPeriodo == "3" || vm.IdPeriodo == "4"));
        };

        function CargaOtros() {
            vm.flagOtros = vm.IdPeriodo == 4 ? true : false;
            vm.flagDuracion = vm.IdPeriodo == 1 ? true : false;

        };

        function CargarPeriodos() {

            var periodo = {
                IdRelacion: 28
            };

            var promise = MaestraService.ListarMaestraPorIdRelacion(periodo);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListPeriodos = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarAnoMes() {

            var proyecto = {
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio
            };
            console.log("ano-mes");
            console.log($scope.$parent.$parent.vm.IdOportunidadLineaNegocio);
            var promise = FlujoCajaService.ListarAnoMesProyecto(proyecto);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListTiempoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);
                vm.ListInicioProyecto = vm.ListTiempoProyecto;
            }, function (response) {
                blockUI.stop();

            });
        };

        function ObtenerFlujoCajaConfiguracion() {
            if ($scope.$parent.vm.IdFlujoCaja > 0) {
                var Id = $scope.$parent.vm.IdFlujoCajaConfiguracion;
                vm.IdPeriodo = $scope.$parent.vm.IdPeriodos;

                var flujocaja = {
                    IdFlujoCaja: $scope.$parent.vm.IdFlujoCaja,
                    IdFlujoCajaConfiguracion: Id,
                    IdGrupo: 6
                };

                if (vm.IdPeriodo < 4) {

                    var promise = FlujoCajaService.ObtenerOportunidadFlujoCajaConfiguracion(flujocaja);
                    promise.then(function (response) {
                        var Respuesta = response.data;
                        vm.Ponderacion = Respuesta.Ponderacion;
                        vm.CostoPreOperativo = Respuesta.CostoPreOperativo;
                        vm.IdInicioProyecto = Respuesta.Inicio;
                        vm.Meses = Respuesta.Meses;

                    }, function (response) {
                        blockUI.stop();

                    });

                } else {
                    BuscarConfiguraciones();
                }
            }

        };


        function GrabarFlujoCajaConfiguracion() {
            var Id = $scope.$parent.vm.IdFlujoCajaConfiguracion;

            var flujocaja = {
                IdFlujoCaja: $scope.$parent.vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: $scope.$parent.vm.IdOportunidadLineaNegocio,
                IdPeriodos: vm.IdPeriodo
            };

            var promise = FlujoCajaService.ActualizarPeriodoOportunidadFlujoCaja(flujocaja);
            promise.then(function (response) {


                var flujocajaConfiguracion = {
                    IdFlujoCaja: $scope.$parent.vm.IdFlujoCaja,
                    IdFlujoCajaConfiguracion: Id,
                    Ponderacion: vm.Ponderacion,
                    CostoPreOperativo: vm.CostoPreOperativo,
                    Inicio: vm.IdInicioProyecto,
                    Meses: vm.Meses
                };


                var promise = (Id > 0) ? FlujoCajaService.ActualizarOportunidadFlujoCajaConfiguracion(flujocajaConfiguracion) : FlujoCajaService.RegistrarOportunidadFlujoCajaConfiguracion(flujocajaConfiguracion);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta != 0) {
                        UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'danger', MensajesUI.DatosError, 20);
                    } else {
                        UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'success', Respuesta.Mensaje, 10);

                        if (vm.IdPeriodo == 4 && Id > 0) {
                            $scope.$parent.vm.IdFlujoCajaConfiguracion = 0;
                            BuscarConfiguraciones();
                        }
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'danger', MensajesUI.DatosError, 5);
                });


            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#lblAlerta_FlujoCaja', 'danger', MensajesUI.DatosError, 5);
            });

        };

        /******************************************* Load *******************************************/

        CargarPeriodos();
        CargarAnoMes();
        BuscarConfiguraciones();

    }

})();