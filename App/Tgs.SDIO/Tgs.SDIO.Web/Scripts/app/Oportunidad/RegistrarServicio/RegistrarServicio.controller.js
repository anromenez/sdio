﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarServicio', RegistrarServicio);

    RegistrarServicio.$inject = ['RegistrarServicioService', 'ComunService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope'];

    function RegistrarServicio(RegistrarServicioService, ComunService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope) {

        var vm = this;
        vm.RegistrarSubServicio = RegistrarSubServicio;
    
        vm.Descripcion = '';



         function RegistrarSubServicio() {
            blockUI.start();
            var subServicio = {

                CostoInstalacion: vm.CostoInstalacion,
                Descripcion:vm.DescripcionNuevo,
                IdTipoSubServicio:vm.IdtipoServicio,
                IdDepreciacion:vm.IdDepreciacion

        };
            var promise = ComunService.RegistrarSubServicio(subServicio);

        promise.then(function (resultado) {
            blockUI.stop();

            var Respuesta = resultado.data;



        }, function (response) {
            blockUI.stop();

        });
    }
    }
})();