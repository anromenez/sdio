﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SatelitalesService', SatelitalesService);

    SatelitalesService.$inject = ['$http', 'URLS'];

    function SatelitalesService($http, $urls) {

        var service = {
            ModalSatelitales: ModalSatelitales,
            CantidadSatelites: CantidadSatelites
        };

        return service;

        function ModalSatelitales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Satelitales/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadSatelites(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/CantidadCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }
})();