﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('CaratulaController', CaratulaController);

    CaratulaController.$inject = ['CaratulaService', 'OportunidadServicioCMIService', 'FlujoCajaService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function CaratulaController(CaratulaService, OportunidadServicioCMIService, FlujoCajaService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;

        var modalEnum = { "CIRCUITOS": 1, "MEDIO3G": 2, "OPEX": 3, "ROUTER": 4, "SOPEX": 5 };
        Object.freeze(modalEnum);

        vm.tituloModal = "";
        vm.idGenerico = 0;
        vm.idOportunidadLineaNegocio = 21;
        vm.IdOportunidadLineaNegocio = 2;

        vm.BuscarServicioCMI = BuscarServicioCMI;
        vm.CargaModal = CargaModal;

        vm.Test = Test;

        /******************************************* ServicioCMI *******************************************/
        vm.dataMasiva = [];
        vm.dtInstanceServicioCMI = {};

        vm.dtColumnsServicioCMI = [
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('GerenciaProducto').withTitle('Gerencia').notSortable(),
         DTColumnBuilder.newColumn('Linea').withTitle('Linea').notSortable(),
         DTColumnBuilder.newColumn('SubLinea').withTitle('SubLinea').notSortable(),
         DTColumnBuilder.newColumn('DescripcionCMI').withTitle('Servicio CMI').notSortable(),
         DTColumnBuilder.newColumn('Producto_AF').withTitle('Producto AF').notSortable(),
         DTColumnBuilder.newColumn('Porcentaje').withTitle('Porcentaje').notSortable(),
         DTColumnBuilder.newColumn('CentroCosto').withTitle('Centro de Costo').notSortable()
        ];

        function BuscarServicioCMI() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsServicioCMI = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarServicioCMIPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarServicioCMIPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdEstado: 1
            };

            var promise = OportunidadServicioCMIService.ListarServicioCMIPorLineaNegocio(dto);
            promise.then(function (response) {

                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
                //$("[id$='frmCaratula']")[0].style.height = "";
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


        /******************************************* Circuitos *******************************************/
        vm.dtInstanceCircuitos = {};

        vm.dtColumnsCircuitos = [
         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Servicio').notSortable(),
         //DTColumnBuilder.newColumn('Circuito').withTitle('N° Circuito').notSortable(),
         DTColumnBuilder.newColumn('IdMedio').withTitle('Medio').notSortable(),
         //DTColumnBuilder.newColumn('TipoEnlace').withTitle('Tipo Enlace').notSortable(),
         DTColumnBuilder.newColumn('Estado').withTitle('Activo/Pasivo').notSortable(),
         DTColumnBuilder.newColumn('Localidad').withTitle('Localidad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         //DTColumnBuilder.newColumn('BM').withTitle('BM').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaCircuitos)

        ];

        function AccionesBusquedaCircuitos(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.CargaModal(" + modalEnum.CIRCUITOS + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Eliminar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.EliminarCircuito()' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function BuscarCircuitos() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsCircuitos = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarCircuitosPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarCircuitosPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdGrupo: 4
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
            
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        /******************************************* Medio3G *******************************************/
        vm.dtInstanceMedio3G = {};

        vm.dtColumnsMedio3G = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('Circuito').withTitle('N° Circuito').notSortable(),
         DTColumnBuilder.newColumn('IdMedio').withTitle('Medio').notSortable(),
         DTColumnBuilder.newColumn('TipoEnlace').withTitle('Tipo Enlace').notSortable(),
         DTColumnBuilder.newColumn('Localidad').withTitle('Localidad').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         //DTColumnBuilder.newColumn('BW').withTitle('BW').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaMedio3G)

        ];

        function AccionesBusquedaMedio3G(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.CargaModal(" + modalEnum.MEDIO3G + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarMedio3G()' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function BuscarMedio3G() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsMedio3G = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarMedio3GPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarMedio3GPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdGrupo: 3
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
            
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        /******************************************* Opex *******************************************/
        vm.dtInstanceOpex = {};

        vm.dtColumnsOpex = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('Meses').withTitle('N° Meses').notSortable(),
         DTColumnBuilder.newColumn('MontoUnitarioMensual').withTitle('Monto Mensual').notSortable(),
         DTColumnBuilder.newColumn('MontoTotalMensual').withTitle('Monto Total Mensual').notSortable(),
         DTColumnBuilder.newColumn('NumeroMesInicioGasto').withTitle('N° mes Inicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaOpex)

        ];

        function AccionesBusquedaOpex(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' ng-click='vm.CargaModal(" + modalEnum.OPEX + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn 'ng-click='vm.EliminarOpex()' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function BuscarOpex() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsOpex = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarOpexPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarOpexPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdGrupo: 3
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
            
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


        /******************************************* Router Sonda *******************************************/
        vm.dtInstanceRouterSonda = {};

        vm.dtColumnsRouterSonda = [

         //DTColumnBuilder.newColumn('IdModelo').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('CodigoModelo').notVisible(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Equipo').notSortable(),
         DTColumnBuilder.newColumn('FlagRenovacion').withTitle('Cruce').notSortable(),
         DTColumnBuilder.newColumn('Instalacion').withTitle('A.E. Reducido').notSortable(),
         DTColumnBuilder.newColumn('Desinstalacion').withTitle('Tipo de Equipo').notSortable(),
         DTColumnBuilder.newColumn('PU').withTitle('Marca-Modelo').notSortable(),
         DTColumnBuilder.newColumn('Alquiler').withTitle('Antigüedad').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaRouterSonda)

        ];

        function AccionesBusquedaRouterSonda(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' ng-click='vm.CargaModal(" + modalEnum.ROUTER + ", " + data.IdSubServicioDatosCaratula + ")'>" + "<span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm'  id='tab-button' class='btn ' ng-click='vm.EliminarRouter()' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function BuscarRouterSonda() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsRouterSonda = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarRouterSondaPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarRouterSondaPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdGrupo: 4
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
            
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };


        /******************************************* Seguridad Opex *******************************************/
        vm.dtInstanceSeguridadOpex = {};

        vm.dtColumnsSeguridadOpex = [

         DTColumnBuilder.newColumn('IdSubServicioDatosCaratula').notVisible(),
         DTColumnBuilder.newColumn('IdServicioCMI').notVisible(),
         DTColumnBuilder.newColumn('SubServicio').withTitle('Servicio').notSortable(),
         DTColumnBuilder.newColumn('TipoEnlace').withTitle('Tipo').notSortable(),
         DTColumnBuilder.newColumn('Cantidad').withTitle('Cantidad').notSortable(),
         DTColumnBuilder.newColumn('Meses').withTitle('Número Meses').notSortable(),
         DTColumnBuilder.newColumn('MontoUnitarioMensual').withTitle('Monto Unitario').notSortable(),
         //DTColumnBuilder.newColumn('MontoPxQ').withTitle('MontoPxQ').notSortable(),
         DTColumnBuilder.newColumn('Factor').withTitle('Factor').notSortable(),
         DTColumnBuilder.newColumn('ValorCuota').withTitle('Valor Cuota').notSortable(),
         DTColumnBuilder.newColumn('NumeroMesInicioGasto').withTitle('Mes Inicio').notSortable(),
         DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaSeguridadOpex)

        ];

        function AccionesBusquedaSeguridadOpex(data, type, full, meta) {
            var respuesta = "<div class='col-xs-3 col-sm-3'> <a title='Editar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.CargaModal(" + modalEnum.SOPEX + ", " + data.IdSubServicioDatosCaratula + ");'>" + "<span class='fa fa-edit'></span></a></div> ";
            respuesta = respuesta + "<div class='col-xs-3 col-sm-3'><a title='Cancelar' class='btn btn-sm' id='tab-button' class='btn ' ng-click='vm.EliminarSOPEX()' >" + "<span class='fa fa-trash-o'></span></a></div>";
            return respuesta;
        };

        function BuscarSeguridadOpex() {

            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptionsSeguridadOpex = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarSeguridadOpexPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        };

        function BuscarSeguridadOpexPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;

            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdGrupo: 5
            };
            var promise = FlujoCajaService.ListarDetalleOportunidadCaratula(dto);
            promise.then(function (response) {
                var records = {
                    'draw': draw,
                    'recordsTotal': response.data.length,
                    'recordsFiltered': response.data.length,
                    'data': response.data
                };

                blockUI.stop();
                fnCallback(records);
            
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        };

        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {
            vm.dtOptionsServicioCMI = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);

            vm.dtOptionsCircuitos = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

            vm.dtOptionsMedio3G = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

            vm.dtOptionsOpex = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

            vm.dtOptionsRouterSonda = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

            vm.dtOptionsSeguridadOpex = DTOptionsBuilder
           .newOptions()
           .withOption('data', [])
           .withOption('bFilter', false)
           .withOption('responsive', true)
           .withOption('destroy', true)
           .withOption('order', [])
           .withDisplayLength(0)
           .withOption('paging', false);

        };

        function CargaModal(idTipoModal, idEntidad) {
            vm.idGenerico = idEntidad;

            switch (idTipoModal) {
                case modalEnum.CIRCUITOS: vm.tituloModal = "Circuitos"; break;
                case modalEnum.MEDIO3G: vm.tituloModal = "Medio 3G"; break;
                case modalEnum.OPEX: vm.tituloModal = "OPEX"; break;
                case modalEnum.ROUTER: vm.tituloModal = "Router Sonda"; break;
                case modalEnum.SOPEX: vm.tituloModal = "Seguridad OPEX"; break;
            }

            var promise = CaratulaService.ModalEditar(idTipoModal);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);

                    $("#ContenidoModal").html(content);
                });

                $('#ModalGenerico').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function Test(idOportunidadLineaNegocio, idUsuario, tipoFicha) {
            var dto = {
                IdOportunidadLineaNegocio: idOportunidadLineaNegocio,
                IdUsuarioCreacion: idUsuario,
                TipoFicha: tipoFicha
            };

            var promise = CaratulaService.Test(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                //$injector.invoke(function ($compile) {
                //    var div = $compile(respuesta);
                //    var content = div($scope);
                //    $("#ContenidoSeguridadOpex").html(content);
                //});
            }, function (response) {
                blockUI.stop();
            });

        };

        /******************************************* LOAD *******************************************/
        BuscarServicioCMI();
        BuscarCircuitos();
        BuscarMedio3G();
        BuscarOpex();
        BuscarRouterSonda();
        BuscarSeguridadOpex();
    }
})();



