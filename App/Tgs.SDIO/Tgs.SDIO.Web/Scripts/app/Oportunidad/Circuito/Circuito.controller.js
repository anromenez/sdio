﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('CircuitoController', CircuitoController);

    CircuitoController.$inject = ['CircuitoService', 'SubServicioDatosService', 'MedioService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function CircuitoController(CircuitoService, SubServicioDatosService, MedioService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.idCircuito = 0;
        vm.idFlujoCaja = 0;
        vm.idOportunidadLineaNegocio = 0;

        vm.servicio = "";
        vm.circuito = "";
        vm.cantidad = 0;
        vm.BW = "";

        vm.CargaModal = CargaModal;
        vm.GrabarCircuito = GrabarCircuito;
        vm.CerrarModal = CerrarModal;

        vm.ListMedio = [];
        vm.IdMedio = "-1";

        vm.ListTipoEnlace = [];
        vm.IdTipoEnlace = "-1";

        vm.ListEstado = [];
        vm.IdEstado = "-1";

        vm.ListLocalidad = [];
        vm.IdLocalidad = "-1";

        /******************************************* Circuito *******************************************/

        function GrabarCircuito() {
            //Actualizando FlujoCaja
            var dto = {
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdFlujoCaja: vm.idFlujoCaja,
                IdEstado: 1
            };

            var promise = (vm.Id > 0) ? CircuitoService.ActualizarCaso(dto) : RegistrarCasoNegocioService.RegistrarCaso(dto);
            
            //actualizando SubServicioDatosCaratula
            var dto = {
                Id: vm.idCircuito,
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                Cantidad: vm.cantidad,
                BW: vm.BW,
                //vm.servicio;
                //vm.circuito;
                IdMedio: vm.IdMedio,
                IdTipoEnlace: vm.IdTipoEnlace,
                vm.IdTipoEnlace = respuesta.IdTipoEnlace;
                vm.IdEstado = respuesta.IdEstado;
                vm.IdLocalidad = respuesta.IdLocalidad;
                vm.cantidad = respuesta.Cantidad;
                //vm.BW = "";
            };
            var promise = (vm.Id > 0) ? CircuitoService.ActualizarCaso(dto) : RegistrarCasoNegocioService.RegistrarCaso(dto);

            promise.then(function (response) {
                blockUI.stop();
                var Respuesta = response.data;

                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                } else {
                    vm.Id = Respuesta.Id;
                    if (vm.Id != 0) {
                        InactivarCampos();
                    }
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 5);
                    $scope.$parent.vm.Buscar();
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });
        }

        function CerrarModal() {
            vm.cantidad = vm.cantidad + 1;
        }

        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {

        };

        function CargaModal() {
            var dto = {
                IdTipoModal: idTipoModal
            };

            var promise = CaratulaService.ModalEditar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoModal").html(content);
                });

                $('#ModalGenerico').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };

        function CargarCircuito() {
            var dto = {
                IdSubServicioDatosCaratula: vm.idCircuito,
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdEstado: 1,
                IdGrupo: 3
            };

            var promise = SubServicioDatosService.ObtenerSubServicioDatos(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.servicio = respuesta.SubServicio;
                vm.circuito = respuesta.Circuito;
                vm.IdMedio = respuesta.IdMedio;
                vm.IdTipoEnlace = respuesta.IdTipoEnlace;
                vm.IdEstado = respuesta.IdEstado;
                vm.IdLocalidad = respuesta.IdLocalidad;
                vm.cantidad = respuesta.Cantidad;
                //vm.BW = "";
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarVista() {
            vm.idCircuito = $scope.$parent.vm.idGenerico;
            vm.idOportunidadLineaNegocio = $scope.$parent.vm.idOportunidadLineaNegocio;

            if (vm.idCircuito > 0) {
                CargarCircuito();
            }
        };

        function ListarMedio() {
            var medio = {
                IdEstado: 1
            };
            var promise = MedioService.ListarMedio(medio);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                //
            });
        };

        function CargarCombos() {
            //Medio
            ListarMedio();
            //TipoEnlace
            var filtroTipoEnlace = {
                IdEstado: 1,
                IdRelacion: 89
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoEnlace);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoEnlace = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });

            //Estado
            var filtroEstado = {
                IdEstado: 1,
                IdRelacion: 92
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroEstado);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListEstado = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
            //Localidad
            var filtroLocalidad = {
                IdEstado: 1,
                IdRelacion: 95
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroLocalidad);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListLocalidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarVista();
    }
})();



