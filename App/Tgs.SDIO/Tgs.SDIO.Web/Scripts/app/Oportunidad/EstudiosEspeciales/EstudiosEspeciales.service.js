﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('EstudiosEspecialesService', EstudiosEspecialesService);

    EstudiosEspecialesService.$inject = ['$http', 'URLS'];

    function EstudiosEspecialesService($http, $urls) {

        var service = {
            ModalEstudiosEspeciales: ModalEstudiosEspeciales,
            CantidadEquiposEstudiosEsp: CantidadEquiposEstudiosEsp,
            RegistrarEstudiosEspeciales: RegistrarEstudiosEspeciales
        };

        return service;

        function ModalEstudiosEspeciales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EstudiosEspeciales/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function CantidadEquiposEstudiosEsp(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EquiposEspeciales/CantidadEquiposEstudiosEsp",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function RegistrarEstudiosEspeciales(dto) {
            return $http({
                url: $urls.ApiOportunidad + "EstudiosEspeciales/RegistrarEstudiosEspeciales",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };


    }
})();