﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('EstudiosEspecialesController', EstudiosEspecialesController);

    EstudiosEspecialesController.$inject = ['EstudiosEspecialesService', 'LineaConceptoCmiService', 'FlujoCajaService', 'SubServicioDatosCapexService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function EstudiosEspecialesController(EstudiosEspecialesService, LineaConceptoCmiService, FlujoCajaService, SubServicioDatosCapexService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.CantidadEstudiosEspecialesComplete = CantidadEstudiosEspecialesComplete;
        vm.GrabarEstudiosEspeciales = GrabarEstudiosEspeciales;
        vm.CargarEstudioEspecial = CargarEstudioEspecial;
        vm.IdFlujoCaja = 0;
        vm.IdGrupo = 6;
        vm.Antiguedad = 2;
        vm.IdOportunidadLineaNegocio = 0;
        vm.IdSubServicioDatosCapex = 0;
        vm.IdFlujoCajaConfiguracion = 0;
        vm.IdPeriodos = 0;

        function CargarEstudioEspecial() {

            var dto = {
                IdSubServicioDatosCapex: $scope.$parent.vm.dto.IdSubServicioDatosCapex,
                IdGrupo: 6
            };

            var promise = FlujoCajaService.ObtenerDetalleOportunidadEcapex(dto);
            promise.then(function (response) {

                console.log(response.data);
                var result = response.data;

                vm.SubServicio = result.SubServicio;
                vm.Cruce = result.Cruce;
                vm.AEreducido = result.AEReducido;
                vm.Circuito = result.Circuito;
                vm.Sisego = result.SISEGO;
                vm.Antiguedad = result.MesesAntiguedad;
                vm.Cantidad = result.Cantidad;
                vm.CuAntiguo = result.CostoUnitarioAntiguo;
                vm.ValorResidual = result.ValorResidualSoles;
                vm.CuSoles = result.CostoUnitario;
                vm.CapexDolares = result.CapexDolares;
                vm.CapexSoles = result.CapexSoles;
                vm.TotalCapex = result.TotalCapex;
                vm.MesRq = result.MesRecupero;
                vm.MesComprometido = result.MesComprometido;
                vm.MesCertificado = result.MesCertificado;

                vm.IdFlujoCaja = result.IdFlujoCaja;
                vm.IdOportunidadLineaNegocio = result.IdOportunidadLineaNegocio;
                vm.IdSubServicioDatosCapex = result.IdSubServicioDatosCapex;
                vm.IdFlujoCajaConfiguracion = result.IdFlujoCajaConfiguracion;
                vm.IdPeriodos = result.IdPeriodos;
                $scope.$$childHead.vm.ObtenerFlujoCajaConfiguracion();

                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });

        };

        function GrabarEstudiosEspeciales() {

            var flujocaja = {
                IdFlujoCaja: vm.IdFlujoCaja,
                IdOportunidadLineaNegocio: vm.IdOportunidadLineaNegocio,
                Cantidad: vm.Cantidad,
                CostoUnitario: vm.CapexSoles
            };


            if (confirm('¿Estas seguro de grabar el registro ?')) {
                blockUI.start();

                var promise = FlujoCajaService.ActualizarOportunidadFlujoCaja(flujocaja);
                promise.then(function (response) {
                    blockUI.stop();
                    var Respuesta = response.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'danger', MensajesUI.DatosError, 20);
                    } else {

                        var capex = {
                            IdFlujoCaja: vm.IdFlujoCaja,
                            IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                            Cruce: vm.Cruce,
                            AEreducido: vm.AEreducido,
                            Circuito: vm.Circuito,
                            SISEGO: vm.Sisego,
                            Antiguedad: vm.Antiguedad,
                            Cantidad: vm.Cantidad,
                            CostoUnitarioAntiguo: vm.CuAntiguo,
                            ValorResidualSoles: vm.ValorResidual,
                            CostoUnitario: vm.CuSoles,
                            CapexDolares: vm.CapexDolares,
                            CapexSoles: vm.CapexSoles,
                            TotalCapex: vm.TotalCapex,
                            MesRecupero: vm.MesRq,
                            MesComprometido: vm.MesComprometido,
                            MesCertificado: vm.MesCertificado
                        };

                        var promise = SubServicioDatosCapexService.ActualizarSubServicioDatosCapex(capex);
                        promise.then(function (response) {

                            var Respuesta = response.data;
                            if (Respuesta.TipoRespuesta != 0) {
                                UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'danger', Respuesta.Mensaje, 20);
                            } else {

                                UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'success', Respuesta.Mensaje, 10);
                            }

                        }, function (response) {
                            blockUI.stop();
                            UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'danger', MensajesUI.DatosError, 5);
                        });
                    }
                }, function (response) {
                    blockUI.stop();
                    UtilsFactory.Alerta('#lblAlerta_EstudiosEspeciales', 'danger', MensajesUI.DatosError, 5);
                });
            };
        };




        function CantidadEstudiosEspecialesComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CuSoles,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CuAntiguiedad,
                MesesAntiguedad: vm.Antiguedad,
                Indice: vm.Indice
            };

            var promise = EstudiosEspecialesService.CantidadEquiposEstudiosEsp(dto);
            promise.then(function (response) {
                vm.valorResidualSoles = response.data.ValorResidualSoles;
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };

        CargarEstudioEspecial();

    }

})();