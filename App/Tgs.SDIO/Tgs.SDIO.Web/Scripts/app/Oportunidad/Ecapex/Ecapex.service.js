﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('EcapexService', EcapexService);

    EcapexService.$inject = ['$http', 'URLS'];

    function EcapexService($http, $urls) {

        var service = {
            ListarECapex: ListarECapex
            
        };

        return service;

        function ListarECapex(dto) {
            return $http({
                url: $urls.ApiOportunidad + "ECapex/ListarCapex",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };



    }
})();