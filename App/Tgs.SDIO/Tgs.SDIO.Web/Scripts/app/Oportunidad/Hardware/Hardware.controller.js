﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('HardwareController', HardwareController);

    HardwareController.$inject = ['HardwareService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function HardwareController(HardwareService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.IdOportunidadLineaNegocio = 2;
        vm.IdSubServicioDatosCapex = 73;
        vm.IdGrupo = 11;

        vm.CuSoles = 50;
        vm.Cantidad = 3;
        vm.Antiguiedad = 1;
        vm.CuAntiguiedad = 8;
        vm.MesesAntiguedad = 5;
        vm.ValorResidual = 0;
        vm.Indice = 1;
        vm.CapexDolares = 0;
        vm.CapexSoles = 0;
        vm.TotalCapex = 0;

        vm.CantidadHardwareComplete = CantidadHardwareComplete;

        function CantidadHardwareComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CuSoles,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CuAntiguiedad,
                MesesAntiguedad: vm.Antiguedad,
                Indice: vm.Indice
            };

            var promise = HardwareService.CantidadHardware(dto);
            promise.then(function (response) {
                vm.valorResidualSoles = response.data.ValorResidualSoles;
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };
    }

})();