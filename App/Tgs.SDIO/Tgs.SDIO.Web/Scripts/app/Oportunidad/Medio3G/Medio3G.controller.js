﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('Medio3GController', Medio3GController);

    Medio3GController.$inject = ['Medio3GService', 'SubServicioDatosService', 'MedioService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function Medio3GController(Medio3GService, SubServicioDatosService, MedioService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.idCircuito = 0;
        vm.idOportunidadLineaNegocio = 0;

        vm.servicio = "";
        vm.circuito = "";
        //vm.IdMedio = resultado.IdMedio;
        //vm.IdTipoEnlace = 1;
        //vm.IdEstado = resultado.IdEstado;
        //vm.IdLocalidad = 1;
        vm.cantidad = 0;
        vm.BW = "";

        vm.GrabarMedio3G = GrabarMedio3G;

        vm.ListMedio = [];
        vm.IdMedio = "-1";

        vm.ListTipoEnlace = [];
        vm.IdTipoEnlace = "-1";

        vm.ListLocalidad = [];
        vm.IdLocalidad = "-1";

        /******************************************* ServicioCMI *******************************************/

        function GrabarMedio3G() {
            var dto = {
                Id: vm.Id,
                IdMedio: vm.IdMedio,
                IdTipoEnlace: vm.IdTipoEnlace,
                IdLocalidad: vm.IdLocalidad
            };
            var promise = (vm.Id > 0) ? CircuitoService.ActualizarCaso(dto) : RegistrarCasoNegocioService.RegistrarCaso(dto);

            promise.then(function (response) {
                blockUI.stop();
                var Respuesta = response.data;

                if (Respuesta.TipoRespuesta != 0) {
                    UtilsFactory.Alerta('#divAlertRegistrar', 'danger', Respuesta.Mensaje, 20);
                } else {
                    vm.Id = Respuesta.Id;
                    if (vm.Id != 0) {
                        InactivarCampos();
                    }
                    UtilsFactory.Alerta('#divAlertRegistrar', 'success', Respuesta.Mensaje, 5);
                    $scope.$parent.vm.BuscarCasoNegocio();
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlertRegistrar', 'danger', MensajesUI.DatosError, 5);
            });
        }

        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {

        };

        function CargarMedio3G() {
            var dto = {
                IdSubServicioDatosCaratula: vm.idCircuito,
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdEstado: 1,
                IdGrupo: 3
            };

            var promise = SubServicioDatosService.ObtenerSubServicioDatos(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.servicio = respuesta.SubServicio;
                //vm.circuito = "";
                vm.IdMedio = respuesta.IdMedio;
                vm.IdTipoEnlace = respuesta.IdTipoEnlace;
                vm.IdEstado = respuesta.IdEstado;
                vm.IdLocalidad = respuesta.IdLocalidad;
                vm.cantidad = respuesta.Cantidad;
                //vm.BW = "";
            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarVista() {
            vm.idMedio3G = $scope.$parent.vm.idGenerico;
            vm.idOportunidadLineaNegocio = $scope.$parent.vm.idOportunidadLineaNegocio;

            if (vm.idMedio3G > 0) {
                CargarMedio3G();
            }
        };
        
        function ListarMedio() {
            var medio = {
                IdEstado: 1
            };
            var promise = MedioService.ListarMedio(medio);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListMedio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
        };

        function CargarCombos() {
            //Medio
            ListarMedio();
            //TipoEnlace
            var filtroTipoEnlace = {
                IdEstado: 1,
                IdRelacion: 89
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipoEnlace);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipoEnlace = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
            //Localidad
            var filtroLocalidad = {
                IdEstado: 1,
                IdRelacion: 95
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroLocalidad);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListLocalidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarVista();
    }
})();



