﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('EquiposEspecialesController', EquiposEspecialesController);

    EquiposEspecialesController.$inject = ['EquiposEspecialesService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function EquiposEspecialesController(EquiposEspecialesService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.IdOportunidadLineaNegocio = 2;
        vm.IdSubServicioDatosCapex = 69;
        vm.IdGrupo = 7;
        
        vm.CuSoles = 50;
        vm.Cantidad = 3;
        vm.Antiguiedad = 1;
        vm.CuAntiguiedad = 8;
        vm.MesesAntiguedad = 5;
        vm.ValorResidual = 0;
        vm.Indice = 1;
        vm.CapexDolares = 0;
        vm.CapexSoles = 0;
        vm.TotalCapex = 0;

        vm.CantidadEquipoEspecialesComplete = CantidadEquipoEspecialesComplete;
        vm.Test = Test;

        function Test() {
            alert("vm.valorResidualSoles = " + vm.valorResidualSoles);
            //CantidadEquipoEspecialesComplete();

        };

        function CantidadEquipoEspecialesComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CuSoles,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CuAntiguiedad,
                MesesAntiguedad: vm.Antiguedad,
                Indice: vm.Indice
            };

            var promise = EquiposEspecialesService.CantidadEquiposEstudiosEsp(dto);
            promise.then(function (response) {
                vm.valorResidualSoles = response.data.ValorResidualSoles;
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };
    }

})();
