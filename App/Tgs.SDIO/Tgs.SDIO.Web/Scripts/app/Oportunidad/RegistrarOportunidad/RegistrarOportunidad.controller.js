﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('RegistrarOportunidad', RegistrarOportunidad);

    RegistrarOportunidad.$inject = ['RegistrarOportunidadService', 'ClienteService', 'SectorService', 'MaestraService', 'DireccionComercialService', 'LineaNegocioService',
        'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$injector'];
    function RegistrarOportunidad(RegistrarOportunidadService,  ClienteService, SectorService, MaestraService, DireccionComercialService, LineaNegocioService,
         blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope, $compile, $injector) {

        var vm = this;

        //<a class="btn btn-sm btn-primary btn-space right" href="{{vm.EnlaceRegistrar}}0"><span class="glyphicon glyphicon-list-alt"></span> Nuevo</a>
        vm.EnlaceRegistrar = $urls.ApiOportunidad + "OportunidadContenedor/Index/";
        vm.Nuevo = $urls.ApiOportunidad + "OportunidadContenedor/Index/";
        vm.Editar = $urls.ApiOportunidad + "OportunidadContenedor/Index/";

        vm.Descripcion = '';
        vm.SelectCliente = SelectCliente;
        vm.fillTextbox = fillTextbox;
        vm.ObtenerCliente = ObtenerCliente;
        vm.ObtenerSector = ObtenerSector;
        vm.ObtenerDireccionComercial = ObtenerDireccionComercial;
        vm.RegistrarOportunidad = RegistrarOportunidad;
        vm.ObtenerOportunidadFlujoEstadoId=ObtenerOportunidadFlujoEstadoId;
        vm.Preventa = Preventa;
        vm.Preventa = "";
    

        vm.CargarTipoProyecto = CargarTipoProyecto;
        vm.CargarTipoServicio = CargarTipoServicio;
        vm.CargarAnalistaFIN = CargarAnalistaFIN;
        vm.CargarProductManager = CargarProductManager;
        vm.CargarOpciones = CargarOpciones;

        vm.CargaOportunidad=CargaOportunidad;
        vm.ListCliente=[];

        vm.ListTipoProyecto = [];
        vm.ListTipoServicio = [];
        vm.ListLineaNegocio = [];
        vm.ListProductManager=[];
        vm.ListAnalistaFin = [];
        vm.ListcoordinadorFin = [];


        vm.IdTipoServicio = "-1";
        vm.IdTipoProyecto = "-1";
        vm.IdLineaNegocio = "-1";
        vm.IdAnalistaFin="-1";
        vm.IdProductManager = "-1";
        vm.IdCoordinadorFin = "-1";

        vm.Sector = "";
        vm.GerenteComercial = "";
        vm.DireccionComercial = "";
        vm.IdOportunidad = 0;
        vm.IdEstadoOportunidad=0;
        vm.IdOportunidadLineaNegocio=0;
        vm.ObjOportunidad = [];
        vm.ObjOportunidad = jsonOportunidad;
        
        vm.IdEstado = "-1";
        vm.ListEstado = [];
        vm.ListEstado = jsonListEstado;


        vm.AccionGrabar =!jsonAccionGrabar;


        vm.OptionAnalistaFin = "";
        vm.OptionAnalistaFin = !jsonOptionAnalistaFin;
        vm.OptionCoordinadorFin = "";
        vm.OptionCoordinadorFin = !jsonOptionCoordinadorFin;
        vm.OptionPreVenta = "";
        vm.OptionPreVenta = !jsonOptionPreVenta;

        ListarLineaNegocio();
        CargarTipoProyecto();
        CargarTipoServicio();
        CargarAnalistaFIN();
        CargarProductManager();
        CoordinadorFIN();


        CargaOportunidad();

        function CargaOportunidad() {

            ListarLineaNegocio();
            CargarTipoProyecto();
            CargarTipoServicio();
            CargarAnalistaFIN();
            CargarProductManager();
            CoordinadorFIN();
            Preventa();
            CargarOpciones();

            if (vm.ObjOportunidad != 0) {

                if (vm.ObjOportunidad.IdAnalistaFinanciero == null) {
                    vm.ObjOportunidad.IdAnalistaFinanciero = "-1";
                }

                vm.IdOportunidad=vm.ObjOportunidad.IdOportunidad,
                vm.IdCliente = vm.ObjOportunidad.IdCliente,
                vm.Oportunidad = vm.ObjOportunidad.Descripcion,
                 vm.NumeroSalesForce = vm.ObjOportunidad.NumeroSalesForce,
                 vm.NumeroCaso = vm.ObjOportunidad.NumeroCaso,
                vm.fecha = vm.ObjOportunidad.FechaOportunidad,
                 vm.AlcanceProyecto = vm.ObjOportunidad.Alcance,
                 vm.meses = vm.ObjOportunidad.Periodo,
                vm.Implantacion = vm.ObjOportunidad.TiempoImplantacion,
                 vm.IdTipoServicio = vm.ObjOportunidad.IdTipoServicio,
                 vm.IdLineaNegocio = vm.ObjOportunidad.IdLineaNegocio,
                vm.IdAnalistaFin = vm.ObjOportunidad.IdAnalistaFinanciero,
                 vm.IdProductManager = vm.ObjOportunidad.IdProductManager,
                 vm.IdTipoProyecto = vm.ObjOportunidad.IdTipoProyecto,
                 vm.IdTipoCambio = vm.ObjOportunidad.IdTipoCambio,
                 vm.IdProyectoAnterior = vm.ObjOportunidad.IdProyectoAnterior,
                vm.IdCoordinadorFin = vm.ObjOportunidad.IdCoordinadorFinanciero,
                vm.IdEstado = vm.ObjOportunidad.IdEstado,
                vm.IdEstadoOportunidad = vm.ObjOportunidad.IdEstado,
                vm.IdOportunidadLineaNegocio = vm.ObjOportunidad.IdOportunidadLineaNegocio
                debugger
                CargarClientePorId();
            }
        }

        function CargarOpciones() {
        var Respuesta = vm.ListEstado;

        vm.IdEstado = "-1";
        vm.ListEstado= UtilsFactory.AgregarItemSelect(Respuesta);

        }

       
        function CargarClientePorId() {
            var cliente={
                IdCliente:vm.IdCliente,
                IdEstado:1
            }

            var promise = ClienteService.ObtenerCliente(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.Descripcion = Respuesta.Descripcion;
                vm.GerenteComercial = Respuesta.GerenteComercial;
                var IdDireccionComercial = Respuesta.IdDireccionComercial;
                var IdSector = Respuesta.IdSector;
                ObtenerDireccionComercial(IdDireccionComercial);
                ObtenerSector(IdSector);
         
            }, function (response) {
                blockUI.stop();

            });
        }





        function SelectCliente() {
            if (vm.Descripcion.length > 5) {
                ListarCliente();
            }
        }

        function Preventa() {


            var promise = RegistrarOportunidadService.Preventa();

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.Preventa = Respuesta;

            }, function (response) {
                blockUI.stop();

            });
        }

        function CoordinadorFIN() {

            var usuario_Admin = {
                CodigoPerfil: "CoFiNa"

            };
            var promise = RegistrarOportunidadService.ListarUsuariosPorPerfil(usuario_Admin);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.ListcoordinadorFin = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function CargarAnalistaFIN() {

            var usuario_Admin = {
                CodigoPerfil: "ANLFIN"

            };

            var promise = RegistrarOportunidadService.ListarUsuariosPorPerfil(usuario_Admin);



            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.ListAnalistaFin = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function CargarProductManager() {

            var usuario_Admin = {
                CodigoPerfil :"PDM"

                };


            var promise = RegistrarOportunidadService.ListarUsuariosPorPerfil(usuario_Admin);

         
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListProductManager = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarLineaNegocio() {

            var cliente = {
                IdEstado: 1

            };
            var promise = LineaNegocioService.ListarLineaNegocios(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;


                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function ListarCliente() {

            var cliente = {
                IdEstado: 1,
                Descripcion: vm.Descripcion,

            };
            var promise = ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;



                var output = [];
                angular.forEach(Respuesta, function (cliente) {
               
                    output.push(cliente);
                
                });
                vm.ListCliente = output;


            }, function (response) {
                blockUI.stop();

            });
        }
        function fillTextbox(string,Id) {

            vm.Descripcion = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;
            ObtenerCliente(vm.Descripcion);
        }

        function ObtenerCliente(descripcion) {

            var cliente = {
                IdEstado: 1,
                descripcion: descripcion,

            };
            var promise =ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                
                vm.GerenteComercial = Respuesta[0].GerenteComercial;
                var IdDireccionComercial = Respuesta[0].IdDireccionComercial;
                 var IdSector = Respuesta[0].IdSector;
                 ObtenerDireccionComercial(IdDireccionComercial);
                 ObtenerSector(IdSector);
            }, function (response) {
                blockUI.stop();

            });
        }
        function ObtenerSector(Id) {

            var sector = {
                IdEstado: 1,
                IdSector: Id,

            };
            var promise = SectorService.ObtenerSector(sector);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

                vm.Sector = Respuesta.Descripcion;

            }, function (response) {
                blockUI.stop();

            });
        }
        function ObtenerDireccionComercial(Id) {

            var direccionComercial={
                IdEstado: 1,
                IdDireccion:Id

            }; 
            var promise = DireccionComercialService.ObtenerDireccionComercial(direccionComercial);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                
                vm.DireccionComercial = Respuesta.Descripcion;

            }, function (response) {
                blockUI.stop();

            });
        }
        function CargarTipoProyecto() {

            var maestra={
                IdEstado: 1,
                IdRelacion:7

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListTipoProyecto = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }
        function CargarTipoServicio() {

            var maestra = {
                IdEstado: 1,
                IdRelacion: 14

            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);

            promise.then(function (resultado) {
                blockUI.stop();
                
                var Respuesta = resultado.data;
                vm.ListTipoServicio = UtilsFactory.AgregarItemSelect(Respuesta);

            }, function (response) {
                blockUI.stop();

            });
        }

        function ObtenerOportunidadFlujoEstadoId() {
            var oportunidadFlujoEstado = {
                IdEstado: vm.IdEstado,
                IdOportunidad: vm.IdOportunidad

            }

            var promise = RegistrarOportunidadService.ObtenerOportunidadFlujoEstadoId(oportunidad);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.IdEstadoOportunidad = Respuesta.IdEstado;

                vm.IdEstadoOportunidad
            }, function (response) {
                blockUI.stop();

            });
        }
        function RegistrarOportunidad() {
            var Id = vm.IdOportunidad;
            if( vm.IdAnalistaFin=="-1") 
            {

                vm.IdAnalistaFin =null;

            }

            if( vm.IdEstado!=vm.IdEstadoOportunidad &&vm.IdEstado=="-1") 
            {

                vm.IdEstado = vm.IdEstadoOportunidad;

            }
            if (vm.IdEstado == vm.IdEstadoOportunidad && vm.IdEstado != "-1") {
                vm.IdEstado = vm.IdEstadoOportunidad;
            }
            if (vm.IdEstado != vm.IdEstadoOportunidad && vm.IdEstado != "-1"&&vm.IdOportunidad!=0) {
                var oportunidadFlujoEstado = {
                    IdEstado:vm.IdEstado,
                    IdOportunidad:vm.IdOportunidad

                }



                var promise = RegistrarOportunidadService.RegistrarOportunidadFlujoEstado(oportunidadFlujoEstado);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;

                    ObtenerOportunidadFlujoEstadoId();

                    vm.IdEstadoOportunidad = Respuesta.Id;
                }, function (response) {
                    blockUI.stop();

                });
            }

            var oportunidad = {
                IdOportunidad:vm.IdOportunidad,
                IdCliente: vm.IdCliente,
                IdCliente : vm.IdCliente,
                Descripcion : vm.Oportunidad,
                NumeroSalesForce : vm.NumeroSalesForce,
                NumeroCaso: vm.NumeroCaso,
                Fecha: vm.fecha,
                Alcance : vm.AlcanceProyecto,
                Periodo : vm.meses,
                TiempoImplantacion: vm.Implantacion,
                IdTipoServicio: vm.IdTipoServicio,
                IdEstado: vm.IdEstado,
                IdAnalistaFinanciero : vm.IdAnalistaFin,
                IdProductManager: vm.IdProductManager,
                IdTipoProyecto:vm.IdTipoProyecto,
                IdTipoCambio : vm.IdTipoCambio,
                IdProyectoAnterior: vm.IdProyectoAnterior,
                IdCoordinadorFinanciero:vm.IdCoordinadorFin

            };


            var promise = (vm.ObjOportunidad == 0) ? RegistrarOportunidadService.RegistrarOportunidad(oportunidad) : RegistrarOportunidadService.ActualizarOportunidad(oportunidad);
            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                if (Id==0){ 
                vm.IdOportunidad = Respuesta.Id;
                }
                RegistrarOportunidadFlujoEstado();

                RegistrarOportunidadLineaNegocio();
                UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);
               
            }, function (response) {
                blockUI.stop();

            });
        }
         function RegistrarOportunidadFlujoEstado() {


          var oportunidadFlujoEstado = {
                    IdEstado:vm.IdEstado,
                    IdOportunidad:vm.IdOportunidad

                }

                if(vm.ObjOportunidad == 0){
                   var promise =  RegistrarOportunidadService.RegistrarOportunidadFlujoEstado(oportunidadFlujoEstado);
                promise.then(function (resultado) {
                    blockUI.stop();

                    var Respuesta = resultado.data;
                    ObtenerOportunidadFlujoEstadoId();

                  vm.IdEstadoOportunidad
                }, function (response) {
                    blockUI.stop();

                });


             }

             
       
        }
        function RegistrarOportunidadLineaNegocio() {


            var oportunidadLineaNegocio = {
                IdOportunidadLineaNegocio:vm.IdOportunidadLineaNegocio,
                IdOportunidad:vm.IdOportunidad,
                IdLineaNegocio:vm.IdLineaNegocio

            };


            var promise = (vm.ObjOportunidad == 0) ? RegistrarOportunidadService.RegistrarOportunidadLineaNegocio(oportunidadLineaNegocio) : RegistrarOportunidadService.ActualizarOportunidadLineaNegocio(oportunidadLineaNegocio);
            promise.then(function (resultado) {
                blockUI.stop();
                
                var Respuesta = resultado.data;
                vm.IdOportunidadLineaNegocio=Respuesta.Id;
               // UtilsFactory.Alerta('#divAlert', 'success', Respuesta.Mensaje, 5);

            }, function (response) {
                blockUI.stop();

            });
        }

       


        function LimpiarFiltros() {
            vm.Nombre = '';
            vm.Descripcion = '';
            LimpiarGrilla();
        }


    }
})();