﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('SeguridadOPEXController', SeguridadOPEXController);

    SeguridadOPEXController.$inject = ['SeguridadOPEXService', 'SubServicioDatosService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function SeguridadOPEXController(SeguridadOPEXService, SubServicioDatosService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.idSOPEX = 0;
        vm.idOportunidadLineaNegocio = 0;
        vm.servicio = "";
        vm.cantidad = 0;
        vm.numeroMeses = 0;
        vm.precioUnitario = 0;
        vm.montoPxQ = 0;
        vm.factor = 0;
        vm.valorCuota = 0;
        vm.mesInicioGasto = 0;

        vm.GrabarSOPEX = GrabarSOPEX;
        
        vm.ListTipo = [];
        vm.IdTipo = "-1";

        /******************************************* ServicioCMI *******************************************/

        function GrabarSOPEX() {
            //CargarCombos();
            //alert("vm.IdMedio: " + vm.IdMedio);
            //alert("vm.ListTipoEnlace[0]: " + vm.ListTipoEnlace[0]);
        }

        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {

        };

        function CargarSOPEX() {
            var dto = {
                IdSubServicioDatosCaratula: vm.idSOPEX,
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdEstado: 1,
                IdGrupo: 5
            };

            var promise = SubServicioDatosService.ObtenerSubServicioDatos(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                //vm.servicio = respuesta.servicio;
                vm.cantidad = respuesta.Cantidad;
                vm.numeroMeses = respuesta.Meses;
                vm.precioUnitario = respuesta.MontoUnitarioMensual;
                //vm.montoPxQ = respuesta.MontoPxQ;
                vm.factor = respuesta.Factor;
                vm.valorCuota = respuesta.ValorCuota;
                vm.IdTipo = respuesta.IdTipoCosto;//validar
                vm.mesInicioGasto = respuesta.NumeroMesInicioGasto;
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarVista() {
            vm.idSOPEX = $scope.$parent.vm.idGenerico;
            vm.idOportunidadLineaNegocio = $scope.$parent.vm.idOportunidadLineaNegocio;

            if (vm.idSOPEX > 0) {
                CargarSOPEX();
            }
        };

        function CargarCombos() {
            //Tipo
            var filtroTipo = {
                IdEstado: 1,
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarVista();
    }
})();



