﻿(function () {
    'use strict'
    angular
    .module('app.Oportunidad')
    .controller('OPEXController', OPEXController);

    OPEXController.$inject = ['OPEXService', 'SubServicioDatosService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile', '$modal', '$injector'];

    function OPEXController(OPEXService, SubServicioDatosService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile, $modal, $injector) {
        var vm = this;
        vm.idOPEX = 0;
        vm.idOportunidadLineaNegocio = 0;

        vm.servicio = "";
        vm.cantidad = 0;
        vm.numeroMeses = 0;
        vm.mesInicio = 0;
        vm.montoUnitarioMensual = 0;
        vm.montoTotalMensual = 0;

        vm.GrabarOPEX = GrabarOPEX;

        vm.ListTipo = [];
        vm.IdTipo = "-1";

        /******************************************* OPEX *******************************************/

        function GrabarOPEX() {
            //CargarCombos();
            //alert("vm.IdMedio: " + vm.IdMedio);
            //alert("vm.ListTipoEnlace[0]: " + vm.ListTipoEnlace[0]);
        }

        /******************************************* Funciones *******************************************/

        function LimpiarGrilla() {

        };

        function CargarOPEX() {
            var dto = {
                IdSubServicioDatosCaratula: vm.idOPEX,
                IdOportunidadLineaNegocio: vm.idOportunidadLineaNegocio,
                IdEstado: 1,
                IdGrupo: 3
            };

            var promise = SubServicioDatosService.ObtenerSubServicioDatos(dto);

            promise.then(function (resultado) {
                blockUI.stop();
                var respuesta = resultado.data;

                vm.servicio = respuesta.SubServicio;
                vm.cantidad = respuesta.Cantidad;
                vm.numeroMeses = respuesta.Meses;
                vm.mesInicio = respuesta.NumeroMesInicioGasto;
                vm.montoUnitarioMensual = respuesta.MontoUnitarioMensual;
                vm.montoTotalMensual = respuesta.MontoTotalMensual;
            }, function (response) {
                blockUI.stop();

            });
        };

        function CargarVista() {
            vm.idOPEX = $scope.$parent.vm.idGenerico;
            vm.idOportunidadLineaNegocio = $scope.$parent.vm.idOportunidadLineaNegocio;

            if (vm.idOPEX > 0) {
                CargarOPEX();
            }
        };

        function CargarCombos() {
            //Tipo
            var filtroTipo = {
                IdEstado: 1,
                IdRelacion: 32
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(filtroTipo);
            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListTipo = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                //
            });
        };

        /******************************************* LOAD *******************************************/
        CargarCombos();
        CargarVista();
    }
})();



