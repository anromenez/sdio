﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('BandejaOportunidad', BandejaOportunidad);

    BandejaOportunidad.$inject = ['BandejaOportunidadService', 'RegistrarOportunidadService', 'ClienteService', 'blockUI', 'UtilsFactory', '$timeout', 'MensajesUI', 'URLS', '$scope'];

    function BandejaOportunidad(BandejaOportunidadService,RegistrarOportunidadService, ClienteService, blockUI, UtilsFactory, $timeout, MensajesUI, $urls, $scope) {

        var vm = this;

        //<a class="btn btn-sm btn-primary btn-space right" href="{{vm.EnlaceRegistrar}}0"><span class="glyphicon glyphicon-list-alt"></span> Nuevo</a>
        vm.Nuevo = $urls.ApiOportunidad + "OportunidadContenedor/Index/";
        vm.NuevaVersion = NuevaVersion;



        vm.SelectedFiles = "";
        vm.Version = Version;
        vm.DetalleVersiones=true;
        vm.BuscarOportunidad = BuscarOportunidad;
        vm.EliminarOportunidad = EliminarOportunidad;
        vm.AceptarEliminarOportunidad = AceptarEliminarOportunidad;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionSegmento = '';
        vm.Descripcion = '';
        vm.DescripcionCliente = '';
        vm.IdOportunidad = "";
        vm.MensajeDeAlerta = "";
        vm.Progres = 0;

        vm.SelectLineaNegocio = SelectLineaNegocio;
        vm.CargarLineaNegocio = CargarLineaNegocio;
        vm.CargarMaestra = CargarMaestra;
        vm.SelectCliente = SelectCliente;
        vm.SelectGerente = SelectGerente;
        vm.SelectDireccion = SelectDireccion;
        vm.CargarDireccion = CargarDireccion;
        vm.CargarGerente = CargarGerente;
        vm.DatosPorVersion = DatosPorVersion;

        vm.RegistrarVersionOportunidad = RegistrarVersionOportunidad;
        vm.OportunidadInactivar = OportunidadInactivar;
        vm.OportunidadGanador = OportunidadGanador;


        vm.ListVersiones = [];

        vm.numSalesForce = "";


        vm.setPage = setPage;
        vm.pageChanged = pageChanged;
        vm.setItemsPerPage = setItemsPerPage;

        vm.ListOportunidadCabecera = [];
        vm.ListOportunidadCabecera = jsonListOportunidadCabecera;
        vm.ListLineaNegocio = [];
        vm.ListMaestra = [];
        vm.ListCliente = [];

        vm.DescripcionOportunidad = [];

        vm.ListGerente = [];
        vm.ListDireccion = [];



        vm.fillTextbox = fillTextbox;



        vm.IdUsuario = 0;
        vm.IdLineaNegocio = 0;
        vm.IdCliente = 0;

        vm.NumeroSalesForce = "";
        vm.IdGerente = 0;
        vm.IdDireccion = 0;
        vm.FechaCreacion = "";
        vm.FechaEdicion = "";
        vm.IdEstadoOportunidad = "-1";

        vm.OptionPreVenta = "";
        vm.OptionPreVenta = !jsonOptionPreVenta;

        //vm.viewby = 10;
        //vm.totalItems = vm.ListOportunidadCabecera.length;
        //vm.currentPage = 1;
        //vm.itemsPerPage = vm.viewby;
        //vm.maxSize = 4;




        vm.Oportunidad = "";
        vm.NomCompletUsuCrea = "";
        vm.NomCompletUsuMod = "";
        vm.NomCompletPreventa = "";
        vm.IdOportunidad = 0;
        //acciones


        //jsonListGerente



        //jsonListGerenciaComerial


        function CargarDireccion() {
            vm.ListDireccion = SelectDireccion(jsonListDireccion);
        }
        function CargarGerente() {
            vm.ListGerente = SelectGerente(jsonListGerente);

        }
        function CargarLineaNegocio() {
            vm.ListLineaNegocio = SelectLineaNegocio(jsonListLineaNegocio);
        }


        function CargarMaestra() {
            vm.ListMaestra = UtilsFactory.AgregarItemSelectMaestra(jsonListMaestra);
        }

        function CargarVersiones() {

        }
        function NuevaVersion(Id) {

            ObtenerOportunidad(Id);
         var IdOportunidadLineaNegocio = ObtenerOportunidadLineaNegocio(Id);
         RegistrarOportunidadFlujoEstado(Id);



        }
        /* Obtener */
        function ObtenerOportunidad(Id) {

            var oportunidad = {
                IdOportunidad: Id,

            };
            var promise = RegistrarOportunidadService.ObtenerOportunidad(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;



                RegistrarOportunidad(Respuesta);


            }, function (response) {
                blockUI.stop();

            });
        }

        function ObtenerOportunidadLineaNegocio(Id) {

            var oportunidad = {
                IdOportunidad: Id,

            };
            var promise = RegistrarOportunidadService.ObtenerOportunidadLineaNegocio(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;



                RegistrarOportunidadLineaNegocio(Respuesta);
              return    Respuesta.IdOportunidadLineaNegocio;

            }, function (response) {
                blockUI.stop();

            });
        }



        /* Registrar */
        
        function RegistrarOportunidadFlujoEstado(Id) {
            var oportunidad = {

                IdEstado: 1,
                IdOportunidad:Id

            };
     
            var promise = RegistrarOportunidadService.RegistrarOportunidadFlujoEstado(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;

            }, function (response) {
                blockUI.stop();

            });
        }
        function RegistrarOportunidad(Respuesta) {
            var oportunidad = Respuesta;
            oportunidad.Fecha = ToJavaScriptDate(oportunidad.Fecha);
            oportunidad.FechaCreacion = ToJavaScriptDate(oportunidad.FechaCreacion);
            
            var promise = RegistrarOportunidadService.RegistrarOportunidad(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;




            }, function (response) {
                blockUI.stop();

            });
        }
        function RegistrarOportunidadLineaNegocio(Respuesta) {
            var oportunidadLineaNegocio = Respuesta;
       
            var promise = RegistrarOportunidadService.RegistrarOportunidadLineaNegocio(oportunidadLineaNegocio);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;




            }, function (response) {
                blockUI.stop();

            });
        }





        function setPage(pageNo) {
            vm.currentPage = pageNo;
        }

        function pageChanged() {
            //console.log('Page changed to: ' + vm.currentPage);
        }
        function setItemsPerPage(num) {
            vm.itemsPerPage = num;
            vm.currentPage = parseInt(vm.totalItems / 10); //reset to first page
        }


        function EliminarOportunidad(IdOportunidad) {
            vm.btnEliminar = true;
            $("#idArchivo").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idArchivo").val(IdOportunidad);
        }

        function AceptarEliminarOportunidad() {
            var ddlIdarchivo = angular.element(document.getElementById("idArchivo"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var archivo = {
                IdArchivo: ddlIdarchivo.val()
            }
            blockUI.start();
            var promise = BandejaOportunidadService.EliminarOportunidad(archivo);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.ResponseID == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarOportunidad()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', Respuesta.ResponseMSG, 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                blockUI.stop();
            });

        }



        function SelectCliente() {
            if (vm.Descripcion.length > 5) {
                ListarCliente();
            }


        }
        function ListarCliente() {

            var cliente = {
                IdEstado: 1,
                Descripcion: vm.Descripcion,

            };
            var promise = ClienteService.ListarClientePorDescripcion(cliente);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;



                var output = [];
                angular.forEach(Respuesta, function (cliente) {

                    output.push(cliente);

                });
                vm.ListCliente = output;


            }, function (response) {
                blockUI.stop();

            });
        }

        function fillTextbox(string, Id) {

            vm.Descripcion = string;
            vm.IdCliente = Id;
            vm.ListCliente = null;
        
        }

        function RegistrarVersionOportunidad() {

            if (vm.IdEstadoOportunidad == "-1") {
                vm.IdEstadoOportunidad = 0;
            }

            var Oportunidad = {
                IdLineaNegocio: vm.IdOportunidad,

            };
            var promise = BandejaOportunidadService.RegistrarVersionOportunidad(Oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }
        function OportunidadInactivar() {

            if (vm.IdEstadoOportunidad == "-1") {
                vm.IdEstadoOportunidad = 0;
            }

            var Oportunidad = {
                IdLineaNegocio: vm.IdOportunidad,

            };
            var promise = BandejaOportunidadService.OportunidadInactivar(Oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }
        function OportunidadGanador() {

            if (vm.IdEstadoOportunidad == "-1") {
                vm.IdEstadoOportunidad = 0;
            }

            var Oportunidad = {
                IdLineaNegocio: vm.IdOportunidad,

            };
            var promise = BandejaOportunidadService.OportunidadGanador(Oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }



        function BuscarOportunidad() {
            var IdEstado = 0;
            IdEstado = vm.IdEstadoOportunidad;
            if (vm.IdEstadoOportunidad == "-1") {
                IdEstado = 0;
            }

            var oportunidad = {
                IdLineaNegocio: vm.IdLineaNegocio,
                IdCliente: vm.IdCliente,
                Oportunidad: vm.Descripcion,
                NumeroSalesForce: vm.NumeroSalesForce,
                IdGerente: vm.IdGerente,
                IdDireccion: vm.IdDireccion,
                IdEstado: IdEstado
            };
            var promise = BandejaOportunidadService.ListarOportunidadCabecera(oportunidad);

            promise.then(function (resultado) {
                blockUI.stop();

                var Respuesta = resultado.data;
                vm.ListOportunidadCabecera = Respuesta;
                blockUI.stop();


            }, function (response) {
                blockUI.stop();

            });
        }
        function Version(Id) {
            // blockUI.start();

            var Oportunidad = {
                IdOportunidad: Id
            };
            var promise = BandejaOportunidadService.ObtenerVersiones(Oportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                vm.ListVersiones = Respuesta;
                vm.DetalleVersiones=false;


                //var estado = "";
                //switch (data[key][0].IdEstado) {
                //    case 0:
                //        estado = "Eliminado";
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 1:
                //        estado = "En Proceso"
                //        document.getElementById("acciones").style.display = "block";
                //        break;
                //    case 2:
                //        estado = "Ganador"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 3:
                //        estado = "Desestimado"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //}
                //document.getElementById("Estado").innerHTML = estado;

                vm.DescripcionOportunidad = Respuesta.slice(0, 1);
                var dato = vm.DescripcionOportunidad;

                vm.IdOportunidad = dato[0].IdOportunidad;
                vm.Oportunidad = dato[0].Descripcion;
                vm.NomCompletPreventa = dato[0].NomCompletPreventa;
                vm.NomCompletUsuCrea = dato[0].NomCompletUsuCrea;
                vm.NomCompletUsuMod = dato[0].NomCompletUsuMod;
                $('#pie-chart').remove();
                GraficoOibda(dato[0].IdOportunidad);
                //     blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }
        function DatosPorVersion(Id) {
            // blockUI.start();
            var Oportunidad = {
                IdOportunidad: Id
            };
            var promise = BandejaOportunidadService.DatosPorVersion(Oportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                // vm.ListVersiones = Respuesta;

                //var estado = "";
                //switch (data[key][0].IdEstado) {
                //    case 0:
                //        estado = "Eliminado";
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 1:
                //        estado = "En Proceso"
                //        document.getElementById("acciones").style.display = "block";
                //        break;
                //    case 2:
                //        estado = "Ganador"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //    case 3:
                //        estado = "Desestimado"
                //        document.getElementById("acciones").style.display = "none";
                //        break;
                //}
                //document.getElementById("Estado").innerHTML = estado;

                vm.Oportunidad = Respuesta.Oportunidad;
                vm.NomCompletPreventa = Respuesta.NomCompletPreventa;
                vm.NomCompletUsuCrea = Respuesta.NomCompletUsuCrea;
                vm.NomCompletUsuMod = Respuesta.NomCompletUsuMod;

                GraficoOibda(Id);




                //     blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }
        function GraficoOibda(Id) {
            // blockUI.start();


            document.getElementById("demo").innerHTML = " <div id='pie-chart'></div>";
            var Oportunidad = {
                IdOportunidad: Id
            };
            var promise = BandejaOportunidadService.GraficoOibda(Oportunidad);

            promise.then(function (resultado) {
                var Respuesta = resultado.data;
                var array = [];
                var i = 0;

                for (i = 0; i < Respuesta.length; i++) {
                    //  array[i] += Respuesta[i];

                    array.push(Respuesta[i].Color);
                }


                Morris.Donut({
                    element: 'pie-chart',
                    data: Respuesta,
                    colors: array
                });
                vm
                //     blockUI.stop();

            }, function (response) {
                blockUI.stop();

            });
        }

        function LimpiarFiltros() {
            vm.Nombre = '';
            vm.Descripcion = '';
            LimpiarGrilla();
        }

        function SelectLineaNegocio(data) {
            var lista = data;
            var item = { "IdLineaNegocio": 0, "Descripcion": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;

        };
        function SelectGerente(data) {
            var lista = data;
            var item = { "IdUsuario": 0, "NombresCompletos": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;

        };
        function SelectDireccion(data) {
            var lista = data;
            var item = { "IdDireccion": 0, "Descripcion": "--Seleccione--" };
            lista.splice(0, 0, item);
            return lista;

        };


        function ToJavaScriptDate(value) { //To Parse Date from the Returned Parsed Date
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));
            return dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear();
        }
    }
})();