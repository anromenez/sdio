﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('OportunidadContenedorController', OportunidadContenedorController);

    OportunidadContenedorController.$inject = ['OportunidadContenedorService','LineaNegocioService', 'CasoNegocioService','blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function OportunidadContenedorController(OportunidadContenedorService, LineaNegocioService, CasoNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.ListLineaNegocio = [];
        vm.ListCasoNegocio = [];
        vm.IdLineaNegocio = '-1';
        vm.IdCaso = '-1';
        vm.ListarCasoNegocio = ListarCasoNegocio;

        /******************************************* Metodos *******************************************/

        function ListarLineaNegocio() {

            var linea = {
                IdEstado: 1
            };
            var promise = LineaNegocioService.ListarLineaNegocios(linea);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListLineaNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();

            });
        }

        function ListarCasoNegocio() {

            var casonegocio = {
                IdLineaNegocio: vm.IdLineaNegocio,
                IdEstado: 1
            };

            var promise = CasoNegocioService.ListarCasoNegocioDefecto(casonegocio);

            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.ListCasoNegocio = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                vm.ListCasoNegocio = UtilsFactory.AgregarItemSelect(null);
                blockUI.stop();

            });
        };

        ListarLineaNegocio();
        ListarCasoNegocio();
    }

})();