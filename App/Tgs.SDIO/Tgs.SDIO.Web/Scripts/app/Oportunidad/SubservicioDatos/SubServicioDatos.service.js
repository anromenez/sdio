﻿(function () {
    'use strict',
     angular
    .module('app.Oportunidad')
    .service('SubServicioDatosService', SubServicioDatosService);

    SubServicioDatosService.$inject = ['$http', 'URLS'];

    function SubServicioDatosService($http, $urls) {

        var service = {
            ListarDetalleOportunidadCaratula2: ListarDetalleOportunidadCaratula2,
            ListarDetalleOportunidadEcapexSub2: ListarDetalleOportunidadEcapex2,
            ObtenerSubServicioDatos: ObtenerSubServicioDatos,
            ActualizarSubServicioDatos: ActualizarSubServicioDatos,
            EliminarSubServicioDatos: EliminarSubServicioDatos
        };

        return service;
        
        function ObtenerSubServicioDatos(dto) {
            return $http({
                url: $urls.ApiOportunidad + "Circuito/Obtener",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ActualizarSubServicioDatos(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatos/ActualizarSubServicioDatos",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarSubServicioDatos(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatos/EliminarSubServicioDatos",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleOportunidadCaratula2(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatos/ListarDetalleOportunidadCaratula2",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarDetalleOportunidadEcapex2(dto) {
            return $http({
                url: $urls.ApiOportunidad + "SubServicioDatos/ListarDetalleOportunidadEcapex2",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);
            function DatosCompletados(resultado) {
                return resultado;
            }
        };
    }
})();