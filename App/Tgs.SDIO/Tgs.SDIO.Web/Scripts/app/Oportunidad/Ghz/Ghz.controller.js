﻿(function () {
    'use strict'

    angular
    .module('app.Oportunidad')
    .controller('GhzController', GhzController);

    GhzController.$inject = ['GhzService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$scope', '$compile'];

    function GhzController(GhzService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $scope, $compile) {

        var vm = this;
        vm.IdOportunidadLineaNegocio = 2;
        vm.IdSubServicioDatosCapex = 76;
        vm.IdGrupo = 14

        vm.CuSoles = 50;
        vm.Cantidad = 3;
        vm.Antiguiedad = 1;
        vm.CuAntiguiedad = 8;
        vm.MesesAntiguedad = 5;
        vm.ValorResidual = 0;
        vm.Indice = 1;
        vm.CapexDolares = 0;
        vm.CapexSoles = 0;
        vm.TotalCapex = 0;

        vm.CantidadGhzComplete = CantidadGhzComplete;

        function CantidadGhzComplete() {
            var dto = {
                IdSubServicioDatosCapex: vm.IdSubServicioDatosCapex,
                IdGrupo: vm.IdGrupo,
                IdEstado: 1,
                Cu: vm.CuSoles,
                Cantidad: vm.Cantidad,
                CuAntiguiedad: vm.CuAntiguiedad,
                MesesAntiguedad: vm.Antiguedad,
                Indice: vm.Indice
            };

            var promise = GhzService.CantidadGhz(dto);
            promise.then(function (response) {
                vm.valorResidualSoles = response.data.ValorResidualSoles;
                vm.CapexDolares = response.data.CapexDolares;
                vm.CapexSoles = response.data.CapexSoles;
                vm.TotalCapex = response.data.TotalCapex;
                blockUI.stop();
            }, function (response) {
                blockUI.stop();
            });
        };
    }

})();