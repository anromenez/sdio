﻿(function () {
    'use strict',

    angular
    .module('app.Seguridad')
    .controller('OlvidoClaveController', OlvidoClaveController);

    OlvidoClaveController.$inject = ['OlvidoClaveService', 'blockUI', '$timeout', 'UtilsFactory', 'MensajesUI'];

    function OlvidoClaveController(OlvidoClaveService, blockUI, $timeout, UtilsFactory, $MensajesUI) {
        var vm = this;

        vm.EnvioClave = EnvioClave;

        vm.TxtUsuario = '';
          
        function EnvioClave() {
             
            var UsuarioDto = {
                Login: vm.TxtUsuario
            }; 
            
            if (UsuarioDto.Login == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese su usuario", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();

            var promise = OlvidoClaveService.EnviarClave(UsuarioDto);

            promise.then(function (response) {   
                 
                var respuesta = response.data.Respuesta.Mensaje;

                if (respuesta == "OK")
                { 
                    UtilsFactory.Alerta('#divAlert', 'success','Su clave de acceso fue enviada correctamente', 6);
                } else { 
                    UtilsFactory.Alerta('#divAlert', 'danger', respuesta, 5);
                }

                $timeout(function () {
                    blockUI.stop();
                }, 1000);

            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', $MensajesUI.DatosError, 6);                
            }); 
       };
    }

})();

