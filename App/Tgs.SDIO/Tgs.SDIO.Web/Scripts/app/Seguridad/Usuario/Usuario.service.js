﻿(function () {
    'use strict',
    angular
    .module('app.Seguridad')
    .service('UsuarioService', UsuarioService);

    UsuarioService.$inject = ['$http', 'URLS'];

    function UsuarioService($http, $urls) {

        var service = {
            BuscarUsuario: BuscarUsuario,
            BuscarUsuarioAll: BuscarUsuarioAll,
            ModalUsuarioEditar: ModalUsuarioEditar
        };

        return service;

        function BuscarUsuario(UsuarioDto) {
            return $http({
                url: $urls.ApiSeguridad + "Usuario/BuscarUsuarios",
                method: "POST",
                data: JSON.stringify(UsuarioDto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function BuscarUsuarioAll() {
            return $http({
                url: $urls.ApiSeguridad + "Usuario/ListarUsuarios",
                method: "GET"
                //data: JSON.stringify(UsuarioDto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

        function ModalUsuarioEditar(dto) {
            return $http({
                url: $urls.ApiSeguridad + "UsuarioModal/Index",
                method: "POST",
                data: JSON.stringify(dto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

    }

})();

