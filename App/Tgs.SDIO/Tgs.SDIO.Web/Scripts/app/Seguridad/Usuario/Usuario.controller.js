﻿(function () {
    'use strict'

    angular
    .module('app.Seguridad')
    .controller('UsuarioController', UsuarioController);

    UsuarioController.$inject = ['$scope', 'UsuarioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS', '$compile', '$injector'];

    function UsuarioController($scope, UsuarioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls, $compile, $injector) {

        var vm = this;

        $scope.data = [];

        vm.nombres = "";
        vm.apellidos = "";
        vm.CargaModalUsuarioEditar = CargaModalUsuarioEditar;
        vm.p1 = "param1";
        vm.p2 = "param2";
        //vm.EnlaceConsultar = $urls.ApiSeguridad + "Usuario/BuscarAll"
        vm.EnlaceConsultar = $urls.ApiSeguridad + "Usuario/BuscarAll";
        vm.EnlaceRegistrar = $urls.ApiSeguridad + "Usuario/Buscar";
        vm.BuscarUsuario = BuscarUsuario;
        //vm.EliminarUsuario = EliminarUsuario;
        //vm.AceptarEliminarUsuario = AceptarEliminarUsuario;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Nombre = "";
        vm.TipoUsuario = "";
        vm.IdUsuario = "";
        LimpiarGrilla();
        vm.listUsuarioLoginRais = jsonUsuarioLoginRais;


        vm.dtColumns = [
            DTColumnBuilder.newColumn('IdUsuario').notSortable().notVisible(),
            DTColumnBuilder.newColumn('Login').withTitle('Login').notSortable(),
            DTColumnBuilder.newColumn('Nombres').withTitle('Nombre Completo').notSortable(),
            DTColumnBuilder.newColumn('TipoUsuario').withTitle('Tipo').notSortable(),
            DTColumnBuilder.newColumn('CorreoElectronico').withTitle('CorreoElectronico').notSortable(),
            //DTColumnBuilder.newColumn('Perfil').withTitle('Perfil').notSortable(),
            DTColumnBuilder.newColumn('IdEstadoRegistro').withTitle('Activo').notSortable(),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda)
        ];

        function CargaModalUsuarioEditar(IdUsuario) {

            var dto = {
                IdUsuario: IdUsuario
            };

            console.log(dto);

            var promise = UsuarioService.ModalUsuarioEditar(dto);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoUsuario").html(content);
                });

                $('#ModalUsuario').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };        
        
        $scope.gridHandlers = {
            onRowDblClick: function (entity) {
                this.onRowClick(entity);
                if ($scope.searchForm) {
                    $scope.ok();
                } else {
                    vm.dataEdit();
                }
            },
            onRowClick: function (entity) {
                $scope.DTO = new datacontext.Model.People(entity)
                if ($scope.searchForm) {
                    common.setSearchItem('People', entity);
                }
            }
        };

        //to support multiple grids at the same page
        var filterBarPlugin = {
            init: function (scope, grid) {
                filterBarPlugin.scope[grid.gridId] = scope;
                filterBarPlugin.grid[grid.gridId] = grid;
                $scope.$watch(function () {
                    var searchQuery = "";
                    var gridId = grid.gridId;
                    angular.forEach(filterBarPlugin.scope[gridId].columns, function (col) {
                        if (col.visible && col.filterText) {
                            var filterText = (col.filterText.indexOf('*') == 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
                            searchQuery += col.displayName + ": " + filterText;
                        }
                    });
                    return searchQuery;
                }, function (searchQuery) {
                    var gridId = grid.gridId;
                    filterBarPlugin.scope[gridId].$parent.filterText = searchQuery;
                    filterBarPlugin.grid[gridId].searchProvider.evalFilter();
                });
            },
            scope: {},
            grid: {}
        };
        
        function BuscarUsuario() {
            //blockUI.start();
            LimpiarGrilla();

            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarUsuarioPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withOption('rowCallback',
                    function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                        $compile(nRow)($scope);
                    }
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);

        }

        function BuscarUsuarioPaginado(sSource, aoData, fnCallback, oSettings) {
            debugger;
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            
            var usurario = {
                Nombre: vm.Nombre,
                TipoUsuario: vm.TipoUsuario,
                Indice: draw,
                Tamanio: length
            };

            var promise = UsuarioService.BuscarUsuarioAll();

            promise.then(function (resultado) {
                debugger;

                //anromenez
                //resultado.data.Total = 1;
                resultado.data.Filtered = 1;

                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.length,
                    'recordsFiltered': resultado.data.Filtered,
                    'data': resultado.data

                    //vm.listusuarioLoginRais
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }

        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        
        function AccionesBusqueda(data, type, full, meta) {
            //"<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarUsuario(" + data.IdUsuario + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
            return '<button class="btn btn-primary btn-primary-tgestiona cabfiltrosBTN"' +
                ' ng-click="vm.CargaModalUsuarioEditar(' + data.IdUsuario + ')" type="submit">Editar</button>';
        }

        function LimpiarFiltros() {
            vm.Nombre = '';
            LimpiarGrilla();
        }

    }
})();