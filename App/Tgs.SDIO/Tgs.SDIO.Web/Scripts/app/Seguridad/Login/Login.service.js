﻿(function () {
    'use strict',
    angular
    .module('app.Seguridad')
    .service('LoginService', LoginService);

    LoginService.$inject = ['$http', 'URLS'];

    function LoginService($http, $urls) {

        var service = {
            LoginDatos: LoginDatos, 
            IngresarSistema: IngresarSistema
        };

        return service;

        function LoginDatos(UsuarioDto, token) {
            return $http({
                url: $urls.ApiSeguridad + "Login/Ingresar",
                method: "POST",
                data: JSON.stringify(UsuarioDto),
                headers: {
                    '__RequestVerificationToken': token
                }
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };
          
        function IngresarSistema(UsuarioDtoRequest, token) {
            return $http({
                url: $urls.ApiSeguridad + "Login/IngresarSistema",
                method: "POST",
                data: JSON.stringify(UsuarioDtoRequest),
                headers: {
                    '__RequestVerificationToken': token
                }
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        };

         
    }

})();

