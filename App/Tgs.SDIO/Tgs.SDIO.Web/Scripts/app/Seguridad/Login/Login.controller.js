﻿(function () {
    'use strict',

    angular
    .module('app.Seguridad')
    .controller('LoginController', LoginController);

    LoginController.$inject = ['LoginService', 'blockUI', '$timeout', 'UtilsFactory', 'MensajesUI'];

    function LoginController(LoginService, blockUI, $timeout, UtilsFactory,$MensajesUI) {
        var vm = this;
        vm.Login = Login;

        vm.TxtUsuario = '';
        vm.TxtClave = '';

        function Login() {
             
            var UsuarioDto = {
                Login: vm.TxtUsuario,
                Password: vm.TxtClave
            };

            if (UsuarioDto.Login == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese su usuario", 5);
                blockUI.stop();
                return;
            }

            if (UsuarioDto.Password == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese su clave", 5);
                blockUI.stop();
                return;
            }

            blockUI.start();
            var token = UtilsFactory.TokenAutorizacion();
            var promise = LoginService.LoginDatos(UsuarioDto,token);

            promise.then(function (response) {
              
                if (response.data.Respuesta.Error == "0") {
                    window.location.href = response.data.Respuesta.Mensaje;
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', response.data.Respuesta.Mensaje, 5); 
                }

                $timeout(function () {
                    blockUI.stop();
                }, 2500);

            }, function (response) {

                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', UtilsFactory.GetMensajeError(response), 6);
            });
        };
    }

})();

