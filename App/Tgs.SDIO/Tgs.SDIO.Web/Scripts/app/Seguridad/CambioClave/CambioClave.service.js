﻿(function () {
    'use strict',
    angular
    .module('app.Seguridad')
    .service('CambioClaveService', CambioClaveService);

    CambioClaveService.$inject = ['$http', 'URLS'];

    function CambioClaveService($http, $urls) {

        var service = { 
            CambiarClave: CambiarClave
        };

        return service;
  
        function CambiarClave(UsuarioDto) {
            return $http({
                url: $urls.ApiSeguridad + "CambioClave/CambiarClave",
                method: "POST",
                data: JSON.stringify(UsuarioDto)
            }).then(DatosCompletados);

            function DatosCompletados(response) {
                return response;
            }
        }; 
    }

})();

