﻿(function () {
    'use strict'
    angular
        .module('app.Trazabilidad')
        .controller('BandejaActividadOportunidad', BandejaActividadOportunidad);
    BandejaActividadOportunidad.$inject = ['BandejaActividadOportunidadService', 'EtapaOportunidadService', 'AreaSeguimientoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaActividadOportunidad(BandejaActividadOportunidadService, EtapaOportunidadService, AreaSeguimientoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarActividadOportunidad/Index/";
        vm.BuscarActividadOportunidad = BuscarActividadOportunidad;
        vm.EliminarActividadOportunidad = EliminarActividadOportunidad;
        vm.AceptarEliminarActividadOportunidad = AceptarEliminarActividadOportunidad;
        vm.ListarComboAreas = ListarComboAreas;
        vm.ListarComboEtapas = ListarComboEtapas;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Descripcion = "";
        vm.IdEtapa = "-1";
        vm.IdAreaSeguimiento = "-1";
        LimpiarGrilla();
        ListarComboAreas();
        ListarComboEtapas();
        vm.dtColumns = [            
            DTColumnBuilder.newColumn('IdActividad').withTitle('Código').notSortable().withOption('width', '3%'),
            DTColumnBuilder.newColumn('IdActividadPadre').withTitle('Código Agrupador').notSortable().withOption('width', '3%'),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripción').notSortable().renderWith().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Predecesoras').withTitle('Predecesoras').notSortable().withOption('width', '3%'),
            DTColumnBuilder.newColumn('DescripcionArea').withTitle('Area/Sistema').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn('DescripcionTipoActividad').withTitle('Tipo Actividad').notSortable().withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('Asegurar Oferta').notSortable().renderWith(CheckboxAsegurarOferta).withOption('width', '5%'),
            DTColumnBuilder.newColumn('DescripcionFase').withTitle('Fase').notSortable().withOption('width', '8%'),
            DTColumnBuilder.newColumn(null).withTitle('Capex Mayor').notSortable().renderWith(CheckboxFlgCapexMayor).withOption('width', '5%'),
            DTColumnBuilder.newColumn(null).withTitle('Capex Menor').notSortable().renderWith(CheckboxFlgCapexMenor).withOption('width', '5%'),
            DTColumnBuilder.newColumn('NumeroDiaCapexMenor').withTitle('N° Dia Capex Menor').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('CantidadDiasCapexMenor').withTitle('Cantidad Dias Capex Menor').notSortable().withOption('width', '11%'),
            DTColumnBuilder.newColumn('NumeroDiaCapexMayor').withTitle('N° Dia Capex Mayor').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('CantidadDiasCapexMayor').withTitle('Cantidad Dias Capex Mayor').notSortable().withOption('width', '11%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '3%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaAreaSeguimiento).withOption('width', '5%')
        ];
        //Lista los IdAreaSeguimiento y Descripciones de la tabla [TRAZABILIDAD].[AreaSeguimiento] que esten Activos.
        function ListarComboAreas() {
            blockUI.start();
            var promise = AreaSeguimientoService.ListarComboAreas();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Recursos", 5);
                } else {
                    vm.listAreas = UtilsFactory.AgregarItemSelect(respuesta);
                    vm.IdAreaSeguimiento = "-1";
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ListarComboEtapas() {
            blockUI.start();
            var promise = EtapaOportunidadService.ListarComboEtapas();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                } else {
                    vm.listEtapas = UtilsFactory.AgregarItemSelect(respuesta);
                    vm.IdEtapa = "-1";
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Eliminacion logica de la tabla [TRAZABILIDAD].[ActividadOportunidad] 
        function EliminarActividadOportunidad(IdActividad) {
            vm.btnEliminar = true;
            $("#idClave").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idClave").val(IdActividad);
        }
        function AceptarEliminarActividadOportunidad() {
            var IdActividad = angular.element(document.getElementById("idClave"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var actividad = {
                IdActividad: IdActividad.val()
            }
            blockUI.start();
            var promise = BandejaActividadOportunidadService.EliminarActividadOportunidad(actividad);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarActividadOportunidad()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
        //Filtro de la  tabla [TRAZABILIDAD].[ActividadOportunidad] por Descripcion, Area y Etapa.
        function BuscarActividadOportunidad() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withOption('scrollX', true)
                    .withFnServerData(BuscarActividadOportunidadPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('scrollCollapse', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarActividadOportunidadPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var actividad = {
                Descripcion: vm.Descripcion,
                IdAreaSeguimiento: vm.IdAreaSeguimiento,
                IdEtapa: vm.IdEtapa,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaActividadOportunidadService.ListarActividadOportunidadPaginado(actividad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListActividadOportunidadDto
                };
                blockUI.stop();
                fnCallback(result)

            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion y checkbox.
        function AccionesBusquedaAreaSeguimiento(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdActividad + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
                "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarActividadOportunidad(" + data.IdActividad + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        }
        function CheckboxFlgCapexMayor(data, type, full, meta) {
            if (data.FlgCapexMayor == true) {
                return '<input type="checkbox" checked="checked" disabled="disabled">';
            } else {
                return '<input type="checkbox" disabled="disabled">';
            }
        }
        function CheckboxFlgCapexMenor(data, type, full, meta) {
            if (data.FlgCapexMenor == true) {
                return '<input type="checkbox" checked="checked" disabled="disabled">';
            } else {
                return '<input type="checkbox" disabled="disabled">';
            }
        }
        function CheckboxAsegurarOferta(data, type, full, meta) {
            if (data.AsegurarOferta == true) {
                return '<input type="checkbox" checked="checked" disabled="disabled">';
            } else {
                return '<input type="checkbox" disabled="disabled">';
            }
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.Descripcion = "";
            vm.TipoEtapas = "";
            vm.TipoAreas = "";
            LimpiarGrilla();
        }
    }
})();