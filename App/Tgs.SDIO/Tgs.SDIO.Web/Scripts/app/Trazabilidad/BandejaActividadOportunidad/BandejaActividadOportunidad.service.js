﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaActividadOportunidadService', BandejaActividadOportunidadService);

    BandejaActividadOportunidadService.$inject = ['$http', 'URLS'];

    function BandejaActividadOportunidadService($http, $urls) {

        var service = {
            ListarActividadOportunidadPaginado: ListarActividadOportunidadPaginado,
            EliminarActividadOportunidad: EliminarActividadOportunidad
        };

        return service;

        function ListarActividadOportunidadPaginado(actividad) {
            debugger;
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaActividadOportunidad/ListarActividadOportunidad",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarActividadOportunidad(actividad) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaActividadOportunidad/EliminarActividadOportunidad",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();