﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaRecursoOportunidad', BandejaRecursoOportunidad);
    BandejaRecursoOportunidad.$inject = ['BandejaRecursoOportunidadService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaRecursoOportunidad(BandejaRecursoOportunidadService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarRecursoOportunidad/Index/";
        vm.EnlaceRegistrarMasivo = $urls.ApiTrazabilidad + "RegistrarRecursoOportunidadMasivo/Index";
        vm.BuscarRecursoOportunidad = BuscarRecursoOportunidad;
        vm.EliminarRecursoOportunidad = EliminarRecursoOportunidad;
        vm.AceptarEliminarRecursoOportunidad = AceptarEliminarRecursoOportunidad;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionRecurso = "";
        vm.IdOportunidadSF = "";
        vm.IdAsignacion = "";
        LimpiarGrilla();
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdAsignacion').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DescripcionRecurso').withTitle('Recurso').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('DescripcionRolRecurso').withTitle('Rol de Recurso').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('strFInicioAsignacion').withTitle('Fecha Inicio').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('strFFinAsignacion').withTitle('Fecha Fin').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('PorcentajeDedicacion').withTitle('% Dedicación').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];   
         //Eliminacion logica de la tabla [TRAZABILIDAD].[RolRecurso] 
        function EliminarRecursoOportunidad(IdAsignacion) {
            vm.btnEliminar = true;
            $("#idClave").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idClave").val(IdAsignacion);
        }
        function AceptarEliminarRecursoOportunidad() {
            var ddlIdClave = angular.element(document.getElementById("idClave"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var request = {
                IdAsignacion: ddlIdClave.val()
            }
            blockUI.start();
            var promise = BandejaRecursoOportunidadService.EliminarRecursoOportunidad(request);
            promise.then(function (resultado) {                
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarRecursoOportunidad()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
        //Filtro de la  tabla [TRAZABILIDAD].[RecursoOportunidad] por DescripcionRecurso y Oportunidad.
        function BuscarRecursoOportunidad() {
            debugger;
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarRecursoOportunidadPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarRecursoOportunidadPaginado(sSource, aoData, fnCallback, oSettings) { 
        debugger;
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start+length)/length;
            var recursoOportunidad = {
                DescripcionRecurso: vm.DescripcionRecurso,
                IdOportunidadSF: vm.IdOportunidadSF,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRecursoOportunidadService.ListarRecursoOportunidadPaginado(recursoOportunidad);
            promise.then(function (resultado) {
            debugger;
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoOportunidadDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
         //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
         //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdAsignacion + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
            "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarRecursoOportunidad(" + data.IdAsignacion + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        }
         //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionRecurso = "";
            vm.IdOportunidadSF = "";
            LimpiarGrilla();
        }
    }
})();