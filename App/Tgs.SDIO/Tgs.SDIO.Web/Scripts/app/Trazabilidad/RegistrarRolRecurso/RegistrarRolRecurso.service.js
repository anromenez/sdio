﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarRolRecursoService', RegistrarRolRecursoService);

    RegistrarRolRecursoService.$inject = ['$http', 'URLS'];

    function RegistrarRolRecursoService($http, $urls) {

        var service = {
            RegistrarRolRecurso: RegistrarRolRecurso
        };

        return service;

        function RegistrarRolRecurso(rol) {
            debugger;
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarRolRecurso/RegistrarRolRecurso",
                method: "POST",
                data: JSON.stringify(rol)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();