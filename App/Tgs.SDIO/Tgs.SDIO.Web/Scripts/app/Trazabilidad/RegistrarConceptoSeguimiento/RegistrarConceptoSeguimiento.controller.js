﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarConceptoSeguimiento', RegistrarConceptoSeguimiento);
    RegistrarConceptoSeguimiento.$inject = ['RegistrarConceptoSeguimientoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function RegistrarConceptoSeguimiento(RegistrarConceptoSeguimientoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraConceptoSeguimiento = RegistraConceptoSeguimiento;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaConceptoSeguimiento/Index/";
        vm.IdConcepto = "";
        vm.Descripcion = "";
        vm.Nivel = "";
        vm.OrdenVisual = "";
        vm.IdEstado = "1";
        CargarDatos();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
         //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
           if (jsonDatoConceptoSeguimiento == null || jsonDatoConceptoSeguimiento == "") {
                vm.IdConcepto = "";
                vm.Descripcion = "";
                vm.Nivel = "";
                vm.OrdenVisual = "";
                vm.IdEstado = "1";
            } else {
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdConcepto = jsonDatoConceptoSeguimiento.IdConcepto;
                vm.Descripcion = jsonDatoConceptoSeguimiento.Descripcion;
                vm.Nivel = jsonDatoConceptoSeguimiento.Nivel;
                vm.OrdenVisual = jsonDatoConceptoSeguimiento.OrdenVisual;
                vm.IdEstado = jsonDatoConceptoSeguimiento.IdEstado;
            }
        }
         //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdConcepto = "";
            vm.Descripcion = "";
            vm.Nivel = "";
            vm.OrdenVisual = "";
            vm.IdEstado = "-1";
        }
         //Registra en  la tabla [TRAZABILIDAD].[[ConceptoSeguimiento]] 
        function RegistraConceptoSeguimiento() {
            var concepto = {
                IdConcepto: vm.IdConcepto,
                Descripcion: vm.Descripcion,
                Nivel: vm.Nivel,
                OrdenVisual: vm.OrdenVisual,
                IdEstado: vm.IdEstado
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarConceptoSeguimientoService.RegistrarConceptoSeguimiento(concepto);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Descripcion) == "") {
                mensaje = mensaje + "Ingrese Descripcion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDescripcion', 'Rojo');
            }
            if ($.trim(vm.Nivel) == "") {
                mensaje = mensaje + "Ingrese Nivel" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNivel', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtDescripcion', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtNivel', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
        }
    }
})();