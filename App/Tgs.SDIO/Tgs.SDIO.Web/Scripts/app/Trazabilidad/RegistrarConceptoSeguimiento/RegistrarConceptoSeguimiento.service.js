﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarConceptoSeguimientoService', RegistrarConceptoSeguimientoService);

    RegistrarConceptoSeguimientoService.$inject = ['$http', 'URLS'];

    function RegistrarConceptoSeguimientoService($http, $urls) {

        var service = {
            RegistrarConceptoSeguimiento: RegistrarConceptoSeguimiento,          
        };

        return service;

        function RegistrarConceptoSeguimiento(concepto) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarConceptoSeguimiento/RegistrarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(concepto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };    


    }
})();