﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaObservacionOportunidad', BandejaObservacionOportunidad);
    BandejaObservacionOportunidad.$inject = ['BandejaObservacionOportunidadService', 'ConceptoSeguimientoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaObservacionOportunidad(BandejaObservacionOportunidadService, ConceptoSeguimientoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarObservacionOportunidad/Index/";
        vm.BuscarObservacionOportunidad = BuscarObservacionOportunidad;
        vm.EliminarObservacionOportunidad = EliminarObservacionOportunidad;
        vm.AceptarEliminarObservacionOportunidad = AceptarEliminarObservacionOportunidad;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.IdOportunidadSF = "";
        vm.IdConcepto = "-1";
        LimpiarGrilla();
        ListarTipoConcepto();
        vm.listRecursos = [];
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdObservacion').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('Oportunidad').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('Caso').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn('strFechaSeguimiento').withTitle('Fecha Seguimiento').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdSeguimiento').withTitle('Id Seguimiento').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DescripcionConcepto').withTitle('Concepto').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Observaciones').withTitle('Observaciones').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];
        //Lista los IdConcepto y Descripciones de la tabla [COMUN].[ConceptoSeguimiento] que esten Activos.
        function ListarTipoConcepto() {
            var promise = ConceptoSeguimientoService.ListarComboConceptoSeguimiento();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listConcepto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Eliminacion logica de la tabla [TRAZABILIDAD].[ObservacionOportunidad]
        function EliminarObservacionOportunidad(IdObservacion) {
            vm.btnEliminar = true;
            $("#idClave").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idClave").val(IdObservacion);
        }
        function AceptarEliminarObservacionOportunidad() {
            var ddlIdClave = angular.element(document.getElementById("idClave"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var objObservacion = {
                IdObservacion: ddlIdClave.val()
            }
            blockUI.start();
            var promise = BandejaObservacionOportunidadService.EliminarObservacionOportunidad(objObservacion);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarObservacionOportunidad()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
        //Filtro de la  tabla [TRAZABILIDAD].[ObservacionOportunidad] por IdOportunidadSF y Concepto.
        function BuscarObservacionOportunidad() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarObservacionOportunidadPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarObservacionOportunidadPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var observacion = {
                IdOportunidadSF: vm.IdOportunidadSF,
                IdConcepto: vm.IdConcepto,
                Indice: pageNumber,
                Tamanio: length
            };
            debugger;
            var promise = BandejaObservacionOportunidadService.ListarObservacionOportunidadPaginado(observacion);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListObservacionOportunidadDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdObservacion + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
            "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarObservacionOportunidad(" + data.IdObservacion + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.IdOportunidadSF = "";
            vm.IdConcepto = "-1";
            LimpiarGrilla();
            vm.listRecursos = UtilsFactory.AgregarItemSelect([]);
        }
    }
})();