﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaObservacionOportunidadService', BandejaObservacionOportunidadService);

    BandejaObservacionOportunidadService.$inject = ['$http', 'URLS'];

    function BandejaObservacionOportunidadService($http, $urls) {

        var service = {
            ListarObservacionOportunidadPaginado: ListarObservacionOportunidadPaginado,
            EliminarObservacionOportunidad: EliminarObservacionOportunidad
        };

        return service;

      
        function ListarObservacionOportunidadPaginado(Observacion) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaObservacionOportunidad/ListarObservacionOportunidad",
                method: "POST",
                data: JSON.stringify(Observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarObservacionOportunidad(Observacion) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaObservacionOportunidad/EliminarObservacionOportunidad",
                method: "POST",
                data: JSON.stringify(Observacion)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();