﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('OportunidadSTService', OportunidadSTService);

    OportunidadSTService.$inject = ['$http', 'URLS'];

    function OportunidadSTService($http, $urls) {

        var service = {
            ListaOportunidadSTModalObservacionPaginado: ListaOportunidadSTModalObservacionPaginado,
            ObtenerOportunidadSTPorId: ObtenerOportunidadSTPorId,
            ListaOportunidadSTModalRecursoPaginado: ListaOportunidadSTModalRecursoPaginado,
            ListaOportunidadSTMasivoPaginado: ListaOportunidadSTMasivoPaginado,
            ListaOportunidadSTSeguimientoPreventaPaginado: ListaOportunidadSTSeguimientoPreventaPaginado            
        };

        return service;

        function ListaOportunidadSTModalObservacionPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTModalObservacionPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerOportunidadSTPorId(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ObtenerOportunidadSTPorId",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTModalRecursoPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTModalRecursoPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTMasivoPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTMasivoPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListaOportunidadSTSeguimientoPreventaPaginado(oportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "OportunidadST/ListaOportunidadSTSeguimientoPreventaPaginado",
                method: "POST",
                data: JSON.stringify(oportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();