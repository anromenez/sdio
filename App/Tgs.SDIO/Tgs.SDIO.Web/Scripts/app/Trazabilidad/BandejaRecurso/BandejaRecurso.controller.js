﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaRecurso', BandejaRecurso);
    BandejaRecurso.$inject = ['BandejaRecursoService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaRecurso(BandejaRecursoService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarRecurso/Index/";
        vm.BuscarRecurso = BuscarRecurso;
        vm.EliminarRecurso = EliminarRecurso;
        vm.AceptarEliminarRecurso = AceptarEliminarRecurso;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Nombre = "";
        vm.IdTipoRecurso = "-1";
        LimpiarGrilla();
        ListarTipoRecurso();
        vm.listRecursos = [];
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdRecurso').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Nombre').withTitle('Nombre Completo').notSortable().withOption('width', '40%'),
          DTColumnBuilder.newColumn('TipoRecurso').withTitle('Tipo Recurso').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DNI').withTitle('DNI').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('UserNameSF').withTitle('User Name SF').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoRecurso() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 125
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listRecursos = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Eliminacion logica de la tabla [TRAZABILIDAD].[Recurso]
        function EliminarRecurso(IdRecurso) {
            vm.btnEliminar = true;
            $("#idClave").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idClave").val(IdRecurso);
        }
        function AceptarEliminarRecurso() {
            var ddlIdClave = angular.element(document.getElementById("idClave"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var objConcepto = {
                IdRecurso: ddlIdClave.val()
            }
            blockUI.start();
            var promise = BandejaRecursoService.EliminarRecurso(objConcepto);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarRecurso()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
        //Filtro de la  tabla [TRAZABILIDAD].[Recurso] por Descripcion.
        function BuscarRecurso() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarRecursoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarRecursoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var recurso = {
                Nombre: vm.Nombre,
                TipoRecurso: vm.IdTipoRecurso,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRecursoService.ListarRecursoPaginado(recurso);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdRecurso + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
            "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarRecurso(" + data.IdRecurso + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.Nombre = "";
            vm.IdTipoRecurso = "-1";
            LimpiarGrilla();
            vm.listRecursos = UtilsFactory.AgregarItemSelect([]);
        }
    }
})();