﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaRecursoService', BandejaRecursoService);

    BandejaRecursoService.$inject = ['$http', 'URLS'];

    function BandejaRecursoService($http, $urls) {

        var service = {
            ListarRecursoPaginado: ListarRecursoPaginado,
            EliminarRecurso: EliminarRecurso
        };

        return service;

      
        function ListarRecursoPaginado(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaRecurso/ListarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarRecurso(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaRecurso/EliminarRecurso",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();