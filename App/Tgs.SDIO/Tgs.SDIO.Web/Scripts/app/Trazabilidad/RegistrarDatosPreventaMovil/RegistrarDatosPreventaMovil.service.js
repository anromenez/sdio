﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarDatosPreventaMovilService', RegistrarDatosPreventaMovilService);

    RegistrarDatosPreventaMovilService.$inject = ['$http', 'URLS'];

    function RegistrarDatosPreventaMovilService($http, $urls) {

        var service = {
            RegistrarDatosPreventaMovil: RegistrarDatosPreventaMovil,
            ModalDatosPreventaMovil: ModalDatosPreventaMovil
        };

        return service;


        function RegistrarDatosPreventaMovil(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };    


        function ModalDatosPreventaMovil(datosPreventa) {

            return $http({
                url: $urls.ApiComun + "RegistrarDatosPreventaMovil/Index",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();