﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('DatosPreventaMovilService', DatosPreventaMovilService);

    DatosPreventaMovilService.$inject = ['$http', 'URLS'];

    function DatosPreventaMovilService($http, $urls) {

        var service = {
            ObtenerDatosPreventaMovilPorId: ObtenerDatosPreventaMovilPorId
        };

        return service;

        function ObtenerDatosPreventaMovilPorId(datosPreventa) {
            return $http({
                url: $urls.ApiTrazabilidad + "DatosPreventaMovil/ObtenerDatosPreventaMovilPorId",
                method: "POST",
                data: JSON.stringify(datosPreventa)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();