﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('ActividadOportunidadService', ActividadOportunidadService);

    ActividadOportunidadService.$inject = ['$http', 'URLS'];

    function ActividadOportunidadService($http, $urls) {

        var service = {
               ListarComboActividadPadres: ListarComboActividadPadres        
        };

        return service;

     function ListarComboActividadPadres() {
            return $http({
                url: $urls.ApiTrazabilidad + "ActividadOportunidad/ListarComboActividadPadres",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();