﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('AreaSeguimientoService', AreaSeguimientoService);

    AreaSeguimientoService.$inject = ['$http', 'URLS'];

    function AreaSeguimientoService($http, $urls) {

        var service = {
               ListarComboAreas: ListarComboAreas
        };

        return service;

     function ListarComboAreas() {
            return $http({
                url: $urls.ApiTrazabilidad + "AreaSeguimiento/ListarComboAreas",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();