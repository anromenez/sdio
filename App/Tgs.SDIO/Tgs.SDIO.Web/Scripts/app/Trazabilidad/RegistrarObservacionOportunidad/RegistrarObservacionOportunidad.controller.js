﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarObservacionOportunidad', RegistrarObservacionOportunidad);
    RegistrarObservacionOportunidad.$inject = ['RegistrarObservacionOportunidadService', 'ConceptoSeguimientoService', 'OportunidadSTService', 'CasoOportunidadService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function RegistrarObservacionOportunidad(RegistrarObservacionOportunidadService, ConceptoSeguimientoService, OportunidadSTService, CasoOportunidadService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraObservacionOportunidad = RegistraObservacionOportunidad;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaObservacionOportunidad/Index/";
        vm.IdObservacion = "";
        vm.IdOportunidad = "";
        vm.IdCaso = "";
        vm.FechaSeguimiento = "";
        vm.IdSeguimiento = "";
        vm.Observaciones = "";
        vm.IdConcepto = "-1";
        vm.IdEstado = "1";
        CargarDatos();
        ListarTipoConcepto();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los IdConcepto y Descripciones de la tabla [COMUN].[ConceptoSeguimiento] que esten Activos.
        function ListarTipoConcepto() {
            var promise = ConceptoSeguimientoService.ListarComboConceptoSeguimiento();
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listConcepto = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
         //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
            if (jsonDatoObservacionOportunidad == null || jsonDatoObservacionOportunidad == "") {
                vm.IdObservacion = "";
                vm.IdOportunidad = "";
                vm.IdCaso = "";
                vm.FechaSeguimiento = "";
                vm.IdSeguimiento = "";
                vm.Observaciones = "";
                vm.IdConcepto = "-1";
                vm.IdEstado = "1";
            } else {
                var dateParts2 = jsonDatoObservacionOportunidad.strFechaSeguimiento.split("/");
                var FechaSeguimiento = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdObservacion = jsonDatoObservacionOportunidad.IdObservacion;
                vm.Observaciones = jsonDatoObservacionOportunidad.Observaciones;
                vm.IdOportunidad = jsonDatoObservacionOportunidad.IdOportunidad;
                vm.IdCaso = jsonDatoObservacionOportunidad.IdCaso;
                CargarOportunidad(jsonDatoObservacionOportunidad.IdOportunidad);
                CargaCaso(jsonDatoObservacionOportunidad.IdCaso);
                vm.FechaSeguimiento = FechaSeguimiento;
                vm.IdSeguimiento = jsonDatoObservacionOportunidad.IdSeguimiento;
                vm.IdConcepto = jsonDatoObservacionOportunidad.IdConcepto;
                vm.IdEstado = jsonDatoObservacionOportunidad.IdEstado;
            }
        }
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
                vm.IdObservacion = "";
                vm.IdOportunidad = "";
                vm.IdCaso = "";
                vm.HDescripcionOportunidad = "";
                vm.HDescripcionCaso = "";
                vm.FechaSeguimiento = "";
                vm.IdSeguimiento = "";
                vm.Observaciones = "";
                vm.IdConcepto = "-1";
                vm.IdEstado = "1";
        }
         //Registra en  la tabla [TRAZABILIDAD].[ActividadOportunidad] 
        function RegistraObservacionOportunidad() {
            var observacion = {
                IdObservacion: vm.IdObservacion,
                Observaciones: vm.Observaciones,
                IdOportunidad: vm.IdOportunidad,
                IdCaso: vm.IdCaso,
                FechaSeguimiento: vm.FechaSeguimiento,
                IdSeguimiento: vm.IdSeguimiento,
                IdConcepto: vm.IdConcepto,
                IdEstado: vm.IdEstado
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarObservacionOportunidadService.RegistrarObservacionOportunidad(observacion);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Observaciones) == "") {
                mensaje = mensaje + "Ingrese Observaciones" + '<br/>';
                UtilsFactory.InputBorderColor('#Observaciones', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if ($.trim(vm.IdConcepto) == "-1") {
                mensaje = mensaje + "Elija Tipo Concepto" + '<br/>';
                UtilsFactory.InputBorderColor('#IdConcepto', 'Rojo');
            }

            if ($.trim(vm.FechaSeguimiento) == "") {
                mensaje = mensaje + "Ingrese Fecha" + '<br/>';
                UtilsFactory.InputBorderColor('#FechaSeguimiento', 'Rojo');
            }

            if ($.trim(vm.HDescripcionOportunidad) == "") {
                mensaje = mensaje + "Seleccione una Oportunidad" + '<br/>';
                UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Rojo');
            }

            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#Observaciones', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdConcepto', 'Ninguno');
            UtilsFactory.InputBorderColor('#FechaSeguimiento', 'Ninguno');
            UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Ninguno');
        }

        //************************************************ Modal Busqueda Oportunidad *************************************************************
        vm.BuscarOportunidadST = BuscarOportunidadST;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.HDescripcionCaso = "";
        vm.HDescripcionOportunidad = "";
        vm.DescripcionOportunidad = "";
        vm.DescripcionCaso = "";
        vm.IdCasoSF = "";
        vm.IdOportunidadSF = "";
        vm.SeleccionarOportunidadSF = SeleccionarOportunidadSF;
        LimpiarGrilla();
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdOportunidad').withTitle('ID').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('Oportunidad SF').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DescripcionOportunidad').withTitle('Nombre Oportunidad').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('IdCaso').withTitle('ID Caso').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('Nro del Caso SF').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('DescripcionCaso').withTitle('Descripción Caso').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('DescripcionSolicitud').withTitle('Tipo Solicitud').notSortable().withOption('width', '15%'),
          DTColumnBuilder.newColumn(null).withTitle('Acción').notSortable().renderWith(AccionesBusquedaOportunidadSF).withOption('width', '5%')
        ];
        //variables Globales
        var modalpopupBuscar = angular.element(document.getElementById("dlgBuscar"));
        //Funcion carga input de oportunidad por su Id
        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HDescripcionOportunidad = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Funcion carga input de Caso por su Id
        function CargaCaso(IdCaso) {
            debugger;
            var caso = {
                IdCaso: IdCaso
            };
            var promise = CasoOportunidadService.ObtenerCasoOportunidadPorId(caso);
            promise.then(function (resultado) {
                debugger;
                vm.HDescripcionCaso = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Evento de mostrar PopUp
        $("#btnBuscar").click(function () {
            modalpopupBuscar.modal('show');
            LimpiarFiltros();
        });

         //Filtro de la  tabla [TRAZABILIDAD].[OportunidadST] por Descripcion.
        function BuscarOportunidadST() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withOption('scrollX', true)
                .withFnServerData(BuscarOportunidadSTPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(8);
            }, 500);
        }
        function BuscarOportunidadSTPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var oportunidad = {
                DescripcionOportunidad: vm.DescripcionOportunidad,
                DescripcionCaso: vm.DescripcionCaso,
                IdCasoSF: vm.IdCasoSF,
                IdOportunidadSF: vm.IdOportunidadSF,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTModalObservacionPaginado(oportunidad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withOption('scrollX', true)
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionOportunidad = "";
            vm.DescripcionCaso = "";
            vm.IdCasoSF = "";
            vm.IdOportunidadSF = "";
            LimpiarGrilla();
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaOportunidadSF(data, type, full, meta) {
            return "<button  type='submit' class='btn btn-primary btn-sm' onclick='SeleccionarOportunidadSF(" + data.IdCaso + ", " + data.IdOportunidad + ");'> <span class='fa fa-hand-pointer-o' aria-hidden='true'></span></button>";
        }

        //Selecciona registro
        function SeleccionarOportunidadSF(IdCaso, IdOportunidad) {
            vm.IdOportunidad=IdOportunidad;
            vm.IdCaso = IdCaso;
            CargarOportunidad(IdOportunidad);
            CargaCaso(IdCaso);
            modalpopupBuscar.modal('hide');
        }


         //************************************************ Modal Busqueda Oportunidad ************************************************************
    }
})();