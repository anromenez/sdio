﻿(function () {
    'use strict'
    angular
        .module('app.Trazabilidad')
        .controller('BandejaAreaSeguimiento', BandejaAreaSeguimiento);
    BandejaAreaSeguimiento.$inject = ['BandejaAreaSeguimientoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaAreaSeguimiento(BandejaAreaSeguimientoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarAreaSeguimiento/Index/";
        vm.BuscarAreaSeguimiento = BuscarAreaSeguimiento;
        vm.EliminarAreaSeguimiento = EliminarAreaSeguimiento;
        vm.AceptarEliminarAreaSeguimiento = AceptarEliminarAreaSeguimiento;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionSegmento = "";
        vm.DescripcionArea = "";
        vm.IdAreaSeguimiento = "";
        vm.MensajeDeAlerta = "";
        LimpiarGrilla();
        vm.dtColumns = [
            DTColumnBuilder.newColumn('IdAreaSeguimiento').withTitle('Codigo').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Descripcion').withTitle('Descripcion Area').notSortable().withOption('width', '60%'),
            DTColumnBuilder.newColumn('OrdenVisual').withTitle('Orden Visual').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusquedaAreaSeguimiento).withOption('width', '10%')
        ];
        //Eliminacion logica de la tabla [TRAZABILIDAD].[AreaSeguimiento]
        function EliminarAreaSeguimiento(IdAreaSeguimiento) {
            vm.btnEliminar = true;
            $("#idArea").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idArea").val(IdAreaSeguimiento);
        }
        function AceptarEliminarAreaSeguimiento() {
            var ddlIdAreaSeguimiento = angular.element(document.getElementById("idArea"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var area = {
                IdAreaSeguimiento: ddlIdAreaSeguimiento.val()
            }
            blockUI.start();
            var promise = BandejaAreaSeguimientoService.EliminarAreaSeguimiento(area);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarAreaSeguimiento()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
        //Filtro de la  tabla [TRAZABILIDAD].[AreaSeguimiento] por Descripcion de area y Descripcion de segmento de negocio.
        function BuscarAreaSeguimiento() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withFnServerData(BuscarAreaSeguimientoPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarAreaSeguimientoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var area = {
                Descripcion: vm.DescripcionArea,
                DescripcionSegmento: vm.DescripcionSegmento,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaAreaSeguimientoService.ListarAreaSeguimientoPaginado(area);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListAreaSeguimientoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }                   
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaAreaSeguimiento(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdAreaSeguimiento + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
                "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarAreaSeguimiento(" + data.IdAreaSeguimiento + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        }
         //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionSegmento = '';
            vm.DescripcionArea = '';
            LimpiarGrilla();
        }
    }
})();