﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaAreaSeguimientoService', BandejaAreaSeguimientoService);

    BandejaAreaSeguimientoService.$inject = ['$http', 'URLS'];

    function BandejaAreaSeguimientoService($http, $urls) {

        var service = {
            ListarAreaSeguimientoPaginado: ListarAreaSeguimientoPaginado,
            EliminarAreaSeguimiento: EliminarAreaSeguimiento           
        };

        return service;

        function ListarAreaSeguimientoPaginado(area) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaAreaSeguimiento/ListarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarAreaSeguimiento(area) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaAreaSeguimiento/EliminarAreaSeguimiento",
                method: "POST",
                data: JSON.stringify(area)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();