﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarRecurso', RegistrarRecurso);
    RegistrarRecurso.$inject = ['RegistrarRecursoService', 'MaestraService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function RegistrarRecurso(RegistrarRecursoService, MaestraService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraRecurso = RegistraRecurso;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaRecurso/Index/";
        vm.IdRecurso = "";
        vm.Nombre = "";
        vm.IdTipoRecurso = "-1";
        vm.IdEstado = "1";
        CargarDatos();
        ListarTipoRecurso();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoRecurso() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 125
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listRecursos = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
         //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
            if (jsonDatoRecurso == null || jsonDatoRecurso == "") {
                vm.IdRecurso = "";
                vm.Nombre = "";
                vm.IdTipoRecurso = "-1";
                vm.IdEstado = "1";
              } else {
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdRecurso = jsonDatoRecurso.IdRecurso;
                vm.Nombre = jsonDatoRecurso.Nombre;
                vm.IdTipoRecurso = jsonDatoRecurso.TipoRecurso;
                vm.IdEstado = jsonDatoRecurso.IdEstado;
            }
        }
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdRecurso = "";
            vm.Nombre = "";
            vm.IdTipoRecurso = "-1";
            vm.IdEstado = "-1";
        }
         //Registra en  la tabla [TRAZABILIDAD].[ActividadOportunidad] 
        function RegistraRecurso() {
            var recurso = {
                IdRecurso: vm.IdRecurso,
                Nombre: vm.Nombre,
                TipoRecurso: vm.IdTipoRecurso,
                IdEstado: vm.IdEstado
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarRecursoService.RegistrarRecurso(recurso);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {

                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Nombre) == "") {
                mensaje = mensaje + "Ingrese Nombre" + '<br/>';
                UtilsFactory.InputBorderColor('#txtNombre', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            if ($.trim(vm.IdTipoRecurso) == "-1") {
                mensaje = mensaje + "Elija Tipo Recruso" + '<br/>';
                UtilsFactory.InputBorderColor('#IdTipoRecurso', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtNombre', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdTipoRecurso', 'Ninguno');
        }
    }
})();