﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RolRecursoService', RolRecursoService);

    RolRecursoService.$inject = ['$http', 'URLS'];

    function RolRecursoService($http, $urls) {

        var service = {
             ListarComboRolRecurso: ListarComboRolRecurso
        };

        return service;

      function ListarComboRolRecurso() {
            return $http({
                url: $urls.ApiTrazabilidad + "RolRecurso/ListarComboRolRecurso",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();