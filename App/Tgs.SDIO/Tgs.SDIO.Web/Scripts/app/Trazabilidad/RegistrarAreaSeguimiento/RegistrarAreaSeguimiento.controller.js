﻿(function() {
    'use strict'
    angular
        .module('app.Trazabilidad')
        .controller('RegistrarAreaSeguimiento', RegistrarAreaSeguimiento);
    RegistrarAreaSeguimiento.$inject = ['RegistrarAreaSeguimientoService', 'SegmentosNegocioService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function RegistrarAreaSeguimiento(RegistrarAreaSeguimientoService, SegmentosNegocioService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraAreaSeguimiento = RegistraAreaSeguimiento;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaAreaSeguimiento/Index/";
        vm.IdAreaSeguimiento = "";
        vm.Descripcion = "";
        vm.AreaSegmentoNegocio = "";
        vm.OrdenVisual = "";
        vm.IdEstado = "1";
        vm.SegmentoNegocioModel = [];
        CargarDatos();
        ListarComboSegmentos();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            vm.SegmentoNegocioSettings = {
                scrollableHeight: '200px',
                scrollable: true,
                enableSearch: false
            };
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocio();
            promise.then(function (resultado) {
                vm.SegmentoNegocioData = resultado.data;
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
              if (jsonDatoAreaSeguimiento == null || jsonDatoAreaSeguimiento == "") {
                vm.IdAreaSeguimiento = "";
                vm.Descripcion = "";
                vm.AreaSegmentoNegocio = "";
                vm.OrdenVisual = "";
                vm.IdEstado = "1";
            } else {
                 $('#ddlIdEstado').removeAttr('disabled');
                vm.IdAreaSeguimiento = jsonDatoAreaSeguimiento.IdAreaSeguimiento;
                vm.Descripcion = jsonDatoAreaSeguimiento.Descripcion;
                vm.AreaSegmentoNegocio = jsonDatoAreaSeguimiento.AreaSegmentoNegocio;
                vm.OrdenVisual = jsonDatoAreaSeguimiento.OrdenVisual;
                vm.SegmentoNegocioModel = jsonDatoAreaSeguimiento.ListSegmentoNegocio;
                vm.IdEstado = jsonDatoAreaSeguimiento.IdEstado;
            }
        }
        vm.SegmentoNegocioSettings = {
            scrollableHeight: '200px',
            scrollable: true,
            enableSearch: false
        };
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdAreaSeguimiento = "";
            vm.Descripcion = "";
            vm.OrdenVisual = "";
            vm.IdEstado = "-1";
            vm.SegmentoNegocioModel = [];
        }
        //Registra en  la tabla [TRAZABILIDAD].[AreaSeguimiento] 
        function RegistraAreaSeguimiento() {
            var area = {
                IdAreaSeguimiento: vm.IdAreaSeguimiento,
                Descripcion: vm.Descripcion,
                OrdenVisual: vm.OrdenVisual,
                IdEstado: vm.IdEstado,
                ListSegmentoNegocio: vm.SegmentoNegocioModel
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarAreaSeguimientoService.RegistrarAreaSeguimiento(area);
                promise.then(function(resultado) {
                    debugger;
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function(response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            } else {
                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function() {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";
            if ($.trim(vm.Descripcion) == "") {
                mensaje = mensaje + "Ingrese Descripcion" + '<br/>';
                UtilsFactory.InputBorderColor('#txtDescripcion', 'Rojo');
            }
            if ($.trim(vm.SegmentoNegocioModel) == "") {
                mensaje = mensaje + "Elija SegmentoNegocio" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlAreaSegmentoNegocio', 'Rojo');
            }
            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }

            if (!UtilsFactory.ValidarNumero(vm.OrdenVisual)) {
                mensaje = mensaje + "El Numero de orden visual debe ser numerico" + '<br/>';
                UtilsFactory.InputBorderColor('#txtOrdenVisual', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#txtDescripcion', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlAreaSegmentoNegocio', 'Ninguno');
            UtilsFactory.InputBorderColor('#txtOrdenVisual', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
        }
    }
})();