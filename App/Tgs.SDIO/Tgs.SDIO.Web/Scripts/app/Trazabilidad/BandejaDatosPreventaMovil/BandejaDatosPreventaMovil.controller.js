﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaDatosPreventaMovil', BandejaDatosPreventaMovil);
    BandejaDatosPreventaMovil.$inject = ['BandejaDatosPreventaMovilService', 'RegistrarDatosPreventaMovilService', 'MaestraService', 'EstadoCasoService', 'TipoOportunidadService', 'EtapaOportunidadService', 'SegmentosNegocioService', 'OportunidadSTService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaDatosPreventaMovil(BandejaDatosPreventaMovilService, RegistrarDatosPreventaMovilService, MaestraService, EstadoCasoService, TipoOportunidadService, EtapaOportunidadService, SegmentosNegocioService, OportunidadSTService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarDatosPreventaMovil/Index/";
        vm.BuscarDatosPreventaMovil = BuscarDatosPreventaMovil;
        vm.EliminarDatosPreventaMovil = EliminarDatosPreventaMovil;
        vm.AceptarEliminarDatosPreventaMovil = AceptarEliminarDatosPreventaMovil;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.DescripcionOportunidad = "";
        vm.DescripcionCliente = "";
        vm.IdEtapa = "-1";
        vm.IdEstadoCaso = "-1";
        vm.IdSegmentoNegocio = "-1";
        vm.IdTipoOportunidad = "-1";
        vm.Codigo = "-1";
        vm.PorcentajeCierre = "";
        LimpiarGrilla();
        ListarTipoComplejidad();
        vm.listComplejidad = [];
        ObtenerEtapa();
        vm.listEtapas = [];
        ListarComboSegmentos();
        vm.listSegmentosNegocios = [];
        ListarComboEstadosCaso();
        vm.listEstadosCaso = [];
        ListarComboTipoOportunidad();
        vm.listTiposOportunidad = [];

        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdDatosPreventaMovil').withTitle('ID Preventa').notSortable().notVisible(),
          DTColumnBuilder.newColumn('IdOportunidad').withTitle('ID Oportunidad').notSortable().notVisible(),
          DTColumnBuilder.newColumn('IdCaso').withTitle('ID Caso').notSortable().notVisible().notVisible(),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('ID  Oportunidad').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionOportunidad').withTitle('Nombre Oportunidad').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('IdCasoSF').withTitle('N° del Caso').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionCliente').withTitle('Nombre Cliente').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('Asunto').withTitle('Asunto').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionEstadoCaso').withTitle('Estado').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionEtapa').withTitle('Etapa Oportunidad').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionTipoOportunidad').withTitle('Tipo Oportunidad').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionTipoSolicitud').withTitle('Tipo Solicitud').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('ProbalidadExito').withTitle('Probalidad de Exito').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('strFechaApertura').withTitle('Fecha/Hora Apertura').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('strFechaCierre').withTitle('Fecha/Hora Cierre').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionTipoEntidadCliente').withTitle('Tipo Entidad Cliente').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('ImporteCapex').withTitle('Importe Capex').notSortable().withOption('width', '5%'),
          //DTColumnBuilder.newColumn('DescripcionSegmento').withTitle('Segmento Negocio').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn('DescripcionComplejidad').withTitle('Complejidad').notSortable().withOption('width', '5%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '5%')
        ];

        //****************************************************** Modal show *********************************************************************************

        vm.ModalDatosPreventaMovil = ModalDatosPreventaMovil;

        function ModalDatosPreventaMovil(IdDatosPreventaMovil) {
            vm.IdDatosPreventaMovil = IdDatosPreventaMovil;

            var datosPreventa = {
                IdDatosPreventaMovil: IdDatosPreventaMovil
            };

            var promise = RegistrarDatosPreventaMovilService.ModalDatosPreventaMovil(datosPreventa);
            promise.then(function (response) {
                var respuesta = $(response.data);

                $injector.invoke(function ($compile) {
                    var div = $compile(respuesta);
                    var content = div($scope);
                    $("#ContenidoDatosPreventaMovil").html(content);
                });

                $('#ModalRegistrarDatosPreventaMovil').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            }, function (response) {
                blockUI.stop();
            });

        };



        //vm.ModalDatosPreventaMovil = ModalDatosPreventaMovil;

        //function ModalDatosPreventaMovil(IdDatosPreventaMovil) {
            //vm.IdDatosPreventaMovil = IdDatosPreventaMovil;
            //var DatosPreventaMovil = {
            //    IdDatosPreventaMovil: IdDatosPreventaMovil
            //};

            //var promise = RegistrarSubServicioService.ModalSubServicio(DatosPreventaMovil);
            //promise.then(function (response) {
            //    var respuesta = $(response.data);

        //        $injector.invoke(function ($compile) {
        //            var div = $compile(respuesta);
        //            var content = div($scope);
        //            $("#ContenidoDatosPreventaMovil").html(content);
        //        });

        //        $('#ModalRegistrarDatosPreventaMovil').modal({
        //            keyboard: false,
        //            backdrop: 'static'
        //        });

        //};

        //****************************************************** Modal show *********************************************************************************

        //Lista los Valores y Descripciones de la tabla [COMUN].[Maestra] que esten Activos.
        function ListarTipoComplejidad() {
            var maestra = {
                IdEstado: 1,
                IdRelacion: 160
            };
            var promise = MaestraService.ListarMaestraPorIdRelacion(maestra);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                vm.listComplejidad = UtilsFactory.AgregarItemSelect(Respuesta);
            }, function (response) {
                blockUI.stop();
            });
        }
        //Lista los IdEtapa y Descripciones de la tabla [TRAZABILIDAD].[EtapaOportunidad] que esten Activos.
        function ObtenerEtapa() {
                var promise = EtapaOportunidadService.ListarComboEtapas();
                promise.then(function (response) {
                    if (response == "") {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Etapas", 5);
                    } else {
                        vm.listEtapas = UtilsFactory.AgregarItemSelect(response.data);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[SegmentoNegocio] que esten Activos.
        function ListarComboSegmentos() {
            debugger;
            var promise = SegmentosNegocioService.ListarComboSegmentoNegocioSimple();
            promise.then(function (response) {
                debugger;
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Segmentos de Negocio", 5);
                } else {
                    vm.listSegmentosNegocios = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[EstadoCaso] que esten Activos.
        function ListarComboEstadosCaso() {
            var promise = EstadoCasoService.ListarComboEstadoCaso();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Estados de Caso", 5);
                } else {
                    vm.listEstadosCaso = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Lista los IdSegmentoNegocio y Descripciones de la tabla [TRAZABILIDAD].[TipoOportunidad] que esten Activos.
        function ListarComboTipoOportunidad() {

            var promise = TipoOportunidadService.ListarComboTipoOportunidad();
            promise.then(function (response) {
                if (response == "") {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Tipos de Oportunidad", 5);
                } else {
                    vm.listTiposOportunidad = UtilsFactory.AgregarItemSelect(response.data);
                }
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Eliminacion logica de la tabla [TRAZABILIDAD].[Recurso]
        function EliminarDatosPreventaMovil(IdDatosPreventaMovil) {
            vm.btnEliminar = true;
            $("#idClave").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idClave").val(IdDatosPreventaMovil);
        }
        function AceptarEliminarDatosPreventaMovil() {
            var ddlIdClave = angular.element(document.getElementById("idClave"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var datosPreventa = {
                IdDatosPreventaMovil: ddlIdClave.val()
            }
            blockUI.start();
            var promise = BandejaDatosPreventaMovilService.EliminarDatosPreventaMovil(datosPreventa);
            promise.then(function (resultado) {
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarDatosPreventaMovil()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
        //Filtro de la  tabla [TRAZABILIDAD].[Recurso] por Descripcion.
        function BuscarDatosPreventaMovil() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                    .newOptions().withOption('bFilter', false)
                    .withOption('responsive', true)
                    .withOption('order', [])
                    .withOption('scrollX', true)
                    .withFnServerData(BuscarDatosPreventaMovilPaginado)
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withOption('scrollCollapse', true)
                    .withOption('paging', true)
                    .withOption('destroy', true)
                    .withPaginationType('full_numbers')
                    .withDisplayLength(10);
            }, 500);
        }
        function BuscarDatosPreventaMovilPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;
            var recurso = {
                DescripcionOportunidad: vm.DescripcionOportunidad,
                DescripcionCliente: vm.DescripcionCliente,
                PorcentajeCierre: vm.PorcentajeCierre,
                IdEtapa: vm.IdEtapa,
                IdEstadoCaso: vm.IdEstadoCaso,
                IdSegmentoNegocio: vm.IdSegmentoNegocio,
                IdTipoOportunidad: vm.IdTipoOportunidad,
                Codigo: vm.Codigo,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTSeguimientoPreventaPaginado(recurso);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTSeguimientoPreventaDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('data', [])
                .withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('destroy', true)
                .withOption('scrollCollapse', true)
                .withOption('order', [])
                .withDisplayLength(0)
                .withOption('scrollX', true)
                .withOption('paging', false);
        }
        //Inicializa  html de la columna Accion.
        //function AccionesBusqueda(data, type, full, meta) {
        //    return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdDatosPreventaMovil + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
        //    "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarDatosPreventaMovil(" + data.IdDatosPreventaMovil + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        //}
        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-2 col-sm-1'> <a title='Editar' class='btn btn-sm'   id='tab-button' class='btn ' data-toggle='modal'   ng-click='vm.ModalDatosPreventaMovil(" + data.IdDatosPreventaMovil + ");'>" + "<span class='fa fa-edit'></span></a></div> " +
            "<div class='col-xs-4 col-sm-1'><a title='Inactivar' class='btn btn-sm'  id='tab-button' class='btn '  onclick='EliminarDatosPreventaMovil(" + data.IdDatosPreventaMovil + ");' >" + "<span class='glyphicon glyphicon-trash'></span></a></div>";
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.DescripcionOportunidad = "";
            vm.DescripcionCliente = "";
            vm.IdEtapa = "-1";
            vm.IdEstadoCaso = "-1";
            vm.IdSegmentoNegocio = "-1";
            vm.IdTipoOportunidad = "-1";
            vm.Codigo = "-1";
            vm.PorcentajeCierre = "";
            LimpiarGrilla();
        }
    }
})();