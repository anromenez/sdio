﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('FuncionPropietarioService', FuncionPropietarioService);

    FuncionPropietarioService.$inject = ['$http', 'URLS'];

    function FuncionPropietarioService($http, $urls) {

        var service = {
            ListarComboFuncionPropietario: ListarComboFuncionPropietario
        };

        return service;

        function ListarComboFuncionPropietario() {
            return $http({
                url: $urls.ApiTrazabilidad + "FuncionPropietario/ListarComboFuncionPropietario",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();