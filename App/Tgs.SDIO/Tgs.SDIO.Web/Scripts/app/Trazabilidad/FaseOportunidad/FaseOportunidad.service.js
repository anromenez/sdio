﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('FaseOportunidadService', FaseOportunidadService);

    FaseOportunidadService.$inject = ['$http', 'URLS'];

    function FaseOportunidadService($http, $urls) {

        var service = {
             ListarComboFases: ListarComboFases
        };

        return service;

      function ListarComboFases() {
            return $http({
                url: $urls.ApiTrazabilidad + "FaseOportunidad/ListarComboFases",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();