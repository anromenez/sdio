﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('MotivoOportunidadService', MotivoOportunidadService);

    MotivoOportunidadService.$inject = ['$http', 'URLS'];

    function MotivoOportunidadService($http, $urls) {

        var service = {
            ListarComboMotivoOportunidad: ListarComboMotivoOportunidad
        };

        return service;

        function ListarComboMotivoOportunidad() {
            return $http({
                url: $urls.ApiTrazabilidad + "MotivoOportunidad/ListarComboMotivoOportunidad",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();