﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarRecursoOportunidadService', RegistrarRecursoOportunidadService);

    RegistrarRecursoOportunidadService.$inject = ['$http', 'URLS'];

    function RegistrarRecursoOportunidadService($http, $urls) {

        var service = {
            RegistrarRecursoOportunidad: RegistrarRecursoOportunidad
        };

        return service;

        function RegistrarRecursoOportunidad(recursoOportunidad) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarRecursoOportunidad/RegistrarRecursoOportunidad",
                method: "POST",
                data: JSON.stringify(recursoOportunidad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();