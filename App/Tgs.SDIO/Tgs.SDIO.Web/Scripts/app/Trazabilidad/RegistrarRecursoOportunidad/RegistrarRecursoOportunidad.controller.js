﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('RegistrarRecursoOportunidad', RegistrarRecursoOportunidad);
    RegistrarRecursoOportunidad.$inject = ['RegistrarRecursoOportunidadService', 'OportunidadSTService', 'RecursoService', 'RolRecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function RegistrarRecursoOportunidad(RegistrarRecursoOportunidadService, OportunidadSTService, RecursoService, RolRecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.CargarDatos = CargarDatos;
        vm.DesaparecerEfectosDeValidacion = DesaparecerEfectosDeValidacion;
        vm.ValidarCampos = ValidarCampos;
        vm.RegistraRecursoOportunidad = RegistraRecursoOportunidad;
        vm.EnlaceBandeja = $urls.ApiTrazabilidad + "BandejaRecursoOportunidad/Index/";
        vm.IdAsignacion = "";
        vm.IdEstado = "1";
        vm.IdOportunidad = "";
        vm.IdRecurso = "";
        vm.IdRol = "-1";
        vm.PorcentajeDedicacion = "";
        vm.FInicioAsignacion = "";
        vm.FFinAsignacion = "";
        vm.Observaciones = "";
        CargarDatos();
        ListarComboRolRecurso();
        vm.listEstados = [{ "Codigo": "-1", "Descripcion": "--Seleccione--" }, { "Codigo": "1", "Descripcion": "Activo" }, { "Codigo": "0", "Descripcion": "Inactivo" }];
        //Lista los IdFase y Descripciones de la tabla [TRAZABILIDAD].[RolRecurso] que esten Activos.
        function ListarComboRolRecurso() {
            blockUI.start();
            var promise = RolRecursoService.ListarComboRolRecurso();
            promise.then(function (response) {
                blockUI.stop();
                var respuesta = response.data;
                if (respuesta.length == 0) {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No Existen Recursos", 5);
                } else {
                    vm.listRolRecurso = UtilsFactory.AgregarItemSelect(respuesta);
                }
            }, function (response) {
                blockUI.stop();
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
            });
        }
        //Carga los controles con los datos por defecto si es nuevo y con los datos del registro seleccionado si existente.
        function CargarDatos() {
            if (jsonDatoRecursoOportunidad == null || jsonDatoRecursoOportunidad == "") {
                vm.IdAsignacion = "";
                vm.IdEstado = "1";
                vm.IdOportunidad = "";
                vm.IdRecurso = "";
                vm.IdRol = "-1";
                vm.PorcentajeDedicacion = "";
                vm.FInicioAsignacion = "";
                vm.FFinAsignacion = "";
                vm.Observaciones = "";
            } else {
                var dateParts1 = jsonDatoRecursoOportunidad.strFInicioAsignacion.split("/");
                var FechaInicio = new Date(dateParts1[2], dateParts1[1] - 1, dateParts1[0]);
                var dateParts2 = jsonDatoRecursoOportunidad.strFFinAsignacion.split("/");
                var FechaFin = new Date(dateParts2[2], dateParts2[1] - 1, dateParts2[0]);
                $('#ddlIdEstado').removeAttr('disabled');
                vm.IdAsignacion = jsonDatoRecursoOportunidad.IdAsignacion;
                vm.Observaciones = jsonDatoRecursoOportunidad.Observaciones;
                vm.IdEstado = jsonDatoRecursoOportunidad.IdEstado;
                vm.IdOportunidad = jsonDatoRecursoOportunidad.IdOportunidad;
                vm.IdRecurso = jsonDatoRecursoOportunidad.IdRecurso;
                CargaRecurso(jsonDatoRecursoOportunidad.IdRecurso);
                CargarOportunidad(jsonDatoRecursoOportunidad.IdOportunidad);
                vm.IdRol = jsonDatoRecursoOportunidad.IdRol;
                vm.PorcentajeDedicacion = jsonDatoRecursoOportunidad.PorcentajeDedicacion;
                vm.FInicioAsignacion = FechaInicio;
                vm.FFinAsignacion = FechaFin;
            }
        }
        //Limpia los controles del formulacion a sus valores por defecto.
        function LimpiarCampos() {
            vm.IdAsignacion = "";
            vm.IdEstado = "1";
            vm.IdOportunidad = "";
            vm.IdRecurso = "";
            vm.IdRol = "-1";
            vm.PorcentajeDedicacion = "";
            vm.FInicioAsignacion = "";
            vm.FFinAsignacion = "";
            vm.Observaciones = "";
            vm.HDescripcionOportunidad = "";
            vm.HDescripcionRecurso = "";
        }
        //Registra en  la tabla [TRAZABILIDAD].[RecursoOportunidad]
        function RegistraRecursoOportunidad() {
            var recursoOportunidad = {
                IdAsignacion: vm.IdAsignacion,
                Observaciones: vm.Observaciones,
                IdEstado: vm.IdEstado,
                IdOportunidad: vm.IdOportunidad,
                IdRecurso: vm.IdRecurso,
                IdRol: vm.IdRol,
                PorcentajeDedicacion: vm.PorcentajeDedicacion,
                FInicioAsignacion: vm.FInicioAsignacion,
                FFinAsignacion: vm.FFinAsignacion
            };
            var mensaje = ValidarCampos();
            if (mensaje == "") {
                blockUI.start();
                var promise = RegistrarRecursoOportunidadService.RegistrarRecursoOportunidad(recursoOportunidad);
                promise.then(function (resultado) {
                    blockUI.stop();
                    var Respuesta = resultado.data;
                    if (Respuesta.TipoRespuesta == 0) {
                        LimpiarCampos();
                        UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosOk, 5);
                    } else {
                        UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo registrar.", 5);
                    }
                }, function (response) {
                    UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                    blockUI.stop();
                });
            }
            else {
                UtilsFactory.Alerta('#divAlert', 'danger', mensaje, 5);
                $timeout(function () {
                    DesaparecerEfectosDeValidacion();
                }, 3000);
            }
        }
        //Valida los campos ingresados antes del registro.
        function ValidarCampos() {
            var mensaje = "";

            if ($.trim(vm.HDescripcionOportunidad) == "") {
                mensaje = mensaje + "Seleccione una Oportunidad" + '<br/>';
                UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Rojo');
            }
            if ($.trim(vm.HDescripcionRecurso) == "") {
                mensaje = mensaje + "Seleccione un Recurso" + '<br/>';
                UtilsFactory.InputBorderColor('#HDescripcionRecurso', 'Rojo');
            }
            if ($.trim(vm.IdRol) == "-1") {
                mensaje = mensaje + "Seleccione Rol de Recurso" + '<br/>';
                UtilsFactory.InputBorderColor('#IdRol', 'Rojo');
            }
            if ($.trim(vm.PorcentajeDedicacion) == "-1") {
                mensaje = mensaje + "Ingrese Porcentaje de dedicacion" + '<br/>';
                UtilsFactory.InputBorderColor('#PorcentajeDedicacion', 'Rojo');
            }

            if ($.trim(vm.IdEstado) == "-1") {
                mensaje = mensaje + "Elija Estado" + '<br/>';
                UtilsFactory.InputBorderColor('#ddlIdEstado', 'Rojo');
            }
            return mensaje;
        }
        function DesaparecerEfectosDeValidacion() {
            UtilsFactory.InputBorderColor('#HDescripcionOportunidad', 'Ninguno');
            UtilsFactory.InputBorderColor('#HDescripcionRecurso', 'Ninguno');
            UtilsFactory.InputBorderColor('#IdRol', 'Ninguno');
            UtilsFactory.InputBorderColor('#PorcentajeDedicacion', 'Ninguno');
            UtilsFactory.InputBorderColor('#ddlIdEstado', 'Ninguno');
        }
        //************************************************ Modal Busqueda Oportunidad *************************************************************
        vm.BuscarOportunidadST = BuscarOportunidadST;
        vm.LimpiarFiltrosO = LimpiarFiltrosO;
        vm.HDescripcionOportunidad = "";
        vm.DescripcionOportunidad = "";
        vm.IdOportunidadSF = "";
        vm.SeleccionarOportunidadSF = SeleccionarOportunidadSF;
        LimpiarGrillaO();
        vm.dtColumnsO = [
          DTColumnBuilder.newColumn('IdOportunidad').withTitle('ID').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('IdOportunidadSF').withTitle('ID Oportunidad SF').notSortable().withOption('width', '20%'),
          DTColumnBuilder.newColumn('DescripcionOportunidad').withTitle('Nombre Oportunidad').notSortable().withOption('width', '65%'),
          DTColumnBuilder.newColumn(null).withTitle('Acción').notSortable().renderWith(AccionesBusquedaOportunidadSF).withOption('width', '5%')
        ];
        //variables Globales
        var modalpopupBuscarO = angular.element(document.getElementById("dlgBuscarO"));
        //Funcion carga input de oportunidad por su Id
        function CargarOportunidad(IdOportunidad) {
            var oportunidad = {
                IdOportunidad: IdOportunidad
            };
            var promise = OportunidadSTService.ObtenerOportunidadSTPorId(oportunidad);
            promise.then(function (resultado) {
                vm.HDescripcionOportunidad = resultado.data.Descripcion;
            }, function (response) {
                LimpiarGrilla();
            });
        }
        //Evento de mostrar PopUp
        $("#btnBuscarO").click(function () {
            modalpopupBuscarO.modal('show');
            LimpiarFiltrosO();
        });

        //Filtro de la  tabla [TRAZABILIDAD].[OportunidadST] por Descripcion.
        function BuscarOportunidadST() {
            blockUI.start();
            LimpiarGrillaO();
            $timeout(function () {
                vm.dtOptionsO = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarOportunidadSTPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarOportunidadSTPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var oportunidad = {
                DescripcionOportunidad: vm.DescripcionOportunidad,
                IdOportunidadSF: vm.IdOportunidadSF,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = OportunidadSTService.ListaOportunidadSTModalRecursoPaginado(oportunidad);
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListOportunidadSTDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaO();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrillaO() {
            vm.dtOptionsO = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltrosO() {
            vm.DescripcionOportunidad = "";
            vm.IdOportunidadSF = "";
            LimpiarGrillaO();
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaOportunidadSF(data, type, full, meta) {
            return "<button  type='submit' class='btn btn-primary btn-sm' onclick='SeleccionarOportunidadSF(" + data.IdOportunidad + ");'> <span class='fa fa-hand-pointer-o' aria-hidden='true'></span></button>";
        }

        //Selecciona registro
        function SeleccionarOportunidadSF(IdOportunidad) {
            vm.IdOportunidad = IdOportunidad;
            CargarOportunidad(IdOportunidad);
            modalpopupBuscarO.modal('hide');
        }


        //************************************************ Modal Busqueda Oportunidad ************************************************************
        //************************************************ Modal Busqueda Recurso *************************************************************
        vm.BuscarRecurso = BuscarRecurso;
        vm.LimpiarFiltrosR = LimpiarFiltrosR;
        vm.DescripcionRecurso = "";
        vm.HDescripcionRecurso = "";
        vm.SeleccionarRecurso = SeleccionarRecurso;
        LimpiarGrillaR();
        vm.dtColumnsR = [
          DTColumnBuilder.newColumn('IdRecurso').withTitle('ID').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Nombre').withTitle('Nombre').notSortable().withOption('width', '75%'),
            DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acción').notSortable().renderWith(AccionesBusquedaRecurso).withOption('width', '5%')
        ];
        //variables Globales
        var modalpopupBuscarR = angular.element(document.getElementById("dlgBuscarR"));
        //Funcion carga input de Recurso por su Id
        function CargaRecurso(IdRecurso) {
            var recurso = {
                IdRecurso: IdRecurso
            };
            var promise = RecursoService.ObtenerRecursoPorId(recurso);
            promise.then(function (resultado) {
                vm.HDescripcionRecurso = resultado.data.Nombre;
            }, function (response) {
                LimpiarGrillaR();
            });
        }

        //Evento de mostrar PopUp
        $("#btnBuscarR").click(function () {
            modalpopupBuscarR.modal('show');
            LimpiarFiltrosR();
        });

        //Filtro de la  tabla [TRAZABILIDAD].[OportunidadST] por Descripcion.
        function BuscarRecurso() {
            blockUI.start();
            LimpiarGrillaR();
            $timeout(function () {
                vm.dtOptionsR = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarRecursoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarRecursoPaginado(sSource, aoData, fnCallback, oSettings) {
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start + length) / length;

            var recurso = {
                Nombre: vm.DescripcionRecurso,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = RecursoService.ListaRecursoModalRecursoPaginado(recurso);

            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrillaR();
            });
        }
        //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrillaR() {
            vm.dtOptionsR = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
        //Inicializa controles de los filtros de busqueda
        function LimpiarFiltrosR() {
            vm.DescripcionRecurso = "";
            LimpiarGrillaR();
        }
        //Inicializa  html de la columna Accion.
        function AccionesBusquedaRecurso(data, type, full, meta) {
            return "<button  type='submit' class='btn btn-primary btn-sm' onclick='SeleccionarRecurso(" + data.IdRecurso + ");'> <span class='fa fa-hand-pointer-o' aria-hidden='true'></span></button>";
        }

        //Selecciona registro
        function SeleccionarRecurso(IdRecurso) {
            vm.IdRecurso = IdRecurso;
            CargaRecurso(IdRecurso);
            modalpopupBuscarR.modal('hide');
        }


        //************************************************ Modal Busqueda Recurso ************************************************************
    }
})();