﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RegistrarActividadOportunidadService', RegistrarActividadOportunidadService);

    RegistrarActividadOportunidadService.$inject = ['$http', 'URLS'];

    function RegistrarActividadOportunidadService($http, $urls) {

        var service = {
            RegistrarActividadOportunidad: RegistrarActividadOportunidad,          
        };

        return service;

        function RegistrarActividadOportunidad(actividad) {
            return $http({
                url: $urls.ApiTrazabilidad + "RegistrarActividadOportunidad/RegistrarActividadOportunidad",
                method: "POST",
                data: JSON.stringify(actividad)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();