﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('ConceptoSeguimientoService', ConceptoSeguimientoService);

    ConceptoSeguimientoService.$inject = ['$http', 'URLS'];

    function ConceptoSeguimientoService($http, $urls) {

        var service = {
            ListarComboConceptoSeguimiento: ListarComboConceptoSeguimiento
        };

        return service;

      function ListarComboConceptoSeguimiento() {
            return $http({
                url: $urls.ApiTrazabilidad + "ConceptoSeguimiento/ListarComboConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();