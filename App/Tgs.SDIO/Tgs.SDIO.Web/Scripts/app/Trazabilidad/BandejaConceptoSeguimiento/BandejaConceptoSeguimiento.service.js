﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaConceptoSeguimientoService', BandejaConceptoSeguimientoService);

    BandejaConceptoSeguimientoService.$inject = ['$http', 'URLS'];

    function BandejaConceptoSeguimientoService($http, $urls) {

        var service = {
            ListarConceptoSeguimientoPaginado: ListarConceptoSeguimientoPaginado,
            EliminarConceptoSeguimiento: EliminarConceptoSeguimiento       
        };

        return service;

      
        function ListarConceptoSeguimientoPaginado(concepto) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaConceptoSeguimiento/ListarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(concepto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarConceptoSeguimiento(concepto) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaConceptoSeguimiento/EliminarConceptoSeguimiento",
                method: "POST",
                data: JSON.stringify(concepto)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };   


    }
})();