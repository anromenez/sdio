﻿(function () {
    'use strict'
    angular
    .module('app.Trazabilidad')
    .controller('BandejaRolRecurso', BandejaRolRecurso);
    BandejaRolRecurso.$inject = ['BandejaRolRecursoService', 'blockUI', 'UtilsFactory', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'MensajesUI', 'URLS'];
    function BandejaRolRecurso(BandejaRolRecursoService, blockUI, UtilsFactory, DTOptionsBuilder, DTColumnBuilder, $timeout, MensajesUI, $urls) {
        var vm = this;
        vm.EnlaceRegistrar = $urls.ApiTrazabilidad + "RegistrarRolRecurso/Index/";
        vm.BuscarRolRecurso = BuscarRolRecurso;
        vm.EliminarRolRecurso = EliminarRolRecurso;
        vm.AceptarEliminarRolRecurso = AceptarEliminarRolRecurso;
        vm.LimpiarFiltros = LimpiarFiltros;
        vm.Descripcion = "";
        vm.IdRol = "";
        LimpiarGrilla();
        vm.dtColumns = [
          DTColumnBuilder.newColumn('IdRol').withTitle('Código').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Descripcion').withTitle('Descripción').notSortable().withOption('width', '60%'),
          DTColumnBuilder.newColumn('Nivel').withTitle('Nivel').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn('Estado').withTitle('Activo').notSortable().withOption('width', '10%'),
          DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(AccionesBusqueda).withOption('width', '10%')
        ];   
         //Eliminacion logica de la tabla [TRAZABILIDAD].[[RolRecurso]] 
        function EliminarRolRecurso(IdRol) {
            vm.btnEliminar = true;
            $("#idClave").val("");
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            modalpopupConfirm.modal('show');
            $("#idClave").val(IdRol);
        }
        function AceptarEliminarRolRecurso() {
            var ddlIdClave = angular.element(document.getElementById("idClave"));
            var modalpopupConfirm = angular.element(document.getElementById("IdConfirmacionModal"));
            var objRol = {
                IdRol: ddlIdClave.val()
            }
            blockUI.start();
            var promise = BandejaRolRecursoService.EliminarRolRecurso(objRol);
            promise.then(function (resultado) {                
                blockUI.stop();
                var Respuesta = resultado.data;
                if (Respuesta.TipoRespuesta == 0) {
                    UtilsFactory.Alerta('#divAlert', 'success', MensajesUI.DatosDeleteOk, 5);
                    BuscarRolRecurso()
                } else {
                    UtilsFactory.Alerta('#divAlert', 'danger', "No se pudo eliminar el registro.", 5);
                }
                modalpopupConfirm.modal('hide');
            }, function (response) {
                UtilsFactory.Alerta('#divAlert', 'danger', MensajesUI.DatosError, 5);
                blockUI.stop();
            });
        }
         //Filtro de la  tabla [TRAZABILIDAD].[[RolRecurso]] por Descripcion.
        function BuscarRolRecurso() {
            blockUI.start();
            LimpiarGrilla();
            $timeout(function () {
                vm.dtOptions = DTOptionsBuilder
                .newOptions().withOption('bFilter', false)
                .withOption('responsive', true)
                .withOption('order', [])
                .withFnServerData(BuscarRolRecursoPaginado)
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('paging', true)
                .withOption('destroy', true)
                .withPaginationType('full_numbers')
                .withDisplayLength(10);
            }, 500);
        }
        function BuscarRolRecursoPaginado(sSource, aoData, fnCallback, oSettings) { 
            var draw = aoData[0].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var pageNumber = (start+length)/length;
            var rol = {
                Descripcion: vm.Descripcion,
                Indice: pageNumber,
                Tamanio: length
            };
            var promise = BandejaRolRecursoService.ListarRolRecursoPaginado(rol);         
            promise.then(function (resultado) {
                var result = {
                    'draw': draw,
                    'recordsTotal': resultado.data.TotalItemCount,
                    'recordsFiltered': resultado.data.TotalItemCount,
                    'data': resultado.data.ListRolRecursoDto
                };
                blockUI.stop();
                fnCallback(result)
            }, function (response) {
                blockUI.stop();
                LimpiarGrilla();
            });
        }
         //Inicializa la configuracion de las columnas de la grilla.
        function LimpiarGrilla() {
            vm.dtOptions = DTOptionsBuilder
            .newOptions()
            .withOption('data', [])
            .withOption('bFilter', false)
            .withOption('responsive', true)
            .withOption('destroy', true)
            .withOption('order', [])
            .withDisplayLength(0)
            .withOption('paging', false);
        }
         //Inicializa  html de la columna Accion.
        function AccionesBusqueda(data, type, full, meta) {
            return "<div class='col-xs-6 col-sm-6'><a title='Editar' class='btn btn-primary'  id='tab-button' href='" + vm.EnlaceRegistrar + data.IdRol + "' class='btn' style='background-color: #d5d807;' >" + "<span class='glyphicon glyphicon-edit'></span>" + "</a></div> " +
            "<div class='col-xs-6 col-sm-6'> <a title='Eliminar' class='btn btn-primary'   id='tab-button' class='btn '  onclick='EliminarRolRecurso(" + data.IdRol + ");'>" + "<span class='glyphicon glyphicon-trash'></span>" + "</a> </div>";
        }
         //Inicializa controles de los filtros de busqueda
        function LimpiarFiltros() {
            vm.Descripcion = "";
            LimpiarGrilla();
        }
    }
})();