﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('BandejaRolRecursoService', BandejaRolRecursoService);

    BandejaRolRecursoService.$inject = ['$http', 'URLS'];

    function BandejaRolRecursoService($http, $urls) {

        var service = {
            ListarRolRecursoPaginado: ListarRolRecursoPaginado,
            EliminarRolRecurso: EliminarRolRecurso
        };

        return service;


        function ListarRolRecursoPaginado(rol) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaRolRecurso/ListarRolRecurso",
                method: "POST",
                data: JSON.stringify(rol)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function EliminarRolRecurso(rol) {
            return $http({
                url: $urls.ApiTrazabilidad + "BandejaRolRecurso/EliminarRolRecurso",
                method: "POST",
                data: JSON.stringify(rol)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();