﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('EstadoCasoService', EstadoCasoService);

    EstadoCasoService.$inject = ['$http', 'URLS'];

    function EstadoCasoService($http, $urls) {

        var service = {
             ListarComboEstadoCaso: ListarComboEstadoCaso
        };

        return service;

      function ListarComboEstadoCaso() {
            return $http({
                url: $urls.ApiTrazabilidad + "EstadoCaso/ListarComboEstadoCaso",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();