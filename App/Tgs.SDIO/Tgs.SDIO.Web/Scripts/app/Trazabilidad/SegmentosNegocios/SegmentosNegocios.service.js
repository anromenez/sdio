﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('SegmentosNegocioService', SegmentosNegocioService);

    SegmentosNegocioService.$inject = ['$http', 'URLS'];

    function SegmentosNegocioService($http, $urls) {

        var service = {
            ListarComboSegmentoNegocio: ListarComboSegmentoNegocio,
            ListarComboSegmentoNegocioSimple: ListarComboSegmentoNegocioSimple
        };

        return service;

        function ListarComboSegmentoNegocio() {
            return $http({
                url: $urls.ApiTrazabilidad + "SegmentoNegocio/ListarComboSegmentoNegocio",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ListarComboSegmentoNegocioSimple() {
            return $http({
                url: $urls.ApiTrazabilidad + "SegmentoNegocio/ListarComboSegmentoNegocioSimple",
                method: "POST",
                data: JSON.stringify()
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();