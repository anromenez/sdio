﻿(function () {
    'use strict',
     angular
    .module('app.Trazabilidad')
    .service('RecursoService', RecursoService);

    RecursoService.$inject = ['$http', 'URLS'];

    function RecursoService($http, $urls) {

        var service = {
            ListaRecursoModalRecursoPaginado: ListaRecursoModalRecursoPaginado,
            ObtenerRecursoPorId: ObtenerRecursoPorId
            
        };

        return service;

        function ListaRecursoModalRecursoPaginado(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "Recurso/ListaRecursoModalRecursoPaginado",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

        function ObtenerRecursoPorId(recurso) {
            return $http({
                url: $urls.ApiTrazabilidad + "Recurso/ObtenerRecursoPorId",
                method: "POST",
                data: JSON.stringify(recurso)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };

    }
})();