﻿(function () {
    'use strict'

    angular
    .module('app.Base')
    .directive('selectormeses', function () {
        return {
            
            template: '<select id="CmbMes" class="form-control" ng-model="vm.ListaMeses" ng-options="item.codigo as item.descripcion for item in meses"><option value="" selected="selected">--Seleccione--</option></select>',
            restrict: 'E',
            link: function (scope, elem, attrs) {
                scope.meses = [
                    { codigo: '01', descripcion: 'Enero' },
                    { codigo: '02', descripcion: 'Febrero' },
                    { codigo: '03', descripcion: 'Marzo' },
                    { codigo: '04', descripcion: 'Abril' },
                    { codigo: '05', descripcion: 'Mayo' },
                    { codigo: '06', descripcion: 'Junio' },
                    { codigo: '07', descripcion: 'Julio' },
                    { codigo: '08', descripcion: 'Agosto' },
                    { codigo: '09', descripcion: 'Septiembre' },
                    { codigo: '10', descripcion: 'Obtubre' },
                    { codigo: '11', descripcion: 'Noviembre' },
                    { codigo: '12', descripcion: 'Diciembre' }
                ];
            }
        }
    });
    
})();

