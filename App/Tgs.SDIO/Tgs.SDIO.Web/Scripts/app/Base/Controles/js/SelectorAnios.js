﻿(function () {
    'use strict'

    angular
    .module('app.Base')
    .directive('selectoranios', function () {
        return {
            
            template: '<select id="CmbAnios" class="form-control" ng-model="vm.ListaAnios"  ng-options="item.codigo as item.descripcion for item in anios"></select>',
            restrict: 'E',
            link: function (scope, elem, attrs) {

                scope.anios = [
                    { codigo: '2018', descripcion: '2018' },
                    { codigo: '2019', descripcion: '2019' },
                    { codigo: '2020', descripcion: '2020' },
                    { codigo: '2021', descripcion: '2021' },
                    { codigo: '2022', descripcion: '2022' },
                    { codigo: '2023', descripcion: '2023' },
                    { codigo: '2024', descripcion: '2024' }
                ];

            }
        }
    });
    
})();

