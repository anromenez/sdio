﻿"use strict";
angular.module('app.Base', ["blockUI", "mgcrea.ngStrap", "ngAnimate", "ngSanitize", "summernote", "ui.mask"]).config(config);

function config(blockUIConfig, $httpProvider, $modalProvider) {
    blockUIConfig.autoInjectBodyBlock = false;
    blockUIConfig.autoBlock = false;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
 
    angular.extend($modalProvider.defaults, {
        html: true
    });
     
}

 
 