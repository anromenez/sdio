﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReporteEvolutivoOportunidadController', ReporteEvolutivoOportunidadController);

    ReporteEvolutivoOportunidadController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function ReporteEvolutivoOportunidadController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;
        vm.IdOportunidad = '';

        angular.element(document).ready(function () {

           
        });

        function LimpiarModelo() { 
            $("[id$='FReporte']").contents().find('body').html('');
            vm.IdOportunidad = '';
        };

        function ConsultarReporte() {

            var filtrosDto = { 
                IdOportunidad: vm.IdOportunidad
            };

            if (filtrosDto.IdOportunidad == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese la Oportunidad", 5);
                blockUI.stop();
                return;
            }
           
            if (vm.IdOportunidad == '' || vm.IdOportunidad == null) {
                vm.IdOportunidad = '0';
            }

            var parametros = "reporte=EvolutivoOportunidad&IdOportunidad=" + filtrosDto.IdOportunidad;

            blockUI.start();
            
            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            
            $timeout(function () {
                blockUI.stop();
            },2000);

        };
    }
})();

