﻿(function () {
    'use strict',
     angular
    .module('app.Funnel')
    .service('DashboardService', DashboardService);

    DashboardService.$inject = ['$http', 'URLS'];

    function DashboardService($http, $urls) {

        var service = {
            MostrarDashboard:MostrarDashboard           
        };

        return service;


        function MostrarDashboard(DashoardDtoRequest) {
            return $http({
                url: $urls.ApiFunnel + "Dashboard/MostrarDashboard",
                method: "POST",
                data: JSON.stringify(DashoardDtoRequest)
            }).then(DatosCompletados);

            function DatosCompletados(resultado) {
                return resultado;
            }
        };


    }
})();