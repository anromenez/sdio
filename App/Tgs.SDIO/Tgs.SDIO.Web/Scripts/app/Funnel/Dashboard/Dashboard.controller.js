﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReportDashboardController', ReportDashboardController);

    ReportDashboardController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS', 'DashboardService'];

    function ReportDashboardController(blockUI, $timeout, UtilsFactory, $urls,DashboardService) {
        var vm = this;
       vm.MostrarDashboard=MostrarDashboard;

       vm.CapexTotal = 0;
       vm.IngresoTotal = 0;
       vm.OibdaTotal = 0;
       vm.OpexTotal = 0;
       vm.OportunidadesTrabajadas = 0;
       vm.serctorF = 0;

        function MostrarDashboard() {
          var fechaActual = new Date();
            var anioActual = fechaActual.getFullYear();

            var dashboard =
            {
                Anio : anioActual
            };

            var promise = DashboardService.MostrarDashboard(dashboard);

            promise.then(function (resultado) {

                var result = resultado.data;
                vm.CapexTotal = result.CapexTotal;
                vm.IngresoTotal = result.IngresoTotal;
                vm.OibdaTotal = result.OibdaTotal;
                vm.OpexTotal = result.OpexTotal;
                vm.OportunidadesTrabajadas = result.OportunidadesTrabajadas;


                blockUI.stop();

                
            }, function (response) {
                blockUI.stop();
                
            });
        }
        //angular.element(document).ready(function () {

        //    var fechaActual = new Date();
        //    var anioActual = fechaActual.getFullYear();

        //    var dashboard =
        //    {
        //        Anio : anioActual
        //    };

        //    var promise = DashboardService.MostrarDashboard(dashboard);

        //    promise.then(function (resultado) {
        //        blockUI.stop();

                
        //    }, function (response) {
        //        blockUI.stop();
                
        //    });
            
        //});
        
    }
})();

