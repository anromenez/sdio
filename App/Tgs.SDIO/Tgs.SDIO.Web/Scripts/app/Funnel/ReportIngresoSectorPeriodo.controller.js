﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReportIngresoSectorPeriodoController', ReportIngresoSectorPeriodoController);

    ReportIngresoSectorPeriodoController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function ReportIngresoSectorPeriodoController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;
        vm.ValidarNumero = ValidarNumero;
        
        vm.txtAnio = '';
        vm.ListaMeses = '';

        function ValidarNumero(evento) {
            if (evento.keyCode != 8) {
                if (!UtilsFactory.ValidarNumero(evento.key)) {
                    return evento.preventDefault();
                };
            };
        };

        angular.element(document).ready(function () {

            // Return today's date and time
            var fechaActual = new Date();
            // returns the year (four digits)
            var anioActual = fechaActual.getFullYear();

            vm.txtAnio = anioActual;
            $('#txtAnioId').val(anioActual);
        });

        function LimpiarModelo() {
            
            vm.txtAnio = '';
            $("[id$='FReporte']").contents().find('body').html('');
            vm.ListaMeses = '';
        };

        function ConsultarReporte() {

            var filtrosDto = { 
                Anio: vm.txtAnio
            };

            if (filtrosDto.Anio == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese el año de búsqueda", 5);
                blockUI.stop();
                return;
            }
           
            var parametros = '';
            if (vm.ListaMeses == '' || vm.ListaMeses == null) {
                vm.ListaMeses = '0';
            }

           var parametros =
                    "reporte=IngresoSectorPeriodo&Anio=" + filtrosDto.Anio +
                    '&Mes=' + vm.ListaMeses;

            blockUI.start();
            
            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            
            $timeout(function () {
                blockUI.stop();
            },2000);

        };
    }
})();

