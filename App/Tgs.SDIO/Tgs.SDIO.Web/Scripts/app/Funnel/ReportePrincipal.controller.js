﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReportPrincipalController', ReportPrincipalController);

    ReportPrincipalController.$inject = ['blockUI', '$timeout', 'UtilsFactory', 'URLS'];

    function ReportPrincipalController(blockUI, $timeout, UtilsFactory, $urls) {
        var vm = this;
        vm.ConsultarReporte = ConsultarReporte;
        vm.LimpiarModelo = LimpiarModelo;
        vm.ValidarNumero = ValidarNumero;
        
        vm.txtAnio = '';
        vm.ListaMeses = '';
        vm.ListaFiltros = '';

        function ValidarNumero(evento) {
            if (evento.keyCode != 8) {
                if (!UtilsFactory.ValidarNumero(evento.key)) {
                    return evento.preventDefault();
                };
            };
        };

        angular.element(document).ready(function () {

            // Return today's date and time
            var fechaActual = new Date();
            // returns the year (four digits)
            var anioActual = fechaActual.getFullYear();

            vm.txtAnio = anioActual;
            $('#txtAnioId').val(anioActual);
        });

        function LimpiarModelo() {
            vm.ListaFiltros = '';
            vm.txtAnio = '';
            vm.ListaMeses = '';
            $("[id$='FReporte']").contents().find('body').html('');
        };

        function ConsultarReporte() {

            var nombreReporte = '';

            var filtrosDto = { 
                Anio: vm.txtAnio
            };

            if (vm.ListaFiltros == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese el Filtro de búsqueda", 5);
                blockUI.stop();
                return;
            }

            if (filtrosDto.Anio == '') {
                UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese el año de búsqueda", 5);
                blockUI.stop();
                return;
            }

            //if (vm.ListaMeses == '') {
            //    UtilsFactory.Alerta('#divAlert', 'danger', "Ingrese el mes de búsqueda", 5);
            //    blockUI.stop();
            //    return;
            //}
           
            var parametros = '';
            if (vm.ListaMeses == '' || vm.ListaMeses == null) {
                vm.ListaMeses = '0';
            }

            switch (vm.ListaFiltros) {
                case "1": nombreReporte = 'DashboardPrincipal';
                    parametros =
                    "reporte=" + nombreReporte + "&Anio=" + filtrosDto.Anio + 
                    '&Mes=' + vm.ListaMeses + 
                    "&urlReporte=" + encodeURIComponent(UtilsFactory.GetUrlAbsoluta() + "Funnel/Reportes/RptProbabilidadPorMes");
                    break;
                case "2": nombreReporte = 'MadurezPorMes';
                    parametros =
                    "reporte=" + nombreReporte + "&Anio=" + filtrosDto.Anio + 
                    '&Mes=' + vm.ListaMeses +
                    "&urlReporte=" + encodeURIComponent(UtilsFactory.GetUrlAbsoluta() + "Funnel/Reportes/RptProbabilidadPorMes");
                    break;
                default:
                    nombreReporte = '';
            }

            blockUI.start();
            
            $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            
            $timeout(function () {
                blockUI.stop();
            },2000);

        };
    }
})();

