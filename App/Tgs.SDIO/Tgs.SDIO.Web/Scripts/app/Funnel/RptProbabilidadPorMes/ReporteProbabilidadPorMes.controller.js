﻿(function () {
    'use strict',

    angular
    .module('app.Funnel')
    .controller('ReporteProbabilidadPorMesController', ReporteProbabilidadPorMesController);

    ReporteProbabilidadPorMesController.$inject = ['UtilsFactory', 'URLS'];

    function ReporteProbabilidadPorMesController(UtilsFactory, $urls) {
        var vm = this;

        angular.element(document).ready(function () {
            
            if (IdOportunidad != '' || IdOportunidad != undefined) {

                var parametros =
                    "reporte=ProbabilidadPorMesPopup&IdOportunidad=" + IdOportunidad;
                $("[id$='FReporte']").attr('src', UtilsFactory.GetUrlAbsoluta() + $urls.ApiReportes + parametros);
            }
        });
    }
})();

