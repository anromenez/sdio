﻿using System.Linq;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class OportunidadContenedorController : Controller
    {


        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly  AgenteServicioComunSDio agenteServicioComunSDio = null;

        public OportunidadContenedorController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
           AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Oportunidad/OportunidadContenedor
        public ActionResult Index(int Id)
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }
            ViewBag.vbOptionAnalistaFin = Generales.EstadoLogico.Faslo;
            ViewBag.vbOptionCoordinadorFin = Generales.EstadoLogico.Faslo;
            ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Faslo;



            MaestraDtoRequest maestra = new MaestraDtoRequest();
            maestra.IdEstado = Generales.Numeric.Uno;
            maestra.IdRelacion = Generales.TablaMaestra.EstadoProyectos;
            maestra.Valor2 = CodPerfil.ToUpper();
      
            var estadoPerfil = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestra(maestra));
            if (Id > 0)
            {
                OportunidadDtoRequest oportunidad = new OportunidadDtoRequest();
                OportunidadLineaNegocioDtoRequest lineaNegocio = new OportunidadLineaNegocioDtoRequest();
                

                OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado = new OportunidadFlujoEstadoDtoRequest();


                oportunidad.IdOportunidad = Id;

                var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(oportunidad));

                resultado.IdLineaNegocio = Generales.LineasProducto.Datos;
                resultado.FechaOportunidad = resultado.Fecha.ToString("dd/MM/yyyy");
                resultado.IdOportunidad = Id;

                lineaNegocio.IdOportunidad= Id;

                oportunidadFlujoEstado.IdOportunidad = Id;

                var resultadoLinea = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadLineaNegocio(lineaNegocio));

               

                var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado));

                ViewBag.VbAccionGrabar = Generales.EstadoLogico.Faslo;


                ViewBag.VbListEstado = estadoPerfil;
                resultado.IdEstado = estadoOportunidad.IdEstado;

                if (estadoOportunidad.IdEstado == Generales.Estados.EnProceso && CodPerfil != Generales.TipoPerfil.Coordinador_FIN && CodPerfil != Generales.TipoPerfil.Analista_FIN)
                {

                    ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;

                    estadoPerfil.RemoveRange(2, 1);

                    ViewBag.VbListEstado = estadoPerfil;
                    ViewBag.VbAccionGrabar = Generales.EstadoLogico.Verdadero;

                }
                if (estadoOportunidad.IdEstado == Generales.Estados.registrado && CodPerfil != Generales.TipoPerfil.Analista_FIN && CodPerfil != Generales.TipoPerfil.Preventa)
                {


                    ViewBag.vbOptionCoordinadorFin = Generales.EstadoLogico.Verdadero;
                    ViewBag.VbListEstado = estadoPerfil;
                    ViewBag.VbAccionGrabar = Generales.EstadoLogico.Verdadero;
                    resultado.IdEstado = Generales.Numeric.NegativoUno;
                }
                if (estadoOportunidad.IdEstado == Generales.Estados.EvaluacionEconomica && CodPerfil != Generales.TipoPerfil.Preventa&& CodPerfil != Generales.TipoPerfil.Coordinador_FIN)
                {

                    ViewBag.vbOptionAnalistaFin = Generales.EstadoLogico.Verdadero;
                    ViewBag.VbListEstado = estadoPerfil;
                    ViewBag.VbAccionGrabar = Generales.EstadoLogico.Verdadero;
                    resultado.IdEstado = Generales.Numeric.NegativoUno;
                }
                if (estadoOportunidad.IdEstado == Generales.Estados.Aprobado && CodPerfil != Generales.TipoPerfil.Coordinador_FIN& CodPerfil != Generales.TipoPerfil.Analista_FIN)
                {
                    ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
                    estadoPerfil.RemoveRange(0, 2);
                    ViewBag.VbListEstado = estadoPerfil;
                    ViewBag.VbAccionGrabar = Generales.EstadoLogico.Verdadero;
                    resultado.IdEstado = Generales.Numeric.NegativoUno;

                }
                if (estadoOportunidad.IdEstado == Generales.Estados.Rechazado && CodPerfil != Generales.TipoPerfil.Coordinador_FIN && CodPerfil != Generales.TipoPerfil.Analista_FIN )
                {
                    ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
                    estadoPerfil.RemoveRange(2, 1);
                    ViewBag.VbListEstado = estadoPerfil;
                    ViewBag.VbAccionGrabar = Generales.EstadoLogico.Verdadero;
                    resultado.IdEstado = Generales.Numeric.NegativoUno;

                }

              
                
                resultado.IdLineaNegocio = resultadoLinea.IdLineaNegocio;
                resultado.IdOportunidadLineaNegocio = resultadoLinea.IdOportunidadLineaNegocio;
                ViewBag.VbOportunidad = resultado;

            
            }
            else
            {
                ViewBag.VbAccionGrabar = Generales.EstadoLogico.Verdadero;
                estadoPerfil.RemoveRange(2, 1);
                ViewBag.VbListEstado = estadoPerfil;

                ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;
               
                ViewBag.VbOportunidad = "";
            }
            return View("Contenedor");
        }

        public ActionResult Detalle()
        {
            return View("DetalleContenedor");
        }



    }
}