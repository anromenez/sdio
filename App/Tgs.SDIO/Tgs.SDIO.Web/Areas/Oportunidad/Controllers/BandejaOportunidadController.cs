﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;


namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class BandejaOportunidadController : Controller
    {
        // GET: Oportunidad/BandejaOportunidad
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public BandejaOportunidadController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio,
            AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }


        //[CustomAuthorize]
        //[NoCache]
        public ActionResult Index()
        {


            OportunidadDtoRequest oportunidad = new OportunidadDtoRequest();
            LineaNegocioDtoRequest LineaNegocio = new LineaNegocioDtoRequest();
            MaestraDtoRequest maestra = new MaestraDtoRequest();
            UsuarioDtoRequest usuario = new UsuarioDtoRequest();
            DireccionComercialDtoRequest direccioncomercial = new DireccionComercialDtoRequest();

            oportunidad.IdLineaNegocio = Generales.LineasProducto.Datos;
            oportunidad.IdCliente = Generales.Numeric.Cero;
            oportunidad.Descripcion = "";
            oportunidad.NumeroSalesForce = "";
            oportunidad.IdGerente = Generales.Numeric.Cero;
            oportunidad.IdDireccion = Generales.Numeric.Cero;
            oportunidad.IdEstado = Generales.Estados.Inactivo;

            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);
            }
            ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Faslo;
        

            LineaNegocio.IdEstado = Generales.Estados.Activo;

            maestra.IdEstado = Generales.Estados.Activo;
            maestra.IdRelacion = Generales.TablaMaestra.EstadoProyectos;


            if (CodPerfil == Generales.TipoPerfil.Preventa)
            {
                oportunidad.IdPreVenta = SessionFacade.Usuario.UserId;
                ViewBag.vbOptionPreVenta = Generales.EstadoLogico.Verdadero;

            }
            if (CodPerfil == Generales.TipoPerfil.Coordinador_FIN)
            {
                oportunidad.IdCoordinadorFinanciero = SessionFacade.Usuario.UserId;

            }

            if (CodPerfil == Generales.TipoPerfil.Analista_FIN)
            {
                oportunidad.IdAnalistaFinanciero = SessionFacade.Usuario.UserId;

            }
            var ListOportunidadCabecera = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarOportunidadCabecera(oportunidad));

            ViewBag.vbListOportunidadCabecera = ListOportunidadCabecera;

            var ListLineaNegocio = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(LineaNegocio));

            ViewBag.vbListLineaNegocio = ListLineaNegocio;

            var ListMaestra = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestraPorIdRelacion(maestra));

            ViewBag.vbListMaestra = ListMaestra;

            usuario.CodigoSistema = "SDIO";


            usuario.CodigoPerfil = Generales.TipoPerfil.Gerente;
            usuario.IdEmpresa = 1;
            var ListGerente = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario));

            ViewBag.vbListGerente = ListGerente;
            direccioncomercial.IdEstado = Generales.Numeric.Uno;

            var ListDireccion = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarDireccionComercial(direccioncomercial));

            ViewBag.vbListDireccion = ListDireccion;

            return View();
        }



        //[HttpPost]
        //[CustomAuthorize]
        public JsonResult ListarOportunidadCabecera(OportunidadDtoRequest request)
        {
            var perfil = SessionFacade.Usuario.CodigoPerfil;
            var CodPerfil = string.Empty;
            for (int i = 0; i < perfil.Length; i++)
            {
                CodPerfil = (perfil[i]);

            }
            if (CodPerfil == Generales.TipoPerfil.Preventa)
            {
                request.IdPreVenta = SessionFacade.Usuario.UserId;
            }
            if (CodPerfil == Generales.TipoPerfil.Coordinador_FIN)
            {
                request.IdCoordinadorFinanciero = SessionFacade.Usuario.UserId;
            }
            if (CodPerfil == Generales.TipoPerfil.Product_Manager)
            {
                request.IdProductManager = SessionFacade.Usuario.UserId;
            }

            if (CodPerfil == Generales.TipoPerfil.Analista_FIN)
            {
                request.IdAnalistaFinanciero = SessionFacade.Usuario.UserId;
            }
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ListarOportunidadCabecera(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerVersiones(OportunidadDtoRequest request)
        {

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerVersiones(request));
            //


            //string CodSistema = Configuracion.CodigoAplicacion;
            //int IdEmpresa = Configuracion.CodigoEmpresa;
            string CodSistema = "SDIO";
            int IdEmpresa = Generales.Numeric.Uno;
            string CodPerfilAdmin = Generales.TipoPerfil.Preventa;
            string CodPerfilProduct = Generales.TipoPerfil.Product_Manager;
            string CodPerfilAnalista = Generales.TipoPerfil.Analista_FIN;

            //var perfil = SessionFacade.Usuario.CodigoPerfil;
            //var CodPerfilAdmin = string.Empty;
            //for (int i = 0; i < perfil.Length; i++)
            //{
            //    CodPerfilAdmin = (perfil[i]);
            //}


            UsuarioDtoRequest usuario_Admin = new UsuarioDtoRequest();
            UsuarioDtoResponse ListPerfilAdmin_2 = new UsuarioDtoResponse();
            //UsuarioDtoRequest usuario_Analista = new UsuarioDtoRequest();
            List<UsuarioDtoResponse> obj = new List<UsuarioDtoResponse>();


            usuario_Admin.CodigoPerfil = CodPerfilAdmin;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;
            ListPerfilAdmin_2.NombresCompletos = "";

            var ListPerfilAdmin = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorIdUsuario(resultado[0].IdUsuarioCreacion));
            
            if (request.IdUsuarioEdicion != null)
            {
                ListPerfilAdmin_2 = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorIdUsuario((int)resultado[0].IdUsuarioEdicion));
            }

         

            usuario_Admin.CodigoPerfil = CodPerfilProduct;
            var ListPerfilProduct_Manager = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            usuario_Admin.CodigoPerfil = CodPerfilAnalista;
            var ListPerfilAnalista = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));





            var idCreacion = resultado[0].IdUsuarioCreacion;
            var idProduct = resultado[0].IdProductManager;
            var idAnalista = resultado[0].IdAnalistaFinanciero;
            var idPreventa = resultado[0].IdPreVenta;
            if (resultado[0].IdUsuarioModifica != null)
            {
                var id = resultado[0].IdUsuarioModifica;
                var resul = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPorIdUsuario((int)id));
                resultado[0].NomCompletUsuMod = resul.NombresCompletos;
            }


            //ListPerfilAdmin = ListPerfilAdmin.Where<UsuarioDtoResponse>(p => p.IdUsuario == idCreacion).ToList<UsuarioDtoResponse>();
            //ListPerfilProduct_Manager = ListPerfilProduct_Manager.Where<UsuarioDtoResponse>(p => p.IdUsuario == idProduct).ToList<UsuarioDtoResponse>();
            //ListPerfilAnalista = ListPerfilAnalista.Where<UsuarioDtoResponse>(p => p.IdUsuario == idAnalista).ToList<UsuarioDtoResponse>();
            //ListPerfilAdmin_2 = ListPerfilAdmin_2.Where<UsuarioDtoResponse>(p => p.IdUsuario == idPreventa).ToList<UsuarioDtoResponse>();

            resultado[0].NomCompletUsuCrea = ListPerfilAdmin.NombresCompletos;
            resultado[0].NomCompletProductManager = ListPerfilProduct_Manager[0].NombresCompletos;
            resultado[0].NomCompletAnalistaFinanciero = ListPerfilAnalista[0].NombresCompletos;
            resultado[0].NomCompletPreventa = ListPerfilAdmin.NombresCompletos;




            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DatosPorVersion(OportunidadDtoRequest request)
        {
            string CodSistema = "SDIO";
            int IdEmpresa = Generales.Numeric.Uno;
            string CodPerfilAdmin = Generales.TipoPerfil.Administrador;
            string CodPerfilProduct = Generales.TipoPerfil.Product_Manager;
            string CodPerfilAnalista = Generales.TipoPerfil.Analista_FIN;

            UsuarioDtoRequest usuario_Admin = new UsuarioDtoRequest();
            UsuarioDtoRequest usuario_Product = new UsuarioDtoRequest();
            UsuarioDtoRequest usuario_Analista = new UsuarioDtoRequest();
            List<UsuarioDtoResponse> obj = new List<UsuarioDtoResponse>();
            usuario_Admin.CodigoPerfil = CodPerfilAdmin;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(request));



            var ListPerfilAdmin = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            var ListPerfilAdmin_2 = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            usuario_Admin.CodigoPerfil = CodPerfilProduct;
            var ListPerfilProduct_Manager = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));
            usuario_Admin.CodigoPerfil = CodPerfilAnalista;
            var ListPerfilAnalista = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));


            var idCreacion = resultado.IdUsuarioCreacion;
            var idProduct = resultado.IdProductManager;
            var idAnalista = resultado.IdAnalistaFinanciero;
            var idPreventa = resultado.IdPreVenta;
            if (resultado.IdUsuarioModifica != null)
            {
                var id = resultado.IdUsuarioModifica;

                obj = ListPerfilAdmin.Where(p => p.IdUsuario == id).ToList();
                resultado.NomCompletUsuMod = obj[0].NombresCompletos;
            }


            //ListPerfilAdmin = ListPerfilAdmin.Where<UsuarioDtoResponse>(p => p.IdUsuario == idCreacion).ToList<UsuarioDtoResponse>();
            //ListPerfilProduct_Manager = ListPerfilProduct_Manager.Where<UsuarioDtoResponse>(p => p.IdUsuario == idProduct).ToList<UsuarioDtoResponse>();
            //ListPerfilAnalista = ListPerfilAnalista.Where<UsuarioDtoResponse>(p => p.IdUsuario == idAnalista).ToList<UsuarioDtoResponse>();
            //ListPerfilAdmin_2 = ListPerfilAdmin_2.Where<UsuarioDtoResponse>(p => p.IdUsuario == idPreventa).ToList<UsuarioDtoResponse>();

            resultado.NomCompletUsuCrea = ListPerfilAdmin[0].NombresCompletos;
            resultado.NomCompletProductManager = ListPerfilProduct_Manager[0].NombresCompletos;
            resultado.NomCompletAnalistaFinanciero = ListPerfilAnalista[0].NombresCompletos;
            resultado.NomCompletPreventa = ListPerfilAdmin_2[0].NombresCompletos;

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GraficoOibda(ServicioConceptoProyectadoDtoRequest request)
        {
            ServicioConceptoProyectadoDtoResponse resul = new ServicioConceptoProyectadoDtoResponse();
            // var result = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.DatosPorVersion(request));
            resul.IdLineaNegocio = 5;
             LineaNegocioDtoRequest lineaRequest = new LineaNegocioDtoRequest();
            List<ProyectadoOibdaResponse> data = new List<ProyectadoOibdaResponse>();

            lineaRequest.IdEstado = 1;
            //var resultado =0;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(lineaRequest));
            //if (result.Count == 0)
            //{
                data.Add(new ProyectadoOibdaResponse()
                {
                    label = "Oibda",
                    value = Generales.Numeric.Uno,
                    Linea = "",
                    Color = Generales.ColorLinea.Linea1
                });
            //}
            //else
            //{
                string[] weekDays = {
                    Generales.ColorLinea.Linea1,
                    Generales.ColorLinea.Linea2,
                    Generales.ColorLinea.Linea3,
                    Generales.ColorLinea.Linea4,
                    Generales.ColorLinea.Linea5};
                var i = Generales.Numeric.Cero;
                //foreach (var partlinea in result)
                //{

                    var nombre = "Datos";


                    //if (resultado[i].Codigo == partlinea.IdLineaNegocio.ToString())
                    //{
                    //    nombre = resultado[i].Descripcion;

                    //}

                    //   var nombre = resultado.Where<LineaNegocioDtoResponse>(p => p.IdLineaNegocio == partlinea.IdLineaNegocio).ToList<LineaNegocioDtoResponse>();

                    //data.Add(new ProyectadoOibdaResponse()
                    //{
                    //    label = "Oibda",
                    //    value=0,
                    //    //value = partlinea.Monto,
                    //    Linea = nombre,
                    //    // Linea = nombre[0].Descripcion,   
                    //    Color = weekDays[5],
                    //   // Color = weekDays[partlinea.IdLineaNegocio - 1],

                    //});
                //}



            //}

            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult OportunidadGanador(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.OportunidadGanador(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult OportunidadInactivar(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.OportunidadInactivar(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarVersionOportunidad(OportunidadDtoRequest request)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarVersionOportunidad(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}