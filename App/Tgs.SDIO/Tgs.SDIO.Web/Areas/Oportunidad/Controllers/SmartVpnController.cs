﻿using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class SmartVpnController : Controller
    {
        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public SmartVpnController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        // GET: Oportunidad/SmartVpn
        public ActionResult Index(ConceptoDatosCapexDtoRequest dto)
        {
            return View();
        }

        public JsonResult CantidadSmartVpn(SubServicioDatosCapexDtoRequest dto)
        {
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.CantidadSolarWindVPN(dto));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}