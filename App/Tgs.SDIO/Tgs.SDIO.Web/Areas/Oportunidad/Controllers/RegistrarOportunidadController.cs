﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Oportunidad.Controllers
{
    public class RegistrarOportunidadController : Controller
    {
        // GET: Oportunidad/RegistrarOportunidad

        private readonly AgenteServicioOportunidadSDio agenteServicioOportinodadSDio = null;
        private readonly AgenteServicioSeguridadSDio agenteServicioSeguridadSDio = null;

        public RegistrarOportunidadController(AgenteServicioOportunidadSDio agenteServicioOportinodadSDio, AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            this.agenteServicioOportinodadSDio = agenteServicioOportinodadSDio;
            this.agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }



        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();

        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult Preventa()
        {


            var Preventa = SessionFacade.Usuario.FirstName;



            return Json(Preventa, JsonRequestBehavior.AllowGet);
        }
        


        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {

            var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadFlujoEstadoId(oportunidadFlujoEstado));



            return Json(estadoOportunidad, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidad(OportunidadDtoRequest oportunidadDtoRequest)
        {

            var estadoOportunidad = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidad(oportunidadDtoRequest));

            return Json(estadoOportunidad, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarUsuariosPorPerfil(UsuarioDtoRequest usuario_Admin)
        {

            string CodSistema = "SDIO";
            int IdEmpresa = 1;
            usuario_Admin.CodigoSistema = CodSistema;
            usuario_Admin.IdEmpresa = IdEmpresa;
            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuariosPorPerfil(usuario_Admin));


        
            List<ListaDtoResponse> Lista = new List<ListaDtoResponse>();

            foreach (var part in resultado)
            {
                Lista.Add(new ListaDtoResponse()
                {
                    Codigo = part.IdUsuario.ToString(),
                    Descripcion = part.NombresCompletos
                });

            }


            return Json(Lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadFlujoEstado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarOportunidad(OportunidadDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;
            request.IdPreVenta = SessionFacade.Usuario.UserId;


            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());
            request.FechaCreacion = fecha;
            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidad(OportunidadDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidad(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult CargarPerfil(UsuarioDtoRequest usuario)

        {
            var id = 1;

            var resultado = agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(usuario.IdEmpresa));



            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest request)
        {
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;


            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.RegistrarOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest request)
        {
            request.IdUsuarioEdicion = SessionFacade.Usuario.UserId;


            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ObtenerOportunidadLineaNegocio(OportunidadLineaNegocioDtoRequest request)
        {

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ObtenerOportunidadLineaNegocio(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest request)
        {

            var resultado = agenteServicioOportinodadSDio.InvocarFuncionAsync(o => o.ActualizarOportunidadFlujoEstado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
  


    }
}