﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class ConceptoSeguimientoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public ConceptoSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        [HttpPost]
        public JsonResult ListarComboConceptoSeguimiento()
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListarComboConceptoSeguimiento());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}