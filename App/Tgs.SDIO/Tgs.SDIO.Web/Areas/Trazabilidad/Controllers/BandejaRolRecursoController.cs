﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaRolRecursoController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public BandejaRolRecursoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/RolRecursoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarRolRecurso(RolRecursoDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListadoRolRecursosPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarRolRecurso(RolRecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarRolRecurso(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);


        }
    }
}