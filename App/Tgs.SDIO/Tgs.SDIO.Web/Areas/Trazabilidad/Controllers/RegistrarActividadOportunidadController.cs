﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarActividadOportunidadController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarActividadOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/RegistrarActividadOportunidad
        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbDatoActividadOportunidad = "";
            }
            else
            {
                var actividad = new ActividadOportunidadDtoRequest { IdActividad = id };
                var DatoActividadOportunidad = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerActividadOportunidadPorId(actividad));
                ViewBag.vbDatoActividadOportunidad = DatoActividadOportunidad;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {
            if (request.IdActividad != 0)
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarActividadOportunidad(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarActividadOportunidad(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }


    }
}