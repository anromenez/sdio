﻿using System.Web.Mvc;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaRecursoController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public BandejaRecursoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/RecursoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarRecurso(RecursoDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListadoRecursosPaginado(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarRecurso(RecursoDtoRequest request)
        {
            request.IdUsuario = SessionFacade.Usuario.UserId;
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarRecurso(request));

            return Json(resultado, JsonRequestBehavior.AllowGet);

        }

    }
}