﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RecursoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public RecursoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }

        [HttpPost]
        public JsonResult ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListaRecursoModalRecursoPaginado(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObtenerRecursoPorId(RecursoDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerRecursoPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        
    }
}