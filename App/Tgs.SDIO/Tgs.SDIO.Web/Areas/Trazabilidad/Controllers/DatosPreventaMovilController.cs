﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class DatosPreventaMovilController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public DatosPreventaMovilController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }

        [HttpPost]
        public JsonResult ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerDatosPreventaMovilPorId(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}