﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarObservacionOportunidadController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarObservacionOportunidadController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/RecursoRegistrar
        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbDatoAreaSeguimiento = "";
            }
            else
            {
                var request = new ObservacionOportunidadDtoRequest { IdObservacion = id };
                var DatoObservacionOportunidad = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerObservacionOportunidadPorId(request));
                ViewBag.vbDatoObservacionOportunidad = DatoObservacionOportunidad;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            if (request.IdObservacion != 0)
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarObservacionOportunidad(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarObservacionOportunidad(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
    }
}