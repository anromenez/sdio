﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarConceptoSeguimientoController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarConceptoSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/ConceptoSeguimientoRegistrar
        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbDatoAreaSeguimiento = "";
            }
            else
            {
                var request = new ConceptoSeguimientoDtoRequest { IdConcepto = id };
                var DatoConceptoSeguimiento = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerConceptoSeguimientoPorId(request));
                ViewBag.vbDatoConceptoSeguimiento = DatoConceptoSeguimiento;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            if (request.IdConcepto != 0)
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarConceptoSeguimiento(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarConceptoSeguimiento(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
    }
}