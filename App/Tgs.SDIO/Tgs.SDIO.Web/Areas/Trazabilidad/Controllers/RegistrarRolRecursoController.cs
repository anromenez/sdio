﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarRolRecursoController : Controller
    {

        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarRolRecursoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/RolRecursoRegistrar
        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbDatoRolRecurso = "";
            }
            else
            {
                var area = new RolRecursoDtoRequest { IdRol = id };
                var DatoRolRecurso = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerRolRecursoPorId(area));
                ViewBag.vbDatoRolRecurso = DatoRolRecurso;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarRolRecurso(RolRecursoDtoRequest request)
        {
            if (request.IdRol != 0)
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarRolRecurso(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                request.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarRolRecurso(request));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
    }
}