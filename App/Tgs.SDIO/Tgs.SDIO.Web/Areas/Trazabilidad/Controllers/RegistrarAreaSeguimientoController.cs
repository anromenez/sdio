﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class RegistrarAreaSeguimientoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;

        public RegistrarAreaSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        //GET: Trazabilidad/AreaSeguimientoRegistrar
        [CustomAuthorize]
        public ActionResult Index(int id)
        {
            if (id == 0)
            {
                ViewBag.vbDatoAreaSeguimiento = "";
            }
            else
            {
                var area = new AreaSeguimientoDtoRequest { IdAreaSeguimiento = id };
                var DatoAreaSeguimiento = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ObtenerAreaSeguimientoPorId(area));
                ViewBag.vbDatoAreaSeguimiento = DatoAreaSeguimiento;
            }
            return View();
        }
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest area)
        {
            if (area.IdAreaSeguimiento != 0)
            {
                area.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ActualizarAreaSeguimiento(area));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                area.IdUsuario = SessionFacade.Usuario.UserId;
                var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.RegistrarAreaSeguimiento(area));

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
    }
}