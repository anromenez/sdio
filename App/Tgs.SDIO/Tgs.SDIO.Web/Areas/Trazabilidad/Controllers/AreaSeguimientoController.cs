﻿using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class AreaSeguimientoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public AreaSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        [HttpPost]
        public JsonResult ListarComboAreas()
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListarComboAreaSeguimiento());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}