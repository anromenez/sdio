﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.UI.AgenteServicio;


namespace Tgs.SDIO.Web.Areas.Trazabilidad.Controllers
{
    public class BandejaAreaSeguimientoController : Controller
    {
        private readonly AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio = null;
        public BandejaAreaSeguimientoController(AgenteServicioTrazabilidadSDio agenteServicioTrazabilidadSDio)
        {
            this.agenteServicioTrazabilidadSDio = agenteServicioTrazabilidadSDio;
        }
        // GET: Trazabilidad/AreaSeguimientoBandeja
        [CustomAuthorize]
        [NoCache]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarAreaSeguimiento(AreaSeguimientoDtoRequest areaRequest)
        {
            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.ListadoAreasSeguimientoPaginado(areaRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult EliminarAreaSeguimiento(AreaSeguimientoDtoRequest areaRequest)
        {
            areaRequest.IdUsuario = SessionFacade.Usuario.UserId;

            var resultado = agenteServicioTrazabilidadSDio.InvocarFuncionAsync(o => o.EliminarAreaSeguimiento(areaRequest));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}