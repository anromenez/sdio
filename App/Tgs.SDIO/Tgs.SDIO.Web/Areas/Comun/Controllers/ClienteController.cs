﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ClienteController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ClienteController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Cliente
        public ActionResult Index()
        {
            return View();
        }


        //Cliente
        public JsonResult ListarClientePorDescripcion(ClienteDtoRequest request)
        {
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCliente(request));
            var resultado = result.Where(m => m.IdEstado == request.IdEstado && m.Descripcion.ToLower().Contains(request.Descripcion.ToLower())).ToList();
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarCliente(ClienteDtoRequest request)
        {
            var result = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarCliente(request));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerCliente(ClienteDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerCliente(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


    }
}