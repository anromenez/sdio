﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class ProveedorController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public ProveedorController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Proveedor
        public ActionResult Index()
        {
            return View();
        }

        // Proveedor
        public JsonResult ListarProveedor(ProveedorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActualizarProveedor(ProveedorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ActualizarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ObtenerProveedor(ProveedorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RegistrarProveedor(ProveedorDtoRequest request)
        {
            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());


            request.FechaCreacion = fecha;
            request.IdUsuarioCreacion = SessionFacade.Usuario.UserId;


            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.RegistrarProveedor(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}