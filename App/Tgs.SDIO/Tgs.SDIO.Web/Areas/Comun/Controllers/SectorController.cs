﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class SectorController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public SectorController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Sector
        public ActionResult Index()
        {
            return View();
        }

        //Sector
        public JsonResult ObtenerSector(SectorDtoRequest request)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ObtenerSector(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //Sector
        public JsonResult ListarSector(SectorDtoRequest sector)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarSector(sector));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarComboSector()
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarComboSector());
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}