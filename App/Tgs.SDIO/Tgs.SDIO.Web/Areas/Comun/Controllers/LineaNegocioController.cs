﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class LineaNegocioController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public LineaNegocioController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/LineaNegocio
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarLineaNegocios(LineaNegocioDtoRequest linea)
        {
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarLineaNegocio(linea));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}