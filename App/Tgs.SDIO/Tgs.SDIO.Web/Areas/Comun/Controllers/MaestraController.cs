﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using static Tgs.SDIO.Util.Web.Comun.Constantes.General;

namespace Tgs.SDIO.Web.Areas.Comun.Controllers
{
    public class MaestraController : Controller
    {
        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public MaestraController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }

        // GET: Comun/Maestra
        public ActionResult Index()
        {
            return View();
        }

        //Maestra
        public JsonResult ListarMaestraPorIdRelacion(MaestraDtoRequest request)
        {
            request.IdEstado = Estados.Activo;
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.ListarMaestraPorIdRelacion(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}