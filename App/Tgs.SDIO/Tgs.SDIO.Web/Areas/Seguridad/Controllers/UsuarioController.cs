﻿using System;
using System.Linq;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using System.Collections.Generic;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    [RepErrorCatch]
    public class UsuarioController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio _agenteServicioSeguridadSDio = null;
        public UsuarioController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            _agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            UsuarioDtoRequest usuario = new UsuarioDtoRequest();

            //usuario.piIdUsuarioSistemaEmpresa = 2;
            
            //var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.CambiarPassword(usuario));
            //ViewBag.vbusuarioLoginRais = usuarioLoginRais;

            return View();
        }
        
        [HttpGet]
        [AllowAnonymous]
        [CustomAuthorize]
        public ActionResult ListarUsuarios()
        {
            UsuarioDtoResponse usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarUsuarios(new UsuarioDtoRequest()));

            return Json(usuarioLoginRais.UsuarioDtoResponseLista, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Ingresartest()
        {
            var resultado = string.Empty;
            var usuario = new UsuarioDtoRequest();

            var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ValidarUsuario(usuario));

            return Content("1");
        }

        //public ActionResult ModalEstudiosEspeciales(ConceptoDatosCapexDtoRequest dto)
        //{
        //    var resultado = Url.Action("Index", "EstudiosEspeciales", dto);
        //    return Json(new { Respuesta = new { Mensaje = resultado } });
        //}

        public ActionResult CambioClave()
        {
            return View();
        }

        public ActionResult OlvidoClave()
        {
            return View();
        }

        public ActionResult EnviarClave(UsuarioDtoRequest usuario)
        {
            var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.EnvioClaveUsuario(usuario));

            return Json(new { Respuesta = new { Mensaje = usuarioLoginRais } });
        }

        public ActionResult CambiarClave(UsuarioDtoRequest usuario)
        {
            var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.CambiarPassword(usuario));

            return Json(new { Respuesta = new { Error = usuarioLoginRais.CodigoError, Mensaje = usuarioLoginRais.MensajeError } });
        }

        public ActionResult CerrarSesion()
        {
            SessionFacade.EliminarSesion();
            Session.RemoveAll();

            return RedirectToAction("Index", "Login", new { area = "Seguridad" });
        }
      
    }
}