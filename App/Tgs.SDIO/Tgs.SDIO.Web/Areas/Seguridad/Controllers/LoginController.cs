﻿using System;
using System.Linq;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    [RepErrorCatch]
    public class LoginController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio _agenteServicioSeguridadSDio = null;
        public LoginController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            _agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }
        
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        } 

        [HttpPost]
        [AllowAnonymous]
        [CustomAntiForgeryToken]
        public ActionResult Ingresar(UsuarioDtoRequest usuario)
        {
            var resultado = string.Empty;

            var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ValidarUsuario(usuario));

            if (!usuarioLoginRais.CodigoError.Equals(0))
            {
                resultado = usuarioLoginRais.MensajeError;
            }
            else
            {
                usuario.IdUsuarioSistema = usuarioLoginRais.Usuariosistema;
                usuario.Login = usuario.Login;
              
                var usuarioDato = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerUsuarioPerfil(usuario));

                SessionFacade.CrearSesion(usuarioDato);
             
                resultado = Url.Action("Modulo", "Principal").Replace("/Seguridad", string.Empty);
            }

            return Json(new { Respuesta = new { Error = usuarioLoginRais.CodigoError, Mensaje = resultado } });
        }

 

        public ActionResult CerrarSesion()
        {
            SessionFacade.EliminarSesion();
            Session.RemoveAll();

            return RedirectToAction("Index", "Login", new { area = "Seguridad" });
        }
         
    }
}