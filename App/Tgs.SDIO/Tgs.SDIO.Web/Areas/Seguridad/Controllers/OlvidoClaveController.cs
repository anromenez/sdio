﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class OlvidoClaveController : Controller
    {

        private readonly AgenteServicioSeguridadSDio _agenteServicioSeguridadSDio = null;
        public OlvidoClaveController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            _agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EnviarClave(UsuarioDtoRequest usuario)
        {
            var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.EnvioClaveUsuario(usuario));

            return Json(new { Respuesta = new { Mensaje = usuarioLoginRais } });
        }
    }
}