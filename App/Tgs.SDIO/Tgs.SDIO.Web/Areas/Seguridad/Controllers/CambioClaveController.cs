﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Seguridad.Controllers
{
    public class CambioClaveController : Controller
    {  
        private readonly AgenteServicioSeguridadSDio _agenteServicioSeguridadSDio = null;
        public CambioClaveController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            _agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CambiarClave(UsuarioDtoRequest usuario)
        {
            var usuarioLoginRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.CambiarPassword(usuario));

            return Json(new { Respuesta = new { Error = usuarioLoginRais.CodigoError, Mensaje = usuarioLoginRais.MensajeError } });
        }

    }
}