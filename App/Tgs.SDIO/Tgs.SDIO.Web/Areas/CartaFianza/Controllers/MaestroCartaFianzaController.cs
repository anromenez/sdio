﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class MaestroCartaFianzaController : Controller
    {
        private readonly AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio = null;
        // GET: CartaFianza/RegistroCartaFianza



        public MaestroCartaFianzaController(AgenteServicioCartaFianzaSDio agenteServicioCartaFianzaSDio)
        {
            this.agenteServicioCartaFianzaSDio = agenteServicioCartaFianzaSDio;
        }
        // GET: CartaFianza/CartaFianza
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ListarCartaFianza(CartaFianzaRequest cartaFianza)
        {
           // cartaFianza.IdCartaFianza = 1;
            var resultado = agenteServicioCartaFianzaSDio.InvocarFuncionAsync(o => o.ListaCartaFianza(cartaFianza));
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


  

     
        
    }
}
