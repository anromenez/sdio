﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.CartaFianza.Controllers
{
    public class AccioneCartaFianzaController : Controller
    {
        // GET: CartaFianza/AccioneCartaFianza
        public ActionResult Index()
        {
            return View();
        }

        // GET: CartaFianza/AccioneCartaFianza/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CartaFianza/AccioneCartaFianza/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CartaFianza/AccioneCartaFianza/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CartaFianza/AccioneCartaFianza/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CartaFianza/AccioneCartaFianza/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CartaFianza/AccioneCartaFianza/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CartaFianza/AccioneCartaFianza/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
