﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.UI.AgenteServicio;

namespace Tgs.SDIO.Web.Areas.Funnel.Controllers
{
    public class DashboardController : Controller
    {
        private readonly AgenteServicioFunnelSDio agenteServicioFunnelSDio = null;

        public DashboardController(AgenteServicioFunnelSDio agenteServicioFunnelSDio)
        {
            this.agenteServicioFunnelSDio = agenteServicioFunnelSDio;
        }

        // GET: Funnel/Dashoard
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult MostrarDashboard(DashoardDtoRequest request)
        {
            var resultado = agenteServicioFunnelSDio.InvocarFuncionAsync(x => x.MostrarDashboard(request));
            return Json(resultado, JsonRequestBehavior.AllowGet);

        }
    }
}

