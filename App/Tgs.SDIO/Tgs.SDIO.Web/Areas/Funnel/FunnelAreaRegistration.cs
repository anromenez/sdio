﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tgs.SDIO.Web.Areas.Funnel
{
    public class FunnelAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Funnel";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Funnel_default",
                "Funnel/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}