﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Tgs.SDIO.Util.Enumerables;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.Web.Reportes
{
    public partial class frmContenedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            var reporte = Request.Params["reporte"];

            if ((string.IsNullOrEmpty(reporte)))
            {
                return;
            }

            var rep = (EnumReportes.Reportes)Enum.Parse(typeof(EnumReportes.Reportes), reporte);
            var reportServerUrl = ConfigurationManager.AppSettings["ReportServer"];

            switch (rep)
            { 
                case EnumReportes.Reportes.Prueba:
                    Pruebas(reportServerUrl);
                    break;
                case EnumReportes.Reportes.MadurezPorMes:
                    MadurezPorMes(reportServerUrl);
                    break;
                case EnumReportes.Reportes.ProbabilidadPorMesPopup:
                    ProbabilidadPorMesPopup(reportServerUrl);
                    break;
                case EnumReportes.Reportes.LineaNegocioPeriodo:
                    LineaNegocioPeriodo(reportServerUrl);
                    break;
                case EnumReportes.Reportes.OfertasPorSector:
                    OfertasPorSector(reportServerUrl);
                    break;

                case EnumReportes.Reportes.IngresoLineaNegocioPeriodo:
                    IngresoLineaNegocioPeriodo(reportServerUrl);
                    break;

                case EnumReportes.Reportes.IngresoSectorPeriodo:
                    IngresoSectorPeriodo(reportServerUrl);
                    break;

                case EnumReportes.Reportes.DashboardPrincipal:
                    ProbabilidadPorMes(reportServerUrl);
                    break;
               
                case EnumReportes.Reportes.OfertaPorEstado:
                    OfertaPorEstadoPeriodo(reportServerUrl);
                    break;
                case EnumReportes.Reportes.EvolutivoOportunidad:
                    EvolutivoOportunidad(reportServerUrl);
                    break;
                case EnumReportes.Reportes.OfertasPorCliente:
                    OfertasPorCliente(reportServerUrl);
                    break;
            }
        }


        private void Pruebas(string reportServerUrl)
        { 
            string fechaDerecho = Request.Params["FechaDerecho"].Trim();
            string fechaRol = Request.Params["FechaRol"].Trim();
            string periodo = Request.Params["Periodo"].Trim();
            string rovReprog = Request.Params["RovReprog"].Trim();
            string diasGozado = Request.Params["DiasGozado"].Trim();
            string diasPendienteGoze = Request.Params["DiasPendienteGoze"].Trim(); 

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/TGS.SGV/ConsultaProgramaciones";

            var parametros = new Dictionary<string, string>
            { 
                {"P_FECHADERECHO", fechaDerecho},
                {"P_FECHAROL", fechaRol},
                {"P_PERIODO", periodo},
                {"P_ROVREPROG", rovReprog}
            };
            
            AgregarParametros(parametros);
        }


        private void MadurezPorMes(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var lineaNegocio = Request.Params["LineaNegocio"].Trim() == "0" ? null : Request.Params["LineaNegocio"].Trim();
            var sector = Request.Params["Sector"].Trim() == "0" ? null : Request.Params["Sector"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var urlReporte = Request.Params["urlReporte"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptMadurez";
            rvVisor.Height = 770;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"IdLineaNegocio", lineaNegocio},
                {"IdSector", sector},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"UrlProbabilidadPopup", urlReporte},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdMaestra_Etapa", Generales.TablaMaestra.Etapas.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }

        private void ProbabilidadPorMesPopup(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptProbabilidadPorMesPopup";
            rvVisor.ShowToolBar = false;

            rvVisor.Height = 850;
            rvVisor.Width = 850;
            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void LineaNegocioPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();
            

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptLineaNegocioPeriodo";
            rvVisor.Height = 450;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdMaestra_Etapa", Generales.TablaMaestra.Etapas.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }


        private void OfertasPorSector(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptSectorPeriodo";
            rvVisor.Height = 450;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdMaestra_Etapa", Generales.TablaMaestra.Etapas.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }

        private void IngresoLineaNegocioPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptIngresosPorLineaNegocio";
            rvVisor.Height = 550;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez}
            };

            AgregarParametros(parametros);
        }


        private void IngresoSectorPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var etapa = Request.Params["Etapa"].Trim() == "0" ? null : Request.Params["Etapa"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptIngresoSectorPorNegocio";
            rvVisor.Height = 600;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"ProbabilidadExito", probabilidad},
                {"Etapa", etapa},
                {"Madurez", madurez}
            };

            AgregarParametros(parametros);
        }

        private void ProbabilidadPorMes(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? "-1" : Request.Params["Mes"].Trim();
            var urlReporte = Request.Params["urlReporte"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptPrincipal";

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"UrlProbabilidadPopup", urlReporte}
            };

            AgregarParametros(parametros);
        }
        
        private void OfertaPorEstadoPeriodo(string reportServerUrl)
        {
            var anio = Request.Params["Anio"].Trim();
            var mes = Request.Params["Mes"].Equals("0") ? null : Request.Params["Mes"].Trim();
            var lineaNegocio = Request.Params["IdLineaNegocio"].Trim() == "0" ? null : Request.Params["IdLineaNegocio"].Trim();
            var sector = Request.Params["Sector"].Trim() == "0" ? null : Request.Params["Sector"].Trim();
            var probabilidad = Request.Params["Probabilidad"].Trim() == "-1" ? null : Request.Params["Probabilidad"].Trim();
            var madurez = Request.Params["Madurez"].Trim() == "0" ? null : Request.Params["Madurez"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptOfertasEstado";
            rvVisor.Height = 550;

            var parametros = new Dictionary<string, string>
            {
                {"Anio", anio},
                {"Mes", mes},
                {"IdLineaNegocio", lineaNegocio},
                {"IdSector", sector},
                {"ProbabilidadExito", probabilidad},
                {"Madurez", madurez},
                {"IdMaestra_Madurez", Generales.TablaMaestra.Madurez.ToString()},
                {"IdEstado", Generales.Estados.Activo.ToString()}
            };

            AgregarParametros(parametros);
        }

        private void EvolutivoOportunidad(string reportServerUrl)
        {
            var idOportunidad = Request.Params["IdOportunidad"].Trim();

            rvVisor.ServerReport.ReportServerUrl = new Uri(reportServerUrl);
            rvVisor.ServerReport.ReportPath = "/SDIO/rptEvolutivoOportunidad";

            var parametros = new Dictionary<string, string>
            {
                {"IdOportunidad", idOportunidad}
            };

            AgregarParametros(parametros);
        }

        private void OfertasPorCliente(string reportServerUrl)
        {
            throw new NotImplementedException();
        }


        private void  AgregarParametros(Dictionary<string, string> parametros)
        {
            var paramList = parametros.Select(item => new ReportParameter(item.Key, item.Value)).ToList();

            rvVisor.ServerReport.SetParameters(paramList);
            rvVisor.ServerReport.Refresh();
        }        

    }
}
