﻿using System.Web;
using System.Web.Optimization;
using Tgs.SDIO.Web.App_Start.Bundles;

namespace Tgs.SDIO.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery/js").Include(
                      "~/Scripts/jquery/jquery-{version}.js",
                       "~/Scripts/jquery/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                   "~/Scripts/js/bootstrap.min.js",
                   "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr/js").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/Main/js").Include(
                 "~/Scripts/js/summernote.js",
                  "~/Scripts/js/jquery.auto-complete.js",  
                  "~/Scripts/js/bootstrap-filestyle.js",
                 "~/Scripts/js/waves.js",
                 "~/Scripts/js/metisMenu.min.js",
                 "~/Scripts/js/jquery.slimscroll.js",
                 "~/Scripts/js/jquery.app.js"
                  ));


            bundles.Add(new ScriptBundle("~/bundles/angular/js").Include(
                    "~/Scripts/angular/angular.js",
                    "~/Scripts/angular/angular-route.js",
                    "~/Scripts/angular/angular-animate.js",
                    "~/Scripts/angular/angular-block-ui.js",
                    "~/Scripts/angular/angular-sanitize.js",
                    "~/Scripts/angular/angular-summernote.min.js",
                    "~/Scripts/angular/angular-sanitize.js",
                    "~/Scripts/angular/angular-ui.mask.js",
                    "~/Scripts/Assets/angularjs-dropdown-multiselect/angularjs-dropdown-multiselect.js",
                    "~/Scripts/angular/angular-ui.mask.js" 
                  ));

            bundles.Add(new ScriptBundle("~/bundles/Assets/js").Include(
                  "~/Scripts/Assets/bootstrap-datepicker/moment.js",
                  "~/Scripts/Assets/bootstrap-datepicker/es.js",
                  "~/Scripts/Assets/bootstrap-datepicker/bootstrap-datetimepicker.js",
                  "~/Scripts/Assets/bootstrap-datatables/jquery.dataTables.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.util.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.options.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.instances.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.factory.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.renderer.js",
                  "~/Scripts/Assets/bootstrap-datatables/angular-datatables.directive.js",
                  "~/Scripts/Assets/bootstrap-datatables/dataTables.responsive.js",

                    //EAAR
                    "~/Scripts/angular/dataTables.fixedColumns.js",
                    "~/Scripts/angular/angular-datatables.fixedcolumns.min.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/app/base/js").Include(
                    "~/Scripts/app/Base/base.module.js",
                    "~/Scripts/app/Base/base.constantes.js",
                    "~/Scripts/app/Base/utils.factory.js",
                    "~/Scripts/app/Base/Controles/js/datepicker.directive.js",
                    "~/Scripts/app/Base/Controles/js/angular-strap.compat.js",
                    "~/Scripts/app/Base/Controles/js/angular-strap.js",
                    "~/Scripts/app/Base/Controles/js/angular-strap.tpl.js",
                    "~/Scripts/app/Base/Controles/js/alert.js",
                    "~/Scripts/app/Base/Controles/js/uploadFile.directive.js",
                    "~/Scripts/app/Base/Controles/js/tituloDatosObligatorios.directive.js",
                    "~/Scripts/app/Base/Controles/js/SelectorMeses.js",
                     "~/Scripts/app/Base/Controles/js/SelectorAnios.js",
                    "~/Scripts/app/Base/Controles/js/tooltip.js",
                    "~/Scripts/app/Base/Controles/js/parse-options.js",
                    "~/Scripts/app/Base/Controles/js/typeahead.js",
                     "~/Scripts/js/ui-bootstrap-tpls.js"
                    ));



            bundles.Add(new ScriptBundle("~/bundles/bootstrap/layout/js").Include(

                "~/Scripts/js/jquery-ui-1.10.1.custom.min.js",
                             "~/Scripts/js/bootstrap.min.js",
                            "~/Scripts/js/jquery.dcjqaccordion.2.7.js",
                            "~/Scripts/js/jquery.scrollTo.min.js",
                            "~/Scripts/js/jquery.slimscroll.js",
                            "~/Scripts/js/jquery.nicescroll.js",
                            "~/Scripts/js/skycons.js",
                            "~/Scripts/js/jquery.scrollTo.js",
                            "~/Scripts/js/clndr.js",
                            "~/Scripts/js/jquery.easing.min.js",
                            "~/Scripts/js/underscore-min.js",
                            "~/Scripts/js/moment-2.2.1.js",
                            "~/Scripts/js/evnt.calendar.init.js",
                            "~/Scripts/js/jquery-jvectormap-1.2.2.min.js",
                             "~/Scripts/js/jquery-jvectormap-us-lcc-en.js",
                             "~/Scripts/js/gauge.js",
                                "~/Scripts/js/css3clock.js",
                                "~/Scripts/js/jquery.easypiechart.js",
                                "~/Scripts/js/jquery.sparkline.js",
                                "~/Scripts/js/morris.js",
                                "~/Scripts/js/raphael-min.js",
                                "~/Scripts/js/jquery.flot.js",
                                "~/Scripts/js/jquery.flot.tooltip.min.js",
                                "~/Scripts/js/jquery.flot.resize.js",
                                "~/Scripts/js/jquery.flot.pie.resize.js",
                                "~/Scripts/js/jquery.flot.pie.js",
                                "~/Scripts/js/jquery.flot.animator.min.js",
                                    "~/Scripts/js/jquery.flot.growraf.js",
                                    "~/Scripts/js/jquery.customSelect.min.js",
                                    "~/Scripts/js/scripts.js"
                                ));

            bundles.Add(new StyleBundle("~/bundles/bootstrap/Master/css").Include(
                   "~/Content/plantilla/bs3/css/bootstrap.min.css",
                    "~/Content/plantilla/css/bootstrap-reset.css",
                     "~/Content/plantilla/font-awesome/css/font-awesome.css",
                     "~/Content/plantilla/js/morris-chart/morris.css",
                     "~/Content/plantilla/css/style.css",
                      "~/Content/plantilla/css/style-responsive.css",
                      "~/Content/plantilla/css/table-responsive.css",
                      "~/Content/plantilla/js/css3clock/css/style.css"


                   ));

   


                         bundles.Add(new StyleBundle("~/bundles/Assets/css").Include(
                   "~/Content/Assets/bootstrap-datepicker/bootstrap-datetimepicker.css",
                  "~/Content/Assets/bootstrap-datatables/jquery.dataTables.css",
                  "~/Content/Assets/bootstrap-datatables/angular-datatables.css",
                  "~/Content/Assets/bootstrap-datatables/responsive.dataTables.css",
                  "~/Content/angular-block-ui.min.css" 
                  ));

            bundles.Add(new StyleBundle("~/bundles/login/css").Include(

                     "~/Content/login/css/animate.css",
                     "~/Content/login/css/bootstrap.min.css",
                     "~/Content/login/css/fontawesome-all.min.css",
                     "~/Content/login/css/iofrm-style.css",
                     "~/Content/login/css/iofrm-theme1.css",
                     "~/Content/angular-block-ui.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/login/js").Include(
             "~/Content/login/js/jquery.min.js",
             "~/Content/login/js/bootstrap.min.js",
              "~/Content/login/js/main.js",
              "~/Content/login/js/popper.min.js"
             ));


                bundles.Add(new ScriptBundle("~/bundles/app/seguridad/perfiles/js").Include(
                 "~/Scripts/app/Seguridad/Login.module.js",
                 "~/Scripts/app/Seguridad/Perfil.controller.js",
                  "~/Scripts/app/Seguridad/Login.service.js"
                 ));


            CartaFianzaBundleConfig.RegisterCartaFianzaBundleConfig(bundles);

            ComunBundleConfig.RegisterComunBundleConfig(bundles);

            FunnelBundleConfig.RegisterFunnelBundleConfig(bundles);

            SeguridadBundleConfig.RegisterSeguridadBundleConfig(bundles);

            OportunidadBundleConfig.RegisterOportunidadBundleConfig(bundles);

            TrazabilidadBundleConfig.RegisterTrazabilidadBundleConfig(bundles);
        
            BundleTable.EnableOptimizations = false;
        }
    }

    public class CssRewriteUrlTransformWrapper : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
        }
    }
}