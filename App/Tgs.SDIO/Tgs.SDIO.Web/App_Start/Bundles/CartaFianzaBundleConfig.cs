﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class CartaFianzaBundleConfig
    {


        public static void RegisterCartaFianzaBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/MaestroCartaFianza/js").Include(
             "~/Scripts/app/CartaFianza/CartaFianza.module.js",
             "~/Scripts/app/Comun/Comun.module.js",
             "~/Scripts/app/Comun/Comun.service.js",
             "~/Scripts/app/Comun/Maestra/Maestra.service.js",
             "~/Scripts/app/Comun/Cliente/Cliente.service.js",
             "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/MaestroCartaFianza/MaestroCartaFianza.service.js",
             "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.service.js",
             "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.controller.js",
             "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.service.js"

         ));

            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/RegistroCartaFianza/js").Include(
            "~/Scripts/app/CartaFianza/CartaFianza.module.js",
            "~/Scripts/app/Comun/Comun.module.js",
            "~/Scripts/app/Comun/Comun.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.controller.js",
            "~/Scripts/app/CartaFianza/RegistroCartaFianza/RegistroCartaFianza.service.js"
        ));


            bundles.Add(new ScriptBundle("~/bundles/app/CartaFianza/ClienteCartaFianza/js").Include(
           "~/Scripts/app/CartaFianza/CartaFianza.module.js",
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/Comun.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.controller.js",
           "~/Scripts/app/CartaFianza/ClienteCartaFianza/ClienteCartaFianza.service.js"

       ));


        }
    }
}