﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class TrazabilidadBundleConfig
    {
        public static void RegisterTrazabilidadBundleConfig(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaAreaSeguimiento/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaAreaSeguimiento/BandejaAreaSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/BandejaAreaSeguimiento/BandejaAreaSeguimiento.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarAreaSeguimiento/js").Include(
            "~/Scripts/js/lodash.min.js",
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
            "~/Scripts/app/Trazabilidad/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarAreaSeguimiento/RegistrarAreaSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarAreaSeguimiento/RegistrarAreaSeguimiento.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaConceptoSeguimiento/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaConceptoSeguimiento/BandejaConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/BandejaConceptoSeguimiento/BandejaConceptoSeguimiento.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarConceptoSeguimiento/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
              "~/Scripts/app/Trazabilidad/RegistrarConceptoSeguimiento/RegistrarConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarConceptoSeguimiento/RegistrarConceptoSeguimiento.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaRecurso/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaRecurso/BandejaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/BandejaRecurso/BandejaRecurso.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarRecurso/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
              "~/Scripts/app/Trazabilidad/RegistrarRecurso/RegistrarRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecurso/RegistrarRecurso.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaRolRecurso/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaRolRecurso/BandejaRolRecurso.service.js",
             "~/Scripts/app/Trazabilidad/BandejaRolRecurso/BandejaRolRecurso.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarRolRecurso/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/RegistrarRolRecurso/RegistrarRolRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRolRecurso/RegistrarRolRecurso.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaActividadOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaActividadOportunidad/BandejaActividadOportunidad.service.js",
              "~/Scripts/app/Trazabilidad/EtapaOportunidad/EtapaOportunidad.service.js",
               "~/Scripts/app/Trazabilidad/AreaSeguimiento/AreaSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/BandejaActividadOportunidad/BandejaActividadOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarActividadOportunidad/js").Include(
            "~/Scripts/js/lodash.min.js",
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/EtapaOportunidad/EtapaOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/FaseOportunidad/FaseOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/AreaSeguimiento/AreaSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/ActividadOportunidad/ActividadOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarActividadOportunidad/RegistrarActividadOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarActividadOportunidad/RegistrarActividadOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/EtapaOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/EtapaOportunidad/EtapaOportunidad.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaObservacionOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/BandejaObservacionOportunidad/BandejaObservacionOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/BandejaObservacionOportunidad/BandejaObservacionOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarObservacionOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/ConceptoSeguimiento/ConceptoSeguimiento.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/CasoOportunidad/CasoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarObservacionOportunidad/RegistrarObservacionOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarObservacionOportunidad/RegistrarObservacionOportunidad.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaRecursoOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/BandejaRecursoOportunidad/BandejaRecursoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/BandejaRecursoOportunidad/BandejaRecursoOportunidad.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarRecursoOportunidad/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/RolRecurso/RolRecurso.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/Recurso/Recurso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidad/RegistrarRecursoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidad/RegistrarRecursoOportunidad.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarRecursoOportunidadMasivo/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/RolRecurso/RolRecurso.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/BandejaRecurso/BandejaRecurso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidadMasivo/RegistrarRecursoOportunidadMasivo.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarRecursoOportunidadMasivo/RegistrarRecursoOportunidadMasivo.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/BandejaDatosPreventaMovil/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/EstadoCaso/EstadoCaso.service.js",
             "~/Scripts/app/Trazabilidad/TipoOportunidad/TipoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/EtapaOportunidad/EtapaOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/OportunidadST/OportunidadST.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/BandejaDatosPreventaMovil/BandejaDatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/BandejaDatosPreventaMovil/BandejaDatosPreventaMovil.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/Trazabilidad/RegistrarDatosPreventaMovil/js").Include(
             "~/Scripts/app/Trazabilidad/Trazabilidad.module.js",
             "~/Scripts/app/Trazabilidad/TipoOportunidad/TipoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/EtapaOportunidad/EtapaOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/MotivoOportunidad/MotivoOportunidad.service.js",
             "~/Scripts/app/Trazabilidad/Sector/Sector.service.js",
             "~/Scripts/app/Trazabilidad/FuncionPropietario/FuncionPropietario.service.js",
             "~/Scripts/app/Trazabilidad/SegmentosNegocios/SegmentosNegocios.service.js",
             "~/Scripts/app/Trazabilidad/EstadoCaso/EstadoCaso.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil.service.js",
             "~/Scripts/app/Trazabilidad/RegistrarDatosPreventaMovil/RegistrarDatosPreventaMovil.controller.js"
            ));
        }
    }
}