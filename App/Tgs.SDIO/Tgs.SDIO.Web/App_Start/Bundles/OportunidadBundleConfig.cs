﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public static class OportunidadBundleConfig
    {
        public static void RegisterOportunidadBundleConfig(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/app/Oportunidad/Bandeja/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/BandejaOportunidad/BandejaOportunidad.controller.js",
                "~/Scripts/app/Oportunidad/BandejaOportunidad/BandejaOportunidad.service.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/app/Oportunidad/BandejaServicio/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/BandejaServicio/BandejaServicio.controller.js",
              "~/Scripts/app/Oportunidad/BandejaServicio/BandejaServicio.service.js"
              ));


            bundles.Add(new ScriptBundle("~/bundles/app/Oportunidad/BandejaDocumento/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/BandejaDocumento/BandejaDocumento.controller.js",
                "~/Scripts/app/Oportunidad/BandejaDocumento/BandejaDocumento.service.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/ecapex/js").Include(
                 "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                 "~/Scripts/app/Oportunidad/Ecapex/Ecapex.controller.js",
                 "~/Scripts/app/Oportunidad/Ecapex/Ecapex.service.js",
                 "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.controller.js",
                 "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.service.js",
                 "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
                 "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
                 "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                 "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.service.js",
                 "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
                 "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js",

                "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.controller.js",
                "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.service.js",
                 "~/Scripts/app/Oportunidad/Gabinete/Gabinete.controller.js",
                "~/Scripts/app/Oportunidad/Gabinete/Gabinete.service.js",
                 "~/Scripts/app/Oportunidad/Ghz/Ghz.controller.js",
                "~/Scripts/app/Oportunidad/Ghz/Ghz.service.js",
                "~/Scripts/app/Oportunidad/Hardware/Hardware.controller.js",
                "~/Scripts/app/Oportunidad/Hardware/Hardware.service.js",
                "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
                "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
                "~/Scripts/app/Oportunidad/Modems/Modems.controller.js",
                "~/Scripts/app/Oportunidad/Modems/Modems.service.js",
                "~/Scripts/app/Oportunidad/Routers/Routers.controller.js",
                "~/Scripts/app/Oportunidad/Routers/Routers.service.js",
                "~/Scripts/app/Oportunidad/Satelitales/Satelitales.controller.js",
                "~/Scripts/app/Oportunidad/Satelitales/Satelitales.service.js",
                "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.controller.js",
                "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.service.js",
                "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.controller.js",
                "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.service.js",
                "~/Scripts/app/Oportunidad/Software/Software.controller.js",
                "~/Scripts/app/Oportunidad/Software/Software.service.js",
                "~/Scripts/app/Oportunidad/SolarWind/SolarWind.controller.js",
                "~/Scripts/app/Oportunidad/SolarWind/SolarWind.service.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.controller.js",
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/LineaConceptoCmi/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.service.js",
                "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.controller.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/estudiosEspeciales/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
                "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.controller.js",
                "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.service.js",
                "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
                "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
                "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
                "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.controller.js",
                "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js"
                ).Include("~/bundles/app/oportunidad/LineaConceptoCmi/js"));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/equiposEspeciales/js").Include(
                "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
               "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/equiposeguridad/js").Include(
               "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/EquipoSeguridad.controller.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/gabinete/js").Include(
               "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/Gabinete.controller.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/ghz/js").Include(
               "~/Scripts/app/Oportunidad/Oportunidad.module.js",
               "~/Scripts/app/Oportunidad/Ghz.controller.js",
                "~/Scripts/app/Oportunidad/Oportunidad.service.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/hardware/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/Hardware.controller.js",
               "~/Scripts/app/Oportunidad/Oportunidad.service.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/modems/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/Modems.controller.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/routers/js").Include(
              "~/Scripts/app/Oportunidad/Oportunidad.module.js",
              "~/Scripts/app/Oportunidad/Routers.controller.js",
               "~/Scripts/app/Oportunidad/Routers.service.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/satelitales/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/Satelitales.controller.js",
              "~/Scripts/app/Oportunidad/Oportunidad.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/smartvpn/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/SmartVpn.controller.js",
              "~/Scripts/app/Oportunidad/Oportunidad.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/software/js").Include(
           "~/Scripts/app/Oportunidad/Oportunidad.module.js",
           "~/Scripts/app/Oportunidad/Software.controller.js",
            "~/Scripts/app/Oportunidad/Oportunidad.service.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/solarwind/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/SolarWind.controller.js",
             "~/Scripts/app/Oportunidad/Oportunidad.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/bandejaproyecto/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/BandejaProyecto/BandejaProyecto.controller.js",
             "~/Scripts/app/Oportunidad/BandejaProyecto/BandejaProyecto.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/caratula/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/Caratula/Caratula.controller.js",
             "~/Scripts/app/Oportunidad/Caratula/Caratula.service.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.controller.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.service.js",
             "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
             "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
             "~/Scripts/app/Oportunidad/OportunidadServicioCMIService/OportunidadServicioCMIService.controller.js",
             "~/Scripts/app/Oportunidad/OportunidadServicioCMIService/OportunidadServicioCMIService.service.js",
             "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.controller.js",
             "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.service.js",
             "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
             "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
             "~/Scripts/app/Comun/Medio/Medio.service.js",
             "~/Scripts/app/Comun/Maestra/Maestra.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/circuito/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.controller.js",
             "~/Scripts/app/Oportunidad/Circuito/Circuito.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/SOPEX/js").Include(
             "~/Scripts/app/Oportunidad/Oportunidad.module.js",
             "~/Scripts/app/Oportunidad/SeguridadOPEX/SeguridadOPEX.controller.js",
             "~/Scripts/app/Oportunidad/SeguridadOPEX/SeguridadOPEX.service.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/flujocaja/js").Include(
            "~/Scripts/app/Oportunidad/Oportunidad.module.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/oportunidad/contenedor/js").Include(
            "~/Scripts/app/Oportunidad/Oportunidad.module.js",
            "~/Scripts/app/Comun/Comun.module.js",
            "~/Scripts/app/Comun/Comun.service.js",
            "~/Scripts/app/Comun/Cliente/Cliente.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/Comun/Sector/Sector.service.js",
            "~/Scripts/app/Oportunidad/LineaConceptoCmi/LineaConceptoCmi.service.js",
            "~/Scripts/app/Comun/DireccionComercial/DireccionComercial.service.js",
            "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
            "~/Scripts/app/Oportunidad/OportunidadContenedor/OportunidadContenedor.controller.js",
            "~/Scripts/app/Oportunidad/OportunidadContenedor/OportunidadContenedor.service.js",
            //Caratula
            "~/Scripts/app/Oportunidad/Caratula/Caratula.controller.js",
            "~/Scripts/app/Oportunidad/Caratula/Caratula.service.js",
            "~/Scripts/app/Oportunidad/Circuito/Circuito.controller.js",
            "~/Scripts/app/Oportunidad/Circuito/Circuito.service.js",
            "~/Scripts/app/Oportunidad/SeguridadOPEX/SOPEX.controller.js",
            "~/Scripts/app/Oportunidad/SeguridadOPEX/SOPEX.service.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
            "~/Scripts/app/Oportunidad/OportunidadServicioCMI/OportunidadServicioCMI.controller.js",
            "~/Scripts/app/Oportunidad/OportunidadServicioCMI/OportunidadServicioCMI.service.js",
            "~/Scripts/app/Oportunidad/OportunidadServicioCMI/OportunidadServicioCMI.service.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.controller.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOPEX.service.js",
            "~/Scripts/app/Oportunidad/Opex/OPEX.controller.js",
            "~/Scripts/app/Oportunidad/Opex/OPEX.service.js",
            "~/Scripts/app/Oportunidad/RouterSonda/RouterSonda.controller.js",
            "~/Scripts/app/Oportunidad/RouterSonda/RouterSonda.service.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
            "~/Scripts/app/Oportunidad/SubServicioDatos/SubServicioDatos.service.js",
            "~/Scripts/app/Comun/Medio/Medio.service.js",
            //
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js",
            "~/Scripts/app/Oportunidad/Routers/Routers.service.js",
            "~/Scripts/app/Oportunidad/Ecapex/Ecapex.controller.js",
            "~/Scripts/app/Oportunidad/Ecapex/Ecapex.service.js",
            "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.controller.js",
            "~/Scripts/app/Oportunidad/EstudiosEspeciales/EstudiosEspeciales.service.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.controller.js",
            "~/Scripts/app/Oportunidad/FlujoCaja/FlujoCaja.service.js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.service.js",
            "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.controller.js",
            "~/Scripts/app/Oportunidad/EquiposEspeciales/EquiposEspeciales.service.js",
            "~/Scripts/app/Comun/CasoNegocio/CasoNegocio.service.js",

            "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.controller.js",
            "~/Scripts/app/Oportunidad/EquipoSeguridad/EquipoSeguridad.service.js",
             "~/Scripts/app/Oportunidad/Gabinete/Gabinete.controller.js",
            "~/Scripts/app/Oportunidad/Gabinete/Gabinete.service.js",
             "~/Scripts/app/Oportunidad/Ghz/Ghz.controller.js",
            "~/Scripts/app/Oportunidad/Ghz/Ghz.service.js",
            "~/Scripts/app/Oportunidad/Hardware/Hardware.controller.js",
            "~/Scripts/app/Oportunidad/Hardware/Hardware.service.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.controller.js",
            "~/Scripts/app/Oportunidad/Medio3G/Medio3G.service.js",
            "~/Scripts/app/Oportunidad/Modems/Modems.controller.js",
            "~/Scripts/app/Oportunidad/Modems/Modems.service.js",
            "~/Scripts/app/Oportunidad/Routers/Routers.controller.js",
            "~/Scripts/app/Oportunidad/Routers/Routers.service.js",
            "~/Scripts/app/Oportunidad/Satelitales/Satelitales.controller.js",
            "~/Scripts/app/Oportunidad/Satelitales/Satelitales.service.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.controller.js",
            "~/Scripts/app/Oportunidad/SeguridadOpex/SeguridadOpex.service.js",
            "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.controller.js",
            "~/Scripts/app/Oportunidad/SmartVpn/SmartVpn.service.js",
            "~/Scripts/app/Oportunidad/Software/Software.controller.js",
            "~/Scripts/app/Oportunidad/Software/Software.service.js",
            "~/Scripts/app/Oportunidad/SolarWind/SolarWind.controller.js",
            "~/Scripts/app/Oportunidad/SolarWind/SolarWind.service.js",
            "~/bundles/app/comun/maestra/js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.controller.js",
            "~/Scripts/app/Oportunidad/RegistrarOportunidad/RegistrarOportunidad.service.js",
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.controller.js",
            "~/Scripts/app/Oportunidad/SubServicioDatosCapex/SubServicioDatosCapex.service.js"
          ));


        }
    }
}