﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class ComunBundleConfig
    {
        public static void RegisterComunBundleConfig(BundleCollection bundles)
        {




            bundles.Add(new ScriptBundle("~/bundles/app/comun/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/Comun.service.js"
            ));

            //Servicio

            bundles.Add(new ScriptBundle("~/bundles/app/comun/BandejaServicio/js").Include(
           "~/Scripts/app/Comun/Medio/Medio.service.js",
           "~/Scripts/app/Comun/Cliente/Cliente.service.js",
           "~/Scripts/app/Comun/Depreciacion/Depreciacion.service.js",
           "~/Scripts/app/Comun/DireccionComercial/DireccionComercial.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",
           "~/Scripts/app/Comun/Proveedor/Proveedor.service.js",
           "~/Scripts/app/Comun/Servicio/Servicio.service.js",
           "~/Scripts/app/Comun/ServicioCMI/ServicioCMI.service.js",
           "~/Scripts/app/Comun/SubServicio/SubServicio.service.js",
           "~/Scripts/app/Comun/ServicioSubServicio/ServicioSubServicio.service.js",
           "~/Scripts/app/Comun/BandejaServicio/BandejaServicio.controller.js",
           "~/Scripts/app/Comun/BandejaServicio/BandejaServicio.service.js"
           ));
                    bundles.Add(new ScriptBundle("~/bundles/app/comun/BandejaSubServicio/js").Include(
            "~/Scripts/app/Comun/Comun/Depreciacion.service.js",
            "~/Scripts/app/Comun/Depreciacion/Depreciacion.service.js",
            "~/Scripts/app/Comun/Maestra/Maestra.service.js",
            "~/Scripts/app/Comun/SubServicio/SubServicio.service.js",
            "~/Scripts/app/Comun/Depreciacion/Depreciacion.service.js",
            "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.service.js",
            "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.controller.js",
            "~/Scripts/app/Comun/BandejaSubServicio/BandejaSubServicio.controller.js",
            "~/Scripts/app/Comun/BandejaSubServicio/BandejaSubServicio.service.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/app/comun/RegistrarSubServicio/js").Include(

        "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.controller.js",
        "~/Scripts/app/Comun/RegistrarSubServicio/RegistrarSubServicio.service.js"
        ));


            //
            bundles.Add(new ScriptBundle("~/bundles/app/comun/casonegocio/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/CasoNegocio/CasoNegocio.controller.js",
               "~/Scripts/app/Comun/CasoNegocio/CasoNegocio.service.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.controller.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.service.js",
               "~/Scripts/app/Comun/Servicio/Servicio.service.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Comun/CasoNegocioServicio/CasoNegocioServicio.service.js"
           ));



            bundles.Add(new ScriptBundle("~/bundles/app/comun/registrocasonegocio/js").Include(
               "~/Scripts/app/Comun/Comun.module.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.controller.js",
               "~/Scripts/app/Comun/RegistrarCasoNegocio/RegistrarCasoNegocio.service.js",
               "~/Scripts/app/Comun/Servicio/Servicio.service.js",
               "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
               "~/Scripts/app/Comun/CasoNegocioServicio/CasoNegocioServicio.service.js"
           ));





       

        }
    }
}