﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class SeguridadBundleConfig
    {
        public static void RegisterSeguridadBundleConfig(BundleCollection bundles)
        {
             
            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/login/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/Login/Login.controller.js",
                         "~/Scripts/app/Seguridad/Login/Login.service.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/cambioclave/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/CambioClave/CambioClave.controller.js",
                         "~/Scripts/app/Seguridad/CambioClave/CambioClave.service.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/olvidoclave/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/OlvidoClave/OlvidoClave.controller.js",
                         "~/Scripts/app/Seguridad/OlvidoClave/OlvidoClave.service.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app/seguridad/usuario/js").Include(
                        "~/Scripts/app/Seguridad/Seguridad.module.js",
                        "~/Scripts/app/Seguridad/usuario/Usuario.controller.js",
                         "~/Scripts/app/Seguridad/usuario/Usuario.service.js"
                        ));
        }
    }
}