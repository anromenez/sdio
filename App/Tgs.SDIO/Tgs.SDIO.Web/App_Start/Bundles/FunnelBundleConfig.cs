﻿using System.Web.Optimization;

namespace Tgs.SDIO.Web.App_Start.Bundles
{
    public class FunnelBundleConfig
    {
        public static void RegisterFunnelBundleConfig(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteMadurez/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
                "~/Scripts/app/Comun/Sector/Sector.service.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptMadurez/ReporteMadurez.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReportIngresoSectorPeriodo/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptIngresoSectorPeriodo/ReportIngresoSectorPeriodo.controller.js"
          ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reportIngresoLineaNegocioPeriodo/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
                "~/Scripts/app/Comun/Maestra/Maestra.service.js",
               "~/Scripts/app/Funnel/Funnel.module.js",
               "~/Scripts/app/Funnel/RptIngresoLineaNegocioPeriodo/ReportIngresoLineaNegocioPeriodo.controller.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteProbabilidadPorMes/js").Include(
                "~/Scripts/app/Comun/Comun.module.js",
                "~/Scripts/app/Funnel/Funnel.module.js",
                "~/Scripts/app/Funnel/RptProbabilidadPorMes/ReporteProbabilidadPorMes.controller.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteEvolutivoOportunidad/js").Include(
              "~/Scripts/app/Funnel/Funnel.module.js",
              "~/Scripts/app/Funnel/ReporteEvolutivoOportunidad.controller.js"
           ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteOfertasCliente/js").Include(
             "~/Scripts/app/Funnel/Funnel.module.js",
             "~/Scripts/app/Funnel/ReporteOfertasCliente.controller.js"
          ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/reporteLineaNegocioPeriodo/js").Include(
              "~/Scripts/app/Comun/Comun.module.js",
              "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
              "~/Scripts/app/Comun/Maestra/Maestra.service.js",
              "~/Scripts/app/Funnel/Funnel.module.js",
              "~/Scripts/app/Funnel/RptLineaNegocioPeriodo/ReporteLineaNegocioPeriodo.controller.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteOfertasEstadoPeriodo/js").Include(
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/LineaNegocio/LineaNegocio.service.js",
           "~/Scripts/app/Comun/Sector/Sector.service.js",
           "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",
           "~/Scripts/app/Funnel/Funnel.module.js",
           "~/Scripts/app/Funnel/RptOfertasPorEstadoPeriodo/ReporteOfertasEstadoPeriodo.controller.js"
           ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/ReporteOfertasPorSector/js").Include(
           "~/Scripts/app/Comun/Comun.module.js",
           "~/Scripts/app/Comun/SalesForceConsolidadoCabecera/SalesForceConsolidadoCabecera.service.js",
           "~/Scripts/app/Comun/Maestra/Maestra.service.js",
           "~/Scripts/app/Funnel/Funnel.module.js",
           "~/Scripts/app/Funnel/RptOfertasPorSector/ReporteOfertasPorSector.controller.js"
          ));


            bundles.Add(new ScriptBundle("~/bundles/app/funnel/Dashboard/js").Include(
              "~/Scripts/app/Comun/Comun.module.js",
              "~/Scripts/app/Funnel/Funnel.module.js",
              "~/Scripts/app/Funnel/Dashboard/Dashboard.controller.js",
              "~/Scripts/app/Funnel/Dashboard/Dashboard.service.js"
             ));

        }
    }
}