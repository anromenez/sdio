﻿using System;
using System.Web.Mvc;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;

namespace Tgs.SDIO.Web.Controllers
{
    [RepErrorCatch]
    public class HomeController : Controller
    {

        private readonly AgenteServicioComunSDio agenteServicioComunSDio = null;
        public HomeController(AgenteServicioComunSDio agenteServicioComunSDio)
        {
            this.agenteServicioComunSDio = agenteServicioComunSDio;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            throw new Exception();
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [CustomAuthorize]
        public ActionResult Lista()
        {
            var request = new SalesForceConsolidadoCabeceraListarDtoRequest
            {
                IdOportunidad = "PER-001715866"
            };
            var resultado = agenteServicioComunSDio.InvocarFuncionAsync(o => o.GetListSalesForceConsolidadoCabecera(request));
            return View(resultado);
        }

    }
}