﻿using System.Linq;
using System.Web.Mvc;
using Tgs.SDIO.UI.AgenteServicio;
using Tgs.SDIO.Web.Utilitarios;
using Tgs.SDIO.Web.Models;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.Web.Controllers
{
    public class PrincipalController : BaseController
    {
        private readonly AgenteServicioSeguridadSDio _agenteServicioSeguridadSDio = null;
        public PrincipalController(AgenteServicioSeguridadSDio agenteServicioSeguridadSDio)
        {
            _agenteServicioSeguridadSDio = agenteServicioSeguridadSDio;
        }

        public ActionResult Index()
        {
            return View();
        }
        [CustomAuthorize]
        public ActionResult DashboardFunnel()
        {
            return View();
        }


        [CustomAuthorize]
        public ActionResult DashboardCartaFianza()
        {
            return View();
        }


        [CustomAuthorize]
        public ActionResult DashboardTrazabilidad()
        {
            return View();
        }

        public ActionResult PermisoDenegado()
        {
            return View();

        }
        [CustomAuthorize]
        public ActionResult Modulo()
        {
            var menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));

       
            return PartialView("_Modulo", menu);

        }
        public ActionResult DatosUsuario()
        {
            return PartialView(new DetalleUsuario { Usuario = SessionFacade.Usuario });
        }

        [CustomAuthorize]
        public ActionResult Menu(string id)
        {
            string valor = "";
            valor = SessionFacade.Modulo;
            SessionFacade.Modulo = (string.IsNullOrEmpty(id) ? SessionFacade.Modulo : (id.Equals(Numeric.Cero) ? SessionFacade.Modulo : id) );
            
            var menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));

            var menuPerfil = menu.Where(x => x.OpcionMenuDto.IdOpcion== SessionFacade.Modulo.ToString()).ToList();
            if (menuPerfil.Count == 0)
            {
                SessionFacade.Modulo = valor;

                menu = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ListarOpcionesUsuario(User.IdUsuarioSistema));
                 menuPerfil = menu.Where(x => x.OpcionMenuDto.IdOpcion == SessionFacade.Modulo.ToString()).ToList();
            }
            return PartialView("_Menu", menuPerfil);
        }

        [CustomAuthorize]
        public ActionResult PerfilesUsuario()
        {
            var perfilesRais = _agenteServicioSeguridadSDio.InvocarFuncionAsync(o => o.ObtenerPerfiles(User.IdUsuarioSistema)).Count();

            return PartialView("_Perfil", perfilesRais);
        }
        public ActionResult Oferta()
        {
            return View();
        }

    }
}