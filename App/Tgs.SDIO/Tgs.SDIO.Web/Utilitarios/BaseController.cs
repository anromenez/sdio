﻿using System.Web.Mvc;
using System.Collections.Generic;
using Tgs.SDIO.Web.Models;

namespace Tgs.SDIO.Web.Utilitarios
{
    public abstract  class BaseController : Controller
    {   
        protected virtual new UsuarioLogin User
        {
            get { return HttpContext.User as UsuarioLogin; }
        }
          
    }
}