﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Tgs.SDIO.Web.Models;

namespace Tgs.SDIO.Web.Utilitarios
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string RolesConfigKey { get; set; }

        protected virtual UsuarioLogin CurrentUser
        {
            get { return HttpContext.Current.User as UsuarioLogin; }
        }
                 
        public override void OnAuthorization(AuthorizationContext filterContext)
        {           
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {                
                var authorizedRoles = ConfigurationManager.AppSettings[RolesConfigKey];

                Roles = String.IsNullOrEmpty(Roles) ? authorizedRoles : Roles;
                
                if (!String.IsNullOrEmpty(Roles))
                { 
                    if (!CurrentUser.IsInRole(Roles))
                    { 
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "SinAcceso", action = "Index", area = "" }));
                    }
                }
            }
            else 
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "Index", area = "Seguridad" }));
            }           
        }
    }
}