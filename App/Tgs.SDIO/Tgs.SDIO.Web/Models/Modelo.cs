﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.Web.Models
{
    public class Modelo
    {
        public List<SalesForceConsolidadoCabeceraListarDtoResponse> Lista { get; set; }
    }
}