﻿using ChannelAdam.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioCartaFianzaSDio;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioOportunidadSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{


    public class AgenteServicioCartaFianzaSDio : IDisposable
    {

        private readonly IServiceConsumer<IServicioCartaFianzaGeneralSDio> proxy;
        public AgenteServicioCartaFianzaSDio()
        {

            proxy = ServiceConsumerFactory.Create<IServicioCartaFianzaGeneralSDio>(() => new ServicioCartaFianzaGeneralSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioCartaFianzaGeneralSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioCartaFianzaGeneralSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();

        }


    }
}
