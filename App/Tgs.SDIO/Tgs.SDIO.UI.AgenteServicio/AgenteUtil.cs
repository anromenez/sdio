﻿using System;
using ChannelAdam.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public static class AgenteUtil
    {
        public static T ObtenerResultado<T>(IOperationResult<T> result)
        {
            if (result.HasNoException) return result.Value;
            if (result.HasFaultExceptionOfType<Exception>()) throw new Exception(ObtenerMensajeError(result)); //FaultException(Type)
            if (result.HasFaultException) throw new HandledException(ObtenerMensajeError(result)); //Exception FaultException
            throw new Exception(""); //Exception Generica
        }

        public static void FinalizarResultado<T>(IOperationResult<T> result)
        {
            if (result.HasNoException) return;
            if (result.HasFaultExceptionOfType<Exception>()) throw new Exception(ObtenerMensajeError(result)); //FaultException(Type)
            if (result.HasFaultException) throw new HandledException(ObtenerMensajeError(result));  //Exception FaultException
            throw new Exception(""); //Exception Generica
        }

        public static void FinalizarResultado(IOperationResult result)
        {
            if (result.HasNoException) return;
            if (result.HasFaultExceptionOfType<Exception>()) throw new Exception(ObtenerMensajeError(result)); //FaultException(Type)
            if (result.HasFaultException) throw new HandledException(ObtenerMensajeError(result));  //Exception FaultException
            throw new Exception(""); //Exception Generica
        }

        private static string ObtenerMensajeError(IOperationResult result)
        {
            try
            {
                var error = ((System.ServiceModel.FaultException<ErrorDto>)((OperationResult)result).Exception);
                return error == null ? string.Empty : error.Detail.MensajeCorto;
            }
            catch (Exception)
            {
                return string.Empty;
            } 
        }
    }
}
