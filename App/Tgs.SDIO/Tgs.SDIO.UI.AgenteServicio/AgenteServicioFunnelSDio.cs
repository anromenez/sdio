﻿using ChannelAdam.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.UI.ProxyServicio.ServiceReferenceServicioFunnelSDio;

namespace Tgs.SDIO.UI.AgenteServicio
{
    public class AgenteServicioFunnelSDio : IDisposable
    {
        private readonly IServiceConsumer<IServicioFunnelSDio> proxy;
        public AgenteServicioFunnelSDio()
        {
            proxy = ServiceConsumerFactory.Create<IServicioFunnelSDio>(() => new ServicioFunnelSDioClient());
        }

        public T InvocarFuncionAsync<T>(Expression<Func<IServicioFunnelSDio, T>> func)
        {
            var result = proxy.Consume<T>(func);
            return AgenteUtil.ObtenerResultado<T>(result);
        }

        public async Task InvocarAccionAsync(Expression<Func<IServicioFunnelSDio, Task>> func)
        {
            var result = await proxy.ConsumeAsync(func);
            AgenteUtil.FinalizarResultado(result);
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy.Dispose();
        }
    }
}
