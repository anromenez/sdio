﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IActividadOportunidadDal : IRepository<ActividadOportunidad>
    {
        ActividadOportunidadPaginadoDtoResponse ListatActividadOportunidadPaginado(ActividadOportunidadDtoRequest request);

        ActividadOportunidadDtoResponse ObtenerActividadOportunidadPorId(ActividadOportunidadDtoRequest area);

        List<ListaDtoResponse> ListarComboActividadPadres();
    }
}
