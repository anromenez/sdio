﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
    public interface IEstadoCasoDal : IRepository<EstadoCaso>
    {
        List<ListaDtoResponse> ListarComboEstadoCaso();
    }
}
