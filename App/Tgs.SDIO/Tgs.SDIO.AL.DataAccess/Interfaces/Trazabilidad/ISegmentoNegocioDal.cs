﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad
{
   public interface ISegmentoNegocioDal : IRepository<SegmentoNegocio>
    {
        List<ComboDtoResponse> ListarComboSegmentoNegocio();

        List<ListaDtoResponse> ListarComboSegmentoNegocioSimple();
    }
}
