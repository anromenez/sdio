﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Funnel
{
    public interface IOportunidadFinancieraOrigenDal
    {
        IEnumerable<OportunidadFinancieraOrigenDtoResponse> ListarIndicadorMadurez(IndicadorMadurezDtoRequest request);
        IEnumerable<DashoardOfertasLineaNegocioDtoResponse> ListarIndicadorOfertasLineaNegocio(DashoardDtoRequest request);
        IEnumerable<DashoardOfertasSectorDtoResponse> ListarIndicadorOfertasSector(DashoardDtoRequest request);
        IEnumerable<DashoardIngresosLineaNegocioDtoResponse> ListarIndicadorIngresosLineaNegocio(DashoardDtoRequest request);
        IEnumerable<DashoardIngresosSectorDtoResponse> ListarIndicadorIngresosSector(DashoardDtoRequest request);
        IEnumerable<DashoardOfertasEstadoDtoResponse> ListarIndicadorOfertasEstado(DashoardDtoRequest request);
        IEnumerable<DashboardTotalesDtoResponse> ListarIndicadoresTotales(DashoardDtoRequest request);
    }
}
