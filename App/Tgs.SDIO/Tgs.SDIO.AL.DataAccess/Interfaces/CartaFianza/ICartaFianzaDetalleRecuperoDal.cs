﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{

   
    public interface ICartaFianzaDetalleRecuperoDal:IRepository<CartaFianzaDetalleRecupero>
    {
        List<CartaFianzaDetalleRecuperoResponse> InsertaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoRequestInserta);
        CartaFianzaDetalleRecuperoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoActualiza);
        CartaFianzaDetalleRecuperoResponse ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoLista);




    }
}
