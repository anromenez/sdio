﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza
{
    public interface ICartaFianzaColaboradorDetalleDal : IRepository<CartaFianzaColaboradorDetalle>
    {
        List<CartaFianzaColaboradorDetalleResponse> ListaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList);
        List<CartaFianzaColaboradorDetalleListaResponse> ListaCartaFianzaColaboradorDetalleId(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList);
        CartaFianzaColaboradorDetalleResponse ActualizaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestActualiza);
        CartaFianzaColaboradorDetalleResponse InsertaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestInserta);
    }
}