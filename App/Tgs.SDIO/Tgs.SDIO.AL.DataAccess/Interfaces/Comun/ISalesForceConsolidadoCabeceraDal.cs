﻿using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface ISalesForceConsolidadoCabeceraDal : IRepository<SalesForceConsolidadoCabecera>
    {
        List<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera(SalesForceConsolidadoCabecera request);

        List<SalesForceConsolidadoCabeceraResponse> ListarProbabilidad();

        IEnumerable<SalesForceConsolidadoCabeceraDtoResponse> ListarIndicadorCostoOportunidad(IndicadorMadurezDtoRequest request);
    }
}
