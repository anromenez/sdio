﻿using System;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class SubServicioDatosCapexDal : Repository<SubServicioDatosCapex>, ISubServicioDatosCapexDal
    {
        readonly DioContext context;
        public SubServicioDatosCapexDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public OportunidadFlujoCajaDetalleDtoResponse ObtenerTipoCambio(OportunidadFlujoCajaDtoRequest flujocaja)
        {

            var lista = (from tc in context.Set<OportunidadTipoCambio>()
                                  join tcd in context.Set<OportunidadTipoCambioDetalle>() on tc.IdTipoCambioOportunidad equals tcd.IdTipoCambioOportunidad
                                  where tcd.IdEstado == Generales.Estados.Activo && 
                                  tc.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio &&
                                  tcd.IdTipificacion == 2 //116 ingresos, 1 costos, 2 capex
                                  
                                  select new
                                  {
                                      IdOportunidadLineaNegocio = tc.IdOportunidadLineaNegocio,
                                      IdTipificacion = tcd.IdTipificacion,
                                      Monto = tcd.Monto
                                  }).Single();

            var tipoCambio = new OportunidadFlujoCajaDetalleDtoResponse();
            tipoCambio.Monto = lista.Monto;

            return tipoCambio;
        }

    }
}
