﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadServicioCMIDal : Repository<OportunidadServicioCMI>, IOportunidadServicioCMIDal
    {
        readonly DioContext context;

        public OportunidadServicioCMIDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public void RegistrarFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalleFlujo)
        {

        }

        public ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidadServicioCMI)
        {
            throw new NotImplementedException();
        }

        public OportunidadServicioCMIDtoResponse ObtenerOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidadServicioCMI)
        {
            throw new NotImplementedException();
        }

        public List<OportunidadServicioCMIDtoResponse> ListarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidadServicioCMI)
        {
            var lista = (from sCMI in context.Set<ServicioCMI>()
                         join osCMI in context.Set<OportunidadServicioCMI>() on sCMI.IdServicioCMI equals osCMI.IdServicioCMI
                         join ln in context.Set<Linea>() on sCMI.IdLinea equals ln.IdLinea
                         where (osCMI.IdOportunidadLineaNegocio == oportunidadServicioCMI.IdOportunidadLineaNegocio &&
                                sCMI.IdEstado == oportunidadServicioCMI.IdEstado)
                         select new OportunidadServicioCMIDtoResponse
                         {
                             IdOportunidadLineaNegocio = osCMI.IdOportunidadLineaNegocio,
                             IdServicioCMI = osCMI.IdServicioCMI,
                             CodigoCMI = sCMI.CodigoCMI,
                             DescripcionCMI = sCMI.DescripcionCMI,
                             Porcentaje = osCMI.Porcentaje.ToString(),
                             Linea = ln.Descripcion

                         }).ToList<OportunidadServicioCMIDtoResponse>();

            return lista;
        }
    }
}
