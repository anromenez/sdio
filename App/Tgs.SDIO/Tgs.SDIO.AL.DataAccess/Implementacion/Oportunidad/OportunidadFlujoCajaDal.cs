﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaDal : Repository<OportunidadFlujoCaja>, IOportunidadFlujoCajaDal
    {
        readonly DioContext context;
        public OportunidadFlujoCajaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return new List<OportunidadFlujoCajaDtoResponse>();
        }

        public List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var ListaTiempoProyecto = new List<ListaDtoResponse>();
            var tiempo = (from o in context.Set<Entities.Entities.Oportunidad.Oportunidad>()
                          join oln in context.Set<OportunidadLineaNegocio>() on o.IdOportunidad equals oln.IdOportunidad
                          where oln.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio
                          select o.Periodo + o.TiempoImplantacion).Single();

            decimal ano = decimal.Ceiling((decimal) tiempo / 12);
            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());

            for (int i = 0; i <= tiempo; i++)
            {
                var c = new ListaDtoResponse()
                {
                    Codigo = (i+1).ToString(),
                    Descripcion = fecha.AddMonths(i).Year.ToString() + " - " + fecha.AddMonths(i).Month.ToString()
                };
                ListaTiempoProyecto.Add(c);
            }

            return ListaTiempoProyecto;
        }

        public List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdCasoNegocio = new SqlParameter { ParameterName = "IdCasoNegocio", Value = casonegocio.IdCasoNegocio, SqlDbType = SqlDbType.Int };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = casonegocio.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_INSERTAR_CASO_NEGOCIO] @IdOportunidadLineaNegocio,@IdCasoNegocio,@IdUsuarioCreacion,@IdEstado",
                    IdOportunidadLineaNegocio, IdCasoNegocio, IdUsuarioCreacion, IdEstado).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }

        public ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var resultado = new ProcesoResponse();
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdCasoNegocio = new SqlParameter { ParameterName = "IdCasoNegocio", Value = casonegocio.IdCasoNegocio, SqlDbType = SqlDbType.Int };
            var IdUsuarioEdicion = new SqlParameter { ParameterName = "IdUsuarioEdicion", Value = casonegocio.IdUsuarioEdicion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };

            resultado.TipoRespuesta = context.ExecuteCommand("[OPORTUNIDAD].[USP_ELIMINAR_CASO_NEGOCIO] @IdOportunidadLineaNegocio,@IdCasoNegocio,@IdUsuarioEdicion,@IdEstado",
                                                                IdOportunidadLineaNegocio, IdCasoNegocio, IdUsuarioEdicion, IdEstado);

            return resultado;

        }

        public List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdServicio = new SqlParameter { ParameterName = "IdServicio", Value = casonegocio.IdServicio, SqlDbType = SqlDbType.Int };
            var IdUsuarioCreacion = new SqlParameter { ParameterName = "IdUsuarioCreacion", Value = casonegocio.IdUsuarioCreacion, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_INSERTAR_SERVICIO] @IdOportunidadLineaNegocio,@IdServicio,@IdUsuarioCreacion,@IdEstado",
                    IdOportunidadLineaNegocio, IdServicio, IdUsuarioCreacion, IdEstado).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };
            

            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_ECAPEX] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            casonegocio.IdOportunidadLineaNegocio = 2;//borrar
            var IdOportunidadLineaNegocio = new SqlParameter { ParameterName = "IdOportunidadLineaNegocio", Value = casonegocio.IdOportunidadLineaNegocio, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };


            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA] @IdOportunidadLineaNegocio,@IdEstado,@IdGrupo",
                    IdOportunidadLineaNegocio, IdEstado, IdGrupo).ToList<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }

        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            var IdSubServicioDatosCapex = new SqlParameter { ParameterName = "IdSubServicioDatosCapex", Value = casonegocio.IdSubServicioDatosCapex, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "IdEstado", Value = casonegocio.IdEstado, SqlDbType = SqlDbType.Int };
            var IdGrupo = new SqlParameter { ParameterName = "IdGrupo", Value = casonegocio.IdGrupo, SqlDbType = SqlDbType.Int };


            var OportunidadFlujoCaja = context.ExecuteQuery<OportunidadFlujoCajaDtoResponse>("[OPORTUNIDAD].[USP_OBTENER_OPORTUNIDAD_ECAPEX] @IdSubServicioDatosCapex,@IdEstado,@IdGrupo",
                    IdSubServicioDatosCapex, IdEstado, IdGrupo).Single<OportunidadFlujoCajaDtoResponse>();

            return OportunidadFlujoCaja;
        }

    }
}
