﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class LineaConceptoCMIDal : Repository<LineaConceptoCMI>, ILineaConceptoCMIDal
    {
        readonly DioContext context;
        public LineaConceptoCMIDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<LineaConceptoCMIDtoResponse> ListarConceptosPorLinea(LineaConceptoCMIDtoRequest linea)
        {
            var IdLineaProducto = new SqlParameter { ParameterName = "@IdLineaProducto", Value = linea.IdLineaProducto, SqlDbType = SqlDbType.Int };
            var IdEstado = new SqlParameter { ParameterName = "@IdEstado", Value = linea.IdEstado, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<LineaConceptoCMIDtoResponse>("OPORTUNIDAD.USP_LISTAR_CONCEPTOS_LINEA @IdLineaProducto, @IdEstado", IdLineaProducto, IdEstado).ToList<LineaConceptoCMIDtoResponse>();

            return lista;
        }
    }
}
