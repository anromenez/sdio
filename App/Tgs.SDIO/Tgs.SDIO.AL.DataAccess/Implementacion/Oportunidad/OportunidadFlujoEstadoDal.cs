﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadFlujoEstadoDal : Repository<OportunidadFlujoEstado>, IOportunidadFlujoEstadoDal
    {
        readonly DioContext context;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadFlujoEstadoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public int OportunidadFlujoEstadoUltimoId()

        {
            var maxValue = context.OportunidadFlujoEstado.Max(x => x.IdOportunidadFlujoEstado);

            return maxValue;
        }
    }
}



