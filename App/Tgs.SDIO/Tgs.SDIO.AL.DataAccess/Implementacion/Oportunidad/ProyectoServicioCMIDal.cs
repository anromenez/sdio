﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class ProyectoServicioCMIDal : Repository<ProyectoServicioCMI>, IProyectoServicioCMIDal
    {
        readonly DioContext context;
        public ProyectoServicioCMIDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ProyectoServicioCMIDtoResponse> ListarProyectoServicioCMI(ProyectoServicioCMIDtoRequest proyecto)
        {
            var pIDPROYECTO = new SqlParameter { ParameterName = "IDPROYECTO", Value = proyecto.IdProyecto, SqlDbType = SqlDbType.Int };
            var pIDLINEAPRODUCTO = new SqlParameter { ParameterName = "IDLINEAPRODUCTO", Value = proyecto.IdLineaProducto, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<ProyectoServicioCMIDtoResponse>("STF_LISTARPROYECTOSERVICIOSCMI @IDPROYECTO , @IDLINEAPRODUCTO",
             pIDPROYECTO, pIDLINEAPRODUCTO).ToList<ProyectoServicioCMIDtoResponse>();

            return lista;
        }
    }
}
