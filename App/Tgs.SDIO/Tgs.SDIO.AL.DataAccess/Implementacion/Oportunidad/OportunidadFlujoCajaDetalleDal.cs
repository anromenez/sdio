﻿using System;
using System.Data;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Funciones;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaDetalleDal : Repository<OportunidadFlujoCajaDetalle>, IOportunidadFlujoCajaDetalleDal
    {
        readonly DioContext context;

        public OportunidadFlujoCajaDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }        

        public void RegistrarFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalleFlujo)
        {
            var oportunidadFlujoCajaDetalle = new OportunidadFlujoCajaDetalle()
            {
                IdFlujoCajaConfiguracion = detalleFlujo.IdFlujoCajaConfiguracion,
                Anio = detalleFlujo.Anio,
                Mes = detalleFlujo.Mes,
                Monto = detalleFlujo.Monto,
                IdEstado = Estados.Activo,
                IdUsuarioCreacion = detalleFlujo.IdUsuarioCreacion,
                FechaCreacion = detalleFlujo.FechaCreacion,
                TipoFicha = detalleFlujo.TipoFicha
            };

            this.Add(oportunidadFlujoCajaDetalle);
            this.UnitOfWork.Commit();
        }

        public void CalculaFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            DateTime mesActual = detalle.FechaInicio;
            int mesesDiferencia = 0, apoyo = 0;

            if (detalle.Intervalo != 12)
            {
                while (mesActual < detalle.FechaFin)
                {
                    detalle.Mes = mesActual.Month;
                    detalle.Anio = mesActual.Year;
                    RegistrarFlujoCajaDetalle(detalle);
                    mesActual = mesActual.AddMonths(detalle.Intervalo);
                }
            }
            else
            {
                mesesDiferencia = Funciones.DiferenciaMeses(detalle.FechaFin, detalle.FechaInicio);
                mesesDiferencia = mesesDiferencia / 12;

                while (mesActual < detalle.FechaFin)
                {
                    detalle.Mes = mesActual.Month;
                    detalle.Anio = mesActual.Year;
                    RegistrarFlujoCajaDetalle(detalle);
                    mesActual = mesActual.AddMonths(detalle.Intervalo);

                    if (apoyo == mesesDiferencia)
                    {
                        mesActual = mesActual.AddMonths(999);
                    }
                }
            }
        }

        public OportunidadFlujoCajaDetalleDtoResponse GeneraProyectadoOportunidadFlujoCaja(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            //Obteniendo la lista de registros de FlujoCaja y el objeto Oportunidad
            var listaFlujoCaja = (from fc in context.Set<OportunidadFlujoCaja>()
                                  where fc.IdEstado == Generales.Estados.Activo && fc.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio

                                  orderby fc.IdOportunidadLineaNegocio ascending
                                  select new 
                                  {
                                      IdFlujoCaja = fc.IdFlujoCaja,
                                      IdOportunidadLineaNegocio = fc.IdOportunidadLineaNegocio,
                                      Periodos = fc.IdPeriodos
                                  }).ToList();            

            var oportunidad = (from o in context.Set<Entities.Entities.Oportunidad.Oportunidad>()
                                join ln in context.Set<OportunidadLineaNegocio>() on o.IdOportunidad equals ln.IdOportunidad
                                where o.IdEstado == Generales.Estados.Activo && ln.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio
                                select new {
                                    IdOportunidad = o.IdOportunidad,
                                    Fecha = o.Fecha
                                }).Single();

            //Limpiando la tabla de destino([OPORTUNIDAD].[OportunidadFlujoCajaDetalle]) antes de insertar nuevos datos
            context.ExecuteCommand("truncate table [OPORTUNIDAD].[OportunidadFlujoCajaDetalle]");

            foreach (var item in listaFlujoCaja)
            {
                var oportunidadFlujoCajaConfiguracion = new OportunidadFlujoCajaConfiguracionDtoRequest();
                oportunidadFlujoCajaConfiguracion.IdFlujoCaja = item.IdFlujoCaja;

                OportunidadFlujoCajaConfiguracionDtoResponse cajaConfiguracion = new OportunidadFlujoCajaConfiguracionDal(context).ListarOportunidadFlujoCajaConfiguraciones(oportunidadFlujoCajaConfiguracion)[0];

                double montoCuota;
                montoCuota = Convert.ToDouble(cajaConfiguracion.CostoPreOperativo) * Convert.ToDouble(cajaConfiguracion.Ponderacion) / 100.0;

                int intervalo = 0;
                switch (item.Periodos)
                {
                    case 1: intervalo = 12; break;
                    case 2: intervalo = 1; break;
                    case 3: intervalo = 1; break;
                }

                var detalleflujo = new OportunidadFlujoCajaDetalleDtoRequest();
                detalleflujo.IdFlujoCajaConfiguracion = cajaConfiguracion.IdFlujoCajaConfiguracion;
                detalleflujo.FechaInicio = oportunidad.Fecha.AddMonths(cajaConfiguracion.Inicio - 1);
                detalleflujo.FechaFin = oportunidad.Fecha.AddMonths(cajaConfiguracion.Inicio + cajaConfiguracion.Meses - 1);
                detalleflujo.Monto = montoCuota;
                detalleflujo.Intervalo = intervalo;
                detalleflujo.IdUsuarioCreacion = flujocaja.IdUsuarioCreacion;
                detalleflujo.FechaCreacion = DateTime.Now;
                detalleflujo.TipoFicha = flujocaja.TipoFicha;
                CalculaFlujoCajaDetalle(detalleflujo);

            }

            return new OportunidadFlujoCajaDetalleDtoResponse();
        }

        public ProcesoResponse RegistrarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            throw new NotImplementedException();
        }

        public OportunidadFlujoCajaDetalleDtoResponse ObtenerOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

    }
}
