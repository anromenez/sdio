﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class CasoNegocioServicioDal : Repository<CasoNegocioServicio>, ICasoNegocioServicioDal
    {
        readonly DioContext context;

        public CasoNegocioServicioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<CasoNegocioServicioDtoResponse> ListarCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            var casoNegocioServicio = (from c in context.Set<CasoNegocioServicio>()
                                       join s in context.Set<Servicio>() on c.IdServicio equals s.IdServicio
                                       where c.IdCasoNegocio == casoservicio.IdCasoNegocio && 
                                             c.IdEstado == casoservicio.IdEstado
                                       select new CasoNegocioServicioDtoResponse
                                       {
                                           IdCasoNegocioServicio = c.IdCasoNegocioServicio,
                                           IdCasoNegocio = c.IdCasoNegocio,
                                           IdServicio = c.IdServicio,
                                           IdEstado = c.IdEstado,
                                           Servicio = s.Descripcion
                                       }).ToList<CasoNegocioServicioDtoResponse>();

            return casoNegocioServicio;
        }

        public CasoNegocioServicioDtoResponse ObtenerCasoNegocioServicio(CasoNegocioServicioDtoRequest casoservicio)
        {
            var casoNegocioServicio = (from c in context.Set<CasoNegocioServicio>()
                               join s in context.Set<Servicio>() on c.IdServicio equals s.IdServicio
                               where c.IdCasoNegocio == casoservicio.IdCasoNegocio &&
                                     c.IdEstado == casoservicio.IdEstado
                               select new CasoNegocioServicioDtoResponse
                               {
                                   IdCasoNegocioServicio = c.IdCasoNegocioServicio,
                                   IdCasoNegocio = c.IdCasoNegocio,
                                   IdServicio = c.IdServicio,
                                   IdEstado = c.IdEstado,
                                   Servicio = s.Descripcion
                               }).Single<CasoNegocioServicioDtoResponse>();

            return casoNegocioServicio;
        }
    }
}
