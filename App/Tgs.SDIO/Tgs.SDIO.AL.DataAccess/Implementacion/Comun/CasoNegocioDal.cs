﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class CasoNegocioDal : Repository<CasoNegocio>, ICasoNegocioDal
    {
        readonly DioContext context;

        public CasoNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
        
        public List<CasoNegocioDtoResponse> ListarCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            var casoNegocio = (from c in context.Set<CasoNegocio>()
                               join l in context.Set<LineaNegocio>() on c.IdLineaNegocio equals l.IdLineaNegocio
                               where ((casonegocio.IdLineaNegocio == Numeric.NegativoUno) || (c.IdLineaNegocio == casonegocio.IdLineaNegocio)) &&
                                     c.IdEstado == casonegocio.IdEstado &&
                                     ((casonegocio.Descripcion == null) || (c.Descripcion.Contains(casonegocio.Descripcion)))
                               select new CasoNegocioDtoResponse
                               {
                                   IdLineaNegocio = c.IdLineaNegocio,
                                   IdCasoNegocio = c.IdCasoNegocio,
                                   Descripcion = c.Descripcion,
                                   FlagDefecto = c.FlagDefecto,
                                   IdEstado = c.IdEstado,
                                   LineaNegocio = l.Descripcion
                               }).ToList<CasoNegocioDtoResponse>();

            return casoNegocio;
        }

        public CasoNegocioDtoResponse ObtenerCasoNegocio(CasoNegocioDtoRequest casonegocio)
        {
            var casoNegocio = (from c in context.Set<CasoNegocio>()
                               join l in context.Set<LineaNegocio>() on c.IdLineaNegocio equals l.IdLineaNegocio
                               where c.IdLineaNegocio == casonegocio.IdLineaNegocio && c.IdCasoNegocio == casonegocio.IdCasoNegocio
                               select new CasoNegocioDtoResponse
                               {
                                   IdLineaNegocio = c.IdLineaNegocio,
                                   IdCasoNegocio = c.IdCasoNegocio,
                                   Descripcion = c.Descripcion,
                                   FlagDefecto = c.FlagDefecto,
                                   IdEstado = c.IdEstado,
                                   LineaNegocio = l.Descripcion
                               }).Single<CasoNegocioDtoResponse>();

            return casoNegocio;
        }

    }
}
