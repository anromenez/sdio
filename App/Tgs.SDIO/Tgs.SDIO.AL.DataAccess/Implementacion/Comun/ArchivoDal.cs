﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ArchivoDal : Repository<Archivo>, IArchivoDal
    {
        readonly DioContext context;
        public ArchivoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

    }
}
