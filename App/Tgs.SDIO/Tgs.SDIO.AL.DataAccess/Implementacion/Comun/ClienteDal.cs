﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ClienteDal : Repository<Cliente>, IClienteDal
    {
        readonly DioContext context;

        public ClienteDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente)
        {

            var Listcliente = (from c in context.Set<Cliente>()
                               
                               where ((c.NumeroIdentificadorFiscal.Contains(cliente.NumeroIdentificadorFiscal) || cliente.NumeroIdentificadorFiscal == null)) &&
                                     c.IdEstado == cliente.IdEstado &&
                                     ((cliente.Descripcion == null) || (c.Descripcion.Contains(cliente.Descripcion)))
                               select new ClienteDtoResponse
                               {
                                   IdCliente = c.IdCliente,
                                   CodigoCliente = c.CodigoCliente,
                                   Descripcion = c.Descripcion,
                                   IdEstado = c.IdEstado,
                                   IdSector = c.IdSector,
                                   GerenteComercial = c.GerenteComercial,
                                   IdDireccionComercial = c.IdDireccionComercial,
                                   NumeroIdentificadorFiscal = c.NumeroIdentificadorFiscal,
                                   IdTipoIdentificadorFiscalTm = c.IdTipoIdentificadorFiscalTm
                               }).ToList<ClienteDtoResponse>();

            return Listcliente;

        }


    }
}
