﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ConceptoDal : Repository<Concepto>, IConceptoDal
    {
        readonly DioContext context;

        public ConceptoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ConceptoDtoResponse> ListarBandejaConceptos(ConceptoDtoRequest conceptoDtoRequest)
        {
            var pDESCRIPCION = new SqlParameter { ParameterName = "DESCRIPCION", Value = conceptoDtoRequest.Descripcion, SqlDbType = SqlDbType.VarChar };
            var pIDTIPOCONCEPTO = new SqlParameter { ParameterName = "IDTIPOCONCEPTO", Value = conceptoDtoRequest.IdTipoConcepto, SqlDbType = SqlDbType.Int };
            var pIDDEPRECIACION = new SqlParameter { ParameterName = "IDDEPRECIACION", Value = conceptoDtoRequest.IdTipoConcepto, SqlDbType = SqlDbType.Int };
            var pIDESTADO = new SqlParameter { ParameterName = "IDESTADO", Value = conceptoDtoRequest.IdEstado, SqlDbType = SqlDbType.Int };

            var lista = context.ExecuteQuery<ConceptoDtoResponse>("STF_LISTACONCEPTOS @DESCRIPCION, @IDTIPOCONCEPTO, @IDDEPRECIACION, @IDESTADO", pDESCRIPCION, pIDTIPOCONCEPTO, pIDDEPRECIACION, pIDESTADO).ToList<ConceptoDtoResponse>();

            return lista;
        }
        
    }
}
