﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Paginacion;
using Tgs.SDIO.Util.Constantes;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{

    public class ServicioDal : Repository<Servicio>, IServicioDal
    {
        readonly DioContext context;
        public ServicioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ServicioPaginadoDtoResponse ListaServicioPaginado(ServicioDtoRequest Servicio)
        {
            var query = (from obj in context.Set<Servicio>()

                         where
                     (obj.Descripcion.Contains(Servicio.Descripcion) || string.IsNullOrEmpty(Servicio.Descripcion)
                       && obj.IdEstado == Generales.Estados.Activo)
                         orderby obj.FechaCreacion ascending
                         select new ServicioDtoResponse
                         {
                             IdServicio = obj.IdServicio,
                             Descripcion = obj.Descripcion,


                         }).AsNoTracking().Distinct().OrderBy(x => x.IdServicio).ToPagedList(Servicio.Indice, Servicio.Tamanio);

            var resultado = new ServicioPaginadoDtoResponse
            {
                ListServicioDtoResponse = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return resultado;
        }
  
    }
}
