﻿using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using System.Data.SqlClient;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using System.Data;
using System;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class SalesForceConsolidadoCabeceraDal : Repository<SalesForceConsolidadoCabecera>, ISalesForceConsolidadoCabeceraDal
    {
        readonly DioContext context;

        public SalesForceConsolidadoCabeceraDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera(SalesForceConsolidadoCabecera request)
        {
            var query = (from bit in context.SalesForceConsolidadoCabecera
                         where bit.IdOportunidad == request.IdOportunidad
                         select bit).AsQueryable();

            var resultado = query.ToList();

            return new List<SalesForceConsolidadoCabecera>(resultado);
        }

        public List<SalesForceConsolidadoCabeceraResponse> ListarProbabilidad()
        {
            var lista = context.ExecuteQuery<SalesForceConsolidadoCabeceraResponse>("COMUN.USP_LISTAR_PROBABILIDADEXITO").ToList();
            return lista;
        }

        public IEnumerable<SalesForceConsolidadoCabeceraDtoResponse> ListarIndicadorCostoOportunidad(IndicadorMadurezDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = request.IdLineaNegocio ?? (object)DBNull.Value , SqlDbType = SqlDbType.Int };
            var idSector = new SqlParameter { ParameterName = "IdSector", Value = request.IdSector ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };


            var lista =
                context.ExecuteQuery<SalesForceConsolidadoCabeceraDtoResponse>("[FUNNEL].[USP_INDICADORMADUREZCONSULTAR_DETALLE] @Anio,@Mes,@IdLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa",
                anio, mes, idLineaNegocio, idSector, probabilidadExito, etapa).ToList();

            return lista;

        }


    }
}
