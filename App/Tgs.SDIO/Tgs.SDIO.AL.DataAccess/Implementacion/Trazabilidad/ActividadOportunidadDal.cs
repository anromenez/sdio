﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using System.Linq;
using System.Data.Entity;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class ActividadOportunidadDal : Repository<ActividadOportunidad>, IActividadOportunidadDal
    {
        readonly DioContext context;

        public ActividadOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ActividadOportunidadPaginadoDtoResponse ListatActividadOportunidadPaginado(ActividadOportunidadDtoRequest request)
        {
            var query = (from actividad in context.Set<ActividadOportunidad>()
                         join area in context.Set<AreaSeguimiento>() on actividad.IdAreaSeguimiento equals area.IdAreaSeguimiento
                         join maestra in context.Set<Maestra>().Where(x => x.IdRelacion == 146) on actividad.IdTipoActividad equals maestra.Valor
                         join fase in context.Set<FaseOportunidad>() on actividad.IdFase equals fase.IdFase
                 
                         where ((actividad.Descripcion.Contains(request.Descripcion) || string.IsNullOrEmpty(request.Descripcion))
                                && (actividad.IdEtapa == request.IdEtapa || request.IdEtapa == -1)
                                && (area.IdAreaSeguimiento == request.IdAreaSeguimiento || request.IdAreaSeguimiento == -1)
                                && area.IdEstado == EstadosEntidad.Activo
                                && maestra.IdEstado == EstadosEntidad.Activo
                                && (actividad.IdEstado == EstadosEntidad.Activo || actividad.IdEstado == EstadosEntidad.Inactivo))
                         orderby actividad.FechaCreacion descending
                         select new ActividadOportunidadDtoResponse
                         {
                             IdActividad = actividad.IdActividad,
                             IdActividadPadre = actividad.IdActividadPadre,
                             Descripcion = actividad.Descripcion,
                             Predecesoras = actividad.Predecesoras,
                             DescripcionArea = area.Descripcion,
                             DescripcionTipoActividad = maestra.Descripcion,
                             DescripcionFase = fase.Descripcion,
                             AsegurarOferta = actividad.AsegurarOferta,
                             FlgCapexMayor = actividad.FlgCapexMayor,
                             FlgCapexMenor = actividad.FlgCapexMenor,
                             NumeroDiaCapexMenor = actividad.NumeroDiaCapexMenor,
                             CantidadDiasCapexMenor = actividad.CantidadDiasCapexMenor,
                             NumeroDiaCapexMayor = actividad.NumeroDiaCapexMayor,
                             CantidadDiasCapexMayor = actividad.CantidadDiasCapexMayor,
                             Estado = area.IdEstado == EstadosEntidad.Activo ? "Activo" : "Inactivo"
                         }).AsNoTracking().Distinct().OrderBy(x => x.IdActividad).ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new ActividadOportunidadPaginadoDtoResponse
            {
                ListActividadOportunidadDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public ActividadOportunidadDtoResponse ObtenerActividadOportunidadPorId(ActividadOportunidadDtoRequest request)
        {

            var querySegmento = (from actiseg in context.Set<ActividadSegmentoNegocio>()
                                 join seg in context.Set<SegmentoNegocio>() on actiseg.IdSegmentoNegocio equals seg.IdSegmentoNegocio
                                 where seg.IdEstado == EstadosEntidad.Activo
                                 && (actiseg.IdEstado == EstadosEntidad.Activo && actiseg.IdActividad == request.IdActividad)
                                 orderby actiseg.FechaCreacion descending
                                 select new ComboDtoResponse
                                 {
                                     id = actiseg.IdSegmentoNegocio.ToString(),
                                     label = seg.Descripcion
                                 }).AsNoTracking().Distinct().ToList();

            var queryActividad = (from actividad in context.Set<ActividadOportunidad>()
                             where actividad.IdActividad == request.IdActividad && (actividad.IdEstado == EstadosEntidad.Activo || actividad.IdEstado == EstadosEntidad.Inactivo)
                             select actividad).AsNoTracking().FirstOrDefault();

               return  new ActividadOportunidadDtoResponse()
                {
                    IdActividad = queryActividad.IdActividad,
                    IdActividadPadre = queryActividad.IdActividadPadre,
                    Descripcion = queryActividad.Descripcion,
                    Predecesoras = queryActividad.Predecesoras,
                    IdFase = queryActividad.IdFase,
                    IdEtapa = queryActividad.IdEtapa,
                    FlgCapexMayor = queryActividad.FlgCapexMayor,
                    FlgCapexMenor = queryActividad.FlgCapexMenor,
                    IdAreaSeguimiento = queryActividad.IdAreaSeguimiento,
                    NumeroDiaCapexMenor = queryActividad.NumeroDiaCapexMenor,
                    CantidadDiasCapexMenor = queryActividad.CantidadDiasCapexMenor,
                    NumeroDiaCapexMayor = queryActividad.NumeroDiaCapexMayor,
                    CantidadDiasCapexMayor = queryActividad.CantidadDiasCapexMayor,
                    IdTipoActividad = queryActividad.IdTipoActividad,
                    AsegurarOferta = queryActividad.AsegurarOferta,
                    IdEstado = queryActividad.IdEstado,
                    IdUsuarioCreacion = queryActividad.IdUsuarioCreacion,
                    FechaCreacion = queryActividad.FechaCreacion,
                    IdUsuarioEdicion = queryActividad.IdUsuarioEdicion,
                    FechaEdicion = queryActividad.FechaEdicion,
                    ListSegmentoNegocio = querySegmento
                };


        }

        public List<ListaDtoResponse> ListarComboActividadPadres()
        {
            var query = (from bit in context.ActividadOportunidad.Where(x => x.IdEstado == EstadosEntidad.Activo && x.IdActividadPadre == null) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdActividad.ToString() });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }

    }
}
