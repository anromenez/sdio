﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class FaseOportunidadDal : Repository<FaseOportunidad>, IFaseOportunidadDal
    {
        readonly DioContext context;
        public FaseOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public List<ListaDtoResponse> ListarComboFaseOportunidad()
        {

            var query = (from bit in context.FaseOportunidad.Where(x => x.IdEstado == EstadosEntidad.Activo) select new ListaDtoResponse { Descripcion = bit.Descripcion, Codigo = bit.IdFase.ToString() });

            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
