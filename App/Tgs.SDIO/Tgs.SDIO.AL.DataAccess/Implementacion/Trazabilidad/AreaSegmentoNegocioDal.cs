﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public  class AreaSegmentoNegocioDal : Repository<AreaSegmentoNegocio>, IAreaSegmentoNegocioDal
    {
        readonly DioContext context;

        public AreaSegmentoNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
    }
}
