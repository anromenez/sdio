﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;


namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class ConceptoSeguimientoDal : Repository<ConceptoSeguimiento>, IConceptoSeguimientoDal
    {
        readonly DioContext context;
        public ConceptoSeguimientoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request)
        {

            var query = (from concepto in context.Set<ConceptoSeguimiento>()
                         where ((concepto.Descripcion.Contains(request.Descripcion) || string.IsNullOrEmpty(request.Descripcion))
                         && (concepto.IdEstado == EstadosEntidad.Activo || concepto.IdEstado == EstadosEntidad.Inactivo))
                         orderby concepto.FechaCreacion descending
                         select new ConceptoSeguimientoDtoResponse
                         {
                             IdConcepto= concepto.IdConcepto,
                             IdConceptoPadre = concepto.IdConceptoPadre,
                             Descripcion = concepto.Descripcion,
                             Nivel = concepto.Nivel,
                             OrdenVisual = concepto.OrdenVisual,
                             IdEstado = concepto.IdEstado,
                             Estado = concepto.IdEstado == EstadosEntidad.Activo ? "Activo" : "Inactivo",
                             IdUsuarioCreacion = concepto.IdUsuarioCreacion,
                             FechaCreacion = concepto.FechaCreacion,
                             IdUsuarioEdicion = concepto.IdUsuarioEdicion,
                             FechaEdicion = concepto.FechaEdicion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);


            var datosCarga = new ConceptoSeguimientoPaginadoDtoResponse
            {
                ListConceptoSeguimientoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimiento()
        {
            var query = (from bit in context.ConceptoSeguimiento.Where(x => x.IdEstado == EstadosEntidad.Activo) select new ListaDtoResponse { Codigo = bit.IdConcepto.ToString(), Descripcion = bit.Descripcion });
            var resultado = query.ToList();

            return new List<ListaDtoResponse>(resultado);
        }
    }
}
