﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using System.Linq;
using System.Data.Entity;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
    public class OportunidadSTDal : Repository<OportunidadST>, IOportunidadSTDal
    {
        readonly DioContext context;

        public OportunidadSTDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join solicitud in context.Set<TipoSolicitud>() on caso.IdTipoSolicitud equals solicitud.IdTipoSolicitud
                         where ((oportunidad.Descripcion.Contains(request.DescripcionOportunidad) || string.IsNullOrEmpty(request.DescripcionOportunidad))
                         && (caso.Descripcion.Contains(request.DescripcionCaso) || string.IsNullOrEmpty(request.DescripcionCaso))
                         && (oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && (caso.IdCasoSF.Contains(request.IdCasoSF) || string.IsNullOrEmpty(request.IdCasoSF))
                         && oportunidad.IdEstado == EstadosEntidad.Activo
                         && caso.IdEstado == EstadosEntidad.Activo
                         && solicitud.IdEstado == EstadosEntidad.Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCaso = caso.IdCaso,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCaso = caso.Descripcion,
                             DescripcionSolicitud = solicitud.Descripcion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         where ((oportunidad.Descripcion.Contains(request.DescripcionOportunidad) || string.IsNullOrEmpty(request.DescripcionOportunidad))
                         && (oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && oportunidad.IdEstado == EstadosEntidad.Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join etapa in context.Set<EtapaOportunidad>() on oportunidad.IdEtapa equals etapa.IdEtapa
                         join cliente in context.Set<Cliente>() on oportunidad.IdCliente equals cliente.IdCliente
                         join estadoCaso in context.Set<EstadoCaso>() on caso.IdEstadoCaso equals estadoCaso.IdEstadoCaso
                         where ((oportunidad.Descripcion.Contains(request.DescripcionOportunidad) || string.IsNullOrEmpty(request.DescripcionOportunidad))
                         && oportunidad.IdEstado == EstadosEntidad.Activo
                         && caso.IdEstado == EstadosEntidad.Activo
                         && etapa.IdEstado == EstadosEntidad.Activo
                         && cliente.IdEstado == EstadosEntidad.Activo
                         && estadoCaso.IdEstado == EstadosEntidad.Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCliente = cliente.Descripcion,
                             PorcentajeCierre = oportunidad.PorcentajeCierre,
                             PorcentajeCheckList = oportunidad.PorcentajeCheckList,
                             DescripcionEstadoCaso = estadoCaso.Descripcion,
                             DescripcionEtapa = etapa.Descripcion,
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPorCodigoPaginado(OportunidadSTDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join solicitud in context.Set<TipoSolicitud>() on caso.IdTipoSolicitud equals solicitud.IdTipoSolicitud
                         where ((oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                         && (caso.IdCasoSF.Contains(request.IdCasoSF) || string.IsNullOrEmpty(request.IdCasoSF))
                         && oportunidad.IdEstado == EstadosEntidad.Activo
                         && caso.IdEstado == EstadosEntidad.Activo
                         && solicitud.IdEstado == EstadosEntidad.Activo)
                         orderby oportunidad.FechaCreacion descending
                         select new OportunidadSTDtoResponse
                         {
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCaso = caso.IdCaso,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCaso = caso.Descripcion,
                             DescripcionSolicitud = solicitud.Descripcion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTPaginadoDtoResponse
            {
                ListOportunidadSTDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request)
        {
            var query = (from oportunidad in context.Set<OportunidadST>()
                         join caso in context.Set<CasoOportunidad>() on oportunidad.IdOportunidad equals caso.IdOportunidad
                         join solicitud in context.Set<TipoSolicitud>() on caso.IdTipoSolicitud equals solicitud.IdTipoSolicitud
                         join etapa in context.Set<EtapaOportunidad>() on oportunidad.IdEtapa equals etapa.IdEtapa
                         join cliente in context.Set<Cliente>() on oportunidad.IdCliente equals cliente.IdCliente
                         join estadoCaso in context.Set<EstadoCaso>() on caso.IdEstadoCaso equals estadoCaso.IdEstadoCaso
                         join segmentoNegocio in context.Set<SegmentoNegocio>() on oportunidad.IdSegmentoNegocio equals segmentoNegocio.IdSegmentoNegocio
                         join tipoOportunidad in context.Set<TipoOportunidad>() on oportunidad.IdTipoOportunidad equals tipoOportunidad.IdTipoOportunidad
                         join maestraComplejidad in context.Set<Maestra>().Where(x => x.IdRelacion == 160) on caso.Complejidad equals maestraComplejidad.Valor
                         join maestraTipoEntidad in context.Set<Maestra>().Where(x => x.IdRelacion == 168) on caso.Complejidad equals maestraTipoEntidad.Valor
                         join datosPreventa in context.Set<DatosPreventaMovil>() on oportunidad.IdOportunidad equals datosPreventa.IdOportunidad
                         where ((oportunidad.Descripcion.Contains(request.DescripcionOportunidad) || string.IsNullOrEmpty(request.DescripcionOportunidad))
                         && (cliente.Descripcion.Contains(request.DescripcionCliente) || string.IsNullOrEmpty(request.DescripcionCliente))
                         && (etapa.IdEtapa == request.IdEtapa || request.IdEtapa == -1)
                         && (segmentoNegocio.IdSegmentoNegocio == request.IdSegmentoNegocio || request.IdSegmentoNegocio == -1)
                         && (estadoCaso.IdEstadoCaso == request.IdEstadoCaso || request.IdEstadoCaso == -1)
                         && (tipoOportunidad.IdTipoOportunidad == request.IdTipoOportunidad || request.IdTipoOportunidad == -1)
                         && (maestraComplejidad.Valor == request.Codigo.ToString() || request.Codigo == -1)
                         && (oportunidad.PorcentajeCierre == request.PorcentajeCierre || string.IsNullOrEmpty(request.PorcentajeCierre.ToString()))
                         && oportunidad.IdEstado == EstadosEntidad.Activo
                         && caso.IdEstado == EstadosEntidad.Activo
                         && solicitud.IdEstado == EstadosEntidad.Activo
                         && etapa.IdEstado == EstadosEntidad.Activo
                         && cliente.IdEstado == EstadosEntidad.Activo
                         && estadoCaso.IdEstado == EstadosEntidad.Activo
                         && segmentoNegocio.IdEstado == EstadosEntidad.Activo
                         && tipoOportunidad.IdEstado == EstadosEntidad.Activo
                         && maestraComplejidad.IdEstado == EstadosEntidad.Activo
                         && maestraTipoEntidad.IdEstado == EstadosEntidad.Activo
                         && datosPreventa.IdEstado == EstadosEntidad.Activo
                         && caso.IdCaso == datosPreventa.IdCaso
                         )
                         orderby oportunidad.FechaCreacion descending
                         select new 
                         {
                             IdDatosPreventaMovil = oportunidad.IdOportunidad,
                             IdOportunidad = oportunidad.IdOportunidad,
                             IdCaso = oportunidad.IdOportunidad,
                             IdOportunidadSF = oportunidad.IdOportunidadSF,
                             DescripcionOportunidad = oportunidad.Descripcion,
                             IdCasoSF = caso.IdCasoSF,
                             DescripcionCliente = cliente.Descripcion,
                             Asunto = caso.Asunto,
                             DescripcionEstadoCaso = estadoCaso.Descripcion,
                             DescripcionEtapa = etapa.Descripcion,
                             DescripcionTipoOportunidad = tipoOportunidad.Descripcion,
                             DescripcionTipoSolicitud = solicitud.Descripcion,
                             ProbalidadExito = oportunidad.ProbalidadExito,
                             FechaApertura = oportunidad.FechaApertura,
                             FechaCierre = oportunidad.FechaCierre,
                             DescripcionTipoEntidadCliente = maestraTipoEntidad.Descripcion,
                             ImporteCapex = oportunidad.ImporteCapex,
                             DescripcionSegmento = segmentoNegocio.Descripcion,
                             DescripcionComplejidad = maestraComplejidad.Descripcion
                         }).ToList()
                         .Select(x => new OportunidadSTSeguimientoPreventaDtoResponse
                         {
                             IdDatosPreventaMovil = x.IdOportunidad,
                             IdOportunidad = x.IdOportunidad,
                             IdCaso = x.IdOportunidad,
                             IdOportunidadSF = x.IdOportunidadSF,
                             DescripcionOportunidad = x.DescripcionOportunidad,
                             IdCasoSF = x.IdCasoSF,
                             DescripcionCliente = x.DescripcionCliente,
                             Asunto = x.Asunto,
                             DescripcionEstadoCaso = x.DescripcionEstadoCaso,
                             DescripcionEtapa = x.DescripcionEtapa,
                             DescripcionTipoOportunidad = x.DescripcionTipoOportunidad,
                             DescripcionTipoSolicitud = x.DescripcionTipoSolicitud,
                             ProbalidadExito = x.ProbalidadExito,
                             FechaApertura = x.FechaApertura,
                             FechaCierre = x.FechaCierre,
                             strFechaApertura = x.FechaApertura != null ? Funciones.FormatoFechaHora(x.FechaApertura.Value) : "",
                             strFechaCierre = x.FechaCierre != null ? Funciones.FormatoFechaHora(x.FechaCierre.Value) : "",
                             DescripcionTipoEntidadCliente = x.DescripcionTipoEntidadCliente,
                             ImporteCapex = x.ImporteCapex,
                             DescripcionSegmento = x.DescripcionSegmento,
                             DescripcionComplejidad = x.DescripcionComplejidad
                         }).Distinct().OrderBy(x => x.IdDatosPreventaMovil).AsQueryable().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new OportunidadSTSeguimientoPreventaPaginadoDtoResponse
            {
                ListOportunidadSTSeguimientoPreventaDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }
    }
}
