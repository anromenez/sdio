﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
using Tgs.SDIO.Util.Funciones;


namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class ObservacionOportunidadDal : Repository<ObservacionOportunidad>, IObservacionOportunidadDal
    {
        readonly DioContext context;
        public ObservacionOportunidadDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request)
        {
                var query = (from observacion in context.Set<ObservacionOportunidad>()
                             join oportunidad in context.Set<OportunidadST>() on observacion.IdOportunidad equals oportunidad.IdOportunidad
                             join concepto in context.Set<ConceptoSeguimiento>() on observacion.IdConcepto equals concepto.IdConcepto
                             join caso in context.Set<CasoOportunidad>() on observacion.IdOportunidad equals caso.IdOportunidad
                             where (((observacion.IdConcepto == request.IdConcepto) || (request.IdConcepto == -1))
                             && (oportunidad.IdOportunidadSF.Contains(request.IdOportunidadSF) || string.IsNullOrEmpty(request.IdOportunidadSF))
                             && observacion.IdEstado == EstadosEntidad.Activo
                             && oportunidad.IdEstado == EstadosEntidad.Activo
                             && concepto.IdEstado == EstadosEntidad.Activo
                             && caso.IdEstado == EstadosEntidad.Activo
                             && caso.IdCaso == observacion.IdCaso
                             && (observacion.IdEstado == EstadosEntidad.Activo || observacion.IdEstado == EstadosEntidad.Inactivo))
                             orderby observacion.FechaCreacion descending
                             select new 
                             {
                                 IdObservacion = observacion.IdObservacion,
                                 IdOportunidadSF = oportunidad.IdOportunidadSF,
                                 IdCasoSF = caso.IdCasoSF,
                                 FechaSeguimiento = observacion.FechaSeguimiento,
                                 IdSeguimiento = observacion.IdSeguimiento,
                                 DescripcionConcepto = concepto.Descripcion,
                                 Observaciones = observacion.Observaciones,
                                 Estado = observacion.IdEstado == EstadosEntidad.Activo ? "Activo" : "Inactivo"
                             }).ToList()
                            .Select(x => new ObservacionOportunidadDtoResponse
                            {
                                IdObservacion = x.IdObservacion,
                                IdOportunidadSF = x.IdOportunidadSF,
                                IdCasoSF = x.IdCasoSF,
                                strFechaSeguimiento = x.FechaSeguimiento != null ? Funciones.FormatoFecha(x.FechaSeguimiento.Value) : "",
                                IdSeguimiento = x.IdSeguimiento,
                                DescripcionConcepto = x.DescripcionConcepto,
                                Observaciones = x.Observaciones,
                                Estado = x.Estado
                            }).Distinct().OrderBy(x => x.IdObservacion).AsQueryable().ToPagedList(request.Indice, request.Tamanio);
                             
                             
                   

                var datosCarga = new ObservacionOportunidadPaginadoDtoResponse
                {
                    ListObservacionOportunidadDto = query.ToList(),
                    TotalItemCount = query.TotalItemCount
                };

                return datosCarga;
        }

    }
}
