﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class DatosPreventaMovilDal : Repository<DatosPreventaMovil>, IDatosPreventaMovilDal
    {
        readonly DioContext context;
        public DatosPreventaMovilDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
    }
}
