﻿using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class ActividadSegmentoNegocioDal : Repository<ActividadSegmentoNegocio>, IActividadSegmentoNegocioDal
    {
        readonly DioContext context;

        public ActividadSegmentoNegocioDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }
    }
}
