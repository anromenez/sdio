﻿using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using System;
using System.Data.Entity;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using Tgs.SDIO.Util.Paginacion;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad
{
   public class RecursoDal : Repository<Recurso>, IRecursoDal
    {
        readonly DioContext context;
        public RecursoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request)
        {
            var query = (from recurso in context.Set<Recurso>()
                         where ((recurso.Nombre.Contains(request.Nombre) || string.IsNullOrEmpty(request.Nombre))
                         && (recurso.IdEstado == EstadosEntidad.Activo || recurso.IdEstado == EstadosEntidad.Inactivo)
                         && ((request.TipoRecurso==-1) || (recurso.TipoRecurso == request.TipoRecurso)))
                         orderby recurso.FechaCreacion descending
                         select new RecursoDtoResponse
                         {
                             IdRecurso = recurso.IdRecurso,
                             TipoRecurso = recurso.TipoRecurso,
                             UserNameSF = recurso.UserNameSF,
                             Nombre = recurso.Nombre,
                             DNI = recurso.DNI,
                             IdEstado = recurso.IdEstado,
                             Estado = recurso.IdEstado == EstadosEntidad.Activo ? "Activo" : "Inactivo",
                             IdUsuarioCreacion = recurso.IdUsuarioCreacion,
                             FechaCreacion = recurso.FechaCreacion,
                             IdUsuarioEdicion = recurso.IdUsuarioEdicion,
                             FechaEdicion = recurso.FechaEdicion
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new RecursoPaginadoDtoResponse
            {
                ListRecursoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;
        }

        public RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            var query = (from recurso in context.Set<Recurso>()
                         where ((recurso.Nombre.Contains(request.Nombre) || string.IsNullOrEmpty(request.Nombre))
                         && (recurso.IdEstado == EstadosEntidad.Activo || recurso.IdEstado == EstadosEntidad.Inactivo))
                         orderby recurso.FechaCreacion descending
                         select new RecursoDtoResponse
                         {
                             IdRecurso = recurso.IdRecurso,                          
                             Nombre = recurso.Nombre,                           
                             Estado = recurso.IdEstado == EstadosEntidad.Activo ? "Activo" : "Inactivo"                          
                         }).AsNoTracking().ToPagedList(request.Indice, request.Tamanio);

            var datosCarga = new RecursoPaginadoDtoResponse
            {
                ListRecursoDto = query.ToList(),
                TotalItemCount = query.TotalItemCount
            };

            return datosCarga;

        }
    }
}
