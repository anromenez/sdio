﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;
using Tgs.SDIO.Entities.Entities.Funnel;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Funnel
{
    public class OportunidadFinancieraOrigenDal : Repository<OportunidadFinancieraOrigen>, IOportunidadFinancieraOrigenDal
    {
        readonly DioContext context;

        public OportunidadFinancieraOrigenDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public IEnumerable<OportunidadFinancieraOrigenDtoResponse> ListarIndicadorMadurez(IndicadorMadurezDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = request.IdLineaNegocio ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idSector = new SqlParameter { ParameterName = "IdSector", Value = request.IdSector ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };


            var listaIndicadores =
                context.ExecuteQuery<OportunidadFinancieraOrigenDtoResponse>("[FUNNEL].[USP_INDICADORMADUREZCONSULTAR] @Anio,@Mes,@IdLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa",
                anio, mes, idLineaNegocio, idSector, probabilidadExito, etapa).ToList().Where(x => x.Madurez != 0);

            return listaIndicadores;
        }

        public IEnumerable<DashoardOfertasLineaNegocioDtoResponse> ListarIndicadorOfertasLineaNegocio(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaOfertasLineaNegocio =
               context.ExecuteQuery<DashoardOfertasLineaNegocioDtoResponse>("[FUNNEL].[USP_INDICADOR_LINEANEGOCIO_CONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
               anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaOfertasLineaNegocio;

        }

        public IEnumerable<DashoardOfertasSectorDtoResponse> ListarIndicadorOfertasSector(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaOfertasSector =
             context.ExecuteQuery<DashoardOfertasSectorDtoResponse>("[FUNNEL].[USP_INDICADORSECTORCONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
             anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaOfertasSector;
        }

        public IEnumerable<DashoardIngresosLineaNegocioDtoResponse> ListarIndicadorIngresosLineaNegocio(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaIngresosLineaNegocio =
                 context.ExecuteQuery<DashoardIngresosLineaNegocioDtoResponse>("[FUNNEL].[USP_INDICADOR_INGRESOLINEA_CONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
             anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaIngresosLineaNegocio;
        }

        public IEnumerable<DashoardIngresosSectorDtoResponse> ListarIndicadorIngresosSector(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var etapa = new SqlParameter { ParameterName = "Etapa", Value = request.Etapa ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaIngresosSector =
                 context.ExecuteQuery<DashoardIngresosSectorDtoResponse>("[FUNNEL].[USP_INDICADOR_INGRESOSECTOR_CONSULTAR] @Anio,@Mes,@Etapa,@ProbabilidadExito,@Madurez",
             anio, mes, etapa, probabilidadExito, madurez).ToList();

            return listaIngresosSector;

        }

        public IEnumerable<DashoardOfertasEstadoDtoResponse> ListarIndicadorOfertasEstado(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idLineaNegocio = new SqlParameter { ParameterName = "IdLineaNegocio", Value = request.IdLineaNegocio ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var idSector = new SqlParameter { ParameterName = "IdSector", Value = request.IdSector ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var probabilidadExito = new SqlParameter { ParameterName = "ProbabilidadExito", Value = request.ProbabilidadExito ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };
            var madurez = new SqlParameter { ParameterName = "Madurez", Value = request.Madurez ?? (object)DBNull.Value, SqlDbType = SqlDbType.Char };

            var listaOfertasEstado =
                context.ExecuteQuery<DashoardOfertasEstadoDtoResponse>("[FUNNEL].[USP_INDICADOR_OFERTASESTADO_CONSULTAR] @Anio,@Mes,@IdLineaNegocio,@IdSector,@ProbabilidadExito,@Madurez",
            anio, mes, idLineaNegocio, idSector, probabilidadExito, madurez).ToList();

            return listaOfertasEstado;

        }


        public IEnumerable<DashboardTotalesDtoResponse> ListarIndicadoresTotales(DashoardDtoRequest request)
        {
            var anio = new SqlParameter { ParameterName = "Anio", Value = request.Anio, SqlDbType = SqlDbType.Int };
            var mes = new SqlParameter { ParameterName = "Mes", Value = request.Mes ?? (object)DBNull.Value, SqlDbType = SqlDbType.Int };

            var totalesDashboard =
                context.ExecuteQuery<DashboardTotalesDtoResponse>("[FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALPIE] @Anio,@Mes",
            anio, mes).ToList();

            return totalesDashboard;

        }
    }
}


