﻿

using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{
    public class CartaFianzaColaboradorDetalleDal : Repository<CartaFianzaColaboradorDetalle>, ICartaFianzaColaboradorDetalleDal
    {
        readonly DioContext context;
        public CartaFianzaColaboradorDetalleDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public CartaFianzaColaboradorDetalleResponse ActualizaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestActualiza)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaColaboradorDetalleResponse InsertaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestInserta)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaColaboradorDetalleResponse> ListaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaColaboradorDetalleListaResponse> ListaCartaFianzaColaboradorDetalleId(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList)
        {
         /*   var registroCartaFianzaColaboradorDetalleId = (
                            from cfcd in context.Set<CartaFianzaColaboradorDetalle>()
                            join col in context.Set<Colaborador>() on cfcd.IdCartaFianza equals col.IdCartaFianza
                            where
                                   cfcd.IdCartaFianza == CartaFianzaColaboradorDetalleRequestLista.IdCartaFianza

                            select new CartaFianzaColaboradorDetalleListadoResponse
                            {
                                IdCartaFianzaColaboradorDetalle = cfcd.IdCartaFianzaColaboradorDetalle,
                                IdCartaFianza = cfcd.IdCartaFianza,
                                IdColaborador = cfcd.IdColaborador,
                                Colaborador = col.NombreCompleto,
                                IdEstado = cfcd.IdEstado,
                                IdUsuarioCreacion = cfcd.IdUsuarioCreacion,
                                FechaCreacion = cfcd.FechaCreacion,
                                IdUsuarioEdicion = cfcd.IdUsuarioEdicion,
                                FechaEdicion = cfcd.FechaEdicion

                            }).ToList<CartaFianzaColaboradorDetalleListaResponse>();

            return registroCartaFianzaColaboradorDetalleId;*/

            throw new NotImplementedException();

        }
    }
}