﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{

  
    public class CartaFianzaDetalleRenovacionDal:Repository<CartaFianzaDetalleRenovacion>, ICartaFianzaDetalleRenovacionDal
    {
        readonly DioContext context;
        public CartaFianzaDetalleRenovacionDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public CartaFianzaDetalleRenovacionResponse ActualizaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionActualiza)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaDetalleRenovacionResponse> InsertaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionRequestInserta)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaDetalleRenovacionResponse ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionLista)
        {
            throw new NotImplementedException();
        }
    }
}
