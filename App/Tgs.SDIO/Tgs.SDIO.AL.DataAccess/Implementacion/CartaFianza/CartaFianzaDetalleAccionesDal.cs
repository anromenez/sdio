﻿using System;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{

    public class CartaFianzaDetalleAccionesDal:Repository<CartaFianzaDetalleAcciones>, ICartaFianzaDetalleAccionDal
    {
        readonly DioContext context;
        public CartaFianzaDetalleAccionesDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public CartaFianzaDetalleAccionResponse ActualizaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionesRequestActualiza)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaDetalleAccionResponse> InsertaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionesRequestInserta)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaDetalleAccionResponse ListaCartaFianzaDetalleAccion(CartaFianzaDetalleAccionRequest cartaFianzaDetalleAccionesRequestLista)
        {
            throw new NotImplementedException();
        }
    }
}
