﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{

   
    public class CartaFianzaDetalleRecuperoDal:Repository<CartaFianzaDetalleRecupero>, ICartaFianzaDetalleRecuperoDal
    {

        readonly DioContext context;
        public CartaFianzaDetalleRecuperoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public CartaFianzaDetalleRecuperoResponse ActualizaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoActualiza)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaDetalleRecuperoResponse> InsertaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoRequestInserta)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaDetalleRecuperoResponse ListaCartaFianzaDetalleRecupero(CartaFianzaDetalleRecuperoRequest CartaFianzaDetalleRecuperoLista)
        {
            throw new NotImplementedException();
        }
    }
}
