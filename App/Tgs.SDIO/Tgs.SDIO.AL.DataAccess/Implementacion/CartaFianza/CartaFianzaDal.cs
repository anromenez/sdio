﻿using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{
    public class CartaFianzaDal:Repository<CartaFianzaMaestro>, ICartaFianzaDal
    {
        readonly DioContext context;
        public CartaFianzaDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }


        public List<CartaFianzaListaResponse> ListaCartaFianza(CartaFianzaRequest cartaFianzaRequestLista)
        {
            var registroCartaFianza = (from cf in context.Set<CartaFianzaMaestro>()
                                       join c in context.Set<Cliente>() on cf.IdCliente equals c.IdCliente
                                       where (( cartaFianzaRequestLista.NombreCliente == null) || (c.Descripcion == cartaFianzaRequestLista.NombreCliente))
                                               &&
                                              ((cartaFianzaRequestLista.NumeroContrato == null) || (cf.NumeroContrato == cartaFianzaRequestLista.NumeroContrato))
                                              &&
                                              ((cartaFianzaRequestLista.NumeroIdentificadorFiscal == null) || (c.NumeroIdentificadorFiscal == cartaFianzaRequestLista.NumeroIdentificadorFiscal))
                                       orderby cf.FechaFinServicioContrato descending

                                       select new CartaFianzaListaResponse
                                       {
                                           IdCartaFianza = cf.IdCartaFianza,
                                           IdCliente = c.IdCliente,
                                           NombreCliente = c.Descripcion,
                                           NumeroOportunidad = cf.NumeroOportunidad,
                                           IdTipoContratoTm = cf.IdTipoContratoTm,
                                           NumeroContrato = cf.NumeroContrato,
                                           Processo = cf.Processo,
                                           DescripcionServicioCartaFianza = cf.DescripcionServicioCartaFianza,
                                           FechaFirmaServicioContrato = cf.FechaFirmaServicioContrato,
                                           FechaFinServicioContrato = cf.FechaFinServicioContrato,
                                           IdEmpresaAdjudicadaTm = cf.IdEmpresaAdjudicadaTm,
                                           IdTipoGarantiaTm = cf.IdTipoGarantiaTm,
                                           NumeroGarantia = cf.NumeroGarantia,
                                           IdTipoMonedaTm = cf.IdTipoMonedaTm,
                                           ImporteCartaFianzaSoles = cf.ImporteCartaFianzaSoles,
                                           ImporteCartaFianzaDolares = cf.ImporteCartaFianzaDolares,
                                           IdBanco = cf.IdBanco,

                                           ClienteEspecial = cf.ClienteEspecial,
                                           SeguimientoImportante = cf.SeguimientoImportante,
                                           Observacion = cf.Observacion,
                                           Incidencia = cf.Incidencia,
                                           IdEstado = cf.IdEstado,
                                           IdUsuarioCreacion = cf.IdUsuarioCreacion,
                                           FechaCreacion = cf.FechaCreacion,
                                           IdUsuarioEdicion = cf.IdUsuarioEdicion,
                                           FechaEdicion = cf.FechaEdicion
                                           //  
                                       }
                                            ).ToList()//<CartaFianzaListaResponse>();
                                            .Select(x => new CartaFianzaListaResponse
                                            {
                                                IdCartaFianza = x.IdCartaFianza,
                                                IdCliente = x.IdCliente,
                                                NombreCliente = x.NombreCliente,
                                                NumeroOportunidad = x.NumeroOportunidad,
                                                IdTipoContratoTm = x.IdTipoContratoTm,
                                                NumeroContrato = x.NumeroContrato,
                                                Processo = x.Processo,
                                                DescripcionServicioCartaFianza = x.DescripcionServicioCartaFianza,
                                                FechaFirmaServicioContratostr = Funciones.FormatoFecha(x.FechaFirmaServicioContrato),
                                                FechaFinServicioContratostr = Funciones.FormatoFecha(x.FechaFinServicioContrato),
                                                 IdEmpresaAdjudicadaTm = x.IdEmpresaAdjudicadaTm,
                                                 IdTipoGarantiaTm = x.IdTipoGarantiaTm,
                                                 NumeroGarantia = x.NumeroGarantia,
                                                 IdTipoMonedaTm = x.IdTipoMonedaTm,
                                                 ImporteCartaFianzaSoles = x.ImporteCartaFianzaSoles,
                                                 ImporteCartaFianzaDolares = x.ImporteCartaFianzaDolares,
                                                 IdBanco = x.IdBanco,

                                                 ClienteEspecial = x.ClienteEspecial,
                                                 SeguimientoImportante = x.SeguimientoImportante,
                                                 Observacion = x.Observacion,
                                                 Incidencia = x.Incidencia,
                                                 IdEstado = x.IdEstado,
                                                 IdUsuarioCreacion = x.IdUsuarioCreacion,
                                                 FechaCreacion = x.FechaCreacion,
                                                 IdUsuarioEdicion = x.IdUsuarioEdicion,
                                                 FechaEdicion = x.FechaEdicion,
                                                 IdIndicadorSemaforo = DateTime.DaysInMonth(x.FechaFinServicioContrato.Year, x.FechaFinServicioContrato.Month)

                                             }

                                        );

            return registroCartaFianza.ToList();
        }
 

        public List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest cartaFianzaRequestLista)
        {



            var registroCartaFianzaId = (
                          from cf in context.Set<CartaFianzaMaestro>()
                          join c in context.Set<Cliente>() on cf.IdCliente equals c.IdCliente                        
                          where    
                                       cf.IdCartaFianza == cartaFianzaRequestLista.IdCartaFianza
                                       && cf.IdCliente == cartaFianzaRequestLista.IdCliente
                          orderby cf.FechaFinServicioContrato descending

                          select new CartaFianzaListaResponse
                               {
                              IdCartaFianza = cf.IdCartaFianza,
                              IdCliente = c.IdCliente,
                              NombreCliente = c.Descripcion,
                              NumeroOportunidad = cf.NumeroOportunidad,
                              IdTipoContratoTm = cf.IdTipoContratoTm,
                              NumeroContrato = cf.NumeroContrato,
                              Processo = cf.Processo,
                              DescripcionServicioCartaFianza = cf.DescripcionServicioCartaFianza,
                              FechaFirmaServicioContrato = cf.FechaFirmaServicioContrato,
                              FechaFinServicioContrato = cf.FechaFinServicioContrato,
                              IdEmpresaAdjudicadaTm = cf.IdEmpresaAdjudicadaTm,
                              IdTipoGarantiaTm = cf.IdTipoGarantiaTm,
                              NumeroGarantia = cf.NumeroGarantia,
                              IdTipoMonedaTm = cf.IdTipoMonedaTm,
                              ImporteCartaFianzaSoles = cf.ImporteCartaFianzaSoles,
                              ImporteCartaFianzaDolares = cf.ImporteCartaFianzaDolares,
                              IdBanco = cf.IdBanco,

                              ClienteEspecial = cf.ClienteEspecial,
                              SeguimientoImportante = cf.SeguimientoImportante,
                              Observacion = cf.Observacion,
                              Incidencia = cf.Incidencia,
                              IdEstado = cf.IdEstado,
                              IdUsuarioCreacion = cf.IdUsuarioCreacion,
                              FechaCreacion = cf.FechaCreacion,
                              IdUsuarioEdicion = cf.IdUsuarioEdicion,
                              FechaEdicion = cf.FechaEdicion
                          }
                                            ).ToList()//<CartaFianzaListaResponse>();
                                            .Select(x => new CartaFianzaListaResponse
                                            {
                                                IdCartaFianza = x.IdCartaFianza,
                                                IdCliente = x.IdCliente,
                                                NombreCliente = x.NombreCliente,
                                                NumeroOportunidad = x.NumeroOportunidad,
                                                IdTipoContratoTm = x.IdTipoContratoTm,
                                                NumeroContrato = x.NumeroContrato,
                                                Processo = x.Processo,
                                                DescripcionServicioCartaFianza = x.DescripcionServicioCartaFianza,
                                                FechaFirmaServicioContratostr = Funciones.FormatoFecha(x.FechaFirmaServicioContrato),
                                                FechaFinServicioContratostr = Funciones.FormatoFecha(x.FechaFinServicioContrato),
                                                IdEmpresaAdjudicadaTm = x.IdEmpresaAdjudicadaTm,
                                                IdTipoGarantiaTm = x.IdTipoGarantiaTm,
                                                NumeroGarantia = x.NumeroGarantia,
                                                IdTipoMonedaTm = x.IdTipoMonedaTm,
                                                ImporteCartaFianzaSoles = x.ImporteCartaFianzaSoles,
                                                ImporteCartaFianzaDolares = x.ImporteCartaFianzaDolares,
                                                IdBanco = x.IdBanco,

                                                ClienteEspecial = x.ClienteEspecial,
                                                SeguimientoImportante = x.SeguimientoImportante,
                                                Observacion = x.Observacion,
                                                Incidencia = x.Incidencia,
                                                IdEstado = x.IdEstado,
                                                IdUsuarioCreacion = x.IdUsuarioCreacion,
                                                FechaCreacion = x.FechaCreacion,
                                                IdUsuarioEdicion = x.IdUsuarioEdicion,
                                                FechaEdicion = x.FechaEdicion,
                                                IdIndicadorSemaforo = DateTime.DaysInMonth(x.FechaFinServicioContrato.Year, x.FechaFinServicioContrato.Month)

                                            }

                                        );

            return registroCartaFianzaId.ToList();


        }


    }

}
