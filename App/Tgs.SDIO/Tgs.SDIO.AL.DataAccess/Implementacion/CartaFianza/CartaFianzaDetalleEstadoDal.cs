﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.AL.DataAccess.Core;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza
{

    
    public class CartaFianzaDetalleEstadoDal:Repository<CartaFianzaDetalleEstado>, ICartaFianzaDetalleEstadoDal
    {
        readonly DioContext context;
        public CartaFianzaDetalleEstadoDal(DioContext unitOfWork) : base(unitOfWork)
        {
            context = UnitOfWork as DioContext;
        }

        public CartaFianzaDetalleEstadoResponse ActualizaCartaFianzaDetalleEstado(CartaFianzaDetalleEstadoRequest CartaFianzaDetalleEstadoequestActualiza)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaDetalleEstadoResponse> InsertaCartaFianzaDetalleEstado(CartaFianzaDetalleEstadoRequest CartaFianzaDetalleEstadoRequestInserta)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaDetalleEstadoResponse ListaCartaFianzaDetalleEstado(CartaFianzaDetalleEstadoRequest CartaFianzaDetalleEstadoequestLista)
        {
            throw new NotImplementedException();
        }
    }
}
