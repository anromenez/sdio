﻿using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
    public interface IFaseOportunidadBl
    {
        List<ListaDtoResponse> ListarComboFaseOportunidad();
    }
}
