﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
    public interface ISegmentoNegocioBl
    {
        List<ComboDtoResponse> ListarComboSegmentoNegocio();

        List<ListaDtoResponse> ListarComboSegmentoNegocioSimple();
    }
}
