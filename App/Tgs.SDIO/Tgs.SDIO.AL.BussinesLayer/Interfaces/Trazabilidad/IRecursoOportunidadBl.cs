﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface IRecursoOportunidadBl
    {
        RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request);

        RecursoOportunidadDtoResponse ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request);

        ProcesoResponse ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request);

        ProcesoResponse RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request);

        ProcesoResponse EliminarRecursoOportunidad(RecursoOportunidadDtoRequest request);

        ProcesoResponse RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request);
    }
}
