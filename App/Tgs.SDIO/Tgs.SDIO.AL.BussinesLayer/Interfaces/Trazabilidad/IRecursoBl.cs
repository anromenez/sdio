﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface IRecursoBl
    {
        RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request);

        RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request);

        ProcesoResponse ActualizarRecurso(RecursoDtoRequest request);

        ProcesoResponse RegistrarRecurso(RecursoDtoRequest request);

        ProcesoResponse EliminarRecurso(RecursoDtoRequest request);

        RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request);
    }
}
