﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface IDatosPreventaMovilBl
    {
        DatosPreventaMovilDtoResponse ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request);

        ProcesoResponse ActualizarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);

        ProcesoResponse RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);

        ProcesoResponse EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
    }
}
