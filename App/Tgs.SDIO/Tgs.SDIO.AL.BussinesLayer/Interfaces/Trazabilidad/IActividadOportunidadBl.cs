﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
    public interface IActividadOportunidadBl
    {
        ActividadOportunidadPaginadoDtoResponse ListatActividadOportunidadPaginado(ActividadOportunidadDtoRequest request);

        ActividadOportunidadDtoResponse ObtenerActividadOportunidadPorId(ActividadOportunidadDtoRequest request);

        ProcesoResponse ActualizarActividadOportunidad(ActividadOportunidadDtoRequest request);

        ProcesoResponse RegistrarActividadOportunidad(ActividadOportunidadDtoRequest request);

        ProcesoResponse EliminarActividadOportunidad(ActividadOportunidadDtoRequest request);

        List<ListaDtoResponse> ListarComboActividadPadres();

    }
}
