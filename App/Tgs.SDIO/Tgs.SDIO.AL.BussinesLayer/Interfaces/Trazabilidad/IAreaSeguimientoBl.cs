﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface IAreaSeguimientoBl
    {
        AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request);

        AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest request);

        ProcesoResponse ActualizarAreaSeguimiento(AreaSeguimientoDtoRequest request);

        ProcesoResponse RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest request);

        ProcesoResponse EliminarAreaSeguimiento(AreaSeguimientoDtoRequest request);

        List<ListaDtoResponse> ListarComboAreaSeguimiento();
    }
}
