﻿using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad
{
   public interface ICasoOportunidadBl
    {
        CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request);

    }
}
