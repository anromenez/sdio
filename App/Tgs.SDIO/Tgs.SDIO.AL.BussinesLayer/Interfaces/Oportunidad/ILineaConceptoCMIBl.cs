﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface ILineaConceptoCMIBl
    {
        List<LineaConceptoCMIDtoResponse> ListarConceptosPorLinea(LineaConceptoCMIDtoRequest linea);
        List<LineaConceptoCMIDtoResponse> ListarLineaConceptoCMI(LineaConceptoCMIDtoRequest linea);
    }
}
