﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad
{
    public interface IOportunidadFlujoCajaBl
    {
        ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja);
        List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja);
        List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);
        ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio);
        List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);
        OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio);

        ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja);

    }
}
