﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza
{
    public interface ICartaFianzaDetalleRenovacionBl
    {

        List<CartaFianzaDetalleRenovacionResponse> InsertaCartaFianzaDetalleRenovacion(List<CartaFianzaDetalleRenovacionRequest> CartaFianzaDetalleRenovacionRequestInserta);
        CartaFianzaDetalleRenovacionResponse ActualizaCartaFianzaDetalleRenovacion(List<CartaFianzaDetalleRenovacionRequest> CartaFianzaDetalleRenovacionActualiza);
        CartaFianzaDetalleRenovacionResponse ListaCartaFianzaDetalleRenovacion(List<CartaFianzaDetalleRenovacionRequest> CartaFianzaDetalleRenovacionLista);

    }
}
