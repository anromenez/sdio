﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza
{
    public interface ICartaFianzaDetalleAccionBl
    {

        List<CartaFianzaDetalleAccionResponse> InsertaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> CartaFianzaDetalleAccionRequestInserta);
        CartaFianzaDetalleAccionResponse ActualizaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> CartaFianzaDetalleAccionRequestActualiza);
        CartaFianzaDetalleAccionResponse ListaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> CartaFianzaDetalleAccionRequestLista);

    }
}
