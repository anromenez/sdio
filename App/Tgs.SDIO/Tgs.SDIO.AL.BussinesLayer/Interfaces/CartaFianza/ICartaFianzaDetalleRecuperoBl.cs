﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza
{
    public interface ICartaFianzaDetalleRecuperoBl
    {
        List<CartaFianzaDetalleRecuperoResponse> InsertaCartaFianzaDetalleRecupero(List<CartaFianzaDetalleRecuperoRequest> CartaFianzaDetalleRecuperoRequestInserta);
        CartaFianzaDetalleRecuperoResponse ActualizaCartaFianzaDetalleRecupero(List<CartaFianzaDetalleRecuperoRequest> CartaFianzaDetalleRecuperoActualiza);
        CartaFianzaDetalleRecuperoResponse ListaCartaFianzaDetalleRecupero(List<CartaFianzaDetalleRecuperoRequest> CartaFianzaDetalleRecuperoLista);

    }
}
