﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel
{
    public interface IOportunidadFinancieraOrigenBl
    {
        IndicadorMadurezDtoResponse ListarIndicadorMadurez(IndicadorMadurezDtoRequest request);
        DashoardDtoResponse MostrarDashboard(DashoardDtoRequest request);
    }
}
