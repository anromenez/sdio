﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IDepreciacionBl
    {
        ProcesoResponse RegistrarDepreciacion(DepreciacionDtoRequest depreciacion);
        ProcesoResponse ActualizarDepreciacion(DepreciacionDtoRequest depreciacion);
        DepreciacionDtoResponse ObtenerDepreciacion(DepreciacionDtoRequest depreciacion);
        List<ListaDtoResponse> ListarDepreciacion(DepreciacionDtoRequest empresaRequest);
    }
}
