﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IMaestraBl
    {
        ProcesoResponse RegistrarMaestra(MaestraDtoRequest maestra);
        ProcesoResponse ActualizarMaestra(MaestraDtoRequest maestra);
        MaestraDtoResponse ObtenerMaestra(MaestraDtoRequest maestra);
        List<ListaDtoResponse> ListarMaestraPorIdRelacion(MaestraDtoRequest maestra);
        List<ComboDtoResponse> ListarComboTiposRecursos();
        List<ListaDtoResponse> ListarMaestra(MaestraDtoRequest maestra);
    }
}

