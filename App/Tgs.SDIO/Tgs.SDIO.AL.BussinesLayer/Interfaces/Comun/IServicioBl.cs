﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun
{
    public interface IServicioBl
    {
        List<ListaDtoResponse> ListarServicios(ServicioDtoRequest servicio);

        ServicioDtoResponse ObtenerServicio(ServicioDtoRequest servicio);

        ProcesoResponse RegistrarServicio(ServicioDtoRequest servicio);

        ProcesoResponse ActualizarServicio(ServicioDtoRequest servicio);

        ServicioPaginadoDtoResponse ListaServicioPaginado(ServicioDtoRequest servicio);

        ProcesoResponse InhabilitarServicio(ServicioDtoRequest servicio);


    }
}
