﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.DataAccess.Interfaces.Comun
{
    public interface IServicioCMIBl
    {
        ProcesoResponse RegistrarServicioCMI(ServicioCMIDtoRequest servicioCMI);
        ProcesoResponse ActualizarServicioCMI(ServicioCMIDtoRequest servicioCMI);

        ServicioCMIDtoResponse ObtenerServicioCMI(ServicioCMIDtoRequest servicioCMI);
        List<ServicioCMIDtoResponse> ListarServicioCMI(ServicioCMIDtoRequest servicioCMI);

        List<ListaDtoResponse> ListarServicioCMIPorLineaNegocio(ServicioCMIDtoRequest servicioCMI);

    }
}
