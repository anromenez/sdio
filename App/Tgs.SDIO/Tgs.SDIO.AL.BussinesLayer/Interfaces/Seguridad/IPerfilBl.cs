﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad
{
    public interface IPerfilBl
    {
        List<PerfilDtoResponse> ObtenerPerfiles(int idUsuarioSistema);
    }
}
