﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad
{
    public interface IOpcionMenuBl
    { 
        List<OpcionMenuResponse> ListarOpcionesUsuario(int idUsuarioEmpresa);
    }
}
