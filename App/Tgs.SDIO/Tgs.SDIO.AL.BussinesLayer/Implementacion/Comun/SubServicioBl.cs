﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{
    public class SubServicioBl : ISubServicioBl
    {
        readonly ISubServicioDal iSubServicioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SubServicioBl(ISubServicioDal ISubServicioDal)
        {
            iSubServicioDal = ISubServicioDal;

        }

        public ProcesoResponse ActualizarSubServicio(SubServicioDtoRequest subServicio)
        {



            var objSubServicio = new SubServicio()
            {
                IdSubServicio = subServicio.IdSubServicio,
                Descripcion = subServicio.Descripcion,
                IdTipoSubServicio = subServicio.IdTipoSubServicio,
                //  IdEstado = subServicio.IdEstado,
              
                IdUsuarioEdicion = subServicio.IdUsuarioEdicion,
                FechaEdicion = subServicio.FechaEdicion,
                DescripcionEquivalencia = subServicio.Descripcion,  
                IdDepreciacion = subServicio.IdDepreciacion,
                Orden = Generales.LineasProducto.Datos,
                Negrita = Generales.Numeric.Cero,             
                CostoInstalacion = subServicio.CostoInstalacion,
              
            };


            iSubServicioDal.ActualizarPorCampos(objSubServicio, x => x.IdSubServicio, x => x.Descripcion, x => x.Descripcion, x => x.IdTipoSubServicio,
                x => x.IdUsuarioEdicion, x => x.DescripcionEquivalencia, x => x.IdDepreciacion, x => x.Orden, x => x.Negrita,x=> x.CostoInstalacion);

            iSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.ActualizarSubServicio;


          
            return respuesta;
        }

        public ProcesoResponse InhabilitarSubServicio(SubServicioDtoRequest subServicio)
        {



            var objSubServicio = new SubServicio()
            {
                IdSubServicio = subServicio.IdSubServicio,
                IdEstado = subServicio.IdEstado,
                IdUsuarioEdicion = subServicio.IdUsuarioEdicion,
                FechaEdicion = subServicio.FechaEdicion
            };


            iSubServicioDal.ActualizarPorCampos(objSubServicio,x=>x.IdSubServicio, x => x.IdEstado, x => x.IdUsuarioEdicion, x => x.FechaEdicion);

            iSubServicioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (subServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;



            return respuesta;
        }


        public SubServicioPaginadoDtoResponse ListaSubServicioPaginado(SubServicioDtoRequest subServicio)
        {
            return iSubServicioDal.ListaSubServicioPaginado(subServicio);

        }

        public List<ListaDtoResponse> ListarSubServicios(SubServicioDtoRequest subServicio)
        {
            var query = iSubServicioDal.GetFilteredAsNoTracking(x => x.IdEstado == subServicio.IdEstado
            && subServicio.IdEstado == subServicio.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdSubServicio.ToString(),
                Descripcion = x.Descripcion
            }).ToList();
        }

        public SubServicioDtoResponse ObtenerSubServicio(SubServicioDtoRequest subServicio)
        {
            var objSubServicio = iSubServicioDal.GetFiltered(x => x.IdSubServicio == subServicio.IdSubServicio);
            return objSubServicio.Select(x => new SubServicioDtoResponse
            {
                IdSubServicio = x.IdSubServicio,
                Descripcion = x.Descripcion,
                IdTipoSubServicio = x.IdTipoSubServicio,
                IdDepreciacion = x.IdDepreciacion,
                CostoInstalacion=x.CostoInstalacion,
            }).Single();

        }

        public ProcesoResponse RegistrarSubServicio(SubServicioDtoRequest subServicio)
        {
            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());
            var objSubServicio = new SubServicio()
            {
               
                Descripcion = subServicio.Descripcion,
                DescripcionEquivalencia = subServicio.Descripcion,
                IdTipoSubServicio = subServicio.IdTipoSubServicio,
                IdDepreciacion= subServicio.IdDepreciacion,
                Orden=Generales.LineasProducto.Datos,
                Negrita=Generales.Numeric.Cero,
                IdEstado = Generales.Estados.Activo,
                CostoInstalacion=subServicio.CostoInstalacion,
                IdUsuarioCreacion = subServicio.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(subServicio.FechaCreacion)
            };

            iSubServicioDal.Add(objSubServicio);
            iSubServicioDal.UnitOfWork.Commit();

         
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralComun.RegistrarSubServicio;



            return respuesta;
        }
    }
}
