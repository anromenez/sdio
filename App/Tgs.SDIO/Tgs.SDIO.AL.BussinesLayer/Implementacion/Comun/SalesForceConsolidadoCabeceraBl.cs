﻿using System.Collections.Generic;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun
{

    public class SalesForceConsolidadoCabeceraBl : ISalesForceConsolidadoCabeceraBl
    {
        readonly ISalesForceConsolidadoCabeceraDal _salesForceConsolidadoCabeceraDal;

        public SalesForceConsolidadoCabeceraBl(ISalesForceConsolidadoCabeceraDal salesForceConsolidadoCabeceraDal)
        {
            _salesForceConsolidadoCabeceraDal = salesForceConsolidadoCabeceraDal;
        }
        public List<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera(SalesForceConsolidadoCabecera request)
        {
            if (request.IdOportunidad == "0")
            {
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorValidacionComun.EV00001, null, Generales.Sistemas.Comun);
            }

            if (request.IdOportunidad == "1")
            {
                ExceptionManager.GenerarAppExcepcionReglaNegocio(ErrorNegocioComun.ERN00001, Generales.Sistemas.Comun);
            }

            return _salesForceConsolidadoCabeceraDal.SalesForceConsolidadoCabecera(request);
        }

        public List<SalesForceConsolidadoCabeceraResponse> ListarProbabilidades()
        {
            return _salesForceConsolidadoCabeceraDal.ListarProbabilidad();

        }

    }
}
