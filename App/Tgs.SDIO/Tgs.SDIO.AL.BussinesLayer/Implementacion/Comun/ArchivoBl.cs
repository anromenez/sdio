﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ArchivoBl : IArchivoBl
    {
        readonly IArchivoDal iArchivoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ArchivoBl(IArchivoDal IArchivoDal)
        {
            iArchivoDal = IArchivoDal;
        }

        public ProcesoResponse ActualizarArchivo(ArchivoDtoRequest archivo)
        {
       
                var objArchivo = new Archivo()
                {
                    IdTipoDocumento = archivo.IdTipoDocumento,
                    IdRelacion1 = archivo.IdRelacion1,
                    IdRelacion2 = archivo.IdRelacion2,
                    Descripcion = archivo.Descripcion,
                    NombreArchivo = archivo.NombreArchivo,
                    RutaArchivo = archivo.RutaArchivo,
                    IdEstado = archivo.IdEstado,
                    IdUsuarioEdicion = archivo.IdUsuarioEdicion,
                    FechaEdicion = archivo.FechaEdicion
                };

                iArchivoDal.Modify(objArchivo);
                iArchivoDal.UnitOfWork.Commit();


                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (archivo.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarArchivo : MensajesGeneralComun.ActualizarArchivo;

       
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorArchivo, null, Generales.Sistemas.Comun);
          


            return respuesta;
        }


        public List<ArchivoDtoResponse> ListarArchivo(ArchivoDtoRequest archivo)
        {
            var query = iArchivoDal.GetFilteredAsNoTracking(x => x.IdEstado == archivo.IdEstado).ToList();
            return query.Select(x => new ArchivoDtoResponse
            {
                IdArchivo = x.IdArchivo,
                Descripcion = x.Descripcion,
                IdTipoDocumento = x.IdTipoDocumento,
                IdEstado = x.IdEstado,
                IdRelacion1 = x.IdRelacion1,
                IdRelacion2 = x.IdRelacion2,
                NombreArchivo = x.NombreArchivo,

                RutaArchivo = x.RutaArchivo

    }).ToList();
        }

        public ArchivoDtoResponse ObtenerArchivo(ArchivoDtoRequest archivo)
        {
            var objArchivo = iArchivoDal.GetFiltered(x => x.IdArchivo == archivo.IdArchivo &&
                                                       x.IdEstado == archivo.IdEstado);
            return objArchivo.Select(x => new ArchivoDtoResponse
            {
                IdArchivo = x.IdArchivo,
                Descripcion = x.Descripcion,
                IdTipoDocumento = x.IdTipoDocumento,
                IdEstado = x.IdEstado,
                IdRelacion1 = x.IdRelacion1,
                IdRelacion2 = x.IdRelacion2,
                NombreArchivo = x.NombreArchivo,
                RutaArchivo = x.RutaArchivo
            }).Single();

        }

        public ProcesoResponse RegistrarArchivo(ArchivoDtoRequest archivo)
        {
           
                var objArchivo = new Archivo()
                {
                    IdArchivo = archivo.IdArchivo,
                    Descripcion = archivo.Descripcion,
                    IdTipoDocumento = archivo.IdTipoDocumento,
                    IdEstado = archivo.IdEstado,
                    IdRelacion1 = archivo.IdRelacion1,
                    IdRelacion2 = archivo.IdRelacion2,
                    NombreArchivo = archivo.NombreArchivo,
                    RutaArchivo = archivo.RutaArchivo,
                    IdUsuarioCreacion = archivo.IdUsuarioCreacion,
                    FechaCreacion = archivo.FechaCreacion
                };

                iArchivoDal.Add(objArchivo);
                iArchivoDal.UnitOfWork.Commit();

                respuesta.Id = objArchivo.IdArchivo;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarArchivo;
         

                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorArchivo, null, Generales.Sistemas.Comun);
         

            return respuesta;
        }

    }
}