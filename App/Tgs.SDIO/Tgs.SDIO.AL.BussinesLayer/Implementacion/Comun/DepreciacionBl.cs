﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class DepreciacionBl : IDepreciacionBl
    {
        readonly IDepreciacionDal iDepreciacionDal;

       ProcesoResponse respuesta = new ProcesoResponse();

        public DepreciacionBl(IDepreciacionDal IDepreciacionDal)

        {
            iDepreciacionDal = IDepreciacionDal;
        }

        public ProcesoResponse ActualizarDepreciacion(DepreciacionDtoRequest depreciacion)
        {
           
          
                var objDepreciacion = new Depreciacion()
                {

                    IdDepreciacion = depreciacion.IdDepreciacion,
                    Descripcion = depreciacion.Descripcion,
                    Detalle = depreciacion.Detalle,
                    PorcentajeAnual = depreciacion.PorcentajeAnual,
                    Meses = depreciacion.Meses,
                    IdEstado = depreciacion.IdEstado,
                    IdUsuarioEdicion = depreciacion.IdUsuarioEdicion,
                    FechaEdicion = depreciacion.FechaEdicion


                 };
                iDepreciacionDal.Modify(objDepreciacion);
                iDepreciacionDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (depreciacion.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarDepreciacion : MensajesGeneralComun.ActualizarDepreciacion;
            
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorDepreciacion, null, Generales.Sistemas.Comun);
            


            return respuesta;
        }


        public List<ListaDtoResponse> ListarDepreciacion(DepreciacionDtoRequest depreciacion)
        {
            var query = iDepreciacionDal.GetFilteredAsNoTracking(x => x.IdEstado == depreciacion.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdDepreciacion.ToString(),
                Descripcion = x.Descripcion

            }).ToList();
        }

        public DepreciacionDtoResponse ObtenerDepreciacion(DepreciacionDtoRequest depreciacion)
        {
            var objDepreciacion = iDepreciacionDal.GetFiltered(x => x.IdDepreciacion == depreciacion.IdDepreciacion &&
                                                       x.IdEstado == depreciacion.IdEstado);
            return objDepreciacion.Select(x => new DepreciacionDtoResponse
            {
                IdDepreciacion = x.IdDepreciacion,
                Descripcion = x.Descripcion,
                Detalle = x.Detalle,
                PorcentajeAnual = x.PorcentajeAnual,
                Meses = x.Meses,
                IdEstado = x.IdEstado
            }).Single();

        }

        public ProcesoResponse RegistrarDepreciacion(DepreciacionDtoRequest depreciacion)
        {
            
                var objDepreciacion = new Depreciacion()
                {
                    IdDepreciacion = depreciacion.IdDepreciacion,
                    Descripcion = depreciacion.Descripcion,
                    Detalle = depreciacion.Detalle,
                    PorcentajeAnual = depreciacion.PorcentajeAnual,
                    Meses = depreciacion.Meses,
                    IdEstado = depreciacion.IdEstado,
                    IdUsuarioCreacion = depreciacion.IdUsuarioCreacion,
                    FechaCreacion = depreciacion.FechaCreacion
                };

                iDepreciacionDal.Add(objDepreciacion);
                iDepreciacionDal.UnitOfWork.Commit();

                respuesta.Id = objDepreciacion.IdDepreciacion;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarDepreciacion;
         
             
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorDepreciacion, null, Generales.Sistemas.Comun);
           

            return respuesta;
        }
    }
}
