﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class LineaProductoBl : ILineaProductoBl
    {
        readonly ILineaProductoDal iLineaProductoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public LineaProductoBl(ILineaProductoDal ILineaProductoDal)
        {
            iLineaProductoDal = ILineaProductoDal;
        }


        public ProcesoResponse ActualizarLineaProducto(LineaProductoDtoRequest lineaProducto)
        {
          
                var objLineaProducto = new LineaProducto()
                {

                    IdLineaProducto = lineaProducto.IdLineaProducto,
                    Descripcion = lineaProducto.Descripcion,
                    Plantilla = lineaProducto.Plantilla,
                    IdEstado = lineaProducto.IdEstado,
                    IdUsuarioEdicion = lineaProducto.IdUsuarioEdicion,
                    FechaEdicion = lineaProducto.FechaEdicion
                };
                iLineaProductoDal.Modify(objLineaProducto);
                iLineaProductoDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (lineaProducto.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarLineaProducto : MensajesGeneralComun.ActualizarLineaProducto;
            
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorLineaProducto, null, Generales.Sistemas.Comun);
            


            return respuesta;
        }

        public List<LineaProductoDtoResponse> ListarLineaProducto(LineaProductoDtoRequest lineaProducto)
        {
            var query = iLineaProductoDal.GetFilteredAsNoTracking(x => x.IdEstado == lineaProducto.IdEstado).ToList();
            return query.Select(x => new LineaProductoDtoResponse
            {
                IdLineaProducto = x.IdLineaProducto,
                Descripcion = x.Descripcion,
                Plantilla = x.Plantilla,
                IdEstado = x.IdEstado

            }).ToList();
        }
        public LineaProductoDtoResponse ObtenerLineaProducto(LineaProductoDtoRequest lineaProducto)
        {
            var objLineaProducto = iLineaProductoDal.GetFiltered(x => x.IdEstado == lineaProducto.IdEstado && x.IdLineaProducto == lineaProducto.IdLineaProducto);
            return objLineaProducto.Select(x => new LineaProductoDtoResponse
            {
                IdLineaProducto = x.IdLineaProducto,
                Descripcion = x.Descripcion,
                Plantilla = x.Plantilla,
                IdEstado = x.IdEstado
            }).Single();

        }

        public ProcesoResponse RegistrarLineaProducto(LineaProductoDtoRequest lineaProducto)
        {
           
                var objLineaProducto = new LineaProducto()
                {
                    IdLineaProducto = lineaProducto.IdLineaProducto,
                    Descripcion = lineaProducto.Descripcion,
                    Plantilla = lineaProducto.Plantilla,
                    IdEstado = lineaProducto.IdEstado,
                    IdUsuarioCreacion = lineaProducto.IdUsuarioCreacion,
                    FechaCreacion = lineaProducto.FechaCreacion
                };

                iLineaProductoDal.Add(objLineaProducto);
                iLineaProductoDal.UnitOfWork.Commit();

                respuesta.Id = objLineaProducto.IdLineaProducto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarLineaProducto;
       
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorLineaProducto, null, Generales.Sistemas.Comun);
            
            return respuesta;
        }
    }
}
