﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ClienteBl : IClienteBl
    {
        readonly IClienteDal iClienteDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ClienteBl(IClienteDal IClienteDal)
        {
            iClienteDal = IClienteDal;
        }

        public ProcesoResponse ActualizarCliente(ClienteDtoRequest cliente)
        {
            try
            {
                var objCliente = new Cliente()
                {

                    IdEstado = cliente.IdEstado,
                    IdUsuarioEdicion = cliente.IdUsuarioEdicion,
                    FechaEdicion = cliente.FechaEdicion
                };

                iClienteDal.Modify(objCliente);
                iClienteDal.UnitOfWork.Commit();


                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (cliente.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarCliente : MensajesGeneralComun.ActualizarCliente;

            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(null, null, Generales.Sistemas.Comun);
            }


            return respuesta;
        }


        public List<ClienteDtoResponse> ListarCliente(ClienteDtoRequest cliente)
        {
            //var query = iClienteDal.GetFilteredAsNoTracking(x => x.IdEstado == cliente.IdEstado).ToList();
            //return query.Select(x => new ClienteDtoResponse
            //{
            //    IdCliente = x.IdCliente,
            //    CodigoCliente = x.CodigoCliente,
            //    Descripcion = x.Descripcion,
            //    IdEstado = x.IdEstado,
            //    IdSector = x.IdSector,
            //    GerenteComercial = x.GerenteComercial,
            //    IdDireccionComercial = x.IdDireccionComercial
            //}).ToList();

            return iClienteDal.ListarCliente(cliente);

        }


        public ClienteDtoResponse ObtenerCliente(ClienteDtoRequest cliente)
        {
            var objCliente = iClienteDal.GetFiltered(x => x.IdCliente == cliente.IdCliente &&
                                                       x.IdEstado == cliente.IdEstado);
            return objCliente.Select(x => new ClienteDtoResponse
            {
                IdCliente = x.IdCliente,
                CodigoCliente = x.CodigoCliente,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdSector = x.IdSector,
                GerenteComercial = x.GerenteComercial,
                IdDireccionComercial = x.IdDireccionComercial
            }).Single();

        }

        public ProcesoResponse RegistrarCliente(ClienteDtoRequest cliente)
        {
            try
            {
                var objCliente = new Cliente()
                {
                    IdCliente = cliente.IdCliente,
                    Descripcion = cliente.Descripcion,
                    IdUsuarioCreacion = cliente.IdUsuarioCreacion,
                    FechaCreacion = cliente.FechaCreacion
                };

                iClienteDal.Add(objCliente);
                iClienteDal.UnitOfWork.Commit();

                respuesta.Id = objCliente.IdCliente;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarCliente;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorCliente, null, Generales.Sistemas.Comun);
            }

            return respuesta;
        }

    }
}