﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class FentoceldaBl : IFentoceldaBl
    {
        readonly IFentoceldaDal iFentoceldaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public FentoceldaBl(IFentoceldaDal IFentoceldaDal)
        {
            iFentoceldaDal = IFentoceldaDal;
        }


        public ProcesoResponse ActualizarFentocelda(FentoceldaDtoRequest fentocelda)
        {
            
                var objFentocelda = new Fentocelda()
                {

                    IdFentocelda = fentocelda.IdFentocelda,
                    IdTipificacion = fentocelda.IdTipificacion,
                    Descripcion = fentocelda.Descripcion,
                    Monto = fentocelda.Monto,
                    VelocidadSubidaKBPS = fentocelda.VelocidadSubidaKBPS,
                    PorcentajeGarantizado = fentocelda.PorcentajeGarantizado,
                    PorcentajeSobresuscripcion = fentocelda.PorcentajeSobresuscripcion,
                    CostoSegmentoSatelital = fentocelda.CostoSegmentoSatelital,
                    InvAntenaHubUSD = fentocelda.InvAntenaHubUSD,
                    AntenaCasaClienteUSD = fentocelda.AntenaCasaClienteUSD,
                    Instalacion = fentocelda.Instalacion,
                   IdUsuarioEdicion = fentocelda.IdUsuarioEdicion,
                    FechaEdicion = fentocelda.FechaEdicion
                };
                iFentoceldaDal.Modify(objFentocelda);
                iFentoceldaDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (fentocelda.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarFentocelda : MensajesGeneralComun.ActualizarFentocelda;
         
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorFentocelda, null, Generales.Sistemas.Comun);
         

            return respuesta;
        }

     

        public List<FentoceldaDtoResponse> ListarFentocelda(FentoceldaDtoRequest fentocelda)
        {
            var query = iFentoceldaDal.GetFilteredAsNoTracking(x => x.IdFentocelda == fentocelda.IdFentocelda && x.IdEstado == fentocelda.IdEstado).ToList();
            return query.Select(x => new FentoceldaDtoResponse
            {
                IdFentocelda = x.IdFentocelda,
                IdTipificacion = x.IdTipificacion,
                Descripcion = x.Descripcion,
                Monto = x.Monto,
                VelocidadSubidaKBPS = x.VelocidadSubidaKBPS,
                PorcentajeGarantizado = x.PorcentajeGarantizado,
                PorcentajeSobresuscripcion = x.PorcentajeSobresuscripcion,
                CostoSegmentoSatelital = x.CostoSegmentoSatelital,
                InvAntenaHubUSD = x.InvAntenaHubUSD,
                AntenaCasaClienteUSD = x.AntenaCasaClienteUSD,
                Instalacion = x.Instalacion


            }).ToList();
        }

        public FentoceldaDtoResponse ObtenerFentocelda(FentoceldaDtoRequest fentocelda)
        {
            var objFentocelda = iFentoceldaDal.GetFiltered(x => x.IdFentocelda == fentocelda.IdFentocelda &&
                                                       x.IdEstado == fentocelda.IdEstado);
            return objFentocelda.Select(x => new FentoceldaDtoResponse
            {
                IdFentocelda = x.IdFentocelda,
                IdTipificacion = x.IdTipificacion,
                Descripcion = x.Descripcion,
                Monto = x.Monto,
                VelocidadSubidaKBPS = x.VelocidadSubidaKBPS,
                PorcentajeGarantizado = x.PorcentajeGarantizado,
                PorcentajeSobresuscripcion = x.PorcentajeSobresuscripcion,
                CostoSegmentoSatelital = x.CostoSegmentoSatelital,
                InvAntenaHubUSD = x.InvAntenaHubUSD,
                AntenaCasaClienteUSD = x.AntenaCasaClienteUSD,
                Instalacion = x.Instalacion
            }).Single();

        }

        public ProcesoResponse RegistrarFentocelda(FentoceldaDtoRequest fentocelda)
        {
            
                var objFentocelda = new Fentocelda()
                {
                    IdFentocelda = fentocelda.IdFentocelda,
                    IdTipificacion = fentocelda.IdTipificacion,
                    Descripcion = fentocelda.Descripcion,
                    Monto = fentocelda.Monto,
                    VelocidadSubidaKBPS = fentocelda.VelocidadSubidaKBPS,
                    PorcentajeGarantizado = fentocelda.PorcentajeGarantizado,
                    PorcentajeSobresuscripcion = fentocelda.PorcentajeSobresuscripcion,
                    CostoSegmentoSatelital = fentocelda.CostoSegmentoSatelital,
                    InvAntenaHubUSD = fentocelda.InvAntenaHubUSD,
                    AntenaCasaClienteUSD = fentocelda.AntenaCasaClienteUSD,
                    Instalacion = fentocelda.Instalacion,
                    IdUsuarioCreacion = fentocelda.IdUsuarioCreacion,
                    FechaCreacion = fentocelda.FechaCreacion
                };

                iFentoceldaDal.Add(objFentocelda);
                iFentoceldaDal.UnitOfWork.Commit();

                respuesta.Id = objFentocelda.IdFentocelda;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarFentocelda;
         
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorFentocelda, null, Generales.Sistemas.Comun);
          

            return respuesta;
        }
    }
}
