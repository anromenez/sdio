﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ProveedorBl : IProveedorBl
    {
        readonly IProveedorDal iProveedorDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ProveedorBl(IProveedorDal IProveedorDal)
        {
            iProveedorDal = IProveedorDal;
        }


        public ProcesoResponse ActualizarProveedor(ProveedorDtoRequest proveedor)
        {
          
                var objProveedor = new Proveedor()
                {

                    IdProveedor = proveedor.IdProveedor,
                    Descripcion = proveedor.Descripcion,
                    TipoProveedor = proveedor.TipoProveedor,
                    IdEstado = proveedor.IdEstado,
                    IdUsuarioEdicion = proveedor.IdUsuarioEdicion,
                    FechaEdicion = proveedor.FechaEdicion


                 };

                iProveedorDal.Modify(objProveedor);
                iProveedorDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (proveedor.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarProveedor : MensajesGeneralComun.ActualizarProveedor;
          
        
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorProveedor, null, Generales.Sistemas.Comun);
            


            return respuesta;
        }



        public List<ListaDtoResponse> ListarProveedor(ProveedorDtoRequest proveedor)
        {
            var query = iProveedorDal.GetFilteredAsNoTracking(x =>x.IdEstado == proveedor.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
              
                Codigo = x.IdProveedor.ToString(),
                Descripcion = x.Descripcion,




            }).ToList();
        }

        public ProveedorDtoResponse ObtenerProveedor(ProveedorDtoRequest proveedor)
        {
            var objProveedor = iProveedorDal.GetFiltered(x => x.IdProveedor == proveedor.IdProveedor &&
                                                       x.IdEstado == proveedor.IdEstado);
            return objProveedor.Select(x => new ProveedorDtoResponse
            {
                IdProveedor = x.IdProveedor,
                Descripcion = x.Descripcion,
                TipoProveedor = x.TipoProveedor,
                IdEstado = x.IdEstado,

            }).Single();

        }

        public ProcesoResponse RegistrarProveedor(ProveedorDtoRequest proveedor)
        {
            
                var objProveedor = new Proveedor()
                {
                    IdProveedor = proveedor.IdProveedor,
                    Descripcion = proveedor.Descripcion,
                    TipoProveedor = proveedor.TipoProveedor,
                    IdEstado = proveedor.IdEstado,
                    IdUsuarioCreacion = proveedor.IdUsuarioCreacion,
                    FechaCreacion = proveedor.FechaCreacion
                };

                iProveedorDal.Add(objProveedor);
                iProveedorDal.UnitOfWork.Commit();

                respuesta.Id = objProveedor.IdProveedor;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarProveedor;
          
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorProveedor, null, Generales.Sistemas.Comun);
            

            return respuesta;
        }
    }
}
