﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class MaestraBl : IMaestraBl
    {
        readonly IMaestraDal iMaestraDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public MaestraBl(IMaestraDal IMaestraDal)
        {
            iMaestraDal = IMaestraDal;
        }


        public ProcesoResponse ActualizarMaestra(MaestraDtoRequest maestra)
        {
         
                var objMaestra = new Maestra()
                {

                    IdMaestra = maestra.IdMaestra,
                    Descripcion = maestra.Descripcion,
                    Valor = maestra.Valor,
                    Comentario = maestra.Comentario,
                    IdEstado = maestra.IdEstado,
                    IdUsuarioEdicion = maestra.IdUsuarioEdicion,
                    FechaEdicion = maestra.FechaEdicion
                };
                iMaestraDal.Modify(objMaestra);
                iMaestraDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (maestra.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarMaestra : MensajesGeneralComun.ActualizarMaestra;
         
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorMaestra, null, Generales.Sistemas.Comun);
          


            return respuesta;
        }
        

        public List<ListaDtoResponse> ListarMaestraPorIdRelacion(MaestraDtoRequest maestra)
        {
            var query = iMaestraDal.GetFilteredAsNoTracking(x => x.IdRelacion == maestra.IdRelacion && x.IdEstado == maestra.IdEstado).OrderBy(t => t.Valor).ToList();
            return query.Select(x => new ListaDtoResponse
            {
             Codigo = x.Valor,
                Descripcion = x.Descripcion

            }).ToList();
        }

        public MaestraDtoResponse ObtenerMaestra(MaestraDtoRequest maestra)
        {
            var objMaestra = iMaestraDal.GetFiltered(x => x.IdRelacion == maestra.IdRelacion &&
                                                       x.IdEstado == maestra.IdEstado);
            return objMaestra.Select(x => new MaestraDtoResponse
            {
                IdMaestra = x.IdMaestra,
                Descripcion = x.Descripcion,
                Valor = x.Valor,
                Valor2 = x.Valor2,
                Comentario = x.Comentario,
                IdEstado = x.IdEstado,
            
            }).Single();


        }
        public List<ListaDtoResponse> ListarMaestra(MaestraDtoRequest maestra)
        {
            var objMaestra = iMaestraDal.GetFilteredAsNoTracking(x => x.IdRelacion == maestra.IdRelacion &&
                                                       x.IdEstado == maestra.IdEstado && x.Valor2==maestra.Valor2).OrderBy(t => t.Valor).ToList();
            return objMaestra.Select(x => new ListaDtoResponse
            {
                Codigo = x.Valor,
                Descripcion = x.Descripcion

            }).ToList();


        }


        public ProcesoResponse RegistrarMaestra(MaestraDtoRequest maestra)
        {
            
                var objMaestra = new Maestra()
                {
                    IdMaestra = maestra.IdMaestra,
                    Descripcion = maestra.Descripcion,
                    Valor = maestra.Valor,
                    Comentario = maestra.Comentario,
                    IdEstado = maestra.IdEstado,
                    IdUsuarioCreacion = maestra.IdUsuarioCreacion,
                    FechaCreacion = maestra.FechaCreacion
                };

                iMaestraDal.Add(objMaestra);
                iMaestraDal.UnitOfWork.Commit();

                respuesta.Id = objMaestra.IdMaestra;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarMaestra;
         
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorMaestra, null, Generales.Sistemas.Comun);
            

            return respuesta;
        }

        public List<ComboDtoResponse> ListarComboTiposRecursos()
        {
            return iMaestraDal.ListarComboTiposRecursos();
        }

    }
}
