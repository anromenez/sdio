﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class SectorBl : ISectorBl
    {
        readonly ISectorDal iSectorDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SectorBl(ISectorDal ISectorDal)
        {
            iSectorDal = ISectorDal;
        }


        public List<ListaDtoResponse> ListarComboSector()
        {
            return iSectorDal.ListarComboSector();
        }

        public ProcesoResponse ActualizarSector(SectorDtoRequest sector)
        {
            
                var objSector = new Sector()
                {

                    IdSector = sector.IdSector.Value,
                    Descripcion = sector.Descripcion,
                    IdEstado = sector.IdEstado,
                    IdUsuarioEdicion = sector.IdUsuarioEdicion,
                    FechaEdicion = sector.FechaEdicion
                };
                iSectorDal.Modify(objSector);
                iSectorDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (sector.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSector : MensajesGeneralComun.ActualizarSector;
           
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorSector, null, Generales.Sistemas.Comun);
            


            return respuesta;
        }

        public List<ListaDtoResponse> ListarSector(SectorDtoRequest sector)
        {
            var query = iSectorDal.GetFilteredAsNoTracking(x => x.IdEstado == sector.IdEstado).ToList();
            return query.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdSector.ToString(),
                Descripcion = x.Descripcion

            }).ToList();

        }

        public SectorDtoResponse ObtenerSector(SectorDtoRequest sector)
        {
            var objSector = iSectorDal.GetFiltered(x => x.IdSector == sector.IdSector &&
             x.IdEstado == sector.IdEstado);
            return objSector.Select(x => new SectorDtoResponse
            {
                IdSector = x.IdSector,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado
            }).Single();

        }

        public ProcesoResponse RegistrarSector(SectorDtoRequest sector)
        {
           
                var objSector = new Sector()
                {
                    IdSector = sector.IdSector.Value,
                    Descripcion = sector.Descripcion,
                    IdEstado = sector.IdEstado,
                    IdUsuarioCreacion = sector.IdUsuarioCreacion,
                    FechaCreacion = sector.FechaCreacion
                };

                iSectorDal.Add(objSector);
                iSectorDal.UnitOfWork.Commit();

                respuesta.Id = objSector.IdSector;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarSector;
            
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorSector, null, Generales.Sistemas.Comun);
            

            return respuesta;
        }
    }
}