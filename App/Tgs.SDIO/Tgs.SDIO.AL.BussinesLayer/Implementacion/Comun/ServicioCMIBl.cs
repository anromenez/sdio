﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.DataAccess.Implementacion.Comun
{
    public class ServicioCMIBl : IServicioCMIBl

    {
        readonly IServicioCMIDal iServicioCMIDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ServicioCMIBl(IServicioCMIDal IServicioCMIDal)
        {
            iServicioCMIDal = IServicioCMIDal;
        }


        public ProcesoResponse ActualizarServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            
                var objServicioCMI = new ServicioCMI()
                {
                    IdServicioCMI = servicioCMI.IdServicioCMI,
                    CodigoCMI = servicioCMI.CodigoCMI,
                    DescripcionPlantilla = servicioCMI.DescripcionPlantilla,
                    DescripcionOriginal = servicioCMI.DescripcionOriginal,
                    DescripcionCMI = servicioCMI.DescripcionCMI,
                    IdGerenciaProducto = servicioCMI.IdGerenciaProducto,
                    IdLineaNegocio = servicioCMI.IdLineaNegocio,
                    IdCentroCosto = servicioCMI.IdCentroCosto,
                    IdEstado = servicioCMI.IdEstado,
                    IdUsuarioEdicion = servicioCMI.IdUsuarioEdicion,
                    FechaEdicion = servicioCMI.FechaEdicion
                };
                iServicioCMIDal.Modify(objServicioCMI);
                iServicioCMIDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (servicioCMI.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarServicioCMI : MensajesGeneralComun.ActualizarServicioCMI;
          
             
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorServicioCMI, null, Generales.Sistemas.Comun);
            


            return respuesta;
        }

        

        public List<ListaDtoResponse> ListarServicioCMIPorLineaNegocio(ServicioCMIDtoRequest servicioCMI)
        {
            return iServicioCMIDal.ListarServicioCMIPorLineaNegocio(servicioCMI);
        }

        public List<ServicioCMIDtoResponse> ListarServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            var query = iServicioCMIDal.GetFilteredAsNoTracking(x => x.IdServicioCMI == servicioCMI.IdServicioCMI ).ToList();
            return query.Select(x => new ServicioCMIDtoResponse
            {
                IdServicioCMI = x.IdServicioCMI,
                CodigoCMI = x.CodigoCMI,
                DescripcionPlantilla = x.DescripcionPlantilla,
                DescripcionOriginal = x.DescripcionOriginal,
                DescripcionCMI = x.DescripcionCMI,
                IdGerenciaProducto = x.IdGerenciaProducto,
                IdLineaNegocio = x.IdLineaNegocio,
                IdCentroCosto = x.IdCentroCosto,
                IdEstado = x.IdEstado,


            }).ToList();
        }

        public ServicioCMIDtoResponse ObtenerServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            var objServicioCMI = iServicioCMIDal.GetFiltered(x => x.IdServicioCMI == servicioCMI.IdServicioCMI &&
                                                       x.IdEstado == servicioCMI.IdEstado);
            return objServicioCMI.Select(x => new ServicioCMIDtoResponse
            {
                IdServicioCMI = x.IdServicioCMI,
                CodigoCMI = x.CodigoCMI,
                DescripcionPlantilla = x.DescripcionPlantilla,
                DescripcionOriginal = x.DescripcionOriginal,
                DescripcionCMI = x.DescripcionCMI,
                IdGerenciaProducto = x.IdGerenciaProducto,
                IdLineaNegocio = x.IdLineaNegocio,
                IdCentroCosto = x.IdCentroCosto,
                IdEstado = x.IdEstado,

            }).Single();

        }

        public ProcesoResponse RegistrarServicioCMI(ServicioCMIDtoRequest servicioCMI)
        {
            
                var objServicioCMI = new ServicioCMI()
                {
                    IdServicioCMI = servicioCMI.IdServicioCMI,
                    CodigoCMI = servicioCMI.CodigoCMI,
                    DescripcionPlantilla = servicioCMI.DescripcionPlantilla,
                    DescripcionOriginal = servicioCMI.DescripcionOriginal,
                    DescripcionCMI = servicioCMI.DescripcionCMI,
                    IdGerenciaProducto = servicioCMI.IdGerenciaProducto,
                    IdLineaNegocio = servicioCMI.IdLineaNegocio,
                    IdCentroCosto = servicioCMI.IdCentroCosto,
                    IdEstado = servicioCMI.IdEstado,
                    IdUsuarioCreacion = servicioCMI.IdUsuarioCreacion,
                    FechaCreacion = servicioCMI.FechaCreacion
                };

                iServicioCMIDal.Add(objServicioCMI);
                iServicioCMIDal.UnitOfWork.Commit();

                respuesta.Id = objServicioCMI.IdServicioCMI;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralComun.RegistrarServicioCMI;
          
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorServicioCMI, null, Generales.Sistemas.Comun);
            

            return respuesta;
        }
    }
}
