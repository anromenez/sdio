﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class LineaConceptoCMIBl : ILineaConceptoCMIBl
    {
        readonly ILineaConceptoCMIDal iLineaConceptoCMIDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public LineaConceptoCMIBl(ILineaConceptoCMIDal ILineaConceptoCMIDal)
        {
            iLineaConceptoCMIDal = ILineaConceptoCMIDal;
        }

        public List<LineaConceptoCMIDtoResponse> ListarConceptosPorLinea(LineaConceptoCMIDtoRequest linea)
        {
            return iLineaConceptoCMIDal.ListarConceptosPorLinea(linea);
        }

        public List<LineaConceptoCMIDtoResponse> ListarLineaConceptoCMI(LineaConceptoCMIDtoRequest linea)
        {
            var objConcepto = iLineaConceptoCMIDal.GetFiltered(p => p.IdEstado == linea.IdEstado &&
                                                              p.IdLineaProducto == linea.IdLineaProducto &&
                                                                    p.IdPestana == linea.IdPestana);
            return objConcepto.Select(x => new LineaConceptoCMIDtoResponse
            {
                IdLineaProducto = x.IdLineaProducto,
                IdSubServicio = x.IdSubServicio,
                IdServicioCMI = x.IdServicioCMI,
                Costo = x.Costo,
                IdPestana = x.IdPestana
            }).ToList();

        }
    }
}
