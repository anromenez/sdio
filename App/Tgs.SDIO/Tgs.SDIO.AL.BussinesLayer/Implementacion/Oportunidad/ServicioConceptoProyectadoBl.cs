﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ServicioConceptoProyectadoBl : IServicioConceptoProyectadoBl
    {
        readonly IServicioConceptoProyectadoDal iServicioConceptoProyectadoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ServicioConceptoProyectadoBl(IServicioConceptoProyectadoDal IServicioConceptoProyectadoDal)
        {
            iServicioConceptoProyectadoDal = IServicioConceptoProyectadoDal;
        }

        public ProcesoResponse ActualizarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto)
        {

            try
            {
                var objServicio = new ServicioConceptoProyectado()
                {
                    IdOportunidad = concepto.IdOportunidad,
                    IdLineaNegocio = concepto.IdLineaNegocio,
                    IdServicioCMI = concepto.IdServicioCMI,
                    IdConcepto = concepto.IdConcepto,
                    IdServicioConcepto = concepto.IdServicioConcepto,
                    IdProyectado = concepto.IdProyectado,
                    Mes = concepto.Mes,
                    Anio = concepto.Anio,
                    Monto = concepto.Monto,
                    IdEstado = concepto.IdEstado,
                    IdUsuarioEdicion = concepto.IdUsuarioEdicion,
                    FechaEdicion = concepto.FechaEdicion
                };

                iServicioConceptoProyectadoDal.Modify(objServicio);
                iServicioConceptoProyectadoDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (concepto.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarConcepto : MensajesGeneralOportunidad.ActualizarConcepto;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }

        public List<ServicioConceptoProyectadoDtoResponse> DatosPorVersion(ServicioConceptoProyectadoDtoRequest concepto)
        {
            var objConcepto = iServicioConceptoProyectadoDal.GetFiltered(x => x.IdOportunidad == concepto.IdOportunidad &&
                                                                             x.IdConcepto == Conceptos.IdOibda);

            return objConcepto.Select(x => new ServicioConceptoProyectadoDtoResponse
            {
                IdOportunidad = x.IdOportunidad,
                IdLineaNegocio = x.IdLineaNegocio,
                IdServicioCMI = x.IdServicioCMI,
                IdConcepto = x.IdConcepto,
                IdServicioConcepto = x.IdServicioConcepto,
                IdProyectado = x.IdProyectado,
                Anio = x.Anio,
                Mes = x.Mes,
                Monto = x.Monto,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaCreacion = x.FechaCreacion,
                FechaEdicion = x.FechaEdicion
            }).ToList();

        }

        public decimal? IndicadorCapex(ServicioConceptoProyectadoDtoRequest concepto)
        {
            var objConcepto = iServicioConceptoProyectadoDal.GetFiltered(x => x.IdOportunidad == concepto.IdOportunidad &&
                                                                              x.IdConcepto == Conceptos.IdCapex);
           
          var   sum = objConcepto.ToList().Select(c => c.Monto).Sum();

       
            return sum;

        }

        public decimal? IndicadorCostoDirecto(ServicioConceptoProyectadoDtoRequest concepto)
        {
            var objConcepto = iServicioConceptoProyectadoDal.GetFiltered(x => x.IdOportunidad == concepto.IdOportunidad &&
                                                                               x.IdConcepto == Conceptos.IdCostoDirecto);
            var sum = objConcepto.ToList().Select(c => c.Monto).Sum();

            return sum;
        }

        public decimal? IndicadorIngresoTotal(ServicioConceptoProyectadoDtoRequest concepto)
        {
            var objConcepto = iServicioConceptoProyectadoDal.GetFiltered(x => x.IdOportunidad == concepto.IdOportunidad &&
                                                                               x.IdConcepto == Conceptos.IdIngreso );
            var sum = objConcepto.ToList().Select(c => c.Monto).Sum();

            return sum;
        }

        public List<ServicioConceptoProyectadoDtoResponse> ListarConceptos(ServicioConceptoProyectadoDtoRequest concepto)
        {
            var objConcepto = iServicioConceptoProyectadoDal.GetFiltered(x => x.IdOportunidad == concepto.IdOportunidad &&
                                                                            x.IdServicioCMI == concepto.IdServicioCMI &&
                                                                            x.IdConcepto == concepto.IdConcepto &&
                                                                            x.IdServicioConcepto == concepto.IdServicioConcepto &&
                                                                            x.IdEstado == concepto.IdEstado);

            return objConcepto.Select(x => new ServicioConceptoProyectadoDtoResponse
            {
                IdOportunidad = x.IdOportunidad,
                IdLineaNegocio = x.IdLineaNegocio,
                IdServicioCMI = x.IdServicioCMI,
                IdConcepto = x.IdConcepto,
                IdServicioConcepto = x.IdServicioConcepto,
                IdProyectado = x.IdProyectado,
                Anio = x.Anio,
                Mes = x.Mes,
                Monto = x.Monto,
                IdEstado = x.IdEstado
            }).ToList();

        }

        public ServicioConceptoProyectadoDtoResponse ObtenerConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto)
        {
            var objConcepto = iServicioConceptoProyectadoDal.GetFiltered(x => x.IdOportunidad == concepto.IdOportunidad &&
                                                                                 x.IdServicioCMI == concepto.IdServicioCMI &&
                                                                                 x.IdConcepto == concepto.IdConcepto &&
                                                                                 x.IdServicioConcepto == concepto.IdServicioConcepto &&
                                                                                 x.IdProyectado == concepto.IdProyectado);

            return objConcepto.Select(x => new ServicioConceptoProyectadoDtoResponse
            {
                IdOportunidad = x.IdOportunidad,
                IdLineaNegocio = x.IdLineaNegocio,
                IdServicioCMI = x.IdServicioCMI,
                IdConcepto = x.IdConcepto,
                IdServicioConcepto = x.IdServicioConcepto,
                IdEstado = x.IdEstado,
                Anio = x.Anio,
                Mes = x.Mes,
                Monto = x.Monto 
            }).Single();
        }

        public ProcesoResponse RegistrarConceptoPersonalizado(ServicioConceptoProyectadoDtoRequest concepto)
        {
            try
            {
                if (concepto.IdServicioConcepto != Numeric.Cero)
                {
                    respuesta = ActualizarConceptoPersonalizado(concepto);
                }
                else
                {
                    var objServicio = new ServicioConceptoProyectado()
                    {
                        IdOportunidad = concepto.IdOportunidad,
                        IdLineaNegocio = concepto.IdLineaNegocio,
                        IdServicioCMI = concepto.IdServicioCMI,
                        IdConcepto = concepto.IdConcepto,
                        IdServicioConcepto = concepto.IdServicioConcepto,
                        IdProyectado = concepto.IdProyectado,
                        Mes = concepto.Mes,
                        Anio = concepto.Anio,
                        Monto = concepto.Monto,
                        IdEstado = concepto.IdEstado,
                        IdUsuarioEdicion = concepto.IdUsuarioEdicion,
                        FechaEdicion = concepto.FechaEdicion
                    };

                    iServicioConceptoProyectadoDal.Add(objServicio);
                    iServicioConceptoProyectadoDal.UnitOfWork.Commit();

                    respuesta.Id = objServicio.IdProyectado;
                    respuesta.TipoRespuesta = Proceso.Valido;
                    respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarConcepto;
                }
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }
    }
}
