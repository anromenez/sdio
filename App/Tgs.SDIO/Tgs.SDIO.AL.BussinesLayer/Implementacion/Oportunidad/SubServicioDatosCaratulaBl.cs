﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class SubServicioDatosCaratulaBl : ISubServicioDatosCaratulaBl
    {
        readonly ISubServicioDatosCaratulaDal iSubServicioDatosCaratulaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SubServicioDatosCaratulaBl(ISubServicioDatosCaratulaDal ISubServicioDatosCaratulaDal)
        {
            iSubServicioDatosCaratulaDal = ISubServicioDatosCaratulaDal;
        }

        public ProcesoResponse ActualizarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            try
            {
                var objSubServicio = new SubServicioDatosCaratula()
                {
                    Alquiler = SubServicio.Alquiler,
                    CC = SubServicio.CC,
                    CCQBK = SubServicio.CCQBK,
                    CCQCAPEX = SubServicio.CCQCAPEX,
                    CCQProvincia = SubServicio.CCQProvincia,
                    Circuito = SubServicio.Circuito,
                    //Cantidad = SubServicio.Cantidad,
                    Desinstalacion = SubServicio.Desinstalacion,
                    FechaEdicion = SubServicio.FechaCreacion,
                    Factor = SubServicio.Factor,
                    IdBWOportunidad = SubServicio.IdBW,
                    FlagRenovacion = SubServicio.FlagRenovacion,
                    IdActivoPasivo = SubServicio.IdActivoPasivo,
                    IdEstado = SubServicio.IdEstado,
                    IdSubServicioDatosCaratula = SubServicio.IdSubServicioDatosCaratula,
                    IdLocalidad = SubServicio.IdLocalidad,
                    IdMedio = SubServicio.IdMedio,
                    //IdModelo = SubServicio.IdModelo,
                    IdTipoEnlace = SubServicio.IdTipoEnlace,
                    IdUsuarioEdicion = SubServicio.IdUsuarioEdicion,
                    Instalacion = SubServicio.Instalacion,
                    MontoTotalMensual = SubServicio.MontoTotalMensual,
                    MontoUnitarioMensual = SubServicio.MontoUnitarioMensual,
                    NumeroMeses = SubServicio.NumeroMeses,
                    NumeroMesInicioGasto = SubServicio.NumeroMesInicioGasto,
                    PU = SubServicio.PU,
                    RADIO = SubServicio.RADIO,
                    TIWS = SubServicio.TIWS,
                    ValorCuota = SubServicio.ValorCuota
                };

                iSubServicioDatosCaratulaDal.Modify(objSubServicio);
                iSubServicioDatosCaratulaDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (SubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorSubServicio, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }

        public SubServicioDatosCaratulaDtoResponse ObtenerSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            var lista = iSubServicioDatosCaratulaDal.GetFilteredAsNoTracking(x => x.IdEstado == SubServicio.IdEstado &&
                                                                             x.IdSubServicioDatosCaratula == SubServicio.IdSubServicioDatosCaratula).ToList();
            return lista.Select(x => new SubServicioDatosCaratulaDtoResponse
            {
                Alquiler = x.Alquiler,
                Desinstalacion = x.Desinstalacion
            }).Single();
        }

        public List<SubServicioDatosCaratulaDtoResponse> ListarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            return iSubServicioDatosCaratulaDal.ListarSubServicioDatosCaratula(SubServicio);
        }

        public ProcesoResponse RegistrarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            try
            {
                if (SubServicio.IdSubServicioDatosCaratula != Numeric.Cero)
                {
                    respuesta = ActualizarSubServicioDatosCaratula(SubServicio);
                }
                else
                {
                    var objSubServicio = new SubServicioDatosCaratula()
                    {
                        Alquiler = SubServicio.Alquiler,
                        CC = SubServicio.CC,
                        CCQBK = SubServicio.CCQBK,
                        CCQCAPEX = SubServicio.CCQCAPEX,
                        CCQProvincia = SubServicio.CCQProvincia,
                        Circuito = SubServicio.Circuito,
                        //Cantidad = SubServicio.Cantidad,
                        Desinstalacion = SubServicio.Desinstalacion,
                        FechaEdicion = SubServicio.FechaCreacion,
                        Factor = SubServicio.Factor,
                        IdBWOportunidad = SubServicio.IdBW,
                        FlagRenovacion = SubServicio.FlagRenovacion,
                        IdActivoPasivo = SubServicio.IdActivoPasivo,
                        IdEstado = SubServicio.IdEstado,
                        IdSubServicioDatosCaratula = SubServicio.IdSubServicioDatosCaratula,
                        IdLocalidad = SubServicio.IdLocalidad,
                        IdMedio = SubServicio.IdMedio,
                        //IdModelo = SubServicio.IdModelo,
                        //IdServicioSubServicio = SubServicio.IdServicioSubServicio,
                        IdTipoEnlace = SubServicio.IdTipoEnlace,
                        IdUsuarioEdicion = SubServicio.IdUsuarioEdicion,
                        Instalacion = SubServicio.Instalacion,
                        MontoTotalMensual = SubServicio.MontoTotalMensual,
                        MontoUnitarioMensual = SubServicio.MontoUnitarioMensual,
                        NumeroMeses = SubServicio.NumeroMeses,
                        NumeroMesInicioGasto = SubServicio.NumeroMesInicioGasto,
                        PU = SubServicio.PU,
                        RADIO = SubServicio.RADIO,
                        TIWS = SubServicio.TIWS,
                        ValorCuota = SubServicio.ValorCuota
                    };
                    iSubServicioDatosCaratulaDal.Add(objSubServicio);
                    iSubServicioDatosCaratulaDal.UnitOfWork.Commit();

                    respuesta.Id = objSubServicio.IdSubServicioDatosCaratula;
                    respuesta.TipoRespuesta = Proceso.Valido;
                    respuesta.Mensaje = MensajesGeneralComun.RegistrarSubServicio;
                }
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorSubServicio, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }

        public ProcesoResponse EliminarSubServicioDatosCaratula(SubServicioDatosCaratulaDtoRequest SubServicio)
        {
            throw new NotImplementedException();
        }
    }
}
