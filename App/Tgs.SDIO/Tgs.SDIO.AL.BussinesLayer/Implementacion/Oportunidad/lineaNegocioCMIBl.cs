﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class LineaNegocioCMIBl : ILineaNegocioCMIBl
    {
        readonly ILineaNegocioCMIDal iLineaNegocioCMIDal;
        public LineaNegocioCMIBl(ILineaNegocioCMIDal ILineaNegocioCMIDal)
        {
            iLineaNegocioCMIDal = ILineaNegocioCMIDal;
        }
        ProcesoResponse respuesta = new ProcesoResponse();
        public ProcesoResponse ActualizarLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI)
        {

            var objLineaNegocioCMI = new LineaNegocioCMI()
            {
                
                IdEstado = lineaNegocioCMI.IdEstado,
                IdUsuarioEdicion = lineaNegocioCMI.IdUsuarioEdicion,
                FechaEdicion = lineaNegocioCMI.FechaEdicion,
               
               

            };

            iLineaNegocioCMIDal.Modify(objLineaNegocioCMI);
            iLineaNegocioCMIDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (lineaNegocioCMI.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarLineaNegocioCMI : MensajesGeneralOportunidad.ActualizarLineaNegocioCMI;

            return respuesta;
        }


        public List<ListaDtoResponse> ListarLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI)
        {

            var query = iLineaNegocioCMIDal.GetFilteredAsNoTracking(x => x.IdEstado == lineaNegocioCMI.IdEstado
            


            ).ToList();
            return query.Select(x => new ListaDtoResponse
            {

               
         
            }).ToList();
        }

        public LineaNegocioCMIDtoResponse ObtenerLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI)
        {
            var objLineaNegocioCMI = iLineaNegocioCMIDal.GetFiltered(x => x.IdServicioCMI == lineaNegocioCMI.IdServicioCMI);
            return objLineaNegocioCMI.Select(x => new LineaNegocioCMIDtoResponse
            {
      
            }).Single();

        }

        public ProcesoResponse RegistrarLineaNegocioCMI(LineaNegocioCMIDtoRequest lineaNegocioCMI)
        {

            var objLineaNegocioCMI = new LineaNegocioCMI()
            {

                IdUsuarioCreacion = lineaNegocioCMI.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(lineaNegocioCMI.FechaCreacion)
            };

            iLineaNegocioCMIDal.Add(objLineaNegocioCMI);
            iLineaNegocioCMIDal.UnitOfWork.Commit();


            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarLineaNegocioCMI;

            return respuesta;
        }

    }
}
