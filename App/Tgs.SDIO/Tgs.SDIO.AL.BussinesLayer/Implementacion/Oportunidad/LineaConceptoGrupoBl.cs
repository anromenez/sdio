﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class LineaConceptoGrupoBl : ILineaConceptoGrupoBl
    {
        readonly ILineaConceptoGrupoDal iLineaConceptoGrupoDal;
        public LineaConceptoGrupoBl(ILineaConceptoGrupoDal ILineaConceptoGrupoDal)
        {
            iLineaConceptoGrupoDal = ILineaConceptoGrupoDal;
        }

        public List<LineaConceptoGrupoDtoResponse> ListarLineaConceptoGrupo(LineaConceptoGrupoDtoRequest linea)
        {
            var objConcepto = iLineaConceptoGrupoDal.GetFiltered(p => p.IdEstado == linea.IdEstado &&
                                                                       p.IdPestana == linea.IdPestana &&
                                                                       p.IdGrupo == linea.IdGrupo &&
                                                                       p.IdLineaProducto == linea.IdLineaProducto);

            return objConcepto.Select(x => new LineaConceptoGrupoDtoResponse
            {
                IdLineaProducto = x.IdLineaProducto,
                IdConcepto = x.IdConcepto,
                IdPestana = x.IdPestana,
                IdGrupo = x.IdGrupo
            }).ToList();
        }
    }
}
