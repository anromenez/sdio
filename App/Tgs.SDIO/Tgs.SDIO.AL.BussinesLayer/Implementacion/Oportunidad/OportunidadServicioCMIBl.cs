﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadServicioCMIBl : IOportunidadServicioCMIBl
    {
        readonly IOportunidadServicioCMIDal iOportunidadServicioCMIDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadServicioCMIBl(IOportunidadServicioCMIDal IOportunidadServicioCMIDal)
        {
            iOportunidadServicioCMIDal = IOportunidadServicioCMIDal;
        }
        public ProcesoResponse ActualizarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad)
        {
            try
            {
                var objServicio = new OportunidadServicioCMI()
                {
                    IdOportunidadLineaNegocio = oportunidad.IdOportunidadLineaNegocio,
                    IdServicioCMI = oportunidad.IdServicioCMI,
                    Porcentaje = oportunidad.Porcentaje,
                    IdAnalista = oportunidad.IdAnalista,
                    IdEstado = oportunidad.IdEstado,
                    IdUsuarioEdicion = oportunidad.IdUsuarioEdicion,
                    FechaEdicion = oportunidad.FechaEdicion
                };

                iOportunidadServicioCMIDal.Modify(objServicio);
                iOportunidadServicioCMIDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (oportunidad.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarServicioCMI: MensajesGeneralOportunidad.ActualizarServicioCMI;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }
        
        public List<OportunidadServicioCMIDtoResponse> ListarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad)
        {
            return iOportunidadServicioCMIDal.ListarOportunidadServicioCMI(oportunidad);
        }

        public ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad)
        {
            try
            {
                var objServicio = new OportunidadServicioCMI()
                {
                    IdOportunidadLineaNegocio = oportunidad.IdOportunidadLineaNegocio,
                    IdServicioCMI = oportunidad.IdServicioCMI,
                    Porcentaje = oportunidad.Porcentaje,
                    IdAnalista = oportunidad.IdAnalista,
                    IdEstado = oportunidad.IdEstado,
                    IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                    FechaCreacion = oportunidad.FechaCreacion
                };

                iOportunidadServicioCMIDal.Add(objServicio);
                iOportunidadServicioCMIDal.UnitOfWork.Commit();

                respuesta.Id = objServicio.IdOportunidadLineaNegocio;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarServicioCMI;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
            }

            return respuesta;
        }

        //Circuitos
        public List<OportunidadServicioCMIDtoResponse> ListarCircuitos(OportunidadServicioCMIDtoRequest oportunidad)
        {
            var lista = iOportunidadServicioCMIDal.GetFilteredAsNoTracking(x => x.IdEstado == oportunidad.IdEstado &&
                                                                             x.IdOportunidadLineaNegocio == oportunidad.IdOportunidadLineaNegocio
                                                                             ).ToList();
            return lista.Select(x => new OportunidadServicioCMIDtoResponse
            {
                IdOportunidadLineaNegocio = x.IdOportunidadLineaNegocio,
                IdServicioCMI = x.IdServicioCMI
            }).ToList();
        }

    }
}
