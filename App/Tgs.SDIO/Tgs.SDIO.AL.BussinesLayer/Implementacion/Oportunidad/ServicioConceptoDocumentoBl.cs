﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ServicioConceptoDocumentoBl : IServicioConceptoDocumentoBl
    {
        readonly IServicioConceptoDocumentoDal iServicioConceptoDocumentoDal;
        public ServicioConceptoDocumentoBl(IServicioConceptoDocumentoDal IServicioConceptoDocumentoDal) 
        {
            iServicioConceptoDocumentoDal = IServicioConceptoDocumentoDal;
        }
    }
}
