﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaBl : IOportunidadFlujoCajaBl
    {
        readonly IOportunidadFlujoCajaDal iOportunidadFlujoCajaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadFlujoCajaBl(IOportunidadFlujoCajaDal IOportunidadFlujoCajaDal)
        {
            iOportunidadFlujoCajaDal = IOportunidadFlujoCajaDal;
        }

        public ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja,
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                Cantidad = flujocaja.Cantidad,
                CostoUnitario = flujocaja.CostoUnitario,
                IdEstado = flujocaja.IdEstado,
                FechaEdicion = flujocaja.FechaEdicion,
                IdUsuarioEdicion = flujocaja.IdUsuarioEdicion

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdOportunidadLineaNegocio, x => x.Cantidad, x => x.CostoUnitario, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (oportunidadFlujoCaja.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarFlujoCaja : MensajesGeneralOportunidad.ActualizarFlujoCaja;
            
            return respuesta;
        }

        public List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var lista = iOportunidadFlujoCajaDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio &&
                                                                              x.IdEstado == flujocaja.IdEstado).ToList();
            return lista.Select(x => new OportunidadFlujoCajaDtoResponse
            {
                IdAgrupador = x.IdAgrupador,
                IdTipoCosto = x.IdTipoCosto,
                Descripcion = x.Descripcion,
                IdPeriodos = x.IdPeriodos,
                IdPestana = x.IdPestana,
                IdGrupo = x.IdGrupo
            }).ToList();
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ListarOportunidadFlujoCajaBandeja(flujocaja);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = iOportunidadFlujoCajaDal.GetFilteredAsNoTracking(x => x.IdOportunidadLineaNegocio == flujocaja.IdOportunidadLineaNegocio &&
                                                                              x.IdFlujoCaja == flujocaja.IdFlujoCaja &&
                                                                              x.IdEstado == flujocaja.IdEstado);
            return oportunidadFlujoCaja.Select(x => new OportunidadFlujoCajaDtoResponse
            {
                IdAgrupador = x.IdAgrupador,
                IdTipoCosto = x.IdTipoCosto,
                Descripcion = x.Descripcion,
                IdPeriodos = x.IdPeriodos,
                IdPestana = x.IdPestana,
                IdGrupo = x.IdGrupo
            }).Single();
        }

        public ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {

            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdAgrupador = flujocaja.IdAgrupador,
                IdTipoCosto = flujocaja.IdTipoCosto,
                Descripcion = flujocaja.Descripcion,
                IdProveedor = flujocaja.IdProveedor,
                IdPeriodos = flujocaja.IdPeriodos,
                IdPestana = flujocaja.IdPestana,
                IdGrupo = flujocaja.IdGrupo,
                IdCasoNegocio = flujocaja.IdCasoNegocio,
                IdServicio = flujocaja.IdServicio,
                Cantidad = flujocaja.Cantidad,
                CostoUnitario = flujocaja.CostoUnitario,
                ContratoMarco = flujocaja.ContratoMarco,
                IdMoneda = flujocaja.IdMoneda,
                FlagSISEGO = flujocaja.FlagSISEGO,
                IdServicioCMI = flujocaja.IdServicioCMI,
                IdEstado = flujocaja.IdEstado,
                IdUsuarioCreacion = flujocaja.IdUsuarioCreacion,
                FechaCreacion = flujocaja.FechaCreacion
            };

            iOportunidadFlujoCajaDal.Add(oportunidadFlujoCaja);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarFlujoCaja;
            
            return respuesta;
        }

        public List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaDal.ListaAnoMesProyecto(flujocaja);
        }

        public List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.GeneraCasoNegocio(casonegocio);
        }

        public ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.EliminarCasoNegocio(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.GeneraServicio(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.ListarDetalleOportunidadCaratula(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.ListarDetalleOportunidadEcapex(casonegocio);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaDal.ObtenerDetalleOportunidadEcapex(casonegocio);
        }

        public ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            var oportunidadFlujoCaja = new OportunidadFlujoCaja()
            {
                IdFlujoCaja = flujocaja.IdFlujoCaja,
                IdOportunidadLineaNegocio = flujocaja.IdOportunidadLineaNegocio,
                IdPeriodos = flujocaja.IdPeriodos,
                IdEstado = flujocaja.IdEstado,
                FechaEdicion = flujocaja.FechaEdicion,
                IdUsuarioEdicion = flujocaja.IdUsuarioEdicion

            };

            iOportunidadFlujoCajaDal.ActualizarPorCampos(oportunidadFlujoCaja, x => x.IdFlujoCaja, x => x.IdOportunidadLineaNegocio, x => x.IdPeriodos, x => x.IdEstado, x => x.FechaEdicion, x => x.IdUsuarioEdicion);
            iOportunidadFlujoCajaDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarFlujoCaja;

            return respuesta;
        }
    }
}
