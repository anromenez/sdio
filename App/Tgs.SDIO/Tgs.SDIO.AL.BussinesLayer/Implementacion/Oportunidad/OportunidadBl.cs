﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadBl : IOportunidadBl
    {
        readonly IOportunidadDal iOportunidadDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadBl(IOportunidadDal IOportunidadDal)
        {
            iOportunidadDal = IOportunidadDal;
        }



        public ProcesoResponse ActualizarOportunidad(OportunidadDtoRequest oportunidad)
        {
            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());

            if (oportunidad.IdEstado==Generales.Estados.EvaluacionEconomica || oportunidad.IdEstado== Generales.Estados.Aprobado || oportunidad.IdEstado==Generales.Estados.Rechazado
                || oportunidad.IdEstado == Generales.Estados.Cancelado || oportunidad.IdEstado == Generales.Estados.RechazadoPorCliente)
            {
                var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
                {
                    IdOportunidad = oportunidad.IdOportunidad,
                    IdEstado = oportunidad.IdEstado,                 
                    FechaEdicion = fecha,
                    IdUsuarioEdicion = oportunidad.IdUsuarioEdicion,
                   
                };

                iOportunidadDal.ActualizarPorCampos(objOportunidad, x => x.IdOportunidad, x => x.IdEstado,
                    x => x.FechaEdicion,  x => x.IdUsuarioEdicion);

                iOportunidadDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidad;

            }
            else
            {
                var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
                {
                    IdOportunidad = oportunidad.IdOportunidad,
                    IdCliente = oportunidad.IdCliente,
                    Descripcion = oportunidad.Descripcion,
                    NumeroSalesForce = oportunidad.NumeroSalesForce,
                    NumeroCaso = oportunidad.NumeroCaso,
                    Alcance = oportunidad.Alcance,
                    Periodo = oportunidad.Periodo,
                    IdEstado = oportunidad.IdEstado,
                    TiempoImplantacion = oportunidad.TiempoImplantacion,
                    IdTipoServicio = oportunidad.IdTipoServicio,
                    IdAnalistaFinanciero = oportunidad.IdAnalistaFinanciero,
                    IdProductManager = oportunidad.IdProductManager,
                    IdProyectoAnterior = oportunidad.IdProyectoAnterior,
                    IdCoordinadorFinanciero = oportunidad.IdCoordinadorFinanciero,
                    IdTipoProyecto = oportunidad.IdTipoProyecto,
                    FechaEdicion = fecha,
                    IdUsuarioEdicion = oportunidad.IdUsuarioEdicion,
                    Fecha = oportunidad.Fecha,
                    TiempoProyecto = oportunidad.Periodo + oportunidad.TiempoImplantacion
                };

                iOportunidadDal.ActualizarPorCampos(objOportunidad, x => x.IdOportunidad, x => x.IdCliente, x => x.Descripcion,
                  x => x.NumeroSalesForce, x => x.NumeroCaso, x => x.Alcance, x => x.Periodo, x => x.IdEstado, x => x.TiempoImplantacion,
                   x => x.IdTipoServicio, x => x.IdAnalistaFinanciero, x => x.IdProductManager, x => x.IdCoordinadorFinanciero,
                    x => x.IdTipoProyecto, x => x.FechaEdicion, x => x.IdUsuarioEdicion, x => x.Fecha, x => x.TiempoProyecto,
                    x => x.IdProyectoAnterior);

                iOportunidadDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.ActualizarOportunidad;

            }
          




            return respuesta;
        }

        public List<OportunidadDtoResponse> ListarProyectadoOIBDA(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.ListarProyectadoOIBDA(oportunidad);
        }

        public List<OportunidadDtoResponse> ListarOportunidad(OportunidadDtoRequest oportunidad)
        {
            throw new NotImplementedException();
        }

        public List<OportunidadDtoResponse> ListarOportunidadCabecera(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.ListarOportunidadCabecera(oportunidad);
        }

        public OportunidadDtoResponse ObtenerOportunidad(OportunidadDtoRequest oportunidad)
        {
            var objOportunidad = iOportunidadDal.GetFiltered(p => p.IdOportunidad == oportunidad.IdOportunidad);

            return objOportunidad.Select(x => new OportunidadDtoResponse
            {

                IdOportunidad = x.IdOportunidad,
                IdTipoEmpresa = x.IdTipoEmpresa,
                IdCliente = x.IdCliente,
                Descripcion = x.Descripcion,
                NumeroSalesForce = x.NumeroSalesForce,
                NumeroCaso = x.NumeroCaso,
                Alcance = x.Alcance,
                Periodo = x.Periodo,
                TiempoImplantacion = x.TiempoImplantacion,
                IdTipoServicio = x.IdTipoServicio,
                IdEstado = x.IdEstado,
                IdAnalistaFinanciero = x.IdAnalistaFinanciero,
                IdProductManager = x.IdProductManager,
                IdPreVenta = x.IdPreVenta,
                IdCoordinadorFinanciero = x.IdCoordinadorFinanciero,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                IdTipoProyecto = x.IdTipoProyecto,
                FechaCreacion = x.FechaCreacion,
                Fecha = x.Fecha,
                Version = x.Version,
                VersionPadre = x.VersionPadre,
                FlagGanador = x.FlagGanador,
                Agrupador = x.Agrupador,
                TiempoProyecto = x.TiempoProyecto,
                IdTipoCambio = x.IdTipoCambio,
                IdProyectoAnterior = x.IdProyectoAnterior
                
            }).Single();
        }


        public OportunidadDtoResponse ObtenerOportunidadId(OportunidadDtoRequest oportunidad)
        {
            var objOportunidad = iOportunidadDal.GetFiltered(p => p.IdOportunidad == oportunidad.IdOportunidad);

            return objOportunidad.Select(x => new OportunidadDtoResponse
            {
                IdOportunidad = x.IdOportunidad,
                Version = x.Version,
                IdCliente = x.IdCliente,
                Oportunidad = x.Descripcion,
                IdPreVenta = x.IdPreVenta,
                IdProductManager = x.IdProductManager,
                IdAnalistaFinanciero = x.IdAnalistaFinanciero,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                IdUsuarioModifica = x.IdUsuarioEdicion,
                IdEstado = x.IdEstado
            }).Single();
        }


        public ProcesoResponse OportunidadGanador(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.OportunidadGanador(oportunidad);
        }

        public ProcesoResponse OportunidadInactivar(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.OportunidadInactivar(oportunidad);
        }

        public ProcesoResponse RegistrarOportunidad(OportunidadDtoRequest oportunidad)
        {

            var Id = iOportunidadDal.OportunidadUltimoId();

            var objOportunidad = new Entities.Entities.Oportunidad.Oportunidad()
            {
                IdTipoEmpresa = Generales.Numeric.Uno,
                IdCliente = oportunidad.IdCliente,
                Descripcion = oportunidad.Descripcion,
                NumeroSalesForce = oportunidad.NumeroSalesForce,
                NumeroCaso = oportunidad.NumeroCaso,
                Alcance = oportunidad.Alcance,
                Periodo = oportunidad.Periodo,
                TiempoImplantacion = oportunidad.TiempoImplantacion,
                IdTipoServicio = oportunidad.IdTipoServicio,
                IdEstado = Generales.Estados.Activo,
                IdAnalistaFinanciero = oportunidad.IdAnalistaFinanciero,
                IdProductManager = oportunidad.IdProductManager,
                IdPreVenta = oportunidad.IdPreVenta,
                IdCoordinadorFinanciero = oportunidad.IdCoordinadorFinanciero,
                IdUsuarioCreacion = oportunidad.IdUsuarioCreacion,
                IdTipoProyecto = oportunidad.IdTipoProyecto,
                FechaCreacion = oportunidad.FechaCreacion,
                Fecha = oportunidad.Fecha,
                Version = Generales.Numeric.Uno,
                VersionPadre = Id + Generales.Numeric.Uno,
                FlagGanador = Generales.Numeric.Cero,
                Agrupador = Id + Generales.Numeric.Uno,
                TiempoProyecto = oportunidad.Periodo + oportunidad.TiempoImplantacion,
                IdTipoCambio = Generales.Numeric.Uno,
                IdProyectoAnterior=oportunidad.IdProyectoAnterior


            };


            iOportunidadDal.Add(objOportunidad);
            iOportunidadDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidad.IdOportunidad;
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarOportunidad;


            return respuesta;
        }

        public ProcesoResponse RegistrarVersionOportunidad(OportunidadDtoRequest oportunidad)
        {
            return iOportunidadDal.RegistrarVersionOportunidad(oportunidad);
        }



        public List<OportunidadDtoResponse> ObtenerVersiones(OportunidadDtoRequest oportunidad)
        {

            List<OportunidadDtoResponse> data = new List<OportunidadDtoResponse>();
            OportunidadDtoResponse obj = new OportunidadDtoResponse();

            var objOportunidad = iOportunidadDal.GetFiltered(p => p.IdOportunidad == oportunidad.IdOportunidad).Single();



            var dto = iOportunidadDal.GetFiltered(p => p.Agrupador == objOportunidad.Agrupador).OrderByDescending(t => t.Version).ToList();



            foreach (var part in dto)
            {
                data.Add(new OportunidadDtoResponse()
                {
                    IdOportunidad = part.IdOportunidad,
                    Version = part.Version + Generales.Numeric.Uno,
                    Descripcion = part.Descripcion,
                    IdPreVenta = part.IdPreVenta,
                    IdProductManager = part.IdProductManager,
                    IdAnalistaFinanciero = part.IdAnalistaFinanciero,
                    IdUsuarioCreacion = part.IdUsuarioCreacion,
                    IdUsuarioModifica = part.IdUsuarioEdicion,
                    IdEstado = part.IdEstado
                });

            }

            return data;
        }

    }
}
