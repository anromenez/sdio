﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class PestanaGrupoBl : IPestanaGrupoBl
    {
        readonly IPestanaGrupoDal iPestanaGrupoDal;
        public PestanaGrupoBl(IPestanaGrupoDal IPestanaGrupoDal)
        {
            iPestanaGrupoDal = IPestanaGrupoDal;
        }
        public List<ListaDtoResponse> ListarPestanaGrupo(PestanaGrupoDtoRequest grupo)
        {
            var objLinea = iPestanaGrupoDal.GetFilteredAsNoTracking(p => p.IdEstado == grupo.IdEstado && p.FlagServicio==grupo.FlagServicio);

            return objLinea.Select(x => new ListaDtoResponse
            {
                Codigo = x.IdGrupo.ToString(),
              
                Descripcion = x.Descripcion
              
            }).ToList();
        }
    }
}
