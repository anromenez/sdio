﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ProyectoServicioConceptoBl : IProyectoServicioConceptoBl
    {
        readonly IProyectoServicioConceptoDal iProyectoServicioConceptoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ProyectoServicioConceptoBl(IProyectoServicioConceptoDal IProyectoServicioConceptoDal)
        {
            iProyectoServicioConceptoDal = IProyectoServicioConceptoDal;
        }

        public ProcesoResponse ActualizarProyectoServicioConcepto(ProyectoDtoRequest proyecto)
        {
            try
            {
                var objServicio = new ProyectoServicioConcepto()
                {
                    IdProyecto = proyecto.IdProyecto,
                    IdLineaProducto = proyecto.IdLineaProducto,
                    IdServicioCMI = proyecto.IdServicioCMI,
                    IdConcepto = proyecto.IdConcepto,
                    IdServicioConcepto = proyecto.IdServicioConcepto,
                    Descripcion = proyecto.Descripcion,
                    IdEstado = proyecto.IdEstado,
                    IdUsuarioEdicion = proyecto.IdUsuarioEdicion,
                    FechaEdicion = proyecto.FechaEdicion
                };

                iProyectoServicioConceptoDal.Modify(objServicio);
                iProyectoServicioConceptoDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (proyecto.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarConcepto : MensajesGeneralOportunidad.ActualizarConcepto;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }

        public List<ProyectoConceptoDtoResponse> ListarCMIProyecto(ProyectoDtoRequest proyecto)
        {
            return iProyectoServicioConceptoDal.ListarCMIProyecto(proyecto);
        }

        public List<ProyectoConceptoDtoResponse> ListarCMIProyectoCabecera(ProyectoDtoRequest proyecto)
        {
            return iProyectoServicioConceptoDal.ListarCMIProyectoCabecera(proyecto);
        }


        public ProyectoConceptoDtoResponse ObtenerProyectoServicioConcepto(ProyectoDtoRequest proyecto)
        {
            var objProyecto = iProyectoServicioConceptoDal.GetFiltered(p => p.IdProyecto == proyecto.IdProyecto &&
                                                                             p.IdLineaProducto == proyecto.IdLineaProducto &&
                                                                             p.IdConcepto == proyecto.IdConcepto &&
                                                                             p.IdServicioConcepto == proyecto.IdServicioConcepto);

            return objProyecto.Select(x => new ProyectoConceptoDtoResponse
            {
                IdProyecto = x.IdProyecto,
                IdLineaProducto = x.IdLineaProducto,
                IdServicioCMI = x.IdServicioCMI,
                IdConcepto = x.IdConcepto,
                IdServicioConcepto = x.IdServicioConcepto,
                Descripcion = x.Descripcion,
                IdEstado = x.IdEstado,
                IdTipoCosto = x.IdTipoCosto,
                Porcentaje = x.Ponderacion,
                CostoPreOperativo = x.CostoPreOperativo
            }).Single();
        }

        public ProcesoResponse RegistrarProyectoServicioConcepto(ProyectoDtoRequest proyecto)
        {
            try
            {
                if (proyecto.IdServicioConcepto != Numeric.Cero)
                {
                    respuesta = ActualizarProyectoServicioConcepto(proyecto);
                }
                else
                {
                    var objServicio = new ProyectoServicioConcepto()
                    {
                        IdProyecto = proyecto.IdProyecto,
                        IdLineaProducto = proyecto.IdLineaProducto,
                        IdServicioCMI = proyecto.IdServicioCMI,
                        IdConcepto = proyecto.IdConcepto,
                        IdServicioConcepto = proyecto.IdServicioConcepto,
                        Descripcion = proyecto.Descripcion,
                        IdEstado = proyecto.IdEstado,
                        IdUsuarioCreacion = proyecto.IdUsuarioCreacion,
                        FechaCreacion = proyecto.FechaCreacion
                    };

                    iProyectoServicioConceptoDal.Add(objServicio);
                    iProyectoServicioConceptoDal.UnitOfWork.Commit();

                    respuesta.Id = objServicio.IdProyecto;
                    respuesta.TipoRespuesta = Proceso.Valido;
                    respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarConcepto;
                }
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }
    }
}
