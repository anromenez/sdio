﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadTipoCambioDetalleBl : IOportunidadTipoCambioDetalleBl
    {
        readonly IOportunidadTipoCambioDetalleDal iOportunidadTipoCambioDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadTipoCambioDetalleBl(IOportunidadTipoCambioDetalleDal IOportunidadTipoCambioDetalleDal)
        {
            iOportunidadTipoCambioDetalleDal = IOportunidadTipoCambioDetalleDal;
        }

        public ProcesoResponse ActualizarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {

            var objOportunidadTipoCambioDetalle = new OportunidadTipoCambioDetalle()
            {

                IdEstado = OportunidadTipoCambioDetalle.IdEstado,
                IdUsuarioEdicion = OportunidadTipoCambioDetalle.IdUsuarioEdicion,
                FechaEdicion = OportunidadTipoCambioDetalle.FechaEdicion
            };

            iOportunidadTipoCambioDetalleDal.Modify(objOportunidadTipoCambioDetalle);
            iOportunidadTipoCambioDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;


            return respuesta;
        }


        public OportunidadTipoCambioDetalleDtoResponse ObtenerOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {
            var objOportunidadTipoCambioDetalle = iOportunidadTipoCambioDetalleDal.GetFiltered(x => x.IdTipoCambioDetalle == OportunidadTipoCambioDetalle.IdTipoCambioDetalle &&
                                                       x.IdEstado == OportunidadTipoCambioDetalle.IdEstado);
            return objOportunidadTipoCambioDetalle.Select(x => new OportunidadTipoCambioDetalleDtoResponse
            {

                IdEstado = x.IdEstado
            }).Single();

        }

        public ProcesoResponse RegistrarOportunidadTipoCambioDetalle(OportunidadTipoCambioDetalleDtoRequest OportunidadTipoCambioDetalle)
        {

            var objOportunidadTipoCambioDetalle = new OportunidadTipoCambioDetalle()
            {

                IdEstado = OportunidadTipoCambioDetalle.IdEstado,
                IdUsuarioCreacion = OportunidadTipoCambioDetalle.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(OportunidadTipoCambioDetalle.FechaCreacion)
            };

            iOportunidadTipoCambioDetalleDal.Add(objOportunidadTipoCambioDetalle);
            iOportunidadTipoCambioDetalleDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidadTipoCambioDetalle.IdTipoCambioDetalle;
            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }
    }
}
