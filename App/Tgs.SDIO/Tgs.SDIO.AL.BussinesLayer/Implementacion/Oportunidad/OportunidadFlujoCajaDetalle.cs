﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoCajaDetalle : IOportunidadFlujoCajaDetalleBl
    {
        readonly IOportunidadFlujoCajaDetalleDal iOportunidadFlujoCajaDetalleDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public OportunidadFlujoCajaDetalle(IOportunidadFlujoCajaDetalleDal IOportunidadFlujoCajaDetalleDal)
        {
            iOportunidadFlujoCajaDetalleDal = IOportunidadFlujoCajaDetalleDal;
        }

        public ProcesoResponse ActualizarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            var oportunidadFlujoCajaDetalle = new Entities.Entities.Oportunidad.OportunidadFlujoCajaDetalle()
            {
                IdFlujoCajaConfiguracion = detalle.IdFlujoCajaConfiguracion,
                Anio = detalle.Anio,
                Mes = detalle.Mes,
                Monto = detalle.Monto,
                IdEstado = detalle.IdEstado,
                IdUsuarioEdicion = detalle.IdUsuarioEdicion,
                FechaEdicion = detalle.FechaEdicion
            };

            iOportunidadFlujoCajaDetalleDal.ActualizarPorCampos(oportunidadFlujoCajaDetalle, x=>x.IdFlujoCajaConfiguracion, x=> x.Anio, x=> x.Mes, x=> x.Monto,x=>x.IdEstado,x=>x.IdUsuarioEdicion,x=> x.FechaEdicion);
            iOportunidadFlujoCajaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (detalle.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarFlujoCajaDetalle : MensajesGeneralOportunidad.ActualizarFlujoCajaDetalle;

            return respuesta;
        }

        public OportunidadFlujoCajaDetalleDtoResponse GeneraProyectadoOportunidadFlujoCaja(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public List<OportunidadFlujoCajaDetalleDtoResponse> ListaOportunidadFlujoCajaConfiguracion(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public List<OportunidadFlujoCajaDetalleDtoResponse> ListarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public OportunidadFlujoCajaDetalleDtoResponse ObtenerOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest flujocaja)
        {
            throw new NotImplementedException();
        }

        public ProcesoResponse RegistrarOportunidadFlujoCajaDetalle(OportunidadFlujoCajaDetalleDtoRequest detalle)
        {
            var oportunidadFlujoCajaDetalle = new Entities.Entities.Oportunidad.OportunidadFlujoCajaDetalle()
            {
                IdFlujoCajaConfiguracion = detalle.IdFlujoCajaConfiguracion,
                Anio = detalle.Anio,
                Mes = detalle.Mes,
                Monto = detalle.Monto,
                IdEstado = detalle.IdEstado,
                IdUsuarioCreacion = detalle.IdUsuarioCreacion,
                FechaCreacion = detalle.FechaCreacion
            };

            iOportunidadFlujoCajaDetalleDal.Add(oportunidadFlujoCajaDetalle);
            iOportunidadFlujoCajaDetalleDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarFlujoCajaDetalle;

            return respuesta;
        }
    }
}
