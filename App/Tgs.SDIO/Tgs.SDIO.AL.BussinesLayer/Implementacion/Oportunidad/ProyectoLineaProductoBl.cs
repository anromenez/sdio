﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class ProyectoLineaProductoBl : IProyectoLineaProductoBl
    {
        readonly IProyectoLineaProductoDal iProyectoLineaProductoDal;
        ProcesoResponse respuesta = new ProcesoResponse(); 

        public ProyectoLineaProductoBl(IProyectoLineaProductoDal IProyectoLineaProductoDal)
        {
            iProyectoLineaProductoDal = IProyectoLineaProductoDal;
        }

        public ProcesoResponse ActualizarProyectoLineaProducto(ProyectoLineaProductoDtoRequest proyectoLineaProducto)
        {
            try
            {
                var objProyecto = new ProyectoLineaProducto()
                {
                    IdProyecto = proyectoLineaProducto.IdProyecto,
                    IdLineaProducto = proyectoLineaProducto.IdLineaProducto,
                    IdEstado = proyectoLineaProducto.IdEstado,
                    IdUsuarioEdicion = proyectoLineaProducto.IdUsuarioEdicion,
                    FechaEdicion = proyectoLineaProducto.FechaEdicion
                };

                iProyectoLineaProductoDal.Modify(objProyecto);
                iProyectoLineaProductoDal.UnitOfWork.Commit();

                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = (proyectoLineaProducto.IdEstado == Estados.Inactivo) ? MensajesGeneralOportunidad.EliminarLineaProducto : MensajesGeneralOportunidad.ActualizarLineaProducto;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }

        public List<ProyectoLineaProductoDtoResponse> ListaProyectoLineaProducto(ProyectoLineaProductoDtoRequest proyectoLineaProducto)
        {
            var objProyecto = iProyectoLineaProductoDal.GetFiltered(p => p.IdProyecto == proyectoLineaProducto.IdProyecto);

            return objProyecto.Select(x => new ProyectoLineaProductoDtoResponse
            {
                IdProyecto = x.IdProyecto,
                IdLineaProducto = x.IdLineaProducto,
                IdEstado = x.IdEstado
            }).ToList();
        }

        public ProcesoResponse RegistrarProyectoLineaProducto(ProyectoLineaProductoDtoRequest proyectoLineaProducto)
        {
            try
            {
                var objProyecto = new ProyectoLineaProducto()
                {
                    IdProyecto = proyectoLineaProducto.IdProyecto,
                    IdLineaProducto = proyectoLineaProducto.IdLineaProducto,
                    IdEstado = proyectoLineaProducto.IdEstado,
                    IdUsuarioCreacion = proyectoLineaProducto.IdUsuarioCreacion,
                    FechaCreacion = proyectoLineaProducto.FechaCreacion
                };

                iProyectoLineaProductoDal.Add(objProyecto);
                iProyectoLineaProductoDal.UnitOfWork.Commit();

                respuesta.Id = objProyecto.IdProyecto;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralOportunidad.RegistrarLineaProducto;
            }
            catch (Exception)
            {
                respuesta.TipoRespuesta = Proceso.Invalido;
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, null, Generales.Sistemas.Oportunidad);
            }

            return respuesta;
        }
    }
}
