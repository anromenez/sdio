﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadFlujoEstadoBl : IOportunidadFlujoEstadoBl
    {
        readonly IOportunidadFlujoEstadoDal iOportunidadFlujoEstadoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadFlujoEstadoBl(IOportunidadFlujoEstadoDal IOportunidadFlujoEstadoDal)
        {
            iOportunidadFlujoEstadoDal = IOportunidadFlujoEstadoDal;
        }



        public ProcesoResponse ActualizarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());

            var objOportunidadFlujoEstado = new OportunidadFlujoEstado()
            {
                IdOportunidadFlujoEstado = oportunidadFlujoEstado.IdOportunidadFlujoEstado,
              
            };

            iOportunidadFlujoEstadoDal.ActualizarPorCampos(objOportunidadFlujoEstado);

            iOportunidadFlujoEstadoDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
          //  respuesta.Mensaje = MensajesGeneralOportunidadFlujoEstado.ActualizarOportunidadFlujoEstado;




            return respuesta;
        }

     

        public List<OportunidadFlujoEstadoDtoResponse> ListarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            throw new NotImplementedException();
        }

        public OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            var objOportunidadFlujoEstado = iOportunidadFlujoEstadoDal.GetFiltered(p => p.IdOportunidadFlujoEstado == oportunidadFlujoEstado.IdOportunidadFlujoEstado);

            return objOportunidadFlujoEstado.Select(x => new OportunidadFlujoEstadoDtoResponse
            {

                IdOportunidadFlujoEstado = x.IdOportunidadFlujoEstado,
              
            }).Single();
        }

        public OportunidadFlujoEstadoDtoResponse ObtenerOportunidadFlujoEstadoId(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {
            OportunidadFlujoEstadoDtoResponse oportunidadFlujoEstadoResponse = new OportunidadFlujoEstadoDtoResponse(); 

            var objOportunidadFlujoEstado = iOportunidadFlujoEstadoDal.GetFiltered(p => p.IdOportunidad == oportunidadFlujoEstado.IdOportunidad).OrderByDescending(t => t.IdOportunidadFlujoEstado).ToList().First();

            oportunidadFlujoEstadoResponse.IdEstado = objOportunidadFlujoEstado.IdEstado;

            return oportunidadFlujoEstadoResponse;
        }

        public ProcesoResponse RegistrarOportunidadFlujoEstado(OportunidadFlujoEstadoDtoRequest oportunidadFlujoEstado)
        {

            DateTime fecha = DateTime.Parse(DateTime.Now.ToLongDateString());


            var objOportunidadFlujoEstado = new OportunidadFlujoEstado()
            {
              FechaCreacion = fecha,
             IdUsuarioCreacion= oportunidadFlujoEstado.IdUsuarioCreacion,
             IdOportunidad= oportunidadFlujoEstado.IdOportunidad,
             IdEstado= oportunidadFlujoEstado.IdEstado
            };


            iOportunidadFlujoEstadoDal.Add(objOportunidadFlujoEstado);
            iOportunidadFlujoEstadoDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidadFlujoEstado.IdOportunidadFlujoEstado;
            respuesta.TipoRespuesta = Proceso.Valido;
          //  respuesta.Mensaje = MensajesGeneralOportunidadFlujoEstado.RegistrarOportunidadFlujoEstado;


            return respuesta;
        }



        
    }
}
