﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class OportunidadTipoCambioBl : IOportunidadTipoCambioBl
    {
        readonly IOportunidadTipoCambioDal iOportunidadTipoCambioDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public OportunidadTipoCambioBl(IOportunidadTipoCambioDal IOportunidadTipoCambioDal)
        {
            iOportunidadTipoCambioDal = IOportunidadTipoCambioDal;
        }

        public ProcesoResponse ActualizarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {

            var objOportunidadTipoCambio = new OportunidadTipoCambio()
            {

                IdEstado = oportunidadTipoCambio.IdEstado,
                IdUsuarioEdicion = oportunidadTipoCambio.IdUsuarioEdicion,
                FechaEdicion = oportunidadTipoCambio.FechaEdicion
            };

            iOportunidadTipoCambioDal.Modify(objOportunidadTipoCambio);
            iOportunidadTipoCambioDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;


            return respuesta;
        }


        public OportunidadTipoCambioDtoResponse ObtenerOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {
            var objOportunidadTipoCambio = iOportunidadTipoCambioDal.GetFiltered(x => x.IdTipoCambioOportunidad == oportunidadTipoCambio.IdTipoCambioOportunidad &&
                                                       x.IdEstado == oportunidadTipoCambio.IdEstado);
            return objOportunidadTipoCambio.Select(x => new OportunidadTipoCambioDtoResponse
            {

                IdEstado = x.IdEstado
            }).Single();

        }


        public ProcesoResponse RegistrarOportunidadTipoCambio(OportunidadTipoCambioDtoRequest oportunidadTipoCambio)
        {

            var objOportunidadTipoCambio = new OportunidadTipoCambio()
            {

                IdEstado = oportunidadTipoCambio.IdEstado,
                IdUsuarioCreacion = oportunidadTipoCambio.IdUsuarioCreacion,
                FechaCreacion = Convert.ToDateTime(oportunidadTipoCambio.FechaCreacion)
            };

            iOportunidadTipoCambioDal.Add(objOportunidadTipoCambio);
            iOportunidadTipoCambioDal.UnitOfWork.Commit();

            respuesta.Id = objOportunidadTipoCambio.IdTipoCambioOportunidad;
            respuesta.TipoRespuesta = Proceso.Valido;

            return respuesta;
        }
    }
}
