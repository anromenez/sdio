﻿using System;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Implementacion.Comun;
using Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Mensajes.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad
{
    public class SubServicioDatosCapexBl : ISubServicioDatosCapexBl
    {
        readonly ISubServicioDatosCapexDal iSubServicioDatosCapexDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public SubServicioDatosCapexBl(ISubServicioDatosCapexDal ISubServicioDatosCapexDal)
        {
            iSubServicioDatosCapexDal = ISubServicioDatosCapexDal;
        }

        public ProcesoResponse ActualizarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var objSubServicio = new SubServicioDatosCapex()
            {
                IdFlujoCaja = SubServicio.IdFlujoCaja,
                IdSubServicioDatosCapex = SubServicio.IdSubServicioDatosCapex,
                Circuito = SubServicio.Circuito,
                SISEGO = SubServicio.SISEGO,
                MesesAntiguedad = SubServicio.MesesAntiguedad,
                CostoUnitarioAntiguo = SubServicio.CostoUnitarioAntiguo,
                ValorResidualSoles = SubServicio.ValorResidualSoles,
                CostoUnitario = SubServicio.CostoUnitario,
                CapexDolares = SubServicio.CapexDolares,
                CapexSoles = SubServicio.CapexSoles,
                TotalCapex = SubServicio.TotalCapex,
                AnioRecupero = SubServicio.AnioRecupero,
                MesRecupero = SubServicio.MesRecupero,
                AnioComprometido = SubServicio.AnioComprometido,
                MesComprometido = SubServicio.MesComprometido,
                AnioCertificado = SubServicio.AnioCertificado,
                MesCertificado = SubServicio.MesCertificado,
                Medio = SubServicio.Medio,
                IdTipo = SubServicio.IdTipo,
                IdOportunidadCosto = SubServicio.IdOportunidadCosto,
                Garantizado = SubServicio.Garantizado,
                IdEstado = SubServicio.IdEstado,
                IdUsuarioEdicion = SubServicio.IdUsuarioEdicion,
                FechaEdicion = SubServicio.FechaEdicion,
                Cruce = SubServicio.Cruce,
                AEReducido = SubServicio.AEReducido,
                Combo = SubServicio.Combo,
                Antiguedad = SubServicio.Antiguedad,
                CapexInstalacion = SubServicio.CapexInstalacion,
                CapexReal = SubServicio.CapexReal,
                CapexTotalReal = SubServicio.CapexTotalReal,
                Marca = SubServicio.Marca,
                Modelo = SubServicio.Modelo,
                Tipo = SubServicio.Tipo
            };

            iSubServicioDatosCapexDal.ActualizarPorCampos(objSubServicio, x => x.IdFlujoCaja, x=> x.IdSubServicioDatosCapex, x => x.Circuito, x => x.SISEGO, x => x.MesesAntiguedad, x => x.CostoUnitarioAntiguo, x => x.ValorResidualSoles, x => x.CostoUnitario, x => x.CapexDolares, x => x.CapexSoles,
                                                x => x.TotalCapex, x => x.AnioRecupero, x => x.MesComprometido, x => x.AnioCertificado, x => x.MesCertificado, x => x.Medio, x => x.IdTipo, x => x.IdOportunidadCosto, x => x.Garantizado, x => x.IdEstado, x => x.IdUsuarioEdicion,
                                                x => x.FechaEdicion, x => x.Cruce, x => x.AEReducido, x => x.Combo, x => x.Antiguedad, x => x.CapexInstalacion, x => x.CapexReal, x => x.CapexTotalReal, x => x.Marca, x => x.Modelo, x => x.Tipo);
            iSubServicioDatosCapexDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (SubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;

            return respuesta;
        }
        
        public SubServicioDatosCapexDtoResponse ListarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var lista = iSubServicioDatosCapexDal.GetFilteredAsNoTracking(x => x.IdEstado == SubServicio.IdEstado &&
                                                                            x.IdSubServicioDatosCapex == SubServicio.IdSubServicioDatosCapex).ToList();
            return lista.Select(x => new SubServicioDatosCapexDtoResponse
            {
                AEReducido = x.AEReducido,
                AnioCertificado = x.AnioCertificado,
                AnioComprometido = x.AnioComprometido,
                AnioRecupero = x.AnioRecupero,
                Antiguedad = x.Antiguedad,
                CapexDolares = x.CapexDolares,
                CapexInstalacion = x.CapexInstalacion,
                CapexReal = x.CapexReal,
                CapexSoles = x.CapexSoles,
                CapexTotalReal = x.CapexTotalReal,
                Circuito = x.Circuito,
                Combo = x.Combo,
                CostoUnitario = x.CostoUnitario,
                CostoUnitarioAntiguo = x.CostoUnitarioAntiguo,
                Cruce = x.Cruce,
                Garantizado = x.Garantizado,
                IdFlujoCaja = x.IdFlujoCaja,
                IdOportunidadCosto = x.IdOportunidadCosto,
                IdSubServicioDatosCapex = x.IdSubServicioDatosCapex,
                IdTipo = x.IdTipo,
                Marca = x.Marca,
                Medio = x.Medio,
                MesCertificado = x.MesCertificado,
                MesComprometido = x.MesComprometido,
                MesesAntiguedad = x.MesesAntiguedad,
                MesRecupero = x.MesRecupero,
                Modelo = x.Modelo,
                SISEGO = x.SISEGO,
                Tipo = x.Tipo,
                TotalCapex = x.TotalCapex,
                ValorResidualSoles = x.ValorResidualSoles
            }).Single();
        }

        public SubServicioDatosCapexDtoResponse ObtenerSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var lista = iSubServicioDatosCapexDal.GetFilteredAsNoTracking(x => x.IdEstado == SubServicio.IdEstado &&
                                                                            x.IdSubServicioDatosCapex == SubServicio.IdSubServicioDatosCapex).ToList();
            return lista.Select(x => new SubServicioDatosCapexDtoResponse
            {
                AEReducido = x.AEReducido,
                AnioCertificado = x.AnioCertificado,
                AnioComprometido = x.AnioComprometido,
                AnioRecupero = x.AnioRecupero,
                Antiguedad = x.Antiguedad,
                CapexDolares = x.CapexDolares,
                CapexInstalacion = x.CapexInstalacion,
                CapexReal = x.CapexReal,
                CapexSoles = x.CapexSoles,
                CapexTotalReal = x.CapexTotalReal,
                Circuito = x.Circuito,
                Combo = x.Combo,
                CostoUnitario = x.CostoUnitario,
                CostoUnitarioAntiguo = x.CostoUnitarioAntiguo,
                Cruce = x.Cruce,
                Garantizado = x.Garantizado,
                IdFlujoCaja = x.IdFlujoCaja,
                IdOportunidadCosto = x.IdOportunidadCosto,
                IdSubServicioDatosCapex = x.IdSubServicioDatosCapex,
                IdTipo = x.IdTipo,
                Marca = x.Marca,
                Medio = x.Medio,
                MesCertificado = x.MesCertificado,
                MesComprometido = x.MesComprometido,
                MesesAntiguedad = x.MesesAntiguedad,
                MesRecupero = x.MesRecupero,
                Modelo = x.Modelo,
                SISEGO = x.SISEGO,
                Tipo = x.Tipo,
                TotalCapex = x.TotalCapex,
                ValorResidualSoles = x.ValorResidualSoles
            }).Single();
        }

        public ProcesoResponse RegistrarSubServicioDatosCapex(SubServicioDatosCapexDtoRequest SubServicio)
        {
            var objSubServicio = new SubServicioDatosCapex()
            {
                IdFlujoCaja = SubServicio.IdFlujoCaja,
                Circuito = SubServicio.Circuito,
                SISEGO = SubServicio.SISEGO,
                MesesAntiguedad = SubServicio.MesesAntiguedad,
                CostoUnitarioAntiguo = SubServicio.CostoUnitarioAntiguo,
                ValorResidualSoles = SubServicio.ValorResidualSoles,
                CostoUnitario = SubServicio.CostoUnitario,
                CapexDolares = SubServicio.CapexDolares,
                CapexSoles = SubServicio.CapexSoles,
                TotalCapex = SubServicio.TotalCapex,
                AnioRecupero = SubServicio.AnioRecupero,
                MesRecupero = SubServicio.MesRecupero,
                AnioComprometido = SubServicio.AnioComprometido,
                MesComprometido = SubServicio.MesComprometido,
                AnioCertificado = SubServicio.AnioCertificado,
                MesCertificado = SubServicio.MesCertificado,
                Medio = SubServicio.Medio,
                IdTipo = SubServicio.IdTipo,
                IdOportunidadCosto = SubServicio.IdOportunidadCosto,
                Garantizado = SubServicio.Garantizado,
                Cruce = SubServicio.Cruce,
                AEReducido = SubServicio.AEReducido,
                Combo = SubServicio.Combo,
                Antiguedad = SubServicio.Antiguedad,
                CapexInstalacion = SubServicio.CapexInstalacion,
                CapexReal = SubServicio.CapexReal,
                CapexTotalReal = SubServicio.CapexTotalReal,
                Marca = SubServicio.Marca,
                Modelo = SubServicio.Modelo,
                Tipo = SubServicio.Tipo,
                IdEstado = SubServicio.IdEstado,
                IdUsuarioCreacion = SubServicio.IdUsuarioCreacion,
                FechaCreacion = SubServicio.FechaCreacion,
            };

            iSubServicioDatosCapexDal.Add(objSubServicio);
            iSubServicioDatosCapexDal.UnitOfWork.Commit();

            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = (SubServicio.IdEstado == Estados.Inactivo) ? MensajesGeneralComun.EliminarSubServicio : MensajesGeneralComun.ActualizarSubServicio;

            ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorSubServicio, null, Generales.Sistemas.Oportunidad);

            return respuesta;
        }

        public SubServicioDatosCapexDtoResponse CantidadEquiposEstudiosEsp(SubServicioDatosCapexDtoRequest dto) {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdDepreciacion == subServicio.IdDepreciacion).Single();            

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int meses = Convert.ToInt32(depreciacion.Meses);
            decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);

            datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : 0;
            datosCapex.CapexDolares = Convert.ToDecimal(dto.Cu * dto.Cantidad);
            datosCapex.CapexSoles = (datosCapex.CapexDolares * Convert.ToDecimal(tipoCambio.Monto));
            datosCapex.TotalCapex = datosCapex.CapexSoles;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadRouters(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
            var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdMaestra == 153).Single(); //153 = instalacion

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int meses = Convert.ToInt32(depreciacion.Meses);
            decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            int Instalacion = Convert.ToInt32(instalacion.Valor);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : 0;
            datosCapex.CapexDolares = Convert.ToDecimal(dto.Cu * dto.Cantidad);
            datosCapex.CapexSoles = (datosCapex.CapexDolares * Convert.ToDecimal(tipoCambio.Monto));
            datosCapex.TotalCapex = (dto.Indice == 1) ? datosCapex.CapexSoles : ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadModems(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
            var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdMaestra == 153).Single(); //153 = instalacion

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int meses = Convert.ToInt32(depreciacion.Meses);
            decimal valorResidual = Decimal.Divide(dto.CuAntiguiedad, meses) * (meses - dto.MesesAntiguedad);
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            int Instalacion = Convert.ToInt32(instalacion.Valor);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;
            datosCapex.CapexDolares = decimal.Round((Convert.ToDecimal(dto.Cu) * dto.Cantidad), 2);
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            datosCapex.TotalCapex = (dto.Indice == 1) ? datosCapex.CapexSoles : ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadEquiposSeguridad(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
            var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdMaestra == 153).Single(); //153 = instalacion

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int meses = Convert.ToInt32(depreciacion.Meses);
            decimal valorResidual = Numeric.Cero;
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            int Instalacion = Convert.ToInt32(instalacion.Valor);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? decimal.Round(valorResidual, 2) : Numeric.Cero;
            datosCapex.CapexDolares = dto.Cu * dto.Cantidad;
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            datosCapex.TotalCapex = datosCapex.CapexSoles;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadCapex(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
            var instalacion = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdMaestra == 153).Single(); //153 = instalacion

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int meses = Convert.ToInt32(depreciacion.Meses);
            decimal valorResidual = Numeric.Cero;
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            int Instalacion = Convert.ToInt32(instalacion.Valor);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);

            datosCapex.ValorResidualSoles = (dto.Indice == Numeric.Uno) ? Convert.ToDecimal(valorResidual.ToString("#.00")) : Numeric.Cero;
            datosCapex.CapexDolares = dto.Cu * dto.Cantidad;
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            datosCapex.TotalCapex = (dto.Indice == Numeric.Uno) ? datosCapex.CapexSoles : ((dto.Cantidad * Instalacion) * tc) + datosCapex.CapexSoles;

            return datosCapex;
        }
        
        public SubServicioDatosCapexDtoResponse CantidadSolarWindVPN(SubServicioDatosCapexDtoRequest dto)
        {
            var context = new DioContext();
            var datosCapex = new SubServicioDatosCapexDtoResponse();

            var flujoCajaDto = new OportunidadFlujoCajaDtoRequest();
            flujoCajaDto.IdSubServicioDatosCapex = dto.IdSubServicioDatosCapex;
            flujoCajaDto.IdEstado = dto.IdEstado;
            flujoCajaDto.IdGrupo = dto.IdGrupo;
            var flujoCaja = new OportunidadFlujoCajaDal(context).ObtenerDetalleOportunidadEcapex(flujoCajaDto);
            var subServicio = new SubServicioDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdSubServicio == flujoCaja.IdSubServicio).Single();
            var depreciacion = new DepreciacionDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdDepreciacion == subServicio.IdDepreciacion).Single();
            var solarw = new MaestraDal(context).GetFiltered(x => x.IdEstado == Generales.Estados.Activo
                                        && x.IdMaestra == 108).Single(); //108 = solarwind

            OportunidadFlujoCajaDtoRequest auxTipoCambio = new OportunidadFlujoCajaDtoRequest();
            auxTipoCambio.IdOportunidadLineaNegocio = flujoCaja.IdOportunidadLineaNegocio;

            int solarwind = Convert.ToInt32(solarw.Valor);
            var tipoCambio = iSubServicioDatosCapexDal.ObtenerTipoCambio(auxTipoCambio);
            decimal tc = Convert.ToDecimal(tipoCambio.Monto);
            
            datosCapex.CapexDolares = solarwind * dto.Cantidad;
            datosCapex.CapexSoles = (datosCapex.CapexDolares * tc);
            datosCapex.TotalCapex = datosCapex.CapexSoles;

            return datosCapex;
        }

    }
}
