﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
   public class ConceptoSeguimientoBl : IConceptoSeguimientoBl
    {
        readonly IConceptoSeguimientoDal iconceptoSeguimientoDal;
        ProcesoResponse respuesta = new ProcesoResponse();
        public ConceptoSeguimientoBl(IConceptoSeguimientoDal conceptoSeguimientoDal)
        {
            iconceptoSeguimientoDal = conceptoSeguimientoDal;
        }

        public List<ListaDtoResponse> ListarComboConceptoSeguimiento()
        {
            return iconceptoSeguimientoDal.ListarComboConceptoSeguimiento();
        }
        public ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoDal.ListadoConceptosSeguimientoPaginado(request);
        }

        public ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request)
        {
            var query = iconceptoSeguimientoDal.GetFilteredAsNoTracking(x => x.IdConcepto == request.IdConcepto && (x.IdEstado == EstadosEntidad.Activo || x.IdEstado == EstadosEntidad.Inactivo)).ToList();
            return query.Select(x => new ConceptoSeguimientoDtoResponse
            {
                IdConcepto = x.IdConcepto,
                IdConceptoPadre = x.IdConceptoPadre,
                Descripcion = x.Descripcion,
                Nivel = x.Nivel,
                OrdenVisual = x.OrdenVisual,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
                        var areaAntigua = ObtenerConceptoSeguimientoPorId(request);
                        var areaSeguimiento = new ConceptoSeguimiento()
                        {
                            IdConcepto = request.IdConcepto,
                            IdConceptoPadre = request.IdConceptoPadre,
                            Descripcion = request.Descripcion,
                            Nivel = request.Nivel,
                            OrdenVisual = request.OrdenVisual,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now,
                        };
                            iconceptoSeguimientoDal.Modify(areaSeguimiento);
                            iconceptoSeguimientoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActivoRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
                        var areaSeguimiento = new ConceptoSeguimiento()
                        {
                            IdConceptoPadre = request.IdConceptoPadre,
                            Descripcion = request.Descripcion,
                            Nivel = request.Nivel,
                            OrdenVisual = request.OrdenVisual,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = request.IdUsuario,
                            FechaCreacion = DateTime.Now
                        };
                            iconceptoSeguimientoDal.Add(areaSeguimiento);
                            iconceptoSeguimientoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
                        var areaAntigua = ObtenerConceptoSeguimientoPorId(request);
                        var areaSeguimiento = new ConceptoSeguimiento()
                        {
                            IdConcepto = request.IdConcepto,
                            IdConceptoPadre = request.IdConceptoPadre,
                            Descripcion = areaAntigua.Descripcion,
                            Nivel = areaAntigua.Nivel,
                            OrdenVisual = areaAntigua.OrdenVisual,
                            IdEstado = EstadosEntidad.Eliminado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now
                        };
                            iconceptoSeguimientoDal.Modify(areaSeguimiento);
                            iconceptoSeguimientoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }



    }
}
