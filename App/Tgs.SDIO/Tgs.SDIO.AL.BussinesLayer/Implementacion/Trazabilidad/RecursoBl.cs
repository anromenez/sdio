﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;
namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class RecursoBl : IRecursoBl
    {
        readonly IRecursoDal irecursoDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public RecursoBl(IRecursoDal recursoDal)
        {
            irecursoDal = recursoDal;
        }

        public RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request)
        {
            return irecursoDal.ListadoRecursosPaginado(request);
        }

        public RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            return irecursoDal.ListaRecursoModalRecursoPaginado(request);
        }

        public RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request)
        {
            var query = irecursoDal.GetFilteredAsNoTracking(x => x.IdRecurso == request.IdRecurso && (x.IdEstado == EstadosEntidad.Activo || x.IdEstado == EstadosEntidad.Inactivo)).ToList();
            return query.Select(x => new RecursoDtoResponse
            {
                IdRecurso = x.IdRecurso,
                TipoRecurso = x.TipoRecurso,
                UserNameSF = x.UserNameSF,
                Nombre = x.Nombre,
                DNI = x.DNI,
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }

        public ProcesoResponse ActualizarRecurso(RecursoDtoRequest request)
        {
                        var areaAntigua = ObtenerRecursoPorId(request);
                        var areaSeguimiento = new Recurso()
                        {
                            IdRecurso = request.IdRecurso,
                            TipoRecurso = request.TipoRecurso,
                            UserNameSF = request.UserNameSF,
                            Nombre = request.Nombre,
                            DNI = request.DNI,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now,
                        };
                            irecursoDal.Modify(areaSeguimiento);
                            irecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActualizaRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarRecurso(RecursoDtoRequest request)
        {
                        var areaSeguimiento = new Recurso()
                        {
                            TipoRecurso = request.TipoRecurso,
                            UserNameSF = request.UserNameSF,
                            Nombre = request.Nombre,
                            DNI = request.DNI,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = request.IdUsuario,
                            FechaCreacion = DateTime.Now
                        };
                            irecursoDal.Add(areaSeguimiento);
                            irecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarRecurso(RecursoDtoRequest request)
        {
                        var areaAntigua = ObtenerRecursoPorId(request);
                        var areaSeguimiento = new Recurso()
                        {
                            IdRecurso = request.IdRecurso,
                            TipoRecurso = areaAntigua.TipoRecurso,
                            UserNameSF = areaAntigua.UserNameSF,
                            Nombre = areaAntigua.Nombre,
                            DNI = areaAntigua.DNI,
                            IdEstado = EstadosEntidad.Eliminado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now
                        };
                            irecursoDal.Modify(areaSeguimiento);
                            irecursoDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }

    }
}
