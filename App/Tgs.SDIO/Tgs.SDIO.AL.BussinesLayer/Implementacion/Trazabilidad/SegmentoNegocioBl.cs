﻿using System.Collections.Generic;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;


namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class SegmentoNegocioBl : ISegmentoNegocioBl
    {
        readonly ISegmentoNegocioDal isegmentoNegocioDal;
        public SegmentoNegocioBl(ISegmentoNegocioDal segmentoNegocioDal)
        {
            isegmentoNegocioDal = segmentoNegocioDal;
        }

        public List<ComboDtoResponse> ListarComboSegmentoNegocio()
        {
            return isegmentoNegocioDal.ListarComboSegmentoNegocio();
        }

        public List<ListaDtoResponse> ListarComboSegmentoNegocioSimple()
        {
            return isegmentoNegocioDal.ListarComboSegmentoNegocioSimple();
        }
    }
}
