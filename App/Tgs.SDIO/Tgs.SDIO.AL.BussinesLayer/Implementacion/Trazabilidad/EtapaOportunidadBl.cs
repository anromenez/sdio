﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class EtapaOportunidadBl : IEtapaOportunidadBl
    {
        readonly IEtapaOportunidadDal ietapaOportunidadDal;

        public EtapaOportunidadBl(IEtapaOportunidadDal etapaOportunidadDal)
        {
            ietapaOportunidadDal = etapaOportunidadDal;
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {
            return ietapaOportunidadDal.ListarComboEtapaOportunidad();
        }

        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseOportunidadDtoRequest request)
        {
            return ietapaOportunidadDal.ListarComboEtapaOportunidadPorIdFase(request);
        }
    }
}
