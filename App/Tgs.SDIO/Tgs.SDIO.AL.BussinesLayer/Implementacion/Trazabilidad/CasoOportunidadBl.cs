﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;


namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
   public class CasoOportunidadBl : ICasoOportunidadBl
    {
        readonly ICasoOportunidadDal icasoOportunidadDal;

        public CasoOportunidadBl(ICasoOportunidadDal casoOportunidadDal)
        {
            icasoOportunidadDal = casoOportunidadDal;
        }

        public CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request)
        {
            var query = icasoOportunidadDal.GetFilteredAsNoTracking(x => x.IdCaso == request.IdCaso && (x.IdEstado == EstadosEntidad.Activo || x.IdEstado == EstadosEntidad.Inactivo)).ToList();
            return query.Select(x => new CasoOportunidadDtoResponse
            {
                IdCaso = x.IdCaso,
                IdOportunidad = x.IdOportunidad,
                Descripcion = x.Descripcion,
                IdCasoSF = x.IdCasoSF,
                IdCasoPadre = x.IdCasoPadre,
                IdTipoSolicitud = x.IdTipoSolicitud,
                Asunto = x.Asunto,
                IdFase = x.IdFase,
                IdEtapa = x.IdEtapa,
                Complejidad = x.Complejidad,
                Prioridad = x.Prioridad,
                IdEstadoCaso = x.IdEstadoCaso,
                SolucionCaso = x.SolucionCaso,
                FechaApertura = x.FechaApertura,
                FechaCierre = x.FechaCierre,
                FechaCompromisoAtencion = x.FechaCompromisoAtencion,               
                IdEstado = x.IdEstado,
                IdUsuarioCreacion = x.IdUsuarioCreacion,
                FechaCreacion = x.FechaCreacion,
                IdUsuarioEdicion = x.IdUsuarioEdicion,
                FechaEdicion = x.FechaEdicion
            }).FirstOrDefault();
        }
    }
}
