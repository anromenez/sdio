﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class RecursoEquipoTrabajoBl : IRecursoEquipoTrabajoBl
    {
        readonly IRecursoEquipoTrabajoDal irecursoEquipoTrabajoDal;

        public RecursoEquipoTrabajoBl(IRecursoEquipoTrabajoDal recursoEquipoTrabajoDal)
        {
            irecursoEquipoTrabajoDal = recursoEquipoTrabajoDal;
        }
    }
}
