﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.Util.Mensajes.Trazabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using static Tgs.SDIO.Util.Constantes.Trazabilidad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
    public class ActividadOportunidadBl : IActividadOportunidadBl
    {
        readonly IActividadOportunidadDal iactividadOportunidadDal;
        readonly IActividadSegmentoNegocioDal iactividadSegmentoNegocioDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public ActividadOportunidadBl(IActividadOportunidadDal actividadOportunidadDal, IActividadSegmentoNegocioDal actividadSegmentoNegocioDal)
        {
            iactividadOportunidadDal = actividadOportunidadDal;
            iactividadSegmentoNegocioDal = actividadSegmentoNegocioDal;
        }

        public ActividadOportunidadPaginadoDtoResponse ListatActividadOportunidadPaginado(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadDal.ListatActividadOportunidadPaginado(request);
        }

        public ActividadOportunidadDtoResponse ObtenerActividadOportunidadPorId(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadDal.ObtenerActividadOportunidadPorId(request);
        }

        public List<ListaDtoResponse> ListarComboActividadPadres()
        {
            return iactividadOportunidadDal.ListarComboActividadPadres();
        }

        public ProcesoResponse ActualizarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {

                        var actividadAntigua = ObtenerActividadOportunidadPorId(request);
                        var actividadSeguimiento = new ActividadOportunidad()
                        {
                            IdActividad = request.IdActividad,
                            IdActividadPadre = request.IdActividadPadre,
                            Descripcion = request.Descripcion,
                            Predecesoras = request.Predecesoras,
                            IdFase = request.IdFase,
                            IdEtapa = request.IdEtapa,
                            FlgCapexMayor = request.FlgCapexMayor,
                            FlgCapexMenor = request.FlgCapexMenor,
                            IdAreaSeguimiento = request.IdAreaSeguimiento,
                            NumeroDiaCapexMenor = request.NumeroDiaCapexMenor,
                            CantidadDiasCapexMenor = request.CantidadDiasCapexMenor,
                            NumeroDiaCapexMayor = request.NumeroDiaCapexMayor,
                            CantidadDiasCapexMayor = request.CantidadDiasCapexMayor,
                            IdTipoActividad = request.IdTipoActividad,
                            AsegurarOferta = request.AsegurarOferta,
                            IdEstado = request.IdEstado,
                            IdUsuarioCreacion = actividadAntigua.IdUsuarioCreacion,
                            FechaCreacion = actividadAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now,
                        };
                            iactividadOportunidadDal.Modify(actividadSeguimiento);
                            iactividadOportunidadDal.UnitOfWork.Commit();
                            var segmentos = iactividadSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdActividad == request.IdActividad && x.IdEstado == EstadosEntidad.Activo).ToList();
                                            iactividadSegmentoNegocioDal.Remove(segmentos);
                        foreach (ComboDtoResponse segmento in request.ListSegmentoNegocio)
                        {
                            var actividadSegmentoNegocio = new ActividadSegmentoNegocio()
                            {
                                IdActividad = actividadSeguimiento.IdActividad,
                                IdSegmentoNegocio = Convert.ToInt32(segmento.id),
                                IdEstado = EstadosEntidad.Activo,
                                IdUsuarioCreacion = request.IdUsuario,
                                FechaCreacion = DateTime.Now
                            };
                            iactividadSegmentoNegocioDal.Add(actividadSegmentoNegocio);
                        }
                            iactividadSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.ActivoRegistro;

            return respuesta;
        }

        public ProcesoResponse RegistrarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {
                var actividadSeguimiento = new ActividadOportunidad()
                            {
                                IdActividadPadre = request.IdActividadPadre,
                                Descripcion = request.Descripcion,
                                Predecesoras = request.Predecesoras,
                                IdFase = request.IdFase,
                                IdEtapa = request.IdEtapa,
                                FlgCapexMayor = request.FlgCapexMayor,
                                FlgCapexMenor = request.FlgCapexMenor,
                                IdAreaSeguimiento = request.IdAreaSeguimiento,
                                NumeroDiaCapexMenor = request.NumeroDiaCapexMenor,
                                CantidadDiasCapexMenor = request.CantidadDiasCapexMenor,
                                NumeroDiaCapexMayor = request.NumeroDiaCapexMayor,
                                CantidadDiasCapexMayor = request.CantidadDiasCapexMayor,
                                IdTipoActividad = request.IdTipoActividad,
                                AsegurarOferta = request.AsegurarOferta,
                                IdEstado = request.IdEstado,
                                IdUsuarioCreacion = request.IdUsuario,
                                FechaCreacion = DateTime.Now
                            };
                                iactividadOportunidadDal.Add(actividadSeguimiento);
                                iactividadOportunidadDal.UnitOfWork.Commit();
                        foreach (ComboDtoResponse segmento in request.ListSegmentoNegocio)
                            {
                                var actividadSegmentoNegocio = new ActividadSegmentoNegocio()
                                {
                                    IdActividad = actividadSeguimiento.IdActividad,
                                    IdSegmentoNegocio = Convert.ToInt32(segmento.id),
                                    IdEstado = EstadosEntidad.Activo,
                                    IdUsuarioCreacion = request.IdUsuario,
                                    FechaCreacion = DateTime.Now
                                };
                                iactividadSegmentoNegocioDal.Add(actividadSegmentoNegocio);

                            }
                                iactividadSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.RegistraOk;

            return respuesta;
        }

        public ProcesoResponse EliminarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {
                        var areaAntigua = ObtenerActividadOportunidadPorId(request);
                        var actividad = new ActividadOportunidad()
                        {
                            IdActividad = request.IdActividad,
                            IdActividadPadre = areaAntigua.IdActividadPadre,
                            Descripcion = areaAntigua.Descripcion,
                            Predecesoras = areaAntigua.Predecesoras,
                            IdFase = areaAntigua.IdFase,
                            IdEtapa = areaAntigua.IdEtapa,
                            FlgCapexMayor = areaAntigua.FlgCapexMayor,
                            FlgCapexMenor = areaAntigua.FlgCapexMenor,
                            IdAreaSeguimiento = areaAntigua.IdAreaSeguimiento,
                            NumeroDiaCapexMenor = areaAntigua.NumeroDiaCapexMenor,
                            CantidadDiasCapexMenor = areaAntigua.CantidadDiasCapexMenor,
                            NumeroDiaCapexMayor = areaAntigua.NumeroDiaCapexMayor,
                            CantidadDiasCapexMayor = areaAntigua.CantidadDiasCapexMayor,
                            IdTipoActividad = areaAntigua.IdTipoActividad,
                            AsegurarOferta = areaAntigua.AsegurarOferta,
                            IdEstado = EstadosEntidad.Eliminado,
                            IdUsuarioCreacion = areaAntigua.IdUsuarioCreacion,
                            FechaCreacion = areaAntigua.FechaCreacion,
                            IdUsuarioEdicion = request.IdUsuario,
                            FechaEdicion = DateTime.Now

                        };
                        iactividadOportunidadDal.Modify(actividad);
                        iactividadOportunidadDal.UnitOfWork.Commit();
                        var segmentos = iactividadSegmentoNegocioDal.GetFilteredAsNoTracking(x => x.IdActividad == request.IdActividad && x.IdEstado == EstadosEntidad.Activo).ToList();
                        foreach (ActividadSegmentoNegocio segmento in segmentos)
                        {
                            var actividadSegmento = new ActividadSegmentoNegocio()
                            {
                                IdActividadSegmentoNegocio = segmento.IdActividadSegmentoNegocio,
                                IdActividad = segmento.IdActividad,
                                IdSegmentoNegocio = segmento.IdSegmentoNegocio,
                                IdEstado = EstadosEntidad.Eliminado,
                                IdUsuarioCreacion = segmento.IdUsuarioCreacion,
                                FechaCreacion = segmento.FechaCreacion,
                                IdUsuarioEdicion = request.IdUsuario,
                                FechaEdicion = DateTime.Now
                            };
                            iactividadSegmentoNegocioDal.Add(actividadSegmento);
                        }
                            iactividadSegmentoNegocioDal.UnitOfWork.Commit();
            respuesta.TipoRespuesta = Proceso.Valido;
            respuesta.Mensaje = General.EliminaRegistro;

            return respuesta;
        }
    }
}
