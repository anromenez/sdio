﻿using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using System.Collections.Generic;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad
{
   public class FaseOportunidadBl : IFaseOportunidadBl
    {
        readonly IFaseOportunidadDal ifaseOportunidadDal;

        public FaseOportunidadBl(IFaseOportunidadDal faseOportunidadDal)
        {
            ifaseOportunidadDal = faseOportunidadDal;
        }

        public List<ListaDtoResponse> ListarComboFaseOportunidad()
        {
            return ifaseOportunidadDal.ListarComboFaseOportunidad();
        }
    }
}
