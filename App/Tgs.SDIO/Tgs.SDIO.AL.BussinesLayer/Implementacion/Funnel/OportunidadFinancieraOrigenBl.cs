﻿using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Funnel
{
    public class OportunidadFinancieraOrigenBl : IOportunidadFinancieraOrigenBl
    {
        readonly IOportunidadFinancieraOrigenDal iOportunidadFinancieraOrigenDal;
        readonly ISalesForceConsolidadoCabeceraDal iSalesForceConsolidadoCabeceraDal;

        public OportunidadFinancieraOrigenBl(IOportunidadFinancieraOrigenDal IOportunidadFinancieraOrigenDal,
            ISalesForceConsolidadoCabeceraDal ISalesForceConsolidadoCabeceraDal)
        {
            iOportunidadFinancieraOrigenDal = IOportunidadFinancieraOrigenDal;
            iSalesForceConsolidadoCabeceraDal = ISalesForceConsolidadoCabeceraDal;
        }

        public IndicadorMadurezDtoResponse ListarIndicadorMadurez(IndicadorMadurezDtoRequest request)
        {
            request.Mes = request.Mes.Equals(0) ? null : request.Mes;
            request.IdLineaNegocio = request.IdLineaNegocio.Equals(0) ? null : request.IdLineaNegocio;
            request.IdSector = request.IdSector.Equals(0) ? null : request.IdSector;
            request.ProbabilidadExito = request.ProbabilidadExito.Equals(-1) ? null : request.ProbabilidadExito;
            request.Etapa = request.Etapa.Equals("0") ? null : request.Etapa;

            var indicadorMadurez = iOportunidadFinancieraOrigenDal.ListarIndicadorMadurez(request);
            var indicadorMadurezCostos = iSalesForceConsolidadoCabeceraDal.ListarIndicadorCostoOportunidad(request);

            return new IndicadorMadurezDtoResponse { ListaMadurez = indicadorMadurez, ListaMadurezCostos = indicadorMadurezCostos };       
        }

        public DashoardDtoResponse MostrarDashboard(DashoardDtoRequest request)
        {
            var requestListaMadurez = new IndicadorMadurezDtoRequest { Anio = request.Anio };
            var indicadorMadurez = iOportunidadFinancieraOrigenDal.ListarIndicadorMadurez(requestListaMadurez);
            var indicadorOfertasLineaNegocio = iOportunidadFinancieraOrigenDal.ListarIndicadorOfertasLineaNegocio(request);
            var indicadorOfertasSector = iOportunidadFinancieraOrigenDal.ListarIndicadorOfertasSector(request);
            var indicadorIngresosLineaNegocio = iOportunidadFinancieraOrigenDal.ListarIndicadorIngresosLineaNegocio(request);
            var indicadorIngresosSector = iOportunidadFinancieraOrigenDal.ListarIndicadorIngresosSector(request);
            var indicadorOfertasEstado = iOportunidadFinancieraOrigenDal.ListarIndicadorOfertasEstado(request);
            var indicadorTotales = iOportunidadFinancieraOrigenDal.ListarIndicadoresTotales(request).FirstOrDefault();

            return new DashoardDtoResponse
            {
                ListaMadurez = indicadorMadurez.Select(x => new DashoardMadurezDtoResponse
                {
                    Madurez = x.Madurez,
                    Mes = x.Mes,
                    Anio = request.Anio,
                    IdOportunidad = x.IdOportunidad,
                    Capex = x.Capex
                }).ToList(),
                OfertasLineaNegocio = indicadorOfertasLineaNegocio.ToList(),
                OfertasSector = indicadorOfertasSector.ToList(),
                IngresosLineaNegocio = indicadorIngresosLineaNegocio.ToList(),
                IngresosSector = indicadorIngresosSector.ToList(),
                OfertasEstado = indicadorOfertasEstado.ToList(),
                OportunidadesTrabajadas = indicadorTotales.OportTrabajadas,
                IngresoTotal = indicadorTotales.SumIngresoAnual,
                CapexTotal = indicadorTotales.SumCapex,
                OpexTotal = 0,
                OibdaTotal = indicadorTotales.SumOibdaAnual
            };
        }

    }
}

