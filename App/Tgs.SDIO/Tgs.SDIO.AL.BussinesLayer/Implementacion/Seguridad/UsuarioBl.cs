﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tgs.SDIO.AL.AgenteServicio;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.AL.ProxyServicio;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad
{
    public class UsuarioBl : IUsuarioBl
    {
        private readonly IPerfilBl iPerfilBl;
        public UsuarioBl(IPerfilBl perfilBl)
        { 
            iPerfilBl = perfilBl;
        }

        public UsuarioDtoResponse ObtenerUsuarioPorLogin(string login)
        {
            var usuarioProxy = AgenteServicioUsuario.ObtenerUsuarioPorLogin(login);

            return new UsuarioDtoResponse()
            {
                Apellidos = usuarioProxy.Apellidos_Usuario,
                CodigoCip = usuarioProxy.CIP_Usuario,
                CorreoElectronico = usuarioProxy.Correo_Electronico,
                FechaCaducidad = usuarioProxy.Fecha_Caducidad,
                FechaCambioPassword = usuarioProxy.Fecha_Caducidad,
                FechaModoficacion = usuarioProxy.FH_Modifica,
                FechaRegistro = usuarioProxy.FH_Registro,
                FechaUltimoAcceso = usuarioProxy.FH_Ultimo_Acceso,
                FlagSolicitarCambioClave = usuarioProxy.Flag_Solicitar_Cambio_Clave,
                FlagUsuarioBloqueado = usuarioProxy.Flag_Usuario_Bloqueado,
                IdEmpresa = usuarioProxy.IdEmpresa,
                IdEstadoRegistro = usuarioProxy.IdEstadoRegistro,
                IdUsuario = usuarioProxy.IdUsuario,
                IdUsuarioModificacion = usuarioProxy.IdUsuario_Modifica,
                IdUsuarioRegistro = usuarioProxy.IdUsuario_Registro,
                IdUsuarioSuperior = usuarioProxy.IdUsuario_Superior,
                IntentosFallidos = usuarioProxy.Numero_Intentos_Fallidos,
                Login = usuarioProxy.Login,
                LoginWindows = usuarioProxy.Login_Windows,
                Nombres = usuarioProxy.Nombre_Usuario,
                Password = usuarioProxy.Password,
                TipoUsuario = usuarioProxy.Tipo_Usuario
            };
        }

        public UsuarioDtoResponse ObtenerUsuarioPorIdUsuario(int idUsuario)
        {
            var usuarioProxy = AgenteServicioUsuario.ObtenerUsuarioPorIdUsuario(idUsuario);

            return new UsuarioDtoResponse()
            {
                Apellidos = usuarioProxy.Apellidos_Usuario,
                CodigoCip = usuarioProxy.CIP_Usuario,
                CorreoElectronico = usuarioProxy.Correo_Electronico,
                FechaCaducidad = usuarioProxy.Fecha_Caducidad,
                FechaCambioPassword = usuarioProxy.Fecha_Caducidad,
                FechaModoficacion = usuarioProxy.FH_Modifica,
                FechaRegistro = usuarioProxy.FH_Registro,
                FechaUltimoAcceso = usuarioProxy.FH_Ultimo_Acceso,
                FlagSolicitarCambioClave = usuarioProxy.Flag_Solicitar_Cambio_Clave,
                FlagUsuarioBloqueado = usuarioProxy.Flag_Usuario_Bloqueado,
                IdEmpresa = usuarioProxy.IdEmpresa,
                IdEstadoRegistro = usuarioProxy.IdEstadoRegistro,
                IdUsuario = usuarioProxy.IdUsuario,
                IdUsuarioModificacion = usuarioProxy.IdUsuario_Modifica,
                IdUsuarioRegistro = usuarioProxy.IdUsuario_Registro,
                IdUsuarioSuperior = usuarioProxy.IdUsuario_Superior,
                IntentosFallidos = usuarioProxy.Numero_Intentos_Fallidos,
                Login = usuarioProxy.Login,
                LoginWindows = usuarioProxy.Login_Windows,
                Nombres = usuarioProxy.Nombre_Usuario,
                Password = usuarioProxy.Password,
                NombresCompletos = usuarioProxy.Nombre_Usuario + "" + usuarioProxy.Apellidos_Usuario,
                TipoUsuario = usuarioProxy.Tipo_Usuario
            };
        }

        public UsuarioDtoResponse ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            var dataSet = AgenteServicioUsuario.ValidarUsuario(usuarioDtoRequest);

            UsuarioDtoResponse seguridad = new UsuarioDtoResponse();

            if (dataSet.Tables[0].Rows.Count >= 0)
            {
                if (dataSet.Tables[0].Rows[0]["CodigoResultado"].ToString().Equals("0"))
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        Login = usuarioDtoRequest.Login.Trim().ToUpper(),
                        Usuariosistema =
                            !string.IsNullOrEmpty(dataSet.Tables[0].Rows[0][5].ToString())
                                ? Convert.ToInt16(dataSet.Tables[0].Rows[0][5].ToString())
                                : Convert.ToInt16(dataSet.Tables[0].Rows[0][3].ToString()),
                        CodigoError = 0
                    };
                }
                else
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        MensajeError = dataSet.Tables[0].Rows[0][1].ToString(),
                        CodigoError = -1
                    };
                }
            }

            return seguridad;
        }
         
        public string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            usuarioDtoRequest.CorreoElectronico = Configuracion.CorreoSalida;
            usuarioDtoRequest.UsuarioEnvia = Configuracion.NombreSistemaCorreoSalida;
            return AgenteServicioUsuario.EnvioClaveUsuario(usuarioDtoRequest);
        }

        public List<EntidadDtoResponse> ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            var entidadProxy = AgenteServicioUsuario.ObtenerAmbitoUsuario(usuarioDtoRequest);

            var listado = entidadProxy.Select(item => new EntidadDtoResponse()
            {
                CodigoEntidad = item.CodigoEntidad,
                DescripcionEntidad = item.DescripcionEntidad,
                IdEntidad = item.IdEntidad,
                NombreEntidad = item.NombreEntidad,
                TipoEntidad = item.TipoEntidad
            }).ToList();

            return listado;
        }

        public string RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return AgenteServicioUsuario.RegistrarUsuario(usuarioDtoRequest);
        }

        public UsuarioDtoResponse CambiarPassword(UsuarioDtoRequest usuarioDtoRequest)
        {
            var dataSet = AgenteServicioUsuario.CambiarPassword(usuarioDtoRequest);

            UsuarioDtoResponse seguridad = new UsuarioDtoResponse();

            if (dataSet.Tables[0].Rows.Count >= 0)
            {
                if (dataSet.Tables[0].Rows[0]["CodigoResultado"].ToString().Equals("0"))
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        MensajeError = dataSet.Tables[0].Rows[0][1].ToString(),
                        CodigoError = 0
                    };
                }
                else
                {
                    seguridad = new UsuarioDtoResponse
                    {
                        MensajeError = dataSet.Tables[0].Rows[0][1].ToString(),
                        CodigoError = -1
                    };
                }
            }

            return seguridad;
        }

        public UsuarioDtoResponse ObtenerUsuarioPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            var perfilesRais = iPerfilBl.ObtenerPerfiles(usuarioDtoRequest.IdUsuarioSistema);

            var usuarioRais = ObtenerUsuarioPorLogin(usuarioDtoRequest.Login);

            usuarioRais.Login = usuarioDtoRequest.Login;

            usuarioRais.IdUsuarioSistema = usuarioDtoRequest.IdUsuarioSistema;

            usuarioRais.PerfilDtoResponseLista = perfilesRais;

            return usuarioRais;
        }

        public List<UsuarioDtoResponse> ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
              var dataset =  AgenteServicioUsuario.ObtenerUsuariosPorPerfil(usuarioDtoRequest);
         

            var listado = (from DataRow dataRow in dataset.Tables[0].Rows
                           select new UsuarioDtoResponse()
                           {
                               Apellidos = dataRow["Apellidos_Usuario"].ToString(),
                               Nombres = dataRow["Nombre_Usuario"].ToString(),
                               CodigoCip = dataRow["CIP_Usuario"].ToString(),
                               IdUsuario = Convert.ToInt32(dataRow["IdUsuario"]),
                               Login = dataRow["Login"].ToString(),
                               TipoUsuario = Convert.ToInt32(dataRow["Tipo_Usuario"]),
                               NombresCompletos = dataRow["Nombre_Usuario"].ToString() + " " + dataRow["Apellidos_Usuario"].ToString(),
                               CorreoElectronico = dataRow["Correo_Electronico"].ToString(),
                           }).ToList();
            return listado;       
        }

        public UsuarioDtoResponse ListarUsuarios(UsuarioDtoRequest usuarioDtoRequest)
        {
            UsuarioDtoResponse respuesta = new UsuarioDtoResponse();
            usuarioDtoRequest.IdEmpresa = 1;
            usuarioDtoRequest.IdEstadoRegistro = -1;
            usuarioDtoRequest.IdUsuarioSistema = 1;
            usuarioDtoRequest.Nombres = "";
            usuarioDtoRequest.Apellidos = "";
            usuarioDtoRequest.Login = "";

            var usuarios = AgenteServicioUsuario.ListarUsuarios(usuarioDtoRequest);
            List<UsuarioDtoResponse> lista = new List<UsuarioDtoResponse>();

            foreach (ProxyServicio.WsRaisUsuario.BEUsuario usuario in usuarios)                
            {
                UsuarioDtoResponse obj = new UsuarioDtoResponse();
                obj.Apellidos = usuario.Apellidos_Usuario;
                obj.Nombres = usuario.Nombre_Usuario;
                obj.Login = usuario.Login;
                obj.IdUsuario = usuario.IdUsuario;
                obj.CorreoElectronico = usuario.Correo_Electronico;
                //
                lista.Add(obj);
            }
            respuesta.UsuarioDtoResponseLista = lista;

            return respuesta;
        }
    }
}
