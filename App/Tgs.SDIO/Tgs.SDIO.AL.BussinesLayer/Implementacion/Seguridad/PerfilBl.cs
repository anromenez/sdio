﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tgs.SDIO.AL.AgenteServicio;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad
{
    public class PerfilBl : IPerfilBl
    {
        public List<PerfilDtoResponse> ObtenerPerfiles(int idUsuarioSistema)
        {
            var dataSet = AgenteServicioPerfil.ObtenerPerfiles(idUsuarioSistema);

            if (dataSet.Tables[0] == null || dataSet.Tables[0].Rows.Count <= 0)
            {
                return null;
            }

            var listado = (from DataRow dataRow in dataSet.Tables[0].Rows
                           select new PerfilDtoResponse()
                           {
                               IdPerfil = Convert.ToInt32(dataRow["IdPerfil"].ToString()),
                               CodigoPerfil = dataRow["CodigoPerfil"].ToString(),
                               NombrePerfil = dataRow["NombrePerfil"].ToString()
                           }).ToList();

            return listado;
        }
    }
}
