﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaColaboradorDetalleBl : ICartaFianzaColaboradorDetalleBl
    {
        public CartaFianzaColaboradorDetalleResponse ActualizaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestActualiza)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaColaboradorDetalleResponse InsertaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestInserta)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaColaboradorDetalleResponse> ListaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList)
        {
            throw new NotImplementedException();
        }
    }
}
