﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaDetalleRenovacionBl : ICartaFianzaDetalleRenovacionBl
    {
        public CartaFianzaDetalleRenovacionResponse ActualizaCartaFianzaDetalleRenovacion(List<CartaFianzaDetalleRenovacionRequest> CartaFianzaDetalleRenovacionActualiza)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaDetalleRenovacionResponse> InsertaCartaFianzaDetalleRenovacion(List<CartaFianzaDetalleRenovacionRequest> CartaFianzaDetalleRenovacionRequestInserta)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaDetalleRenovacionResponse ListaCartaFianzaDetalleRenovacion(List<CartaFianzaDetalleRenovacionRequest> CartaFianzaDetalleRenovacionLista)
        {
            throw new NotImplementedException();
        }
    }
}
