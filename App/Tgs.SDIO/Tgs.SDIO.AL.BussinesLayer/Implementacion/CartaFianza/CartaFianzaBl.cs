﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using static Tgs.SDIO.Util.Constantes.Generales;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Util.Mensajes.CartaFianza;
using Tgs.SDIO.Util.Funciones;

namespace Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza
{
    public class CartaFianzaBl : ICartaFianzaBl
    {
        readonly ICartaFianzaDal iCartaFianzaDal;
        ProcesoResponse respuesta = new ProcesoResponse();

        public CartaFianzaBl(ICartaFianzaDal vICartaFianzaDal)
        {
            iCartaFianzaDal = vICartaFianzaDal;
        }

        public ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest CartaFianzaRequestActualiza)
        {
            try {


                var objCartaFianza = new CartaFianzaMaestro()
                {
                    IdCartaFianza = CartaFianzaRequestActualiza.IdCartaFianza,
                    IdCliente = CartaFianzaRequestActualiza.IdCartaFianza,
                    NumeroOportunidad = CartaFianzaRequestActualiza.NumeroOportunidad,
                    IdTipoContratoTm = CartaFianzaRequestActualiza.IdTipoContratoTm,
                    NumeroContrato = CartaFianzaRequestActualiza.NumeroContrato,
                    Processo = CartaFianzaRequestActualiza.Processo,
                    DescripcionServicioCartaFianza = CartaFianzaRequestActualiza.DescripcionServicioCartaFianza,
                    FechaFirmaServicioContrato = CartaFianzaRequestActualiza.FechaFirmaServicioContrato,
                    FechaFinServicioContrato = CartaFianzaRequestActualiza.FechaFinServicioContrato,
                    IdEmpresaAdjudicadaTm = CartaFianzaRequestActualiza.IdEmpresaAdjudicadaTm,
                    IdTipoGarantiaTm = CartaFianzaRequestActualiza.IdTipoGarantiaTm,
                    NumeroGarantia = CartaFianzaRequestActualiza.NumeroGarantia,
                    IdTipoMonedaTm = CartaFianzaRequestActualiza.IdTipoMonedaTm,
                    ImporteCartaFianzaSoles = CartaFianzaRequestActualiza.ImporteCartaFianzaSoles,
                    ImporteCartaFianzaDolares = CartaFianzaRequestActualiza.ImporteCartaFianzaDolares,
                    IdBanco = CartaFianzaRequestActualiza.IdBanco,
                    //IdTipoAccionTm = CartaFianzaRequestInserta.IdTipoAccionTm,
                    //IdEstadoVencimientoTm = CartaFianzaRequestInserta.IdEstadoVencimientoTm,
                    //IdEstadoCartaFianzaTm = CartaFianzaRequestInserta.IdEstadoCartaFianzaTm,
                    //IdRenovarTm = CartaFianzaRequestInserta.IdRenovarTm,
                    //IdEstadoRecuperacionCartaFianzaTm = CartaFianzaRequestInserta.IdEstadoRecuperacionCartaFianzaTm,
                    ClienteEspecial = CartaFianzaRequestActualiza.ClienteEspecial,
                    SeguimientoImportante = CartaFianzaRequestActualiza.SeguimientoImportante,
                    Observacion = CartaFianzaRequestActualiza.Observacion,
                    Incidencia = CartaFianzaRequestActualiza.Incidencia,
                    IdEstado = CartaFianzaRequestActualiza.IdEstado,
                    IdUsuarioEdicion = CartaFianzaRequestActualiza.IdUsuarioEdicion,
                    FechaEdicion = DateTime.Parse(Funciones.FormatoFechaHora(DateTime.Now))
                };
               

                iCartaFianzaDal.ActualizarPorCampos(objCartaFianza,
                                                    //x=> x.IdCliente,
                                                    x => x.NumeroOportunidad,
                                                    x => x.IdTipoContratoTm,
                                                    x => x.NumeroContrato,
                                                    x => x.Processo,
                                                    x => x.DescripcionServicioCartaFianza,
                                                    x => x.FechaFirmaServicioContrato,
                                                    x => x.FechaFinServicioContrato,
                                                    x => x.IdEmpresaAdjudicadaTm,
                                                    x => x.IdTipoGarantiaTm,
                                                    x => x.IdTipoMonedaTm,
                                                    x => x.ImporteCartaFianzaSoles,
                                                    x => x.ImporteCartaFianzaDolares,
                                                    x => x.IdBanco,
                                                    x => x.ClienteEspecial,
                                                    x => x.SeguimientoImportante,
                                                    x => x.Observacion,
                                                    x => x.Incidencia,
                                                    x => x.IdEstado,
                                                    x => x.IdUsuarioEdicion,
                                                    x => x.FechaEdicion
                                                    );
                iCartaFianzaDal.UnitOfWork.Commit();

                respuesta.Id = objCartaFianza.IdCartaFianza;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;


                return respuesta;


            } catch(Exception ex) {
                    ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);

            }

          
            return respuesta;
        }

        public ProcesoResponse InsertaCartaFianza(CartaFianzaRequest CartaFianzaRequestInserta)
        {
            try {
                var objCartaFianza = new CartaFianzaMaestro()
                {
                    IdCartaFianza = CartaFianzaRequestInserta.IdCartaFianza,
                    IdCliente = CartaFianzaRequestInserta.IdCliente,
                    NumeroOportunidad = CartaFianzaRequestInserta.NumeroOportunidad,
                    IdTipoContratoTm = CartaFianzaRequestInserta.IdTipoContratoTm,
                    NumeroContrato = CartaFianzaRequestInserta.NumeroContrato,
                    Processo = CartaFianzaRequestInserta.Processo,
                    DescripcionServicioCartaFianza = CartaFianzaRequestInserta.DescripcionServicioCartaFianza,
                    FechaFirmaServicioContrato = CartaFianzaRequestInserta.FechaFirmaServicioContrato,
                    FechaFinServicioContrato = CartaFianzaRequestInserta.FechaFinServicioContrato,
                    IdEmpresaAdjudicadaTm = CartaFianzaRequestInserta.IdEmpresaAdjudicadaTm,
                    IdTipoGarantiaTm = CartaFianzaRequestInserta.IdTipoGarantiaTm,
                    NumeroGarantia = CartaFianzaRequestInserta.NumeroGarantia,
                    IdTipoMonedaTm = CartaFianzaRequestInserta.IdTipoMonedaTm,
                    ImporteCartaFianzaSoles = CartaFianzaRequestInserta.ImporteCartaFianzaSoles,
                    ImporteCartaFianzaDolares = CartaFianzaRequestInserta.ImporteCartaFianzaDolares,
                    IdBanco = CartaFianzaRequestInserta.IdBanco,
                    //IdTipoAccionTm = CartaFianzaRequestInserta.IdTipoAccionTm,
                    //IdEstadoVencimientoTm = CartaFianzaRequestInserta.IdEstadoVencimientoTm,
                    //IdEstadoCartaFianzaTm = CartaFianzaRequestInserta.IdEstadoCartaFianzaTm,
                    //IdRenovarTm = CartaFianzaRequestInserta.IdRenovarTm,
                    //IdEstadoRecuperacionCartaFianzaTm = CartaFianzaRequestInserta.IdEstadoRecuperacionCartaFianzaTm,
                    ClienteEspecial = CartaFianzaRequestInserta.ClienteEspecial,
                    SeguimientoImportante = CartaFianzaRequestInserta.SeguimientoImportante,
                    Observacion = CartaFianzaRequestInserta.Observacion,
                    Incidencia = CartaFianzaRequestInserta.Incidencia,
                    IdEstado = CartaFianzaRequestInserta.IdEstado,
                    IdUsuarioCreacion = CartaFianzaRequestInserta.IdUsuarioCreacion,
                    FechaCreacion = DateTime.Parse(Funciones.FormatoFechaHora(DateTime.Now))
                };


                iCartaFianzaDal.Add(objCartaFianza);
                iCartaFianzaDal.UnitOfWork.Commit();

                respuesta.Id = objCartaFianza.IdCartaFianza;
                respuesta.TipoRespuesta = Proceso.Valido;
                respuesta.Mensaje = MensajesGeneralCartaFianza.RegistrarCartaFianza;
            } catch (Exception ex){
                ExceptionManager.GenerarAppExcepcionValidacion(ErrorNegocioComun.ErrorConcepto, ex.Message.ToString(), Generales.Sistemas.CartaFianza);


            }

            return respuesta;
        }

        public List<CartaFianzaListaResponse> ListaCartaFianza(CartaFianzaRequest CartaFianzaRequestLista)
        {

            return iCartaFianzaDal.ListaCartaFianza(CartaFianzaRequestLista);

        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest CartaFianzaRequestLista)
        {
            return iCartaFianzaDal.ListaCartaFianzaId(CartaFianzaRequestLista);

        }
    }
}
