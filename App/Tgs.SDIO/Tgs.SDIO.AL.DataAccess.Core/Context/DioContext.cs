﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Tgs.SDIO.Entities.Entities.CartaFianza;
using Tgs.SDIO.Entities.Entities.Comun;
using Tgs.SDIO.Entities.Entities.Oportunidad;
using Tgs.SDIO.Entities.Entities.Trazabilidad;

namespace Tgs.SDIO.AL.DataAccess.Core.Context
{
    public class DioContext : DbContext, IQueryableUnitOfWork
    {
        public DioContext() : base("name=SDioDbConnectionString")
        {
            Database.Log = (sql) => Debug.Write(sql);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.ValidateOnSaveEnabled = false;
        }

        public virtual DbSet<SalesForceConsolidadoCabecera> SalesForceConsolidadoCabecera { get; set; }


        #region CartaFianza
        public virtual DbSet<CartaFianzaMaestro> CartaFianza { get; set; }
        public virtual DbSet<CartaFianzaColaboradorDetalle> CartaFianzaColaboradorDetalle { get; set; }
        public virtual DbSet<CartaFianzaDetalleAcciones> CartaFianzaDetalleAcciones { get; set; }
        public virtual DbSet<CartaFianzaDetalleEstado> CartaFianzaDetalleEstado { get; set; }
        public virtual DbSet<CartaFianzaDetalleRecupero> CartaFianzaDetalleRecupero { get; set; }
        public virtual DbSet<CartaFianzaDetalleRenovacion> CartaFianzaDetalleRenovacion { get; set; }
        #endregion


        #region trazabilidad dbsets

        public virtual DbSet<AreaSeguimiento> AreaSeguimiento { get; set; }

        public virtual DbSet<SegmentoNegocio> SegmentoNegocio { get; set; }

        public virtual DbSet<AreaSegmentoNegocio> AreaSegmentoNegocio { get; set; }

        public virtual DbSet<ConceptoSeguimiento> ConceptoSeguimiento { get; set; }
        public virtual DbSet<Recurso> Recurso { get; set; }
        public virtual DbSet<RolRecurso> RolRecurso { get; set; }

        public virtual DbSet<ActividadOportunidad> ActividadOportunidad { get; set; }
        public virtual DbSet<EtapaOportunidad> EtapaOportunidad { get; set; }
        public virtual DbSet<ActividadSegmentoNegocio> ActividadSegmentoNegocio { get; set; }
        public virtual DbSet<FaseOportunidad> FaseOportunidad { get; set; }
        public virtual DbSet<RecursoOportunidad> RecursoOportunidad { get; set; }
        public virtual DbSet<ObservacionOportunidad> ObservacionOportunidad { get; set; }
        public virtual DbSet<OportunidadST> OportunidadST { get; set; }
        public virtual DbSet<CasoOportunidad> CasoOportunidad { get; set; }
        public virtual DbSet<RecursoEquipoTrabajo> RecursoEquipoTrabajo { get; set; }
        public virtual DbSet<TipoSolicitud> TipoSolicitud { get; set; }

        public virtual DbSet<EstadoCaso> EstadoCaso { get; set; }
        public virtual DbSet<DetalleActividadOportunidad> DetalleActividadOportunidad { get; set; }
        public virtual DbSet<TipoOportunidad> TipoOportunidad { get; set; }
        public virtual DbSet<DatosPreventaMovil> DatosPreventaMovil { get; set; }
        public virtual DbSet<MotivoOportunidad> MotivoOportunidad { get; set; }
        public virtual DbSet<FuncionPropietario> FuncionPropietario { get; set; }



        #endregion

        #region Comun dbsets


        public virtual DbSet<Colaborador> Colaborador { get; set; }
        public virtual DbSet<Medio> Medio { get; set; }
        public virtual DbSet<Servicio> Servicio { get; set; }
        public virtual DbSet<ServicioSubServicio> ServicioSubServicio { get; set; }
        public virtual DbSet<SubServicio> SubServicio { get; set; }
        public virtual DbSet<Archivo> Archivo { get; set; }
        public virtual DbSet<Concepto> Concepto { get; set; }

        public virtual DbSet<BW> BW { get; set; }
        public virtual DbSet<SubServicioDatosCaratula> SubServicioDatosCaratula { get; set; }
        public virtual DbSet<ConceptoDatosCapex> ConceptoDatosCapex { get; set; }
        public virtual DbSet<LineaNegocioCMI> LineaNegocioCMI { get; set; }
        public virtual DbSet<LineaConceptoCMI> LineaConceptoCMI { get; set; }
        public virtual DbSet<LineaConceptoGrupo> LineaConceptoGrupo { get; set; }
        public virtual DbSet<LineaPestana> LineaPestana { get; set; }
        public virtual DbSet<LineaProductoCMI> LineaProductoCMI { get; set; }

        public virtual DbSet<PestanaGrupo> PestanaGrupo { get; set; }

        public virtual DbSet<ProyectoLineaConceptoProyectado> ProyectoLineaConceptoProyectado { get; set; }
        public virtual DbSet<ProyectoLineaProducto> ProyectoLineaProducto { get; set; }


        public virtual DbSet<OportunidadServicioCMI> OportunidadServicioCMI { get; set; }
        public virtual DbSet<OportunidadTipoCambio> OportunidadTipoCambio { get; set; }
        public virtual DbSet<OportunidadTipoCambioDetalle> OportunidadTipoCambioDetalle { get; set; }
        public virtual DbSet<ProyectoServicioCMI> ProyectoServicioCMI { get; set; }
        public virtual DbSet<ProyectoServicioConcepto> ProyectoServicioConcepto { get; set; }
        public virtual DbSet<ServicioConceptoDocumento> ServicioConceptoDocumento { get; set; }
        public virtual DbSet<ServicioConceptoProyectado> ServicioConceptoProyectado { get; set; }



        public virtual DbSet<Depreciacion> Depreciacion { get; set; }
        public virtual DbSet<DG> DG { get; set; }
        public virtual DbSet<DireccionComercial> DireccionComercial { get; set; }
        public virtual DbSet<Fentocelda> Fentocelda { get; set; }
        public virtual DbSet<Linea> Linea { get; set; }
        public virtual DbSet<LineaNegocio> LineaNegocio { get; set; }
        public virtual DbSet<LineaProducto> LineaProducto { get; set; }
        public virtual DbSet<Maestra> Maestra { get; set; }
        public virtual DbSet<Proveedor> Proveedor { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<ServicioCMI> ServicioCMI { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }
        public virtual DbSet<CasoNegocio> CasoNegocio { get; set; }
        public virtual DbSet<CasoNegocioServicio> CasoNegocioServicio { get; set; }

        //public virtual DbSet<ActividadOportunidad> ActividadOportunidad { get; set; }
        //public virtual DbSet<AreaSegmentoNegocio> AreaSegmentoNegocio { get; set; }
        //public virtual DbSet<CasoOportunidad> CasoOportunidad { get; set; }
        //public virtual DbSet<Cliente> Cliente { get; set; }
        //public virtual DbSet<ConceptoSeguimiento> ConceptoSeguimiento { get; set; }
        //public virtual DbSet<DatosPreventaMovil> DatosPreventaMovil { get; set; }
        //public virtual DbSet<DocumentoAdjunto> DocumentoAdjunto { get; set; }
        //public virtual DbSet<EtapaOportunidad> EtapaOportunidad { get; set; }
        //public virtual DbSet<FaseOportunidad> FaseOportunidad { get; set; }
        //public virtual DbSet<FileDocumentoAdjunto> FileDocumentoAdjunto { get; set; }
        //public virtual DbSet<GrupoFaseOportunidad> GrupoFaseOportunidad { get; set; }
        //public virtual DbSet<MotivoOportunidad> MotivoOportunidad { get; set; }
        //public virtual DbSet<ObservacionOportunidad> ObservacionOportunidad { get; set; }
        //public virtual DbSet<Recurso> Recurso { get; set; }
        //public virtual DbSet<RecursoOportunidad> RecursoOportunidad { get; set; }
        //public virtual DbSet<RolRecurso> RolRecurso { get; set; }
        //public virtual DbSet<Sector> Sector { get; set; }
        //public virtual DbSet<SeguimientoActividad> SeguimientoActividad { get; set; }

        #endregion
        
        public virtual DbSet<OportunidadFlujoEstado> OportunidadFlujoEstado { get; set; }
        public virtual DbSet<OportunidadLineaNegocio> OportunidadLineaNegocio { get; set; }
        public virtual DbSet<Oportunidad> Oportunidad { get; set; }
        public virtual DbSet<OportunidadDocumento> OportunidadDocumento { get; set; }
        public virtual DbSet<OportunidadFlujoCaja> OportunidadFlujoCaja { get; set; }
        public virtual DbSet<OportunidadFlujoCajaConfiguracion> OportunidadFlujoCajaConfiguracion { get; set; }
        public virtual DbSet<OportunidadFlujoCajaDetalle> OportunidadFlujoCajaDetalle { get; set; }
        public virtual DbSet<SubServicioDatosCapex> SubServicioDatosCapex { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DioContext>(null);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            #region CARTAFIANZA
            modelBuilder.Entity<CartaFianzaMaestro>()
             .ToTable("CartaFianza", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaColaboradorDetalle>()
             .ToTable("CartaFianzaColaboradorDetalle", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleAcciones>()
             .ToTable("CartaFianzaDetalleAcciones", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleEstado>()
             .ToTable("CartaFianzaDetalleEstado", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleRecupero>()
             .ToTable("CartaFianzaDetalleRecupero", "CARTAFIANZA");

            modelBuilder.Entity<CartaFianzaDetalleRenovacion>()
          .ToTable("CartaFianzaDetalleRenovacion", "CARTAFIANZA");

            #endregion




            modelBuilder.Entity<Colaborador>()
                    .ToTable("Colaborador", "COMUN");

            modelBuilder.Entity<SalesForceConsolidadoCabecera>()
                .ToTable("SalesForceConsolidadoCabecera", "COMUN");

            modelBuilder.Entity<ConceptoDatosCapex>()
                .ToTable("ConceptoDatosCapex", "OPORTUNIDAD");

            modelBuilder.Entity<SubServicioDatosCaratula>()
                .ToTable("SubServicioDatosCaratula", "OPORTUNIDAD");

            modelBuilder.Entity<LineaConceptoCMI>()
                .ToTable("LineaConceptoCMI", "OPORTUNIDAD");

            modelBuilder.Entity<LineaNegocioCMI>()
                .ToTable("LineaNegocioCMI", "OPORTUNIDAD");

            modelBuilder.Entity<LineaConceptoGrupo>()
               .ToTable("LineaConceptoGrupo", "OPORTUNIDAD");

            modelBuilder.Entity<LineaPestana>()
               .ToTable("LineaPestana", "OPORTUNIDAD");

            modelBuilder.Entity<PestanaGrupo>()
               .ToTable("PestanaGrupo", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadLineaNegocio>()
              .ToTable("OportunidadLineaNegocio", "OPORTUNIDAD");


            modelBuilder.Entity<ProyectoLineaConceptoProyectado>()
               .ToTable("ProyectoLineaConceptoProyectado", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoLineaProducto>()
               .ToTable("ProyectoLineaProducto", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadServicioCMI>()
               .ToTable("OportunidadServicioCMI", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadTipoCambio>()
               .ToTable("OportunidadTipoCambio", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadTipoCambioDetalle>()
               .ToTable("OportunidadTipoCambioDetalle", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoServicioCMI>()
               .ToTable("ProyectoServicioCMI", "OPORTUNIDAD");

            modelBuilder.Entity<ProyectoServicioConcepto>()
               .ToTable("ProyectoServicioConcepto", "OPORTUNIDAD");

            modelBuilder.Entity<ServicioConceptoDocumento>()
               .ToTable("ServicioConceptoDocumento", "OPORTUNIDAD");

            modelBuilder.Entity<ServicioConceptoProyectado>()
               .ToTable("ServicioConceptoProyectado", "OPORTUNIDAD");

            modelBuilder.Entity<LineaProductoCMI>()
              .ToTable("LineaProductoCMI", "OPORTUNIDAD");

            modelBuilder.Entity<Oportunidad>()
              .ToTable("Oportunidad", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoCaja>()
              .ToTable("OportunidadFlujoCaja", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoCajaConfiguracion>()
              .ToTable("OportunidadFlujoCajaConfiguracion", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoCajaDetalle>()
              .ToTable("OportunidadFlujoCajaDetalle", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadFlujoEstado>()
             .ToTable("OportunidadFlujoEstado", "OPORTUNIDAD");

            modelBuilder.Entity<SubServicioDatosCapex>()
            .ToTable("SubServicioDatosCapex", "OPORTUNIDAD");

            modelBuilder.Entity<OportunidadDocumento>()
           .ToTable("OportunidadDocumento", "OPORTUNIDAD");
            #region trazabilidad modelBuilders



            modelBuilder.Entity<AreaSeguimiento>()
                 .ToTable("AreaSeguimiento", "TRAZABILIDAD");

            modelBuilder.Entity<SegmentoNegocio>()
                .ToTable("SegmentoNegocio", "TRAZABILIDAD");

            modelBuilder.Entity<AreaSegmentoNegocio>()
                .ToTable("AreaSegmentoNegocio", "TRAZABILIDAD");

            modelBuilder.Entity<Recurso>()
               .ToTable("Recurso", "TRAZABILIDAD");

            modelBuilder.Entity<RolRecurso>()
               .ToTable("RolRecurso", "TRAZABILIDAD");

            modelBuilder.Entity<ConceptoSeguimiento>()
               .ToTable("ConceptoSeguimiento", "TRAZABILIDAD");

            modelBuilder.Entity<ActividadOportunidad>()
               .ToTable("ActividadOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<EtapaOportunidad>()
               .ToTable("EtapaOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<ActividadSegmentoNegocio>()
               .ToTable("ActividadSegmentoNegocio", "TRAZABILIDAD");

            modelBuilder.Entity<FaseOportunidad>()
                 .ToTable("FaseOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<CasoOportunidad>()
                  .ToTable("CasoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<OportunidadST>()
                 .ToTable("OportunidadST", "TRAZABILIDAD");

            modelBuilder.Entity<ObservacionOportunidad>()
                .ToTable("ObservacionOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<RecursoEquipoTrabajo>()
                 .ToTable("RecursoEquipoTrabajo", "TRAZABILIDAD");

            modelBuilder.Entity<RecursoOportunidad>()
                .ToTable("RecursoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<TipoSolicitud>()
                .ToTable("TipoSolicitud", "TRAZABILIDAD");

            modelBuilder.Entity<EstadoCaso>()
                .ToTable("EstadoCaso", "TRAZABILIDAD");

            modelBuilder.Entity<DetalleActividadOportunidad>()
              .ToTable("DetalleActividadOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<TipoOportunidad>()
             .ToTable("TipoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<DatosPreventaMovil>()
             .ToTable("DatosPreventaMovil", "TRAZABILIDAD");

            modelBuilder.Entity<MotivoOportunidad>()
             .ToTable("MotivoOportunidad", "TRAZABILIDAD");

            modelBuilder.Entity<FuncionPropietario>()
             .ToTable("FuncionPropietario", "TRAZABILIDAD");



            #endregion
            #region Comun modelBuilders
            modelBuilder.Entity<Medio>()
            .ToTable("Medio", "COMUN");
            modelBuilder.Entity<Servicio>()
             .ToTable("Servicio", "COMUN");
            modelBuilder.Entity<SubServicio>()
              .ToTable("SubServicio", "COMUN");
            modelBuilder.Entity<ServicioSubServicio>()
               .ToTable("ServicioSubServicio", "COMUN");
            modelBuilder.Entity<Archivo>()
               .ToTable("Archivo", "COMUN");
            modelBuilder.Entity<BW>()
               .ToTable("BW", "COMUN");
            modelBuilder.Entity<Concepto>()
               .ToTable("Concepto", "COMUN");
            modelBuilder.Entity<Depreciacion>()
               .ToTable("Depreciacion", "COMUN");
            modelBuilder.Entity<DG>()
               .ToTable("DG", "COMUN");
            modelBuilder.Entity<DireccionComercial>()
               .ToTable("DireccionComercial", "COMUN");
            modelBuilder.Entity<Fentocelda>()
               .ToTable("Fentocelda", "COMUN");
            modelBuilder.Entity<Linea>()
               .ToTable("Linea", "COMUN");
            modelBuilder.Entity<LineaNegocio>()
               .ToTable("LineaNegocio", "COMUN");
            modelBuilder.Entity<Maestra>()
              .ToTable("Maestra", "COMUN");
            modelBuilder.Entity<Proveedor>()
              .ToTable("Proveedor", "COMUN");
            modelBuilder.Entity<Sector>()
              .ToTable("Sector", "COMUN");
            modelBuilder.Entity<ServicioCMI>()
             .ToTable("ServicioCMI", "COMUN");
            modelBuilder.Entity<LineaProducto>()
              .ToTable("LineaProducto", "COMUN");
            modelBuilder.Entity<Cliente>()
           .ToTable("Cliente", "COMUN");

            modelBuilder.Entity<CasoNegocio>()
            .ToTable("CasoNegocio", "COMUN");

            modelBuilder.Entity<CasoNegocioServicio>()
             .ToTable("CasoNegocioServicio", "COMUN");


            #endregion
        }

        #region IQueryableUnitOfWork Members

        public DbSet<TEntity> CreateSet<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void Attach<TEntity>(TEntity item) where TEntity : class
        {
            base.Entry<TEntity>(item).State = EntityState.Unchanged;
        }

        public void SetModified<TEntity>(TEntity item) where TEntity : class
        {
            base.Entry<TEntity>(item).State = EntityState.Modified;
        }

        public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class
        {
            base.Entry<TEntity>(original).CurrentValues.SetValues(current);
        }

        public void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        public async Task<int> CommitAsync()
        {
            return await base.SaveChangesAsync();
        }

        public void CommitAndRefreshChanges()
        {
            bool saveFailed = false;
            do
            {
                try
                {
                    base.SaveChanges();
                    saveFailed = false;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.ToList()
                              .ForEach(entry =>
                              {
                                  entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                              });
                }
            } while (saveFailed);
        }

        public void RollbackChanges()
        {
            base.ChangeTracker
                .Entries()
                .ToList()
                .ForEach(entry => entry.State = EntityState.Unchanged);
        }

        public IEnumerable<TEntity> ExecuteQuery<TEntity>(string sqlQuery, params object[] parameters)
        {
            return base.Database.SqlQuery<TEntity>(sqlQuery, parameters);
        }

        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        public async Task<int> ExecuteCommandAsync(string sqlCommand, params object[] parameters)
        {
            return await base.Database.ExecuteSqlCommandAsync(sqlCommand, parameters);
        }

        public DbContextTransaction BeginTransaction()
        {
            return base.Database.BeginTransaction();
        }

        #endregion
    }

    public abstract class BaseDomainContext : DbContext
    {
        static BaseDomainContext()
        {
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}