﻿
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio

    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse InsertaCartaFianza(CartaFianzaRequest CartaFianzaRequestInserta);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest CartaFianzaRequestActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaListaResponse> ListaCartaFianza(CartaFianzaRequest CartaFianzaRequestLista);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest CartaFianzaRequestLista);
    }
}
