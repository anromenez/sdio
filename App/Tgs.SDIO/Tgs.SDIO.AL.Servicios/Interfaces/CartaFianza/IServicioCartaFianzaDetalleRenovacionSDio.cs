﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaDetalleRenovacionResponse> InsertaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionRequestInserta);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleRenovacionResponse ActualizaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleRenovacionResponse ListaCartaFianzaDetalleRenovacion(CartaFianzaDetalleRenovacionRequest CartaFianzaDetalleRenovacionLista);

        
    }
}
