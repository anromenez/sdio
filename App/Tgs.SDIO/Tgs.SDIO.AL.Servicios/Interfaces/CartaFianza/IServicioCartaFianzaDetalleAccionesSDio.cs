﻿
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaDetalleAccionResponse> InsertaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> CartaFianzaDetalleAccionesRequestInserta);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleAccionResponse ActualizaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> CartaFianzaDetalleAccionesRequestActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleAccionResponse ListaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> CartaFianzaDetalleAccionesRequestLista);


       
    }
}
