﻿
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaColaboradorDetalleResponse> ListaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestList);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaColaboradorDetalleResponse ActualizaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaColaboradorDetalleResponse InsertaCartaFianzaColaboradorDetalle(CartaFianzaColaboradorDetalleRequest CartaFianzaColaboradorDetalleRequestInserta);


    }
}
