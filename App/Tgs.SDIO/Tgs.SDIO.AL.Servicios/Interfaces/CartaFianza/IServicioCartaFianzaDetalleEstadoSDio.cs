﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza
{
    public partial interface IServicioCartaFianzaGeneralSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<CartaFianzaDetalleEstadoResponse> InsertaCartaFianzaDetalleEstado(List<CartaFianzaDetalleEstadoRequest> CartaFianzaDetalleEstadoRequestInserta);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleEstadoResponse ActualizaCartaFianzaDetalleEstado(List<CartaFianzaDetalleEstadoRequest> CartaFianzaDetalleEstadoequestActualiza);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        CartaFianzaDetalleEstadoResponse ListaCartaFianzaDetalleEstado(List<CartaFianzaDetalleEstadoRequest> CartaFianzaDetalleEstadoequestLista);


    }
}
