﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Seguridad
{
    public partial interface IServicioSeguridadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UsuarioDtoResponse ObtenerUsuarioPorLogin(string login);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UsuarioDtoResponse ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<EntidadDtoResponse> ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        string RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UsuarioDtoResponse CambiarPassword(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UsuarioDtoResponse ObtenerUsuarioPerfil(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<UsuarioDtoResponse> ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UsuarioDtoResponse ListarUsuarios(UsuarioDtoRequest usuarioDtoRequest);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        UsuarioDtoResponse ObtenerUsuarioPorIdUsuario(int idUsuario);
    }
}
