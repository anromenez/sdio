﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Seguridad
{
    public partial interface IServicioSeguridadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<PerfilDtoResponse> ObtenerPerfiles(int idUsuarioSistema);
    }
}
