﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ServicioSubServicioDtoResponse> ListarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ServicioSubServicioDtoResponse ObtenerServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ServicioSubServicioPaginadoDtoResponse ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ProcesoResponse InhabilitarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ProcesoResponse EliminarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ServicioSubServicioPaginadoDtoResponse ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest ServiciosubServicio);
    }
}
