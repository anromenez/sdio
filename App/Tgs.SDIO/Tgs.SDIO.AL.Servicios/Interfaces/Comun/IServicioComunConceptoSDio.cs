﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ConceptoDtoResponse> ListarBandejaConceptos(ConceptoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ConceptoDtoResponse> ListarConceptos(ConceptoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        ConceptoDtoResponse ObtenerConcepto(ConceptoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarConcepto(ConceptoDtoRequest concepto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarConcepto(ConceptoDtoRequest concepto);
    }
}
