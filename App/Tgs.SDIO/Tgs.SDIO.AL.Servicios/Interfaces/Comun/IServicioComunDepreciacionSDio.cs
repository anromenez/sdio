﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
 
    
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarDepreciacion(DepreciacionDtoRequest depreciacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        DepreciacionDtoResponse ObtenerDepreciacion(DepreciacionDtoRequest depreciacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarDepreciacion(DepreciacionDtoRequest depreciacion);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarDepreciacion(DepreciacionDtoRequest depreciacion);
    }
}
