﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ServicioCMIDtoResponse> ListarServicioCMI(ServicioCMIDtoRequest servicioCMI);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ServicioCMIDtoResponse ObtenerServicioCMI(ServicioCMIDtoRequest servicioCMI);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarServicioCMI(ServicioCMIDtoRequest servicioCMI);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarServicioCMI(ServicioCMIDtoRequest servicioCMI);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<ListaDtoResponse> ListarServicioCMIPorLineaNegocio(ServicioCMIDtoRequest servicioCMI);
    }
}
