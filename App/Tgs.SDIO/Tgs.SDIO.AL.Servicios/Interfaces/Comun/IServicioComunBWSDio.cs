﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Comun
{

    public partial interface IServicioComunSDio
    {
 

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        List<BWDtoResponse> ListarBW(BWDtoRequest bw);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]

        BWDtoResponse ObtenerBW(BWDtoRequest bw);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarBW(BWDtoRequest bw);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarBW(BWDtoRequest bw);
    }
}
