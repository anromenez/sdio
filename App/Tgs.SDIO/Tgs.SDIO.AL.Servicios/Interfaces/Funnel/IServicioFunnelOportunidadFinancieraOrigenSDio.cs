﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Funnel;
using Tgs.SDIO.DataContracts.Dto.Response.Funnel;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Funnel
{
    public partial interface IServicioFunnelSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        MemoryStream ListarIndicadorOfertasAnio(IndicadorMadurezDtoRequest request);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        DashoardDtoResponse MostrarDashboard(DashoardDtoRequest request);
    }
}
