﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarProyectoServicioConcepto(ProyectoDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarProyectoServicioConcepto(ProyectoDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProyectoConceptoDtoResponse ObtenerProyectoServicioConcepto(ProyectoDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ProyectoConceptoDtoResponse> ListarCMIProyecto(ProyectoDtoRequest proyecto);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ProyectoConceptoDtoResponse> ListarCMIProyectoCabecera(ProyectoDtoRequest proyecto);

    }
}
