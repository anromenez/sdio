﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Oportunidad
{
    public partial interface IServicioOportunidadSDio
    {
        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ConceptoDatosCapexDtoResponse ObtenerConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ConceptoDatosCapexDtoResponse> ConceptosDatoCapex(ConceptoDatosCapexDtoRequest capex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        List<ConceptoDatosCapexDtoResponse> ListarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse RegistrarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex);

        [OperationContract]
        [FaultContract(typeof(ErrorDto))]
        ProcesoResponse ActualizarConceptoDatoCapex(ConceptoDatosCapexDtoRequest capex);
    }
}
