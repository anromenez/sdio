﻿using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Interfaces.Trazabilidad
{
   public  partial interface IServicioTrazabilidadSDio
    {
        [OperationContract]
        AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request);
        [OperationContract]
        AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarAreaSeguimiento(AreaSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarAreaSeguimiento(AreaSeguimientoDtoRequest request);
        [OperationContract]
        List<ComboDtoResponse> ListarComboSegmentoNegocio();
        [OperationContract]
        AreaSegmentoNegocioDtoResponse ObtenerAreaSegmentoNegocioPorId(AreaSegmentoNegocioDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request);
        [OperationContract]
        RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request);
        [OperationContract]
        RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRecurso(RecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRecurso(RecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarRecurso(RecursoDtoRequest request);
        [OperationContract]
        RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request);
        [OperationContract]
        RolRecursoDtoResponse ObtenerRolRecursoPorId(RolRecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRolRecurso(RolRecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRolRecurso(RolRecursoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarRolRecurso(RolRecursoDtoRequest request);
        [OperationContract]
        ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request);
        [OperationContract]
        ActividadOportunidadPaginadoDtoResponse ListatActividadOportunidadPaginado(ActividadOportunidadDtoRequest request);
        [OperationContract]
        ActividadOportunidadDtoResponse ObtenerActividadOportunidadPorId(ActividadOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarActividadOportunidad(ActividadOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarActividadOportunidad(ActividadOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarActividadOportunidad(ActividadOportunidadDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboAreaSeguimiento();
        [OperationContract]
        List<ListaDtoResponse> ListarComboEtapaOportunidad();    
        [OperationContract]
        List<ListaDtoResponse> ListarComboFaseOportunidad();
        [OperationContract]
        ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ObservacionOportunidadDtoResponse ObtenerObservacionOportunidadPorId(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request);
        [OperationContract]
        RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request);
        [OperationContract]
        RecursoOportunidadDtoResponse ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarRecursoOportunidad(RecursoOportunidadDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseOportunidadDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboActividadPadres();
        [OperationContract]
        List<ListaDtoResponse> ListarComboConceptoSeguimiento();
        [OperationContract]
        List<ListaDtoResponse> ListarComboRolRecurso();
        [OperationContract]
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request);
        [OperationContract]
        OportunidadSTDtoResponse ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request);
        [OperationContract]
        CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request);
        [OperationContract]
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request);
        [OperationContract]
        RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request);
        [OperationContract]
        OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request);
        [OperationContract]
        OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request);
        [OperationContract]
        DatosPreventaMovilDtoResponse ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse ActualizarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        ProcesoResponse EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request);
        [OperationContract]
        List<ListaDtoResponse> ListarComboEstadoCaso();
        [OperationContract]
        List<ListaDtoResponse> ListarComboTipoOportunidad();
        [OperationContract]
        List<ListaDtoResponse> ListarComboSegmentoNegocioSimple();
        [OperationContract]
        List<ListaDtoResponse> ListarComboMotivoOportunidad();
        [OperationContract]
        List<ListaDtoResponse> ListarComboFuncionPropietario();
    }
}
