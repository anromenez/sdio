﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{
    public partial class ServicioCartaFianzaGeneralSDio
    {
        public CartaFianzaDetalleAccionResponse ActualizaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> cartaFianzaDetalleAccionRequestActualiza)
        {
            throw new NotImplementedException();
        }

        public List<CartaFianzaDetalleAccionResponse> InsertaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> cartaFianzaDetalleAccionRequestInserta)
        {
            throw new NotImplementedException();
        }

        public CartaFianzaDetalleAccionResponse ListaCartaFianzaDetalleAccion(List<CartaFianzaDetalleAccionRequest> cartaFianzaDetalleAccionRequestLista)
        {
            throw new NotImplementedException();
        }
    }
}
