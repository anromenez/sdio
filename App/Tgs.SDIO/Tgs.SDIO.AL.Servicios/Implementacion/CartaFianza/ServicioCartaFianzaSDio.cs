﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.AL.Servicios.Interfaces.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Request.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.CartaFianza;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.CartaFianza
{

    public partial class ServicioCartaFianzaGeneralSDio
    {

       
        public ProcesoResponse ActualizaCartaFianza(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ActualizaCartaFianza(cartaFianzaRequestActualiza);
        }

        public ProcesoResponse InsertaCartaFianza(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.InsertaCartaFianza(cartaFianzaRequestActualiza);
        }

        public List<CartaFianzaListaResponse> ListaCartaFianza(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ListaCartaFianza(cartaFianzaRequestActualiza);
        }

        public List<CartaFianzaListaResponse> ListaCartaFianzaId(CartaFianzaRequest cartaFianzaRequestActualiza)
        {
            return iCartaFianzaBl.ListaCartaFianzaId(cartaFianzaRequestActualiza);           
        }
        /**/
    }
}
