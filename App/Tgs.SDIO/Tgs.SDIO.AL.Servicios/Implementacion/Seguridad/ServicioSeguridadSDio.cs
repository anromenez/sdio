﻿using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.AL.Servicios.Interfaces.Seguridad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioSeguridad", Name = "ServicioSeguridad",
      ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioSeguridadSDio : IServicioSeguridadSDio
    {
        private readonly IUsuarioBl _iUsuarioBl;
        private readonly IOpcionMenuBl _iOpcionMenuBl;
        private readonly IPerfilBl _iPerfilBl;

        public ServicioSeguridadSDio(IUsuarioBl usuarioBl, IOpcionMenuBl opcionMenuBl, IPerfilBl perfilBl)
        {
            _iUsuarioBl = usuarioBl;
            _iOpcionMenuBl = opcionMenuBl;
            _iPerfilBl = perfilBl;
        } 
    }
}
