﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.DataContracts.Dto.Response.Seguridad;
using Tgs.SDIO.Util.Constantes;
using Tgs.SDIO.Util.Error.Comun;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Seguridad
{
    public partial class ServicioSeguridadSDio
    {
        public UsuarioDtoResponse ObtenerUsuarioPorLogin(string login)
        {
            return _iUsuarioBl.ObtenerUsuarioPorLogin(login);
        }

        public UsuarioDtoResponse ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.ValidarUsuario(usuarioDtoRequest);
        }

        public string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.EnvioClaveUsuario(usuarioDtoRequest);
        }

        public List<EntidadDtoResponse> ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.ObtenerAmbitoUsuario(usuarioDtoRequest);
        }

        public string RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.RegistrarUsuario(usuarioDtoRequest);
        }

        public UsuarioDtoResponse CambiarPassword(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.CambiarPassword(usuarioDtoRequest);
        }

        public UsuarioDtoResponse ObtenerUsuarioPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.ObtenerUsuarioPerfil(usuarioDtoRequest);
        }
        public List<UsuarioDtoResponse> ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.ObtenerUsuariosPorPerfil(usuarioDtoRequest);
        }
        public UsuarioDtoResponse ListarUsuarios(UsuarioDtoRequest usuarioDtoRequest)
        {
            return _iUsuarioBl.ListarUsuarios(usuarioDtoRequest);
        }
        public UsuarioDtoResponse ObtenerUsuarioPorIdUsuario(int idUsuario)
        {
            return _iUsuarioBl.ObtenerUsuarioPorIdUsuario(idUsuario);
        }

       
    }
}
