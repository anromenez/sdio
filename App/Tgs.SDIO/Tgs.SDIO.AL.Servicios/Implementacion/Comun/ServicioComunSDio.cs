﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.Servicios.Interfaces.Comun;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioComunSDio", Name = "ServicioComunSDio",
        ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioComunSDio : IServicioComunSDio
    {

        private readonly ISalesForceConsolidadoCabeceraBl _salesForceConsolidadoCabeceraBl;
        public readonly IArchivoBl iArchivoBl;
        public readonly IConceptoBl iConceptoBl;
        public readonly IBWBl iBWBl;
        public readonly ILineaNegocioBl iLineaNegocioBl;
        public readonly IDepreciacionBl iDepreciacionBl;
        public readonly IDGBl iDGBl;
        public readonly IDireccionComercialBl iDireccionComercialBl;
        public readonly IFentoceldaBl iFentoceldaBl;
        public readonly ILineaProductoBl iLineaProductoBl;
        public readonly IMaestraBl iMaestraBl;
        public readonly IProveedorBl iProveedorBl;
        public readonly ISectorBl iSectorBl;
        public readonly IServicioCMIBl iServicioCMIBl;
        public readonly IClienteBl iClienteBl;
        public readonly ISubServicioBl iSubServicioBl;
        public readonly IServicioSubServicioBl iServicioSubServicioBl;

        public readonly IServicioBl iServicioBl;


        public readonly IMedioBl iMedioBl;
        public readonly ICasoNegocioBl iCasoNegocioBl;
        public readonly ICasoNegocioServicioBl iCasoNegocioServicioBl;


        public ServicioComunSDio(ISalesForceConsolidadoCabeceraBl salesForceConsolidadoCabeceraBl, IArchivoBl IArchivoBl, IConceptoBl IConceptoBl, IBWBl IBWBl, ILineaNegocioBl ILineaNegocioBl,
         IDepreciacionBl IDepreciacionBl, IDGBl IDGBl, IDireccionComercialBl IDireccionComercialBl, IFentoceldaBl IFentoceldaBl, ILineaProductoBl ILineaProductoBl,
         IMaestraBl IMaestraBl, IProveedorBl IProveedorBl, ISectorBl ISectorBl, IServicioCMIBl IServicioCMIBl, IClienteBl  IClienteBl , ISubServicioBl ISubServicioBl ,
         IServicioSubServicioBl IServicioSubServicioBl , IServicioBl IServicioBl, IMedioBl IMedioBl, ICasoNegocioBl ICasoNegocioBl,
         ICasoNegocioServicioBl ICasoNegocioServicioBl)

        {
            _salesForceConsolidadoCabeceraBl = salesForceConsolidadoCabeceraBl;
            iArchivoBl = IArchivoBl;
            iConceptoBl = IConceptoBl;
            iBWBl = IBWBl;
            iLineaNegocioBl = ILineaNegocioBl;
            iDepreciacionBl = IDepreciacionBl;
            iDGBl = IDGBl;
            iDireccionComercialBl = IDireccionComercialBl;
            iFentoceldaBl = IFentoceldaBl;
            iLineaProductoBl = ILineaProductoBl;
            iMaestraBl = IMaestraBl;
            iProveedorBl = IProveedorBl;
            iSectorBl = ISectorBl;
            iServicioCMIBl = IServicioCMIBl;
            iClienteBl = IClienteBl;
            iSubServicioBl = ISubServicioBl;
            iServicioSubServicioBl = IServicioSubServicioBl;
            iServicioBl = IServicioBl;
            iMedioBl = IMedioBl;
            iCasoNegocioBl = ICasoNegocioBl;
            iCasoNegocioServicioBl = ICasoNegocioServicioBl;

        }

    }

}