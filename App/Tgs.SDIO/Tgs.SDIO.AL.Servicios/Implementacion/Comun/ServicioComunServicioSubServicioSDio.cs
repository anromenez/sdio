﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public ProcesoResponse ActualizarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.ActualizarServicioSubServicio(servicioSubServicio);
        }
        public List<ServicioSubServicioDtoResponse> ListarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.ListarServicioSubServicio(servicioSubServicio);
        }
   
        public ServicioSubServicioDtoResponse ObtenerServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.ObtenerServicioSubServicio(servicioSubServicio);
        }

        public ProcesoResponse RegistrarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.RegistrarServicioSubServicio(servicioSubServicio);
        }
        public ServicioSubServicioPaginadoDtoResponse ListaServicioSubServicioPaginado(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.ListaServicioSubServicioPaginado(servicioSubServicio);
        }
        public ProcesoResponse InhabilitarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.InhabilitarServicioSubServicio(servicioSubServicio);
        }

        public ProcesoResponse EliminarServicioSubServicio(ServicioSubServicioDtoRequest servicioSubServicio)
        {
            return iServicioSubServicioBl.EliminarServicioSubServicio(servicioSubServicio);
        }

        public ServicioSubServicioPaginadoDtoResponse ListarServicioSubServicioGrupos(ServicioSubServicioDtoRequest ServiciosubServicio)
        {
            return iServicioSubServicioBl.ListarServicioSubServicioGrupos(ServiciosubServicio);
        }


    }
}
