﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarFentocelda(FentoceldaDtoRequest fentocelda)
        {


            return iFentoceldaBl.ActualizarFentocelda(fentocelda);
        }

        public List<FentoceldaDtoResponse> ListarFentocelda(FentoceldaDtoRequest fentocelda)
        {
            return iFentoceldaBl.ListarFentocelda(fentocelda);
        }

        public FentoceldaDtoResponse ObtenerFentocelda(FentoceldaDtoRequest fentocelda)
        {
            return iFentoceldaBl.ObtenerFentocelda(fentocelda);
        }

        public ProcesoResponse RegistrarFentocelda(FentoceldaDtoRequest fentocelda)
        {
            return iFentoceldaBl.RegistrarFentocelda(fentocelda);
        }
    }
}
