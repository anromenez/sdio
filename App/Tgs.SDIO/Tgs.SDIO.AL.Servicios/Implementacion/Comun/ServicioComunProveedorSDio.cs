﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarProveedor(ProveedorDtoRequest proveedor)
        {


            return iProveedorBl.ActualizarProveedor(proveedor);
        }

        public List<ListaDtoResponse> ListarProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ListarProveedor(proveedor);
        }

        public ProveedorDtoResponse ObtenerProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.ObtenerProveedor(proveedor);
        }

        public ProcesoResponse RegistrarProveedor(ProveedorDtoRequest proveedor)
        {
            return iProveedorBl.RegistrarProveedor(proveedor);
        }
    }
}
