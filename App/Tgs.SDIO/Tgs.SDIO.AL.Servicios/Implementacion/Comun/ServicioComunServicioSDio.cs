﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public ProcesoResponse ActualizarServicio(ServicioDtoRequest servicio)
        {
            return iServicioBl.ActualizarServicio(servicio);
        }
        public List<ListaDtoResponse> ListarServicios(ServicioDtoRequest servicio)
        {
            return iServicioBl.ListarServicios(servicio);
        }

        public ServicioDtoResponse ObtenerServicio(ServicioDtoRequest servicio)
        {
            return iServicioBl.ObtenerServicio(servicio);
        }

        public ProcesoResponse RegistrarServicio(ServicioDtoRequest servicio)
        {
            return iServicioBl.RegistrarServicio(servicio);
        }
        public ServicioPaginadoDtoResponse ListaServicioPaginado(ServicioDtoRequest servicio)
        {
            return iServicioBl.ListaServicioPaginado(servicio);
        }
        public ProcesoResponse InhabilitarServicio(ServicioDtoRequest servicio)
        {
            return iServicioBl.InhabilitarServicio(servicio);
        }


    }
}
