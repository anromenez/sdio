﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun
{
    public partial class ServicioComunSDio
    {

        public ProcesoResponse ActualizarMedio(MedioDtoRequest medio)
        {
            return iMedioBl.ActualizarMedio(medio);
        }

        public List<ListaDtoResponse> ListarMedio(MedioDtoRequest medio)
        {
            return iMedioBl.ListarMedio(medio);
        }

        public MedioDtoResponse ObtenerMedio(MedioDtoRequest medio)
        {
            return iMedioBl.ObtenerMedio(medio);
        }

        public ProcesoResponse RegistrarMedio(MedioDtoRequest medio)
        {
            return iMedioBl.RegistrarMedio(medio);
        }
    }
}
