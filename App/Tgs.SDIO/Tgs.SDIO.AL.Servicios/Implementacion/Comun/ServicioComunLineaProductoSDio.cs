﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.DataContracts.Dto.Request.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.Entities.Entities.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Comun

{
    public partial class ServicioComunSDio
    {
        public ProcesoResponse ActualizarLineaProducto(LineaProductoDtoRequest LineaProducto)
        {


            return iLineaProductoBl.ActualizarLineaProducto(LineaProducto);
        }

        public List<LineaProductoDtoResponse> ListarLineaProducto(LineaProductoDtoRequest LineaProducto)
        {
            return iLineaProductoBl.ListarLineaProducto(LineaProducto);
        }

        public LineaProductoDtoResponse ObtenerLineaProducto(LineaProductoDtoRequest LineaProducto)
        {
            return iLineaProductoBl.ObtenerLineaProducto(LineaProducto);
        }

        public ProcesoResponse RegistrarLineaProducto(LineaProductoDtoRequest LineaProducto)
        {
            return iLineaProductoBl.RegistrarLineaProducto(LineaProducto);
        }
    }
}
