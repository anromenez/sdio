﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio 
    {
        public ProcesoResponse ActualizarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad)
        {
            return iOportunidadServicioCMIBl.ActualizarOportunidadServicioCMI(oportunidad);
        }

        public List<OportunidadServicioCMIDtoResponse> ListarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad)
        {
            return iOportunidadServicioCMIBl.ListarOportunidadServicioCMI(oportunidad);
        }

        public ProcesoResponse RegistrarOportunidadServicioCMI(OportunidadServicioCMIDtoRequest oportunidad)
        {
            return iOportunidadServicioCMIBl.RegistrarOportunidadServicioCMI(oportunidad);
        }
    }
}
