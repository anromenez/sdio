﻿using System;
using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Oportunidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;
using Tgs.SDIO.DataContracts.Dto.Response.Oportunidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Oportunidad
{
    public partial class ServicioOportunidadSDio
    {

        public ProcesoResponse RegistrarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.RegistrarOportunidadFlujoCaja(flujocaja);
        }

        public ProcesoResponse ActualizarOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ActualizarOportunidadFlujoCaja(flujocaja);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ObtenerOportunidadFlujoCaja(flujocaja);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListaOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListaOportunidadFlujoCaja(flujocaja);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarOportunidadFlujoCajaBandeja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListarOportunidadFlujoCajaBandeja(flujocaja);
        }

        public List<ListaDtoResponse> ListaAnoMesProyecto(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ListaAnoMesProyecto(flujocaja);
        }
        public List<OportunidadFlujoCajaDtoResponse> GeneraCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.GeneraCasoNegocio(casonegocio);
        }

        public ProcesoResponse EliminarCasoNegocio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.EliminarCasoNegocio(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> GeneraServicio(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.GeneraServicio(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadCaratula(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ListarDetalleOportunidadCaratula(casonegocio);
        }

        public List<OportunidadFlujoCajaDtoResponse> ListarDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ListarDetalleOportunidadEcapex(casonegocio);
        }

        public OportunidadFlujoCajaDtoResponse ObtenerDetalleOportunidadEcapex(OportunidadFlujoCajaDtoRequest casonegocio)
        {
            return iOportunidadFlujoCajaBl.ObtenerDetalleOportunidadEcapex(casonegocio);
        }

        public ProcesoResponse ActualizarPeriodoOportunidadFlujoCaja(OportunidadFlujoCajaDtoRequest flujocaja)
        {
            return iOportunidadFlujoCajaBl.ActualizarPeriodoOportunidadFlujoCaja(flujocaja);
        }
    }
}
