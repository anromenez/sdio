﻿using System;
using System.ServiceModel;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.Servicios.Interfaces.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Trazabilidad
{
    [ServiceBehavior(Namespace = "http://TGestiona/SDIO/ServicioTrazabilidadSDio", Name = "ServicioTrazabilidadSDio",
    ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public partial class ServicioTrazabilidadSDio : IServicioTrazabilidadSDio
    {
        private readonly IAreaSeguimientoBl iareaSeguimientoBl;
        private readonly ISegmentoNegocioBl isegmentoNegocioBl;
        private readonly IAreaSegmentoNegocioBl iareaSegmentoNegocioBl;
        private readonly IRecursoBl irecursoBl;
        private readonly IRolRecursoBl irolRecursoBl;
        private readonly IConceptoSeguimientoBl iconceptoSeguimientoBl;
        private readonly IActividadOportunidadBl iactividadOportunidadBl;
        private readonly IEtapaOportunidadBl ietapaOportunidadBl;
        private readonly IFaseOportunidadBl ifaseOportunidadBl;
        private readonly IObservacionOportunidadBl iobservacionOportunidadBl;
        private readonly IRecursoOportunidadBl irecursoOportunidadBl;
        private readonly IOportunidadSTBl ioportunidadSTBl;
        private readonly ICasoOportunidadBl icasoOportunidadBl;
        private readonly IDatosPreventaMovilBl idatosPreventaMovilBl;
        private readonly IEstadoCasoBl iestadoCasoBl;
        private readonly ITipoOportunidadBl itipoOportunidadBl;
        private readonly IMotivoOportunidadBl imotivoOportunidadBl;
        private readonly IFuncionPropietarioBl ifuncionPropietarioBl;
        public ServicioTrazabilidadSDio(
            IAreaSeguimientoBl areaSeguimientoBl,
            ISegmentoNegocioBl segmentonegocioBl,
            IAreaSegmentoNegocioBl areaSegmentoNegocioBl,
            IRecursoBl recursoBl,
            IRolRecursoBl rolRecursoBl,
            IConceptoSeguimientoBl conceptoSeguimientoBl,
            IActividadOportunidadBl actividadOportunidadBl,
            IEtapaOportunidadBl etapaOportunidadBl,
            IFaseOportunidadBl faseOportunidadBl,
            IObservacionOportunidadBl observacionOportunidadBl,
            IRecursoOportunidadBl recursoOportunidadBl,
            IOportunidadSTBl oportunidadSTBl,
            ICasoOportunidadBl casoOportunidadBl,
            IDatosPreventaMovilBl datosPreventaMovilBl,
            IEstadoCasoBl estadoCasoBl,
            ITipoOportunidadBl tipoOportunidadBl,
            IMotivoOportunidadBl motivoOportunidadBl,
            IFuncionPropietarioBl funcionPropietarioBl
            )
        {
            iareaSeguimientoBl = areaSeguimientoBl;
            isegmentoNegocioBl = segmentonegocioBl;
            iareaSegmentoNegocioBl = areaSegmentoNegocioBl;
            irecursoBl = recursoBl;
            irolRecursoBl = rolRecursoBl;
            iconceptoSeguimientoBl = conceptoSeguimientoBl;
            irolRecursoBl = rolRecursoBl;
            iconceptoSeguimientoBl = conceptoSeguimientoBl;
            iactividadOportunidadBl = actividadOportunidadBl;
            ietapaOportunidadBl = etapaOportunidadBl;
            ifaseOportunidadBl = faseOportunidadBl;
            iobservacionOportunidadBl = observacionOportunidadBl;
            irecursoOportunidadBl = recursoOportunidadBl;
            ioportunidadSTBl = oportunidadSTBl;
            icasoOportunidadBl = casoOportunidadBl;
            idatosPreventaMovilBl = datosPreventaMovilBl;
            iestadoCasoBl = estadoCasoBl;
            itipoOportunidadBl = tipoOportunidadBl;
            imotivoOportunidadBl = motivoOportunidadBl;
            ifuncionPropietarioBl = funcionPropietarioBl;
        }

        
    }
}
