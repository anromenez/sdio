﻿using System.Collections.Generic;
using Tgs.SDIO.DataContracts.Dto.Request.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Trazabilidad;
using Tgs.SDIO.DataContracts.Dto.Response.Comun;

namespace Tgs.SDIO.AL.Servicios.Implementacion.Trazabilidad
{
    public partial class ServicioTrazabilidadSDio
    {
        public AreaSeguimientoPaginadoDtoResponse ListadoAreasSeguimientoPaginado(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.ListadoAreasSeguimientoPaginado(request);
        }
        public AreaSeguimientoDtoResponse ObtenerAreaSeguimientoPorId(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.ObtenerAreaSeguimientoPorId(request);
        }
        public ProcesoResponse ActualizarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.ActualizarAreaSeguimiento(request);
        }
        public ProcesoResponse RegistrarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.RegistrarAreaSeguimiento(request);
        }
        public ProcesoResponse EliminarAreaSeguimiento(AreaSeguimientoDtoRequest request)
        {
            return iareaSeguimientoBl.EliminarAreaSeguimiento(request);
        }
        public List<ComboDtoResponse> ListarComboSegmentoNegocio()
        {
            return isegmentoNegocioBl.ListarComboSegmentoNegocio();
        }
        public AreaSegmentoNegocioDtoResponse ObtenerAreaSegmentoNegocioPorId(AreaSegmentoNegocioDtoRequest request)
        {
            return iareaSegmentoNegocioBl.ObtenerAreaSegmentoNegocioPorId(request);
        }
        public ProcesoResponse RegistrarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request)
        {
            return iareaSegmentoNegocioBl.RegistrarAreaSegmentoNegocio(request);
        }
        public ProcesoResponse EliminarAreaSegmentoNegocio(AreaSegmentoNegocioDtoRequest request)
        {
            return iareaSegmentoNegocioBl.EliminarAreaSegmentoNegocio(request);
        }
        public RecursoPaginadoDtoResponse ListadoRecursosPaginado(RecursoDtoRequest request)
        {
            return irecursoBl.ListadoRecursosPaginado(request);
        }
        public RecursoDtoResponse ObtenerRecursoPorId(RecursoDtoRequest request)
        {
            return irecursoBl.ObtenerRecursoPorId(request);
        }
        public ProcesoResponse ActualizarRecurso(RecursoDtoRequest request)
        {
            return irecursoBl.ActualizarRecurso(request);
        }
        public ProcesoResponse RegistrarRecurso(RecursoDtoRequest request)
        {
            return irecursoBl.RegistrarRecurso(request);
        }
        public ProcesoResponse EliminarRecurso(RecursoDtoRequest request)
        {
            return irecursoBl.EliminarRecurso(request);
        }
        public RolRecursoPaginadoDtoResponse ListadoRolRecursosPaginado(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.ListadoRolRecursosPaginado(request);
        }
        public RolRecursoDtoResponse ObtenerRolRecursoPorId(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.ObtenerRolRecursoPorId(request);
        }
        public ProcesoResponse ActualizarRolRecurso(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.ActualizarRolRecurso(request);
        }
        public ProcesoResponse RegistrarRolRecurso(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.RegistrarRolRecurso(request);
        }
        public ProcesoResponse EliminarRolRecurso(RolRecursoDtoRequest request)
        {
            return irolRecursoBl.EliminarRolRecurso(request);
        }
        public ConceptoSeguimientoPaginadoDtoResponse ListadoConceptosSeguimientoPaginado(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.ListadoConceptosSeguimientoPaginado(request);
        }
        public ConceptoSeguimientoDtoResponse ObtenerConceptoSeguimientoPorId(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.ObtenerConceptoSeguimientoPorId(request);
        }
        public ProcesoResponse ActualizarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.ActualizarConceptoSeguimiento(request);
        }
        public ProcesoResponse RegistrarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.RegistrarConceptoSeguimiento(request);
        }
        public ProcesoResponse EliminarConceptoSeguimiento(ConceptoSeguimientoDtoRequest request)
        {
            return iconceptoSeguimientoBl.EliminarConceptoSeguimiento(request);
        }
        public ActividadOportunidadPaginadoDtoResponse ListatActividadOportunidadPaginado(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadBl.ListatActividadOportunidadPaginado(request);
        }
        public ActividadOportunidadDtoResponse ObtenerActividadOportunidadPorId(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadBl.ObtenerActividadOportunidadPorId(request);
        }
        public ProcesoResponse ActualizarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadBl.ActualizarActividadOportunidad(request);
        }
        public ProcesoResponse RegistrarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadBl.RegistrarActividadOportunidad(request);
        }
        public ProcesoResponse EliminarActividadOportunidad(ActividadOportunidadDtoRequest request)
        {
            return iactividadOportunidadBl.EliminarActividadOportunidad(request);
        }
        public List<ListaDtoResponse> ListarComboAreaSeguimiento()
        {
            return iareaSeguimientoBl.ListarComboAreaSeguimiento();
        }
        public List<ListaDtoResponse> ListarComboEtapaOportunidad()
        {
            return ietapaOportunidadBl.ListarComboEtapaOportunidad();
        }
        //EAAR:PF12
        public List<ListaDtoResponse> ListarComboFaseOportunidad()
        {
            return ifaseOportunidadBl.ListarComboFaseOportunidad();
        }
        public ObservacionOportunidadPaginadoDtoResponse ListadoObservacionesOportunidadPaginado(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.ListadoObservacionesOportunidadPaginado(request);
        }
        public ObservacionOportunidadDtoResponse ObtenerObservacionOportunidadPorId(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.ObtenerObservacionOportunidadPorId(request);
        }
        public ProcesoResponse ActualizarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.ActualizarObservacionOportunidad(request);
        }
        public ProcesoResponse RegistrarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.RegistrarObservacionOportunidad(request);
        }
        public ProcesoResponse EliminarObservacionOportunidad(ObservacionOportunidadDtoRequest request)
        {
            return iobservacionOportunidadBl.EliminarObservacionOportunidad(request);
        }
        public RecursoOportunidadPaginadoDtoResponse ListadoRecursosOportunidadPaginado(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.ListadoRecursosOportunidadPaginado(request);
        }
        public RecursoOportunidadDtoResponse ObtenerRecursoOportunidadPorId(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.ObtenerRecursoOportunidadPorId(request);
        }
        public ProcesoResponse ActualizarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.ActualizarRecursoOportunidad(request);
        }
        public ProcesoResponse RegistrarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.RegistrarRecursoOportunidad(request);
        }
        public ProcesoResponse EliminarRecursoOportunidad(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.EliminarRecursoOportunidad(request);
        }
        public List<ListaDtoResponse> ListarComboEtapaOportunidadPorIdFase(FaseOportunidadDtoRequest request)
        {
            return ietapaOportunidadBl.ListarComboEtapaOportunidadPorIdFase(request);
        }
        public List<ListaDtoResponse> ListarComboActividadPadres()
        {
            return iactividadOportunidadBl.ListarComboActividadPadres();
        }
        public List<ListaDtoResponse> ListarComboConceptoSeguimiento()
        {
            return iconceptoSeguimientoBl.ListarComboConceptoSeguimiento();
        }
        public List<ListaDtoResponse> ListarComboRolRecurso()
        {
            return irolRecursoBl.ListarComboRolRecurso();
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalObservacionPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTModalObservacionPaginado(request);
        }
        public CasoOportunidadDtoResponse ObtenerCasoOportunidadPorId(CasoOportunidadDtoRequest request)
        {
            return icasoOportunidadBl.ObtenerCasoOportunidadPorId(request);
        }
        public OportunidadSTDtoResponse ObtenerOportunidadSTPorId(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ObtenerOportunidadSTPorId(request);
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTModalRecursoPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTModalRecursoPaginado(request);
        }
        public RecursoPaginadoDtoResponse ListaRecursoModalRecursoPaginado(RecursoDtoRequest request)
        {
            return irecursoBl.ListaRecursoModalRecursoPaginado(request);
        }
        public OportunidadSTPaginadoDtoResponse ListaOportunidadSTMasivoPaginado(OportunidadSTDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTMasivoPaginado(request);
        }
        public ProcesoResponse RegistrarRecursoOportunidadMasivo(RecursoOportunidadDtoRequest request)
        {
            return irecursoOportunidadBl.RegistrarRecursoOportunidadMasivo(request);
        }
        public OportunidadSTSeguimientoPreventaPaginadoDtoResponse ListaOportunidadSTSeguimientoPreventaPaginado(OportunidadSTSeguimientoPreventaDtoRequest request)
        {
            return ioportunidadSTBl.ListaOportunidadSTSeguimientoPreventaPaginado(request);
        }

        public DatosPreventaMovilDtoResponse ObtenerDatosPreventaMovilPorId(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.ObtenerDatosPreventaMovilPorId(request);
        }

        public ProcesoResponse ActualizarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.ActualizarDatosPreventaMovil(request);
        }

        public ProcesoResponse RegistrarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.RegistrarDatosPreventaMovil(request);
        }

        public ProcesoResponse EliminarDatosPreventaMovil(DatosPreventaMovilDtoRequest request)
        {
            return idatosPreventaMovilBl.EliminarDatosPreventaMovil(request);
        }

        public List<ListaDtoResponse> ListarComboEstadoCaso()
        {
            return iestadoCasoBl.ListarComboEstadoCaso();
        }

        public List<ListaDtoResponse> ListarComboTipoOportunidad()
        {
            return itipoOportunidadBl.ListarComboTipoOportunidad();
        }

        public List<ListaDtoResponse> ListarComboSegmentoNegocioSimple()
        {
            return isegmentoNegocioBl.ListarComboSegmentoNegocioSimple();
        }

        public List<ListaDtoResponse> ListarComboMotivoOportunidad()
        {
            return imotivoOportunidadBl.ListarComboMotivoOportunidad();
        }

        public List<ListaDtoResponse> ListarComboFuncionPropietario()
        {
            return ifuncionPropietarioBl.ListarComboFuncionPropietario();
        }
    }

}
