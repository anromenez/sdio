﻿
namespace Tgs.SDIO.Util.Validador
{ 
    public interface IEntityValidatorFactory
    { 
        IEntityValidator Create();
    }
}
