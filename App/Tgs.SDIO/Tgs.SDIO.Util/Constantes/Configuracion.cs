﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Constantes
{
    public static class Configuracion
    {
        public static string CodigoAplicacion
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings.Get("CodigoAplicacion"));
            }
        }

        public static int CodigoEmpresa
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings.Get("CodigoEmpresa"));
            }
        }

        public static int IdSistema
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings.Get("IdSistema"));
            }
        }

        public static string CorreoSalida
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("CorreoSalida");
            }
        }

        public static string NombreSistemaCorreoSalida
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("NombreSistema");
            }
        }

        public static bool DummyServicio
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings.Get("DummyServicio"));
            }
        }

    }
}
