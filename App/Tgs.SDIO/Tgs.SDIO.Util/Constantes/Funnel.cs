﻿namespace Tgs.SDIO.Util.Constantes
{
    public static class Funnel
    {
        public class ExportacionMadurez
        {
            public const string NombreHojaExportacion = "OfertasPorAño";
            public const string FormatoExportacionMontos = "###,###,##0.00";
            public const string CabeceraMes = "Mes";
            public const string CabeceraIdOportunidad = "Id de Oportunidad";
            public const string CabeceraLineaNegocio = "Linea de Negocio";
            public const string CabeceraSector = "Sector";
            public const string CabeceraNombreCliente = "Nombre de Cliente";
            public const string CabeceraCodigoCliente = "Codigo de Cliente";
            public const string CabeceraIngresoTotal = "Ingreso Total";
            public const string CabeceraCapex = "Capex";
            public const string CabeceraOpex = "Opex";
            public const string CabeceraOibda = "Oibda";
            public const string CabeceraMadurez = "Madurez";
            public const string CabeceraDetalleOportunidades = "Oportunidades Trabajadas por Linea Negocio";
            public const string CabeceraDetalleIngresos = "Ingresos por Linea de Negocio";
            public const string CabeceraDetalleCapex = "Capex por Linea Negocio";
            public const string CabeceraDetalleOpex = "Opex por Linea Negocio";
            public const string CabeceraDetalleOidba = "Oidba por Linea Negocio";
            public const string CabeceraDetalleN1 = "N1";
            public const string CabeceraDetalleN2 = "N2";
            public const string CabeceraDetalleN3 = "N3";
            public const string CabeceraDetalleN4 = "N4";
            public const string CabeceraDetalleN5 = "N5";
        }

        public class Extensiones
        {
            public const string xls = "xls";
            public const string xlsx = "xlsx";
        }

    }
}
