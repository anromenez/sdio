﻿namespace Tgs.SDIO.Util.Constantes
{
    public static class Generales
    {
        public enum Sistemas
        {
            Comun,
            Oportunidad,
            CartaFianza
        }
        public class EstadoLogico
        {
            public const bool Verdadero = true;
            public const bool Faslo = false;

        }
        public class Numeric
        {
            public const int Cero = 0;
            public const int Uno = 1;
            public const int NegativoUno = -1;
        }
        public class Confirmacion
        {
            public const string si = "Si";
            public const string no = "No";
        }
        public class Estados
        {
            public const int Inactivo = 0;
            public const int Activo = 1;

            public const int EnProceso = 1;
            public const int registrado = 2;
            public const int EvaluacionEconomica = 3;
            public const int Aprobado = 4;
            public const int Rechazado = 5;
            public const int Ganado = 6;
            public const int Cancelado = 7;
            public const int RechazadoPorCliente = 8;
        }

        public class EstadoDescripcion
        {
            public const string Inactivo = "Inactivo";
            public const string Activo = "Activo";
        }
        public class Proceso
        {
            public const int Valido = 0;
            public const int Invalido = 1;
        }
        public class LineasProducto
        {
            public const int Datos = 5;
        }


        public class TablaMaestra
        {

            public const int TipoProyecto = 19;
            public const int EstadoBasicos = 1;
            public const int TipoCostos = 4;
            public const int ProyectoTipo = 7;
            public const int EstadoProyectos = 11;
            public const int TipoServicio = 14;
            public const int GerenciaProductos = 17;
            public const int Periodos = 28;
            public const int TiposConceptos = 32;
            public const int ConceptosAdicionales = 35;
            public const int TiposCosto = 4;
            public const int Agrupador = 66;
            public const int TipoProveedor = 74;
            public const int TipoArchivo = 75;
            public const int Medio = 84;
            public const int TipoEnlace = 89;
            public const int ActivoPasivo = 92;
            public const int Localidad = 95;
            public const int Renovacion = 8;
            public const int ValoresFijos = 107;
            public const int TipoEmpresa = 19;
            public const int Etapas = 129;
            public const int Madurez = 136;
        }

        public class TipoConcepto
        {
            public const int Capex = 2;
            public const int Opex = 1;
            public const int Unicos = 3;
            public const int Unicos2 = 4;
            public const int Unicos3 = 5;
            public const int Unicos4 = 6;
            public const int Unicos7 = 7;

        }

        public class IdConcepto
        {
            public const int Depreciacion = 119;
            public const int PagoUnico = 114;
            public const int Recurrente = 115;
        }

        public class TipoPerfil
        {
            public const string Administrador = "ADM";
            public const string Gerente = "GRT";
            public const string Analista_FIN = "ANLFIN";
            public const string Product_Manager = "PDM";
            public const string Preventa = "PrevOP";
            public const string Coordinador_FIN = "CoFiNa";
            public const string Analista = "Analista";

        }

        public class Plantilla
        {
            public const int Id = 23;
            public const int Housing = 1;
            public const int Outsourcing = 2;
            public const int Social = 3;
            public const int Atento = 4;
            public const int Datos = 5;
            public const int Caratula = 1;
            public const int ECapex = 2;
        }


        public class Conceptos
        {
            public const int Circuitos = 1;
            public const int Medio3G = 2;
            public const int Opex = 3;
            public const int RouterSonda = 4;
            public const int SeguridadOpex = 5;
            public const int EstudiosEspeciales = 6;
            public const int EquiposEE = 7;
            public const int Routers = 8;
            public const int Modems = 9;
            public const int EquiposSeguridad = 10;
            public const int Hardware = 11;
            public const int Software = 12;
            public const int Gabinetes = 13;
            public const int Ghz = 14;
            public const int SolarWind = 15;
            public const int SmartVPN = 16;
            public const int Satelitales = 17;
            public const int IdRouterSonda = 177;
            public const int IdIngreso = 116;
            public const int IdCostoDirecto = 141;
            public const int IdOibda = 143;
            public const int IdCapex = 144;
        }

        public class Calculos
        {
            public const int Instalacion = 110;
            public const int Desinstalacion = 111;
        }

        public class MesDepreciacion
        {
            public const int meses1 = 120;
            public const int meses2 = 60;
        }
        public class ColorLinea
        {
            public const string Linea1 = "#fb0201";
            public const string Linea2 = "#fb0201";
            public const string Linea3 = "#fb0201";
            public const string Linea4 = "#fb0201";
            public const string Linea5 = "#fb0201";
        }

        public class TiposMadurez
        {
            public const string N1 = "N1";
            public const string N2 = "N2";
            public const string N3 = "N3";
            public const string N4 = "N4";
            public const string N5 = "N5";
        }

        public enum TiposDetalleOfertaAnio
        {
            OportunidadesTrabajadas,
            Ingresos,
            Capex,
            Opex,
            Oidba
        }

    }
}
