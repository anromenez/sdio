﻿namespace Tgs.SDIO.Util.Enumerables
{
    public class EnumReportes
    {
        public enum Reportes
        { 
            Prueba,
            DashboardPrincipal,
            ProbabilidadPorMesPopup,
            MadurezPorMes,
            LineaNegocioPeriodo,
            IngresoLineaNegocioPeriodo,
            IngresoSectorPeriodo,
            OfertaPorEstado,
            EvolutivoOportunidad,
            OfertasPorCliente,
            OfertasPorSector
        } 
    }
}

