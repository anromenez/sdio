﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Error.Comun;

namespace Tgs.SDIO.Util.Funciones
{
    public static class Funciones
    {
        public static string GenerarClaveAleatoria(int numMinusculas, int numMayusculas, int numNumeros)
        {
            string lowers = "abcdefghijklmnopqrstuvwxyz";
            string uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string number = "0123456789";

            Random random = new Random();

            string generated = "!";
            for (int i = 1; i <= numMinusculas; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    lowers[random.Next(lowers.Length - 1)].ToString()
                );

            for (int i = 1; i <= numMayusculas; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    uppers[random.Next(uppers.Length - 1)].ToString()
                );

            for (int i = 1; i <= numNumeros; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    number[random.Next(number.Length - 1)].ToString()
                );

            return generated.Replace("!", string.Empty);
        }

        public static string FormatoFecha(DateTime fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:dd/MM/yyyy}", fecha);
        } 

        public static string FormatoFechaHora(DateTime fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:dd/MM/yyyy HH:mm}", fecha);
        }

      
        public static string FormatoHora(DateTime fecha)
        {
            CultureInfo Culture = CultureInfo.CreateSpecificCulture("es-PE");
            return String.Format(Culture, "{0:T}", fecha);
        }

        public static string GenerarNombreArchivo(string nombreArchivo)
        {
            return DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "_" + nombreArchivo;
        }

        public static byte[] ConvertirDatosABytes(Stream input)
        {
            byte[] archivo = null;

            using (Stream inputStream = input)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;

                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }

                archivo = memoryStream.ToArray();

                inputStream.Dispose();
            }

            return archivo;
        }

        public static string EnviarCorreo(DatosCorreo datosEnvio)
        {
            string respuesta = ErrorGeneralComun.Ok;

            try
            {
                MailMessage Mensaje = new MailMessage();

                Mensaje.From = new MailAddress(datosEnvio.CorreoEnvia);

                foreach (string Correo in datosEnvio.Destinatarios.Split(','))
                {
                    Mensaje.To.Add(new MailAddress(Correo));
                }

                Mensaje.Subject = datosEnvio.Asunto;
                Mensaje.Body = datosEnvio.Cuerpo;
                Mensaje.IsBodyHtml = true;

                SmtpClient SMTP = new SmtpClient(ConfigurationManager.AppSettings["Smtp_server"], Convert.ToInt32(ConfigurationManager.AppSettings["Smtp_port"]));
                SMTP.EnableSsl = false;
                System.Threading.Thread.Sleep(1000);
                SMTP.Send(Mensaje);
                SMTP.Dispose();
            }
            catch (Exception)
            {
                respuesta = ErrorGeneralComun.Fallo;
            } 

            return respuesta;
        }

        public static int DiferenciaMeses(DateTime FechaFin, DateTime FechaInicio)
        {
            return Math.Abs((FechaFin.Month - FechaInicio.Month) + 12 * (FechaFin.Year - FechaInicio.Year));
        }

    }
}
