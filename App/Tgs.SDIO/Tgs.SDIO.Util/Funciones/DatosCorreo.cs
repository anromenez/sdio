﻿namespace Tgs.SDIO.Util.Funciones
{
    public  class DatosCorreo
    {       
        public string Cuerpo { get; set; }
        public string Destinatarios { get; set; }
        public string Asunto { get; set; }
        public string CorreoEnvia { get; set; }
    }
}
