﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tgs.SDIO.Util.Error.Logging.Implementaciones
{
    public enum TipoException
    {
        ReglaNegocio = 1,
        ValidacionEntidad = 2,
        Otro = 3
    }
}
