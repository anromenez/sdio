﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.Util.Error.Logging.Implementaciones
{
    public static class ExceptionManager
    {
        public static void GenerarAppExcepcionReglaNegocio(string mensaje, Generales.Sistemas sistema)
        {
            GenerarAppExcepcion(TipoException.ReglaNegocio, string.Format("SISTEMA:{0},{1}", sistema.ToString(),mensaje), string.Empty, null, string.Empty);
        }

        public static void GenerarAppExcepcionValidacion(string mensaje, string accion, Generales.Sistemas sistema)
        {
            GenerarAppExcepcion(TipoException.ValidacionEntidad, string.Format("SISTEMA:{0},{1}", sistema.ToString(), mensaje), accion, null, null);
        }

        public static void GenerarAppExcepcionGeneral(string mensaje)
        {
            GenerarAppExcepcion(TipoException.Otro, mensaje, null, null, null);
        }

        #region Private Methods
        private static void GenerarAppExcepcion(TipoException tipo, string mensaje, string accion, Exception ex, string codigo)
        {
            if (ex != null)
                throw new AppException(tipo, mensaje, accion, ex);
            else
                throw new AppException(tipo, mensaje, accion, codigo);
        }
        #endregion
    }
}
