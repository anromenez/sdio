﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Tgs.SDIO.Util.Error.Logging.Interfaces;

namespace Tgs.SDIO.Util.Error.Logging.NLog
{
    public class NLogLogging : Interfaces.ILogger
    {
        Logger logger = null;
        public NLogLogging(string nombre)
        {
            logger = LogManager.GetLogger(nombre);
        }

        public void Debug(string message, params object[] args)
        {
            logger.Debug(message, args);
        }

        public void Debug(string message, Exception exception, params object[] args)
        {
            logger.Debug(exception, message, args);
        }

        public void Debug(object item)
        {
            logger.Debug(item);
        }

        public void Fatal(string message, params object[] args)
        {
            logger.Debug(message, args);
        }

        public void Fatal(string message, Exception exception, params object[] args)
        {
            logger.Fatal(exception, message, args);
        }

        public void LogError(string message, params object[] args)
        {
            logger.Error(message, args);
        }

        public void LogError(string message, Exception exception, params object[] args)
        {
            logger.Error(exception, message, args);
        }

        public void LogInfo(string message, params object[] args)
        {
            logger.Info(message, args);
        }

        public void LogWarning(string message, params object[] args)
        {
            logger.Warn(message, args);
        }
    }
}
