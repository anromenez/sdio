﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tgs.SDIO.DataContracts.Base;
using Tgs.SDIO.Util.Error.Logging;
using Tgs.SDIO.Util.Error.Logging.Implementaciones;

namespace Tgs.SDIO.AL.Host.Utils
{
    public class FactoryError
    {
        public static ErrorDto ConstruirError(Exception error)
        {
            var errorDto = new ErrorDto
            {
                IdError = Guid.NewGuid(),
                Mensaje = error.InnerException != null ? error.InnerException.ToString() : error.Message
            };

            if (error.GetType() == typeof(TimeoutException))
            {
                errorDto.Mensaje = "La solicitud tomo demasiado tiempo. Por favor reintente o pongase en contacto con el Administrador del Sistema";
                errorDto.Tipo = TipoErrorServicioEnum.TipoErrorServicio.ErrorGeneral;
            }
            else if (error.GetType() == typeof(AppException))
            {
                var errorApp = (AppException)error;

                errorDto.IdError = errorApp.Id;
                switch (errorApp.Tipo)
                {
                    case TipoException.ReglaNegocio:
                        errorDto.Tipo = TipoErrorServicioEnum.TipoErrorServicio.ErrorNegocio;
                        break;
                    case TipoException.ValidacionEntidad:
                        errorDto.Tipo = TipoErrorServicioEnum.TipoErrorServicio.ErrorValidacion;
                        break;
                    case TipoException.Otro:
                        errorDto.Tipo = TipoErrorServicioEnum.TipoErrorServicio.ErrorGeneral;
                        break;
                    default:
                        errorDto.Tipo = TipoErrorServicioEnum.TipoErrorServicio.ErrorNoManejado;
                        break;
                }
                errorDto.Mensaje = errorApp.MensajeDefault;
                errorDto.MensajeCorto = errorApp.Mensaje;
            }
            else
            {
                errorDto.Tipo = TipoErrorServicioEnum.TipoErrorServicio.ErrorNoManejado;
            }

            return errorDto;
        }
    }
}