﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace Tgs.SDIO.AL.Host.Utils
{
    public class ConfigurationHelper
    {
        public static ErrorBehavior ObtenerConfiguracion(string configurationFilePath)
        {
            var map = new ExeConfigurationFileMap
            {
                ExeConfigFilename = configurationFilePath
            };
            var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);

            var section = config.GetSectionGroup("system.serviceModel") as ServiceModelSectionGroup;
            var behaviors = (BehaviorsSection)section.Sections["behaviors"];
            var serviceBehavior = behaviors.ServiceBehaviors["BehaviorServicio"];
            foreach (var extension in serviceBehavior)
            {
                if (extension.GetType() == typeof(ErrorBehavior))
                {
                    var t = extension as ErrorBehavior;
                    return t;
                }
            }
            return null;
        }

    }
}
