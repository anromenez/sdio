﻿using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Comun;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Seguridad;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Trazabilidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Comun;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Seguridad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Core.Context;
using Tgs.SDIO.AL.DataAccess.Implementacion.Comun;
using Tgs.SDIO.AL.DataAccess.Interfaces.Comun;
using Tgs.SDIO.AL.DataAccess.Implementacion.Trazabilidad;
using Tgs.SDIO.AL.DataAccess.Interfaces.Trazabilidad;
using Tgs.SDIO.Util.Adapter;
using Tgs.SDIO.Util.AutoMapper;
using Tgs.SDIO.Util.Error.Logging.Interfaces;
using Tgs.SDIO.Util.Error.Logging.NLog;
using Tgs.SDIO.AL.DataAccess.Interfaces.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Oportunidad;
using Tgs.SDIO.AL.DataAccess.Implementacion.Oportunidad;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.Funnel;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.Funnel;
using Tgs.SDIO.AL.DataAccess.Implementacion.Funnel;
using Tgs.SDIO.AL.DataAccess.Interfaces.CartaFianza;
using Tgs.SDIO.AL.DataAccess.Implementacion.CartaFianza;
using Tgs.SDIO.AL.BussinesLayer.Interfaces.CartaFianza;
using Tgs.SDIO.AL.BussinesLayer.Implementacion.CartaFianza;

namespace Tgs.SDIO.AL.Host.Configuration
{
    public static class InjectorConfiguration
    {
        public static void ConfigurationContainer()
        {

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();

            container.Register<DioContext, DioContext>(Lifestyle.Scoped);

            container.Register<ITypeAdapterFactory, AutoMapperTypeAdapterFactory>(Lifestyle.Singleton);
            //container.Register<IEntityValidatorFactory, DataAnnotationEntityValidatorFactory>(Lifestyle.Singleton);
            container.Register<ILoggerFactory, NLogLoggingFactory>(Lifestyle.Singleton);


            container.Register<ISalesForceConsolidadoCabeceraDal, SalesForceConsolidadoCabeceraDal>(Lifestyle.Scoped);
            container.Register<ISalesForceConsolidadoCabeceraBl, SalesForceConsolidadoCabeceraBl>(Lifestyle.Scoped);

			 #region CartaFianza

            container.Register<ICartaFianzaDal, CartaFianzaDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaColaboradorDetalleDal, CartaFianzaColaboradorDetalleDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleAccionDal, CartaFianzaDetalleAccionesDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleEstadoDal, CartaFianzaDetalleEstadoDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRecuperoDal, CartaFianzaDetalleRecuperoDal>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRenovacionDal, CartaFianzaDetalleRenovacionDal>(Lifestyle.Scoped);

            container.Register<ICartaFianzaBl, CartaFianzaBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaColaboradorDetalleBl, CartaFianzaColaboradorDetalleBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleAccionBl, CartaFianzaDetalleAccionBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleEstadoBl, CartaFianzaDetalleEstadoBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRecuperoBl, CartaFianzaDetalleRecuperoBl>(Lifestyle.Scoped);
            container.Register<ICartaFianzaDetalleRenovacionBl, CartaFianzaDetalleRenovacionBl>(Lifestyle.Scoped);

            #endregion


          
            #region Oportunidad
            container.Register<IOportunidadFlujoEstadoDal, OportunidadFlujoEstadoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadLineaNegocioDal, OportunidadLineaNegocioDal>(Lifestyle.Scoped);
            container.Register<IOportunidadDal, OportunidadDal>(Lifestyle.Scoped);
            container.Register<IConceptoDal, ConceptoDal>(Lifestyle.Scoped);
            container.Register<IConceptoDatosCapexDal, ConceptoDatosCapexDal>(Lifestyle.Scoped);
            container.Register<ISubServicioDatosCaratulaDal, SubServicioDatosCaratulaDal>(Lifestyle.Scoped);
            container.Register<ILineaConceptoCMIDal, LineaConceptoCMIDal>(Lifestyle.Scoped);
            container.Register<ILineaConceptoGrupoDal, LineaConceptoGrupoDal>(Lifestyle.Scoped);
            container.Register<ILineaPestanaDal, LineaPestanaDal>(Lifestyle.Scoped);
            container.Register<ILineaProductoCMIDal, LineaProductoCMIDal>(Lifestyle.Scoped);
            container.Register<IPestanaGrupoDal, PestanaGrupoDal>(Lifestyle.Scoped);
            container.Register<IProyectoLineaConceptoProyectadoDal, ProyectoLineaConceptoProyectadoDal>(Lifestyle.Scoped);
            container.Register<IProyectoLineaProductoDal, ProyectoLineaProductoDal>(Lifestyle.Scoped);
            container.Register<IProyectoServicioCMIDal, ProyectoServicioCMIDal>(Lifestyle.Scoped);
            container.Register<IProyectoServicioConceptoDal, ProyectoServicioConceptoDal>(Lifestyle.Scoped);
            container.Register<IServicioConceptoDocumentoDal, ServicioConceptoDocumentoDal>(Lifestyle.Scoped);
            container.Register<IServicioConceptoProyectadoDal, ServicioConceptoProyectadoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaDetalleDal, OportunidadFlujoCajaDetalleDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaDal, OportunidadFlujoCajaDal>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaConfiguracionDal, OportunidadFlujoCajaConfiguracionDal>(Lifestyle.Scoped);
            container.Register<IOportunidadServicioCMIDal, OportunidadServicioCMIDal>(Lifestyle.Scoped);
            container.Register<ISubServicioDatosCapexDal, SubServicioDatosCapexDal>(Lifestyle.Scoped);
            container.Register<IOportunidadDocumentoDal, OportunidadDocumentoDal>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioDal, OportunidadTipoCambioDal>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioDetalleDal, OportunidadTipoCambioDetalleDal>(Lifestyle.Scoped);


            container.Register<IOportunidadFlujoEstadoBl, OportunidadFlujoEstadoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadLineaNegocioBl, OportunidadLineaNegocioBl>(Lifestyle.Scoped);
            container.Register<IOportunidadBl, OportunidadBl>(Lifestyle.Scoped);
            container.Register<IConceptoBl, ConceptoBl>(Lifestyle.Scoped);
            container.Register<IConceptoDatosCapexBl, ConceptoDatosCapexBl>(Lifestyle.Scoped);
            container.Register<ISubServicioDatosCaratulaBl, SubServicioDatosCaratulaBl>(Lifestyle.Scoped);
            container.Register<ILineaConceptoCMIBl, LineaConceptoCMIBl>(Lifestyle.Scoped);
            container.Register<ILineaConceptoGrupoBl, LineaConceptoGrupoBl>(Lifestyle.Scoped);
            container.Register<ILineaPestanaBl, LineaPestanaBl>(Lifestyle.Scoped);
            container.Register<ILineaProductoCMIBl, LineaProductoCMIBl>(Lifestyle.Scoped);
            container.Register<IPestanaGrupoBl, PestanaGrupoBl>(Lifestyle.Scoped);
            container.Register<IProyectoLineaConceptoProyectadoBl, ProyectoLineaConceptoProyectadoBl>(Lifestyle.Scoped);
            container.Register<IProyectoLineaProductoBl, ProyectoLineaProductoBl>(Lifestyle.Scoped);
            container.Register<IProyectoServicioCMIBl, ProyectoServicioCMIBl>(Lifestyle.Scoped);
            container.Register<IProyectoServicioConceptoBl, ProyectoServicioConceptoBl>(Lifestyle.Scoped);
            container.Register<IServicioConceptoDocumentoBl, ServicioConceptoDocumentoBl>(Lifestyle.Scoped);
            container.Register<IServicioConceptoProyectadoBl, ServicioConceptoProyectadoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaBl, OportunidadFlujoCajaBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaConfiguracionBl, OportunidadFlujoCajaConfiguracionBl>(Lifestyle.Scoped);
            container.Register<IOportunidadFlujoCajaDetalleBl, OportunidadFlujoCajaDetalle>(Lifestyle.Scoped);
            container.Register<IOportunidadServicioCMIBl, OportunidadServicioCMIBl>(Lifestyle.Scoped);                                                 
            container.Register<ISubServicioDatosCapexBl, SubServicioDatosCapexBl>(Lifestyle.Scoped);
            container.Register<IOportunidadDocumentoBl, OportunidadDocumentoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioBl, OportunidadTipoCambioBl>(Lifestyle.Scoped);
            container.Register<IOportunidadTipoCambioDetalleBl, OportunidadTipoCambioDetalleBl>(Lifestyle.Scoped);
            #endregion


            #region trazabilidad conteiners

            container.Register<IAreaSeguimientoDal, AreaSeguimientoDal>(Lifestyle.Scoped);
            container.Register<IAreaSeguimientoBl, AreaSeguimientoBl>(Lifestyle.Scoped);
            container.Register<ISegmentoNegocioDal, SegmentoNegocioDal>(Lifestyle.Scoped);
            container.Register<ISegmentoNegocioBl, SegmentoNegocioBl>(Lifestyle.Scoped);
            container.Register<IAreaSegmentoNegocioDal, AreaSegmentoNegocioDal>(Lifestyle.Scoped);
            container.Register<IAreaSegmentoNegocioBl, AreaSegmentoNegocioBl>(Lifestyle.Scoped);

            container.Register<IRecursoDal, RecursoDal>(Lifestyle.Scoped);
            container.Register<IRecursoBl, RecursoBl>(Lifestyle.Scoped);
            container.Register<IRolRecursoDal, RolRecursoDal>(Lifestyle.Scoped);
            container.Register<IRolRecursoBl, RolRecursoBl>(Lifestyle.Scoped);
            container.Register<IConceptoSeguimientoDal, ConceptoSeguimientoDal>(Lifestyle.Scoped);
            container.Register<IConceptoSeguimientoBl, ConceptoSeguimientoBl>(Lifestyle.Scoped);
            container.Register<IActividadOportunidadDal, ActividadOportunidadDal>(Lifestyle.Scoped);
            container.Register<IActividadOportunidadBl, ActividadOportunidadBl>(Lifestyle.Scoped);
            container.Register<IEtapaOportunidadDal, EtapaOportunidadDal>(Lifestyle.Scoped);
            container.Register<IEtapaOportunidadBl, EtapaOportunidadBl>(Lifestyle.Scoped);
            container.Register<IFaseOportunidadDal, FaseOportunidadDal>(Lifestyle.Scoped);
            container.Register<IFaseOportunidadBl, FaseOportunidadBl>(Lifestyle.Scoped);
            container.Register<IActividadSegmentoNegocioDal, ActividadSegmentoNegocioDal>(Lifestyle.Scoped);

            container.Register<IRecursoOportunidadDal, RecursoOportunidadDal>(Lifestyle.Scoped);
            container.Register<IRecursoOportunidadBl, RecursoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IObservacionOportunidadDal, ObservacionOportunidadDal>(Lifestyle.Scoped);
            container.Register<IObservacionOportunidadBl, ObservacionOportunidadBl>(Lifestyle.Scoped);
            container.Register<IRecursoEquipoTrabajoDal, RecursoEquipoTrabajoDal>(Lifestyle.Scoped);
            container.Register<IRecursoEquipoTrabajoBl, RecursoEquipoTrabajoBl>(Lifestyle.Scoped);
            container.Register<IOportunidadSTDal, OportunidadSTDal>(Lifestyle.Scoped);
            container.Register<IOportunidadSTBl, OportunidadSTBl>(Lifestyle.Scoped);
            container.Register<ICasoOportunidadDal, CasoOportunidadDal>(Lifestyle.Scoped);
            container.Register<ICasoOportunidadBl, CasoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IDatosPreventaMovilDal, DatosPreventaMovilDal>(Lifestyle.Scoped);
            container.Register<IDatosPreventaMovilBl, DatosPreventaMovilBl>(Lifestyle.Scoped);
            container.Register<IEstadoCasoDal, EstadoCasoDal>(Lifestyle.Scoped);
            container.Register<IEstadoCasoBl, EstadoCasoBl>(Lifestyle.Scoped);
            container.Register<ITipoOportunidadDal, TipoOportunidadDal>(Lifestyle.Scoped);
            container.Register<ITipoOportunidadBl, TipoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IMotivoOportunidadDal, MotivoOportunidadDal>(Lifestyle.Scoped);
            container.Register<IMotivoOportunidadBl, MotivoOportunidadBl>(Lifestyle.Scoped);
            container.Register<IFuncionPropietarioDal, FuncionPropietarioDal>(Lifestyle.Scoped);
            container.Register<IFuncionPropietarioBl, FuncionPropietarioBl>(Lifestyle.Scoped);

            #endregion


            container.Register<IOpcionMenuBl, OpcionMenuBl>(Lifestyle.Scoped);
            container.Register<IPerfilBl, PerfilBl>(Lifestyle.Scoped);
            container.Register<IUsuarioBl, UsuarioBl>(Lifestyle.Scoped);



            #region comun 
            container.Register<IMedioDal, MedioDal>(Lifestyle.Scoped);
            container.Register<ISubServicioDal, SubServicioDal>(Lifestyle.Scoped);
            container.Register<IServicioDal, ServicioDal>(Lifestyle.Scoped);
            container.Register<IServicioSubServicioDal, ServicioSubServicioDal>(Lifestyle.Scoped);
            container.Register<IArchivoDal, ArchivoDal>(Lifestyle.Scoped);
            container.Register<IClienteDal, ClienteDal>(Lifestyle.Scoped);
            container.Register<IBWDal, BWDal>(Lifestyle.Scoped);
            container.Register<ILineaNegocioDal, LineaNegocioDal>(Lifestyle.Scoped);
            container.Register<IDepreciacionDal, DepreciacionDal>(Lifestyle.Scoped);
            container.Register<IDGDal, DGDal>(Lifestyle.Scoped);
            container.Register<IDireccionComercialDal, DireccionComercialDal>(Lifestyle.Scoped);
            container.Register<IFentoceldaDal, FentoceldaDal>(Lifestyle.Scoped);
            container.Register<ILineaProductoDal, LineaProductoDal>(Lifestyle.Scoped);
            container.Register<IMaestraDal, MaestraDal>(Lifestyle.Scoped);
            container.Register<IProveedorDal, ProveedorDal>(Lifestyle.Scoped);
            container.Register<ISectorDal, SectorDal>(Lifestyle.Scoped);
            container.Register<IServicioCMIDal, ServicioCMIDal>(Lifestyle.Scoped);

            container.Register<ICasoNegocioDal, CasoNegocioDal>(Lifestyle.Scoped);
            container.Register<ICasoNegocioServicioDal, CasoNegocioServicioDal>(Lifestyle.Scoped);
            #endregion

            #region comun BL
            container.Register<IMedioBl, MedioBl>(Lifestyle.Scoped);
            container.Register<IServicioBl, ServicioBl>(Lifestyle.Scoped);

            container.Register<ISubServicioBl, SubServicioBl>(Lifestyle.Scoped);
            container.Register<IServicioSubServicioBl, ServicioSubServicioBl>(Lifestyle.Scoped);

            container.Register<IClienteBl, ClienteBl>(Lifestyle.Scoped);
            container.Register<IArchivoBl, ArchivoBl>(Lifestyle.Scoped);
            container.Register<IBWBl, BWBl>(Lifestyle.Scoped);
            container.Register<ILineaNegocioBl, LineaNegocioBl>(Lifestyle.Scoped);
            container.Register<IDepreciacionBl, DepreciacionBl>(Lifestyle.Scoped);
            container.Register<IDGBl, DGBl>(Lifestyle.Scoped);
            container.Register<IDireccionComercialBl, DireccionComercialBl>(Lifestyle.Scoped);
            container.Register<IFentoceldaBl, FentoceldaBl>(Lifestyle.Scoped);
            container.Register<ILineaProductoBl, LineaProductoBl>(Lifestyle.Scoped);
            container.Register<IMaestraBl, MaestraBl>(Lifestyle.Scoped);
            container.Register<IProveedorBl, ProveedorBl>(Lifestyle.Scoped);
            container.Register<ISectorBl, SectorBl>(Lifestyle.Scoped);
            container.Register<IServicioCMIBl, ServicioCMIBl>(Lifestyle.Scoped);

            container.Register<ICasoNegocioBl, CasoNegocioBl>(Lifestyle.Scoped);
            container.Register<ICasoNegocioServicioBl, CasoNegocioServicioBl>(Lifestyle.Scoped);
            #endregion


            #region Funnel BL

            container.Register<IOportunidadFinancieraOrigenBl, OportunidadFinancieraOrigenBl>(Lifestyle.Scoped);

            #endregion Funnel BL


            #region Funnel DA

            container.Register<IOportunidadFinancieraOrigenDal, OportunidadFinancieraOrigenDal>(Lifestyle.Scoped);

            #endregion Funnel DA



            container.Verify();

            SimpleInjectorServiceHostFactory.SetContainer(container);


            var automapperTypeAdapterFactory = container.GetInstance<ITypeAdapterFactory>();
            TypeAdapterFactory.SetCurrent(automapperTypeAdapterFactory);

            //var dataAnnotationEntityValidatorFactory = container.GetInstance<IEntityValidatorFactory>();
            //EntityValidatorFactory.SetCurrent(dataAnnotationEntityValidatorFactory);

            var nlogLoggingFactory = container.GetInstance<ILoggerFactory>();
            LoggerFactory.SetCurrent(nlogLoggingFactory);

        }
    }
}
