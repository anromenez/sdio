﻿using System;
using Tgs.SDIO.AL.Host.Configuration;
using Tgs.SDIO.AL.Host.Utils;
using Tgs.SDIO.Util.Error.Logging.NLog;

namespace Tgs.SDIO.AL.Host
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            InjectorConfiguration.ConfigurationContainer();
            var errorBehavior = ConfigurationHelper.ObtenerConfiguracion(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            if (errorBehavior == null) throw new Exception("No se configuró adecuadamente al log de errores");
            InitializeLog.Initialize(errorBehavior.Nombre, errorBehavior.Ruta);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}