﻿using System.Data;
using Tgs.SDIO.AL.ProxyServicio.WsRaisAccesoSistema;
using Tgs.SDIO.AL.ProxyServicio.WsRaisUsuario;
using Tgs.SDIO.DataContracts.Dto.Request.Seguridad;
using Tgs.SDIO.Util.Constantes;

namespace Tgs.SDIO.AL.AgenteServicio
{
    public static class AgenteServicioUsuario
    {
        public static ProxyServicio.WsRaisAccesoSistema.BEUsuario ObtenerUsuarioPorLogin(string login)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ObtenerUsuario(login);
            }
        }

        public static ProxyServicio.WsRaisUsuario.BEUsuario ObtenerUsuarioPorIdUsuario(int idUsuario)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.ObtenerUsuarioPorIdUsuario(idUsuario);
            }
        }

        public static DataSet ValidarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ValidarInicioSesionSecundario(
                    Configuracion.CodigoAplicacion,
                    usuarioDtoRequest.Login,
                    usuarioDtoRequest.Password,
                    usuarioDtoRequest.NroIntentos, 
                    Configuracion.CodigoEmpresa);
            }
        } 

        public static string EnvioClaveUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.EnvioClaveUsuario(usuarioDtoRequest.Login,
                    usuarioDtoRequest.CorreoElectronico,
                    usuarioDtoRequest.UsuarioEnvia);
            }
        }

        public static BEEntidad[] ObtenerAmbitoUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.ObtenerAmbitoUsuario(usuarioDtoRequest.Login, usuarioDtoRequest.CodigoSistema);
            }
        }

        public static string RegistrarUsuario(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                var usuarioProxy = new ProxyServicio.WsRaisUsuario.BEUsuario
                {
                    Login = usuarioDtoRequest.Login,
                    Password = usuarioDtoRequest.Password,
                    Apellidos_Usuario = usuarioDtoRequest.Apellidos,
                    Nombre_Usuario = usuarioDtoRequest.Nombres,
                    CIP_Usuario = usuarioDtoRequest.CodigoCip,
                    Tipo_Usuario = usuarioDtoRequest.TipoUsuario,
                    IdEmpresa = usuarioDtoRequest.IdEmpresa,
                    FH_Registro = usuarioDtoRequest.FechaRegistro,
                    IdEstadoRegistro = usuarioDtoRequest.IdEstadoRegistro,
                    IdUsuario_Registro = usuarioDtoRequest.IdUsuarioRegistro,
                    Correo_Electronico = usuarioDtoRequest.CorreoElectronico,
                    Fecha_Caducidad = usuarioDtoRequest.FechaCaducidad,
                    Numero_Intentos_Fallidos = usuarioDtoRequest.IntentosFallidos
                };

                var perfilProxy = new BEUsuarioPerfil
                {
                    IdPerfil = usuarioDtoRequest.PerfilDtoRequest.IdPerfil,
                    FechaRegistro = usuarioDtoRequest.PerfilDtoRequest.FechaRegistro,
                    IdUsuarioRegistro = usuarioDtoRequest.PerfilDtoRequest.IdUsuarioRegistro,
                    IdEstadoRegistro = usuarioDtoRequest.PerfilDtoRequest.IdEstadoRegistro
                };

                var usuarioSistemaEmpresaProxy = new BEUsuarioSistemaEmpresa
                {
                    SistemaEmpresa = new BESistemaEmpresa {
                        IdEmpresa = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.IdEmpresa,
                        IdSistema = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.IdSistema
                    },
                    IdTurno = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.IdTurno,
                    FechaInicio = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaInicio,
                    FechaFin = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaFin,
                    EstadoRegistro = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.EstadoRegistro,
                    IdUsuarioRegistra = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.IdUsuarioRegistra,
                    FechaRegistro = usuarioDtoRequest.UsuarioSistemaEmpresaDtoRequest.FechaRegistro
                };

                return proxy.RegistrarUsuario(usuarioProxy, perfilProxy, usuarioSistemaEmpresaProxy);
            }
        }

        public static DataSet CambiarPassword(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                return proxy.CambiarPassword(usuarioDtoRequest.Login, usuarioDtoRequest.Password, usuarioDtoRequest.NuevoPassword);
            }
        }
        public static DataSet ObtenerUsuariosPorPerfil(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_AccesoSoapClient())
            {
                return proxy.ListarUsuarios_X_Perfil(usuarioDtoRequest.CodigoSistema, usuarioDtoRequest.CodigoPerfil, usuarioDtoRequest.IdEmpresa);
            }

        }

        public static ProxyServicio.WsRaisUsuario.BEUsuario[] ListarUsuarios(UsuarioDtoRequest usuarioDtoRequest)
        {
            using (var proxy = new RAIS_UsuarioSoapClient())
            {
                //ultimos parametros en duro corresponden a loginwindows e id sistema
                return proxy.ListarUsuarios(usuarioDtoRequest.Apellidos, usuarioDtoRequest.Nombres, usuarioDtoRequest.Login, "", 1);
            }
        }

    }
}
