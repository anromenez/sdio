﻿using Tgs.SDIO.Entities.Entities.Base;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadTipoCambio : Auditoria
    {        
        [Key]
        public int IdTipoCambioOportunidad { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
    }
}
