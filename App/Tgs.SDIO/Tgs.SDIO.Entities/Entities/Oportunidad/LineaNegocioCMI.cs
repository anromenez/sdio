using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public  class LineaNegocioCMI: Auditoria
    {
     
        [Key]
        public int IdLineaNegocio { get; set; }
        public int IdServicioCMI { get; set; }

    }
}
