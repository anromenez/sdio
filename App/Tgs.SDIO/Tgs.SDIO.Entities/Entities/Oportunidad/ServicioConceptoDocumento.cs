
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ServicioConceptoDocumento : Auditoria
    {
        [Key]
        public int IdProyecto { get; set; }
        public int IdLineaProducto { get; set; }
        public int IdDocumento { get; set; }
        public int TipoDocumento { get; set; }
        public string RutaDocumento { get; set; }
        public string Descripcion { get; set; }
        public int IdTipoDocumento { get; set; }

    }

}
