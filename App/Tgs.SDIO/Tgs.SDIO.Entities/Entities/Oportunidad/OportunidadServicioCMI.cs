﻿using Tgs.SDIO.Entities.Entities.Base;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class OportunidadServicioCMI : Auditoria
    {        
        [Key]
        public int IdServicioCMI { get; set; }
        public int IdOportunidadLineaNegocio { get; set; }
        public decimal Porcentaje { get; set; }
        public System.Nullable<int> IdAnalista { get; set; }
    }
}
