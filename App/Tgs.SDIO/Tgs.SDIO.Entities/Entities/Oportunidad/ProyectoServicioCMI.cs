using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ProyectoServicioCMI : Auditoria
    {  
        
        [Key]
        public int IdServicioCMI { get; set; }
        public int IdProyecto { get; set; }
        public int IdLineaProducto { get; set; }
        public decimal Porcentaje { get; set; }
        public int IdAnalista { get; set; }

    }

}
