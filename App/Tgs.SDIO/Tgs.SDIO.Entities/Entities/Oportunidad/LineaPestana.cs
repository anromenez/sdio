using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class LineaPestana : Auditoria
    {

        [Key]
        public int IdPestana { get; set; }
        public int IdLineaNegocio { get; set; }       
        public string Descripcion { get; set; }
    }
}
