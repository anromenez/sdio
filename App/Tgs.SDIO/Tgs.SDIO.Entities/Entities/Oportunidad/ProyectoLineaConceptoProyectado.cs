using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ProyectoLineaConceptoProyectado : Auditoria
    {
            [Key]
            public int IdProyecto { get; set; }
            public int IdLineaProducto { get; set; }
            public int IdConcepto { get; set; }
            public int IdProyectoConcepto { get; set; }
            public int IdConceptoProyectado { get; set; }
            public int Anio { get; set; }
            public int Mes { get; set; }
            public decimal Monto { get; set; }
            public string TipoFicha { get; set; }
        }
    }