using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class LineaConceptoCMI : Auditoria
    {
 
        [Key]
        public int IdLineaProducto { get; set; }
        public int IdSubServicio { get; set; }
        public int IdServicioCMI { get; set; }
        public decimal Costo { get; set; }
        public int IdPestana { get; set; }

    }
}
