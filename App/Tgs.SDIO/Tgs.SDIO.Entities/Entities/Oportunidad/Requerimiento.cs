﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class Requerimiento : Auditoria
    {
        [Key]
        public int Id { set; get; }
        public string Nombre { set; get; }
        public string Descripcion { set; get; }

        #region Relacion
        public int IdOportunidad { set; get; }
        public Oportunidad Oportunidad { set; get; }
        #endregion
    }
}
