using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Oportunidad
{
    public class ProyectoLineaProducto : Auditoria
    {
        [Key]
        public int IdProyecto { get; set; }
        public int IdLineaProducto { get; set; }
    }

}
