﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class Modelo
    {
   
        [Key]
        public int IdModelo { get; set; }
        public string CodigoModelo { get; set; }
        public string Modelo1 { get; set; }
        public string Descripcion { get; set; }
        public decimal? Monto { get; set; }
        public int? FlagDefault { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }
        
    }
}
