﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Linea : Auditoria
    {
        [Key]
        public int IdLinea { get; set; }
        public string Descripcion { get; set; }

    }
}
