﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class LineaNegocio : Auditoria
    {
        [Key]
        public int IdLineaNegocio { get; set; }
        public string Descripcion { get; set; }

    }
}
