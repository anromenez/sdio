﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class VSAT
    {
        public string servicio { get; set; }
        public decimal? downspeed { get; set; }
        public string garantizado { get; set; }
        public decimal costo_principal { get; set; }
        public decimal costo_backup { get; set; }
    }
}
