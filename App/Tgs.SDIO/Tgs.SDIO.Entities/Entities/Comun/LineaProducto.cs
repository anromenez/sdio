﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class LineaProducto
    {

        [Key]

        public int IdLineaProducto { get; set; }
        public string Descripcion { get; set; }
        public string Plantilla { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public int? IdPlantilla { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }
    }
}
