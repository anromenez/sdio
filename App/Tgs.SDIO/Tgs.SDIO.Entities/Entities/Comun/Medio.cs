﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class Medio: Auditoria
    {
        [Key]
        public int IdMedio { get; set; }
        public string Descripcion { get; set; }
    }
}
