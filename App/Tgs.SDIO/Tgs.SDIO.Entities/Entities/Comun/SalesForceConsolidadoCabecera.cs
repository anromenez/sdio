﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class SalesForceConsolidadoCabecera
    {
        [Key]
        public int Id { get; set; }
        public string IdOportunidad { get; set; }
        public string NumeroDelCaso { get; set; }
        public string PropietarioOportunidad { get; set; }
        public string TipologiaOportunidad { get; set; }
        public string NombreCliente { get; set; }
        public string NombreOportunidad { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string ProbabilidadExito { get; set; }
        public string Etapa { get; set; }
        public string LoginRegistro { get; set; }
        public string LoginUltimaModificacion { get; set; }
        public string Descripcion { get; set; }
    }
}
