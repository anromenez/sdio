﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class Proveedor: Auditoria
    {
     
        [Key]
        public int IdProveedor { get; set; }
        public string Descripcion { get; set; }
        public int? TipoProveedor { get; set; }

    }
}
