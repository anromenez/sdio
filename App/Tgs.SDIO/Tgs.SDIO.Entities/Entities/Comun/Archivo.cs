﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Archivo
    {
        [Key]
        public int IdArchivo { get; set; }
        public int? IdTipoDocumento { get; set; }
        public int? IdRelacion1 { get; set; }
        public int? IdRelacion2 { get; set; }
        public string Descripcion { get; set; }
        public string NombreArchivo { get; set; }
        public string RutaArchivo { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }

    }
}
