﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class TipoCambio
    {   [Key]
        public int IdLineaProducto { get; set; }
        public int IdTipoCambio { get; set; }
        public int? IdMoneda { get; set; }
        public int IdTipificacion { get; set; }
        public int? Anio { get; set; }
        public decimal? Monto { get; set; }
        public int? IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }

    }
}
