﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public  class Concepto
    {
        [Key]
        public int IdConcepto { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionEquivalencia { get; set; }
        public int? IdTipoConcepto { get; set; }
        public int? Orden { get; set; }
        public int? Negrita { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int? IdUsuarioEdicion { get; set; }
        public DateTime?FechaEdicion { get; set; }

    }
}
