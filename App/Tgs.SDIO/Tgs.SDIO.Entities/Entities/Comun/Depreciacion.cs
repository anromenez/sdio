﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Comun
{
    public class Depreciacion : Auditoria
    {
 
        [Key]
        public int IdDepreciacion { get; set; }
        public string Descripcion { get; set; }
        public string Detalle { get; set; }
        public decimal? PorcentajeAnual { get; set; }
        public int? Meses { get; set; }


    }
}
