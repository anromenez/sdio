﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{ 
   public class CartaFianzaMaestro
    {
        [Key]
        public int IdCartaFianza { set; get; }
        public int IdCliente { set; get; }
        public string NumeroOportunidad { set; get; }
        public int IdTipoContratoTm { set; get; }
        public string NumeroContrato { set; get; }
        public string Processo { set; get; }
        public string DescripcionServicioCartaFianza { set; get; }
        
        public DateTime FechaFirmaServicioContrato { set; get; }
        public DateTime FechaFinServicioContrato { set; get; }
        public int IdEmpresaAdjudicadaTm { set; get; }
        public int IdTipoGarantiaTm { set; get; }
        public string NumeroGarantia { set; get; }
       //IdTipoCambio int,
        public int IdTipoMonedaTm { set; get; }
        public decimal ImporteCartaFianzaSoles { set; get; }
        public decimal ImporteCartaFianzaDolares { set; get; }
        public int IdBanco  { set; get; }

        /*
    IdTipoAccionTm	int,
    IdEstadoVencimientoTm	int,
    IdEstadoCartaFianzaTm	int,
    IdRenovarTm	int,
    IdEstadoRecuperacionCartaFianzaTm int,
    */
        public int ClienteEspecial { set; get; }
        public int SeguimientoImportante { set; get; }
        public string Observacion { set; get; }
        public string Incidencia { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public Nullable<DateTime> FechaEdicion { set; get; }


    }

}
