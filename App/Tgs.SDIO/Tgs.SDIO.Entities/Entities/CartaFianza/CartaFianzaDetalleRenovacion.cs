﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tgs.SDIO.Entities.Entities.CartaFianza
{

  
    public class CartaFianzaDetalleRenovacion
    {
        [Key]
        public int IdCartaFianzaDetalleSeguimiento { set; get; }
        public int IdCartaFianza { set; get; }
        public int IdColaborador { set; get; }
        public int IdColaboradorACargo { set; get; }
        public int IdRenovarTm { set; get; }
        public int PeriodoMesRenovacion { set; get; }
        public DateTime FechaVencimientoRenovacion { set; get; }
        public string SustentoRenovacion { set; get; }
        public string Observacion { set; get; }
        public int IdEstado { set; get; }
        public int IdUsuarioCreacion { set; get; }
        public DateTime FechaCreacion { set; get; }
        public int IdUsuarioEdicion { set; get; }
        public DateTime FechaEdicion { set; get; }

}
}
