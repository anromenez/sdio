﻿using System;

namespace Tgs.SDIO.Entities.Entities.Base
{ 
    public interface IAuditoria
    {
         int? IdEstado { get; set; }

         int IdUsuarioCreacion { get; set; }

         DateTime FechaCreacion { get; set; }

         int? IdUsuarioEdicion { get; set; }

         DateTime? FechaEdicion { get; set; }
    }
}
