﻿using System;

namespace Tgs.SDIO.Entities.Entities.Base
{
    public abstract class Auditoria : IAuditoria
    { 
        public int? IdEstado { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public int? IdUsuarioEdicion { get; set; }

        public DateTime? FechaEdicion { get; set; }

    }
}
