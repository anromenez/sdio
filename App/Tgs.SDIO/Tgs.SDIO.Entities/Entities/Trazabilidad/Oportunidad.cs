namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class OportunidadTs : Auditoria
    {

        [Key]
        public int IdOportunidad { get; set; }

        public int? IdOportunidadPadre { get; set; }

        public string IdOportunidadSF { get; set; }

        public string IdOportunidadAux { get; set; }

        public string Descripcion { get; set; }

        public int? IdTipoOportunidad { get; set; }

        public int? IdMotivoOportunidad { get; set; }

        public int? IdTipoEntidadCliente { get; set; }

        public int? IdCliente { get; set; }

        public int? IdSegmentoNegocio { get; set; }

        public int? Prioridad { get; set; }

        public int? ProbalidadExito { get; set; }

        public int? PorcentajeCierre { get; set; }

        public int? PorcentajeCheckList { get; set; }

        public int? IdFase { get; set; }

        public int? IdEtapa { get; set; }

        public bool? FlgCapexMayor { get; set; }

        public decimal? ImporteCapex { get; set; }

        public DateTime? FechaApertura { get; set; }

        public DateTime? FechaCierre { get; set; }
    }
}
