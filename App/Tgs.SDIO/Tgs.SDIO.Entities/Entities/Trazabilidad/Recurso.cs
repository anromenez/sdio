namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class Recurso : Auditoria
    {
        [Key]
        public int IdRecurso { get; set; }

        public int? TipoRecurso { get; set; }

        public string UserNameSF { get; set; }

        public string Nombre { get; set; }

        public string DNI { get; set; }

        public string Email { get; set; }

        public int? IdRolDefecto { get; set; }

        public int? IdOrigen { get; set; }

        public int? IdEmpresa { get; set; }
    }
}
