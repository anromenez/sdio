﻿
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
   public  class EquipoTrabajo : Auditoria
    {
        [Key]
        public int IdEquipoTrabajo { get; set; }

        public int? IdEquipoTrabajoPadre { get; set; }

        public string Descripcion { get; set; }

        public int? Nivel { get; set; }

        public int? OrdenVisual { get; set; }
    }
}
