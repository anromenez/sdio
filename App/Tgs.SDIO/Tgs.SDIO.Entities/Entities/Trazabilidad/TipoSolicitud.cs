﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class TipoSolicitud : Auditoria
    {
        [Key]
        public int IdTipoSolicitud { get; set; }

        public string Descripcion { get; set; }

        public int OrdenVisual { get; set; }
    }
}
