﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class FuncionPropietario : Auditoria
    {
        [Key]
        public int IdFuncionPropietario { get; set; }

        [StringLength(250)]
        public string Descripcion { get; set; }

        public int? IdSector { get; set; }
    }
}
