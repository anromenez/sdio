namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class SeguimientoActividad : Auditoria
    {
        [Key]
        public int IdSeguimiento { get; set; }

        public int IdOportunidad { get; set; }

        public int? IdCaso { get; set; }

        public DateTime? FechaSeguimiento { get; set; }

        public DateTime? FechaFinSeguimiento { get; set; }

        public int? IdAreaSeguimiento { get; set; }

        public int? IdActividad { get; set; }

        public int? IdEstadoEjecucion { get; set; }

        public int? IdEstadoCumplimiento { get; set; }

        public bool? FlgObservaciones { get; set; }

        public bool? FlgFilesAdjuntos { get; set; }

    }
}
