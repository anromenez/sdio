namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;

    public class MotivoOportunidad : Auditoria
    {
        [Key]
        public int IdMotivoOportunidad { get; set; }

        public string Descripcion { get; set; }

    }
}
