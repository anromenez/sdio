﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class RecursoEquipoTrabajo : Auditoria
    {
        [Key]
        public int IdRecursoEquipoTrabajo { get; set; }

        public int IdRecurso { get; set; }

        public int? IdEquipoTrabajoNivel1 { get; set; }

        public int? IdEquipoTrabajoNivel2 { get; set; }

        public int? IdEquipoTrabajoNivel3 { get; set; }

        public int? IdEquipoTrabajoNivel4 { get; set; }

        public int? IdEquipoTrabajoNivel5 { get; set; }

        public DateTime? FInicioAsignacion { get; set; }

        public DateTime? FFinAsignacion { get; set; }

        public string Observaciones { get; set; }

    }
}
