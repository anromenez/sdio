namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Tgs.SDIO.Entities.Entities.Base;

    public class RecursoOportunidad : Auditoria
    {
        
        public int IdOportunidad { get; set; }

        [Key]
        public int IdAsignacion { get; set; }

        public int IdRecurso { get; set; }

        public int? IdCaso { get; set; }

        public int? IdRol { get; set; }

        public int? PorcentajeDedicacion { get; set; }

        public string Observaciones { get; set; }

        public DateTime? FInicioAsignacion { get; set; }

        public DateTime? FFinAsignacion { get; set; }

    }
}
