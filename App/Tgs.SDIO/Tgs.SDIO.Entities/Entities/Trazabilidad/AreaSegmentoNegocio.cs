﻿using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class AreaSegmentoNegocio : Auditoria
    {
        [Key]
        public int IdAreaSegmentoNegocio { get; set; }

        public int IdAreaSeguimiento { get; set; }

        public int IdSegmentoNegocio { get; set; }

    }
}
