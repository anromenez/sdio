﻿using System;
using System.ComponentModel.DataAnnotations;
using Tgs.SDIO.Entities.Entities.Base;

namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    public class Feriado : Auditoria
    {
        [Key]
        public int IdFeriado { get; set; }

        [StringLength(200)]
        public string Descripcion { get; set; }

        public DateTime? FechaInicio { get; set; }

        public DateTime? FechaFin { get; set; }
    }
}
