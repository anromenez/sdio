namespace Tgs.SDIO.Entities.Entities.Trazabilidad
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Tgs.SDIO.Entities.Entities.Base;
    public class ObservacionOportunidad : Auditoria
    {
        [Key]
        public int IdObservacion { get; set; }

        public int IdOportunidad { get; set; }

        public int? IdCaso { get; set; }

        public int? IdSeguimiento { get; set; }

        public DateTime? FechaSeguimiento { get; set; }

        public int? IdConcepto { get; set; }

        public string Observaciones { get; set; }

    }
}
