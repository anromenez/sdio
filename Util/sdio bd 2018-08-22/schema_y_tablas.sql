USE [DIO]
GO
/****** Object:  Schema [CAPEX]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [CAPEX]
GO
/****** Object:  Schema [CARTAFIANZA]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [CARTAFIANZA]
GO
/****** Object:  Schema [COMPRAS]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [COMPRAS]
GO
/****** Object:  Schema [COMUN]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [COMUN]
GO
/****** Object:  Schema [ECONOMICA]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [ECONOMICA]
GO
/****** Object:  Schema [FUNNEL]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [FUNNEL]
GO
/****** Object:  Schema [OPORTUNIDAD]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [OPORTUNIDAD]
GO
/****** Object:  Schema [PLANTAEXTERNA]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [PLANTAEXTERNA]
GO
/****** Object:  Schema [PROYECTO]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [PROYECTO]
GO
/****** Object:  Schema [RAIS]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [RAIS]
GO
/****** Object:  Schema [TRAZABILIDAD]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [TRAZABILIDAD]
GO
/****** Object:  Schema [TRAZAPREVENTA]    Script Date: 8/22/2018 9:04:41 PM ******/
CREATE SCHEMA [TRAZAPREVENTA]
GO
/****** Object:  Table [CAPEX].[accion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[accion](
	[iacc_id] [int] IDENTITY(1,1) NOT NULL,
	[vacc_nom] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iacc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[accion_estrategica]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[accion_estrategica](
	[iaes_id] [int] NOT NULL,
	[vaes_nom] [varchar](255) NOT NULL,
	[caes_est] [char](1) NOT NULL,
	[ilin_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[iaes_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[auditoria]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CAPEX].[auditoria](
	[iaud_id] [int] IDENTITY(1,1) NOT NULL,
	[daud_fyh] [datetime] NOT NULL,
	[iusu_id] [int] NOT NULL,
	[iacc_id] [int] NOT NULL,
	[isca_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iaud_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [CAPEX].[boton]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[boton](
	[ibot_id] [int] NOT NULL,
	[vbot_nom] [varchar](255) NOT NULL,
	[ibot_ord] [int] NOT NULL,
	[ibot_pad] [int] NULL,
	[vbot_rut] [varchar](255) NULL,
	[vbot_ico] [varchar](255) NOT NULL,
	[cbot_vis] [varchar](255) NULL,
	[vbot_con] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ibot_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[boton_perfil]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CAPEX].[boton_perfil](
	[ibot_id] [int] NOT NULL,
	[iprf_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ibot_id] ASC,
	[iprf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [CAPEX].[concepto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[concepto](
	[icon_id] [int] IDENTITY(1,1) NOT NULL,
	[vcon_nom] [varchar](255) NOT NULL,
	[ccon_est] [char](1) NOT NULL,
	[idae_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[icon_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[descripcion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[descripcion](
	[ides_id] [int] IDENTITY(1,1) NOT NULL,
	[vdes_nom] [varchar](255) NOT NULL,
	[cdes_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ides_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[descripcion_accion_estrategica]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[descripcion_accion_estrategica](
	[idae_id] [int] IDENTITY(1,1) NOT NULL,
	[vdae_nom] [varchar](255) NOT NULL,
	[cdae_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idae_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[detalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[detalle](
	[idet_id] [int] IDENTITY(1,1) NOT NULL,
	[isca_id] [int] NOT NULL,
	[ipro_id] [int] NOT NULL,
	[vdet_pve] [varchar](255) NULL,
	[vdet_mod] [varchar](255) NULL,
	[vdet_cer] [varchar](255) NULL,
	[idet_can] [int] NOT NULL,
	[ndet_pus] [decimal](15, 2) NOT NULL,
	[ndet_ptd] [decimal](15, 2) NOT NULL,
	[ndet_pts] [decimal](15, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idet_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[estado]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[estado](
	[iest_id] [int] NOT NULL,
	[vest_nom] [varchar](255) NOT NULL,
	[vest_cls] [varchar](255) NOT NULL,
	[cest_est] [char](1) NOT NULL,
 CONSTRAINT [PK__estado__E4208B058A97DA0A] PRIMARY KEY CLUSTERED 
(
	[iest_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[etapa]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[etapa](
	[ieta_id] [int] IDENTITY(1,1) NOT NULL,
	[veta_nom] [varchar](255) NOT NULL,
	[ceta_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ieta_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[linea_negocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[linea_negocio](
	[ilin_id] [int] IDENTITY(1,1) NOT NULL,
	[vlin_nom] [varchar](255) NOT NULL,
	[clin_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ilin_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[mes]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[mes](
	[imes_id] [int] NOT NULL,
	[vmes_nom] [varchar](255) NOT NULL,
	[cmes_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[imes_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[observacion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CAPEX].[observacion](
	[iobs_id] [int] IDENTITY(1,1) NOT NULL,
	[tobs_des] [text] NOT NULL,
	[dobs_fyh] [datetime] NOT NULL,
	[iusu_id] [int] NOT NULL,
	[isca_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iobs_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [CAPEX].[perfil]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[perfil](
	[iprf_id] [int] NOT NULL,
	[vprf_nom] [varchar](255) NOT NULL,
	[vprf_cls] [varchar](255) NOT NULL,
	[cprf_est] [char](1) NULL,
 CONSTRAINT [PK__perfil__DFA893C3AA918EB9] PRIMARY KEY CLUSTERED 
(
	[iprf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[producto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[producto](
	[ipro_id] [int] IDENTITY(1,1) NOT NULL,
	[vpro_nom] [varchar](255) NOT NULL,
	[cpro_est] [char](1) NOT NULL,
	[ides_id] [int] NOT NULL,
	[iaes_id] [int] NULL,
	[iae2_id] [int] NULL,
	[icon_id] [int] NULL,
	[cpro_sto] [char](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ipro_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[solicitud_capex]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[solicitud_capex](
	[isca_id] [int] IDENTITY(1,1) NOT NULL,
	[dsca_fec] [date] NOT NULL,
	[copo_cod] [char](13) NULL,
	[iest_id] [int] NOT NULL,
	[iusu_id] [int] NOT NULL,
	[iten_id] [int] NULL,
	[vsca_lin] [varchar](255) NULL,
	[tsca_dgp] [text] NULL,
	[tsca_rso] [text] NULL,
	[tsca_oso] [text] NULL,
	[nsca_if2] [decimal](15, 2) NULL,
	[tsca_scl] [text] NULL,
	[tsca_scn] [text] NULL,
	[nsca_van] [decimal](18, 2) NULL,
	[isca_pay] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[isca_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[sustento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[sustento](
	[isus_id] [int] IDENTITY(1,1) NOT NULL,
	[vsus_nom] [varchar](255) NOT NULL,
	[vsus_arc] [varchar](255) NOT NULL,
	[dsus_fyh] [datetime] NOT NULL,
	[iusu_id] [int] NOT NULL,
	[isca_id] [int] NOT NULL,
	[itsu_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[isus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[tarea]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[tarea](
	[itar_id] [int] IDENTITY(1,1) NOT NULL,
	[vtar_nom] [varchar](255) NOT NULL,
	[ctar_est] [char](1) NOT NULL,
	[isca_id] [int] NOT NULL,
	[itar_tar] [int] NULL,
	[vtar_ord] [varchar](11) NOT NULL,
	[dtar_fei] [date] NULL,
	[dtar_fef] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[itar_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[tipo_documento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[tipo_documento](
	[itdo_id] [int] IDENTITY(1,1) NOT NULL,
	[vtdo_nom] [varchar](255) NOT NULL,
	[itdo_num] [int] NOT NULL,
	[ctdo_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[itdo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[tipo_entidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[tipo_entidad](
	[iten_id] [int] IDENTITY(1,1) NOT NULL,
	[vten_nom] [varchar](255) NOT NULL,
	[cten_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iten_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[tipo_proyecto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[tipo_proyecto](
	[itpr_id] [int] IDENTITY(1,1) NOT NULL,
	[vtpr_nom] [varchar](255) NOT NULL,
	[ctpr_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[itpr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[tipo_sustento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[tipo_sustento](
	[itsu_id] [int] IDENTITY(1,1) NOT NULL,
	[vtsu_nom] [varchar](255) NOT NULL,
	[ctsu_est] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[itsu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CAPEX].[usuario]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CAPEX].[usuario](
	[iusu_id] [int] NOT NULL,
	[dusu_fec] [datetime] NOT NULL,
	[iusu_usc] [int] NOT NULL,
	[dusu_fem] [datetime] NULL,
	[iusu_usm] [int] NULL,
	[cusu_est] [char](1) NOT NULL,
	[vusu_usu] [varchar](255) NOT NULL,
	[vusu_cla] [varchar](255) NOT NULL,
	[vusu_nom] [varchar](255) NOT NULL,
	[vusu_ape] [varchar](255) NOT NULL,
	[vusu_tel] [varchar](255) NULL,
	[vusu_ema] [varchar](255) NOT NULL,
	[iprf_id] [int] NOT NULL,
	[dusu_fua] [datetime] NULL,
 CONSTRAINT [PK__usuario__DE8029875ECB0324] PRIMARY KEY CLUSTERED 
(
	[iusu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [usu_ema] UNIQUE NONCLUSTERED 
(
	[vusu_ema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [usu_usu] UNIQUE NONCLUSTERED 
(
	[vusu_usu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianza]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianza](
	[IdCartaFianza] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NULL,
	[NumeroOportunidad] [varchar](50) NULL,
	[IdTipoContratoTm] [int] NULL,
	[NumeroContrato] [varchar](50) NULL,
	[Processo] [varchar](150) NULL,
	[DescripcionServicioCartaFianza] [varchar](4000) NULL,
	[FechaFirmaServicioContrato] [datetime] NULL,
	[FechaFinServicioContrato] [datetime] NULL,
	[IdEmpresaAdjudicadaTm] [int] NULL,
	[IdTipoGarantiaTm] [int] NULL,
	[NumeroGarantia] [varchar](25) NULL,
	[IdTipoMonedaTm] [int] NULL,
	[ImporteCartaFianzaSoles] [decimal](18, 2) NULL,
	[ImporteCartaFianzaDolares] [decimal](18, 2) NULL,
	[IdBanco] [int] NULL,
	[IdTipoAccionTm] [int] NULL,
	[IdEstadoVencimientoTm] [int] NULL,
	[IdEstadoCartaFianzaTm] [int] NULL,
	[IdRenovarTm] [int] NULL,
	[IdEstadoRecuperacionCartaFianzaTm] [int] NULL,
	[ClienteEspecial] [int] NULL,
	[SeguimientoImportante] [int] NULL,
	[Observacion] [varchar](1000) NULL,
	[Incidencia] [varchar](500) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianzaColaboradorDetalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianzaColaboradorDetalle](
	[IdCartaFianzaColaboradorDetalle] [int] IDENTITY(1,1) NOT NULL,
	[IdCartaFianza] [int] NULL,
	[IdColaborador] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [varchar](25) NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [varchar](25) NULL,
	[FechaEdicion] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianzaDetalleAcciones]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianzaDetalleAcciones](
	[IdCartaFianzaDetalleSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdCartaFianza] [int] NULL,
	[IdColaboradorACargo] [int] NULL,
	[IdTipoAccionTm] [int] NULL,
	[IdEstadoVencimientoTm] [int] NULL,
	[Observacion] [varchar](1000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [varchar](25) NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [varchar](25) NULL,
	[FechaEdicion] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianzaDetalleEstado]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianzaDetalleEstado](
	[IdCartaFianzaDetalleSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdCartaFianza] [int] NULL,
	[IdColaboradorACargo] [int] NULL,
	[IdEstadoCartaFianzaTm] [int] NULL,
	[Observacion] [varchar](1000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianzaDetalleRecupero]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianzaDetalleRecupero](
	[IdCartaFianzaDetalleSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdCartaFianza] [int] NULL,
	[IdColaborador] [int] NULL,
	[IdColaboradorACargo] [int] NULL,
	[IdEstadoRecuperacionCartaFianzaTm] [int] NULL,
	[Comentario] [varchar](1000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [varchar](25) NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [varchar](25) NULL,
	[FechaEdicion] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianzaDetalleRenovacion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianzaDetalleRenovacion](
	[IdCartaFianzaDetalleSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdCartaFianza] [int] NULL,
	[IdColaborador] [int] NULL,
	[IdColaboradorACargo] [int] NULL,
	[IdRenovarTm] [int] NULL,
	[PeriodoMesRenovacion] [int] NULL,
	[FechaVencimientoRenovacion] [datetime] NULL,
	[SustentoRenovacion] [varchar](1000) NULL,
	[Observacion] [varchar](1000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CARTAFIANZA].[CartaFianzaDetalleSeguimiento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CARTAFIANZA].[CartaFianzaDetalleSeguimiento](
	[IdCartaFianzaDetalleSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdCartaFianza] [int] NULL,
	[IdColaborador] [int] NULL,
	[IdColaboradorACargo] [int] NULL,
	[Comentario] [varchar](1000) NULL,
	[IdUsuarioCreacion] [varchar](25) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioModificacion] [varchar](25) NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[Vigencia] [bit] NULL,
	[Eliminado] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Archivo]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Archivo](
	[Id] [int] NOT NULL,
	[IdTabla] [int] NOT NULL,
	[IdTablaRegistro] [int] NOT NULL,
	[IdTipo] [int] NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[Ruta] [varchar](100) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Archivos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Banco]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Banco](
	[IdBanco] [int] IDENTITY(1,1) NOT NULL,
	[DescripcionCorta] [varchar](50) NULL,
	[DescripcionLarga] [varchar](50) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [varchar](25) NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [varchar](25) NULL,
	[FechaEdicion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdBanco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[CasoNegocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[CasoNegocio](
	[IdLineaNegocio] [int] NULL,
	[IdCasoNegocio] [int] IDENTITY(2,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[FlagDefecto] [char](1) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_CasoNegocio] PRIMARY KEY CLUSTERED 
(
	[IdCasoNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[CasoNegocioServicio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [COMUN].[CasoNegocioServicio](
	[IdCasoNegocio] [int] NOT NULL,
	[IdServicio] [int] NOT NULL,
	[IdCasoNegocioServicio] [int] IDENTITY(1,1) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_CasoNegocioServicio_1] PRIMARY KEY CLUSTERED 
(
	[IdCasoNegocioServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [COMUN].[CentroCosto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[CentroCosto](
	[IdCentroCosto] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_CentroCosto] PRIMARY KEY CLUSTERED 
(
	[IdCentroCosto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Cliente]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Cliente](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[CodigoCliente] [varchar](20) NULL,
	[Descripcion] [varchar](250) NULL,
	[IdSector] [int] NULL,
	[GerenteComercial] [varchar](200) NULL,
	[IdDireccionComercial] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[IdTipoIdentificadorFiscalTm] [int] NULL,
	[NumeroIdentificadorFiscal] [varchar](25) NULL,
	[Email] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Cliente2]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Cliente2](
	[IdCliente] [int] NOT NULL,
	[CodigoCliente] [varchar](20) NULL,
	[Descripcion] [varchar](250) NULL,
	[IdSector] [int] NULL,
	[GerenteComercial] [varchar](200) NULL,
	[IdDireccionComercial] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[IdTipoIdentificadorFiscalTm] [int] NULL,
	[NumeroIdentificadorFiscal] [varchar](25) NULL,
	[Email] [varchar](250) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Colaboradores]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Colaboradores](
	[IdColaborador] [int] IDENTITY(1,1) NOT NULL,
	[IdentificadorColaborador] [varchar](25) NULL,
	[NombreCompleto] [varchar](200) NOT NULL,
	[Email] [varchar](250) NULL,
	[IdTipoColaboradorFianzaTm] [int] NULL,
	[IdEmailAlertasTm] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [varchar](25) NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [varchar](25) NULL,
	[FechaEdicion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdColaborador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Costo]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Costo](
	[IdCosto] [int] NOT NULL,
	[IdTipoCosto] [int] NULL,
	[Descripcion] [varchar](200) NULL,
	[Monto] [decimal](18, 4) NULL,
	[VelocidadSubidaKBPS] [decimal](18, 4) NULL,
	[PorcentajeGarantizado] [decimal](18, 4) NULL,
	[PorcentajeSobresuscripcion] [decimal](18, 4) NULL,
	[CostoSegmentoSatelital] [decimal](18, 4) NULL,
	[InvAntenaHubUSD] [decimal](18, 4) NULL,
	[AntenaCasaClienteUSD] [decimal](18, 4) NULL,
	[Instalacion] [decimal](18, 4) NULL,
	[IdUnidadConsumo] [int] NULL,
	[IdTipificacion] [int] NULL,
	[CodigoModelo] [varchar](20) NULL,
	[Modelo] [varchar](50) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Costo] PRIMARY KEY CLUSTERED 
(
	[IdCosto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Depreciacion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Depreciacion](
	[IdDepreciacion] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Detalle] [varchar](300) NULL,
	[PorcentajeAnual] [decimal](18, 2) NULL,
	[Meses] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Depreciacion] PRIMARY KEY CLUSTERED 
(
	[IdDepreciacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[DireccionComercial]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[DireccionComercial](
	[IdDireccion] [int] NOT NULL,
	[Descripcion] [varchar](200) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_DireccionComercial] PRIMARY KEY CLUSTERED 
(
	[IdDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[GerenciaProducto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[GerenciaProducto](
	[IdGerenciaProducto] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_GerenciaProducto] PRIMARY KEY CLUSTERED 
(
	[IdGerenciaProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Linea]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Linea](
	[IdLinea] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_LineaNegocio] PRIMARY KEY CLUSTERED 
(
	[IdLinea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[LineaNegocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[LineaNegocio](
	[IdLineaNegocio] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Plantilla] [varchar](100) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[IdPlantilla] [int] NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_LineasProductos] PRIMARY KEY CLUSTERED 
(
	[IdLineaNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Maestra]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Maestra](
	[IdMaestra] [int] NOT NULL,
	[IdRelacion] [int] NULL,
	[Descripcion] [varchar](200) NULL,
	[Valor] [varchar](10) NULL,
	[Comentario] [varchar](300) NULL,
	[Valor2] [varchar](20) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Maestra] PRIMARY KEY CLUSTERED 
(
	[IdMaestra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Medio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Medio](
	[IdMedio] [int] NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[DescripcionCorta] [varchar](10) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Medio] PRIMARY KEY CLUSTERED 
(
	[IdMedio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[MedioCosto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [COMUN].[MedioCosto](
	[IdMedio] [int] NOT NULL,
	[IdCosto] [int] NOT NULL,
	[IdMedioCosto] [int] IDENTITY(1,1) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_MedioCosto_1] PRIMARY KEY CLUSTERED 
(
	[IdMedioCosto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [COMUN].[Proveedor]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Proveedor](
	[IdProveedor] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[TipoProveedor] [int] NULL,
 CONSTRAINT [PK_Proveedores] PRIMARY KEY CLUSTERED 
(
	[IdProveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[SalesForceConsolidadoCabecera]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[SalesForceConsolidadoCabecera](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [nvarchar](255) NULL,
	[NumeroDelCaso] [nvarchar](255) NULL,
	[PropietarioOportunidad] [nvarchar](255) NULL,
	[TipologiaOportunidad] [nvarchar](255) NULL,
	[NombreCliente] [nvarchar](255) NULL,
	[NombreOportunidad] [nvarchar](255) NULL,
	[FechaCreacion] [datetime] NULL,
	[ProbabilidadExito] [nvarchar](255) NULL,
	[Etapa] [nvarchar](255) NULL,
	[FechaCierreEstimada] [datetime] NULL,
	[Asunto] [nvarchar](255) NULL,
	[FechaCreacionDB] [datetime] NULL,
	[FechaEdicion] [datetime] NULL,
	[LoginRegistro] [varchar](100) NULL,
	[LoginUltimaModificacion] [varchar](100) NULL,
	[IdTipoCapex] [int] NULL,
	[PorcentajeRealizado] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[IdUsuarioEdicion] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[SalesForceConsolidadoDetalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[SalesForceConsolidadoDetalle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [nvarchar](255) NULL,
	[PropietarioOportunidad] [nvarchar](255) NULL,
	[TipologiaOportunidad] [nvarchar](255) NULL,
	[NombreCliente] [nvarchar](255) NULL,
	[NombreOportunidad] [nvarchar](255) NULL,
	[FechaCreacion] [datetime] NULL,
	[FechaCierreEstimada] [datetime] NULL,
	[FechaCierreReal] [datetime] NULL,
	[ProbabilidadExito] [nvarchar](255) NULL,
	[Etapa] [nvarchar](255) NULL,
	[TipoOportunidad] [nvarchar](255) NULL,
	[PlazoEstimadoProvision] [int] NULL,
	[FechaEstimadaInstalacionServicio] [datetime] NULL,
	[DuracionContrato] [int] NULL,
	[IngresoPorUnicaVezDivisa] [nvarchar](255) NULL,
	[IngresoPorUnicaVez] [nvarchar](255) NULL,
	[FullContractValueNetoDivisa] [nvarchar](255) NULL,
	[FullContractValueNeto] [nvarchar](255) NULL,
	[RecurrenteBrutoMensualDivisa] [nvarchar](255) NULL,
	[RecurrenteBrutoMensual] [nvarchar](255) NULL,
	[NumeroDelCaso] [nvarchar](255) NULL,
	[Estado] [nvarchar](255) NULL,
	[Departamento] [nvarchar](255) NULL,
	[TipoSolicitud] [nvarchar](255) NULL,
	[Asunto] [nvarchar](255) NULL,
	[FechaCreacionDB] [datetime] NULL,
	[FechaEdicion] [datetime] NULL,
	[LoginRegistro] [varchar](100) NULL,
	[LoginUltimaModificacion] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[IdCliente] [int] NULL,
 CONSTRAINT [PK_SalesForceConsolidado] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[SalesForceHistoriaCarga]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [COMUN].[SalesForceHistoriaCarga](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FechaCarga] [datetime] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_SalesForceHistoriaCarga] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [COMUN].[SalesForceOrigen]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[SalesForceOrigen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [nvarchar](255) NULL,
	[PropietarioOportunidad] [nvarchar](255) NULL,
	[TipologiaOportunidad] [nvarchar](255) NULL,
	[NombreCliente] [nvarchar](255) NULL,
	[NombreOportunidad] [nvarchar](255) NULL,
	[FechaCreacion] [datetime] NULL,
	[FechaCierreEstimada] [datetime] NULL,
	[FechaCierreReal] [datetime] NULL,
	[ProbabilidadExito] [nvarchar](255) NULL,
	[Etapa] [nvarchar](255) NULL,
	[TipoOportunidad] [nvarchar](255) NULL,
	[PlazoEstimadoProvision] [int] NULL,
	[FechaEstimadaInstalacionServicio] [datetime] NULL,
	[DuracionContrato] [int] NULL,
	[IngresoPorUnicaVezDivisa] [nvarchar](255) NULL,
	[IngresoPorUnicaVez] [nvarchar](255) NULL,
	[FullContractValueNetoDivisa] [nvarchar](255) NULL,
	[FullContractValueNeto] [nvarchar](255) NULL,
	[RecurrenteBrutoMensualDivisa] [nvarchar](255) NULL,
	[RecurrenteBrutoMensual] [nvarchar](255) NULL,
	[CapexDivisa] [nvarchar](255) NULL,
	[Capex] [nvarchar](255) NULL,
	[NumeroDelCaso] [nvarchar](255) NULL,
	[Estado] [nvarchar](255) NULL,
	[Departamento] [nvarchar](255) NULL,
	[TipoSolicitud] [nvarchar](255) NULL,
	[Asunto] [nvarchar](255) NULL,
	[FechaCarga] [datetime] NULL,
	[LoginCarga] [varchar](100) NULL,
	[IdCarga] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacionDB] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_SalesForceOrigen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Sector]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Sector](
	[IdSector] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Sector] PRIMARY KEY CLUSTERED 
(
	[IdSector] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[Servicio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[Servicio](
	[IdLineaNegocio] [int] NULL,
	[IdServicio] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdMedio] [int] NULL,
	[IdGrupo] [int] NULL,
	[IdTipoEnlace] [int] NULL,
	[IdServicioCMI] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Servicio] PRIMARY KEY CLUSTERED 
(
	[IdServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[ServicioCMI]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[ServicioCMI](
	[IdServicioCMI] [int] NOT NULL,
	[CodigoCMI] [varchar](10) NULL,
	[DescripcionPlantilla] [varchar](100) NULL,
	[DescripcionOriginal] [varchar](100) NULL,
	[DescripcionCMI] [varchar](100) NULL,
	[IdGerenciaProducto] [int] NULL,
	[IdLinea] [int] NULL,
	[IdCentroCosto] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ServiciosCMI] PRIMARY KEY CLUSTERED 
(
	[IdServicioCMI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[ServicioSubServicio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[ServicioSubServicio](
	[IdServicio] [int] NOT NULL,
	[IdSubServicio] [int] NULL,
	[IdServicioSubServicio] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoCosto] [int] NULL,
	[IdProveedor] [int] NULL,
	[ContratoMarco] [varchar](50) NULL,
	[IdPeriodos] [int] NULL,
	[Inicio] [int] NULL,
	[Ponderacion] [decimal](18, 2) NULL,
	[IdMoneda] [int] NULL,
	[IdGrupo] [int] NULL,
	[FlagSISEGO] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ServicioSubServicio] PRIMARY KEY CLUSTERED 
(
	[IdServicioSubServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[SubLinea]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[SubLinea](
	[IdLinea] [int] NOT NULL,
	[IdSubLinea] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_SubLineaNegocio] PRIMARY KEY CLUSTERED 
(
	[IdLinea] ASC,
	[IdSubLinea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[SubServicio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[SubServicio](
	[IdSubServicio] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[DescripcionEquivalencia] [varchar](100) NULL,
	[IdTipoSubServicio] [int] NULL,
	[IdDepreciacion] [int] NULL,
	[Orden] [int] NULL,
	[Negrita] [int] NULL,
	[CostoInstalacion] [decimal](18, 2) NULL,
	[Concatenado] [varchar](100) NULL,
	[AEReducido] [varchar](30) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Conceptos] PRIMARY KEY CLUSTERED 
(
	[IdSubServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[TipoCambio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [COMUN].[TipoCambio](
	[IdLineaNegocio] [int] NOT NULL,
	[IdTipoCambio] [int] NOT NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_TipoCambio] PRIMARY KEY CLUSTERED 
(
	[IdTipoCambio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [COMUN].[TipoCambioDetalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [COMUN].[TipoCambioDetalle](
	[IdTipoCambio] [int] NULL,
	[IdTipoCambioDetalle] [int] NOT NULL,
	[IdMoneda] [int] NULL,
	[IdTipificacion] [int] NOT NULL,
	[Anio] [int] NULL,
	[Monto] [decimal](18, 2) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_TipoCambioDetalle] PRIMARY KEY CLUSTERED 
(
	[IdTipoCambioDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [COMUN].[tmp_CCHN]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[tmp_CCHN](
	[BW] [varchar](50) NOT NULL,
	[FEC] [decimal](18, 2) NOT NULL,
	[costo] [decimal](18, 2) NOT NULL,
	[inversion] [decimal](18, 2) NOT NULL,
	[instalacion] [decimal](18, 2) NOT NULL,
	[mantenimiento] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [COMUN].[tmp_VSAT]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [COMUN].[tmp_VSAT](
	[servicio] [nchar](10) NOT NULL,
	[downspeed] [decimal](18, 2) NULL,
	[garantizado] [varchar](50) NULL,
	[costo_principal] [decimal](18, 2) NOT NULL,
	[costo_backup] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FUNNEL].[CmiConfiguracion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FUNNEL].[CmiConfiguracion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Proceso] [varchar](100) NULL,
	[DatosProceso] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FUNNEL].[OportunidadContableOrigen]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FUNNEL].[OportunidadContableOrigen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [varchar](255) NULL,
	[NumeroCaso] [varchar](255) NULL,
	[IngresoAnual] [numeric](10, 2) NULL,
	[OibdaAnual] [numeric](10, 2) NULL,
	[Capex] [numeric](10, 2) NULL,
	[OibdaPorcentaje] [numeric](10, 2) NULL,
	[VanProyecto] [numeric](10, 2) NULL,
	[VanPorVai] [numeric](10, 2) NULL,
	[PayBack] [varchar](50) NULL,
	[ValorRescate] [numeric](10, 2) NULL,
	[IngresoTotal] [numeric](10, 2) NULL,
	[CostosDirectos] [numeric](10, 2) NULL,
	[UtilidadOperativa] [numeric](10, 2) NULL,
	[MargenOperativo] [numeric](10, 2) NULL,
	[NroCotizacion] [int] NULL,
	[CantPtos] [int] NULL,
	[TipoCambio] [numeric](10, 2) NULL,
	[Anio] [int] NULL,
	[FechaCarga] [datetime] NULL,
	[LoginCarga] [varchar](100) NULL,
	[IdCarga] [int] NULL,
	[TipoCambioIngresos] [numeric](10, 2) NULL,
	[TipoCambioCostos] [numeric](10, 2) NULL,
	[TipoCambioCapex] [numeric](10, 2) NULL,
	[RutaCarga] [varchar](255) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[EstadoGanada] [bit] NULL,
 CONSTRAINT [PK_OportunidadContableOrigen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FUNNEL].[OportunidadesHistoriaCarga]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FUNNEL].[OportunidadesHistoriaCarga](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FechaCarga] [datetime] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadesHistoriaCarga] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [FUNNEL].[OportunidadFinancieraOrigen]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FUNNEL].[OportunidadFinancieraOrigen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [varchar](255) NULL,
	[NumeroCaso] [varchar](255) NULL,
	[PeriodoMeses] [int] NULL,
	[IngresoTotal] [numeric](10, 2) NULL,
	[PagoUnicoDolares] [numeric](10, 2) NULL,
	[RecurrenteDolares] [numeric](10, 2) NULL,
	[VanFlujoNeto] [numeric](10, 2) NULL,
	[VanFlujoNetoPorVanIngresos] [numeric](10, 2) NULL,
	[UtilidadOperativa] [numeric](10, 2) NULL,
	[CostosDirectos] [numeric](10, 2) NULL,
	[MargenOperativa] [numeric](10, 2) NULL,
	[CantidadPtos] [int] NULL,
	[Oidba] [numeric](10, 2) NULL,
	[OidbaPorcentaje] [numeric](10, 2) NULL,
	[Capex] [numeric](10, 2) NULL,
	[Payback] [varchar](50) NULL,
	[Depreciacion] [numeric](10, 2) NULL,
	[TipoCambio] [numeric](10, 2) NULL,
	[Tir] [numeric](10, 2) NULL,
	[FechaCarga] [datetime] NULL,
	[LoginCarga] [varchar](100) NULL,
	[IdCarga] [int] NULL,
	[LineaNegocio] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[EstadoGanada] [bit] NULL,
 CONSTRAINT [PK_OportunidadFinanciera] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FUNNEL].[OportunidadHistorialLecturaExcel]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FUNNEL].[OportunidadHistorialLecturaExcel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [varchar](100) NULL,
	[Archivo] [varchar](100) NULL,
	[Ruta] [varchar](500) NULL,
	[FechaModificacion] [datetime] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadHistorialLecturaExcel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FUNNEL].[OportunidadPmoLotusLinea]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FUNNEL].[OportunidadPmoLotusLinea](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [varchar](100) NULL,
	[NumeroCaso] [varchar](100) NULL,
	[Tipo] [int] NULL,
	[Item] [int] NULL,
	[Proceso] [varchar](100) NULL,
	[DatosProceso] [varchar](100) NULL,
	[DetalleProceso] [varchar](100) NULL,
	[OportunidadGanada] [bit] NULL,
	[FechaCarga] [datetime] NULL,
	[LoginCarga] [varchar](100) NULL,
	[IdCarga] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[EstadoGanada] [bit] NULL,
 CONSTRAINT [PK_OportunidadPmoLotusLinea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[Formula]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[Formula](
	[IdFormula] [int] NOT NULL,
	[IdSubServicio] [int] NULL,
	[Descripcion] [varchar](50) NULL,
	[Formula] [varchar](100) NULL,
	[Comentario] [varchar](600) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Formula] PRIMARY KEY CLUSTERED 
(
	[IdFormula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[FormulaDetalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[FormulaDetalle](
	[IdFormula] [int] NOT NULL,
	[IdFormulaDetalle] [int] NOT NULL,
	[Componente] [char](1) NOT NULL,
	[IdSubServicio] [int] NULL,
	[Valor1] [int] NULL,
	[Valor2] [int] NULL,
	[Comentario] [varchar](200) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_FormulaDetalle] PRIMARY KEY CLUSTERED 
(
	[IdFormulaDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[LineaNegocioCMI]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[LineaNegocioCMI](
	[IdLineaNegocio] [int] NOT NULL,
	[IdServicioCMI] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_LineaProductoServicioCMI] PRIMARY KEY CLUSTERED 
(
	[IdLineaNegocio] ASC,
	[IdServicioCMI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[LineaPestana]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[LineaPestana](
	[IdLineaNegocio] [int] NOT NULL,
	[IdPestana] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_LineaPestana_1] PRIMARY KEY CLUSTERED 
(
	[IdPestana] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[Oportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[Oportunidad](
	[IdOportunidad] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoEmpresa] [int] NULL,
	[IdCliente] [int] NULL,
	[Descripcion] [varchar](200) NULL,
	[NumeroSalesForce] [varchar](20) NULL,
	[NumeroCaso] [varchar](20) NULL,
	[Fecha] [datetime] NULL,
	[Alcance] [varchar](max) NULL,
	[Periodo] [int] NULL,
	[TiempoImplantacion] [int] NULL,
	[IdTipoProyecto] [int] NULL,
	[IdTipoServicio] [int] NULL,
	[IdProyectoAnterior] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdAnalistaFinanciero] [int] NULL,
	[IdProductManager] [int] NULL,
	[IdPreVenta] [int] NULL,
	[IdCoordinadorFinanciero] [int] NULL,
	[TiempoProyecto] [int] NULL,
	[IdTipoCambio] [int] NULL,
	[Agrupador] [int] NULL,
	[Version] [int] NULL,
	[VersionPadre] [int] NULL,
	[FlagGanador] [int] NULL,
	[IdMonedaFacturacion] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED 
(
	[IdOportunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadCosto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadCosto](
	[IdOportunidadLineaNegocio] [int] NOT NULL,
	[IdCosto] [int] NULL,
	[IdOportunidadCosto] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoCosto] [int] NULL,
	[Descripcion] [varchar](200) NULL,
	[Monto] [decimal](18, 4) NULL,
	[VelocidadSubidaKBPS] [decimal](18, 4) NULL,
	[PorcentajeGarantizado] [decimal](18, 4) NULL,
	[PorcentajeSobresuscripcion] [decimal](18, 4) NULL,
	[CostoSegmentoSatelital] [decimal](18, 4) NULL,
	[InvAntenaHubUSD] [decimal](18, 4) NULL,
	[AntenaCasaClienteUSD] [decimal](18, 4) NULL,
	[Instalacion] [decimal](18, 4) NULL,
	[IdUnidadConsumo] [int] NULL,
	[IdTipificacion] [int] NULL,
	[CodigoModelo] [varchar](20) NULL,
	[Modelo] [varchar](50) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadCosto] PRIMARY KEY CLUSTERED 
(
	[IdOportunidadCosto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadDocumento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadDocumento](
	[IdOportunidadLineaNegocio] [int] NOT NULL,
	[IdDocumento] [int] IDENTITY(1,1) NOT NULL,
	[IdFlujoCaja] [int] NULL,
	[TipoDocumento] [int] NULL,
	[RutaDocumento] [varchar](300) NULL,
	[Descripcion] [varchar](100) NULL,
	[IdTipoDocumento] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadDocumento] PRIMARY KEY CLUSTERED 
(
	[IdDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadFlujoCaja]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadFlujoCaja](
	[IdOportunidadLineaNegocio] [int] NOT NULL,
	[IdFlujoCaja] [int] IDENTITY(1,1) NOT NULL,
	[IdAgrupador] [int] NULL,
	[IdTipoCosto] [int] NULL,
	[Descripcion] [varchar](100) NULL,
	[IdProveedor] [int] NULL,
	[IdPeriodos] [int] NULL,
	[IdPestana] [int] NULL,
	[IdGrupo] [int] NULL,
	[IdCasoNegocio] [int] NULL,
	[IdServicio] [int] NULL,
	[Cantidad] [int] NULL,
	[CostoUnitario] [decimal](18, 4) NULL,
	[ContratoMarco] [varchar](50) NULL,
	[IdMoneda] [int] NULL,
	[FlagSISEGO] [int] NULL,
	[IdServicioCMI] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[IdSubServicio] [int] NULL,
 CONSTRAINT [PK_ProyectoCMIConcepto] PRIMARY KEY CLUSTERED 
(
	[IdFlujoCaja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion](
	[IdFlujoCaja] [int] NULL,
	[IdFlujoCajaConfiguracion] [int] IDENTITY(1,1) NOT NULL,
	[Ponderacion] [decimal](18, 4) NULL,
	[CostoPreOperativo] [decimal](18, 4) NULL,
	[Inicio] [int] NULL,
	[Meses] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadFlujoCajaConfiguracion] PRIMARY KEY CLUSTERED 
(
	[IdFlujoCajaConfiguracion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadFlujoCajaDetalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadFlujoCajaDetalle](
	[IdFlujoCajaConfiguracion] [int] NOT NULL,
	[IdFlujoCajaDetalle] [int] IDENTITY(1,1) NOT NULL,
	[Anio] [int] NULL,
	[Mes] [int] NULL,
	[Monto] [decimal](18, 7) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[TipoFicha] [char](1) NULL,
 CONSTRAINT [PK_OportunidadFlujoCajaDetalle] PRIMARY KEY CLUSTERED 
(
	[IdFlujoCajaDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadFlujoEstado]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadFlujoEstado](
	[IdOportunidad] [int] NOT NULL,
	[IdOportunidadFlujoEstado] [int] IDENTITY(1,1) NOT NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_OportunidadFlujoEstado] PRIMARY KEY CLUSTERED 
(
	[IdOportunidadFlujoEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadLineaNegocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadLineaNegocio](
	[IdOportunidad] [int] NOT NULL,
	[IdLineaNegocio] [int] NOT NULL,
	[IdOportunidadLineaNegocio] [int] IDENTITY(1,1) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[FechaEdicion] [datetime] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_OportunidadLineaNegocio] PRIMARY KEY CLUSTERED 
(
	[IdOportunidadLineaNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadServicioCMI]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadServicioCMI](
	[IdOportunidadLineaNegocio] [int] NOT NULL,
	[IdServicioCMI] [int] NOT NULL,
	[IdOportunidadServicioCMI] [int] IDENTITY(1,1) NOT NULL,
	[Porcentaje] [decimal](18, 2) NULL,
	[IdAnalista] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadServicioCMI] PRIMARY KEY CLUSTERED 
(
	[IdOportunidadServicioCMI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadTipoCambio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadTipoCambio](
	[IdOportunidadLineaNegocio] [int] NOT NULL,
	[IdTipoCambioOportunidad] [int] IDENTITY(1,1) NOT NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadTipoCambio] PRIMARY KEY CLUSTERED 
(
	[IdTipoCambioOportunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[OportunidadTipoCambioDetalle]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[OportunidadTipoCambioDetalle](
	[IdTipoCambioOportunidad] [int] NOT NULL,
	[IdTipoCambioDetalle] [int] IDENTITY(1,1) NOT NULL,
	[IdMoneda] [int] NULL,
	[IdTipificacion] [int] NOT NULL,
	[Anio] [int] NULL,
	[Monto] [decimal](18, 2) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_OportunidadTipoCambioDetalle] PRIMARY KEY CLUSTERED 
(
	[IdTipoCambioDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[PestanaGrupo]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[PestanaGrupo](
	[IdPestana] [int] NOT NULL,
	[IdGrupo] [int] NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[FlagServicio] [int] NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_PestanaGrupo] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[Requerimiento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[Requerimiento](
	[Id] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](250) NOT NULL,
	[IdOportunidad] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Requerimiento] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[ServicioSubServicioCMI]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[ServicioSubServicioCMI](
	[IdLineaNegocio] [int] NOT NULL,
	[IdSubServicio] [int] NOT NULL,
	[IdServicioCMI] [int] NULL,
	[Costo] [decimal](18, 4) NULL,
	[IdPestana] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_LineaConceptoCMI] PRIMARY KEY CLUSTERED 
(
	[IdLineaNegocio] ASC,
	[IdSubServicio] ASC,
	[IdPestana] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[ServicioSubServicioGrupo]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [OPORTUNIDAD].[ServicioSubServicioGrupo](
	[IdLineaNegocio] [int] NOT NULL,
	[IdConcepto] [int] NOT NULL,
	[IdPestana] [int] NULL,
	[IdGrupo] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_LineaConceptoGrupo] PRIMARY KEY CLUSTERED 
(
	[IdLineaNegocio] ASC,
	[IdConcepto] ASC,
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [OPORTUNIDAD].[SubServicioDatosCapex]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[SubServicioDatosCapex](
	[IdFlujoCaja] [int] NOT NULL,
	[IdSubServicioDatosCapex] [int] IDENTITY(1,1) NOT NULL,
	[Circuito] [varchar](20) NULL,
	[SISEGO] [varchar](20) NULL,
	[MesesAntiguedad] [int] NULL,
	[CostoUnitarioAntiguo] [decimal](18, 2) NULL,
	[ValorResidualSoles] [decimal](18, 2) NULL,
	[CostoUnitario] [decimal](18, 2) NULL,
	[CapexDolares] [decimal](18, 2) NULL,
	[CapexSoles] [decimal](18, 2) NULL,
	[TotalCapex] [decimal](18, 2) NULL,
	[AnioRecupero] [int] NULL,
	[MesRecupero] [int] NULL,
	[AnioComprometido] [int] NULL,
	[MesComprometido] [int] NULL,
	[AnioCertificado] [int] NULL,
	[MesCertificado] [int] NULL,
	[Medio] [varchar](50) NULL,
	[IdTipo] [int] NULL,
	[IdOportunidadCosto] [int] NULL,
	[Garantizado] [varchar](50) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[Cruce] [varchar](100) NULL,
	[AEReducido] [varchar](30) NULL,
	[Combo] [varchar](50) NULL,
	[Antiguedad] [int] NULL,
	[CapexInstalacion] [decimal](18, 2) NULL,
	[CapexReal] [decimal](18, 2) NULL,
	[CapexTotalReal] [decimal](18, 2) NULL,
	[Marca] [varchar](100) NULL,
	[Modelo] [varchar](100) NULL,
	[Tipo] [varchar](100) NULL,
 CONSTRAINT [PK_ConceptoDatosCapex_1] PRIMARY KEY CLUSTERED 
(
	[IdSubServicioDatosCapex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [OPORTUNIDAD].[SubServicioDatosCaratula]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [OPORTUNIDAD].[SubServicioDatosCaratula](
	[IdFlujoCaja] [int] NOT NULL,
	[IdSubServicioDatosCaratula] [int] IDENTITY(1,1) NOT NULL,
	[Circuito] [varchar](50) NULL,
	[IdMedio] [int] NULL,
	[IdTipoEnlace] [int] NULL,
	[IdActivoPasivo] [int] NULL,
	[IdLocalidad] [int] NULL,
	[IdOportunidadCosto] [int] NULL,
	[NumeroMeses] [int] NULL,
	[MontoUnitarioMensual] [decimal](18, 7) NULL,
	[MontoTotalMensual] [decimal](18, 7) NULL,
	[NumeroMesInicioGasto] [int] NULL,
	[IdCostoSondaOportunidad] [int] NULL,
	[FlagRenovacion] [char](1) NULL,
	[Instalacion] [decimal](18, 7) NULL,
	[Desinstalacion] [decimal](18, 7) NULL,
	[PU] [decimal](18, 7) NULL,
	[Alquiler] [decimal](18, 7) NULL,
	[Factor] [decimal](18, 7) NULL,
	[ValorCuota] [decimal](18, 7) NULL,
	[CC] [decimal](18, 7) NULL,
	[CCQProvincia] [decimal](18, 7) NULL,
	[CCQBK] [decimal](18, 7) NULL,
	[CCQCAPEX] [decimal](18, 7) NULL,
	[TIWS] [decimal](18, 7) NULL,
	[RADIO] [decimal](18, 7) NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ConceptoDatosCaratula] PRIMARY KEY CLUSTERED 
(
	[IdSubServicioDatosCaratula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[AccionEstrategica]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[AccionEstrategica](
	[IdAccionEstrategica] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_AccionEstrategica] PRIMARY KEY CLUSTERED 
(
	[IdAccionEstrategica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[EstadoEjecucion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[EstadoEjecucion](
	[IdEstadoEjecucion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_EstadoEjecucion] PRIMARY KEY CLUSTERED 
(
	[IdEstadoEjecucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[EstadoImplementacion]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[EstadoImplementacion](
	[IdEstadoImplementacion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_EstadoImplementacion] PRIMARY KEY CLUSTERED 
(
	[IdEstadoImplementacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[EstadoSisego]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[EstadoSisego](
	[IdEstadoSisego] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_EstadoSisego] PRIMARY KEY CLUSTERED 
(
	[IdEstadoSisego] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[Plan]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[Plan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[KeyPlan] [varchar](250) NULL,
	[SisegoTroba] [varchar](250) NULL,
	[ItemPlan] [varchar](250) NULL,
	[Ke2Plan] [varchar](250) NULL,
	[Po] [varchar](250) NULL,
	[Area] [varchar](250) NULL,
	[Proyecto] [varchar](250) NULL,
	[SubProyecto] [varchar](250) NULL,
	[FechaCreaIP] [datetime] NULL,
	[FechaInicio] [datetime] NULL,
	[FechaPrevista] [datetime] NULL,
	[FechaTermino] [datetime] NULL,
	[FechaCancelacion] [datetime] NULL,
	[FechaLiquidacion] [datetime] NULL,
	[Estado] [varchar](250) NULL,
	[Titulo] [varchar](250) NULL,
	[Jefatura] [varchar](250) NULL,
	[Zonal] [varchar](250) NULL,
	[Mdf] [varchar](250) NULL,
	[Grafo] [varchar](250) NULL,
	[EmpColaboradora] [varchar](250) NULL,
	[ValorManoObra] [numeric](18, 2) NULL,
	[ValorMaterial] [numeric](18, 2) NULL,
	[Vr] [varchar](250) NULL,
	[FechaUltimaEstado] [datetime] NULL,
	[FechaCreacion] [datetime] NULL,
	[Usuario] [varchar](250) NULL,
	[EstadoPlan] [varchar](250) NULL,
 CONSTRAINT [PK_Plan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[PresupuestoSap]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[PresupuestoSap](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Pep0Extrae] [varchar](250) NULL,
	[Pep1Extrae] [varchar](250) NULL,
	[Pep2Extrae] [varchar](250) NULL,
	[Objetivo] [varchar](250) NULL,
	[TotalAccionEstrategica] [varchar](250) NULL,
	[Portafolio] [varchar](250) NULL,
	[AccionEstrategica] [varchar](250) NULL,
	[Pep1Sap] [varchar](250) NULL,
	[Pep2Sap] [varchar](250) NULL,
	[Grafos] [varchar](250) NULL,
	[EnBlanco] [varchar](250) NULL,
	[DescSegProy] [varchar](250) NULL,
	[Segmento] [varchar](250) NULL,
	[Portafolio2] [varchar](250) NULL,
	[DescripcionAccionEstrategica] [varchar](250) NULL,
	[DescripcionPer1] [varchar](250) NULL,
	[DescripcionPer2] [varchar](250) NULL,
	[OrdenDeCompra] [varchar](250) NULL,
	[EnBlanco2] [varchar](250) NULL,
	[PlanSegProy] [numeric](18, 2) NULL,
	[Presupuesto] [numeric](18, 2) NULL,
	[RealCertificado] [numeric](18, 2) NULL,
	[Comprometido] [numeric](18, 2) NULL,
	[PlanResOrd] [numeric](18, 2) NULL,
	[Asignado] [numeric](18, 2) NULL,
	[Disponible] [numeric](18, 2) NULL,
 CONSTRAINT [PK_CuadroSeguimientoProyecto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[Proyecto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[Proyecto](
	[IdProyecto] [char](7) NOT NULL,
	[IdOportunidad] [char](13) NULL,
	[Formato] [varchar](9) NULL,
	[EstadoCpi] [varchar](16) NULL,
	[Fase] [varchar](14) NULL,
	[IdCliente] [int] NULL,
	[Cliente] [varchar](255) NULL,
	[Descripcion] [text] NULL,
	[TipoSolucion] [varchar](44) NULL,
	[Complejidad] [varchar](12) NULL,
	[Licitacion] [char](2) NULL,
	[IngePreventa] [varchar](255) NULL,
	[LiderJefeProyecto] [varchar](255) NULL,
	[JefeProyecto] [varchar](255) NULL,
	[FechaGanada] [date] NULL,
	[FechaPvRegistra] [date] NULL,
	[FechaPvAsignaLjp1] [date] NULL,
	[FechaLjpObservaPv1] [date] NULL,
	[FechaPvAsignaLjp2] [date] NULL,
	[FechaLjpObservaPv2] [date] NULL,
	[FechaPvAsignaLjp3] [date] NULL,
	[FechaLjpInformaAf] [date] NULL,
	[FechaAfObservaPv1] [date] NULL,
	[FechaPvInformaAf1] [date] NULL,
	[FechaAfObservaPv2] [date] NULL,
	[FechaPvInformaAf2] [date] NULL,
	[FechaAfInformaControl1] [date] NULL,
	[FechaControlObservaAf1] [date] NULL,
	[FechaAfInformaControl2] [date] NULL,
	[FechaControlObservaAf2] [date] NULL,
	[FechaAfInformaControl3] [date] NULL,
	[ControlInformaPresupuestado] [date] NULL,
	[FechaKickoffInicioDe] [date] NULL,
	[FechaKickoffInicioHasta] [date] NULL,
	[FechaInicialPreimplantacion] [date] NULL,
	[FechaFinalPreimplantacion] [date] NULL,
	[FechaInicialImplantacion] [date] NULL,
	[FechaFinalImplantacion] [date] NULL,
	[FechaAnteriorFinalImplantacion] [date] NULL,
	[FechaAnteriorFinal1] [date] NULL,
	[FechaAnteriorFinal2] [date] NULL,
	[FechaAnteriorFinal3] [date] NULL,
	[FechaAnteriorFinal4] [date] NULL,
	[FechaAnteriorFinal5] [date] NULL,
	[FechaAnteriorFinal6] [date] NULL,
	[FechaAnteriorFinal7] [date] NULL,
	[FechaAnteriorFinal8] [date] NULL,
	[FechaAnteriorFinal9] [date] NULL,
	[FechaAnteriorFinal0] [date] NULL,
	[FechaInicialProvicion] [date] NULL,
	[FechaFinalProvicion] [date] NULL,
	[FechaCierre] [date] NULL,
	[PasePosventa] [varchar](255) NULL,
	[FechaInicialContratacion] [date] NULL,
	[FechaFinalContratacion] [date] NULL,
	[FechaPrevistaFacturacion] [date] NULL,
	[FechaFinalFacturacion] [date] NULL,
	[Monto] [decimal](18, 2) NULL,
	[PagoUnico] [decimal](18, 2) NULL,
	[PagoRecurrente] [decimal](18, 2) NULL,
	[NroMesesPagoRecurrente] [int] NULL,
	[TipoCambio] [decimal](18, 2) NULL,
	[Opex] [decimal](18, 2) NULL,
	[Capex] [decimal](18, 2) NULL,
	[ControlerOpex] [varchar](255) NULL,
	[ControlerCapex] [varchar](255) NULL,
	[AnalistaFinanciero] [varchar](255) NULL,
	[EstadoSuspencion] [varchar](255) NULL,
	[FechaSuspencion] [date] NULL,
	[FechaFinSuspencion] [date] NULL,
	[ComentarioBacklogOtros] [text] NULL,
	[Van] [decimal](18, 2) NULL,
	[VanVai] [decimal](18, 2) NULL,
	[Obida] [decimal](18, 2) NULL,
	[MargenObida] [decimal](18, 2) NULL,
	[PayBack] [int] NULL,
	[FechaContratacionServicio] [date] NULL,
	[TipoProyecto] [varchar](255) NULL,
	[NroCaso] [varchar](255) NULL,
	[FechaKickOff] [date] NULL,
	[FechaActaCierra] [date] NULL,
	[FechaRegistroActaCierra] [date] NULL,
	[LiderPreventa] [varchar](255) NULL,
	[FechaNuevoInicioContratacion] [date] NULL,
	[FechaNuevoFinContratacion] [date] NULL,
	[PendienteActa] [char](2) NULL,
	[TipoProyecto2] [varchar](255) NULL,
	[ProyectoEstado] [varchar](255) NULL,
	[FechaEjecucion] [date] NULL,
	[EstadoPasePostVenta] [date] NULL,
	[ConfirmacionPaseFinalizado] [date] NULL,
	[Comentarios] [text] NULL,
	[RequisitosPendientes] [text] NULL,
	[FechaKickoffReal] [date] NULL,
	[CodProyectoMilestone] [char](13) NULL,
	[Ruc] [varchar](11) NULL,
	[TipoEntidad] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[SegProyecto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[SegProyecto](
	[idSegProyec] [int] IDENTITY(1,1) NOT NULL,
	[idSisego] [int] NULL,
	[Pep] [varchar](250) NULL,
	[IdAccionEstrategica] [int] NULL,
	[SupervisionEstudio] [varchar](250) NULL,
	[DescripcionAccionEstratOriginal] [varchar](250) NULL,
	[SupervisionAccionEstrategia] [varchar](250) NULL,
	[ValorTipoProyecto] [char](50) NULL,
	[NombreProyecto] [char](10) NULL,
	[Pmo] [char](10) NULL,
	[PmoManual] [varchar](250) NULL,
	[PublicaPrivada] [char](10) NULL,
	[Cont] [varchar](50) NULL,
	[EsstadoPmo] [varchar](50) NULL,
	[TipoSolucion] [varchar](250) NULL,
	[FiImplantacion] [datetime] NULL,
	[Ffimplantacion] [datetime] NULL,
	[IdTipoCuadrante] [int] NULL,
	[DiasFinPmo] [char](5) NULL,
	[Supervision] [varchar](250) NULL,
	[RasgosDsp] [varchar](250) NULL,
	[RangoFinPmo] [varchar](250) NULL,
	[JefeProyecto] [varchar](250) NULL,
	[Ppto] [numeric](18, 2) NULL,
	[real] [numeric](18, 2) NULL,
	[Compro] [numeric](18, 2) NULL,
	[PlanRes] [numeric](18, 2) NULL,
	[Asignado] [numeric](18, 2) NULL,
	[Disponible] [numeric](18, 2) NULL,
	[Anio] [varchar](50) NULL,
 CONSTRAINT [PK_SegProyecto] PRIMARY KEY CLUSTERED 
(
	[idSegProyec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[Sisego]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[Sisego](
	[IdSisego] [int] IDENTITY(1,1) NOT NULL,
	[CodSisego] [varchar](50) NOT NULL,
	[BiAnualItem] [varchar](50) NULL,
	[Duplicado] [char](10) NULL,
	[IdCliente] [int] NULL,
	[IdTipoRequerimiento] [int] NULL,
	[TotalSoles] [numeric](18, 2) NULL,
	[TotalDolares] [numeric](18, 2) NULL,
	[TotalSupervision] [numeric](18, 2) NULL,
	[DiasCalendarioEjecucion] [int] NULL,
	[TotalSisego] [numeric](18, 2) NULL,
	[Pep1] [varchar](50) NULL,
	[Pep2] [varchar](50) NULL,
	[Grafo] [varchar](50) NULL,
	[IdAccionEstrategica] [int] NULL,
	[IdTipoProyecto] [int] NULL,
	[IdTipoDecicionProyecto] [int] NULL,
	[NombreSolicitante] [varchar](250) NULL,
	[NombreSupervisorEstudio] [varchar](250) NULL,
	[IdProyecto] [char](7) NULL,
	[Pmo2] [char](10) NULL,
	[SalesForce] [varchar](50) NULL,
	[IdTipoEntidad] [int] NULL,
	[IdEstadoSisego] [int] NULL,
	[Bitacora] [varchar](250) NULL,
	[Quiebre] [varchar](250) NULL,
	[Responsable] [varchar](250) NULL,
	[JefeResponsable] [varchar](250) NULL,
	[AreaResponsable] [varchar](250) NULL,
	[FechaIngreso] [datetime] NULL,
	[MesIngreso] [datetime] NULL,
	[AnioIngreso] [char](4) NULL,
	[FechaActual] [datetime] NULL,
	[DiasBandeja] [int] NULL,
	[AprobPlaneaControl] [datetime] NULL,
	[anioAprobacion] [datetime] NULL,
	[fechaGrafo] [datetime] NULL,
	[FechaInicioEje] [datetime] NULL,
	[FechaFinEje] [datetime] NULL,
	[IdEstadoImplementacion] [int] NULL,
	[Cancelado] [varchar](50) NULL,
	[IdTipoCriticidad] [int] NULL,
	[IdEstadoEjecucion] [int] NULL,
	[nivel] [int] NULL,
	[Pep0] [varchar](250) NULL,
	[Denominacion] [varchar](250) NULL,
	[Pep1ResultadoDefault] [varchar](50) NULL,
	[CantidadSisegoKey] [varchar](250) NULL,
	[FibraRepetida] [char](10) NULL,
	[BaseSandy] [char](10) NULL,
	[MontoSisegoReal] [numeric](18, 2) NULL,
	[AnioOrigenSisego] [char](10) NULL,
	[KeySisegoAccionEstrategica] [varchar](250) NULL,
	[ItemPlan] [varchar](250) NULL,
	[FlagExpediente] [char](10) NULL,
	[DiasDesdeFechaInicio] [int] NULL,
	[RangoInicioObras] [varchar](250) NULL,
	[FechaFinObraOMci] [datetime] NULL,
	[DiasDesdeFinObra] [int] NULL,
	[MesFinObraOMci] [datetime] NULL,
	[FechaCertificacion] [datetime] NULL,
	[Departamento] [varchar](250) NULL,
	[Provincia] [varchar](250) NULL,
	[ResponsableRed] [varchar](250) NULL,
	[IdEstado] [char](10) NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_Sisego] PRIMARY KEY CLUSTERED 
(
	[IdSisego] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoCriticidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoCriticidad](
	[IdTipoCriticidad] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoCriticidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoCriticidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoCuadrante]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoCuadrante](
	[IdTipoCuadrante] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoCuadrante] PRIMARY KEY CLUSTERED 
(
	[IdTipoCuadrante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoDecisionProyecto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoDecisionProyecto](
	[IdTipoDecisionProyecto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoDecisionProyecto] PRIMARY KEY CLUSTERED 
(
	[IdTipoDecisionProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoEntidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoEntidad](
	[IdTipoEntidad] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoEntidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoProyecto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoProyecto](
	[IdTipoProyecto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [nchar](10) NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoProyecto] PRIMARY KEY CLUSTERED 
(
	[IdTipoProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoRequerimiento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoRequerimiento](
	[IdTipoRequerimiento] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoRequerimiento] PRIMARY KEY CLUSTERED 
(
	[IdTipoRequerimiento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TipoTrabajoRed]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TipoTrabajoRed](
	[IdTipoTrabajoRed] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
 CONSTRAINT [PK_TipoTrabajoRed] PRIMARY KEY CLUSTERED 
(
	[IdTipoTrabajoRed] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PLANTAEXTERNA].[TrabajoRed]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PLANTAEXTERNA].[TrabajoRed](
	[idDetalle] [int] IDENTITY(1,1) NOT NULL,
	[idSisego] [int] NULL,
	[Correlativo] [char](2) NULL,
	[Ptr] [varchar](250) NULL,
	[IdTipoTrabajoRed] [int] NULL,
	[EstadoPtr] [char](2) NULL,
	[Eecc] [varchar](50) NULL,
	[FechaM] [datetime] NULL,
	[MesM] [datetime] NULL,
	[ImportePtr] [numeric](18, 2) NULL,
	[HojaGestion] [varchar](250) NULL,
	[FechaHojaGestion] [datetime] NULL,
	[Zonal] [char](10) NULL,
 CONSTRAINT [PK_BaseRed] PRIMARY KEY CLUSTERED 
(
	[idDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PROYECTO].[ProyectoSF]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PROYECTO].[ProyectoSF](
	[IdProyecto] [char](7) NOT NULL,
	[IdOportunidad] [char](13) NULL,
	[Formato] [varchar](9) NULL,
	[EstadoCpi] [varchar](16) NULL,
	[Fase] [varchar](14) NULL,
	[IdCliente] [int] NULL,
	[Cliente] [varchar](255) NULL,
	[Descripcion] [text] NULL,
	[TipoSolucion] [varchar](44) NULL,
	[Complejidad] [varchar](12) NULL,
	[Licitacion] [char](2) NULL,
	[IngePreventa] [varchar](255) NULL,
	[LiderJefeProyecto] [varchar](255) NULL,
	[JefeProyecto] [varchar](255) NULL,
	[FechaGanada] [date] NULL,
	[FechaPvRegistra] [date] NULL,
	[FechaPvAsignaLjp1] [date] NULL,
	[FechaLjpObservaPv1] [date] NULL,
	[FechaPvAsignaLjp2] [date] NULL,
	[FechaLjpObservaPv2] [date] NULL,
	[FechaPvAsignaLjp3] [date] NULL,
	[FechaLjpInformaAf] [date] NULL,
	[FechaAfObservaPv1] [date] NULL,
	[FechaPvInformaAf1] [date] NULL,
	[FechaAfObservaPv2] [date] NULL,
	[FechaPvInformaAf2] [date] NULL,
	[FechaAfInformaControl1] [date] NULL,
	[FechaControlObservaAf1] [date] NULL,
	[FechaAfInformaControl2] [date] NULL,
	[FechaControlObservaAf2] [date] NULL,
	[FechaAfInformaControl3] [date] NULL,
	[ControlInformaPresupuestado] [date] NULL,
	[FechaKickoffInicioDe] [date] NULL,
	[FechaKickoffInicioHasta] [date] NULL,
	[FechaInicialPreimplantacion] [date] NULL,
	[FechaFinalPreimplantacion] [date] NULL,
	[FechaInicialImplantacion] [date] NULL,
	[FechaFinalImplantacion] [date] NULL,
	[FechaAnteriorFinalImplantacion] [date] NULL,
	[FechaAnteriorFinal1] [date] NULL,
	[FechaAnteriorFinal2] [date] NULL,
	[FechaAnteriorFinal3] [date] NULL,
	[FechaAnteriorFinal4] [date] NULL,
	[FechaAnteriorFinal5] [date] NULL,
	[FechaAnteriorFinal6] [date] NULL,
	[FechaAnteriorFinal7] [date] NULL,
	[FechaAnteriorFinal8] [date] NULL,
	[FechaAnteriorFinal9] [date] NULL,
	[FechaAnteriorFinal0] [date] NULL,
	[FechaInicialProvicion] [date] NULL,
	[FechaFinalProvicion] [date] NULL,
	[FechaCierre] [date] NULL,
	[PasePosventa] [varchar](255) NULL,
	[FechaInicialContratacion] [date] NULL,
	[FechaFinalContratacion] [date] NULL,
	[FechaPrevistaFacturacion] [date] NULL,
	[FechaFinalFacturacion] [date] NULL,
	[Monto] [decimal](18, 2) NULL,
	[PagoUnico] [decimal](18, 2) NULL,
	[PagoRecurrente] [decimal](18, 2) NULL,
	[NroMesesPagoRecurrente] [int] NULL,
	[TipoCambio] [decimal](18, 2) NULL,
	[Opex] [decimal](18, 2) NULL,
	[Capex] [decimal](18, 2) NULL,
	[ControlerOpex] [varchar](255) NULL,
	[ControlerCapex] [varchar](255) NULL,
	[AnalistaFinanciero] [varchar](255) NULL,
	[EstadoSuspencion] [varchar](255) NULL,
	[FechaSuspencion] [date] NULL,
	[FechaFinSuspencion] [date] NULL,
	[ComentarioBacklogOtros] [text] NULL,
	[Van] [decimal](18, 2) NULL,
	[VanVai] [decimal](18, 2) NULL,
	[Obida] [decimal](18, 2) NULL,
	[MargenObida] [decimal](18, 2) NULL,
	[PayBack] [int] NULL,
	[FechaContratacionServicio] [date] NULL,
	[TipoProyecto] [varchar](255) NULL,
	[NroCaso] [varchar](255) NULL,
	[FechaKickOff] [date] NULL,
	[FechaActaCierra] [date] NULL,
	[FechaRegistroActaCierra] [date] NULL,
	[LiderPreventa] [varchar](255) NULL,
	[FechaNuevoInicioContratacion] [date] NULL,
	[FechaNuevoFinContratacion] [date] NULL,
	[PendienteActa] [char](2) NULL,
	[TipoProyecto2] [varchar](255) NULL,
	[ProyectoEstado] [varchar](255) NULL,
	[FechaEjecucion] [date] NULL,
	[EstadoPasePostVenta] [date] NULL,
	[ConfirmacionPaseFinalizado] [date] NULL,
	[Comentarios] [text] NULL,
	[RequisitosPendientes] [text] NULL,
	[FechaKickoffReal] [date] NULL,
	[CodProyectoMilestone] [char](13) NULL,
	[Ruc] [varchar](11) NULL,
	[TipoEntidad] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[ActividadOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[ActividadOportunidad](
	[IdActividad] [int] IDENTITY(1,1) NOT NULL,
	[IdActividadPadre] [int] NULL,
	[Descripcion] [varchar](250) NULL,
	[Predecesoras] [varchar](150) NULL,
	[IdFase] [int] NULL,
	[IdEtapa] [int] NULL,
	[FlgCapexMayor] [bit] NULL,
	[FlgCapexMenor] [bit] NULL,
	[IdAreaSeguimiento] [int] NULL,
	[NumeroDiaCapexMenor] [int] NULL,
	[CantidadDiasCapexMenor] [int] NULL,
	[NumeroDiaCapexMayor] [int] NULL,
	[CantidadDiasCapexMayor] [int] NULL,
	[IdTipoActividad] [varchar](10) NULL,
	[AsegurarOferta] [bit] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ActividadOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdActividad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[ActividadSegmentoNegocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [TRAZABILIDAD].[ActividadSegmentoNegocio](
	[IdActividadSegmentoNegocio] [int] IDENTITY(1,1) NOT NULL,
	[IdActividad] [int] NOT NULL,
	[IdSegmentoNegocio] [int] NOT NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ActividadSegmentoNegocio] PRIMARY KEY CLUSTERED 
(
	[IdActividadSegmentoNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [TRAZABILIDAD].[AreaSegmentoNegocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [TRAZABILIDAD].[AreaSegmentoNegocio](
	[IdAreaSegmentoNegocio] [int] IDENTITY(1,1) NOT NULL,
	[IdAreaSeguimiento] [int] NOT NULL,
	[IdSegmentoNegocio] [int] NOT NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_AreaSegmentoNegocio] PRIMARY KEY CLUSTERED 
(
	[IdAreaSegmentoNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [TRAZABILIDAD].[AreaSeguimiento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[AreaSeguimiento](
	[IdAreaSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_AreaSeguimiento] PRIMARY KEY CLUSTERED 
(
	[IdAreaSeguimiento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[CasoHistoricoSF]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[CasoHistoricoSF](
	[IdOportunidadSF] [varchar](20) NULL,
	[IdCasoSF] [varchar](20) NULL,
	[FechaApertura] [datetime] NULL,
	[FechaModificacion] [varchar](30) NULL,
	[FechaCompromisoAtencion] [varchar](30) NULL,
	[TipoCaso] [varchar](200) NULL,
	[UsuarioModificacion] [varchar](200) NULL,
	[CampoEvento] [varchar](200) NULL,
	[ValorAnterior] [varchar](300) NULL,
	[ValorNuevo] [varchar](300) NULL,
	[UsuarioCreacion] [varchar](200) NULL,
	[FechaCierre] [datetime] NULL,
	[Estado] [varchar](30) NULL,
	[Complejidad] [varchar](30) NULL,
	[EtapaOportunidad] [varchar](100) NULL,
	[PropietarioCaso] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[CasoHistoricoSF02]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[CasoHistoricoSF02](
	[IdOportunidadSF] [varchar](20) NULL,
	[TipoOportunidad] [varchar](60) NULL,
	[LineaNegocio] [varchar](100) NULL,
	[TipoCiclo] [varchar](60) NULL,
	[NombreOportunidad] [varchar](200) NULL,
	[NombreCliente] [varchar](200) NULL,
	[UsuarioCreacion] [varchar](200) NULL,
	[PropietarioCaso] [varchar](200) NULL,
	[Moneda] [varchar](20) NULL,
	[ImporteFCV] [varchar](100) NULL,
	[FechaCreacion] [varchar](30) NULL,
	[IdCasoSF] [varchar](20) NULL,
	[FechaApertura] [varchar](30) NULL,
	[FechaCompromisoAtencion] [varchar](30) NULL,
	[FechaAsignacion] [varchar](30) NULL,
	[Complejidad] [varchar](60) NULL,
	[DuracionAsignacionCaso] [numeric](6, 2) NULL,
	[EstadoOportunidad] [varchar](60) NULL,
	[FechaCierreReal] [varchar](30) NULL,
	[SectorComercial] [varchar](200) NULL,
	[TipoEntidad] [varchar](60) NULL,
	[EtapaOportunidad] [varchar](100) NULL,
	[FlgLicitacion] [varchar](2) NULL,
	[PropietarioOportunidad] [varchar](200) NULL,
	[FechaCierre] [varchar](30) NULL,
	[DepartamentoArea] [varchar](100) NULL,
	[TipoSolicitud] [varchar](100) NULL,
	[EstadoCaso] [varchar](60) NULL,
	[PropietarioAsignado] [varchar](200) NULL,
	[FechaFinAsignacion] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[CasoOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[CasoOportunidad](
	[IdCaso] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [int] NOT NULL,
	[Descripcion] [varchar](250) NULL,
	[IdCasoSF] [varchar](20) NULL,
	[IdCasoPadre] [int] NULL,
	[IdTipoSolicitud] [int] NULL,
	[Asunto] [varchar](250) NULL,
	[IdFase] [int] NULL,
	[IdEtapa] [int] NULL,
	[Complejidad] [varchar](20) NULL,
	[Prioridad] [int] NULL,
	[IdEstadoCaso] [int] NULL,
	[SolucionCaso] [varchar](250) NULL,
	[FechaApertura] [datetime] NULL,
	[FechaCierre] [datetime] NULL,
	[FechaCompromisoAtencion] [datetime] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_CasoOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdCaso] ASC,
	[IdOportunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[ConceptoSeguimiento]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[ConceptoSeguimiento](
	[IdConcepto] [int] IDENTITY(1,1) NOT NULL,
	[IdConceptoPadre] [int] NULL,
	[Descripcion] [varchar](250) NULL,
	[Nivel] [int] NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ConceptoSeguimiento] PRIMARY KEY CLUSTERED 
(
	[IdConcepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[DatosPreventaMovil]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[DatosPreventaMovil](
	[IdDatosPreventaMovil] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [int] NOT NULL,
	[IdCaso] [int] NOT NULL,
	[IdOperadorActual] [int] NULL,
	[LineasActuales] [int] NULL,
	[MesesContratoActual] [int] NULL,
	[RecurrenteMensualActual] [numeric](12, 2) NULL,
	[FCVActual] [numeric](12, 2) NULL,
	[ArpuServicioActual] [numeric](12, 2) NULL,
	[Lineas] [int] NULL,
	[MesesContrato] [int] NULL,
	[RecurrenteMensualVR] [numeric](12, 2) NULL,
	[FCVVR] [numeric](12, 2) NULL,
	[ArpuServicioVR] [numeric](12, 2) NULL,
	[FechaPresentacion] [datetime] NULL,
	[FechaBuenaPro] [datetime] NULL,
	[Observaciones] [varchar](4000) NULL,
	[MontoCapex] [numeric](12, 2) NULL,
	[IdEstadoOportunidad] [int] NULL,
	[FCVMovistar] [numeric](12, 2) NULL,
	[ArpuTotalMovistar] [numeric](12, 2) NULL,
	[VanVai] [int] NULL,
	[MesesRecupero] [int] NULL,
	[FCVClaro] [numeric](12, 2) NULL,
	[ArpuTotalClaro] [numeric](12, 2) NULL,
	[FCVEntel] [numeric](12, 2) NULL,
	[ArpuTotalEntel] [numeric](12, 2) NULL,
	[FCVViettel] [numeric](12, 2) NULL,
	[ArpuTotalViettel] [numeric](12, 2) NULL,
	[ArpuServicio] [numeric](12, 2) NULL,
	[ArpuEquipos] [numeric](12, 2) NULL,
	[ModalidadEquipo] [int] NULL,
	[CostoPromedioEquipo] [numeric](12, 2) NULL,
	[PorcentajeSubsidio] [numeric](12, 2) NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[IdEstado] [int] NULL,
 CONSTRAINT [PK_DatosPreventaMovil] PRIMARY KEY CLUSTERED 
(
	[IdDatosPreventaMovil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[DetalleActividadOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[DetalleActividadOportunidad](
	[IdSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [int] NOT NULL,
	[IdCaso] [int] NULL,
	[IdAreaSeguimiento] [int] NULL,
	[IdActividad] [int] NULL,
	[DuracionEstimada] [numeric](10, 2) NULL,
	[TrabajoEstimado] [numeric](10, 2) NULL,
	[FechaInicioEstimada] [datetime] NULL,
	[FechaFinEstimada] [datetime] NULL,
	[Predecesoras] [varchar](150) NULL,
	[DuracionReal] [numeric](10, 2) NULL,
	[TrabajoReal] [numeric](10, 2) NULL,
	[FechaInicioReal] [datetime] NULL,
	[FechaFinReal] [datetime] NULL,
	[AvanceEstimado] [numeric](6, 2) NULL,
	[AvanceReal] [numeric](6, 2) NULL,
	[IdEstadoEjecucion] [int] NULL,
	[IdEstadoCumplimiento] [int] NULL,
	[FlgObservaciones] [bit] NULL,
	[FlgFilesAdjuntos] [bit] NULL,
	[IdRecursoEjecutor] [int] NULL,
	[IdRecursoRevisor] [int] NULL,
	[Observaciones] [varchar](2000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_SeguimientoActividad] PRIMARY KEY CLUSTERED 
(
	[IdSeguimiento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[DocumentoAdjunto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[DocumentoAdjunto](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [int] NULL,
	[IdCaso] [int] NULL,
	[IdTipoDocumento] [int] NULL,
	[Descripcion] [varchar](150) NULL,
	[IdSeguimiento] [int] NULL,
	[IdObservacion] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_DocumentoAdjunto] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[EquipoTrabajo]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[EquipoTrabajo](
	[IdEquipoTrabajo] [int] NOT NULL,
	[IdEquipoTrabajoPadre] [int] NULL,
	[Descripcion] [varchar](250) NULL,
	[Nivel] [int] NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_EquipoTrabajo] PRIMARY KEY CLUSTERED 
(
	[IdEquipoTrabajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[EstadoCaso]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[EstadoCaso](
	[IdEstadoCaso] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[EstadoStop] [bit] NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_EstadoCaso] PRIMARY KEY CLUSTERED 
(
	[IdEstadoCaso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[EtapaOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[EtapaOportunidad](
	[IdEtapa] [int] IDENTITY(1,1) NOT NULL,
	[IdFase] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_EtapaOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdEtapa] ASC,
	[IdFase] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[FaseOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[FaseOportunidad](
	[IdFase] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdGrupoFase] [int] NOT NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_FaseOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdFase] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[Feriado]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[Feriado](
	[IdFeriado] [int] NOT NULL,
	[Descripcion] [varchar](200) NULL,
	[FechaInicio] [datetime] NULL,
	[FechaFin] [datetime] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Feriado] PRIMARY KEY CLUSTERED 
(
	[IdFeriado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[FileDocumentoAdjunto]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[FileDocumentoAdjunto](
	[IdDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[IdFileDocAdjunto] [int] NOT NULL,
	[Descripcion] [varchar](150) NULL,
	[RutaFile] [varchar](250) NULL,
	[TipoFile] [int] NULL,
	[Tamanio] [numeric](6, 2) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_FileDocumentoAdjunto] PRIMARY KEY CLUSTERED 
(
	[IdDocAdjunto] ASC,
	[IdFileDocAdjunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[FuncionPropietario]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[FuncionPropietario](
	[IdFuncionPropietario] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](250) NULL,
	[IdSector] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_FuncionPropietario] PRIMARY KEY CLUSTERED 
(
	[IdFuncionPropietario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[GrupoFaseOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[GrupoFaseOportunidad](
	[IdGrupoFase] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_GrupoFaseOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdGrupoFase] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[MotivoOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[MotivoOportunidad](
	[IdMotivoOportunidad] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_MotivoOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdMotivoOportunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[ObservacionOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[ObservacionOportunidad](
	[IdObservacion] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidad] [int] NOT NULL,
	[IdCaso] [int] NULL,
	[IdSeguimiento] [int] NULL,
	[FechaSeguimiento] [datetime] NULL,
	[IdConcepto] [int] NULL,
	[Observaciones] [varchar](5000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_ObservacionOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdObservacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[OportunidadSF]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[OportunidadSF](
	[IdOportunidad] [varchar](20) NULL,
	[NombreOportunidad] [varchar](300) NULL,
	[NombreCliente] [varchar](300) NULL,
	[CicloVenta] [varchar](30) NULL,
	[TipoCiclo] [varchar](30) NULL,
	[UsuarioCreacion] [varchar](200) NULL,
	[FuncionPropietario] [varchar](200) NULL,
	[MonedaFCV] [varchar](20) NULL,
	[ImporteFCV] [numeric](14, 2) NULL,
	[FlgLicitacion] [varchar](2) NULL,
	[FechaCierreReal] [varchar](30) NULL,
	[ProbalidadExito] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[OportunidadST]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[OportunidadST](
	[IdOportunidad] [int] IDENTITY(1,1) NOT NULL,
	[IdOportunidadPadre] [int] NULL,
	[IdOportunidadSF] [varchar](20) NULL,
	[IdOportunidadAux] [varchar](20) NULL,
	[Descripcion] [varchar](250) NULL,
	[IdTipoOportunidad] [int] NULL,
	[IdMotivoOportunidad] [int] NULL,
	[IdTipoEntidadCliente] [int] NULL,
	[IdCliente] [int] NULL,
	[IdSector] [int] NULL,
	[IdSegmentoNegocio] [int] NULL,
	[IdTipoCicloVenta] [int] NULL,
	[IdTipoCicloImplementacion] [int] NULL,
	[Prioridad] [int] NULL,
	[ProbalidadExito] [int] NULL,
	[PorcentajeCierre] [int] NULL,
	[PorcentajeCheckList] [int] NULL,
	[IdFase] [int] NULL,
	[IdEtapa] [int] NULL,
	[FlgCapexMayor] [bit] NULL,
	[ImporteCapex] [numeric](12, 2) NULL,
	[IdMoneda] [int] NULL,
	[ImporteFCV] [numeric](12, 2) NULL,
	[FechaApertura] [datetime] NULL,
	[FechaCierre] [datetime] NULL,
	[IdEstadoOportunidad] [varchar](10) NULL,
	[IdRecursoComercial] [int] NULL,
	[IdRecursoPreventa] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
	[IdFuncionPropietario] [int] NULL,
 CONSTRAINT [PK_Oportunidad] PRIMARY KEY CLUSTERED 
(
	[IdOportunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[Recurso]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[Recurso](
	[IdRecurso] [int] IDENTITY(1,1) NOT NULL,
	[TipoRecurso] [int] NULL,
	[UserNameSF] [varchar](30) NULL,
	[Nombre] [varchar](150) NULL,
	[DNI] [varchar](16) NULL,
	[Email] [varchar](80) NULL,
	[IdRolDefecto] [int] NULL,
	[IdOrigen] [int] NULL,
	[IdEmpresa] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_Recurso] PRIMARY KEY CLUSTERED 
(
	[IdRecurso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[RecursoEquipoTrabajo]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[RecursoEquipoTrabajo](
	[IdRecursoEquipoTrabajo] [int] IDENTITY(1,1) NOT NULL,
	[IdRecurso] [int] NOT NULL,
	[IdEquipoTrabajoNivel1] [int] NULL,
	[IdEquipoTrabajoNivel2] [int] NULL,
	[IdEquipoTrabajoNivel3] [int] NULL,
	[IdEquipoTrabajoNivel4] [int] NULL,
	[IdEquipoTrabajoNivel5] [int] NULL,
	[FInicioAsignacion] [datetime] NULL,
	[FFinAsignacion] [datetime] NULL,
	[Observaciones] [varchar](1000) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_RecursoEquipoTrabajo] PRIMARY KEY CLUSTERED 
(
	[IdRecursoEquipoTrabajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[RecursoOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[RecursoOportunidad](
	[IdOportunidad] [int] NOT NULL,
	[IdAsignacion] [int] IDENTITY(1,1) NOT NULL,
	[IdRecurso] [int] NOT NULL,
	[IdCaso] [int] NULL,
	[IdRol] [int] NULL,
	[PorcentajeDedicacion] [int] NULL,
	[Observaciones] [varchar](2000) NULL,
	[FInicioAsignacion] [datetime] NULL,
	[FFinAsignacion] [datetime] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_RecursoOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdAsignacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[RolRecurso]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[RolRecurso](
	[IdRol] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](150) NULL,
	[Nivel] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_RolRecurso] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[SegmentoNegocio]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[SegmentoNegocio](
	[IdSegmentoNegocio] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_SegmentoNegocio] PRIMARY KEY CLUSTERED 
(
	[IdSegmentoNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[TipoOportunidad]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[TipoOportunidad](
	[IdTipoOportunidad] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_TipoOportunidad] PRIMARY KEY CLUSTERED 
(
	[IdTipoOportunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [TRAZABILIDAD].[TipoSolicitud]    Script Date: 8/22/2018 9:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [TRAZABILIDAD].[TipoSolicitud](
	[IdTipoSolicitud] [int] NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[OrdenVisual] [int] NULL,
	[IdEstado] [int] NULL,
	[IdUsuarioCreacion] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdUsuarioEdicion] [int] NULL,
	[FechaEdicion] [datetime] NULL,
 CONSTRAINT [PK_TipoSolicitud] PRIMARY KEY CLUSTERED 
(
	[IdTipoSolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [CAPEX].[boton] ADD  DEFAULT (NULL) FOR [ibot_pad]
GO
ALTER TABLE [CAPEX].[boton] ADD  DEFAULT (NULL) FOR [vbot_rut]
GO
ALTER TABLE [CAPEX].[boton] ADD  DEFAULT (NULL) FOR [cbot_vis]
GO
ALTER TABLE [CAPEX].[boton] ADD  DEFAULT (NULL) FOR [vbot_con]
GO
ALTER TABLE [CAPEX].[concepto] ADD  DEFAULT ('1') FOR [ccon_est]
GO
ALTER TABLE [CAPEX].[descripcion] ADD  DEFAULT ('1') FOR [cdes_est]
GO
ALTER TABLE [CAPEX].[descripcion_accion_estrategica] ADD  DEFAULT ('1') FOR [cdae_est]
GO
ALTER TABLE [CAPEX].[detalle] ADD  DEFAULT ('1') FOR [idet_can]
GO
ALTER TABLE [CAPEX].[detalle] ADD  DEFAULT ('0.00') FOR [ndet_pus]
GO
ALTER TABLE [CAPEX].[detalle] ADD  DEFAULT ('0.00') FOR [ndet_ptd]
GO
ALTER TABLE [CAPEX].[detalle] ADD  DEFAULT ('0.00') FOR [ndet_pts]
GO
ALTER TABLE [CAPEX].[estado] ADD  CONSTRAINT [DF__estado__cest_est__3D5E1FD2]  DEFAULT ('1') FOR [cest_est]
GO
ALTER TABLE [CAPEX].[etapa] ADD  DEFAULT ('1') FOR [ceta_est]
GO
ALTER TABLE [CAPEX].[mes] ADD  DEFAULT ('1') FOR [cmes_est]
GO
ALTER TABLE [CAPEX].[producto] ADD  DEFAULT ('1') FOR [cpro_est]
GO
ALTER TABLE [CAPEX].[producto] ADD  DEFAULT (NULL) FOR [iaes_id]
GO
ALTER TABLE [CAPEX].[producto] ADD  DEFAULT (NULL) FOR [iae2_id]
GO
ALTER TABLE [CAPEX].[producto] ADD  DEFAULT (NULL) FOR [icon_id]
GO
ALTER TABLE [CAPEX].[solicitud_capex] ADD  DEFAULT (NULL) FOR [copo_cod]
GO
ALTER TABLE [CAPEX].[solicitud_capex] ADD  DEFAULT ('0') FOR [iest_id]
GO
ALTER TABLE [CAPEX].[solicitud_capex] ADD  DEFAULT (NULL) FOR [iten_id]
GO
ALTER TABLE [CAPEX].[solicitud_capex] ADD  DEFAULT (NULL) FOR [nsca_if2]
GO
ALTER TABLE [CAPEX].[solicitud_capex] ADD  DEFAULT (NULL) FOR [nsca_van]
GO
ALTER TABLE [CAPEX].[solicitud_capex] ADD  DEFAULT (NULL) FOR [isca_pay]
GO
ALTER TABLE [CAPEX].[sustento] ADD  DEFAULT ('1') FOR [itsu_id]
GO
ALTER TABLE [CAPEX].[tarea] ADD  DEFAULT ('1') FOR [ctar_est]
GO
ALTER TABLE [CAPEX].[tarea] ADD  DEFAULT (NULL) FOR [itar_tar]
GO
ALTER TABLE [CAPEX].[tarea] ADD  DEFAULT ('1') FOR [vtar_ord]
GO
ALTER TABLE [CAPEX].[tarea] ADD  DEFAULT (NULL) FOR [dtar_fei]
GO
ALTER TABLE [CAPEX].[tarea] ADD  DEFAULT (NULL) FOR [dtar_fef]
GO
ALTER TABLE [CAPEX].[tipo_documento] ADD  DEFAULT ('0') FOR [itdo_num]
GO
ALTER TABLE [CAPEX].[tipo_documento] ADD  DEFAULT ('1') FOR [ctdo_est]
GO
ALTER TABLE [CAPEX].[tipo_entidad] ADD  DEFAULT ('1') FOR [cten_est]
GO
ALTER TABLE [CAPEX].[tipo_proyecto] ADD  DEFAULT ('1') FOR [ctpr_est]
GO
ALTER TABLE [CAPEX].[tipo_sustento] ADD  DEFAULT ('1') FOR [ctsu_est]
GO
ALTER TABLE [CAPEX].[usuario] ADD  CONSTRAINT [DF__usuario__dusu_fe__32E0915F]  DEFAULT (getdate()) FOR [dusu_fec]
GO
ALTER TABLE [CAPEX].[usuario] ADD  CONSTRAINT [DF__usuario__dusu_fe__33D4B598]  DEFAULT (NULL) FOR [dusu_fem]
GO
ALTER TABLE [CAPEX].[usuario] ADD  CONSTRAINT [DF__usuario__iusu_us__34C8D9D1]  DEFAULT (NULL) FOR [iusu_usm]
GO
ALTER TABLE [CAPEX].[usuario] ADD  CONSTRAINT [DF__usuario__cusu_es__35BCFE0A]  DEFAULT ('1') FOR [cusu_est]
GO
ALTER TABLE [CAPEX].[usuario] ADD  CONSTRAINT [DF__usuario__vusu_te__36B12243]  DEFAULT (NULL) FOR [vusu_tel]
GO
ALTER TABLE [CAPEX].[usuario] ADD  CONSTRAINT [DF__usuario__dusu_fu__37A5467C]  DEFAULT (NULL) FOR [dusu_fua]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT ('') FOR [NumeroOportunidad]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT ((0)) FOR [ImporteCartaFianzaSoles]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT ((0)) FOR [ImporteCartaFianzaDolares]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT ((0)) FOR [IdBanco]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT ((0)) FOR [IdUsuarioCreacion]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT (getdate()) FOR [FechaCreacion]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianza] ADD  DEFAULT (NULL) FOR [FechaEdicion]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaColaboradorDetalle] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaDetalleAcciones] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaDetalleEstado] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaDetalleRecupero] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaDetalleRenovacion] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaDetalleSeguimiento] ADD  DEFAULT ((0)) FOR [Vigencia]
GO
ALTER TABLE [CARTAFIANZA].[CartaFianzaDetalleSeguimiento] ADD  DEFAULT ((0)) FOR [Eliminado]
GO
ALTER TABLE [COMUN].[Banco] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [COMUN].[Cliente2] ADD  DEFAULT ((0)) FOR [IdTipoIdentificadorFiscalTm]
GO
ALTER TABLE [COMUN].[Cliente2] ADD  DEFAULT ('') FOR [NumeroIdentificadorFiscal]
GO
ALTER TABLE [COMUN].[Cliente2] ADD  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [COMUN].[Colaboradores] ADD  DEFAULT ('') FOR [IdentificadorColaborador]
GO
ALTER TABLE [COMUN].[Colaboradores] ADD  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [COMUN].[Colaboradores] ADD  DEFAULT ((0)) FOR [IdEstado]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [IdOportunidad]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Formato]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [EstadoCpi]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Fase]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [IdCliente]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Cliente]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [TipoSolucion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Complejidad]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Licitacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [IngePreventa]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [LiderJefeProyecto]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [JefeProyecto]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaGanada]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPvRegistra]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPvAsignaLjp1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaLjpObservaPv1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPvAsignaLjp2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaLjpObservaPv2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPvAsignaLjp3]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaLjpInformaAf]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAfObservaPv1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPvInformaAf1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAfObservaPv2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPvInformaAf2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAfInformaControl1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaControlObservaAf1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAfInformaControl2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaControlObservaAf2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAfInformaControl3]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [ControlInformaPresupuestado]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaKickoffInicioDe]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaKickoffInicioHasta]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaInicialPreimplantacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaFinalPreimplantacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaInicialImplantacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaFinalImplantacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinalImplantacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal1]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal3]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal4]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal5]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal6]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal7]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal8]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal9]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal0]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaInicialProvicion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaFinalProvicion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaCierre]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [PasePosventa]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaInicialContratacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaFinalContratacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaPrevistaFacturacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaFinalFacturacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Monto]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [PagoUnico]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [PagoRecurrente]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [NroMesesPagoRecurrente]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [TipoCambio]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Opex]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Capex]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [ControlerOpex]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [ControlerCapex]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [AnalistaFinanciero]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [EstadoSuspencion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaSuspencion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaFinSuspencion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Van]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [VanVai]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Obida]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [MargenObida]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [PayBack]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaContratacionServicio]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [TipoProyecto]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [NroCaso]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaKickOff]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaActaCierra]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaRegistroActaCierra]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [LiderPreventa]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaNuevoInicioContratacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaNuevoFinContratacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [PendienteActa]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [TipoProyecto2]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [ProyectoEstado]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaEjecucion]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [EstadoPasePostVenta]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [ConfirmacionPaseFinalizado]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [FechaKickoffReal]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [CodProyectoMilestone]
GO
ALTER TABLE [PLANTAEXTERNA].[Proyecto] ADD  DEFAULT (NULL) FOR [Ruc]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [IdOportunidad]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Formato]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [EstadoCpi]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Fase]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [IdCliente]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Cliente]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [TipoSolucion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Complejidad]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Licitacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [IngePreventa]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [LiderJefeProyecto]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [JefeProyecto]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaGanada]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPvRegistra]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPvAsignaLjp1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaLjpObservaPv1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPvAsignaLjp2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaLjpObservaPv2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPvAsignaLjp3]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaLjpInformaAf]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAfObservaPv1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPvInformaAf1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAfObservaPv2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPvInformaAf2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAfInformaControl1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaControlObservaAf1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAfInformaControl2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaControlObservaAf2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAfInformaControl3]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [ControlInformaPresupuestado]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaKickoffInicioDe]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaKickoffInicioHasta]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaInicialPreimplantacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaFinalPreimplantacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaInicialImplantacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaFinalImplantacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinalImplantacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal1]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal3]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal4]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal5]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal6]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal7]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal8]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal9]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaAnteriorFinal0]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaInicialProvicion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaFinalProvicion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaCierre]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [PasePosventa]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaInicialContratacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaFinalContratacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaPrevistaFacturacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaFinalFacturacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Monto]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [PagoUnico]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [PagoRecurrente]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [NroMesesPagoRecurrente]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [TipoCambio]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Opex]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Capex]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [ControlerOpex]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [ControlerCapex]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [AnalistaFinanciero]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [EstadoSuspencion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaSuspencion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaFinSuspencion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Van]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [VanVai]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Obida]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [MargenObida]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [PayBack]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaContratacionServicio]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [TipoProyecto]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [NroCaso]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaKickOff]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaActaCierra]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaRegistroActaCierra]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [LiderPreventa]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaNuevoInicioContratacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaNuevoFinContratacion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [PendienteActa]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [TipoProyecto2]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [ProyectoEstado]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaEjecucion]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [EstadoPasePostVenta]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [ConfirmacionPaseFinalizado]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [FechaKickoffReal]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [CodProyectoMilestone]
GO
ALTER TABLE [PROYECTO].[ProyectoSF] ADD  DEFAULT (NULL) FOR [Ruc]
GO
ALTER TABLE [CAPEX].[accion_estrategica]  WITH CHECK ADD  CONSTRAINT [accion_estrategica_ibfk_1] FOREIGN KEY([ilin_id])
REFERENCES [CAPEX].[linea_negocio] ([ilin_id])
GO
ALTER TABLE [CAPEX].[accion_estrategica] CHECK CONSTRAINT [accion_estrategica_ibfk_1]
GO
ALTER TABLE [CAPEX].[auditoria]  WITH CHECK ADD  CONSTRAINT [aud_acc] FOREIGN KEY([iacc_id])
REFERENCES [CAPEX].[accion] ([iacc_id])
GO
ALTER TABLE [CAPEX].[auditoria] CHECK CONSTRAINT [aud_acc]
GO
ALTER TABLE [CAPEX].[auditoria]  WITH CHECK ADD  CONSTRAINT [aud_sca] FOREIGN KEY([isca_id])
REFERENCES [CAPEX].[solicitud_capex] ([isca_id])
GO
ALTER TABLE [CAPEX].[auditoria] CHECK CONSTRAINT [aud_sca]
GO
ALTER TABLE [CAPEX].[auditoria]  WITH CHECK ADD  CONSTRAINT [aud_usu] FOREIGN KEY([iusu_id])
REFERENCES [CAPEX].[usuario] ([iusu_id])
GO
ALTER TABLE [CAPEX].[auditoria] CHECK CONSTRAINT [aud_usu]
GO
ALTER TABLE [CAPEX].[boton]  WITH CHECK ADD  CONSTRAINT [boton_ibfk_1] FOREIGN KEY([ibot_pad])
REFERENCES [CAPEX].[boton] ([ibot_id])
GO
ALTER TABLE [CAPEX].[boton] CHECK CONSTRAINT [boton_ibfk_1]
GO
ALTER TABLE [CAPEX].[boton_perfil]  WITH CHECK ADD  CONSTRAINT [boton_perfil_ibfk_1] FOREIGN KEY([ibot_id])
REFERENCES [CAPEX].[boton] ([ibot_id])
GO
ALTER TABLE [CAPEX].[boton_perfil] CHECK CONSTRAINT [boton_perfil_ibfk_1]
GO
ALTER TABLE [CAPEX].[boton_perfil]  WITH CHECK ADD  CONSTRAINT [boton_perfil_ibfk_2] FOREIGN KEY([iprf_id])
REFERENCES [CAPEX].[perfil] ([iprf_id])
GO
ALTER TABLE [CAPEX].[boton_perfil] CHECK CONSTRAINT [boton_perfil_ibfk_2]
GO
ALTER TABLE [CAPEX].[concepto]  WITH CHECK ADD  CONSTRAINT [concepto_ibfk_1] FOREIGN KEY([idae_id])
REFERENCES [CAPEX].[descripcion_accion_estrategica] ([idae_id])
GO
ALTER TABLE [CAPEX].[concepto] CHECK CONSTRAINT [concepto_ibfk_1]
GO
ALTER TABLE [CAPEX].[detalle]  WITH CHECK ADD  CONSTRAINT [det_pro] FOREIGN KEY([ipro_id])
REFERENCES [CAPEX].[producto] ([ipro_id])
GO
ALTER TABLE [CAPEX].[detalle] CHECK CONSTRAINT [det_pro]
GO
ALTER TABLE [CAPEX].[detalle]  WITH CHECK ADD  CONSTRAINT [det_sca] FOREIGN KEY([isca_id])
REFERENCES [CAPEX].[solicitud_capex] ([isca_id])
GO
ALTER TABLE [CAPEX].[detalle] CHECK CONSTRAINT [det_sca]
GO
ALTER TABLE [CAPEX].[observacion]  WITH CHECK ADD  CONSTRAINT [observacion_ibfk_2] FOREIGN KEY([isca_id])
REFERENCES [CAPEX].[solicitud_capex] ([isca_id])
GO
ALTER TABLE [CAPEX].[observacion] CHECK CONSTRAINT [observacion_ibfk_2]
GO
ALTER TABLE [CAPEX].[observacion]  WITH CHECK ADD  CONSTRAINT [observacion_ibfk_3] FOREIGN KEY([iusu_id])
REFERENCES [CAPEX].[usuario] ([iusu_id])
GO
ALTER TABLE [CAPEX].[observacion] CHECK CONSTRAINT [observacion_ibfk_3]
GO
ALTER TABLE [CAPEX].[producto]  WITH CHECK ADD  CONSTRAINT [pro_ae2] FOREIGN KEY([iae2_id])
REFERENCES [CAPEX].[accion_estrategica] ([iaes_id])
GO
ALTER TABLE [CAPEX].[producto] CHECK CONSTRAINT [pro_ae2]
GO
ALTER TABLE [CAPEX].[producto]  WITH CHECK ADD  CONSTRAINT [pro_aes] FOREIGN KEY([iaes_id])
REFERENCES [CAPEX].[accion_estrategica] ([iaes_id])
GO
ALTER TABLE [CAPEX].[producto] CHECK CONSTRAINT [pro_aes]
GO
ALTER TABLE [CAPEX].[producto]  WITH CHECK ADD  CONSTRAINT [pro_con] FOREIGN KEY([icon_id])
REFERENCES [CAPEX].[concepto] ([icon_id])
GO
ALTER TABLE [CAPEX].[producto] CHECK CONSTRAINT [pro_con]
GO
ALTER TABLE [CAPEX].[producto]  WITH CHECK ADD  CONSTRAINT [pro_des] FOREIGN KEY([ides_id])
REFERENCES [CAPEX].[descripcion] ([ides_id])
GO
ALTER TABLE [CAPEX].[producto] CHECK CONSTRAINT [pro_des]
GO
ALTER TABLE [CAPEX].[solicitud_capex]  WITH CHECK ADD  CONSTRAINT [sca_est] FOREIGN KEY([iest_id])
REFERENCES [CAPEX].[estado] ([iest_id])
GO
ALTER TABLE [CAPEX].[solicitud_capex] CHECK CONSTRAINT [sca_est]
GO
ALTER TABLE [CAPEX].[solicitud_capex]  WITH CHECK ADD  CONSTRAINT [sca_ten] FOREIGN KEY([iten_id])
REFERENCES [CAPEX].[tipo_entidad] ([iten_id])
GO
ALTER TABLE [CAPEX].[solicitud_capex] CHECK CONSTRAINT [sca_ten]
GO
ALTER TABLE [CAPEX].[solicitud_capex]  WITH CHECK ADD  CONSTRAINT [sca_usu] FOREIGN KEY([iusu_id])
REFERENCES [CAPEX].[usuario] ([iusu_id])
GO
ALTER TABLE [CAPEX].[solicitud_capex] CHECK CONSTRAINT [sca_usu]
GO
ALTER TABLE [CAPEX].[sustento]  WITH CHECK ADD  CONSTRAINT [sus_sca] FOREIGN KEY([isca_id])
REFERENCES [CAPEX].[solicitud_capex] ([isca_id])
GO
ALTER TABLE [CAPEX].[sustento] CHECK CONSTRAINT [sus_sca]
GO
ALTER TABLE [CAPEX].[sustento]  WITH CHECK ADD  CONSTRAINT [sus_tsu] FOREIGN KEY([itsu_id])
REFERENCES [CAPEX].[tipo_sustento] ([itsu_id])
GO
ALTER TABLE [CAPEX].[sustento] CHECK CONSTRAINT [sus_tsu]
GO
ALTER TABLE [CAPEX].[sustento]  WITH CHECK ADD  CONSTRAINT [sus_usu] FOREIGN KEY([iusu_id])
REFERENCES [CAPEX].[usuario] ([iusu_id])
GO
ALTER TABLE [CAPEX].[sustento] CHECK CONSTRAINT [sus_usu]
GO
ALTER TABLE [CAPEX].[tarea]  WITH CHECK ADD  CONSTRAINT [tar_sca] FOREIGN KEY([isca_id])
REFERENCES [CAPEX].[solicitud_capex] ([isca_id])
GO
ALTER TABLE [CAPEX].[tarea] CHECK CONSTRAINT [tar_sca]
GO
ALTER TABLE [CAPEX].[tarea]  WITH CHECK ADD  CONSTRAINT [tar_tar] FOREIGN KEY([itar_tar])
REFERENCES [CAPEX].[tarea] ([itar_id])
GO
ALTER TABLE [CAPEX].[tarea] CHECK CONSTRAINT [tar_tar]
GO
ALTER TABLE [CAPEX].[usuario]  WITH CHECK ADD  CONSTRAINT [usu_prf] FOREIGN KEY([iprf_id])
REFERENCES [CAPEX].[perfil] ([iprf_id])
GO
ALTER TABLE [CAPEX].[usuario] CHECK CONSTRAINT [usu_prf]
GO
ALTER TABLE [CAPEX].[usuario]  WITH CHECK ADD  CONSTRAINT [usu_usc] FOREIGN KEY([iusu_usc])
REFERENCES [CAPEX].[usuario] ([iusu_id])
GO
ALTER TABLE [CAPEX].[usuario] CHECK CONSTRAINT [usu_usc]
GO
ALTER TABLE [CAPEX].[usuario]  WITH CHECK ADD  CONSTRAINT [usu_usm] FOREIGN KEY([iusu_usm])
REFERENCES [CAPEX].[usuario] ([iusu_id])
GO
ALTER TABLE [CAPEX].[usuario] CHECK CONSTRAINT [usu_usm]
GO
ALTER TABLE [COMUN].[CasoNegocio]  WITH CHECK ADD  CONSTRAINT [FK_CasoNegocio_LineaProducto] FOREIGN KEY([IdLineaNegocio])
REFERENCES [COMUN].[LineaNegocio] ([IdLineaNegocio])
GO
ALTER TABLE [COMUN].[CasoNegocio] CHECK CONSTRAINT [FK_CasoNegocio_LineaProducto]
GO
ALTER TABLE [COMUN].[CasoNegocioServicio]  WITH CHECK ADD  CONSTRAINT [FK_CasoNegocioServicio_CasoNegocio] FOREIGN KEY([IdCasoNegocio])
REFERENCES [COMUN].[CasoNegocio] ([IdCasoNegocio])
GO
ALTER TABLE [COMUN].[CasoNegocioServicio] CHECK CONSTRAINT [FK_CasoNegocioServicio_CasoNegocio]
GO
ALTER TABLE [COMUN].[CasoNegocioServicio]  WITH CHECK ADD  CONSTRAINT [FK_CasoNegocioServicio_Servicio] FOREIGN KEY([IdServicio])
REFERENCES [COMUN].[Servicio] ([IdServicio])
GO
ALTER TABLE [COMUN].[CasoNegocioServicio] CHECK CONSTRAINT [FK_CasoNegocioServicio_Servicio]
GO
ALTER TABLE [COMUN].[Cliente2]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_DireccionComercial] FOREIGN KEY([IdDireccionComercial])
REFERENCES [COMUN].[DireccionComercial] ([IdDireccion])
GO
ALTER TABLE [COMUN].[Cliente2] CHECK CONSTRAINT [FK_Cliente_DireccionComercial]
GO
ALTER TABLE [COMUN].[Cliente2]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Sectores] FOREIGN KEY([IdSector])
REFERENCES [COMUN].[Sector] ([IdSector])
GO
ALTER TABLE [COMUN].[Cliente2] CHECK CONSTRAINT [FK_Clientes_Sectores]
GO
ALTER TABLE [COMUN].[MedioCosto]  WITH CHECK ADD  CONSTRAINT [FK_MedioCosto_Costo] FOREIGN KEY([IdCosto])
REFERENCES [COMUN].[Costo] ([IdCosto])
GO
ALTER TABLE [COMUN].[MedioCosto] CHECK CONSTRAINT [FK_MedioCosto_Costo]
GO
ALTER TABLE [COMUN].[MedioCosto]  WITH CHECK ADD  CONSTRAINT [FK_MedioCosto_Medio] FOREIGN KEY([IdMedio])
REFERENCES [COMUN].[Medio] ([IdMedio])
GO
ALTER TABLE [COMUN].[MedioCosto] CHECK CONSTRAINT [FK_MedioCosto_Medio]
GO
ALTER TABLE [COMUN].[Servicio]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_LineaNegocio] FOREIGN KEY([IdLineaNegocio])
REFERENCES [COMUN].[LineaNegocio] ([IdLineaNegocio])
GO
ALTER TABLE [COMUN].[Servicio] CHECK CONSTRAINT [FK_Servicio_LineaNegocio]
GO
ALTER TABLE [COMUN].[Servicio]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_Medio] FOREIGN KEY([IdMedio])
REFERENCES [COMUN].[Medio] ([IdMedio])
GO
ALTER TABLE [COMUN].[Servicio] CHECK CONSTRAINT [FK_Servicio_Medio]
GO
ALTER TABLE [COMUN].[Servicio]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_PestanaGrupo] FOREIGN KEY([IdGrupo])
REFERENCES [OPORTUNIDAD].[PestanaGrupo] ([IdGrupo])
GO
ALTER TABLE [COMUN].[Servicio] CHECK CONSTRAINT [FK_Servicio_PestanaGrupo]
GO
ALTER TABLE [COMUN].[ServicioCMI]  WITH CHECK ADD  CONSTRAINT [FK_ServicioCMI_GerenciaProducto] FOREIGN KEY([IdGerenciaProducto])
REFERENCES [COMUN].[GerenciaProducto] ([IdGerenciaProducto])
GO
ALTER TABLE [COMUN].[ServicioCMI] CHECK CONSTRAINT [FK_ServicioCMI_GerenciaProducto]
GO
ALTER TABLE [COMUN].[ServicioCMI]  WITH CHECK ADD  CONSTRAINT [FK_ServicioCMI_LineaNegocio] FOREIGN KEY([IdLinea])
REFERENCES [COMUN].[Linea] ([IdLinea])
GO
ALTER TABLE [COMUN].[ServicioCMI] CHECK CONSTRAINT [FK_ServicioCMI_LineaNegocio]
GO
ALTER TABLE [COMUN].[ServicioCMI]  WITH CHECK ADD  CONSTRAINT [FK_ServiciosCMI_CentroCosto] FOREIGN KEY([IdCentroCosto])
REFERENCES [COMUN].[CentroCosto] ([IdCentroCosto])
GO
ALTER TABLE [COMUN].[ServicioCMI] CHECK CONSTRAINT [FK_ServiciosCMI_CentroCosto]
GO
ALTER TABLE [COMUN].[ServicioSubServicio]  WITH CHECK ADD  CONSTRAINT [FK_ServicioSubServicio_PestanaGrupo] FOREIGN KEY([IdGrupo])
REFERENCES [OPORTUNIDAD].[PestanaGrupo] ([IdGrupo])
GO
ALTER TABLE [COMUN].[ServicioSubServicio] CHECK CONSTRAINT [FK_ServicioSubServicio_PestanaGrupo]
GO
ALTER TABLE [COMUN].[ServicioSubServicio]  WITH CHECK ADD  CONSTRAINT [FK_ServicioSubServicio_Proveedor] FOREIGN KEY([IdProveedor])
REFERENCES [COMUN].[Proveedor] ([IdProveedor])
GO
ALTER TABLE [COMUN].[ServicioSubServicio] CHECK CONSTRAINT [FK_ServicioSubServicio_Proveedor]
GO
ALTER TABLE [COMUN].[ServicioSubServicio]  WITH CHECK ADD  CONSTRAINT [FK_ServicioSubServicio_Servicio] FOREIGN KEY([IdServicio])
REFERENCES [COMUN].[Servicio] ([IdServicio])
GO
ALTER TABLE [COMUN].[ServicioSubServicio] CHECK CONSTRAINT [FK_ServicioSubServicio_Servicio]
GO
ALTER TABLE [COMUN].[ServicioSubServicio]  WITH CHECK ADD  CONSTRAINT [FK_ServicioSubServicio_SubServicio] FOREIGN KEY([IdSubServicio])
REFERENCES [COMUN].[SubServicio] ([IdSubServicio])
GO
ALTER TABLE [COMUN].[ServicioSubServicio] CHECK CONSTRAINT [FK_ServicioSubServicio_SubServicio]
GO
ALTER TABLE [COMUN].[SubLinea]  WITH CHECK ADD  CONSTRAINT [FK_SubLineaNegocio_LineaNegocio] FOREIGN KEY([IdLinea])
REFERENCES [COMUN].[Linea] ([IdLinea])
GO
ALTER TABLE [COMUN].[SubLinea] CHECK CONSTRAINT [FK_SubLineaNegocio_LineaNegocio]
GO
ALTER TABLE [COMUN].[TipoCambio]  WITH CHECK ADD  CONSTRAINT [FK_TipoCambioLinea_LineaProducto] FOREIGN KEY([IdLineaNegocio])
REFERENCES [COMUN].[LineaNegocio] ([IdLineaNegocio])
GO
ALTER TABLE [COMUN].[TipoCambio] CHECK CONSTRAINT [FK_TipoCambioLinea_LineaProducto]
GO
ALTER TABLE [COMUN].[TipoCambioDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TipoCambioDetalle_TipoCambio] FOREIGN KEY([IdTipoCambio])
REFERENCES [COMUN].[TipoCambio] ([IdTipoCambio])
GO
ALTER TABLE [COMUN].[TipoCambioDetalle] CHECK CONSTRAINT [FK_TipoCambioDetalle_TipoCambio]
GO
ALTER TABLE [OPORTUNIDAD].[FormulaDetalle]  WITH CHECK ADD  CONSTRAINT [FK_FormulaDetalle_Formula] FOREIGN KEY([IdFormula])
REFERENCES [OPORTUNIDAD].[Formula] ([IdFormula])
GO
ALTER TABLE [OPORTUNIDAD].[FormulaDetalle] CHECK CONSTRAINT [FK_FormulaDetalle_Formula]
GO
ALTER TABLE [OPORTUNIDAD].[LineaNegocioCMI]  WITH CHECK ADD  CONSTRAINT [FK_LineaProductoServicioCMI_LineaProducto] FOREIGN KEY([IdLineaNegocio])
REFERENCES [COMUN].[LineaNegocio] ([IdLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[LineaNegocioCMI] CHECK CONSTRAINT [FK_LineaProductoServicioCMI_LineaProducto]
GO
ALTER TABLE [OPORTUNIDAD].[LineaNegocioCMI]  WITH CHECK ADD  CONSTRAINT [FK_LineaProductoServicioCMI_ServicioCMI] FOREIGN KEY([IdServicioCMI])
REFERENCES [COMUN].[ServicioCMI] ([IdServicioCMI])
GO
ALTER TABLE [OPORTUNIDAD].[LineaNegocioCMI] CHECK CONSTRAINT [FK_LineaProductoServicioCMI_ServicioCMI]
GO
ALTER TABLE [OPORTUNIDAD].[LineaPestana]  WITH CHECK ADD  CONSTRAINT [FK_LineaPestana_LineaNegocio] FOREIGN KEY([IdLineaNegocio])
REFERENCES [COMUN].[LineaNegocio] ([IdLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[LineaPestana] CHECK CONSTRAINT [FK_LineaPestana_LineaNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[Oportunidad]  WITH CHECK ADD  CONSTRAINT [FK_Proyecto_Clientes] FOREIGN KEY([IdCliente])
REFERENCES [COMUN].[Cliente2] ([IdCliente])
GO
ALTER TABLE [OPORTUNIDAD].[Oportunidad] CHECK CONSTRAINT [FK_Proyecto_Clientes]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadCosto]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadCosto_Costo] FOREIGN KEY([IdCosto])
REFERENCES [COMUN].[Costo] ([IdCosto])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadCosto] CHECK CONSTRAINT [FK_OportunidadCosto_Costo]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadCosto]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadCosto_OportunidadLineaNegocio] FOREIGN KEY([IdOportunidadLineaNegocio])
REFERENCES [OPORTUNIDAD].[OportunidadLineaNegocio] ([IdOportunidadLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadCosto] CHECK CONSTRAINT [FK_OportunidadCosto_OportunidadLineaNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadDocumento]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadDocumento_OportunidadLineaNegocio] FOREIGN KEY([IdOportunidadLineaNegocio])
REFERENCES [OPORTUNIDAD].[OportunidadLineaNegocio] ([IdOportunidadLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadDocumento] CHECK CONSTRAINT [FK_OportunidadDocumento_OportunidadLineaNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadFlujoCaja_CasoNegocio] FOREIGN KEY([IdCasoNegocio])
REFERENCES [COMUN].[CasoNegocio] ([IdCasoNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja] CHECK CONSTRAINT [FK_OportunidadFlujoCaja_CasoNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadFlujoCaja_OportunidadLineaNegocio] FOREIGN KEY([IdOportunidadLineaNegocio])
REFERENCES [OPORTUNIDAD].[OportunidadLineaNegocio] ([IdOportunidadLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja] CHECK CONSTRAINT [FK_OportunidadFlujoCaja_OportunidadLineaNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadFlujoCaja_Servicio] FOREIGN KEY([IdServicio])
REFERENCES [COMUN].[Servicio] ([IdServicio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja] CHECK CONSTRAINT [FK_OportunidadFlujoCaja_Servicio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja]  WITH CHECK ADD  CONSTRAINT [FK_ProyectosServiciosConcepto_Proveedores] FOREIGN KEY([IdProveedor])
REFERENCES [COMUN].[Proveedor] ([IdProveedor])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCaja] CHECK CONSTRAINT [FK_ProyectosServiciosConcepto_Proveedores]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadFlujoCajaConfiguracion_OportunidadFlujoCaja1] FOREIGN KEY([IdFlujoCaja])
REFERENCES [OPORTUNIDAD].[OportunidadFlujoCaja] ([IdFlujoCaja])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] CHECK CONSTRAINT [FK_OportunidadFlujoCajaConfiguracion_OportunidadFlujoCaja1]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoEstado]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadFlujoEstado_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [OPORTUNIDAD].[Oportunidad] ([IdOportunidad])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadFlujoEstado] CHECK CONSTRAINT [FK_OportunidadFlujoEstado_Oportunidad]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadLineaNegocio]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoLineaProducto_LineaProducto] FOREIGN KEY([IdLineaNegocio])
REFERENCES [COMUN].[LineaNegocio] ([IdLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadLineaNegocio] CHECK CONSTRAINT [FK_ProyectoLineaProducto_LineaProducto]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadLineaNegocio]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoLineaProducto_Proyecto] FOREIGN KEY([IdOportunidad])
REFERENCES [OPORTUNIDAD].[Oportunidad] ([IdOportunidad])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadLineaNegocio] CHECK CONSTRAINT [FK_ProyectoLineaProducto_Proyecto]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadServicioCMI]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadServicioCMI_OportunidadLineaNegocio] FOREIGN KEY([IdOportunidadLineaNegocio])
REFERENCES [OPORTUNIDAD].[OportunidadLineaNegocio] ([IdOportunidadLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadServicioCMI] CHECK CONSTRAINT [FK_OportunidadServicioCMI_OportunidadLineaNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadServicioCMI]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoServiciosCMI_ServiciosCMI] FOREIGN KEY([IdServicioCMI])
REFERENCES [COMUN].[ServicioCMI] ([IdServicioCMI])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadServicioCMI] CHECK CONSTRAINT [FK_ProyectoServiciosCMI_ServiciosCMI]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadTipoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadTipoCambio_OportunidadLineaNegocio] FOREIGN KEY([IdOportunidadLineaNegocio])
REFERENCES [OPORTUNIDAD].[OportunidadLineaNegocio] ([IdOportunidadLineaNegocio])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadTipoCambio] CHECK CONSTRAINT [FK_OportunidadTipoCambio_OportunidadLineaNegocio]
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadTipoCambioDetalle]  WITH CHECK ADD  CONSTRAINT [FK_OportunidadTipoCambioDetalle_OportunidadTipoCambio] FOREIGN KEY([IdTipoCambioOportunidad])
REFERENCES [OPORTUNIDAD].[OportunidadTipoCambio] ([IdTipoCambioOportunidad])
GO
ALTER TABLE [OPORTUNIDAD].[OportunidadTipoCambioDetalle] CHECK CONSTRAINT [FK_OportunidadTipoCambioDetalle_OportunidadTipoCambio]
GO
ALTER TABLE [OPORTUNIDAD].[PestanaGrupo]  WITH CHECK ADD  CONSTRAINT [FK_PestanaGrupo_LineaPestana] FOREIGN KEY([IdPestana])
REFERENCES [OPORTUNIDAD].[LineaPestana] ([IdPestana])
GO
ALTER TABLE [OPORTUNIDAD].[PestanaGrupo] CHECK CONSTRAINT [FK_PestanaGrupo_LineaPestana]
GO
ALTER TABLE [OPORTUNIDAD].[Requerimiento]  WITH CHECK ADD  CONSTRAINT [FK_Oportunidad_Requerimiento] FOREIGN KEY([IdOportunidad])
REFERENCES [OPORTUNIDAD].[Oportunidad] ([IdOportunidad])
GO
ALTER TABLE [OPORTUNIDAD].[Requerimiento] CHECK CONSTRAINT [FK_Oportunidad_Requerimiento]
GO
ALTER TABLE [OPORTUNIDAD].[ServicioSubServicioGrupo]  WITH CHECK ADD  CONSTRAINT [FK_LineaConceptoGrupo_LineaConceptoCMI] FOREIGN KEY([IdLineaNegocio], [IdConcepto], [IdPestana])
REFERENCES [OPORTUNIDAD].[ServicioSubServicioCMI] ([IdLineaNegocio], [IdSubServicio], [IdPestana])
GO
ALTER TABLE [OPORTUNIDAD].[ServicioSubServicioGrupo] CHECK CONSTRAINT [FK_LineaConceptoGrupo_LineaConceptoCMI]
GO
ALTER TABLE [OPORTUNIDAD].[SubServicioDatosCapex]  WITH CHECK ADD  CONSTRAINT [FK_SubServicioDatosCapex_OportunidadFlujoCaja] FOREIGN KEY([IdFlujoCaja])
REFERENCES [OPORTUNIDAD].[OportunidadFlujoCaja] ([IdFlujoCaja])
GO
ALTER TABLE [OPORTUNIDAD].[SubServicioDatosCapex] CHECK CONSTRAINT [FK_SubServicioDatosCapex_OportunidadFlujoCaja]
GO
ALTER TABLE [OPORTUNIDAD].[SubServicioDatosCaratula]  WITH CHECK ADD  CONSTRAINT [FK_SubServicioDatosCaratula_OportunidadFlujoCaja] FOREIGN KEY([IdFlujoCaja])
REFERENCES [OPORTUNIDAD].[OportunidadFlujoCaja] ([IdFlujoCaja])
GO
ALTER TABLE [OPORTUNIDAD].[SubServicioDatosCaratula] CHECK CONSTRAINT [FK_SubServicioDatosCaratula_OportunidadFlujoCaja]
GO
ALTER TABLE [PLANTAEXTERNA].[SegProyecto]  WITH CHECK ADD  CONSTRAINT [FK_SegProyecto_Sisego] FOREIGN KEY([idSisego])
REFERENCES [PLANTAEXTERNA].[Sisego] ([IdSisego])
GO
ALTER TABLE [PLANTAEXTERNA].[SegProyecto] CHECK CONSTRAINT [FK_SegProyecto_Sisego]
GO
ALTER TABLE [PLANTAEXTERNA].[SegProyecto]  WITH CHECK ADD  CONSTRAINT [FK_SegProyecto_TipoCuadrante] FOREIGN KEY([IdTipoCuadrante])
REFERENCES [PLANTAEXTERNA].[TipoCuadrante] ([IdTipoCuadrante])
GO
ALTER TABLE [PLANTAEXTERNA].[SegProyecto] CHECK CONSTRAINT [FK_SegProyecto_TipoCuadrante]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_AccionEstrategica] FOREIGN KEY([IdAccionEstrategica])
REFERENCES [PLANTAEXTERNA].[AccionEstrategica] ([IdAccionEstrategica])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_AccionEstrategica]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [COMUN].[Cliente2] ([IdCliente])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_Cliente]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_EstadoEjecucion] FOREIGN KEY([IdEstadoEjecucion])
REFERENCES [PLANTAEXTERNA].[EstadoEjecucion] ([IdEstadoEjecucion])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_EstadoEjecucion]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_EstadoImplementacion] FOREIGN KEY([IdEstadoImplementacion])
REFERENCES [PLANTAEXTERNA].[EstadoImplementacion] ([IdEstadoImplementacion])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_EstadoImplementacion]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_EstadoSisego] FOREIGN KEY([IdEstadoSisego])
REFERENCES [PLANTAEXTERNA].[EstadoSisego] ([IdEstadoSisego])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_EstadoSisego]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_Proyecto] FOREIGN KEY([IdProyecto])
REFERENCES [PLANTAEXTERNA].[Proyecto] ([IdProyecto])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_Proyecto]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_tipo_entidad] FOREIGN KEY([IdTipoEntidad])
REFERENCES [CAPEX].[tipo_entidad] ([iten_id])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_tipo_entidad]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_TipoCriticidad] FOREIGN KEY([IdTipoCriticidad])
REFERENCES [PLANTAEXTERNA].[TipoCriticidad] ([IdTipoCriticidad])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_TipoCriticidad]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_TipoDecisionProyecto] FOREIGN KEY([IdTipoDecicionProyecto])
REFERENCES [PLANTAEXTERNA].[TipoDecisionProyecto] ([IdTipoDecisionProyecto])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_TipoDecisionProyecto]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_TipoEntidad] FOREIGN KEY([IdTipoEntidad])
REFERENCES [PLANTAEXTERNA].[TipoEntidad] ([IdTipoEntidad])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_TipoEntidad]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_TipoProyecto] FOREIGN KEY([IdTipoProyecto])
REFERENCES [PLANTAEXTERNA].[TipoProyecto] ([IdTipoProyecto])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_TipoProyecto]
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego]  WITH CHECK ADD  CONSTRAINT [FK_Sisego_TipoRequerimiento] FOREIGN KEY([IdTipoRequerimiento])
REFERENCES [PLANTAEXTERNA].[TipoRequerimiento] ([IdTipoRequerimiento])
GO
ALTER TABLE [PLANTAEXTERNA].[Sisego] CHECK CONSTRAINT [FK_Sisego_TipoRequerimiento]
GO
ALTER TABLE [PLANTAEXTERNA].[TrabajoRed]  WITH CHECK ADD  CONSTRAINT [FK_TrabajoRed_Sisego] FOREIGN KEY([idSisego])
REFERENCES [PLANTAEXTERNA].[Sisego] ([IdSisego])
GO
ALTER TABLE [PLANTAEXTERNA].[TrabajoRed] CHECK CONSTRAINT [FK_TrabajoRed_Sisego]
GO
ALTER TABLE [PLANTAEXTERNA].[TrabajoRed]  WITH CHECK ADD  CONSTRAINT [FK_TrabajoRed_TipoTrabajoRed] FOREIGN KEY([IdTipoTrabajoRed])
REFERENCES [PLANTAEXTERNA].[TipoTrabajoRed] ([IdTipoTrabajoRed])
GO
ALTER TABLE [PLANTAEXTERNA].[TrabajoRed] CHECK CONSTRAINT [FK_TrabajoRed_TipoTrabajoRed]
GO
ALTER TABLE [TRAZABILIDAD].[ActividadOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_ActividadOportunidad_AreaSeguimiento] FOREIGN KEY([IdAreaSeguimiento])
REFERENCES [TRAZABILIDAD].[AreaSeguimiento] ([IdAreaSeguimiento])
GO
ALTER TABLE [TRAZABILIDAD].[ActividadOportunidad] CHECK CONSTRAINT [FK_ActividadOportunidad_AreaSeguimiento]
GO
ALTER TABLE [TRAZABILIDAD].[ActividadSegmentoNegocio]  WITH CHECK ADD  CONSTRAINT [FK_ActividadSegmentoNegocio_ActividadOportunidad] FOREIGN KEY([IdActividad])
REFERENCES [TRAZABILIDAD].[ActividadOportunidad] ([IdActividad])
GO
ALTER TABLE [TRAZABILIDAD].[ActividadSegmentoNegocio] CHECK CONSTRAINT [FK_ActividadSegmentoNegocio_ActividadOportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[ActividadSegmentoNegocio]  WITH CHECK ADD  CONSTRAINT [FK_ActividadSegmentoNegocio_SegmentoNegocio] FOREIGN KEY([IdSegmentoNegocio])
REFERENCES [TRAZABILIDAD].[SegmentoNegocio] ([IdSegmentoNegocio])
GO
ALTER TABLE [TRAZABILIDAD].[ActividadSegmentoNegocio] CHECK CONSTRAINT [FK_ActividadSegmentoNegocio_SegmentoNegocio]
GO
ALTER TABLE [TRAZABILIDAD].[AreaSegmentoNegocio]  WITH CHECK ADD  CONSTRAINT [FK_AreaSegmentoNegocio_AreaSeguimiento] FOREIGN KEY([IdAreaSeguimiento])
REFERENCES [TRAZABILIDAD].[AreaSeguimiento] ([IdAreaSeguimiento])
GO
ALTER TABLE [TRAZABILIDAD].[AreaSegmentoNegocio] CHECK CONSTRAINT [FK_AreaSegmentoNegocio_AreaSeguimiento]
GO
ALTER TABLE [TRAZABILIDAD].[AreaSegmentoNegocio]  WITH CHECK ADD  CONSTRAINT [FK_AreaSegmentoNegocio_SegmentoNegocio] FOREIGN KEY([IdSegmentoNegocio])
REFERENCES [TRAZABILIDAD].[SegmentoNegocio] ([IdSegmentoNegocio])
GO
ALTER TABLE [TRAZABILIDAD].[AreaSegmentoNegocio] CHECK CONSTRAINT [FK_AreaSegmentoNegocio_SegmentoNegocio]
GO
ALTER TABLE [TRAZABILIDAD].[CasoOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_CasoOportunidad_EstadoCaso] FOREIGN KEY([IdEstadoCaso])
REFERENCES [TRAZABILIDAD].[EstadoCaso] ([IdEstadoCaso])
GO
ALTER TABLE [TRAZABILIDAD].[CasoOportunidad] CHECK CONSTRAINT [FK_CasoOportunidad_EstadoCaso]
GO
ALTER TABLE [TRAZABILIDAD].[CasoOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_CasoOportunidad_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [TRAZABILIDAD].[OportunidadST] ([IdOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[CasoOportunidad] CHECK CONSTRAINT [FK_CasoOportunidad_Oportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[CasoOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_CasoOportunidad_TipoSolicitud] FOREIGN KEY([IdTipoSolicitud])
REFERENCES [TRAZABILIDAD].[TipoSolicitud] ([IdTipoSolicitud])
GO
ALTER TABLE [TRAZABILIDAD].[CasoOportunidad] CHECK CONSTRAINT [FK_CasoOportunidad_TipoSolicitud]
GO
ALTER TABLE [TRAZABILIDAD].[DatosPreventaMovil]  WITH CHECK ADD  CONSTRAINT [FK_DatosPreventaMovil_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [TRAZABILIDAD].[OportunidadST] ([IdOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[DatosPreventaMovil] CHECK CONSTRAINT [FK_DatosPreventaMovil_Oportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[DetalleActividadOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_SeguimientoActividad_ActividadOportunidad] FOREIGN KEY([IdActividad])
REFERENCES [TRAZABILIDAD].[ActividadOportunidad] ([IdActividad])
GO
ALTER TABLE [TRAZABILIDAD].[DetalleActividadOportunidad] CHECK CONSTRAINT [FK_SeguimientoActividad_ActividadOportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[DetalleActividadOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_SeguimientoActividad_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [TRAZABILIDAD].[OportunidadST] ([IdOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[DetalleActividadOportunidad] CHECK CONSTRAINT [FK_SeguimientoActividad_Oportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[DocumentoAdjunto]  WITH CHECK ADD  CONSTRAINT [FK_DocumentoAdjunto_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [TRAZABILIDAD].[OportunidadST] ([IdOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[DocumentoAdjunto] CHECK CONSTRAINT [FK_DocumentoAdjunto_Oportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[EtapaOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_EtapaOportunidad_FaseOportunidad] FOREIGN KEY([IdFase])
REFERENCES [TRAZABILIDAD].[FaseOportunidad] ([IdFase])
GO
ALTER TABLE [TRAZABILIDAD].[EtapaOportunidad] CHECK CONSTRAINT [FK_EtapaOportunidad_FaseOportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[FaseOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_FaseOportunidad_GrupoFaseOportunidad] FOREIGN KEY([IdGrupoFase])
REFERENCES [TRAZABILIDAD].[GrupoFaseOportunidad] ([IdGrupoFase])
GO
ALTER TABLE [TRAZABILIDAD].[FaseOportunidad] CHECK CONSTRAINT [FK_FaseOportunidad_GrupoFaseOportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[FileDocumentoAdjunto]  WITH CHECK ADD  CONSTRAINT [FK_FileDocumentoAdjunto_DocumentoAdjunto] FOREIGN KEY([IdDocAdjunto])
REFERENCES [TRAZABILIDAD].[DocumentoAdjunto] ([IdDocAdjunto])
GO
ALTER TABLE [TRAZABILIDAD].[FileDocumentoAdjunto] CHECK CONSTRAINT [FK_FileDocumentoAdjunto_DocumentoAdjunto]
GO
ALTER TABLE [TRAZABILIDAD].[FuncionPropietario]  WITH CHECK ADD  CONSTRAINT [FK_FuncionPropietario_Sector] FOREIGN KEY([IdSector])
REFERENCES [COMUN].[Sector] ([IdSector])
GO
ALTER TABLE [TRAZABILIDAD].[FuncionPropietario] CHECK CONSTRAINT [FK_FuncionPropietario_Sector]
GO
ALTER TABLE [TRAZABILIDAD].[ObservacionOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_ObservacionOportunidad_ConceptoSeguimiento] FOREIGN KEY([IdConcepto])
REFERENCES [TRAZABILIDAD].[ConceptoSeguimiento] ([IdConcepto])
GO
ALTER TABLE [TRAZABILIDAD].[ObservacionOportunidad] CHECK CONSTRAINT [FK_ObservacionOportunidad_ConceptoSeguimiento]
GO
ALTER TABLE [TRAZABILIDAD].[ObservacionOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_ObservacionOportunidad_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [TRAZABILIDAD].[OportunidadST] ([IdOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[ObservacionOportunidad] CHECK CONSTRAINT [FK_ObservacionOportunidad_Oportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[OportunidadST]  WITH CHECK ADD  CONSTRAINT [FK_Oportunidad_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [COMUN].[Cliente2] ([IdCliente])
GO
ALTER TABLE [TRAZABILIDAD].[OportunidadST] CHECK CONSTRAINT [FK_Oportunidad_Cliente]
GO
ALTER TABLE [TRAZABILIDAD].[OportunidadST]  WITH CHECK ADD  CONSTRAINT [FK_Oportunidad_MotivoOportunidad] FOREIGN KEY([IdMotivoOportunidad])
REFERENCES [TRAZABILIDAD].[MotivoOportunidad] ([IdMotivoOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[OportunidadST] CHECK CONSTRAINT [FK_Oportunidad_MotivoOportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[OportunidadST]  WITH CHECK ADD  CONSTRAINT [FK_Oportunidad_TipoOportunidad] FOREIGN KEY([IdTipoOportunidad])
REFERENCES [TRAZABILIDAD].[TipoOportunidad] ([IdTipoOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[OportunidadST] CHECK CONSTRAINT [FK_Oportunidad_TipoOportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[RecursoEquipoTrabajo]  WITH CHECK ADD  CONSTRAINT [FK_RecursoEquipoTrabajo_Recurso] FOREIGN KEY([IdRecurso])
REFERENCES [TRAZABILIDAD].[Recurso] ([IdRecurso])
GO
ALTER TABLE [TRAZABILIDAD].[RecursoEquipoTrabajo] CHECK CONSTRAINT [FK_RecursoEquipoTrabajo_Recurso]
GO
ALTER TABLE [TRAZABILIDAD].[RecursoOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_RecursoOportunidad_Oportunidad] FOREIGN KEY([IdOportunidad])
REFERENCES [TRAZABILIDAD].[OportunidadST] ([IdOportunidad])
GO
ALTER TABLE [TRAZABILIDAD].[RecursoOportunidad] CHECK CONSTRAINT [FK_RecursoOportunidad_Oportunidad]
GO
ALTER TABLE [TRAZABILIDAD].[RecursoOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_RecursoOportunidad_Recurso] FOREIGN KEY([IdRecurso])
REFERENCES [TRAZABILIDAD].[Recurso] ([IdRecurso])
GO
ALTER TABLE [TRAZABILIDAD].[RecursoOportunidad] CHECK CONSTRAINT [FK_RecursoOportunidad_Recurso]
GO
ALTER TABLE [TRAZABILIDAD].[RecursoOportunidad]  WITH CHECK ADD  CONSTRAINT [FK_RecursoOportunidad_RolRecurso] FOREIGN KEY([IdRol])
REFERENCES [TRAZABILIDAD].[RolRecurso] ([IdRol])
GO
ALTER TABLE [TRAZABILIDAD].[RecursoOportunidad] CHECK CONSTRAINT [FK_RecursoOportunidad_RolRecurso]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id propia de la tabla, correlativo.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Para el Caso del registro de Oportunidades, se asocia con la tabla oportunidadLineaNegocio:
IdRelacion1 = IdOportunidad.
IdRelacion2 = IdLineaNegocio.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'IdTabla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Para el Caso del registro de Oportunidades, se asocia con la tabla ProyectoLineaProducto:
IdRelacion1 = IdProyecto.
IdRelacion2 = IdLineaProducto.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'IdTablaRegistro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de Documento:
1. Oportunidad.
2. Cotizacion.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'IdTipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del Documento.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción opcional del documento que el usuario puede ingresar.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ruta del Documento.
1. Desarrollo:
    \\tgpesvlcli1054\Compartido\STF\Oportunidades' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Archivo', @level2type=N'COLUMN',@level2name=N'Ruta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RUC' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Cliente2', @level2type=N'COLUMN',@level2name=N'CodigoCliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1. Costos Sonda. (Modelo)
2. Costos FentoCelda.
3. Costos Ancho de Banda.
4. Costos de Comisiones.
5. Costos de Equipos de Fibra.
6. Costos Adicionales.
7. Costos asociados a Internet.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Costo', @level2type=N'COLUMN',@level2name=N'IdTipoCosto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IDRELACION: 101.
 * 102: Costo Fentocelda.
 * 103: Velocidades.
 * 104: Costo BGAN.
 * 105: Costo ISAT PHONE2.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Costo', @level2type=N'COLUMN',@level2name=N'IdTipificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maestra: IdRelacion = 89' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'Servicio', @level2type=N'COLUMN',@level2name=N'IdTipoEnlace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IdRelación: 153.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'ServicioSubServicio', @level2type=N'COLUMN',@level2name=N'IdTipoCosto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maestra: IDRELACION = 28' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'ServicioSubServicio', @level2type=N'COLUMN',@level2name=N'IdPeriodos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica el número de mes en el que se iniciará el pago.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'ServicioSubServicio', @level2type=N'COLUMN',@level2name=N'Inicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maestra, IdRelacion: 98' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'ServicioSubServicio', @level2type=N'COLUMN',@level2name=N'IdMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maestra IdRelación: 32, el tipo de sub-servicio 3 hace referencia a los Servicios' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'SubServicio', @level2type=N'COLUMN',@level2name=N'IdTipoSubServicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla maestra IDRELACION = 98. Define la Moneda.' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'TipoCambioDetalle', @level2type=N'COLUMN',@level2name=N'IdMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla maestra IDRELACION = 58. Define el agrupador (Ingresos, Costos, Capex).' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'TipoCambioDetalle', @level2type=N'COLUMN',@level2name=N'IdTipificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Asociada a la tabla Maestra. Dolares / Euros' , @level0type=N'SCHEMA',@level0name=N'COMUN', @level1type=N'TABLE',@level1name=N'TipoCambioDetalle', @level2type=N'COLUMN',@level2name=N'Anio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cmi_Venta: 1
Cmi_Alquiler: 2
Linea: 3
Cmi : 4' , @level0type=N'SCHEMA',@level0name=N'FUNNEL', @level1type=N'TABLE',@level1name=N'OportunidadPmoLotusLinea', @level2type=N'COLUMN',@level2name=N'Tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hace referencia a la agrupación por formula.
A, B, C, D, E, F, G, H, I, J, etc....
Todos los registros que pertenezcan al mismo Componente, se suman.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'FormulaDetalle', @level2type=N'COLUMN',@level2name=N'Componente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Referencia al Sub-Servicio que suma.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'FormulaDetalle', @level2type=N'COLUMN',@level2name=N'IdSubServicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maestra: Id Relación 19.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'Oportunidad', @level2type=N'COLUMN',@level2name=N'IdTipoEmpresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IdRelacion: 7' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'Oportunidad', @level2type=N'COLUMN',@level2name=N'IdTipoProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IdRelacion: 14' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'Oportunidad', @level2type=N'COLUMN',@level2name=N'IdTipoServicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la Oportunidad con la versión 1.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'Oportunidad', @level2type=N'COLUMN',@level2name=N'Agrupador'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Define la Moneda. Tabla maestra IDRELACION = 98.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'Oportunidad', @level2type=N'COLUMN',@level2name=N'IdMonedaFacturacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1. Costos Sonda. (Modelo)
2. Costos FentoCelda.
3. Costos Ancho de Banda.
4. Costos de Comisiones.
5. Costos de Equipos de Fibra.
6. Costos Adicionales.
7. Costos asociados a Internet.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadCosto', @level2type=N'COLUMN',@level2name=N'IdTipoCosto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IDRELACION: 101.
 * 102: Costo Fentocelda.
 * 103: Velocidades.
 * 104: Costo BGAN.
 * 105: Costo ISAT PHONE2.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadCosto', @level2type=N'COLUMN',@level2name=N'IdTipificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Asociado a la tabla Maestra: Costos(OPEX) / CAPEX' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadDocumento', @level2type=N'COLUMN',@level2name=N'TipoDocumento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Asociado a la tabla Maestra: Costos / Evidencia.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadDocumento', @level2type=N'COLUMN',@level2name=N'IdTipoDocumento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IdRelacion: 4. Asociada a la tabla Maestra: Costo Terceros / Costo Propios' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadFlujoCaja', @level2type=N'COLUMN',@level2name=N'IdTipoCosto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SELECT * FROM MAESTRA WHERE IDRELACION = 28' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadFlujoCaja', @level2type=N'COLUMN',@level2name=N'IdPeriodos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este campo debe almacenar el costo unitario de los sub-servicios.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadFlujoCaja', @level2type=N'COLUMN',@level2name=N'CostoUnitario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maestra, IdRelacion: 98' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadFlujoCaja', @level2type=N'COLUMN',@level2name=N'IdMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IdRelación: 11' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadFlujoEstado', @level2type=N'COLUMN',@level2name=N'IdEstado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Define el agrupador (Ingresos, Costos, Capex). Tabla maestra IDRELACION = 58.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadTipoCambioDetalle', @level2type=N'COLUMN',@level2name=N'IdTipificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Asociada a la tabla Maestra. Dolares / Euros' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'OportunidadTipoCambioDetalle', @level2type=N'COLUMN',@level2name=N'Anio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla Maestra: IDRELACION = 89' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCapex', @level2type=N'COLUMN',@level2name=N'IdTipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla Maestra, id relación = 84.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'IdMedio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla Maestra, id relación = 89.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'IdTipoEnlace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla Maestra, id relación = 92.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'IdActivoPasivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla Maestra, id relación = 95.' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'IdLocalidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formula excel: BUSCARV(CARATULA!I35,CC!$E$5:$G$243,3,0)' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'CC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formula excel: =SI(D5="Provincia",I5*$J$3,I5)' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'CCQProvincia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formula excel: =SI(F5="Pasivo",J5*$J$2,J5)' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'CCQBK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formula excel: =REDONDEAR((H5*K5)*$L$3,0)' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'CCQCAPEX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formula excel: =SI(Y(E5="Principal",B5="INFOINTERNET")=VERDADERO,BUSCARV(G5,CC!$E$6:$F$243,2,0),0)' , @level0type=N'SCHEMA',@level0name=N'OPORTUNIDAD', @level1type=N'TABLE',@level1name=N'SubServicioDatosCaratula', @level2type=N'COLUMN',@level2name=N'TIWS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 : SI, 0 : NO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'ActividadOportunidad', @level2type=N'COLUMN',@level2name=N'FlgCapexMayor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 : SI, 0 : NO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'ActividadOportunidad', @level2type=N'COLUMN',@level2name=N'FlgCapexMenor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Servirá para tipificar las actividades en: 1 - Tarea, 2 - Agrupadora, 3 - Hito, 4 - Entregable.' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'ActividadOportunidad', @level2type=N'COLUMN',@level2name=N'IdTipoActividad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Servirá para configurar sólo las actividades que servirán para validación o seguimiento prioritario para aseguramiento de la oportunidad.' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'ActividadOportunidad', @level2type=N'COLUMN',@level2name=N'AsegurarOferta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de Solicitud de Caso' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'CasoHistoricoSF', @level2type=N'COLUMN',@level2name=N'TipoCaso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DURACION ESTIMADA DE LA ACTIVIDAD EN JORNADAS' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'DuracionEstimada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRABAJO ESTIMADO DE LA ACTIVIDAD EN JORNADAS' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'TrabajoEstimado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FECHA DE INICIO ESTIMADA: Fecha de Apertura de oportunidad más la cantidad de días configurados según catalogo de actividades.' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'FechaInicioEstimada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FECHA FIN ESTIMADA: Se obtendrá en base en base a la Fecha Inico Seguimiento más la cantidad de días configurados en el catálogo de actividades de seguimiento' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'FechaFinEstimada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CODIGOS DE ACTIVIDADES PREDECESORAS SEPARADAS POR COMAS' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'Predecesoras'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DURACION REAL DE LA ACTIVIDAD EN JORNADAS' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'DuracionReal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRABAJO REAL DE LA ACTIVIDAD EN JORNADAS' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'TrabajoReal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FECHA DE INICIO REAL' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'FechaInicioReal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FECHA FIN REAL' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'FechaFinReal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PORCENTAJE DE AVANCE ESTIMADO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'AvanceEstimado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PORCENTAJE DE AVANCE REAL' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'AvanceReal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Atendido, 0: Sin Atender, 2: En Proceso' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'IdEstadoEjecucion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:A Tiempo, 2:Vencido, 3:Por Vencerse' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'IdEstadoCumplimiento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CODIGO DEL RECURSO EJECUTOR RESPONSABLE' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'IdRecursoEjecutor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CODIGO DEL RECURSO REVISOR RESPONSABLE' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'IdRecursoRevisor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OBSERVACIONES X ACTIVIDAD' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad', @level2type=N'COLUMN',@level2name=N'Observaciones'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DETALLE DE ACTIVIDADES POR OPORTUNIDAD / PROYECTO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'DetalleActividadOportunidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DESCRIPCION DEL ARCHIVO ADJUNTO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'FileDocumentoAdjunto', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'URL O DIRECCION FISICA DEL ARCHIVO ADJUNTO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'FileDocumentoAdjunto', @level2type=N'COLUMN',@level2name=N'RutaFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TIPO DE EXTENSION DEL ARCHIVO ADJUNTO (1: PDF, 2: DOC, 3: XLS, 4: OTROS)' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'FileDocumentoAdjunto', @level2type=N'COLUMN',@level2name=N'TipoFile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'2:Pública o Privada:1' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'OportunidadST', @level2type=N'COLUMN',@level2name=N'IdTipoEntidadCliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SECTOR COMERCIAL' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'OportunidadST', @level2type=N'COLUMN',@level2name=N'IdSector'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Porcentaje CheckList Documental' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'OportunidadST', @level2type=N'COLUMN',@level2name=N'PorcentajeCheckList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CODIGO DE RECURSO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'Recurso', @level2type=N'COLUMN',@level2name=N'IdRecurso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'	TIPO DE RECURSO (1:Trabajo, 2:Material, 3:Costo)' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'Recurso', @level2type=N'COLUMN',@level2name=N'TipoRecurso'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NOMBRE COMPLETO DEL RECURSO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'Recurso', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Para indicar el código del sistema origen. Sales Force o DIO.' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'Recurso', @level2type=N'COLUMN',@level2name=N'IdOrigen'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Para indicar el código de la empresa a la que pertenece el recurso. TGSC, TDP, etc.' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'Recurso', @level2type=N'COLUMN',@level2name=N'IdEmpresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de Grupo Preventa' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RecursoEquipoTrabajo', @level2type=N'COLUMN',@level2name=N'IdEquipoTrabajoNivel1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID EQUIPO PV TGESTIONA' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RecursoEquipoTrabajo', @level2type=N'COLUMN',@level2name=N'IdEquipoTrabajoNivel2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID 2 LINEA' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RecursoEquipoTrabajo', @level2type=N'COLUMN',@level2name=N'IdEquipoTrabajoNivel3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID EQUIPO DE SOPORTE' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RecursoEquipoTrabajo', @level2type=N'COLUMN',@level2name=N'IdEquipoTrabajoNivel4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DETALLE DE RECURSOS POR PROYECTO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RecursoOportunidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CODIGO DE ROL FUNCIONAL' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RolRecurso', @level2type=N'COLUMN',@level2name=N'IdRol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DESCRIPCION DEL ROL FUNCIONAL' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RolRecurso', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NIVEL DE MANDO' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RolRecurso', @level2type=N'COLUMN',@level2name=N'Nivel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ROLES FUNCIONALES' , @level0type=N'SCHEMA',@level0name=N'TRAZABILIDAD', @level1type=N'TABLE',@level1name=N'RolRecurso'
GO
