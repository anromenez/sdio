USE [DIO]
GO
/****** Object:  StoredProcedure [CARTAFIANZA].[USP_ListarCartaFianza]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Creado	:	13/08/2018
Autor	:	Edwin Murga
NombreSP	:	USP_ListarCartaFianza
Descripcion	:	El siguiente sp muestra la data para el listado principal de la Aplicación	

exec CARTAFIANZA.USP_ListarCartaFianza 1,0,0
*/

CREATE PROCEDURE [CARTAFIANZA].[USP_ListarCartaFianza](
@pIdCliente int,
@pIdCartaFianza int,
@pIcliente int
)
as
begin
	SET NOCOUNT ON;

	declare @vIdCliente int = @pIdCliente,
			@vIdCartaFianza int = @pIdCartaFianza,
			@vIcliente int = @pIcliente

if @pIdCliente = 0 and @pIdCartaFianza = 0
	begin 

		select
		cf.IdCartaFianza
		,cf.IdCliente
		,c.Descripcion as NombreCliente
		,cf.NumeroOportunidad
		,cf.IdTipoContratoTm
		,cf.NumeroContrato
		,cf.Processo
		,cf.DescripcionServicioCartaFianza
		,cf.FechaFirmaServicioContrato
		,cf.FechaFinServicioContrato
		,cf.IdEmpresaAdjudicadaTm
		,cf.IdTipoGarantiaTm
		,cf.NumeroGarantia
		,cf.IdTipoMonedaTm
		,cf.ImporteCartaFianzaSoles
		,cf.ImporteCartaFianzaDolares
		,cf.IdTipoAccionTm
		,cf.IdEstadoVencimientoTm
		,cf.IdEstadoCartaFianzaTm
		,cf.IdRenovarTm
		,cf.IdEstadoRecuperacionCartaFianzaTm
		,cf.ClienteEspecial
		,cf.SeguimientoImportante
		,cf.Observacion
		,cf.Incidencia
		,cf.IdEstado
		,cf.IdUsuarioCreacion
		,cf.FechaCreacion 
		from cartafianza.cartafianza cf	with(nolock)
		right join COMUN.Cliente c on cf.idcliente = c.idcliente 
		order by
			cf.FechaFinServicioContrato  asc 

	end
else
	begin

		select
		cf.IdCartaFianza
		,cf.IdCliente
		,c.Descripcion as NombreCliente
		,cf.NumeroOportunidad
		,cf.IdTipoContratoTm
		,cf.NumeroContrato
		,cf.Processo
		,cf.DescripcionServicioCartaFianza
		,cf.FechaFirmaServicioContrato
		,cf.FechaFinServicioContrato
		,cf.IdEmpresaAdjudicadaTm
		,cf.IdTipoGarantiaTm
		,cf.NumeroGarantia
		,cf.IdTipoMonedaTm
		,cf.ImporteCartaFianzaSoles
		,cf.ImporteCartaFianzaDolares
		,cf.IdTipoAccionTm
		,cf.IdEstadoVencimientoTm
		,cf.IdEstadoCartaFianzaTm
		,cf.IdRenovarTm
		,cf.IdEstadoRecuperacionCartaFianzaTm
		,cf.ClienteEspecial
		,cf.SeguimientoImportante
		,cf.Observacion
		,cf.Incidencia
		,cf.IdEstado
		,cf.IdUsuarioCreacion
		,cf.FechaCreacion 
		from cartafianza.cartafianza cf	with(nolock)
		right join COMUN.Cliente c on cf.idcliente = c.idcliente	
		where 
		 c.IdCliente = @vIdCliente --( case  when @pIdCliente = 0 then  c.IdCliente else @pIdCliente end )
			or cf.IdCartaFianza = @vIdCartaFianza --= ( case  when @pIdCartaFianza = 0 then  cf.IdCartaFianza else @pIdCartaFianza end )

			
		order by
				cf.FechaFinServicioContrato  asc 

	end
end



GO
/****** Object:  StoredProcedure [COMUN].[USP_CONSOLIDAR_SALESFORCE]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_CONSOLIDAR_SALESFORCE
Descripcion   : Actualiza el detalle y cabeceras de las tablas SalesForce
Parametros Ingreso : 
@IdCarga: Id de Carga en la DB
@LoginCarga: Login de Carga en la DB
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC [COMUN].[USP_CONSOLIDAR_SALESFORCE]
(
@IdCarga int,
@LoginCarga varchar(100)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	MERGE COMUN.[SalesForceConsolidadoDetalle] AS TARGET
	USING (
		SELECT S.*,C.IdCliente FROM COMUN.[SalesForceOrigen] S LEFT JOIN
		COMUN.Cliente C ON
		S.NombreCliente = C.Descripcion
		where IdCarga = @IdCarga) AS SOURCE
	ON (TARGET.IdOportunidad = SOURCE.IdOportunidad
			and CONVERT(INT,TARGET.NumeroDelCaso) = CONVERT(INT,SOURCE.NumeroDelCaso))
    WHEN MATCHED THEN
	UPDATE SET 
			TARGET.[PropietarioOportunidad] = SOURCE.PropietarioOportunidad, 
			TARGET.[TipologiaOportunidad] = SOURCE.TipologiaOportunidad, 
			TARGET.[NombreCliente] = SOURCE.NombreCliente, 
			TARGET.[NombreOportunidad] = SOURCE.NombreOportunidad, 
			TARGET.[FechaCreacion] = SOURCE.FechaCreacion, 
			TARGET.[FechaCierreEstimada] = SOURCE.FechaCierreEstimada, 
			TARGET.[FechaCierreReal] = SOURCE.FechaCierreReal, 
			TARGET.[ProbabilidadExito] = SOURCE.ProbabilidadExito, 
			TARGET.[Etapa] = SOURCE.Etapa, 
			TARGET.[TipoOportunidad] = SOURCE.TipoOportunidad, 
			TARGET.[PlazoEstimadoProvision] = SOURCE.PlazoEstimadoProvision, 
			TARGET.[FechaEstimadaInstalacionServicio] = SOURCE.FechaEstimadaInstalacionServicio, 
			TARGET.[DuracionContrato] = SOURCE.DuracionContrato, 
			TARGET.[IngresoPorUnicaVezDivisa] = SOURCE.IngresoPorUnicaVezDivisa, 
			TARGET.[IngresoPorUnicaVez] = SOURCE.IngresoPorUnicaVez, 
			TARGET.[FullContractValueNetoDivisa] = SOURCE.FullContractValueNetoDivisa, 
			TARGET.[FullContractValueNeto] = SOURCE.FullContractValueNeto, 
			TARGET.[RecurrenteBrutoMensualDivisa] = SOURCE.RecurrenteBrutoMensualDivisa, 
			TARGET.[RecurrenteBrutoMensual] = SOURCE.RecurrenteBrutoMensual, 
			TARGET.[Estado] = SOURCE.Estado, 
			TARGET.[Departamento] = SOURCE.Departamento, 
			TARGET.[TipoSolicitud] = SOURCE.TipoSolicitud, 
			TARGET.[Asunto] = SOURCE.Asunto, 
			TARGET.FechaEdicion = GETDATE(), 
			TARGET.[LoginUltimaModificacion] = @LoginCarga,
			TARGET.IdEstado = 1,
			TARGET.IdCliente = SOURCE.IdCliente
	WHEN NOT MATCHED THEN

			INSERT
			([IdOportunidad], [PropietarioOportunidad], [TipologiaOportunidad], 
			[NombreCliente], [NombreOportunidad], [FechaCreacion], [FechaCierreEstimada], 
			[FechaCierreReal], [ProbabilidadExito], [Etapa], [TipoOportunidad], 
			[PlazoEstimadoProvision], [FechaEstimadaInstalacionServicio], 
			[DuracionContrato], [IngresoPorUnicaVezDivisa], [IngresoPorUnicaVez], 
			[FullContractValueNetoDivisa], [FullContractValueNeto], [RecurrenteBrutoMensualDivisa], 
			[RecurrenteBrutoMensual], [NumeroDelCaso], [Estado], [Departamento], [TipoSolicitud], 
			[Asunto], FechaCreacionDB, FechaEdicion, [LoginRegistro], [LoginUltimaModificacion],
			IdEstado,IdCliente)
			VALUES(
			  SOURCE.[IdOportunidad]
			  ,SOURCE.[PropietarioOportunidad]
			  ,SOURCE.[TipologiaOportunidad]
			  ,SOURCE.[NombreCliente]
			  ,SOURCE.[NombreOportunidad]
			  ,SOURCE.[FechaCreacion]
			  ,SOURCE.[FechaCierreEstimada]
			  ,SOURCE.[FechaCierreReal]
			  ,SOURCE.[ProbabilidadExito]
			  ,SOURCE.[Etapa]
			  ,SOURCE.[TipoOportunidad]
			  ,SOURCE.[PlazoEstimadoProvision]
			  ,SOURCE.[FechaEstimadaInstalacionServicio]
			  ,SOURCE.[DuracionContrato]
			  ,SOURCE.[IngresoPorUnicaVezDivisa]
			  ,SOURCE.[IngresoPorUnicaVez]
			  ,SOURCE.[FullContractValueNetoDivisa]
			  ,SOURCE.[FullContractValueNeto]
			  ,SOURCE.[RecurrenteBrutoMensualDivisa]
			  ,SOURCE.[RecurrenteBrutoMensual]
			  ,SOURCE.[NumeroDelCaso]
			  ,SOURCE.[Estado]
			  ,SOURCE.[Departamento]
			  ,SOURCE.[TipoSolicitud]
			  ,SOURCE.[Asunto]
			  ,getdate()
			  ,null
			  ,@LoginCarga
			  ,null,
			  1,
			  IdCliente);


	MERGE COMUN.[SalesForceConsolidadoCabecera] AS TARGET
	USING (
	SELECT C.* FROM COMUN.[SalesForceConsolidadoDetalle] AS C INNER JOIN
	(
	SELECT IdOportunidad,MAX(CONVERT(INT,NumeroDelCaso)) AS NumeroDelCaso FROM COMUN.SalesForceConsolidadoDetalle
	GROUP BY IdOportunidad
	) AS T
	ON C.IdOportunidad = T.IdOportunidad AND CONVERT(INT,C.NumeroDelCaso) = CONVERT(INT,T.NumeroDelCaso)
	) AS SOURCE
	ON (TARGET.IdOportunidad = SOURCE.IdOportunidad)
	WHEN MATCHED AND CONVERT(INT,SOURCE.NumeroDelCaso) >= CONVERT(INT,TARGET.NumeroDelCaso)  THEN
	UPDATE SET 
			TARGET.NumeroDelCaso = SOURCE.NumeroDelCaso, 
			TARGET.PropietarioOportunidad = SOURCE.PropietarioOportunidad,
			TARGET.TipologiaOportunidad = SOURCE.TipologiaOportunidad,
			TARGET.NombreCliente = SOURCE.NombreCliente,
			TARGET.NombreOportunidad = SOURCE.NombreOportunidad,
			TARGET.FechaCreacion = SOURCE.FechaCreacion,
			TARGET.ProbabilidadExito = SOURCE.ProbabilidadExito,
			TARGET.Etapa = SOURCE.Etapa,
			TARGET.FechaCierreEstimada = SOURCE.FechaCierreEstimada,
			TARGET.Asunto = SOURCE.Asunto,
			TARGET.IdEstado = 1,
			TARGET.IdUsuarioEdicion = null,
			TARGET.LoginUltimaModificacion = @LoginCarga,
			TARGET.FechaEdicion = GETDATE()
	WHEN NOT MATCHED THEN
	INSERT ([IdOportunidad],[NumeroDelCaso],PropietarioOportunidad,TipologiaOportunidad,
			NombreCliente,NombreOportunidad,FechaCreacion,ProbabilidadExito,Etapa,FechaCierreEstimada,Asunto,
			FechaCreacionDB,LoginRegistro,IdEstado)
			 VALUES(
			 SOURCE.[IdOportunidad],
			 SOURCE.[NumeroDelCaso],
			 SOURCE.PropietarioOportunidad,
			 SOURCE.TipologiaOportunidad,
			 SOURCE.NombreCliente,
			 SOURCE.NombreOportunidad,
			 SOURCE.FechaCreacion,
			 SOURCE.ProbabilidadExito,
			 SOURCE.Etapa,
			 SOURCE.FechaCierreEstimada,
			 SOURCE.Asunto,
			 getdate(),
			 @LoginCarga,
			 1
			 );

END

GO
/****** Object:  StoredProcedure [COMUN].[USP_INSERTAR_SALESFORCEORIGEN]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_INSERTAR_SALESFORCEORIGEN
Descripcion   : Registro de Sales Force en el Sistema
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC [COMUN].[USP_INSERTAR_SALESFORCEORIGEN] 
    @IdOportunidad nvarchar(255) = NULL,
    @PropietarioOportunidad nvarchar(255) = NULL,
    @TipologiaOportunidad nvarchar(255) = NULL,
    @NombreCliente nvarchar(255) = NULL,
    @NombreOportunidad nvarchar(255) = NULL,
    @FechaCreacion datetime = NULL,
    @FechaCierreEstimada datetime = NULL,
    @FechaCierreReal datetime = NULL,
    @ProbabilidadExito nvarchar(255) = NULL,
    @Etapa nvarchar(255) = NULL,
    @TipoOportunidad nvarchar(255) = NULL,
    @PlazoEstimadoProvision int = NULL,
    @FechaEstimadaInstalacionServicio datetime = NULL,
    @DuracionContrato int = NULL,
    @IngresoPorUnicaVezDivisa nvarchar(255) = NULL,
    @IngresoPorUnicaVez nvarchar(255) = NULL,
    @FullContractValueNetoDivisa nvarchar(255) = NULL,
    @FullContractValueNeto nvarchar(255) = NULL,
    @RecurrenteBrutoMensualDivisa nvarchar(255) = NULL,
    @RecurrenteBrutoMensual nvarchar(255) = NULL,
	@CapexDivisa nvarchar(255) = NULL,
	@Capex nvarchar(255) = NULL,
    @NumeroDelCaso nvarchar(255) = NULL,
    @Estado nvarchar(255) = NULL,
    @Departamento nvarchar(255) = NULL,
    @TipoSolicitud nvarchar(255) = NULL,
    @Asunto nvarchar(255) = NULL,
	@FechaCarga datetime,
    @LoginCarga varchar(100) = NULL,
	@IdCarga int
AS 
	SET NOCOUNT ON 
	
	
	INSERT INTO COMUN.[SalesForceOrigen] ([IdOportunidad], [PropietarioOportunidad], [TipologiaOportunidad], [NombreCliente], [NombreOportunidad], [FechaCreacion], [FechaCierreEstimada], [FechaCierreReal], [ProbabilidadExito], [Etapa], [TipoOportunidad], [PlazoEstimadoProvision], [FechaEstimadaInstalacionServicio], [DuracionContrato], [IngresoPorUnicaVezDivisa], [IngresoPorUnicaVez], [FullContractValueNetoDivisa], [FullContractValueNeto], [RecurrenteBrutoMensualDivisa], [RecurrenteBrutoMensual], 
	CapexDivisa,Capex,[NumeroDelCaso], [Estado], [Departamento], [TipoSolicitud], [Asunto], [FechaCarga], [LoginCarga],IdCarga,
	IdEstado,FechaCreacionDB)
	SELECT @IdOportunidad, @PropietarioOportunidad, @TipologiaOportunidad, @NombreCliente, @NombreOportunidad, @FechaCreacion, @FechaCierreEstimada, @FechaCierreReal, @ProbabilidadExito, @Etapa, @TipoOportunidad, @PlazoEstimadoProvision, @FechaEstimadaInstalacionServicio, @DuracionContrato, @IngresoPorUnicaVezDivisa, @IngresoPorUnicaVez, @FullContractValueNetoDivisa, @FullContractValueNeto, @RecurrenteBrutoMensualDivisa, @RecurrenteBrutoMensual, 
	@CapexDivisa,@Capex,@NumeroDelCaso, @Estado, @Departamento, @TipoSolicitud, @Asunto, @FechaCarga, @LoginCarga,@IdCarga,
	1,GETDATE()

GO
/****** Object:  StoredProcedure [COMUN].[USP_INSERTAR_SALESFORCEORIGENBEGIN]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_INSERTAR_SALESFORCEORIGENBEGIN
Descripcion   : Historial de registro de SalesForce en el sistema
Parametros Ingreso : 
@FechaCarga: Fecha de carga en la DB
Parametros Retorno : 
@Id: Id de carga generado
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC [COMUN].[USP_INSERTAR_SALESFORCEORIGENBEGIN] 
    @FechaCarga datetime,
	@Id int output
AS 
	SET NOCOUNT ON 
	

	INSERT INTO COMUN.[SalesForceHistoriaCarga]
			   ([FechaCarga],IdEstado,FechaCreacion)
		 VALUES
			   (@FechaCarga,1,GETDATE())

	set @Id = SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [COMUN].[USP_LISTAR_LINEA_NEGOCIO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Lista de Lineas de Negocio
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
27/07/2018       jaguilar            Creacion
*/

CREATE PROC [COMUN].[USP_LISTAR_LINEA_NEGOCIO] 
(
@IdEstado INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT NULL AS VALOR, 'Todos' AS Descripcion
	UNION ALL
	SELECT [IdLineaNegocio] as VALOR
		  ,[Descripcion]
	  FROM [COMUN].[LineaNegocio] WHERE IdEstado = @IdEstado

END

GO
/****** Object:  StoredProcedure [COMUN].[USP_LISTAR_MAESTRA]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [COMUN].[USP_LISTAR_MAESTRA]
(
	@IdMaestra_Etapa INT = null,
	@IdMaestra_Madurez INT = null,
	@IdEstado INT
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@IdMaestra_Etapa IS NOT NULL)
	BEGIN
		SELECT 'Todos' AS DESCRIPCION,NULL AS VALOR
		UNION ALL
		SELECT 
       [Descripcion]
      ,[Valor]
       FROM [COMUN].[Maestra] 
	  WHERE IdRelacion = @IdMaestra_Etapa AND IdEstado = @IdEstado
	END

	IF (@IdMaestra_Madurez IS NOT NULL)
	BEGIN
		SELECT 'Todos' AS DESCRIPCION,NULL AS VALOR
		UNION ALL
		SELECT 
       [Descripcion]
      ,[Valor]
       FROM [COMUN].[Maestra] 
	  WHERE IdRelacion = @IdMaestra_Madurez AND IdEstado = @IdEstado
	END

	

END

GO
/****** Object:  StoredProcedure [COMUN].[USP_LISTAR_PROBABILIDADEXITO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Lista las probabilidades de exito
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
26/07/2018       jaguilar            Creacion
*/

CREATE PROC [COMUN].[USP_LISTAR_PROBABILIDADEXITO]
AS
BEGIN
	
	SELECT CONS.ProbabilidadExito,Descripcion FROM (
	SELECT NULL AS ProbabilidadExito,'Todos' as Descripcion
	UNION ALL
	SELECT DISTINCT CONVERT(INT,ProbabilidadExito),ProbabilidadExito
	  FROM [COMUN].[SalesForceConsolidadoCabecera]
	WHERE  IdEstado = 1
	) AS CONS
	ORDER BY CONVERT(NUMERIC(10,2),CONS.ProbabilidadExito)

END

GO
/****** Object:  StoredProcedure [COMUN].[USP_LISTAR_SECTOR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Lista de Lineas de Negocio
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
27/07/2018       jaguilar            Creacion
*/

CREATE PROC [COMUN].[USP_LISTAR_SECTOR] 
(
@IdEstado INT
)
AS
BEGIN

	SET NOCOUNT ON;

	 SELECT NULL AS IdSector, 'Todos' AS Descripcion
	 UNION ALL
	 SELECT [IdSector],[Descripcion] 
	 FROM [COMUN].[Sector] WHERE [IdEstado] = @IdEstado

END

GO
/****** Object:  StoredProcedure [COMUN].[USP_OBTENER_SALESFORCEVIGENTE]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Procedimiento : USP_OBTENER_SALESFORCEVIGENTE
Descripcion   : Identifica si la oportunidad esta disponible para analizar
Parametros Ingreso : 
@IdOportunidad: Id de la Oportunidad
Parametros Retorno : 
@existe: Esta registrada en SalesForce
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC [COMUN].[USP_OBTENER_SALESFORCEVIGENTE] 
(
@IdOportunidad varchar(255),
@existe BIT output
)
AS
BEGIN

SET NOCOUNT ON;

	  IF EXISTS(SELECT[IdOportunidad]
	  FROM COMUN.[SalesForceConsolidadoCabecera] p where not exists
		(
			SELECT [IdOportunidad]
		  FROM COMUN.[SalesForceConsolidadoCabecera] where [IdOportunidad] = p.[IdOportunidad] and Etapa
		  in
		  ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
		)
	  AND p.IdOportunidad = @IdOportunidad)
	  BEGIN
		SET @existe = 1
	  END
	   ELSE
	  BEGIN
	   SET @existe = 0
	  END

END

GO
/****** Object:  StoredProcedure [COMUN].[USP_ULTIMO_SALESFORCE_HISTORICOCARGAS]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : USP_SALESFORCE_HISTORICOCARGAS
Modificaciones    :
*************************************************************
Fecha           Autor              Accion
*************************************************************
25/07/2018      Juan Aguilar       Creacion
*/
CREATE PROC [COMUN].[USP_ULTIMO_SALESFORCE_HISTORICOCARGAS] 
(
	@ID INT OUTPUT
)
AS
BEGIN
	 SET NOCOUNT ON

	SELECT @ID = MAX(ID) FROM COMUN.[SalesForceHistoriaCarga]

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_ACTUALIZAR_OPORTUNIDAD_CMI_PROCESO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Actualiza Nombre de procesos por su Dato Cargado
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
01/08/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_ACTUALIZAR_OPORTUNIDAD_CMI_PROCESO]
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE CMI 
	SET CMI.Proceso = CONF.Proceso,
	FechaEdicion = GETDATE()
	FROM [FUNNEL].[OportunidadPmoLotusLinea] CMI
	INNER JOIN [FUNNEL].CmiConfiguracion CONF
	ON CMI.[DatosProceso] = CONF.DatosProceso
	WHERE CMI.IdEstado = 1

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALCABECERA]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_CONSULTAR_OPORTUNIDADPRINCIPALCABECERA
Descripcion   : Reporte de Indicador Principal - Cabecera
Parametros Ingreso : 
@Anio: AÑO
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALCABECERA]
(
@Anio int,
@Mes int = null
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@Mes = -1)
	BEGIN
		SET @Mes = NULL
	END;


	WITH cte AS
	(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MesOrden,CASE MONTH(S.[FechaCierreEstimada]) 
			   WHEN 1 THEN 'Ene'
			   WHEN 2 THEN 'Feb'
			   WHEN 3 THEN 'Mar'
			   WHEN 4 THEN 'Abr'
			   WHEN 5 THEN 'May'
			   WHEN 6 THEN 'Jun'
			   WHEN 7 THEN 'Jul'
			   WHEN 8 THEN 'Ago'
			   WHEN 9 THEN 'Set'
			   WHEN 10 THEN 'Oct'
			   WHEN 11 THEN 'Nov'
			   WHEN 12 THEN 'Dic'
			   END
		AS MES,MONTH(S.[FechaCierreEstimada]) AS MESPOSICION,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		O.IdOportunidad,O.Capex, S.ProbabilidadExito,S.FechaCierreEstimada,S.Asunto,S.NombreCliente
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		(	
			SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
			SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
			FROM FUNNEL.OportunidadContableOrigen
			WHERE IdEstado = 1
			GROUP BY IdOportunidad
		) O ON S.IdOportunidad = O.IdOportunidad 
		WHERE S.IdEstado = 1
	)
	SELECT ROW_NUMBER() OVER(PARTITION BY MesOrden ORDER BY convert(int,ProbabilidadExito)) * 0.05  + MesOrden AS MesOrden
	  ,[Mes]
      ,[Anio]
      ,[IdOportunidad]
      ,[Capex]
	  ,convert(int,ProbabilidadExito) as ProbabilidadExito
	  ,FechaCierreEstimada
	  ,Asunto
	  ,NombreCliente
	FROM cte
	WHERE ANIO = @Anio and (@Mes is null or MESPOSICION = @Mes)
	ORDER BY MesOrden

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALPIE]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
Procedimiento : USP_CONSULTAR_OPORTUNIDADPRINCIPALPIE
Descripcion   : Reporte de Indicador Principal - Pie
Parametros Ingreso : 
@Anio: AÑO
@Mes: MES
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALPIE]
(
@Anio int,
@Mes int = NULL
)
AS
BEGIN

	SET NOCOUNT ON 

	IF (@Mes = -1)
	BEGIN
		SET @Mes = NULL
	END


	DECLARE @INDICADORES_PIE TABLE (
	[Anio] [int],
	[Mes] [int],
	[SUMIngresoAnual] [numeric](10, 2),
	[SUMCapex] [numeric](10, 2),
	[SUMOibdaAnual] [numeric](10, 2),
	[OportTrabajadas] [int],
	[OportMaduras] [int],
	[OportGanadas] [int])

	INSERT INTO @INDICADORES_PIE
		SELECT 
		TOTALES.ANIO,TOTALES.MES,TOTALES.IngresoTotal,TOTALES.Capex,TOTALES.Oidba,
		ISNULL(TRABAJADAS.OPORT_TRABAJADAS,0) AS OPORT_TRABAJADAS,
		0,
		ISNULL(GANADAS.OPORT_GANADAS,0) AS OPORT_GANADAS FROM(
		SELECT C.MES,C.ANIO,SUM(C.IngresoTotal) AS IngresoTotal,SUM(C.Capex) AS Capex,SUM(C.Oidba) AS Oidba FROM(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		O.IngresoTotal, O.Capex,O.Oidba
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		(	
			SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
			SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
			FROM FUNNEL.OportunidadContableOrigen
			WHERE IdEstado = 1
			GROUP BY IdOportunidad
		) O ON S.IdOportunidad = O.IdOportunidad 
		WHERE (@Mes IS NULL OR MONTH(S.[FechaCierreEstimada]) = @Mes) AND YEAR(S.[FechaCierreEstimada]) = @Anio
		AND S.IdEstado = 1
		) AS C
		GROUP BY C.MES,C.ANIO
		) AS TOTALES
		LEFT JOIN 
		(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		COUNT(S.IdOportunidad) AS OPORT_TRABAJADAS
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
		WHERE NOT EXISTS
			(
				SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
				AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
				AND IdEstado = 1
			)
		AND (@Mes IS NULL OR MONTH(S.[FechaCierreEstimada]) = @Mes) AND YEAR(S.[FechaCierreEstimada]) = @Anio
		AND S.IdEstado = 1
		GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
		) AS TRABAJADAS
		ON TOTALES.ANIO = TRABAJADAS.ANIO AND TOTALES.MES = TRABAJADAS.MES
		LEFT JOIN 
		(
		SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
		COUNT(S.IdOportunidad) AS OPORT_GANADAS
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
		FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
		WHERE Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
		AND (@Mes IS NULL OR MONTH(S.[FechaCierreEstimada]) = @Mes) AND YEAR(S.[FechaCierreEstimada]) = @Anio
		AND S.IdEstado = 1 AND O.IdEstado = 1
		GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
		) AS GANADAS
		ON TOTALES.ANIO = GANADAS.ANIO AND TOTALES.MES = GANADAS.MES

	 
	 DECLARE @CANTI_FILAS INT
	 SELECT @CANTI_FILAS = COUNT(Anio)
	  FROM @INDICADORES_PIE where Anio = @Anio and (@Mes IS NULL OR Mes = @Mes)
	
	IF (@CANTI_FILAS = 1)
	BEGIN

	  SELECT 
		   Anio
		  ,Mes
		  ,SUMIngresoAnual
		  ,SUMCapex 
		  ,SUMOibdaAnual 
		  ,OportTrabajadas 
		  ,OportMaduras
		  ,OportGanadas
	   FROM @INDICADORES_PIE where Anio = @Anio and (@Mes IS NULL OR Mes = @Mes)

	END
	 ELSE
	BEGIN
		
		IF (@CANTI_FILAS > 1)
		BEGIN
			
			SELECT 
			   @Anio AS Anio
			  ,0 AS Mes
			  ,SUM(SUMIngresoAnual) AS SUMIngresoAnual
			  ,SUM(SUMCapex) AS SUMCapex 
			  ,SUM(SUMOibdaAnual) AS SUMOibdaAnual  
			  ,SUM(OportTrabajadas) AS OportTrabajadas  
			  ,SUM(OportMaduras) AS OportMaduras 
			  ,SUM(OportGanadas) AS OportGanadas 
		   FROM @INDICADORES_PIE where Anio = @Anio and (@Mes IS NULL OR Mes = @Mes)
			
		END
		 ELSE
		BEGIN

		 SELECT 0 AS SUMIngresoAnual
		  ,0 AS SUMCapex
		  ,0 AS SUMOibdaAnual
		  ,0 AS OportTrabajadas
		  ,0 AS OportMaduras
		  ,0 AS OportGanadas

		END

	END


END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALPOPUP]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Reporte popup del reporte de Probabilidad por AÑO/Mes
Parametros Ingreso : 
@IdOportunidad: Id de Oportunidad
Parametros Retorno : 
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
10/07/2018      jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_CONSULTAR_OPORTUNIDADPRINCIPALPOPUP]
(
@IdOportunidad VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @NombreCliente NVARCHAR(255),@Etapa NVARCHAR(255),@CodigoCliente VARCHAR(20),
	@DescripcionSector VARCHAR(50),@TotalCasos INT,@UltimoCaso VARCHAR(255)

	SELECT @NombreCliente = NombreCliente,@Etapa = LEFT(Etapa,2),@UltimoCaso = NumeroDelCaso
	FROM COMUN.SalesForceConsolidadoCabecera 
	WHERE IdOportunidad = @IdOportunidad and IdEstado = 1

	SELECT @TotalCasos = COUNT(NumeroDelCaso) FROM [COMUN].[SalesForceConsolidadoDetalle] 
	WHERE IdOportunidad = @IdOportunidad and IdEstado = 1

	SELECT @CodigoCliente = CodigoCliente,@DescripcionSector = SEC.Descripcion FROM [COMUN].[Cliente] CLI 
	LEFT OUTER JOIN [COMUN].[Sector] SEC
	ON CLI.IdSector = SEC.IdSector
	WHERE CLI.Descripcion = LTRIM(RTRIM(@NombreCliente)) 

	DECLARE @LineaNegocio VARCHAR(100)
	SELECT @LineaNegocio = LineaNegocio FROM [FUNNEL].[OportunidadFinancieraOrigen]
	WHERE IdOportunidad = @IdOportunidad

	DECLARE @MADUREZ INT
	SELECT @MADUREZ = CONVERT(numeric(10,2),MADUREZ) 
	FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(NULL,NULL,@IdOportunidad,NULL,NULL,NULL,NULL)

	SELECT 
	@LineaNegocio AS LineaNegocio,
	@NombreCliente AS NombreCliente,
	@CodigoCliente AS CodigoCliente,
	@MADUREZ AS MADUREZ,
	MAX(CONVERT(INT,NumeroCaso)) AS NumeroCaso,
	@Etapa as Etapa,
	SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
	SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX,
	SUM(VanProyecto) AS VanProyecto,
	@DescripcionSector AS DescripcionSector,
	@TotalCasos AS TotalCasos,
	@UltimoCaso AS UltimoCaso
	FROM FUNNEL.OportunidadContableOrigen
	WHERE IdEstado = 1 AND IdOportunidad = @IdOportunidad
	GROUP BY IdOportunidad

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_ELIMINAR_OPORTUNIDADPMOLOTUSLINEA]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_ELIMINAR_OPORTUNIDADPMOLOTUSLINEA
Descripcion   : Elimina grupos de CMI por IdOportunidad y NumeroCaso 
Parametros Ingreso : 
@IdOportunidad: Id de la Oportunidad
@NumeroCaso: Id del Caso
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      19/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_ELIMINAR_OPORTUNIDADPMOLOTUSLINEA] 
(
@IdOportunidad VARCHAR(100),
@NumeroCaso VARCHAR(100) = null
)
AS
BEGIN

	SET NOCOUNT ON

	DELETE FROM [FUNNEL].[OportunidadPmoLotusLinea] 
	WHERE IdOportunidad = @IdOportunidad AND (@NumeroCaso IS NULL OR NumeroCaso = @NumeroCaso)

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_CARGABILIDAD_LIDER]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Reporte de cargabilidad por lider
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      22/08/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_INDICADOR_CARGABILIDAD_LIDER]
AS
BEGIN
	
	SET NOCOUNT ON;
	
	
	SELECT C.Descripcion,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND ETAPA IN ('F2 - Gestión de Contratos')
	) AS OportunidadesAbiertas,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND NOT EXISTS
			(
				SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
				AND Etapa IN ('F1 - Cancelada | Suspendida') 
				AND IdEstado = 1
			)
	) AS OfertasVigentes,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND NOT EXISTS
			(
				SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
				AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida')  
				AND IdEstado = 1
			)
	) AS OfertasTrabajadas,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
	) AS OfertasGanadas
	FROM [COMUN].[Cliente] C
	WHERE [Descripcion] IN
	(
	SELECT NombreCliente FROM [COMUN].[SalesForceConsolidadoCabecera]
	)
	

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_COSTOS_OPORTUNIDAD]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Muestra la informacion del CMI en función del ultimo caso
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
01/08/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INDICADOR_COSTOS_OPORTUNIDAD]
(
	@IdOportunidadCostos VARCHAR(100),
	@PROCESO VARCHAR(100)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @NumeroCaso VARCHAR(100)

	SELECT @NumeroCaso = NumeroDelCaso FROM [COMUN].[SalesForceConsolidadoCabecera] 
	WHERE IdOportunidad = @IdOportunidadCostos

	SELECT DATOSPROCESO,SUM(CONVERT(numeric(10,2),DETALLEPROCESO)) DETALLEPROCESO
	FROM [FUNNEL].[OportunidadPmoLotusLinea] 
	WHERE PROCESO = @PROCESO 
	AND IdOportunidad = @IdOportunidadCostos 
	AND CONVERT(INT,NumeroCaso) = CONVERT(INT,@NumeroCaso)
	AND CONVERT(numeric(10,2),DETALLEPROCESO) > 0.00
	GROUP BY DATOSPROCESO
	ORDER BY DATOSPROCESO

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_DETALLE_OPORTUNIDAD_PREV]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Reporte de detalle de OPORTUNIDAD para preventa
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      22/08/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INDICADOR_DETALLE_OPORTUNIDAD_PREV]
(
	@Anio int
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	
SELECT C.Descripcion,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND ETAPA IN ('F2 - Gestión de Contratos')
	) AS OportunidadesAbiertas,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND NOT EXISTS
			(
				SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
				AND Etapa IN ('F1 - Cancelada | Suspendida') 
				AND IdEstado = 1
			)
	) AS OfertasVigentes,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND NOT EXISTS
			(
				SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
				AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida')  
				AND IdEstado = 1
			)
	) AS OfertasTrabajadas,
	(
		SELECT COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] S
		WHERE NombreCliente = C.Descripcion AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
	) AS OfertasGanadas
	FROM [COMUN].[Cliente] C
	WHERE [Descripcion] IN
	(
	SELECT NombreCliente FROM [COMUN].[SalesForceConsolidadoCabecera]
	WHERE YEAR([FechaCierreEstimada]) = @Anio
	AND IdEstado = 1
	)
	

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_EVOLUTIVO_OPORTUNIDAD_CONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : usp_IndicadorEvolutivoOportunidadConsultar
Descripcion   : Muestra la evolucion de los casos de una oportunidad
Parametros Ingreso : 
@Anio:
@Mes:
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      20/07/2018       jaguilar            Creacion
*/
CREATE PROC [FUNNEL].[USP_INDICADOR_EVOLUTIVO_OPORTUNIDAD_CONSULTAR]
(
	@IdOportunidad VARCHAR(255)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
	IdOportunidad,
	NumeroCaso,
	ISNULL(SUM(Capex * TipoCambioCapex),0) AS Capex,
	ISNULL(SUM(CostosDirectos * TipoCambioCostos),0) AS Opex,
	ISNULL(SUM(OibdaAnual),0) AS Oibda,
	ISNULL(SUM(IngresoTotal * TipoCambioIngresos),0) AS Ingresos 
	FROM [FUNNEL].[OportunidadContableOrigen]
	WHERE IDESTADO = 1 AND NumeroCaso IS NOT NULL
	AND IdOportunidad = @IdOportunidad
	GROUP BY IdOportunidad,NumeroCaso
	ORDER BY IdOportunidad,NumeroCaso

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_INGRESOLINEA_CONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Reporte de ingreso por Linea de Negocio
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
17/07/2018       jaguilar            Creacion
*/
CREATE PROC [FUNNEL].[USP_INDICADOR_INGRESOLINEA_CONSULTAR]
(
@Anio int,
@Mes int = null,
@Etapa char(2) = null,
@ProbabilidadExito INT = null,
@Madurez char(2) = null
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
	YEAR(S.FechaCierreEstimada) ANIO,
	CASE MONTH(S.FechaCierreEstimada) 
	WHEN 1 THEN 'Ene'
			WHEN 2 THEN 'Feb'
			WHEN 3 THEN 'Mar'
			WHEN 4 THEN 'Abr'
			WHEN 5 THEN 'May'
			WHEN 6 THEN 'Jun'
			WHEN 7 THEN 'Jul'
			WHEN 8 THEN 'Ago'
			WHEN 9 THEN 'Set'
			WHEN 10 THEN 'Oct'
			WHEN 11 THEN 'Nov'
			WHEN 12 THEN 'Dic' 
	END AS MES,
	F.LINEANEGOCIO,
	SUM(O.IngresoTotal) AS IngresoTotal 
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	(
		SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal
		FROM FUNNEL.OportunidadContableOrigen
		WHERE IdEstado = 1
		GROUP BY IdOportunidad
	) O ON S.IdOportunidad = O.IdOportunidad
	INNER JOIN FUNNEL.OportunidadFinancieraOrigen F
	ON S.IdOportunidad = F.IdOportunidad
	WHERE
	S.IdEstado = 1 AND F.IdEstado = 1	
	AND YEAR(S.FechaCierreEstimada) = @Anio
	AND (@Mes IS NULL OR MONTH(S.FechaCierreEstimada) = @Mes)	
	AND (@Etapa IS NULL OR LEFT(S.Etapa,2) = @Etapa)
	AND (@ProbabilidadExito IS NULL OR CONVERT(INT,S.ProbabilidadExito) = @ProbabilidadExito)		
	AND (@Madurez IS NULL OR @Madurez =
	( 
	SELECT 'N' + CONVERT(CHAR(1),MADUREZ)  FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(NULL,NULL,S.IdOportunidad,NULL,NULL,NULL,NULL) 
	)
	)	
	GROUP BY YEAR(S.FechaCierreEstimada),MONTH(S.FechaCierreEstimada),F.LINEANEGOCIO
	ORDER BY MONTH(S.FechaCierreEstimada)  

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_INGRESOSECTOR_CONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Reporte de ingreso por Sector
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
19/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_INDICADOR_INGRESOSECTOR_CONSULTAR] 
(
@Anio int,
@Mes int = null,
@Etapa char(2) = null,
@ProbabilidadExito INT = null,
@Madurez char(2) = null
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	YEAR(S.FechaCierreEstimada) ANIO,
	CASE MONTH(S.FechaCierreEstimada) 
			WHEN 1 THEN 'Ene'
			WHEN 2 THEN 'Feb'
			WHEN 3 THEN 'Mar'
			WHEN 4 THEN 'Abr'
			WHEN 5 THEN 'May'
			WHEN 6 THEN 'Jun'
			WHEN 7 THEN 'Jul'
			WHEN 8 THEN 'Ago'
			WHEN 9 THEN 'Set'
			WHEN 10 THEN 'Oct'
			WHEN 11 THEN 'Nov'
			WHEN 12 THEN 'Dic' 
	END AS MES,
	SEC.Descripcion AS SECTOR,
	SUM(O.IngresoTotal) AS IngresoTotal 
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	(
		SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal
		FROM FUNNEL.OportunidadContableOrigen
		WHERE IdEstado = 1
		GROUP BY IdOportunidad
	) O ON S.IdOportunidad = O.IdOportunidad
	INNER JOIN COMUN.Cliente CLI 
	ON S.NombreCliente = CLI.Descripcion
	INNER JOIN [COMUN].[Sector] SEC
	ON CLI.IdSector = SEC.IdSector
	WHERE
	S.IdEstado = 1 AND CLI.IdEstado = 1 AND SEC.IdEstado = 1
	AND YEAR(S.FechaCierreEstimada) = @Anio
	AND (@Mes IS NULL OR MONTH(S.FechaCierreEstimada) = @Mes)	
	AND (@Etapa IS NULL OR LEFT(S.Etapa,2) = @Etapa)
	AND (@ProbabilidadExito IS NULL OR CONVERT(INT,S.ProbabilidadExito) = @ProbabilidadExito)		
	AND (@Madurez IS NULL OR @Madurez =
	( 
	SELECT 'N' + CONVERT(CHAR(1),MADUREZ)  FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(NULL,NULL,S.IdOportunidad,NULL,NULL,NULL,NULL) 
	)
	)		
	GROUP BY YEAR(S.FechaCierreEstimada),MONTH(S.FechaCierreEstimada),SEC.Descripcion
	ORDER BY MONTH(S.FechaCierreEstimada)  
	
END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_LINEANEGOCIO_CONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Procedimiento : usp_IndicadorLineaNegocioConsultar
Descripcion   : Reporte del indicador de Linea de Negocio por periodo
Parametros Ingreso : 
@IdOportunidad: Id de Oportunidad
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      17/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_INDICADOR_LINEANEGOCIO_CONSULTAR]
(
@Anio int,
@Mes int = null,
@Etapa char(2) = null,
@ProbabilidadExito INT = null,
@Madurez char(2) = null
)
AS
BEGIN
	SET NOCOUNT ON;

	
    SELECT O.LINEANEGOCIO,COUNT(S.IdOportunidad) AS NroOportunidades
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	FUNNEL.OportunidadFinancieraOrigen O ON S.IdOportunidad = O.IdOportunidad
	WHERE
	S.IdEstado = 1 AND O.IdEstado = 1	
	AND YEAR(S.FechaCierreEstimada) = @Anio
	AND (@Mes IS NULL OR MONTH(S.FechaCierreEstimada) = @Mes)
	AND (@Etapa IS NULL OR LEFT(S.Etapa,2) = @Etapa)
	AND (@ProbabilidadExito IS NULL OR CONVERT(INT,S.ProbabilidadExito) = @ProbabilidadExito)		
	AND (@Madurez IS NULL OR @Madurez =
	( 
	SELECT 'N' + CONVERT(CHAR(1),MADUREZ) FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(NULL,NULL,S.IdOportunidad,NULL,NULL,NULL,NULL) 
	)
	)
	GROUP BY O.LINEANEGOCIO

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADOR_OFERTASESTADO_CONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Reporte de estados
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
27/07/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INDICADOR_OFERTASESTADO_CONSULTAR] 
(
@Anio int,
@Mes int = null,
@IdLineaNegocio int = null,
@IdSector int = null,
@ProbabilidadExito INT = null,
@Madurez char(2) = null
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @DescripcionLineaNegocio VARCHAR(100) = '';
	IF @IdLineaNegocio IS NOT NULL
	BEGIN
		SELECT @DescripcionLineaNegocio = Descripcion
	    FROM [COMUN].[LineaNegocio] WHERE IdLineaNegocio = @IdLineaNegocio
	END
						
	SELECT YEAR(S.FechaCierreEstimada) AS ANIO,
		   MONTH(S.FechaCierreEstimada) AS MES,
		   'F' + SUBSTRING(S.ETAPA,2,1) AS ETAPA,
		   COUNT(S.ETAPA) AS CANTIDAD
	FROM COMUN.SalesForceConsolidadoCabecera S 
	LEFT JOIN COMUN.Cliente CLI 
	ON S.NombreCliente = CLI.Descripcion
	LEFT JOIN [FUNNEL].[OportunidadFinancieraOrigen] O
	ON S.IdOportunidad = O.IdOportunidad
	WHERE S.IDESTADO = 1 AND CLI.IdEstado = 1 AND O.IdEstado = 1
	AND YEAR(S.FechaCierreEstimada) = @Anio 
	AND (@Mes IS NULL OR MONTH(S.FechaCierreEstimada) = @Mes)
	AND (@ProbabilidadExito IS NULL OR CONVERT(INT,S.ProbabilidadExito) = @ProbabilidadExito)		
	AND (@Madurez IS NULL OR @Madurez =
	( 
	SELECT 'N' + CONVERT(CHAR(1),MADUREZ) FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(NULL,NULL,S.IdOportunidad,NULL,NULL,NULL,NULL) 
	)
	)
	AND (@IdSector IS NULL OR CLI.IdSector = @IdSector)
	AND (@IdLineaNegocio IS NULL OR O.LINEANEGOCIO = @DescripcionLineaNegocio)	
	GROUP BY YEAR(S.FechaCierreEstimada),MONTH(S.FechaCierreEstimada),SUBSTRING(S.ETAPA,2,1)
	ORDER BY YEAR(S.FechaCierreEstimada),MONTH(S.FechaCierreEstimada),SUBSTRING(S.ETAPA,2,1)

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADORMADUREZCONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Reporte del indicador de la madurez
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
17/07/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INDICADORMADUREZCONSULTAR]
(
@Anio int,
@Mes int = null,
@IdLineaNegocio int = null,
@IdSector int = null,
@ProbabilidadExito INT = null,
@Etapa char(2) = null
)
AS
BEGIN
	SET NOCOUNT ON;

		declare @IdOportunidad VARCHAR(255) = '0';


		DECLARE @DescripcionLineaNegocio VARCHAR(100) = '';
		IF @IdLineaNegocio IS NOT NULL
		BEGIN
			SELECT @DescripcionLineaNegocio = Descripcion
			FROM [COMUN].[LineaNegocio] WHERE IdLineaNegocio = @IdLineaNegocio
		END
		 ELSE
		BEGIN
			SET @DescripcionLineaNegocio = NULL;
		END

		SELECT * FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,@IdOportunidad,
		@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa)
		UNION

		SELECT * FROM (
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,1 AS MESORDEN,'Ene' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,2 AS MESORDEN,'Feb' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,3 AS MESORDEN,'Mar' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,4 AS MESORDEN,'Abr' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,5 AS MESORDEN,'May' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,6 AS MESORDEN,'Jun' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,7 AS MESORDEN,'Jul' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,8 AS MESORDEN,'Ago' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,9 AS MESORDEN,'Set' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,10 AS MESORDEN,'Oct' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,11 AS MESORDEN,'Nov' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		UNION
		SELECT 0 AS MADUREZ,0 AS MADUREZ_VALOR,12 AS MESORDEN,'Dic' as MES,@Anio AS ANIO,
		'' AS IDOPORTUNIDAD,'' AS ASUNTO,'' AS NOMBRECLIENTE,0 AS CAPEX,GETDATE() AS FECHACIERREESTIMADA,
		'' AS LineaNegocio,'' AS SECTOR,'' CodigoCliente, 0 as IngresoTotal,0 as Oibda,0 as Opex,NULL AS ETAPA
		) AS MESES
		WHERE @Mes IS NULL

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADORMADUREZCONSULTAR_DETALLE]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Reporte del indicador de la madurez detallado
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
30/07/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INDICADORMADUREZCONSULTAR_DETALLE]
(
@Anio int,
@Mes int = null,
@IdLineaNegocio int = null,
@IdSector int = null,
@ProbabilidadExito INT = null,
@Etapa char(2) = null
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @DescripcionLineaNegocio VARCHAR(100) = '';
	IF @IdLineaNegocio IS NOT NULL
	BEGIN
		SELECT @DescripcionLineaNegocio = Descripcion
		FROM [COMUN].[LineaNegocio] WHERE IdLineaNegocio = @IdLineaNegocio
	END
		ELSE
	BEGIN
		SET @DescripcionLineaNegocio = NULL;
	END;

	WITH CTE
	AS 
	(
	SELECT T.LineaNegocio,'N' + CONVERT(CHAR(1),T.MADUREZ) AS MADUREZ ,
		   IngresoTotal AS IngresoTotal,
		   T.CAPEX AS CAPEX,
		   T.Opex AS Opex,
		   T.Oibda AS Oibda,
		   T.ETAPA AS ETAPA
		   FROM 
	FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,'0',
	@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa) T
	UNION ALL	
	SELECT DISTINCT T.LineaNegocio,'N1' AS MADUREZ,0 AS IngresoTotal,
	0 AS CAPEX,0 AS Opex,0 AS Oibda,NULL AS ETAPA
	FROM 
	FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,'0',
	@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa) T
	UNION ALL
	SELECT DISTINCT T.LineaNegocio,'N2' AS MADUREZ,0 AS IngresoTotal,
	0 AS CAPEX,0 AS Opex,0 AS Oibda,NULL AS ETAPA
	FROM 
	FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,'0',
	@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa) T
	UNION ALL
	SELECT DISTINCT T.LineaNegocio,'N3' AS MADUREZ,0 AS IngresoTotal,
	0 AS CAPEX,0 AS Opex,0 AS Oibda,NULL AS ETAPA
	FROM 
	FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,'0',
	@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa) T
	UNION ALL
	SELECT DISTINCT T.LineaNegocio,'N4' AS MADUREZ,0 AS IngresoTotal,
	0 AS CAPEX,0 AS Opex,0 AS Oibda,NULL AS ETAPA
	FROM 
	FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,'0',
	@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa) T
	UNION ALL
	SELECT DISTINCT T.LineaNegocio,'N5' AS MADUREZ,0 AS IngresoTotal,
	0 AS CAPEX,0 AS Opex,0 AS Oibda,NULL AS ETAPA
	FROM 
	FUNNEL.usp_IndicadorMadurezConsultar_Table(@Anio,@Mes,'0',
	@DescripcionLineaNegocio,@IdSector,@ProbabilidadExito,@Etapa) T
	)
	SELECT LineaNegocio,
		   MADUREZ,
		   ISNULL(SUM(IngresoTotal),0) AS IngresoTotal,
		   ISNULL(SUM(CAPEX),0) AS CAPEX,
		   ISNULL(SUM(Opex),0) AS Opex,
		   ISNULL(SUM(Oibda),0) AS Oibda,
		   COUNT(ETAPA) AS OPORTUNIDADES_TRABAJADAS FROM CTE
	GROUP BY LineaNegocio,MADUREZ
	ORDER BY LineaNegocio,MADUREZ
	 
END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INDICADORSECTORCONSULTAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Reporte de ofertas por sector
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      26/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_INDICADORSECTORCONSULTAR] 
(
@Anio int,
@Mes int = null,
@Etapa char(2) = null,
@ProbabilidadExito INT = null,
@Madurez char(2) = null
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	SEC.Descripcion AS SECTOR,
	COUNT(S.IdOportunidad) AS NroOportunidades
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	FUNNEL.OportunidadFinancieraOrigen O ON S.IdOportunidad = O.IdOportunidad
	INNER JOIN COMUN.Cliente CLI 
	ON S.NombreCliente = CLI.Descripcion
	INNER JOIN [COMUN].[Sector] SEC
	ON CLI.IdSector = SEC.IdSector
	WHERE
	S.IdEstado = 1 AND CLI.IdEstado = 1 AND SEC.IdEstado = 1
	AND YEAR(S.FechaCierreEstimada) = @Anio
	AND (@Mes IS NULL OR MONTH(S.FechaCierreEstimada) = @Mes)	
	AND (@Etapa IS NULL OR LEFT(S.Etapa,2) = @Etapa)
	AND (@ProbabilidadExito IS NULL OR CONVERT(INT,S.ProbabilidadExito) = @ProbabilidadExito)		
	AND (@Madurez IS NULL OR @Madurez =
	( 
	SELECT 'N' + CONVERT(CHAR(1),MADUREZ)  FROM FUNNEL.usp_IndicadorMadurezConsultar_Table(NULL,NULL,S.IdOportunidad,NULL,NULL,NULL,NULL) 
	)
	)	
	GROUP BY SEC.Descripcion
	
END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INSERTAR_OPORTUNIDADCONTABLEORIGEN]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Procedimiento : USP_INSERTAR_OPORTUNIDADCONTABLEORIGEN
Descripcion   : Inserta una oportunidad contable

*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INSERTAR_OPORTUNIDADCONTABLEORIGEN] 
    @IdOportunidad varchar(255) = NULL,
    @NumeroCaso varchar(255) = NULL,
    @IngresoAnual numeric(10, 2) = NULL,
    @OibdaAnual numeric(10, 2) = NULL,
    @Capex numeric(10, 2) = NULL,
    @OibdaPorcentaje numeric(10, 2) = NULL,
    @VanProyecto numeric(10, 2) = NULL,
    @VanPorVai numeric(10, 2) = NULL,
    @PayBack varchar(50) = NULL,
    @ValorRescate numeric(10, 2) = NULL,
    @IngresoTotal numeric(10, 2) = NULL,
    @CostosDirectos numeric(10, 2) = NULL,
    @UtilidadOperativa numeric(10, 2) = NULL,
    @MargenOperativo numeric(10, 2) = NULL,
    @NroCotizacion int = NULL,
    @CantPtos int = NULL,
    @TipoCambio numeric(10, 2) = NULL,
    @Anio int = NULL,
    @FechaCarga datetime = NULL,
    @LoginCarga varchar(100) = NULL,
    @IdCarga int = NULL,
	@TipoCambioIngresos numeric(10, 2) = NULL,
	@TipoCambioCostos numeric(10, 2) = NULL,
	@TipoCambioCapex numeric(10, 2) = NULL,
    @RutaCarga varchar(255) = NULL,
	@EstadoGanada bit = NULL
AS 
	SET NOCOUNT ON 
	
	IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NOT NULL AND @Anio IS NOT NULL)
	BEGIN
		DELETE FROM FUNNEL.[OportunidadContableOrigen] WHERE IdOportunidad = @IdOportunidad
		AND NumeroCaso = @NumeroCaso AND Anio = @Anio
	END
	 ELSE
	BEGIN
		IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NULL AND @Anio IS NOT NULL)
		BEGIN
			DELETE FROM FUNNEL.[OportunidadContableOrigen] WHERE IdOportunidad = @IdOportunidad
			AND Anio = @Anio
		END
	END


	INSERT INTO FUNNEL.[OportunidadContableOrigen] 
	([IdOportunidad], [NumeroCaso], [IngresoAnual], [OibdaAnual], [Capex], 
	[OibdaPorcentaje], [VanProyecto], [VanPorVai], [PayBack], [ValorRescate], 
	[IngresoTotal], [CostosDirectos], [UtilidadOperativa], [MargenOperativo], 
	[NroCotizacion], [CantPtos], [TipoCambio], [Anio], [FechaCarga], [LoginCarga], [IdCarga],
	TipoCambioIngresos,TipoCambioCostos,TipoCambioCapex,RutaCarga,
	IdEstado,FechaCreacion,EstadoGanada)
	SELECT @IdOportunidad, @NumeroCaso, @IngresoAnual, @OibdaAnual, @Capex, @OibdaPorcentaje, 
	@VanProyecto, @VanPorVai, @PayBack, @ValorRescate, @IngresoTotal, @CostosDirectos,
	@UtilidadOperativa, @MargenOperativo, @NroCotizacion, @CantPtos, @TipoCambio, @Anio, 
	@FechaCarga, @LoginCarga, @IdCarga,@TipoCambioIngresos,@TipoCambioCostos,@TipoCambioCapex,@RutaCarga,
	1,GETDATE(),@EstadoGanada

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INSERTAR_OPORTUNIDADFINANCIERAORIGEN]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_INSERTAR_OPORTUNIDADFINANCIERAORIGEN
Descripcion   : Inserta una oportunidad financiera

Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_INSERTAR_OPORTUNIDADFINANCIERAORIGEN] 
    @IdOportunidad varchar(255) = NULL,
    @NumeroCaso varchar(255) = NULL,
    @PeriodoMeses int = NULL,
    @IngresoTotal numeric(10, 2) = NULL,
    @PagoUnicoDolares numeric(10, 2) = NULL,
    @RecurrenteDolares numeric(10, 2) = NULL,
    @VanFlujoNeto numeric(10, 2) = NULL,
    @VanFlujoNetoPorVanIngresos numeric(10, 2) = NULL,
    @UtilidadOperativa numeric(10, 2) = NULL,
    @CostosDirectos numeric(10, 2) = NULL,
    @MargenOperativa numeric(10, 2) = NULL,
    @CantidadPtos int = NULL,
    @Oidba numeric(10, 2) = NULL,
    @OidbaPorcentaje numeric(10, 2) = NULL,
    @Capex numeric(10, 2) = NULL,
    @Payback varchar(50) = NULL,
    @Depreciacion numeric(10, 2) = NULL,
    @TipoCambio numeric(10, 2) = NULL,
    @Tir numeric(10, 2) = NULL,
    @FechaCarga datetime = NULL,
    @LoginCarga varchar(100) = NULL,
    @IdCarga int = NULL,
	@LineaNegocio varchar(255) = NULL,
	@EstadoGanada bit = NULL
AS 
	SET NOCOUNT ON;
	
	IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NOT NULL)
	BEGIN
		DELETE FROM FUNNEL.[OportunidadFinancieraOrigen] WHERE IdOportunidad = @IdOportunidad
		AND NumeroCaso = @NumeroCaso
	END
	 ELSE
	BEGIN
		IF (@IdOportunidad IS NOT NULL AND @NumeroCaso IS NULL)
		BEGIN
			DELETE FROM FUNNEL.[OportunidadFinancieraOrigen] WHERE IdOportunidad = @IdOportunidad
		END
	END

	INSERT INTO FUNNEL.[OportunidadFinancieraOrigen] ([IdOportunidad], [NumeroCaso], 
	[PeriodoMeses], [IngresoTotal], [PagoUnicoDolares], [RecurrenteDolares], 
	[VanFlujoNeto], [VanFlujoNetoPorVanIngresos], [UtilidadOperativa], 
	[CostosDirectos], [MargenOperativa], [CantidadPtos], [Oidba], [OidbaPorcentaje], 
	[Capex], [Payback], [Depreciacion], [TipoCambio], [Tir], [FechaCarga], [LoginCarga], [IdCarga],
	IdEstado,FechaCreacion,LineaNegocio,EstadoGanada)
	SELECT @IdOportunidad, @NumeroCaso, @PeriodoMeses, @IngresoTotal, @PagoUnicoDolares,
	 @RecurrenteDolares, @VanFlujoNeto, @VanFlujoNetoPorVanIngresos, @UtilidadOperativa, 
	 @CostosDirectos, @MargenOperativa, @CantidadPtos, @Oidba, @OidbaPorcentaje, 
	 @Capex, @Payback, @Depreciacion, @TipoCambio, @Tir, @FechaCarga, @LoginCarga, @IdCarga,
	 1,GETDATE(),@LineaNegocio,@EstadoGanada
	

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INSERTAR_OPORTUNIDADORIGENBEGIN]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Procedimiento : USP_INSERTAR_OPORTUNIDADORIGENBEGIN
Descripcion   : Historial de registro de Oportunidades en el sistema
Parametros Ingreso : 
@FechaCarga: Fecha de carga en la DB
@Id: Id de carga generado
Parametros Retorno : 
Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      03/07/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_INSERTAR_OPORTUNIDADORIGENBEGIN] 
    @FechaCarga datetime,
	@Id int output
AS 
	SET NOCOUNT ON 
	

	INSERT INTO FUNNEL.[OportunidadesHistoriaCarga]
			   ([FechaCarga],IdEstado,FechaCreacion)
		 VALUES
			   (@FechaCarga,1,GETDATE())

	set @Id = SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_INSERTAR_OPORTUNIDADPMOLOTUSLINEA]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Procedimiento : USP_INSERTAR_OPORTUNIDADPMOLOTUSLINEA
Descripcion   : Inserta una oportunidad CMI

Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      19/07/2018       jaguilar            Creacion
*/
CREATE PROC [FUNNEL].[USP_INSERTAR_OPORTUNIDADPMOLOTUSLINEA]
    @IdOportunidad varchar(100) = NULL,
    @NumeroCaso varchar(100) = NULL,
    @Proceso varchar(100) = NULL,
    @DatosProceso varchar(100) = NULL,
    @DetalleProceso varchar(100) = NULL,
	@Tipo int = NULL,
	@Item int = NULL,
    @IdEstado int = NULL,
    @IdUsuarioCreacion int = NULL,
    @IdUsuarioEdicion int = NULL,
    @FechaEdicion datetime = NULL,
	@FechaCarga datetime = NULL,
	@IdCarga INT = NULL,
	@LoginCarga varchar(100) = NULL,
	@EstadoGanada bit = NULL
AS 
	SET NOCOUNT ON 
	
	INSERT INTO [FUNNEL].[OportunidadPmoLotusLinea] ([IdOportunidad], 
	[NumeroCaso], [Proceso], [DatosProceso], [DetalleProceso], Tipo,[IdEstado], 
	[IdUsuarioCreacion], [FechaCreacion], [IdUsuarioEdicion], [FechaEdicion],
	FechaCarga,IdCarga,LoginCarga,Item,EstadoGanada)
	SELECT @IdOportunidad, @NumeroCaso, @Proceso, @DatosProceso, @DetalleProceso, @Tipo,
	@IdEstado, @IdUsuarioCreacion, GETDATE(), @IdUsuarioEdicion, @FechaEdicion,
	@FechaCarga,@IdCarga,@LoginCarga,@Item,@EstadoGanada

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OFERTAS_CONVERGENTES]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Muestra las ofertas convergentes
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
13/08/2018       jaguilar            Creacion
*/


CREATE PROC [FUNNEL].[USP_OFERTAS_CONVERGENTES] 
AS
BEGIN
	
	SET NOCOUNT ON;
	
	WITH CTE
	AS
	(
	SELECT S.NombreCliente,COUNT(F.LineaNegocio) AS NroLineas FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN
	FUNNEL.OportunidadFinancieraOrigen F
	ON S.IdOportunidad = F.IdOportunidad
	WHERE F.IdEstado = 1 AND S.IdEstado = 1
	GROUP BY S.NombreCliente
	)
	SELECT CLI.CodigoCliente,CLI.Descripcion as DescripcionCliente,
	CTE.NroLineas FROM CTE INNER JOIN COMUN.Cliente CLI
	ON CTE.NombreCliente = CLI.[Descripcion]
	--WHERE NroLineas > 1

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OFERTAS_POR_VENCER]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Muestra las ofertas por vencer
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
13/08/2018       jaguilar            Creacion
*/



CREATE PROC [FUNNEL].[USP_OFERTAS_POR_VENCER] 
(
	@NroOfertasPorVencer INT OUTPUT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT @NroOfertasPorVencer = COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] 
	--WHERE DATEDIFF(DD,[FechaCierreEstimada] , CONVERT(DATE,GETDATE())) = 15
	WHERE [IdEstado] = 1	

	SELECT F.LineaNegocio,COUNT(S.IdOportunidad) AS Cantidad FROM [COMUN].[SalesForceConsolidadoCabecera] S
	LEFT OUTER JOIN FUNNEL.OportunidadFinancieraOrigen F
	ON S.IdOportunidad = F.IdOportunidad
	WHERE S.[IdEstado] = 1 AND F.[IdEstado] = 1
	--AND DATEDIFF(DD,[FechaCierreEstimada] , CONVERT(DATE,GETDATE())) = 15
	GROUP BY F.LineaNegocio

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OFERTAS_VENCIDAS]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Muestra las ofertas vencidas
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
10/08/2018       jaguilar            Creacion
*/



CREATE PROC [FUNNEL].[USP_OFERTAS_VENCIDAS] 
(
	@NroOfertasVencidas INT OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT @NroOfertasVencidas = COUNT(IdOportunidad) FROM [COMUN].[SalesForceConsolidadoCabecera] 
	WHERE [FechaCierreEstimada] > CONVERT(DATE,GETDATE())
	AND [IdEstado] = 1
	
	SELECT F.LineaNegocio,COUNT(S.IdOportunidad) AS Cantidad FROM [COMUN].[SalesForceConsolidadoCabecera] S
	INNER JOIN FUNNEL.OportunidadFinancieraOrigen F
	ON S.IdOportunidad = F.IdOportunidad
	WHERE S.[IdEstado] = 1 AND F.[IdEstado] = 1
	AND S.[FechaCierreEstimada] > CONVERT(DATE,GETDATE())
	GROUP BY F.LineaNegocio
	
END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OPORTUNIDAD_INSERTAR_LECTURA_EXCEL]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
Procedimiento : USP_OPORTUNIDAD_INSERTAR_LECTURA_EXCEL
Descripcion   : Registra los ultimos ficheros por cada PER

Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      21/08/2018       jaguilar            Creacion
*/
CREATE PROC [FUNNEL].[USP_OPORTUNIDAD_INSERTAR_LECTURA_EXCEL]
(
	@IdOportunidad varchar(100),
	@Archivo varchar(100),
    @Ruta varchar(500),
    @FechaModificacion datetime
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM [FUNNEL].[OportunidadHistorialLecturaExcel]
	WHERE IdOportunidad = @IdOportunidad 
	AND FechaModificacion < @FechaModificacion

	INSERT INTO [FUNNEL].[OportunidadHistorialLecturaExcel]
			   ([IdOportunidad]
			   ,[Archivo]
			   ,[Ruta]
			   ,[FechaModificacion]
			   ,[IdUsuarioCreacion]
			   ,[FechaCreacion]
			   ,[IdUsuarioEdicion]
			   ,[FechaEdicion])
		 VALUES
			   (@IdOportunidad
			   ,@Archivo
			   ,@Ruta
			   ,@FechaModificacion
			   ,NULL
			   ,GETDATE()
			   ,NULL
			   ,NULL)			 

END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OPORTUNIDAD_LISTAR_LECTURA_EXCEL]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Procedimiento : USP_OPORTUNIDAD_LISTAR_LECTURA_EXCEL
Descripcion   : Obtiene los ultimos ficheros por cada PER

Modificaciones    :
*************************************************************
id         Fecha           Autor         Motivo del Cambio
*************************************************************
@JAA      21/08/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_OPORTUNIDAD_LISTAR_LECTURA_EXCEL]
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT [Archivo]
		  ,[Ruta]
		  ,[FechaModificacion]
		  ,IdOportunidad
	  FROM [FUNNEL].[OportunidadHistorialLecturaExcel]
	
END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OPORTUNIDADPRINCIPALCABECERAMIGRAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROC [FUNNEL].[USP_OPORTUNIDADPRINCIPALCABECERAMIGRAR] 
AS
BEGIN

	SET NOCOUNT ON 

	TRUNCATE TABLE FUNNEL.[ReportOportunidadPrincipalCabecera]
	
	INSERT INTO FUNNEL.[ReportOportunidadPrincipalCabecera]([MesOrden],[Mes],[Anio],[IdOportunidad],
	[Capex],[ProbabilidadExito],[FechaCierreEstimada],Asunto,[NombreCliente])
	SELECT MONTH(S.[FechaCierreEstimada]),CASE MONTH(S.[FechaCierreEstimada]) 
		   WHEN 1 THEN 'Ene'
		   WHEN 2 THEN 'Feb'
		   WHEN 3 THEN 'Mar'
		   WHEN 4 THEN 'Abr'
		   WHEN 5 THEN 'May'
		   WHEN 6 THEN 'Jun'
		   WHEN 7 THEN 'Jul'
		   WHEN 8 THEN 'Ago'
		   WHEN 9 THEN 'Set'
		   WHEN 10 THEN 'Oct'
		   WHEN 11 THEN 'Nov'
		   WHEN 12 THEN 'Dic'
		   END
	AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
	O.IdOportunidad,O.Capex, S.ProbabilidadExito,S.FechaCierreEstimada,S.Asunto,S.NombreCliente
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	(
	
	SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
    SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
    FROM FUNNEL.OportunidadContableOrigen
    GROUP BY IdOportunidad

	) O ON S.IdOportunidad = O.IdOportunidad 


END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_OPORTUNIDADPRINCIPALPIEMIGRAR]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [FUNNEL].[USP_OPORTUNIDADPRINCIPALPIEMIGRAR]
AS
BEGIN

	SET NOCOUNT ON 

	TRUNCATE TABLE FUNNEL.[ReporteOportunidadPrincipalPie]

	INSERT INTO FUNNEL.[ReporteOportunidadPrincipalPie]
	(
	ANIO
	,MES
	,SUMIngresoAnual
    ,[SUMCapex]
    ,[SUMOibdaAnual]
    ,[OportTrabajadas]
    ,[OportMaduras]
    ,[OportGanadas]
	)
	SELECT 
	TOTALES.ANIO,TOTALES.MES,TOTALES.IngresoTotal,TOTALES.Capex,TOTALES.Oidba,
	ISNULL(TRABAJADAS.OPORT_TRABAJADAS,0) AS OPORT_TRABAJADAS,
	0,
	ISNULL(GANADAS.OPORT_GANADAS,0) AS OPORT_GANADAS FROM(
	SELECT C.MES,C.ANIO,SUM(C.IngresoTotal) AS IngresoTotal,SUM(C.Capex) AS Capex,SUM(C.Oidba) AS Oidba FROM(
	SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
	O.IngresoTotal, O.Capex,O.Oidba
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	(	
		SELECT IdOportunidad,SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
		SUM(OibdaAnual) AS Oidba,SUM(CAPEX * TipoCambioCapex) AS CAPEX
		FROM FUNNEL.OportunidadContableOrigen
		GROUP BY IdOportunidad
	) O ON S.IdOportunidad = O.IdOportunidad 
	) AS C
	GROUP BY C.MES,C.ANIO
	) AS TOTALES
	LEFT JOIN 
	(
	SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
	COUNT(S.IdOportunidad) AS OPORT_TRABAJADAS
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
	WHERE NOT EXISTS
		(
			SELECT IdOportunidad FROM COMUN.SalesForceConsolidadoCabecera WHERE IdOportunidad = S.IdOportunidad
			AND Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
		)
	GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
	) AS TRABAJADAS
	ON TOTALES.ANIO = TRABAJADAS.ANIO AND TOTALES.MES = TRABAJADAS.MES
	LEFT JOIN 
	(
	SELECT MONTH(S.[FechaCierreEstimada]) AS MES,YEAR(S.[FechaCierreEstimada]) AS ANIO,
	COUNT(S.IdOportunidad) AS OPORT_GANADAS
	FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
	FUNNEL.[OportunidadFinancieraOrigen] O ON S.IdOportunidad = O.IdOportunidad 
	WHERE Etapa IN ('F1 - Ganada','F1 - Cancelada | Suspendida','F1 - Perdida') 
	GROUP BY MONTH(S.[FechaCierreEstimada]),YEAR(S.[FechaCierreEstimada])
	) AS GANADAS
	ON TOTALES.ANIO = GANADAS.ANIO AND TOTALES.MES = GANADAS.MES
	
    
END

GO
/****** Object:  StoredProcedure [FUNNEL].[USP_USUARIOS_ENVIOALERTAS]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Descripcion   : Obtiene los usuarios a enviar las alertas
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
13/08/2018       jaguilar            Creacion
*/

CREATE PROC [FUNNEL].[USP_USUARIOS_ENVIOALERTAS] 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT [usuac_Nombre_Usuario],[usuac_Apellidos_Usuario],[usuac_Correo_Electronico] 
	FROM [RAIS_DIO].[dbo].[RAIST_USUARIO] 
	WHERE usuac_Login IN ('anieto')


END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_ELIMINAR_CASO_NEGOCIO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Eliminar en las tablas de OportunidadFlujoCaja, OportunidadServicioCMI 
	la configuracion del caso de negocio.

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_ELIMINAR_CASO_NEGOCIO]
(
	@IdOportunidadLineaNegocio	INT,
	@IdCasoNegocio				INT,
	@IdUsuarioEdicion			INT,
	@IdEstado					INT
)
AS
BEGIN

UPDATE T2 SET T2.IdEstado = @IdEstado,
			  T2.IdUsuarioEdicion = @IdUsuarioEdicion,
			  T2.FechaEdicion = GETDATE()
from [OPORTUNIDAD].[OportunidadFlujoCaja] T1 
join OPORTUNIDAD.SubServicioDatosCapex T2 ON T2.IdFlujoCaja = T1.IdFlujoCaja
WHERE T1.IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and
	  T1.IdCasoNegocio = @IdCasoNegocio 


UPDATE T2 SET T2.IdEstado = @IdEstado,
			  T2.IdUsuarioEdicion = @IdUsuarioEdicion,
			  T2.FechaEdicion = GETDATE()
from [OPORTUNIDAD].[OportunidadFlujoCaja] T1 
join OPORTUNIDAD.SubServicioDatosCaratula T2 ON T2.IdFlujoCaja = T1.IdFlujoCaja
WHERE T1.IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and
	  T1.IdCasoNegocio = @IdCasoNegocio 
	  
UPDATE T2 SET T2.IdEstado = @IdEstado,
			  T2.IdUsuarioEdicion = @IdUsuarioEdicion,
			  T2.FechaEdicion = GETDATE()
from [OPORTUNIDAD].[OportunidadFlujoCaja] T1 
join [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] T2 ON T2.IdFlujoCaja = T1.IdFlujoCaja
WHERE T1.IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and
	  T1.IdCasoNegocio = @IdCasoNegocio 
	  
UPDATE [OPORTUNIDAD].[OportunidadServicioCMI] SET IdEstado = @IdEstado,
												  IdUsuarioEdicion = @IdUsuarioEdicion,
												  FechaEdicion = GETDATE()
WHERE IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio 
	  
UPDATE [OPORTUNIDAD].[OportunidadFlujoCaja] SET IdEstado = @IdEstado,
												IdUsuarioEdicion = @IdUsuarioEdicion,
												FechaEdicion = GETDATE()
WHERE IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and
	  IdCasoNegocio = @IdCasoNegocio 
	  
	  
END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_35GHZ]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_35GHZ]
(
	@IDOPORTUNIDAD		INT,
	@IDLINEANEGOCIO		INT,
	@IDSUBSERVICIO		INT,
	@IDUSUARIOCREACION	INT,
	@IDESTADO			INT,
	@TIPOFICHA			INT
)
AS
BEGIN

	DECLARE @IDFLUJOCAJACONFIGURACION	INT = 0,
			@IDFLUJOCAJA				INT = 0

	/* 1. OBTENER LOS ID DE LA CONFIGURACIÓN DE LO SERVICIOS. */
	SELECT  @IDFLUJOCAJA = ISNULL(IDFLUJOCAJA,0) 
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA 
	WHERE	IDOPORTUNIDAD	= @IDOPORTUNIDAD 
	AND		IDLINEANEGOCIO	= @IDLINEANEGOCIO 
	AND		IDSUBSERVICIO	= @IDSUBSERVICIO

	SELECT	@IDFLUJOCAJACONFIGURACION = IDFLUJOCAJACONFIGURACION
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION
	WHERE	IDFLUJOCAJA = @IDFLUJOCAJA

	/* 2. SE ESTA RECALCULANDO, ASI QUE DEBE ELIMINARSE LA INFORMACIÓN. */
	DELETE FROM OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE WHERE IDFLUJOCAJACONFIGURACION = @IDFLUJOCAJACONFIGURACION

	--/* 3. REGISTRAR LA INFORMACIÓN DEL RECALCULO. */
	INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE
	(
		IDFLUJOCAJACONFIGURACION, ANIO, MES,
		MONTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION, TIPOFICHA
	)
	SELECT  @IDFLUJOCAJACONFIGURACION, ANIO, MES, SUM(MONTO), @IDESTADO,
			@IDUSUARIOCREACION, GETDATE(), @TIPOFICHA
	FROM (
		SELECT  T3.COMPONENTE, T2.ANIO, T2.MES, SUM(T2.MONTO) MONTO
		FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA T1
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION T5
		ON		T1.IDFLUJOCAJA = T5.IDFLUJOCAJA
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE T2
		ON		T5.IDFLUJOCAJACONFIGURACION = T2.IDFLUJOCAJACONFIGURACION
		JOIN	OPORTUNIDAD.FORMULADETALLE T3
		ON		T1.IDSUBSERVICIO = T3.IDSUBSERVICIO
		JOIN	OPORTUNIDAD.FORMULA T4
		ON		T4.IDFORMULA		= T3.IDFORMULA
		JOIN	OPORTUNIDAD.SUBSERVICIODATOSCARATULA T6
		ON		T6.IDFLUJOCAJA = T1.IDFLUJOCAJA
		WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
		AND		T1.IDLINEANEGOCIO	= @IDLINEANEGOCIO
		AND		T4.IDSUBSERVICIO	= @IDSUBSERVICIO
		AND		T6.IDMEDIO			= T3.VALOR1 /* MEDIO: 3.5Ghz */
		AND		T1.IDGRUPO			= T3.VALOR2 /* GRUPO: CIRCUITOS */
		GROUP	BY T3.COMPONENTE, T2.ANIO, T2.MES
	)A
	GROUP BY ANIO, MES
	ORDER BY ANIO, MES
	
END
GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_CASO_NEGOCIO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Registrar en la tablas OportunidadFlujoCaja, OportunidadServicioCMI 
	la configuracion del caso de negocio.

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
    ***********************************************************
    @Test: exec [OPORTUNIDAD].[USP_INSERTAR_CASO_NEGOCIO] 2,5,1,1,1
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_CASO_NEGOCIO]
(
	@IdOportunidadLineaNegocio	INT,
	@IdCasoNegocio				INT,
	@IdUsuarioCreacion			INT,
	@IdEstado					INT
)
AS
BEGIN
declare @Caratula int=1,
		@Ecapex int=2,
		@IdLineaNegocio int=0
		
set @IdLineaNegocio = (select IdLineaNegocio from OPORTUNIDAD.OportunidadLineaNegocio where IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and IdEstado = @IdEstado)

declare @CasoNegocio table(id int identity(1,1) ,IdCasoNegocio int,IdServicioSubServicio int,IdServicio int,IdSubServicio int,IdTipoCosto int,IdProveedor int, 
					ContratoMarco varchar(50),IdPeriodos int,Inicio int,Ponderacion decimal(18,2),IdMoneda int,IdGrupo int,FlagSISEGO varchar(10),IdServicioCMI int,IdPestana int,Concatenado varchar(100),
					AEReducido varchar(30),IdMedio int,IdTipoEnlace int)

	insert INTO @CasoNegocio (IdCasoNegocio,IdServicioSubServicio,IdServicio,IdSubServicio,IdTipoCosto,IdProveedor,
			ContratoMarco,IdPeriodos,Inicio,Ponderacion,IdMoneda,IdGrupo,FlagSISEGO,IdServicioCMI,IdPestana,Concatenado,AEReducido,IdMedio,IdTipoEnlace)
	select T1.IdCasoNegocio, T4.IdServicioSubServicio,T4.IdServicio,T4.IdSubServicio,T4.IdTipoCosto,T4.IdProveedor,
	T4.ContratoMarco,T4.IdPeriodos,T4.Inicio, T4.Ponderacion,T4.IdMoneda,T4.IdGrupo,T4.FlagSISEGO,T3.IdServicioCMI,P.IdPestana, ISNULL(T5.Concatenado,'')Concatenado,ISNULL(T5.AEReducido,'') AEReducido, T3.IdMedio,T3.IdTipoEnlace
	from [COMUN].[CasoNegocio] T1 
		 JOIN [COMUN].[CasoNegocioServicio] T2 on T1.IdCasoNegocio = T2.IdCasoNegocio
		 JOIN [COMUN].[ServicioSubServicio] T4 on T4.IdServicio = T2.IdServicio
		 JOIN OPORTUNIDAD.PestanaGrupo P on T4.IdGrupo = P.IdGrupo
		 JOIN [COMUN].[Servicio] T3 on T3.IdServicio = T4.IdServicio 
		 JOIN [COMUN].[SubServicio] T5 on T5.IdSubServicio = T4.IdSubServicio
	WHERE T1.IdCasoNegocio = @IdCasoNegocio AND 
		  T1.IdLineaNegocio = @IdLineaNegocio AND 
		  T1.IdEstado = @IdEstado
		  
declare @inicio int= 1,
		@Total int=0,
		@IdFlujoCaja int=0
		
set @Total = (select count(*) from @CasoNegocio)

while(@inicio <= @Total)
begin

	INSERT INTO [OPORTUNIDAD].[OportunidadFlujoCaja] 
	(IdOportunidadLineaNegocio,IdTipoCosto,IdProveedor,IdPeriodos,IdPestana,IdGrupo,IdCasoNegocio,IdServicio,ContratoMarco,IdMoneda,FlagSISEGO,IdServicioCMI,IdEstado,IdUsuarioCreacion,FechaCreacion, IdSubServicio)
	SELECT @IdOportunidadLineaNegocio, T1.IdTipoCosto,T1.IdProveedor,T1.IdPeriodos,T1.IdPestana,T1.IdGrupo,T1.IdCasoNegocio, T1.IdServicio,T1.ContratoMarco,T1.IdMoneda,T1.FlagSISEGO,T1.IdServicioCMI,@IdEstado,@IdUsuarioCreacion,GETDATE(), t1.IdSubServicio
	FROM @CasoNegocio T1 where id = @inicio

	set @IdFlujoCaja = @@IDENTITY

	INSERT INTO [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion]
	(IdFlujoCaja,Ponderacion,CostoPreOperativo,Inicio,Meses,IdEstado,IdUsuarioCreacion,FechaCreacion)
	SELECT @IdFlujoCaja,T1.Ponderacion,0.0,T1.Inicio,CASE WHEN(T1.IdPeriodos=4) THEN 1 ELSE 0 END,@IdEstado,@IdUsuarioCreacion,GETDATE()
	FROM @CasoNegocio T1 where id = @inicio
	
	if((select count(*) FROM @CasoNegocio T1 where T1.IdServicioCMI is not null and id = @inicio)>0)
	begin
		INSERT INTO [OPORTUNIDAD].[OportunidadServicioCMI] (IdOportunidadLineaNegocio,IdServicioCMI,Porcentaje,IdEstado,IdUsuarioCreacion,FechaCreacion)
		SELECT @IdOportunidadLineaNegocio, T1.IdServicioCMI,T1.Ponderacion,@IdEstado,@IdUsuarioCreacion,GETDATE()
		FROM @CasoNegocio T1 where T1.IdServicioCMI is not null and id = @inicio
	end

	INSERT INTO OPORTUNIDAD.SubServicioDatosCaratula (IdFlujoCaja,IdMedio,IdTipoEnlace,IdEstado,IdUsuarioCreacion,FechaCreacion)
	SELECT @IdFlujoCaja,C.IdMedio,C.IdTipoEnlace,@IdEstado,@IdUsuarioCreacion,GETDATE()
	FROM @CasoNegocio C 
	WHERE C.IdPestana = @Caratula and id = @inicio

	INSERT INTO OPORTUNIDAD.SubServicioDatosCapex (IdFlujoCaja,IdEstado,IdUsuarioCreacion,FechaCreacion,Cruce,AEReducido,Marca)
	SELECT @IdFlujoCaja,@IdEstado,@IdUsuarioCreacion,GETDATE(),C.Concatenado,C.AEReducido,''
	FROM @CasoNegocio C 
	WHERE C.IdPestana = @Ecapex and id = @inicio

	set @inicio= @inicio+1;

end

END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_CONFIGURACIONFLUJO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_CONFIGURACIONFLUJO]
(
	@IDOPORTUNIDADLINEANEGOCIO	INT,
	@IDSUBSERVICIO				INT,
	@IDESTADO					INT,
	@IDUSUARIOCREACION			INT
)
AS
BEGIN

	DECLARE @IDFLUJOCAJA				INT = 0
	
	/*
		1. @IDSUBSERVICIO = 116: INGRESOS 
		2. @IDSUBSERVICIO = 231: INTERNET (TIWS), REGISTRAR EN LA TABLA OPORTUNIDADCOSTO, LA INFORMACIÓN DE LA TABLA COMUN.COSTO CON EL IDTIPOCOSTO = 7 ASOCIANDOLO AL IDOPORTUNIDADLINEANEGOCIO.
		3. @IDSUBSERVICIO = 230: 3.5 GHZ.
		4. @IDSUBSERVICIO = 255: MANTENIMIENTO SERVICIO SATELITAL
		5. @IDSUBSERVICIO = 274: MANTENIMIENTO EQUIPOS+FIBRA
	*/
	
	SELECT  @IDFLUJOCAJA = ISNULL(IDFLUJOCAJA,0) FROM OPORTUNIDAD.OPORTUNIDADFLUJOCAJA 
	WHERE	IDOPORTUNIDADLINEANEGOCIO = @IDOPORTUNIDADLINEANEGOCIO AND IDSUBSERVICIO = @IDSUBSERVICIO

	IF(@IDFLUJOCAJA <=0)
	BEGIN
		/* 2. REGISTRAR EL SERVICIO CALCULADO EN OPORTUNIDAD.OPORTUNIDADFLUJOCAJA. */
		INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJA
		(
			IDOPORTUNIDADLINEANEGOCIO, IDAGRUPADOR, IDTIPOCOSTO, DESCRIPCION, IDPROVEEDOR, IDPERIODOS,
			IDESTADO, IDUSUARIOCREACION, FECHACREACION, IDSUBSERVICIO
		)
		SELECT	@IDOPORTUNIDADLINEANEGOCIO, NULL, NULL, T1.DESCRIPCION, NULL, 2, 
				@IDESTADO, @IDUSUARIOCREACION, GETDATE(), @IDSUBSERVICIO
		FROM	COMUN.SUBSERVICIO T1
		WHERE	IDSUBSERVICIO = @IDSUBSERVICIO
	
		SELECT  @IDFLUJOCAJA = ISNULL(IDFLUJOCAJA,0) FROM OPORTUNIDAD.OPORTUNIDADFLUJOCAJA 
		WHERE	IDOPORTUNIDADLINEANEGOCIO = @IDOPORTUNIDADLINEANEGOCIO AND IDSUBSERVICIO = @IDSUBSERVICIO

		/* 3. REGISTRAR EL SERVICIO CALCULADO EN OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION. */
		INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION
		(	
			IDFLUJOCAJA, PONDERACION, COSTOPREOPERATIVO, 
			INICIO, MESES, IDESTADO, IDUSUARIOCREACION, FECHACREACION
		)
		VALUES (@IDFLUJOCAJA, 100, 0, 0, 0, @IDESTADO, @IDUSUARIOCREACION, GETDATE())

		IF (@IDSUBSERVICIO = 231) /* INTERNET (TIWS) */
		BEGIN

			INSERT INTO OPORTUNIDAD.OPORTUNIDADCOSTO
			(		IDOPORTUNIDADLINEANEGOCIO, IDCOSTO, IDTIPOCOSTO, DESCRIPCION, MONTO, VELOCIDADSUBIDAKBPS, 
					PORCENTAJEGARANTIZADO, PORCENTAJESOBRESUSCRIPCION, COSTOSEGMENTOSATELITAL, INVANTENAHUBUSD, ANTENACASACLIENTEUSD,
					INSTALACION, IDUNIDADCONSUMO, IDTIPIFICACION, CODIGOMODELO, MODELO, IDESTADO, IDUSUARIOCREACION, FECHACREACION
			)
			SELECT	@IDOPORTUNIDADLINEANEGOCIO, IDCOSTO, IDTIPOCOSTO, DESCRIPCION, MONTO, VELOCIDADSUBIDAKBPS, 
					PORCENTAJEGARANTIZADO, PORCENTAJESOBRESUSCRIPCION, COSTOSEGMENTOSATELITAL, INVANTENAHUBUSD, ANTENACASACLIENTEUSD,
					INSTALACION, IDUNIDADCONSUMO, IDTIPIFICACION, CODIGOMODELO, MODELO, IDESTADO, @IDUSUARIOCREACION, GETDATE()
			FROM	COMUN.COSTO
			WHERE	IDTIPOCOSTO = 7 /* 7. COSTOS ASOCIADOS A INTERNET. */
		END

	END

END

--EXEC OPORTUNIDAD.USP_INSERTAR_CONFIGURACIONFLUJO 1,5,231,1,1
GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_INGRESOS]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_INGRESOS]
(
	@IDOPORTUNIDADLINEANEGOCIO	INT,
	@IDUSUARIOCREACION			INT,
	@IDESTADO					INT,
	@TIPOFICHA					INT
)
AS
BEGIN

	DECLARE @IDFLUJOCAJACONFIGURACION	INT = 0,
			@IDFLUJOCAJA				INT = 0,
			@IDSUBSERVICIO				INT = 116

	/* 1. OBTENER LOS ID DE LA CONFIGURACIÓN DE LO SERVICIOS. */
	SELECT  @IDFLUJOCAJA = ISNULL(IDFLUJOCAJA,0) 
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA 
	WHERE	IDOPORTUNIDADLINEANEGOCIO	= @IDOPORTUNIDADLINEANEGOCIO 
	AND		IDSUBSERVICIO				= @IDSUBSERVICIO

	SELECT	@IDFLUJOCAJACONFIGURACION = IDFLUJOCAJACONFIGURACION
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION
	WHERE	IDFLUJOCAJA = @IDFLUJOCAJA

	/* 2. SE ESTA RECALCULANDO, ASI QUE DEBE ELIMINARSE LA INFORMACIÓN. */
	DELETE FROM OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE WHERE IDFLUJOCAJACONFIGURACION = @IDFLUJOCAJACONFIGURACION

	/* 3. REGISTRAR LA INFORMACIÓN DEL RECALCULO. */
	INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE
	(
		IDFLUJOCAJACONFIGURACION, ANIO, MES,
		MONTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION, TIPOFICHA
	)
	SELECT  @IDFLUJOCAJACONFIGURACION, ANIO, MES, SUM(MONTO), @IDESTADO,
			@IDUSUARIOCREACION, GETDATE(), @TIPOFICHA
	FROM (
		SELECT  T3.COMPONENTE, T2.ANIO, T2.MES, SUM(T2.MONTO) MONTO
		FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA T1
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION T5
		ON		T1.IDFLUJOCAJA = T5.IDFLUJOCAJA
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE T2
		ON		T5.IdFlujoCajaConfiguracion = T2.IdFlujoCajaConfiguracion
		JOIN	OPORTUNIDAD.FORMULADETALLE T3
		ON		T1.IDSUBSERVICIO = T3.IDSUBSERVICIO
		JOIN	OPORTUNIDAD.FORMULA T4
		ON		T4.IDFORMULA		= T3.IDFORMULA
		WHERE	T1.IDOPORTUNIDADLINEANEGOCIO	= @IDOPORTUNIDADLINEANEGOCIO
		AND		T4.IDSUBSERVICIO	= @IDSUBSERVICIO
		GROUP	BY T3.COMPONENTE, T2.ANIO, T2.MES
	)A
	GROUP BY ANIO, MES
	ORDER BY ANIO, MES

END
GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_INTERNETTIWS]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_INTERNETTIWS]
(
	@IDOPORTUNIDADLINEANEGOCIO	INT,
	@IDUSUARIOCREACION			INT,
	@IDESTADO					INT,
	@TIPOFICHA					INT
)
AS
BEGIN

	DECLARE @IDFLUJOCAJACONFIGURACION	INT = 0,
			@IDFLUJOCAJA				INT = 0,
			@IDSUBSERVICIO				INT = 231,
			@COSTOUNITARIO				DECIMAL(18, 4)

	/* 1. OBTENER LOS ID DE LA CONFIGURACIÓN DE LO SERVICIOS. */
	SELECT  @IDFLUJOCAJA = ISNULL(IDFLUJOCAJA,0), @COSTOUNITARIO = COSTOUNITARIO
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA 
	WHERE	IDOPORTUNIDADLINEANEGOCIO	= @IDOPORTUNIDADLINEANEGOCIO 
	AND		IDSUBSERVICIO				= @IDSUBSERVICIO

	SELECT	@IDFLUJOCAJACONFIGURACION = IDFLUJOCAJACONFIGURACION
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION
	WHERE	IDFLUJOCAJA = @IDFLUJOCAJA

	/* 2. SE ESTA RECALCULANDO, ASI QUE DEBE ELIMINARSE LA INFORMACIÓN. */
	DELETE FROM OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE WHERE IDFLUJOCAJACONFIGURACION = @IDFLUJOCAJACONFIGURACION

	/* 3. REGISTRAR LA INFORMACIÓN DEL RECALCULO. */
	INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE
	(
		IDFLUJOCAJACONFIGURACION, ANIO, MES,
		MONTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION, TIPOFICHA
	)
	SELECT  @IDFLUJOCAJACONFIGURACION, ANIO, MES, SUM(A.MONTO * T7.MONTO), @IDESTADO,
			@IDUSUARIOCREACION, GETDATE(), @TIPOFICHA
	FROM (
		SELECT  T3.COMPONENTE, T2.ANIO, T2.MES, SUM(T2.MONTO) MONTO
		FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA				T1
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION	T5
		ON		T1.IDFLUJOCAJA = T5.IDFLUJOCAJA
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE			T2
		ON		T5.IDFLUJOCAJACONFIGURACION = T2.IDFLUJOCAJACONFIGURACION
		JOIN	OPORTUNIDAD.FORMULADETALLE						T3
		ON		T1.IDSUBSERVICIO = T3.IDSUBSERVICIO
		JOIN	OPORTUNIDAD.FORMULA								T4
		ON		T4.IDFORMULA		= T3.IDFORMULA
		JOIN	OPORTUNIDAD.SUBSERVICIODATOSCARATULA			T6
		ON		T6.IDFLUJOCAJA = T1.IDFLUJOCAJA
		WHERE	T1.IDOPORTUNIDADLINEANEGOCIO	= @IDOPORTUNIDADLINEANEGOCIO
		AND		T4.IDSUBSERVICIO				= @IDSUBSERVICIO
		AND		T6.IDTIPOENLACE					= T3.VALOR1 /* MAESTRA: IDRELACION = 89 / 1 = PRINCIPAL */
		AND		T1.IDGRUPO						= T3.VALOR2 /* GRUPO: CIRCUITOS */
		
		GROUP	BY T3.COMPONENTE, T2.ANIO, T2.MES
	)A
	JOIN	OPORTUNIDAD.OPORTUNIDADCOSTO					T7
	ON		T7.VELOCIDADSUBIDAKBPS			= A.ANIO
	WHERE	T7.IDOPORTUNIDADLINEANEGOCIO	= @IDOPORTUNIDADLINEANEGOCIO
	AND		T7.IDTIPOCOSTO					= 7			/* 7. COSTOS ASOCIADOS A INTERNET. */

	GROUP BY ANIO, MES
	ORDER BY ANIO, MES
	
END
GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_MANTENIMIENTOSATELITAL]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_MANTENIMIENTOSATELITAL]
(
	@IDOPORTUNIDAD		INT,
	@IDLINEANEGOCIO		INT,
	@IDSUBSERVICIO		INT,
	@IDUSUARIOCREACION	INT,
	@IDESTADO			INT,
	@TIPOFICHA			INT
)
AS
BEGIN

	DECLARE @IDFLUJOCAJACONFIGURACION	INT = 0,
			@IDFLUJOCAJA				INT = 0

	/* 1. OBTENER LOS ID DE LA CONFIGURACIÓN DE LO SERVICIOS. */
	SELECT  @IDFLUJOCAJA = ISNULL(IDFLUJOCAJA,0) 
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA 
	WHERE	IDOPORTUNIDAD	= @IDOPORTUNIDAD 
	AND		IDLINEANEGOCIO	= @IDLINEANEGOCIO 
	AND		IDSUBSERVICIO	= @IDSUBSERVICIO

	SELECT	@IDFLUJOCAJACONFIGURACION = IDFLUJOCAJACONFIGURACION
	FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION
	WHERE	IDFLUJOCAJA = @IDFLUJOCAJA

	/* 2. SE ESTA RECALCULANDO, ASI QUE DEBE ELIMINARSE LA INFORMACIÓN. */
	DELETE FROM OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE WHERE IDFLUJOCAJACONFIGURACION = @IDFLUJOCAJACONFIGURACION

	--/* 3. REGISTRAR LA INFORMACIÓN DEL RECALCULO. */
	INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE
	(
		IDFLUJOCAJACONFIGURACION, ANIO, MES,
		MONTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION, TIPOFICHA
	)
	SELECT  @IDFLUJOCAJACONFIGURACION, ANIO, MES, SUM(MONTO), @IDESTADO,
			@IDUSUARIOCREACION, GETDATE(), @TIPOFICHA
	FROM (
		SELECT  T3.COMPONENTE, T2.ANIO, T2.MES, SUM(T1.CANTIDAD*T3.VALOR1) MONTO
		FROM	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA T1
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION T5
		ON		T1.IDFLUJOCAJA = T5.IDFLUJOCAJA
		JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE T2
		ON		T5.IDFLUJOCAJACONFIGURACION = T2.IDFLUJOCAJACONFIGURACION
		JOIN	OPORTUNIDAD.FORMULADETALLE T3
		ON		T1.IDSUBSERVICIO = T3.IDSUBSERVICIO
		JOIN	OPORTUNIDAD.FORMULA T4
		ON		T4.IDFORMULA		= T3.IDFORMULA
		JOIN	OPORTUNIDAD.SUBSERVICIODATOSCAPEX T6
		ON		T6.IDFLUJOCAJA = T1.IDFLUJOCAJA
		WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
		AND		T1.IDLINEANEGOCIO	= @IDLINEANEGOCIO
		AND		T4.IDSUBSERVICIO	= @IDSUBSERVICIO
		GROUP	BY T3.COMPONENTE, T2.ANIO, T2.MES
	)A
	GROUP BY ANIO, MES
	ORDER BY ANIO, MES
	
END
GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_OPORTUNIDADFLUJOCAJADETALLE]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Registrar en la tabla OportunidadFlujoCajaDetalle, se prevee
	que este procedimiento se ejecute dentro de algun bucle.

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    07/08/2018  	Anthony Romero.	         Creación 
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_OPORTUNIDADFLUJOCAJADETALLE]
(
	@IdflujoCajaConfiguracion	INT,
	@Anho						INT,
	@Mes						INT,
	@Monto						DECIMAL(18,4),
	@IdUsuarioCreacion			INT,
	@TipoFicha					INT
)
AS
BEGIN
	DECLARE @Estado INT = 1;

	INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE
	(
		IDFLUJOCAJACONFIGURACION, ANIO, MES,
		MONTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION, TIPOFICHA
	)
	VALUES (
		@IdflujoCajaConfiguracion, @Anho, @Mes, @Monto, @Estado, @IdUsuarioCreacion, GETDATE(), @TipoFicha
	)

END
GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_SERVICIO]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Registrar en la tablas OportunidadFlujoCaja, OportunidadServicioCMI 
	la configuracion del servicio.

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_SERVICIO]
(
	@IdOportunidadLineaNegocio	INT,
	@IdServicio					INT,
	@IdUsuarioCreacion			INT,
	@IdEstado					INT
)
AS
BEGIN
declare @Caratula int=1,
		@Ecapex int=2,
		@IdLineaNegocio int=0
		
set @IdLineaNegocio = (select IdLineaNegocio from OPORTUNIDAD.OportunidadLineaNegocio where IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and IdEstado = @IdEstado)

declare @Servicio table(id int identity(1,1) ,IdCasoNegocio int,IdServicioSubServicio int,IdServicio int,IdSubServicio int,IdTipoCosto int,IdProveedor int, 
					ContratoMarco varchar(10),IdPeriodos int,Inicio int,Ponderacion decimal(18,2),IdMoneda int,IdGrupo int,FlagSISEGO int,IdServicioCMI int,IdPestana int,Concatenado varchar(100),
					AEReducido varchar(30),IdMedio int,IdTipoEnlace int)

insert INTO @Servicio (IdCasoNegocio,IdServicioSubServicio,IdServicio,IdSubServicio,IdTipoCosto,IdProveedor,
			ContratoMarco,IdPeriodos,Inicio,Ponderacion,IdMoneda,IdGrupo,FlagSISEGO,IdServicioCMI,IdPestana,Concatenado,AEReducido,IdMedio,IdTipoEnlace)		
select 0 IdCasoNegocio,T4.IdServicioSubServicio,T4.IdServicio,T4.IdSubServicio,T4.IdTipoCosto,T4.IdProveedor,
	T4.ContratoMarco,T4.IdPeriodos,T4.Inicio, T4.Ponderacion,T4.IdMoneda,T4.IdGrupo,T4.FlagSISEGO,T3.IdServicioCMI,P.IdPestana, ISNULL(T5.Concatenado,'')Concatenado,ISNULL(T5.AEReducido,'') AEReducido, T3.IdMedio,T3.IdTipoEnlace
from [COMUN].[Servicio] T3
	 JOIN [COMUN].[ServicioSubServicio] T4 on T3.IdServicio = T4.IdServicio
	 JOIN OPORTUNIDAD.PestanaGrupo P on T4.IdGrupo = P.IdGrupo
	 JOIN [COMUN].[SubServicio] T5 on T5.IdSubServicio = T4.IdSubServicio
WHERE T3.IdServicio = @IdServicio AND 
	  T3.IdLineaNegocio = @IdLineaNegocio AND 
	  T3.IdEstado = @IdEstado


declare @inicio int= 1,
		@Total int=0,
		@IdFlujoCaja int=0
		
set @Total = (select count(*) from @Servicio)

while(@inicio <= @Total)
begin

	INSERT INTO [OPORTUNIDAD].[OportunidadFlujoCaja] 
	(IdOportunidadLineaNegocio,IdTipoCosto,IdProveedor,IdPeriodos,IdPestana,IdGrupo,IdCasoNegocio,IdServicio,ContratoMarco,IdMoneda,FlagSISEGO,IdServicioCMI,IdEstado,IdUsuarioCreacion,FechaCreacion, IdSubServicio)
	SELECT @IdOportunidadLineaNegocio, T1.IdTipoCosto,T1.IdProveedor,T1.IdPeriodos,T1.IdPestana,T1.IdGrupo,T1.IdCasoNegocio, T1.IdServicio,T1.ContratoMarco,T1.IdMoneda,T1.FlagSISEGO,T1.IdServicioCMI,@IdEstado,@IdUsuarioCreacion,GETDATE(), t1.IdSubServicio
	FROM @Servicio T1 where id = @inicio

	set @IdFlujoCaja = @@IDENTITY

	INSERT INTO [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion]
	(IdFlujoCaja,Ponderacion,CostoPreOperativo,Inicio,Meses,IdEstado,IdUsuarioCreacion,FechaCreacion)
	SELECT @IdFlujoCaja,T1.Ponderacion,0.0,T1.Inicio,CASE WHEN(T1.IdPeriodos=4) THEN 1 ELSE 0 END,@IdEstado,@IdUsuarioCreacion,GETDATE()
	FROM @Servicio T1 where id = @inicio
	
	if((select count(*) FROM @Servicio T1 where T1.IdServicioCMI is not null and id = @inicio)>0)
	begin
		INSERT INTO [OPORTUNIDAD].[OportunidadServicioCMI] (IdOportunidadLineaNegocio,IdServicioCMI,Porcentaje,IdEstado,IdUsuarioCreacion,FechaCreacion)
		SELECT @IdOportunidadLineaNegocio, T1.IdServicioCMI,T1.Ponderacion,@IdEstado,@IdUsuarioCreacion,GETDATE()
		FROM @Servicio T1 where T1.IdServicioCMI is not null and id = @inicio
	end

	INSERT INTO OPORTUNIDAD.SubServicioDatosCaratula (IdFlujoCaja,IdMedio,IdTipoEnlace,IdEstado,IdUsuarioCreacion,FechaCreacion)
	SELECT @IdFlujoCaja,C.IdMedio,C.IdTipoEnlace,@IdEstado,@IdUsuarioCreacion,GETDATE()
	FROM @Servicio C 
	WHERE C.IdPestana = @Caratula and id = @inicio

	INSERT INTO OPORTUNIDAD.SubServicioDatosCapex (IdFlujoCaja,IdEstado,IdUsuarioCreacion,FechaCreacion,Cruce,AEReducido,Marca)
		SELECT @IdFlujoCaja,@IdEstado,@IdUsuarioCreacion,GETDATE(),C.Concatenado,C.AEReducido,''
	FROM @Servicio C 
	WHERE C.IdPestana = @Ecapex and id = @inicio

	set @inicio= @inicio+1;

end

END



GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_INSERTAR_VERSIONOPORTUNIDAD]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [OPORTUNIDAD].[USP_INSERTAR_VERSIONOPORTUNIDAD]
(
	@IDOPORTUNIDAD		INT,
	@IDESTADO			INT,
	@IDUSUARIOCREACION	INT
)
AS
BEGIN
	DECLARE @IDOPORTUNIDADNUEVO INT,
			@ESTADOACTIVO		INT = 1,
			@VERSIONNUEVO		INT,
			@AGRUPADOR			INT


	SELECT	@VERSIONNUEVO	= MAX(VERSION) + 1 FROM OPORTUNIDAD.OPORTUNIDAD WHERE IDOPORTUNIDAD = @IDOPORTUNIDAD
	
	/*1. SE OBTIENE EL AGRUPADOR [ID DE LA OPORTUNIDAD CON LA VERSIÓN 1]. */
	SELECT  @AGRUPADOR = AGRUPADOR FROM OPORTUNIDAD.OPORTUNIDAD WHERE IDOPORTUNIDAD = @IDOPORTUNIDAD

	INSERT INTO OPORTUNIDAD.OPORTUNIDAD
	(IDTIPOEMPRESA, IDCLIENTE, DESCRIPCION, NUMEROSALESFORCE, NUMEROCASO, FECHA, ALCANCE, PERIODO, TIEMPOIMPLANTACION,
	IDTIPOPROYECTO, IDTIPOSERVICIO, IDPROYECTOANTERIOR, IDESTADO, IDANALISTAFINANCIERO, IDPRODUCTMANAGER, IDPREVENTA, IDCOORDINADORFINANCIERO,
	TIEMPOPROYECTO, IDTIPOCAMBIO, AGRUPADOR, VERSION, VERSIONPADRE, FLAGGANADOR, IDMONEDAFACTURACION, IDUSUARIOCREACION, FECHACREACION)
	SELECT	IDTIPOEMPRESA, IDCLIENTE, DESCRIPCION, NUMEROSALESFORCE, NUMEROCASO, FECHA, ALCANCE, PERIODO, TIEMPOIMPLANTACION,
			IDTIPOPROYECTO, IDTIPOSERVICIO, IDPROYECTOANTERIOR, IDESTADO, IDANALISTAFINANCIERO, IDPRODUCTMANAGER, IDPREVENTA, IDCOORDINADORFINANCIERO,
			TIEMPOPROYECTO, IDTIPOCAMBIO, @AGRUPADOR, @VERSIONNUEVO, VERSION, FLAGGANADOR, IDMONEDAFACTURACION, IDUSUARIOCREACION, FECHACREACION
	FROM	OPORTUNIDAD.OPORTUNIDAD
	WHERE	IDOPORTUNIDAD = @IDOPORTUNIDAD

	SELECT @IDOPORTUNIDADNUEVO = MAX(IDOPORTUNIDAD) FROM OPORTUNIDAD.OPORTUNIDAD 

	/* 2. OPORTUNIAD - LINEA DE NEGOCIO */
	INSERT INTO OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO
	(IDOPORTUNIDAD,	IDLINEANEGOCIO,	IDESTADO, IDUSUARIOCREACION,	FECHACREACION)
	SELECT	@IDOPORTUNIDADNUEVO, IDLINEANEGOCIO, IDESTADO, IDUSUARIOCREACION, FECHACREACION
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO
	WHERE	IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		IDESTADO		= @ESTADOACTIVO

	
	--/* 3. OPORTUNIAD - DOCUMENTOS */
	INSERT INTO OPORTUNIDAD.OPORTUNIDADDOCUMENTO
	(IDOPORTUNIDADLINEANEGOCIO, TIPODOCUMENTO, RUTADOCUMENTO, DESCRIPCION,
	 IDTIPODOCUMENTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION)
	SELECT	DISTINCT T2.IDOPORTUNIDADLINEANEGOCIO, T3.TIPODOCUMENTO, T3.RUTADOCUMENTO, T3.DESCRIPCION,
			T3.IDTIPODOCUMENTO, T3.IDESTADO, @IDUSUARIOCREACION, GETDATE()
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T1
	JOIN	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T2	ON	T1.IDLINEANEGOCIO = T2.IDLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADDOCUMENTO	T3	ON	T3.IDOPORTUNIDADLINEANEGOCIO = T1.IDOPORTUNIDADLINEANEGOCIO
	WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		T2.IDOPORTUNIDAD	= @IDOPORTUNIDADNUEVO
	AND		T3.IDFLUJOCAJA		IS NULL
	AND		T3.IDESTADO			= @ESTADOACTIVO

	--/* 4. OPORTUNIAD - COSTO */

	INSERT INTO OPORTUNIDAD.OPORTUNIDADCOSTO
	(IDOPORTUNIDADLINEANEGOCIO, IDCOSTO, IDTIPOCOSTO, DESCRIPCION, MONTO, VELOCIDADSUBIDAKBPS, 
	 PORCENTAJEGARANTIZADO, PORCENTAJESOBRESUSCRIPCION, COSTOSEGMENTOSATELITAL, INVANTENAHUBUSD, ANTENACASACLIENTEUSD, 
	 INSTALACION, IDUNIDADCONSUMO, IDTIPIFICACION, CODIGOMODELO, MODELO, IDESTADO, IDUSUARIOCREACION, FECHACREACION)
	SELECT	DISTINCT T2.IDOPORTUNIDADLINEANEGOCIO, T1.IDCOSTO, T1.IDTIPOCOSTO, T1.DESCRIPCION, T1.MONTO, T1.VELOCIDADSUBIDAKBPS, 
			T1.PORCENTAJEGARANTIZADO, T1.PORCENTAJESOBRESUSCRIPCION, T1.COSTOSEGMENTOSATELITAL, T1.INVANTENAHUBUSD, 
			T1.ANTENACASACLIENTEUSD, T1.INSTALACION, T1.IDUNIDADCONSUMO, T1.IDTIPIFICACION, T1.CODIGOMODELO, T1.MODELO, 
			T1.IDESTADO, @IDUSUARIOCREACION, GETDATE()
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T3
	JOIN	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T2		ON T3.IDLINEANEGOCIO = T2.IDLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADCOSTO	T1
	ON		T1.IDOPORTUNIDADLINEANEGOCIO = T3.IDOPORTUNIDADLINEANEGOCIO
	WHERE	T3.IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		T2.IDOPORTUNIDAD	= @IDOPORTUNIDADNUEVO
	AND		T1.IDESTADO			= @ESTADOACTIVO


	INSERT INTO OPORTUNIDAD.OPORTUNIDADTIPOCAMBIO
	(IDOPORTUNIDADLINEANEGOCIO,	IDESTADO, IDUSUARIOCREACION, FECHACREACION)
	SELECT	DISTINCT T2.IDOPORTUNIDADLINEANEGOCIO, T3.IDESTADO, @IDUSUARIOCREACION, GETDATE()
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T1
	JOIN	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T2	ON	T1.IDLINEANEGOCIO = T2.IDLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADTIPOCAMBIO	T3	ON	T3.IDOPORTUNIDADLINEANEGOCIO = T1.IDOPORTUNIDADLINEANEGOCIO
	WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		T2.IDOPORTUNIDAD	= @IDOPORTUNIDADNUEVO
	AND		T3.IDESTADO			= @ESTADOACTIVO


	INSERT INTO OPORTUNIDAD.OPORTUNIDADTIPOCAMBIODETALLE
	(IDTIPOCAMBIOOPORTUNIDAD, IDMONEDA, IDTIPIFICACION, ANIO, MONTO, IDESTADO, IDUSUARIOCREACION, FECHACREACION)
	SELECT	DISTINCT T5.IDTIPOCAMBIOOPORTUNIDAD, T4.IDMONEDA, T4.IDTIPIFICACION, T4.ANIO, T4.MONTO, T4.IDESTADO, @IDUSUARIOCREACION, GETDATE()
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T1
	JOIN	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T2	ON	T1.IDLINEANEGOCIO = T2.IDLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADTIPOCAMBIO	T3	ON	T3.IDOPORTUNIDADLINEANEGOCIO = T1.IDOPORTUNIDADLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADTIPOCAMBIO	T5	ON	T5.IDOPORTUNIDADLINEANEGOCIO = T2.IDOPORTUNIDADLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADTIPOCAMBIODETALLE T4 ON T4.IDTIPOCAMBIOOPORTUNIDAD = T3.IDTIPOCAMBIOOPORTUNIDAD
	WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		T2.IDOPORTUNIDAD	= @IDOPORTUNIDADNUEVO
	AND		T3.IDESTADO			= @ESTADOACTIVO	


	INSERT INTO OPORTUNIDAD.OPORTUNIDADSERVICIOCMI
	(IDOPORTUNIDADLINEANEGOCIO, IDSERVICIOCMI, PORCENTAJE, IDANALISTA, IDESTADO, IDUSUARIOCREACION, FECHACREACION)
	SELECT	DISTINCT T2.IDOPORTUNIDADLINEANEGOCIO, T3.IDSERVICIOCMI, T3.PORCENTAJE, T3.IDANALISTA, T3.IDESTADO, @IDUSUARIOCREACION, GETDATE()
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T1
	JOIN	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T2	ON	T1.IDLINEANEGOCIO = T2.IDLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADSERVICIOCMI  T3	ON	T3.IDOPORTUNIDADLINEANEGOCIO = T1.IDOPORTUNIDADLINEANEGOCIO
	WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		T2.IDOPORTUNIDAD	= @IDOPORTUNIDADNUEVO
	AND		T3.IDESTADO			= @ESTADOACTIVO	

	INSERT INTO OPORTUNIDAD.OPORTUNIDADFLUJOCAJA
	(IDOPORTUNIDADLINEANEGOCIO, IDAGRUPADOR, IDTIPOCOSTO, DESCRIPCION, IDPROVEEDOR, IDPERIODOS,
	IDPESTANA, IDGRUPO, IDCASONEGOCIO, IDSERVICIO, CANTIDAD, COSTOUNITARIO, CONTRATOMARCO, IDMONEDA, FLAGSISEGO,
	IDSERVICIOCMI, IDESTADO, IDUSUARIOCREACION, FECHACREACION)
	SELECT	T2.IDOPORTUNIDADLINEANEGOCIO, IDAGRUPADOR, T3.IDTIPOCOSTO, T3.DESCRIPCION, T3.IDPROVEEDOR, T3.IDPERIODOS,
			T3.IDPESTANA, T3.IDGRUPO, T3.IDCASONEGOCIO, T3.IDSERVICIO, T3.CANTIDAD, T3.COSTOUNITARIO, T3.CONTRATOMARCO, T3.IDMONEDA, T3.FLAGSISEGO,
			T3.IDSERVICIOCMI, T3.IDESTADO, @IDUSUARIOCREACION, GETDATE()
	FROM	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T1
	JOIN	OPORTUNIDAD.OPORTUNIDADLINEANEGOCIO T2	ON	T1.IDLINEANEGOCIO = T2.IDLINEANEGOCIO
	JOIN	OPORTUNIDAD.OPORTUNIDADFLUJOCAJA	T3	ON	T1.IDOPORTUNIDADLINEANEGOCIO = T3.IDOPORTUNIDADLINEANEGOCIO
	WHERE	T1.IDOPORTUNIDAD	= @IDOPORTUNIDAD
	AND		T2.IDOPORTUNIDAD	= @IDOPORTUNIDADNUEVO
	AND		T3.IDESTADO			= @ESTADOACTIVO	

	--OPORTUNIDAD.OPORTUNIDADFLUJOCAJA
	--OPORTUNIDAD.SUBSERVICIODATOSCAPEX
	--OPORTUNIDAD.SUBSERVICIODATOSCARATULA
	--OPORTUNIDAD.OPORTUNIDADFLUJOCAJACONFIGURACION
	--OPORTUNIDAD.OPORTUNIDADFLUJOCAJADETALLE

	--OPORTUNIDAD.LINEAOPORTUNIDAD
	
END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_LISTA_OPORTUNIDAD_CAB]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURe [OPORTUNIDAD].[USP_LISTA_OPORTUNIDAD_CAB]
(
	@pIdLineaNegocio	INT,
	@pIDCLIENTE			INT,
	@pDESCRIPCION		VARCHAR(200),
	@pNUMEROSALESFORCE	VARCHAR(20),
	@pIDGERENTE			INT,
	@pIDDIRECCION		INT,
	@pIDESTADO			INT
	
	--
	
)
AS
----------------------------------------------------
--@Test: EXEC [OPORTUNIDAD].[STF_LISTAPROYECTOS_CAB]  0,0,'','',0,0,0
----------------------------------------------------
BEGIN



	SELECT	 T1.IdOportunidad,
	         --,T1.FechaCreacion
			 T1.IdPreVenta,
			 T1.IdCoordinadorFinanciero,
			T1.IdAnalistaFinanciero 
			 ,T8.FechaEdicion
			,isnull(T1.DESCRIPCION,'') Oportunidad
			,T8.IdLineaNegocio
			,T2.DESCRIPCION LineaProducto
			,T1.IdTipoEmpresa
			,T3.DESCRIPCION TipoEmpresa
			,T1.NumeroSalesForce
			,isnull(T1.NumeroCaso,'')
			,T1.IdCliente
			,T4.Descripcion Cliente
			,T4.IdSector
			,T5.Descripcion Sector
			,T9.GerenteComercial
			,T6.IdDireccion
			,isnull(T1.Descripcion,'') DireccionComercial
			--,T6.NOMBRE + ' ' + APEPATERNO  + ' ' +  APEMATERNO
			,0.0 OIBDA
			,0.0 VANProyecto
			,0.0 VANVAIProyecto
			,0.0 PayBackProyecto
			,0.0 ValorRescate
		--t1.IDESTADO  =
			--min(IdEstado) 
			,IdEstado= COALESCE((select min(IdEstado) from Oportunidad where agrupador = T1.Agrupador AND IdEstado <> 0) ,(select min(IdEstado) from Oportunidad where agrupador = T1.Agrupador AND IdEstado = 0),null)
			,Estado = (select top 1 (DESCRIPCION )from [COMUN].[Maestra] where IdRelacion=11 AND Valor =COALESCE((select min(IdEstado) from Oportunidad where agrupador = T1.Agrupador AND IdEstado <> 0) ,(select min(IdEstado) from Oportunidad where agrupador = T1.Agrupador AND IdEstado = 0),null))
  
                         
--	7.DESCRIPCION Estado
			,T1.VersionPadre
			,T1.Version	
			,T1.Agrupador
			,[Versiones]= (select  count(Version)from Oportunidad where agrupador = T1.Agrupador ) 
			,[FechaCreacion] = (select top 1 FechaCreacion from Oportunidad where agrupador = T1.Agrupador order by version desc) 
	FROM	Oportunidad T1
	JOIN	OportunidadLineaNegocio T8
	ON		T1.IdOportunidad = T8.IdOportunidad
	
	JOIN	[COMUN].[LineaNegocio] T2
	ON		T2.IdLineaNegocio = T8.IdLineaNegocio
	JOIN	[COMUN].[Maestra] T3
	ON		T3.VALOR = T1.IDTIPOEMPRESA
	JOIN	[COMUN].[Cliente]T4
	ON		T4.IDCLIENTE = T1.IDCLIENTE
	JOIN	[COMUN].[Sector] T5
	ON		T5.IDSECTOR = T4.IDSECTOR
	JOIN	[COMUN].[Cliente] T9
	ON		T9.IDCLIENTE = T1.IDCLIENTE
	JOIN	[COMUN].[DireccionComercial] T6
	ON		T6.IDDIRECCION	= T9.IdDireccionComercial
	JOIN	[COMUN].[Maestra] T7
	ON		T7.IdEstado = 1
	AND		T7.VALOR = T1.IDESTADO
	WHERE	T3.IDRELACION = 19 /*TIPOS DE PROYECTO*/ 
	AND		T7.IDRELACION = 11
	--AND		(isnull(T1.Agrupador,0) = 0 OR isnull(T1.FlagGanador,0)=1)
	AND		(@pIdLineaNegocio IS NULL OR @pIdLineaNegocio = 0 OR T2.IdLineaNegocio = @pIdLineaNegocio)
	AND		(@pIDCLIENTE IS NULL OR @pIDCLIENTE = 0 OR T1.IDCLIENTE = @pIDCLIENTE)
	--AND		(@pIDGERENTE IS NULL OR @pIDGERENTE = 0 OR T1.IDGERENTE = @pIDGERENTE)
	AND		(@pIDDIRECCION IS NULL OR @pIDDIRECCION = 0 OR T6.IDDIRECCION = @pIDDIRECCION)	
	AND		(@pDESCRIPCION ='' OR (T1.DESCRIPCION LIKE '%' + @pDESCRIPCION + '%'))	
	AND		(@pNUMEROSALESFORCE ='' OR (T1.NUMEROSALESFORCE LIKE '%' + @pNUMEROSALESFORCE + '%'))
	AND		(@pIDESTADO IS NULL OR @pIDESTADO = 0 OR T1.IdEstado = @pIDESTADO)
	--AND		(T1.IdEstado = @pIDESTADO)
	AND		(T1.VersionPadre =T1.IdOportunidad)
	
	 AND (T1.Version=1)
	
ORDER BY T2.DESCRIPCION	


END




GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_LISTAR_CONCEPTOS_LINEA]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [OPORTUNIDAD].[USP_LISTAR_CONCEPTOS_LINEA]
@IdLineaProducto INT,
@IdEstado INT
AS
BEGIN
/*
Descripción   : Obtener los conceptos por linea de negocio
Modificaciones:
***********************************************************
Fecha           	Autor         		Acción
***********************************************************
 26/17/2018  	Karina Gomez	        Creación 
*/

select T1.IdSubServicio, 
	   T1.Descripcion as Concepto,
	   0 IdPestana,
	   '' as Pestana,
	   0 IdGrupo,
	   '' Grupo
from [COMUN].SubServicio T1
where T1.IdEstado = @IdEstado

--select lcg.IdConcepto, 
--	   c.Descripcion as Concepto,
--	   lcg.IdPestana,
--	   lp.Descripcion Pestana,
--	   lcg.IdGrupo,
--	   pg.Descripcion Grupo
--from [OPORTUNIDAD].LineaConceptoGrupo lcg 
--join [OPORTUNIDAD].LineaPestana lp on lcg.IdEstado = lp.IdEstado and
--						lcg.IdLineaProducto = lp.IdLineaProducto and
--						lcg.IdPestana = lp.IdPestana
--join [OPORTUNIDAD].pestanagrupo pg on lcg.IdEstado = pg.IdEstado and
--						lcg.IdPestana = pg.IdPestana and 
--						lcg.IdGrupo = pg.IdGrupo
--join [COMUN].SubServicio c on lcg.IdEstado = c.IdEstado and
--				   lcg.IdConcepto = c.IdConcepto
--where lcg.IdLineaProducto = @IdLineaProducto	and
--	  lcg.IdEstado = @IdEstado
	
END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_LISTAR_DETALLE_OPORTUNIDAD]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Lista la configuraciones para las grillas de caratula y ecapex

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
	***********************************************************
	@Test: exec [OPORTUNIDAD].[USP_LISTAR_DETALLE_OPORTUNIDAD] 1,5,1,1,4
	exec [OPORTUNIDAD].[USP_LISTAR_DETALLE_OPORTUNIDAD] 1,5,1,2,6
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_LISTAR_DETALLE_OPORTUNIDAD]
(
	@IdOportunidad				INT,
	@IdLineaNegocio				INT,
	@IdEstado   				INT,
	@IdPestana   				INT,
	@IdGrupo   					INT
)
AS
BEGIN
if	(@IdPestana=1)
begin
	
	SELECT T1.IdOportunidadLineaNegocio,
		   T2.IdLineaNegocio,
		   T3.IdServicioSubServicio,
		   T1.IdFlujoCaja, 
		   T3.IdSubServicio,
		   T4.Descripcion SubServicio,
		   ISNULL(T4.CostoInstalacion,0) CostoInstalacion, 
		   T1.IdProveedor,
		   T4.Descripcion Proveedor,
		   T1.IdTipoCosto,
		   T7.Descripcion TipoCosto,
		   T1.IdPeriodos,
		   T8.Descripcion Periodos,
		   ISNULL(T1.IdPestana,0)IdPestana,
		   T1.IdGrupo,
		   T1.IdCasoNegocio,
		   T1.IdServicio,
		   ISNULL(T1.CostoUnitario,0.0)CostoUnitario,
		   T1.IdEstado,
		   T9.Descripcion Estado,
			ISNULL(T10.IdServicioCMI,0) IdServicioCMI,
			ISNULL(T11.DescripcionPlantilla,'') ServicioCMI,
			0 IdAgrupador,
			ISNULL(T1.IdTipoCosto,0) IdTipoCosto,
			ISNULL(T3.Ponderacion,0) Ponderacion,
			ISNULL(T12.CostoPreOperativo,0.0)CostoPreOperativo,
			ISNULL(T12.Inicio,0)Inicio,
			ISNULL(T12.Meses,0) Meses,
			ISNULL(T6.IdSubServicioDatosCaratula,0) IdSubServicioDatosCaratula,
			ISNULL(T6.Circuito,'')Circuito,
			ISNULL(T1.Cantidad,0) AS Cantidad,
			ISNULL(T6.IdOportunidadCosto,0) IdOportunidadCosto,
			ISNULL(T6.IdMedio,0) IdMedio,	
			ISNULL(T6.IdTipoEnlace,0) IdTipoEnlace,
			ISNULL(T6.IdActivoPasivo,0) IdActivoPasivo,
			ISNULL(T6.IdLocalidad,0) IdLocalidad,
			ISNULL(T6.NumeroMeses,0)NumeroMeses,
			ISNULL(T6.MontoUnitarioMensual,0.0)MontoUnitarioMensual,
			ISNULL(T6.MontoTotalMensual,0.0)MontoTotalMensual,
			ISNULL(T6.NumeroMesInicioGasto,0)NumeroMesInicioGasto,
			ISNULL(T6.FlagRenovacion,'')FlagRenovacion,
			ISNULL(T6.Instalacion,0.0)Instalacion,
			ISNULL(T6.Desinstalacion,0.0)Desinstalacion,
			ISNULL(T6.PU,0.0),
			ISNULL(T6.Alquiler,0.0)Alquiler,
			ISNULL(T6.Factor,0.0)Factor,
			ISNULL(T6.ValorCuota,0.0)ValorCuota,
			ISNULL(T6.CC,0.0)CC,
			ISNULL(T6.CCQProvincia,0.0)CCQProvincia,
			ISNULL(T6.CCQBK,0.0)CCQBK,
			ISNULL(T6.CCQCAPEX,0.0)CCQCAPEX,																																			
			ISNULL(T6.TIWS,0.0)TIWS,
			ISNULL(T6.RADIO,0.0)RADIO,
			ISNULL(T13.Descripcion,'') Medio,
			ISNULL(T14.Descripcion,'') TipoEnlace,
			ISNULL(T15.Descripcion,'') ActivoPasivo,
			ISNULL(T16.Descripcion,'') Localidad,
			ISNULL(T17.Descripcion,'') Costo,
			ISNULL(T17.VelocidadSubidaKBPS,0.0) VelocidadSubidaKBPS,
			ISNULL(T17.Monto,0.0) MontoCosto,
			ISNULL(T17.CodigoModelo,'')CodigoModelo,
			ISNULL(T17.Modelo,'') Modelo
	FROM [OPORTUNIDAD].[OportunidadFlujoCaja] T1  
	JOIN [OPORTUNIDAD].[OportunidadLineaNegocio] T2 ON T1.IdOportunidadLineaNegocio = T2.IdOportunidadLineaNegocio
	JOIN COMUN.ServicioSubServicio T3 ON T1.IdServicio = T3.IdServicio 
	JOIN COMUN.Servicio T10 ON T10.IdServicio = T3.IdServicio 
	JOIN COMUN.SubServicio T4 ON T4.IdSubservicio = T3.IdSubservicio 
	LEFT JOIN COMUN.Proveedor T5 ON T5.IdProveedor = T1.IdProveedor
	JOIN OPORTUNIDAD.SubServicioDatosCaratula T6 ON T6.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN [COMUN].[ServicioCMI] T11 ON T11.IdServicioCMI = T10.IdServicioCMI
	JOIN [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] T12 ON T12.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN OPORTUNIDAD.OportunidadCosto T17 ON T17.IdOportunidadLineaNegocio = T1.IdOportunidadLineaNegocio
	 JOIN COMUN.MAESTRA T7 ON T7.IdRelacion = 4 and T7.Valor = T1.IdTipoCosto
	 JOIN COMUN.MAESTRA T8 ON T8.IdRelacion = 28 and T8.Valor = T1.IdPeriodos
	 JOIN COMUN.MAESTRA T9 ON T9.IdRelacion = 1 and T9.Valor = T1.IdEstado
	 JOIN COMUN.MAESTRA T13 ON T13.IdRelacion = 84 and T13.Valor = T6.IdMedio
	 JOIN COMUN.MAESTRA T14 ON T14.IdRelacion = 89 and T14.Valor = T6.IdTipoEnlace
	 LEFT JOIN COMUN.MAESTRA T15 ON T15.IdRelacion = 92 and T15.Valor = T6.IdActivoPasivo
	 LEFT JOIN COMUN.MAESTRA T16 ON T16.IdRelacion = 95 and T16.Valor = T6.IdLocalidad
	WHERE T2.IdOportunidad = @IdOportunidad and
		  T2.IdLineaNegocio = @IdLineaNegocio and
		  T1.IdPestana = @IdPestana and
		  T1.IdGrupo  =  @IdGrupo and
		  T1.IdEstado = @IdEstado 
end

if	(@IdPestana=2)
begin

	SELECT T1.IdOportunidadLineaNegocio,
		   T2.IdLineaNegocio,
		   T3.IdServicioSubServicio,
		   T1.IdFlujoCaja, 
		   T3.IdSubServicio,
		   T4.Descripcion SubServicio,
		   ISNULL(T4.CostoInstalacion,0.0) CostoInstalacion, 
		   ISNULL(T6.Cruce,'') Cruce,
		   ISNULL(T6.AEReducido,'') AEReducido,
		   T1.IdProveedor,
		   T4.Descripcion Proveedor,
		   T1.IdTipoCosto,
		   T7.Descripcion TipoCosto,
		   T1.IdPeriodos,
		   T8.Descripcion Periodos,
		   ISNULL(T1.IdPestana,0)IdPestana,
		   T1.IdGrupo,
		   T1.IdCasoNegocio,
		   T1.IdServicio,
		   ISNULL(T1.CostoUnitario,0.0)CostoUnitario,
		   T1.IdEstado,
		   T9.Descripcion Estado,
			ISNULL(T10.IdServicioCMI,0) IdServicioCMI,
			ISNULL(T11.DescripcionPlantilla,'') ServicioCMI,
			0 IdAgrupador,
			ISNULL(T1.IdTipoCosto,0) IdTipoCosto,
			ISNULL(T3.Ponderacion,0.0) Ponderacion,
			ISNULL(T12.CostoPreOperativo,0.0)CostoPreOperativo,
			ISNULL(T12.Inicio,0)Inicio,
			ISNULL(T12.Meses,0) Meses,
			ISNULL(T6.IdSubServicioDatosCapex,0) IdSubServicioDatosCapex,
			ISNULL(T6.Circuito,'')Circuito,
			ISNULL(T6.SISEGO,'')SISEGO,
			ISNULL(T6.MesesAntiguedad,0) MesesAntiguedad,
			ISNULL(T1.Cantidad,0) AS Cantidad,
			ISNULL(T6.CostoUnitarioAntiguo,0.0)CostoUnitarioAntiguo,
			ISNULL(T6.ValorResidualSoles,0.0)ValorResidualSoles,
			ISNULL(T6.CostoUnitario,0.0)CostoUnitario,
			ISNULL(T6.CapexDolares,0.0)CapexDolares,
			ISNULL(T6.CapexSoles,0.0)CapexSoles,
			ISNULL(T6.TotalCapex,0.0)TotalCapex,
			ISNULL(T6.AnioRecupero,0)AnioRecupero,
			ISNULL(T6.MesRecupero,0)MesRecupero,
			ISNULL(T6.AnioComprometido,0)AnioComprometido,
			ISNULL(T6.MesComprometido,0)MesComprometido,
			ISNULL(T6.AnioCertificado,0)AnioCertificado,
			ISNULL(T6.MesCertificado,0)MesCertificado,
			ISNULL(T6.Medio,'')Medio,
			ISNULL(T6.IdTipo,0) IdTipo,
			ISNULL(T6.IdOportunidadCosto,0) IdOportunidadCosto,
			ISNULL(T6.Garantizado,'') Garantizado,
			ISNULL(T6.Modelo,'')Modelo,
			ISNULL(T6.Combo,'')Combo,
			ISNULL(T6.Tipo,'')TipoEquipo,
			ISNULL(T6.Marca,'')Marca,
			ISNULL(T6.CapexInstalacion,0.0) Instalacion,
			ISNULL(T15.Descripcion,'') Costo,
			ISNULL(T15.VelocidadSubidaKBPS,0.0) VelocidadSubidaKBPS,
			ISNULL(T15.Monto,0.0) MontoCosto,
			ISNULL(T15.CodigoModelo,'')CodigoModelo,
			ISNULL(T15.Modelo,'') Modelo
	FROM [OPORTUNIDAD].[OportunidadFlujoCaja] T1  
	JOIN [OPORTUNIDAD].[OportunidadLineaNegocio] T2 ON T1.IdOportunidadLineaNegocio = T2.IdOportunidadLineaNegocio
	JOIN COMUN.ServicioSubServicio T3 ON T1.IdServicio = T3.IdServicio
	JOIN COMUN.Servicio T10 ON T10.IdServicio = T3.IdServicio  
	JOIN COMUN.SubServicio T4 ON T4.IdSubservicio = T3.IdSubservicio 
	LEFT JOIN COMUN.Proveedor T5 ON T5.IdProveedor = T1.IdProveedor
	JOIN OPORTUNIDAD.SubServicioDatosCapex T6 ON T6.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN [COMUN].[ServicioCMI] T11 ON T11.IdServicioCMI = T10.IdServicioCMI
	JOIN [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] T12 ON T12.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN OPORTUNIDAD.OportunidadCosto T15 ON T15.IdOportunidadLineaNegocio = T1.IdOportunidadLineaNegocio
	 JOIN COMUN.MAESTRA T7 ON T7.IdRelacion = 4 and T7.Valor = T1.IdTipoCosto
	 JOIN COMUN.MAESTRA T8 ON T8.IdRelacion = 28 and T8.Valor = T1.IdPeriodos
	 JOIN COMUN.MAESTRA T9 ON T9.IdRelacion = 1 and T9.Valor = T1.IdEstado
	WHERE T2.IdOportunidad = @IdOportunidad and
		  T2.IdLineaNegocio = @IdLineaNegocio and
		  T1.IdPestana = @IdPestana and
		  T1.IdGrupo  =  @IdGrupo and
		  T1.IdEstado = @IdEstado 

end

END


GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Lista la configuraciones para las grillas de caratula

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
	***********************************************************
	@Test: exec [OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA] 2,1,2
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_CARATULA]
(
	@IdOportunidadLineaNegocio	INT,
	@IdEstado   				INT,
	@IdGrupo   					INT
)
AS
BEGIN

	declare @IdPestana INT=1;
	
	SELECT T1.IdOportunidadLineaNegocio,
		   T2.IdLineaNegocio,
		   T3.IdServicioSubServicio,
		   T1.IdFlujoCaja, 
		   T3.IdSubServicio,
		   T4.Descripcion SubServicio,
		   ISNULL(T4.CostoInstalacion,0) CostoInstalacion, 
		   T1.IdProveedor,
		   T4.Descripcion Proveedor,
		   T1.IdTipoCosto,
		   T7.Descripcion TipoCosto,
		   T1.IdPeriodos,
		   T8.Descripcion Periodos,
		   ISNULL(T1.IdPestana,0)IdPestana,
		   T1.IdGrupo,
		   T1.IdCasoNegocio,
		   T1.IdServicio,
		   ISNULL(T1.CostoUnitario,0.0)CostoUnitario,
		   T1.IdEstado,
		   T9.Descripcion Estado,
			ISNULL(T10.IdServicioCMI,0) IdServicioCMI,
			ISNULL(T11.DescripcionPlantilla,'') ServicioCMI,
			0 IdAgrupador,
			ISNULL(T1.IdTipoCosto,0) IdTipoCosto,
			ISNULL(T3.Ponderacion,0) Ponderacion,
			ISNULL(T12.CostoPreOperativo,0.0)CostoPreOperativo,
			ISNULL(T12.Inicio,0)Inicio,
			ISNULL(T12.Meses,0) Meses,
			ISNULL(T6.IdSubServicioDatosCaratula,0) IdSubServicioDatosCaratula,
			ISNULL(T6.Circuito,'')Circuito,
			ISNULL(T1.Cantidad,0) AS Cantidad,
			ISNULL(T6.IdOportunidadCosto,0) IdOportunidadCosto,
			ISNULL(T6.IdMedio,0) IdMedio,	
			ISNULL(T6.IdTipoEnlace,0) IdTipoEnlace,
			ISNULL(T6.IdActivoPasivo,0) IdActivoPasivo,
			ISNULL(T6.IdLocalidad,0) IdLocalidad,
			ISNULL(T6.NumeroMeses,0)NumeroMeses,
			ISNULL(T6.MontoUnitarioMensual,0.0)MontoUnitarioMensual,
			ISNULL(T6.MontoTotalMensual,0.0)MontoTotalMensual,
			ISNULL(T6.NumeroMesInicioGasto,0)NumeroMesInicioGasto,
			ISNULL(T6.FlagRenovacion,'')FlagRenovacion,
			ISNULL(T6.Instalacion,0.0)Instalacion,
			ISNULL(T6.Desinstalacion,0.0)Desinstalacion,
			ISNULL(T6.PU,0.0),
			ISNULL(T6.Alquiler,0.0)Alquiler,
			ISNULL(T6.Factor,0.0)Factor,
			ISNULL(T6.ValorCuota,0.0)ValorCuota,
			ISNULL(T6.CC,0.0)CC,
			ISNULL(T6.CCQProvincia,0.0)CCQProvincia,
			ISNULL(T6.CCQBK,0.0)CCQBK,
			ISNULL(T6.CCQCAPEX,0.0)CCQCAPEX,																																			
			ISNULL(T6.TIWS,0.0)TIWS,
			ISNULL(T6.RADIO,0.0)RADIO,
			ISNULL(T13.Descripcion,'') Medio,
			ISNULL(T14.Descripcion,'') TipoEnlace,
			ISNULL(T15.Descripcion,'') ActivoPasivo,
			ISNULL(T16.Descripcion,'') Localidad,
			ISNULL(T17.Descripcion,'') Costo,
			ISNULL(T17.VelocidadSubidaKBPS,0.0) VelocidadSubidaKBPS,
			ISNULL(T17.Monto,0.0) MontoCosto,
			ISNULL(T17.CodigoModelo,'')CodigoModelo,
			ISNULL(T17.Modelo,'') Modelo
	FROM [OPORTUNIDAD].[OportunidadFlujoCaja] T1  
	JOIN [OPORTUNIDAD].[OportunidadLineaNegocio] T2 ON T1.IdOportunidadLineaNegocio = T2.IdOportunidadLineaNegocio
	JOIN COMUN.ServicioSubServicio T3 ON T1.IdServicio = T3.IdServicio 
	JOIN COMUN.Servicio T10 ON T10.IdServicio = T3.IdServicio 
	JOIN COMUN.SubServicio T4 ON T4.IdSubservicio = T3.IdSubservicio 
	LEFT JOIN COMUN.Proveedor T5 ON T5.IdProveedor = T1.IdProveedor
	JOIN OPORTUNIDAD.SubServicioDatosCaratula T6 ON T6.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN [COMUN].[ServicioCMI] T11 ON T11.IdServicioCMI = T10.IdServicioCMI
	JOIN [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] T12 ON T12.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN OPORTUNIDAD.OportunidadCosto T17 ON T17.IdOportunidadLineaNegocio = T1.IdOportunidadLineaNegocio
	 JOIN COMUN.MAESTRA T7 ON T7.IdRelacion = 4 and T7.Valor = T1.IdTipoCosto
	 JOIN COMUN.MAESTRA T8 ON T8.IdRelacion = 28 and T8.Valor = T1.IdPeriodos
	 JOIN COMUN.MAESTRA T9 ON T9.IdRelacion = 1 and T9.Valor = T1.IdEstado
	 JOIN COMUN.MAESTRA T13 ON T13.IdRelacion = 84 and T13.Valor = T6.IdMedio
	 JOIN COMUN.MAESTRA T14 ON T14.IdRelacion = 89 and T14.Valor = T6.IdTipoEnlace
	 LEFT JOIN COMUN.MAESTRA T15 ON T15.IdRelacion = 92 and T15.Valor = T6.IdActivoPasivo
	 LEFT JOIN COMUN.MAESTRA T16 ON T16.IdRelacion = 95 and T16.Valor = T6.IdLocalidad
	WHERE T1.IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and
		  T1.IdPestana = @IdPestana and
		  T1.IdGrupo  =  @IdGrupo and
		  T3.IdGrupo  =  @IdGrupo and
		  T1.IdEstado = @IdEstado 

END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_ECAPEX]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Lista la configuraciones para las grillas de ecapex

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
	***********************************************************
	@Test: exec [OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_ECAPEX] 2,1,6
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_LISTAR_OPORTUNIDAD_ECAPEX]
(
	@IdOportunidadLineaNegocio	INT,
	@IdEstado   				INT,
	@IdGrupo   					INT
)
AS
BEGIN
	DECLARE @IdPestana INT=2;

	SELECT T1.IdOportunidadLineaNegocio,
		   T2.IdLineaNegocio,
		   T3.IdServicioSubServicio,
		   T1.IdFlujoCaja, 
		   T3.IdSubServicio,
		   T4.Descripcion SubServicio,
		   ISNULL(T4.CostoInstalacion,0.0) CostoInstalacion, 
		   ISNULL(T6.Cruce,'') Cruce,
		   ISNULL(T6.AEReducido,'') AEReducido,
		   T1.IdProveedor,
		   T4.Descripcion Proveedor,
		   T1.IdTipoCosto,
		   T7.Descripcion TipoCosto,
		   T1.IdPeriodos,
		   T8.Descripcion Periodos,
		   ISNULL(T1.IdPestana,0)IdPestana,
		   T1.IdGrupo,
		   T1.IdCasoNegocio,
		   T1.IdServicio,
		   ISNULL(T1.CostoUnitario,0.0)CostoUnitario,
		   T1.IdEstado,
		   T9.Descripcion Estado,
			ISNULL(T10.IdServicioCMI,0) IdServicioCMI,
			ISNULL(T11.DescripcionPlantilla,'') ServicioCMI,
			0 IdAgrupador,
			ISNULL(T1.IdTipoCosto,0) IdTipoCosto,
			ISNULL(T3.Ponderacion,0.0) Ponderacion,
			ISNULL(T12.CostoPreOperativo,0.0)CostoPreOperativo,
			ISNULL(T12.Inicio,0)Inicio,
			ISNULL(T12.Meses,0) Meses,
			ISNULL(T6.IdSubServicioDatosCapex,0) IdSubServicioDatosCapex,
			ISNULL(T6.Circuito,'')Circuito,
			ISNULL(T6.SISEGO,'')SISEGO,
			ISNULL(T6.MesesAntiguedad,0) MesesAntiguedad,
			ISNULL(T1.Cantidad,0) AS Cantidad,
			ISNULL(T6.CostoUnitarioAntiguo,0.0)CostoUnitarioAntiguo,
			ISNULL(T6.ValorResidualSoles,0.0)ValorResidualSoles,
			ISNULL(T6.CostoUnitario,0.0)CostoUnitario,
			ISNULL(T6.CapexDolares,0.0)CapexDolares,
			ISNULL(T6.CapexSoles,0.0)CapexSoles,
			ISNULL(T6.TotalCapex,0.0)TotalCapex,
			ISNULL(T6.AnioRecupero,0)AnioRecupero,
			ISNULL(T6.MesRecupero,0)MesRecupero,
			ISNULL(T6.AnioComprometido,0)AnioComprometido,
			ISNULL(T6.MesComprometido,0)MesComprometido,
			ISNULL(T6.AnioCertificado,0)AnioCertificado,
			ISNULL(T6.MesCertificado,0)MesCertificado,
			ISNULL(T6.Medio,'')Medio,
			ISNULL(T6.IdTipo,0) IdTipo,
			ISNULL(T6.IdOportunidadCosto,0) IdOportunidadCosto,
			ISNULL(T6.Garantizado,'') Garantizado,
			ISNULL(T6.Modelo,'')Modelo,
			ISNULL(T6.Combo,'')Combo,
			ISNULL(T6.Tipo,'')TipoEquipo,
			ISNULL(T6.Marca,'')Marca,
			ISNULL(T6.CapexInstalacion,0.0) Instalacion,
			ISNULL(T15.Descripcion,'') Costo,
			ISNULL(T15.VelocidadSubidaKBPS,0.0) VelocidadSubidaKBPS,
			ISNULL(T15.Monto,0.0) MontoCosto,
			ISNULL(T15.CodigoModelo,'')CodigoModelo,
			ISNULL(T15.Modelo,'') Modelo
	FROM [OPORTUNIDAD].[OportunidadFlujoCaja] T1  
	JOIN [OPORTUNIDAD].[OportunidadLineaNegocio] T2 ON T1.IdOportunidadLineaNegocio = T2.IdOportunidadLineaNegocio
	JOIN COMUN.ServicioSubServicio T3 ON T1.IdServicio = T3.IdServicio
	JOIN COMUN.Servicio T10 ON T10.IdServicio = T3.IdServicio  
	JOIN COMUN.SubServicio T4 ON T4.IdSubservicio = T3.IdSubservicio 
	LEFT JOIN COMUN.Proveedor T5 ON T5.IdProveedor = T1.IdProveedor
	JOIN OPORTUNIDAD.SubServicioDatosCapex T6 ON T6.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN [COMUN].[ServicioCMI] T11 ON T11.IdServicioCMI = T10.IdServicioCMI
	JOIN [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] T12 ON T12.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN OPORTUNIDAD.OportunidadCosto T15 ON T15.IdOportunidadLineaNegocio = T1.IdOportunidadLineaNegocio
	 JOIN COMUN.MAESTRA T7 ON T7.IdRelacion = 4 and T7.Valor = T1.IdTipoCosto
	 JOIN COMUN.MAESTRA T8 ON T8.IdRelacion = 28 and T8.Valor = T1.IdPeriodos
	 JOIN COMUN.MAESTRA T9 ON T9.IdRelacion = 1 and T9.Valor = T1.IdEstado
	WHERE T1.IdOportunidadLineaNegocio = @IdOportunidadLineaNegocio and
		  T1.IdPestana = @IdPestana and
		  T1.IdGrupo  =  @IdGrupo and
		  T3.IdGrupo  =  @IdGrupo and
		  T1.IdEstado = @IdEstado 

END

GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[USP_OBTENER_OPORTUNIDAD_ECAPEX]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    Descripción   : Lista la configuraciones para las grillas de ecapex

    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    03/08/2018  	Karina Gomez.	         Creación 
	***********************************************************
	@Test: exec [OPORTUNIDAD].[USP_OBTENER_OPORTUNIDAD_ECAPEX] 68,1,6
*/
CREATE PROCEDURE [OPORTUNIDAD].[USP_OBTENER_OPORTUNIDAD_ECAPEX]
(
	@IdSubServicioDatosCapex	INT,
	@IdEstado   				INT,
	@IdGrupo   					INT
)
AS
BEGIN
	DECLARE @IdPestana INT=2;

	SELECT T1.IdOportunidadLineaNegocio,
		   T2.IdLineaNegocio,
		   T3.IdServicioSubServicio,
		   T1.IdFlujoCaja, 
		   T3.IdSubServicio,
		   T4.Descripcion SubServicio,
		   ISNULL(T4.CostoInstalacion,0.0) CostoInstalacion, 
		   ISNULL(T6.Cruce,'') Cruce,
		   ISNULL(T6.AEReducido,'') AEReducido,
		   T1.IdProveedor,
		   T4.Descripcion Proveedor,
		   T1.IdTipoCosto,
		   T7.Descripcion TipoCosto,
		   T1.IdPeriodos,
		   T8.Descripcion Periodos,
		   ISNULL(T1.IdPestana,0)IdPestana,
		   T1.IdGrupo,
		   T1.IdCasoNegocio,
		   T1.IdServicio,
		   --ISNULL(T1.CostoUnitario,0.0)CostoUnitario,
		   T1.IdEstado,
		   T9.Descripcion Estado,
			ISNULL(T10.IdServicioCMI,0) IdServicioCMI,
			ISNULL(T11.DescripcionPlantilla,'') ServicioCMI,
			0 IdAgrupador,
			ISNULL(T1.IdTipoCosto,0) IdTipoCosto,
			ISNULL(T3.Ponderacion,0.0) Ponderacion,
			ISNULL(T12.CostoPreOperativo,0.0)CostoPreOperativo,
			ISNULL(T12.Inicio,0)Inicio,
			ISNULL(T12.Meses,0) Meses,
			ISNULL(T6.IdSubServicioDatosCapex,0) IdSubServicioDatosCapex,
			ISNULL(T6.Circuito,'')Circuito,
			ISNULL(T6.SISEGO,'')SISEGO,
			ISNULL(T6.MesesAntiguedad,0) MesesAntiguedad,
			ISNULL(T1.Cantidad,0) AS Cantidad,
			ISNULL(T6.CostoUnitarioAntiguo,0.0)CostoUnitarioAntiguo,
			ISNULL(T6.ValorResidualSoles,0.0)ValorResidualSoles,
			ISNULL(T6.CostoUnitario,0.0)CostoUnitario,
			ISNULL(T6.CapexDolares,0.0)CapexDolares,
			ISNULL(T6.CapexSoles,0.0)CapexSoles,
			ISNULL(T6.TotalCapex,0.0)TotalCapex,
			ISNULL(T6.AnioRecupero,0)AnioRecupero,
			ISNULL(T6.MesRecupero,0)MesRecupero,
			ISNULL(T6.AnioComprometido,0)AnioComprometido,
			ISNULL(T6.MesComprometido,0)MesComprometido,
			ISNULL(T6.AnioCertificado,0)AnioCertificado,
			ISNULL(T6.MesCertificado,0)MesCertificado,
			ISNULL(T6.Medio,'')Medio,
			ISNULL(T6.IdTipo,0) IdTipo,
			ISNULL(T6.IdOportunidadCosto,0) IdOportunidadCosto,
			ISNULL(T6.Garantizado,'') Garantizado,
			ISNULL(T6.Modelo,'')Modelo,
			ISNULL(T6.Combo,'')Combo,
			ISNULL(T6.Tipo,'')TipoEquipo,
			ISNULL(T6.Marca,'')Marca,
			ISNULL(T6.CapexInstalacion,0.0) Instalacion,
			ISNULL(T15.Descripcion,'') Costo,
			ISNULL(T15.VelocidadSubidaKBPS,0.0) VelocidadSubidaKBPS,
			ISNULL(T15.Monto,0.0) MontoCosto,
			ISNULL(T15.CodigoModelo,'')CodigoModelo,
			ISNULL(T15.Modelo,'') Modelo,
			ISNULL(T12.IdFlujoCajaConfiguracion,0) IdFlujoCajaConfiguracion
	FROM [OPORTUNIDAD].[OportunidadFlujoCaja] T1  
	JOIN [OPORTUNIDAD].[OportunidadLineaNegocio] T2 ON T1.IdOportunidadLineaNegocio = T2.IdOportunidadLineaNegocio
	JOIN COMUN.ServicioSubServicio T3 ON T1.IdServicio = T3.IdServicio
	JOIN COMUN.Servicio T10 ON T10.IdServicio = T3.IdServicio  
	JOIN COMUN.SubServicio T4 ON T4.IdSubservicio = T3.IdSubservicio 
	LEFT JOIN COMUN.Proveedor T5 ON T5.IdProveedor = T1.IdProveedor
	JOIN OPORTUNIDAD.SubServicioDatosCapex T6 ON T6.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN [COMUN].[ServicioCMI] T11 ON T11.IdServicioCMI = T10.IdServicioCMI
	JOIN [OPORTUNIDAD].[OportunidadFlujoCajaConfiguracion] T12 ON T12.IdFlujoCaja = T1.IdFlujoCaja
	LEFT JOIN OPORTUNIDAD.OportunidadCosto T15 ON T15.IdOportunidadLineaNegocio = T1.IdOportunidadLineaNegocio
	 JOIN COMUN.MAESTRA T7 ON T7.IdRelacion = 4 and T7.Valor = T1.IdTipoCosto
	 JOIN COMUN.MAESTRA T8 ON T8.IdRelacion = 28 and T8.Valor = T1.IdPeriodos
	 JOIN COMUN.MAESTRA T9 ON T9.IdRelacion = 1 and T9.Valor = T1.IdEstado
	WHERE T6.IdSubServicioDatosCapex = @IdSubServicioDatosCapex and
		  T6.IdEstado = @IdEstado and
		  T1.IdGrupo  =  @IdGrupo and
		  T3.IdGrupo  =  @IdGrupo 
END


GO
/****** Object:  StoredProcedure [OPORTUNIDAD].[usp_OportunidadListasCapex]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [OPORTUNIDAD].[usp_OportunidadListasCapex]
@pIDPROYECTO INT,
@pIDLINEAPRODUCTO INT,
@pIDPESTANA INT,
@pIDGRUPO INT,
@pIDESTADO INT
AS
------------------------------------------------------------------------------------
-- exec [OPORTUNIDAD].[usp_OportunidadListasCapex] 0,5,2,6,1
------------------------------------------------------------------------------------
BEGIN



select 
							   0 IdProyecto,
							   0 IdLineaProducto,
							   0 IdServicioCMI,
							   ISNULL('SERVICIO CMI','') ServicioCMI,
							    T1.IdSubServicio, 
							    T3.Descripcion as SubServicio,
							   0 IdServicioSubServicio,
							   0 IdAgrupador,
							   0 IdTipoCosto,
							   '' Proyecto,
							   0.0 Ponderacion,
							   0.0 CostoPreOperativo,
							   0 IdProveedor,
							   0 IdPeriodos,
							   '' Periodo,
							   0 Inicio,
							   0 Meses,
							   0 IdEstado,
							   0 IdSubServicioDatosCapex,
								'' Circuito,
								'' SISEGO,
								0 MesesAntiguedad,
								0 Cantidad,
								0.0 CostoUnitarioAntiguo,
								0.0 ValorResidualSoles,
								0.0 CostoUnitario,
								0.0 CapexDolares,
								0.0 CapexSoles,
								0.0 TotalCapex,
								0 AnioRecupero,
								0 MesRecupero,
								0 AnioComprometido,
								0 MesComprometido,
								0 AnioCertificado,
								0 MesCertificado,
								'' Medio,
								0 IdTipo,
								0 IdBM,
								'' Garantizado,
							   '' BM,
							   0.0 BM_Numero,
							   0.0 BM_Precio,
							   isnull('Concatenado','')Cruce,
							   T7.IdPestana,
							   T1.IdServicio,
							   T7.Descripcion Pestana,
							   T2.Descripcion Grupo,
							   isnull('AEReducido','')AEReducido,
							   ''Modelo,
							   ''Combo,
							   ''TipoEquipo,
							   ''Marca,
							   0.0 Instalacion	
into #SubServicioDefault									   			   
						from [COMUN].[ServicioSubServicio] T1 
						join [COMUN].Servicio T2 on T2.IdServicio = T1.IdServicio and
												    T2.IdEstado = T1.IdEstado 
						join [COMUN].SubServicio T3 on T3.IdSubServicio = T1.IdSubServicio and
													   T3.IdEstado = T1.IdEstado 
						left join [COMUN].[CasoNegocioServicio] T4 on T4.IdServicio = T2.IdServicio
						left join [COMUN].[CasoNegocio] T5 on T5.IdCasoNegocio = T4.IdCasoNegocio
						left join [COMUN].[LineaNegocio] T6 on T6.IdLineaNegocio = T5.IdLineaNegocio
 						join [OPORTUNIDAD].[LineaPestana] T7 on T7.IdLineaNegocio = T6.IdLineaNegocio
						WHERE T7.IdPestana = @pIDPESTANA AND
							  T6.IdLineaNegocio = @pIDLINEAPRODUCTO AND
						      T2.IdEstado = @pIDESTADO

	
	IF @pIDPROYECTO = 0 
		BEGIN
						select * from #SubServicioDefault
		END
	ELSE
		BEGIN
			
						--SELECT psc.IdProyecto,
						--		   psc.IdLineaProducto,
						--		   ISNULL(psc.IdServicioCMI,0) IdServicioCMI,
						--		   ISNULL(cmi.DescripcionPlantilla,'') ServicioCMI,
						--		   psc.IdSubServicio,
						--		   c.Descripcion SubServicio,
						--		   psc.IdServicioSubServicio,
						--		   ISNULL(psc.IdAgrupador,0) IdAgrupador,
						--		   ISNULL(psc.IdTipoCosto,0) IdTipoCosto,
						--		   isnull(psc.Descripcion,'') Proyecto,
						--		   ISNULL(psc.Ponderacion,0) Ponderacion,
						--		   ISNULL(psc.CostoPreOperativo,0.0)CostoPreOperativo,
						--		   ISNULL(psc.IdProveedor,0)IdProveedor,
						--		   isnull(psc.IdPeriodos,0) IdPeriodos,
						--		   ISNULL(prdo.Descripcion,'') Periodo,
						--		   ISNULL(psc.Inicio,0)Inicio,
						--		   ISNULL(psc.Meses,0) Meses,
						--		   ISNULL(psc.IdEstado,0) IdEstado,
						--		   ISNULL(cdc.IdSubServicioDatosCapex,0) IdSubServicioDatosCapex,
						--		   ISNULL(cdc.Circuito,'')Circuito,
						--		   ISNULL(cdc.SISEGO,'')SISEGO,
						--		   ISNULL(cdc.MesesAntiguedad,0) MesesAntiguedad,
						--			ISNULL(cdc.Cantidad,0) AS Cantidad,
						--			ISNULL(cdc.CostoUnitarioAntiguo,0.0)CostoUnitarioAntiguo,
						--			ISNULL(cdc.ValorResidualSoles,0.0)ValorResidualSoles,
						--			ISNULL(cdc.CostoUnitario,0.0)CostoUnitario,
						--			ISNULL(cdc.CapexDolares,0.0)CapexDolares,
						--			ISNULL(cdc.CapexSoles,0.0)CapexSoles,
						--			ISNULL(cdc.TotalCapex,0.0)TotalCapex,
						--			ISNULL(cdc.AnioRecupero,0)AnioRecupero,
						--			ISNULL(cdc.MesRecupero,0)MesRecupero,
						--			ISNULL(cdc.AnioComprometido,0)AnioComprometido,
						--			ISNULL(cdc.MesComprometido,0)MesComprometido,
						--			ISNULL(cdc.AnioCertificado,0)AnioCertificado,
						--			ISNULL(cdc.MesCertificado,0)MesCertificado,
						--			ISNULL(cdc.Medio,'')Medio,
						--			ISNULL(cdc.IdTipo,0) IdTipo,
						--			ISNULL(cdc.IdBWOportunidad,0) IdBM,
						--			ISNULL(cdc.Garantizado,'') Garantizado,
						--			ISNULL(bm.Descripcion,'') BM,
						--			ISNULL(bm.BW_NUM,0.0) BM_Numero,
						--			ISNULL(bm.Precio,0.0) BM_Precio,
						--			ISNULL(cdc.Cruce,'') Cruce,
						--			lcg.IdPestana,
						--			lcg.IdGrupo,
						--			'' Pestana,
						--			'' Grupo,
						--	   ISNULL(cdc.AEReducido,'')AEReducido,
						--	   ISNULL(cdc.Modelo,'')Modelo,
						--	   ISNULL(cdc.Combo,'')Combo,
						--	   ISNULL(cdc.Tipo,'')TipoEquipo,
						--	   ISNULL(cdc.Marca,'')Marca,
						--	   ISNULL(cdc.CapexInstalacion,0.0) Instalacion
						--	into #Proyecto_SubServicio
						--	FROM [OPORTUNIDAD].PROYECTOSERVICIOSubServicio psc 
						--	left join [OPORTUNIDAD].SubServicioDATOSCAPEX cdc 
						--		 on psc.IdServicioSubServicio = cdc.IdServicioSubServicio and psc.IdEstado=cdc.IdEstado
						--	left join [OPORTUNIDAD].LineaSubServicioGrupo lcg on lcg.IdLineaProducto = psc.IdLineaProducto and
						--								   lcg.IdSubServicio = psc.IdSubServicio
						--	left join [COMUN].ServicioCMI cmi on psc.IdServicioCMI = cmi.IdServicioCMI
						--	join [COMUN].SubServicio c on c.IdSubServicio = lcg.IdSubServicio
						--	left join [COMUN].Proveedor prv on psc.IdProveedor = prv.IdProveedor
						--	left join [COMUN].maestra prdo on prdo.IdRelacion = 28 and prdo.Valor = psc.IdPeriodos and prdo.IdEstado = cdc.IdEstado
						--    left join [COMUN].BW bm on bm.IdLineaProducto = lcg.IdLineaProducto and
						--					   bm.IdPestana = lcg.IdPestana and 
						--					   bm.IdGrupo = lcg.IdGrupo and
						--					   bm.IdSubServicio = c.IdSubServicio and bm.IdEstado = 1 
						--	where psc.IdProyecto = @pIDPROYECTO AND 
						--		  psc.IdLineaProducto = @pIDLINEAPRODUCTO AND 
						--		  lcg.IdPestana = @pIDPESTANA and
						--		  lcg.IdGrupo = @pIDGRUPO 
								  
					
					--select * from #SubServicioDefault where idSubServicio not in(select DISTINCT IdSubServicio from #Proyecto_SubServicio)
					--union all  
					--select * from #Proyecto_SubServicio
					select * from #SubServicioDefault
										
					DROP TABLE #Proyecto_SubServicio
		END
					DROP TABLE #SubServicioDefault
END

GO
/****** Object:  StoredProcedure [TRAZABILIDAD].[USP_CARGAR_CASOSSF]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





--EXEC [TRAZABILIDAD].USP_CARGAR_CASOSSF '\\GPPESVLCLI1487\Compartido\DIO\CargaDiariaSF\CasoHistoricoSF.xlsx', 3, '2016-04-05'
CREATE PROC [TRAZABILIDAD].[USP_CARGAR_CASOSSF] 
    @vch_FileInput varchar(300),
	@int_CodigoUsuario INT,
	@dat_FechaIngreso DATE

--WITH EXECUTE AS OWNER 
AS 
/****************************************************************************/
/* Descripción  :  SP para carga de informacion de oportunidades del Sales Force*/
/* Modificaciones :                                                         */
/***************************************************************************
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    08/08/2018  	Heber Atalaya R.	   Creación 

***************************************************************************/
SET NOCOUNT ON 
	
BEGIN
	DECLARE @comandosql nvarchar(3000)
	DECLARE @resultadoexeccmd INT
	DECLARE @returnstatus nvarchar(15)
	DECLARE @int_CodigoEmpresa INT
	DECLARE @int_CodigoAlmacen INT
	DECLARE @vch_CodigoAlmacenMovistar VARCHAR(30)


	SELECT @resultadoexeccmd = 0

	DELETE FROM [DIO].[TRAZABILIDAD].[CasoHistoricoSF] 

	SELECT @comandosql = N'INSERT INTO [DIO].[TRAZABILIDAD].[CasoHistoricoSF] SELECT * FROM OPENROWSET(''Microsoft.ACE.OLEDB.12.0'', ''Excel 12.0;Database=' + LTRIM(RTRIM(@vch_FileInput)) + ';HDR=YES;IMEX=1'', ''SELECT 
* FROM [CasoHistoricoSF$]'')'
	--SELECT @comandosql = N'INSERT INTO IngresoProductosTEMP SELECT ' + LTRIM(RTRIM(STR(@int_CodigoUsuario))) + ', 0.00, * FROM OPENQUERY(''ExcelDataSourceTCC'', ''SELECT * FROM [Detalle$]'')'
	--SELECT @comandosql

	EXEC (@comandosql)

	SELECT @resultadoexeccmd = COUNT(IdOportunidadSF) FROM [DIO].[TRAZABILIDAD].[CasoHistoricoSF] 

	--SELECT @resultadoexeccmd

	/*
	--IdOportunidad, 
	INSERT INTO [TRAZABILIDAD].[OportunidadST]
	(IdOportunidadPadre, IdOportunidadSF, IdOportunidadAux, Descripcion, IdTipoOportunidad, IdMotivoOportunidad, IdTipoEntidadCliente, 
	IdCliente, 
	IdSector, IdSegmentoNegocio, 
	IdTipoCicloVenta, 
	IdTipoCicloImplementacion, 
	Prioridad, ProbalidadExito, 
	PorcentajeCierre, PorcentajeCheckList, IdFase, IdEtapa, FlgCapexMayor, ImporteCapex, 
	IdMoneda, ImporteFCV, 
	FechaApertura, FechaCierre, IdEstadoOportunidad, 
	IdRecursoComercial, IdRecursoPreventa, 
	IdEstado, IdUsuarioCreacion, FechaCreacion, IdUsuarioEdicion, FechaEdicion)
	SELECT TOP 50 
	NULL, OSF.IdOportunidad, NULL, OSF.NombreOportunidad, NULL, NULL, NULL, 
	(SELECT IdCliente FROM COMUN.Cliente WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(OSF.NombreCliente)) ),
	NULL, NULL,
	(SELECT Valor FROM COMUN.Maestra WHERE IdRelacion = 156 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(OSF.CicloVenta)) ),
	(SELECT Valor FROM COMUN.Maestra WHERE IdRelacion = 165 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(OSF.TipoCiclo)) ),
	NULL, OSF.ProbalidadExito,
	0, 0, NULL, NULL, NULL, 0.00,
	(CASE OSF.MonedaFCV WHEN 'PEN' THEN 1 ELSE 2 END), OSF.ImporteFCV,
	NULL, NULL, NULL,
	NULL, NULL, 
	1, 1, GETDATE(), NULL, NULL  
	FROM [TRAZABILIDAD].[OportunidadSF] AS OSF   
	WHERE IdOportunidad NOT IN (SELECT DISTINCT IdOportunidadSF FROM [TRAZABILIDAD].[OportunidadST]) 
	*/

END



GO
/****** Object:  StoredProcedure [TRAZABILIDAD].[USP_CARGAR_CASOSSF02]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





--EXEC [TRAZABILIDAD].USP_CARGAR_CASOSSF02 '\\GPPESVLCLI1487\Compartido\DIO\CargaDiariaSF\CasoHistoricoSF02.xlsx', 3, '2016-04-05'
CREATE PROC [TRAZABILIDAD].[USP_CARGAR_CASOSSF02] 
    @vch_FileInput varchar(300),
	@int_CodigoUsuario INT,
	@dat_FechaIngreso DATE

--WITH EXECUTE AS OWNER 
AS 
/****************************************************************************/
/* Descripción  :  SP para carga de informacion de casos del Sales Force*/
/* Modificaciones :                                                         */
/***************************************************************************
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    08/08/2018  	Heber Atalaya R.	   Creación 

***************************************************************************/
SET NOCOUNT ON 
	
BEGIN
	DECLARE @comandosql nvarchar(3000)
	DECLARE @resultadoexeccmd INT
	DECLARE @returnstatus nvarchar(15)
	DECLARE @int_CodigoEmpresa INT
	DECLARE @int_CodigoAlmacen INT
	DECLARE @vch_CodigoAlmacenMovistar VARCHAR(30)


	SELECT @resultadoexeccmd = 0

	DELETE FROM [DIO].[TRAZABILIDAD].[CasoHistoricoSF02] 

	SELECT @comandosql = N'INSERT INTO [DIO].[TRAZABILIDAD].[CasoHistoricoSF02] SELECT * FROM OPENROWSET(''Microsoft.ACE.OLEDB.12.0'', ''Excel 12.0;Database=' + LTRIM(RTRIM(@vch_FileInput)) + ';HDR=YES;IMEX=1'', ''SELECT 
* FROM [CasoHistoricoSF02$]'')'
	--SELECT @comandosql = N'INSERT INTO IngresoProductosTEMP SELECT ' + LTRIM(RTRIM(STR(@int_CodigoUsuario))) + ', 0.00, * FROM OPENQUERY(''ExcelDataSourceTCC'', ''SELECT * FROM [Detalle$]'')'
	--SELECT @comandosql

	EXEC (@comandosql)

	SELECT @resultadoexeccmd = COUNT(IdOportunidadSF) FROM [DIO].[TRAZABILIDAD].[CasoHistoricoSF02] 

	--SELECT @resultadoexeccmd


	UPDATE [DIO].[TRAZABILIDAD].[CasoHistoricoSF02]
	SET FechaApertura = REPLACE(FechaApertura,'. m.', '.m.'),
	FechaAsignacion = REPLACE(FechaAsignacion,'. m.', '.m.'),
	FechaCierre = REPLACE(FechaCierre,'. m.', '.m.'),
	FechaFinAsignacion = REPLACE(FechaFinAsignacion,'. m.', '.m.'), 
	FechaCompromisoAtencion = REPLACE(FechaCompromisoAtencion,'. m.', '.m.')

	UPDATE [DIO].[TRAZABILIDAD].[CasoHistoricoSF02]
	SET FechaApertura = REPLACE(FechaApertura,'p.m.', 'PM'), 
	FechaAsignacion = REPLACE(FechaAsignacion,'p.m.', 'PM'),
	FechaCierre = REPLACE(FechaCierre,'p.m.', 'PM'),
	FechaFinAsignacion = REPLACE(FechaFinAsignacion,'p.m.', 'PM'),
	FechaCompromisoAtencion = REPLACE(FechaCompromisoAtencion,'p.m.', 'PM')

	UPDATE [DIO].[TRAZABILIDAD].[CasoHistoricoSF02]
	SET FechaApertura = REPLACE(FechaApertura,'a.m.', 'AM'), 
	FechaAsignacion = REPLACE(FechaAsignacion,'a.m.', 'AM'),
	FechaCierre = REPLACE(FechaCierre,'a.m.', 'AM'),
	FechaFinAsignacion = REPLACE(FechaFinAsignacion,'a.m.', 'AM'),
	FechaCompromisoAtencion = REPLACE(FechaCompromisoAtencion,'a.m.', 'AM')

	
	UPDATE OST
	SET 
	OST.IdTipoOportunidad = (SELECT TOP 1 IdTipoOportunidad FROM [TRAZABILIDAD].[TipoOportunidad], [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF2.TipoOportunidad)) AND CSF2.IdOportunidadSF = OST.IdOportunidadSF  ORDER BY CSF2.FechaAsignacion DESC),
	OST.IdMotivoOportunidad = (SELECT TOP 1 IdMotivoOportunidad FROM [TRAZABILIDAD].[MotivoOportunidad], [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF2.TipoOportunidad)) AND CSF2.IdOportunidadSF = OST.IdOportunidadSF  ORDER BY CSF2.FechaAsignacion DESC),
	OST.IdTipoEntidadCliente = (SELECT TOP 1 Convert(int,Valor) FROM COMUN.Maestra, [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE IdRelacion = 168 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF2.TipoEntidad)) AND CSF2.IdOportunidadSF = OST.IdOportunidadSF  ORDER BY CSF2.FechaAsignacion DESC), 
	OST.IdSector = (SELECT TOP 1 IdSector FROM [COMUN].[Sector], [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF2.SectorComercial)) AND CSF2.IdOportunidadSF = OST.IdOportunidadSF  ORDER BY CSF2.FechaAsignacion DESC),
	--OST.IdSegmentoNegocio = 
	OST.IdTipoCicloVenta = (SELECT TOP 1 Convert(int,Valor) FROM COMUN.Maestra, [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE IdRelacion = 156 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF2.TipoCiclo)) AND CSF2.IdOportunidadSF = OST.IdOportunidadSF ORDER BY CSF2.FechaAsignacion DESC), 
	--OST.IdTipoCicloImplementacion = 
	--OST.IdFase = 
	OST.IdEtapa = (SELECT TOP 1 IdEtapa FROM [TRAZABILIDAD].[EtapaOportunidad], [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF2.EtapaOportunidad)) AND CSF2.IdOportunidadSF = OST.IdOportunidadSF  ORDER BY CSF2.FechaAsignacion DESC),
	--OST.IdEstadoOportunidad = 
	--OST.IdRecursoComercial = 
	--OST.IdRecursoPreventa =  
	OST.FechaApertura = (SELECT MIN(TRY_PARSE(CSF2.FechaApertura AS DATETIME USING 'en-gb')) FROM [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE CSF2.IdOportunidadSF = OST.IdOportunidadSF),
	OST.FechaCierre = (SELECT MIN(TRY_PARSE(CSF2.FechaCierre AS DATETIME USING 'en-gb')) FROM [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF2 WHERE CSF2.IdOportunidadSF = OST.IdOportunidadSF)
	FROM [TRAZABILIDAD].[OportunidadST] AS OST 



	INSERT INTO [TRAZABILIDAD].[CasoOportunidad] 
	(IdOportunidad, Descripcion, IdCasoSF, IdCasoPadre, 
	IdTipoSolicitud, 
	Asunto, IdFase, IdEtapa, 
	Complejidad, 
	Prioridad, IdEstadoCaso, 
	SolucionCaso, 
	FechaApertura, FechaCierre, FechaCompromisoAtencion, 
	IdEstado, IdUsuarioCreacion, FechaCreacion, IdUsuarioEdicion, FechaEdicion)
	SELECT DISTINCT 
	OST.IdOportunidad, '', CSF.IdCasoSF, NULL, 
	NULL,
	'', NULL, NULL,
	NULL, 
	NULL, NULL,
	'', 
	NULL, NULL, NULL, 
	1, 1, getdate(), NULL, NULL 
	FROM [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF, [TRAZABILIDAD].[OportunidadST] AS OST    
	WHERE CSF.IdOportunidadSF = OST.IdOportunidadSF 
	AND CSF.IdCasoSF NOT IN (SELECT DISTINCT IdCasoSF FROM [TRAZABILIDAD].[CasoOportunidad] WHERE IdOportunidad = OST.IdOportunidad) 
	ORDER BY OST.IdOportunidad, CSF.IdCasoSF



	UPDATE COSF
	SET   	
	COSF.IdTipoSolicitud = (SELECT TOP 1 IdTipoSolicitud FROM [TRAZABILIDAD].[TipoSolicitud] WHERE Descripcion = CSF.TipoSolicitud), 
	COSF.Asunto = '', COSF.IdFase = (SELECT TOP 1 IdFase FROM [TRAZABILIDAD].[EtapaOportunidad] WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF.EtapaOportunidad))), COSF.IdEtapa = (SELECT TOP 1 IdEtapa FROM [TRAZABILIDAD].[EtapaOportunidad] WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF.EtapaOportunidad))), 
	COSF.Complejidad = (SELECT TOP 1 Convert(int,Valor) FROM COMUN.Maestra WHERE IdRelacion = 160 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF.TipoEntidad))),
	COSF.IdEstadoCaso = (SELECT TOP 1 IdEstadoCaso FROM [TRAZABILIDAD].[EstadoCaso] WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(CSF.EstadoCaso))),
	COSF.SolucionCaso = '', 
	COSF.FechaApertura = TRY_PARSE(CSF.FechaApertura AS DATETIME USING 'en-gb'), 
	COSF.FechaCierre = TRY_PARSE(CSF.FechaCierre AS DATETIME USING 'en-gb'), 
	COSF.FechaCompromisoAtencion = TRY_PARSE(CSF.FechaCompromisoAtencion AS DATETIME USING 'en-gb'), 
	COSF.IdEstado = 1, COSF.IdUsuarioCreacion = 1, COSF.FechaCreacion = getdate(), COSF.IdUsuarioEdicion = NULL, COSF.FechaEdicion = NULL 
	FROM [TRAZABILIDAD].[CasoOportunidad] COSF, [TRAZABILIDAD].[CasoHistoricoSF02] AS CSF     
	WHERE CSF.IdCasoSF = COSF.IdCasoSF  
	AND TRY_PARSE(CSF.FechaAsignacion AS DATETIME USING 'en-gb') = 
	(SELECT MAX(TRY_PARSE(FechaAsignacion AS DATETIME USING 'en-gb')) 
	  FROM [DIO].[TRAZABILIDAD].[CasoHistoricoSF02] 
	  WHERE IdCasoSF = COSF.IdCasoSF 
	  GROUP BY IdOportunidadSF, IdCasoSF
	)


END



GO
/****** Object:  StoredProcedure [TRAZABILIDAD].[USP_CARGAR_OPORTUNIDADESSF]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





--EXEC [TRAZABILIDAD].USP_CARGAR_OPORTUNIDADESSF '\\GPPESVLCLI1487\Compartido\DIO\CargaDiariaSF\OportunidadSF.xlsx', 3, '2016-04-05'
CREATE PROC [TRAZABILIDAD].[USP_CARGAR_OPORTUNIDADESSF] 
    @vch_FileInput varchar(300),
	@int_CodigoUsuario INT,
	@dat_FechaIngreso DATE

--WITH EXECUTE AS OWNER 
AS 
/****************************************************************************/
/* Descripción  :  SP para carga de informacion de oportunidades del Sales Force*/
/* Modificaciones :                                                         */
/***************************************************************************
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
    31/07/2018  	Heber Atalaya R.	   Creación 

***************************************************************************/
SET NOCOUNT ON 
	
BEGIN
	DECLARE @comandosql nvarchar(3000)
	DECLARE @resultadoexeccmd INT
	DECLARE @returnstatus nvarchar(15)
	DECLARE @int_CodigoEmpresa INT
	DECLARE @int_CodigoAlmacen INT
	DECLARE @vch_CodigoAlmacenMovistar VARCHAR(30)


	SELECT @resultadoexeccmd = 0

	DELETE FROM [DIO].[TRAZABILIDAD].[OportunidadSF] 

	SELECT @comandosql = N'INSERT INTO [DIO].[TRAZABILIDAD].[OportunidadSF] SELECT * FROM OPENROWSET(''Microsoft.ACE.OLEDB.12.0'', ''Excel 12.0;Database=' + LTRIM(RTRIM(@vch_FileInput)) + ';HDR=YES;IMEX=1'', ''SELECT 
* FROM [OportunidadSF$]'')'
	--SELECT @comandosql = N'INSERT INTO IngresoProductosTEMP SELECT ' + LTRIM(RTRIM(STR(@int_CodigoUsuario))) + ', 0.00, * FROM OPENQUERY(''ExcelDataSourceTCC'', ''SELECT * FROM [Detalle$]'')'
	--SELECT @comandosql

	EXEC (@comandosql)

	SELECT @resultadoexeccmd = COUNT(IdOportunidad) FROM [DIO].[TRAZABILIDAD].[OportunidadSF] 

	--SELECT @resultadoexeccmd

	--IdOportunidad, 
	INSERT INTO [TRAZABILIDAD].[OportunidadST]
	(IdOportunidadPadre, IdOportunidadSF, IdOportunidadAux, Descripcion, IdTipoOportunidad, IdMotivoOportunidad, IdTipoEntidadCliente, 
	IdCliente, 
	IdSector, IdSegmentoNegocio, 
	IdTipoCicloVenta, 
	IdTipoCicloImplementacion, 
	Prioridad, ProbalidadExito, 
	PorcentajeCierre, PorcentajeCheckList, IdFase, IdEtapa, FlgCapexMayor, ImporteCapex, 
	IdMoneda, ImporteFCV, 
	FechaApertura, FechaCierre, IdEstadoOportunidad, 
	IdRecursoComercial, IdRecursoPreventa, 
	IdEstado, IdUsuarioCreacion, FechaCreacion, IdUsuarioEdicion, FechaEdicion)
	SELECT  
	NULL, OSF.IdOportunidad, NULL, OSF.NombreOportunidad, NULL, NULL, NULL, 
	(SELECT TOP 1 IdCliente FROM COMUN.Cliente WHERE RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(OSF.NombreCliente)) ),
	NULL, NULL,
	(SELECT TOP 1 Convert(int,Valor) FROM COMUN.Maestra WHERE IdRelacion = 156 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(OSF.CicloVenta)) ),
	(SELECT TOP 1 Convert(int,Valor) FROM COMUN.Maestra WHERE IdRelacion = 165 AND RTRIM(LTRIM(Descripcion)) = RTRIM(LTRIM(OSF.TipoCiclo)) ),
	NULL, OSF.ProbalidadExito,
	0, 0, NULL, NULL, NULL, 0.00,
	(CASE OSF.MonedaFCV WHEN 'PEN' THEN 1 ELSE 2 END), OSF.ImporteFCV,
	NULL, NULL, NULL,
	NULL, NULL, 
	1, 1, GETDATE(), NULL, NULL  
	FROM [TRAZABILIDAD].[OportunidadSF] AS OSF   
	WHERE IdOportunidad NOT IN (SELECT DISTINCT IdOportunidadSF FROM [TRAZABILIDAD].[OportunidadST]) 
	ORDER BY OSF.IdOportunidad

END



GO
/****** Object:  StoredProcedure [TRAZABILIDAD].[USP_Listar_Oportunidad_Matriz]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
    Descripción   : Listar las oportundiades por actividad
    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
     30/07/2017  	Elvis A.	            Listar 
*/
-- [Trazabilidad].USP_Listar_Oportunidad_Matriz
/*
IdSeguimiento
IdOportunidad
OportunidadDesc
IdEstadoOportunidad
EstadoOportunidadDesc
IdEtapa
EtapaDesc
RecursoId
RecursoNombre
IdCliente
ClienteDesc

IdActividad
ActividadDesc
IdEstadoEjecucion
FechaInicioReal
FechaFinReal
Tiene
*/

CREATE   PROCEDURE [TRAZABILIDAD].[USP_Listar_Oportunidad_Matriz]
@IdOportunidad int =-1, --1417
@IdCliente int =-1, /*(select * from COMUN.Cliente)*/
@EstadoOportunidad int=-1, /*144	EN PROCESO: (select IdMaestra,Descripcion as EstadoOportunidad from [DIO].[COMUN].[Maestra] where IdRelacion=142)*/ 
@FechaInicio varchar(10)='',--'28/07/2018'
@FechaFin varchar(23)=''--'30/07/2018'
as
begin

set nocount on;
set dateformat dmy;

declare @tabla table(
[IdOportunidad] int,
[OportunidadDesc]   varchar(250),
IdEstadoOportunidad int,
EstadoOportunidadDesc varchar(100),
IdEtapa				int,
EtapaDesc				varchar(100),
IdCliente			int,
ClienteDesc  varchar(250)				
)
 

IF isnull(@FechaInicio,'')='' or isnull(@FechaFin,'')=''
begin
set @FechaInicio=convert(varchar, DATEADD(year,-1,getdate()),103)
set @FechaFin=convert(varchar, getdate(),103)
end

 
insert into @tabla
select 
a.IdOportunidad,a.Descripcion as NombreOportunidad,
a.IdEstadoOportunidad,d.EstadoOportunidad,
a.IdEtapa, c.Descripcion as Estapa,
b.IdCliente,b.Descripcion as Cliente
from 
[TRAZABILIDAD].[OportunidadST] a 
inner join COMUN.Cliente b on a.IdCliente=b.IdCliente
inner join [TRAZABILIDAD].[EtapaOportunidad] c on a.IdEtapa=c.IdEtapa 
left join (select IdMaestra,Descripcion as EstadoOportunidad from [DIO].[COMUN].[Maestra] where IdRelacion=142) d on a.IdEstadoOportunidad=d.IdMaestra --Estado de Oportunidad
where 
a.IdOportunidad = case when @IdOportunidad<>-1 then @IdOportunidad else a.IdOportunidad end
and b.IdCliente = case when @IdCliente<>-1 then @IdCliente else b.IdCliente end
and c.IdEtapa in (4,5,7)		--Únicamente F1(F1-Ganada), F2 y  F3
and d.IdMaestra	= case when @EstadoOportunidad<>-1 then @EstadoOportunidad else d.IdMaestra end --Estado oportunidad
and 
a.IdOportunidad in 
(
select distinct x1.IdOportunidad from TRAZABILIDAD.OportunidadST x1 left join [TRAZABILIDAD].[DetalleActividadOportunidad] dao on x1.IdOportunidad=dao.IdOportunidad 
where 
cast(isnull(dao.FechaInicioEstimada,@fechaInicio) as date)>=cast(@FechaInicio as date)   
and cast(isnull(dao.FechaFinEstimada,@FechaFin) as date)<=cast(@FechaFin as date)
and isnull(dao.IdEstado,2)=2		--/*2:activo;3: inactivo; 124: eliminado (Maestro IdMaestro=1)*/
)
 and a.IdEstado=2		--/*2:activo;3: inactivo; 124: eliminado (Maestro IdMaestro=1)*/
--order by a.IdOportunidad asc 

 

select
isnull( b.IdSeguimiento,-1) as IdSeguimiento,
a.IdOportunidad,
a.OportunidadDesc,
a.IdEstadoOportunidad,
a.EstadoOportunidadDesc,
a.IdEtapa,
a.EtapaDesc,
isnull(c.IdRecurso,-1) as RecursoId,
isnull(c.Nombre,'') as RecursoNombre,
a.IdCliente,
a.ClienteDesc,
a.IdActividad,
a.ActividadDesc,
 
 --b.IdActividad,
cast(isnull(b.IdEstadoEjecucion,0) as bit) as IdEstadoEjecucion,
isnull(convert(varchar,b.FechaInicioReal,103),'') as FechaInicioReal, 
isnull(convert(varchar,b.FechaFinReal,103),'') as FechaFinReal, 

cast(case when isnull(b.IdActividad,0)=0 then 0 else 1 end as bit) Tiene
from  
(select 
x.IdOportunidad,x.OportunidadDesc,
x.IdEstadoOportunidad,x.EstadoOportunidadDesc,
x.IdEtapa,x.EtapaDesc,
x.IdCliente,x.ClienteDesc,
y.IdActividad,y.Descripcion  as ActividadDesc
from @tabla x,(select IdActividad,Descripcion from [TRAZABILIDAD].[ActividadOportunidad] where AsegurarOferta=1 ) y) a
left join [TRAZABILIDAD].[DetalleActividadOportunidad] b on a.IdOportunidad=b.IdOportunidad and a.IdActividad=b.IdActividad
left join TRAZABILIDAD.Recurso c on c.IdRecurso=b.IdRecursoRevisor
order by a.IdOportunidad asc, a.IdActividad asc


end


--select * from COMUN.Maestra where Descripcion like '%ejecu%'
GO
/****** Object:  StoredProcedure [TRAZABILIDAD].[USP_Registrar_Oportunidad_Matriz]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
    Descripción   : Registrar las oportundiades por actividad
    Modificaciones:
    ***********************************************************
    Fecha           	Autor         		Acción
    ***********************************************************
     30/07/2017  	Elvis A.	            Registrar/Actualziar 
*/

-- -1=Crea
-- Trazabilidad.USP_Registrar_Oportunidad_Matriz -1,1417,56,0,'30/07/2018','30/07/2018',1

-- 53= actualzia en caso encuentre
-- USP_Registrar_Oportunidad_Matriz 53,1417,56,1,'30/07/2018','30/07/2018',1

-- Trazabilidad.USP_Registrar_Oportunidad_Matriz -1,2,55,1,'02/08/2018','02/08/2018',1


CREATE PROCEDURE [TRAZABILIDAD].[USP_Registrar_Oportunidad_Matriz]
@IdSeguimiento int,
@IdOportunidad int,

@IdActividad int,
@IdEstadoEjecucion bit,
@FechaInicioReal varchar(10),
@FechaFinReal varchar(10),

@IdUsuario int
as
begin 

set nocount on;
set dateformat dmy;

if exists(select * from [TRAZABILIDAD].[DetalleActividadOportunidad] a where a.IdSeguimiento=@IdSeguimiento)
begin

	update [TRAZABILIDAD].[DetalleActividadOportunidad] set 
	IdEstadoEjecucion=@IdEstadoEjecucion, 
	FechaInicioReal=@FechaInicioReal,
	FechaFinReal=@FechaFinReal,

	IdUsuarioEdicion=@IdUsuario,
	FechaEdicion=GETDATE()
	where
	IdSeguimiento=@IdSeguimiento
 

end
else
begin
	insert into [TRAZABILIDAD].[DetalleActividadOportunidad]
	(IdOportunidad,IdActividad, IdEstadoEjecucion,FechaInicioReal,FechaFinReal, IdUsuarioCreacion, FechaCreacion)
	values
	(@IdOportunidad,@IdActividad, @IdEstadoEjecucion,@FechaInicioReal,@FechaFinReal, @IdUsuario, getdate())
end

 
end


GO
/****** Object:  View [CAPEX].[VistaOportunidad]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [CAPEX].[VistaOportunidad]  
   AS  
   select IdOportunidad, FechaCreacion, NombreCliente, Etapa, NombreOportunidad, ProbabilidadExito, FechaCierreEstimada, PropietarioOportunidad 
   from COMUN.SalesForceOrigen
   group by IdOportunidad, FechaCreacion, NombreCliente, Etapa, NombreOportunidad, ProbabilidadExito, FechaCierreEstimada, PropietarioOportunidad;  


GO
/****** Object:  View [CAPEX].[VistaFetTotal]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [CAPEX].[VistaFetTotal]
AS
SELECT        SCA.iusu_id, SCA.iest_id, SCA.dsca_fec, SCA.copo_cod, OPO.FechaCreacion, OPO.NombreCliente, OPO.Etapa, OPO.NombreOportunidad, OPO.ProbabilidadExito, 
                         OPO.FechaCierreEstimada, OPO.PropietarioOportunidad, PRY.IdProyecto, PRY.TipoProyecto, SCA.iten_id, TEN.vten_nom, SCA.vsca_lin, SCA.tsca_dgp, SCA.tsca_rso,
                          SCA.tsca_oso, SCA.nsca_van, SCA.nsca_if2, SCA.isca_pay, SCA.tsca_scl, SCA.tsca_scn, EST.vest_nom, EST.vest_cls, ISNULL(PRY.NroMesesPagoRecurrente, 0) 
                         AS NroMesesPagoRecurrente, PRY.IngePreventa, PRY.LiderPreventa, PRY.Capex, PRY.Monto, PRY.TipoCambio, PRY.Obida, SCA.isca_id
FROM            CAPEX.solicitud_capex AS SCA LEFT OUTER JOIN
                         CAPEX.VistaOportunidad AS OPO ON OPO.IdOportunidad = SCA.copo_cod LEFT OUTER JOIN
                         PROYECTO.ProyectoSF AS PRY ON PRY.IdOportunidad = OPO.IdOportunidad LEFT OUTER JOIN
                         CAPEX.tipo_entidad AS TEN ON TEN.iten_id = SCA.iten_id INNER JOIN
                         CAPEX.estado AS EST ON EST.iest_id = SCA.iest_id
WHERE        (SCA.copo_cod IS NOT NULL) AND (SCA.iest_id IN ('20', '21', '22', '23', '24', '25', '26'))

GO
/****** Object:  UserDefinedFunction [FUNNEL].[usp_IndicadorMadurezConsultar_Table]    Script Date: 8/22/2018 9:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Descripcion   : Reporte popup del reporte de Probabilidad por AÑO/Mes
Modificaciones    :
*************************************************************
Fecha           Autor         Motivo del Cambio
*************************************************************
10/07/2018       jaguilar            Creacion
*/

CREATE FUNCTION [FUNNEL].[usp_IndicadorMadurezConsultar_Table]
(
@Anio int = NULL,
@Mes int = NULL,
@IdOportunidad VARCHAR(255),
@LineaNegocio VARCHAR(100) = NULL,
@IdSector INT = NULL,
@ProbabilidadExito INT = null,
@Etapa char(2) = null
)
RETURNS TABLE
AS
RETURN
(

		WITH cte AS
		(
		SELECT 
		S.ProbabilidadExito AS PROBABILIDAD,
		(SELECT TOP 1 [FechaCreacion]
		FROM COMUN.SalesForceConsolidadoDetalle 
		WHERE IdOportunidad = S.IdOportunidad
		ORDER BY CONVERT(INT,NumeroDelCaso) DESC ) AS ANTIGUEDAD,
		(SELECT COUNT(ID) FROM [COMUN].[SalesForceConsolidadoDetalle] WHERE IdOportunidad = S.IdOportunidad
		AND TIPOSOLICITUD = 'Evaluación Análisis Financiero' AND IDESTADO = 1) AS OFERTAS,
		CASE S.Etapa
			WHEN 'F2 - Gestión de Contratos' THEN 20
			WHEN 'F3 - Negociación' THEN 15
			WHEN 'F4 - Diseño de la Solución' THEN 10
			WHEN 'F5 - Definición de la necesidad' THEN 5
			WHEN 'F6 - Preoportunidad' THEN 0
			END AS ETAPA,
		S.[FechaCierreEstimada],
		S.IdOportunidad,
		S.ASUNTO,
		S.NombreCliente,
		cli.CodigoCliente,
		O.CAPEX,
		O.IngresoTotal,
		F.LineaNegocio,
		SEC.Descripcion AS SECTOR,
		O.Oibda,
		O.Opex
		FROM COMUN.SalesForceConsolidadoCabecera S INNER JOIN 
			(	
				SELECT IdOportunidad,
				SUM(CAPEX * TipoCambioCapex) AS CAPEX,
				SUM(IngresoAnual * TipoCambioIngresos) AS IngresoTotal,
				SUM(OibdaAnual) AS Oibda,
				ISNULL(SUM(CostosDirectos * TipoCambioCostos),0) AS Opex 
				FROM FUNNEL.OportunidadContableOrigen
				WHERE IdEstado = 1
				GROUP BY IdOportunidad
			) O ON S.IdOportunidad = O.IdOportunidad
		INNER JOIN FUNNEL.OportunidadFinancieraOrigen F
		ON O.IdOportunidad = F.IdOportunidad
		LEFT OUTER JOIN COMUN.Cliente CLI 
		ON S.NombreCliente = CLI.Descripcion
		LEFT OUTER JOIN [COMUN].[Sector] SEC
		ON CLI.IdSector = SEC.IdSector
		WHERE S.IdEstado = 1 AND F.IdEstado = 1 AND CLI.IdEstado = 1
		AND (@IdOportunidad = '0' OR S.IdOportunidad = @IdOportunidad)
		AND (@Mes IS NULL OR MONTH(S.FechaCierreEstimada) = @Mes) 
		AND (@Anio IS NULL OR YEAR(S.FechaCierreEstimada) = @Anio)
		AND (@LineaNegocio IS NULL OR F.LineaNegocio = @LineaNegocio)
		AND (@IdSector IS NULL OR CLI.IdSector = @IdSector)
		AND (@ProbabilidadExito IS NULL OR CONVERT(INT,S.ProbabilidadExito) = @ProbabilidadExito)
		AND (@Etapa IS NULL OR LEFT(S.Etapa,2) = @Etapa)
		),
		ct3_2 AS
		(
		SELECT (((CONVERT(NUMERIC(10,2),PROBABILIDAD) * 20/100) * 0.1 ) + 
		((CASE WHEN DATEDIFF(DAY,CONVERT(DATE,ANTIGUEDAD), CONVERT(DATE,GETDATE())) > = 365
		THEN 20 ELSE ((DATEDIFF(DAY,CONVERT(DATE,ANTIGUEDAD), CONVERT(DATE,GETDATE()))) * 20 / 365)
		END) * 0.3) + 
		(OFERTAS * 0.4) +
		(ETAPA * 0.2))/1 AS MADUREZ,
		 MONTH (FechaCierreEstimada) AS MESORDEN,
		CASE MONTH (FechaCierreEstimada) 
			WHEN 1 THEN 'Ene'
			WHEN 2 THEN 'Feb'
			WHEN 3 THEN 'Mar'
			WHEN 4 THEN 'Abr'
			WHEN 5 THEN 'May'
			WHEN 6 THEN 'Jun'
			WHEN 7 THEN 'Jul'
			WHEN 8 THEN 'Ago'
			WHEN 9 THEN 'Set'
			WHEN 10 THEN 'Oct'
			WHEN 11 THEN 'Nov'
			WHEN 12 THEN 'Dic' 
			END AS MES,
		YEAR (FechaCierreEstimada) AS ANIO,
		IdOportunidad,
		ASUNTO,
		NombreCliente,
		CAPEX,
		IngresoTotal,
		FechaCierreEstimada,
		LineaNegocio,
		SECTOR,
		CodigoCliente,
		Oibda,
		Opex,
		ETAPA
		FROM cte
		)
		SELECT top 100 percent 
		CASE 
		WHEN CONVERT(INT,ROUND(MADUREZ,0)) BETWEEN 1 AND 4 THEN 1
		WHEN CONVERT(INT,ROUND(MADUREZ,0)) BETWEEN 5 AND 8 THEN 2
		WHEN CONVERT(INT,ROUND(MADUREZ,0)) BETWEEN 9 AND 12 THEN 3
		WHEN CONVERT(INT,ROUND(MADUREZ,0)) BETWEEN 13 AND 16 THEN 4
		WHEN CONVERT(INT,ROUND(MADUREZ,0)) BETWEEN 17 AND 20 THEN 5
		END AS MADUREZ,
		CONVERT(INT,ROUND(MADUREZ,0))  AS MADUREZ_VALOR,
		ROW_NUMBER() OVER(PARTITION BY MESORDEN ORDER BY CONVERT(INT,ROUND(MADUREZ,0))) * 
		0.05 +
		MESORDEN AS MESORDEN,
		MES,ANIO,IdOportunidad,ASUNTO,NombreCliente,CAPEX,FechaCierreEstimada,LineaNegocio,
		SECTOR,CodigoCliente,IngresoTotal,Oibda,Opex,ETAPA FROM ct3_2
		ORDER BY MESORDEN
)

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[57] 4[4] 2[25] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SCA"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OPO"
            Begin Extent = 
               Top = 32
               Left = 328
               Bottom = 161
               Right = 543
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TEN"
            Begin Extent = 
               Top = 207
               Left = 236
               Bottom = 319
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EST"
            Begin Extent = 
               Top = 208
               Left = 514
               Bottom = 337
               Right = 723
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PRY"
            Begin Extent = 
               Top = 19
               Left = 628
               Bottom = 148
               Right = 886
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End' , @level0type=N'SCHEMA',@level0name=N'CAPEX', @level1type=N'VIEW',@level1name=N'VistaFetTotal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
End
' , @level0type=N'SCHEMA',@level0name=N'CAPEX', @level1type=N'VIEW',@level1name=N'VistaFetTotal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'CAPEX', @level1type=N'VIEW',@level1name=N'VistaFetTotal'
GO
